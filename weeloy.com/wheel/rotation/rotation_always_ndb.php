<?php

class WY_Images { function l_file_exists($filename) { if($filename == "wheelscript.js") return true; return false; } function showPicture($dummy, $imgname) { return $imgname; } }
class WY_restaurant {  var $logo; var $wheel; function __construct() { $this->logo = "logo.png"; $this->wheel = "stdwheel.png"; } function getRestaurant($dummy) { } }
$_SESSION['user']['member_type'] = "";

function showWheel($content, $style) {

	echo "<html><head><title>Rotation</title>"
		. "<meta name='copyrightright' content='all right reserved (c) WEELOY PTE, 2014 version 3.7'>"
		. "<meta http-equiv='expires' CONTENT='Mon, 1 Jan 2014 05:00:00 GMT'>"
		. "<meta http-equiv='Pragma' CONTENT='no-cache'>"
		. "<meta http-equiv='Pragma' CONTENT='Cache-Control: max-age=0'>"
		. "<meta http-equiv='Last-Modified' CONTENT='Mon, 1 Jan 2014 05:00:00 GMT'>"
		. "<meta http-equiv='Cache-Control' CONTENT='no-cache, must-revalidate'>"
		. "<meta name='viewport' content='user-scalable=1.0,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0'>"
		. "<meta name='apple-mobile-web-app-capable' content='yes'>"
		. "<meta name='format-detection' content='telephone=no'>"
		. "<link rel='stylesheet' href='bootstrap3_2/css/bootstrap.min.css'>"
		. "<link rel='stylesheet' href='bootstrap3_2/css/bootstrap-theme.min.css'>"
		. "<link rel='stylesheet' type='text/css' href='common.css'>"
		. "<style>" . $style . "</style>"
		. "</head><body>"
                . "<div class='container'>"
                . "<div class='row'>"
                . "<div class='col-md-12' style='text-align: center;'>"
		. "<div class='wheel-wrapper'>"
		. $content
		. "</div>"
                 . "</div></div>"
                . "</div>"
                . "</body></html>";
	exit;

}

$theRestaurant = $_REQUEST['restaurant'];
if(empty($theRestaurant)) $theRestaurant = "SG_SG_R_TheFunKitchen";

$fullfeature = ($_SESSION['user']['member_type'] == 'weeloy_agent' || $_SESSION['user']['member_type'] == 'weeloy_admin' || true);
$logipad = (preg_match("/iPad/", $_SERVER['HTTP_USER_AGENT']));
if($android) $imgsize = "height='350' width='350'";
if($logipad) $imgsize = "height='550' width='550'";


$imgdata = new WY_Images($theRestaurant);
$resdata = new WY_restaurant();

$resdata->getRestaurant($theRestaurant);

$logoName = "logo.png";
if($imgdata->l_file_exists($resdata->logo))
	$logoName = $imgdata->showPicture($theRestaurant, $resdata->logo);
$logoimg = "<img style='border-radius: 2px;
    margin: 5px;' class='logo' src='" . $logoName . "' id='theWheel'>";

if(empty($resdata->wheel)) {
	showWheel($logoimg . "<h3>The wheel has not been configured by the restaurant.</h3>", "body { margin: 100px 100px 100px 100px; }");		// never return;
	}

if($imgdata->l_file_exists("wheelscript.js") == false) {
	showWheel($logoimg . "<h3>The wheel has not been configured by the restaurant</h3>", "body { margin: 100px 100px 100px 100px; }");
	exit;
	}

$pict = "stdwheel.png";
if($imgdata->l_file_exists("wheel.png"))
	$pict = $imgdata->showPicture($theRestaurant, "wheel.png") . "?dxcs=" . time();	
$imgwheel = "<img class='wheel' src='" . $pict . "'" . $imgsize . ">";
		
$info = parse_url($_SERVER['HTTP_REFERER']);
//if($info['host'] !== $_SERVER['HTTP_HOST']) exit;


if($fullfeature == false) {
     $logoimg = "<div class = 'row' style=' height: 10%;
    margin: 1% 0 0 10%;
    max-width: 100%;
    text-align: left;'><img style='max-width:100%; height:100%'class='logo' src='" . $logoName . "' id='theWheel3'/></div>";
    $imgwheel = "<div class = 'row' style='margin:0;height:80%'>"
            . "<div class = 'row' style='margin:0;width:100%' >"
            . "<img class='marker_old' style='height:20px;' src='pointer.png' border='0'>"
            . "</div>"
            . "<div class = 'row' style='margin:0;max-width:100%;height:90%;'  >"
            . "<img class='wheel2'  style='max-width:100%;max-height:100%;' src='" . $pict . "'" . $imgsize . "/>"
            . "</div>"
            . "</div>"
        . "<div class = 'row' style='margin:0;max-width:100%; height:10%'><p style='margin-top:10px;font-size:0.8em'>This Wheel is the current Wheel of the restaurant at this time, and subject to change by the restaurant by the time of your visit.</p></div>";

	showWheel($logoimg.$imgwheel, "");
	exit;
}
	
$confirmation = $resCode = $membCode = $guestname = "";
if(isset($_REQUEST['confirmation']))
	$confirmation = $_REQUEST['confirmation'];
if(isset($_REQUEST['restCode']))
	$restCode = $_REQUEST['restCode'];
if(isset($_REQUEST['membCode']))
	$membCode = $_REQUEST['membCode'];
if(isset($_REQUEST['guestname']))
	$guestname = $_REQUEST['guestname'];
	
$html = "<html><head><title>Rotation</title>"
		. "<meta name='copyrightright' content='all right reserved (c) WEELOY PTE, 2014 version 3.4'>"
		. "<meta http-equiv='expires' CONTENT='Mon, 1 Jan 2014 05:00:00 GMT'>"
		. "<meta http-equiv='Pragma' CONTENT='no-cache'>"
		. "<meta http-equiv='Pragma' CONTENT='Cache-Control: max-age=0'>"
		. "<meta http-equiv='Last-Modified' CONTENT='Mon, 1 Jan 2014 05:00:00 GMT'>"
		. "<meta http-equiv='Cache-Control' CONTENT='no-cache, must-revalidate'>"
		. "<meta name='viewport' content='user-scalable=1.0,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0'>"
		. "<meta name='apple-mobile-web-app-capable' content='yes'>"
		. "<meta name='format-detection' content='telephone=no'>"
		. "<link rel='stylesheet' href='bootstrap3_2/css/bootstrap.min.css'>"
		. "<link rel='stylesheet' href='bootstrap3_2/css/bootstrap-theme.min.css'>"
		. "<link rel='stylesheet' type='text/css' href='common.css'>"
		. "<style> body { font-size:10px; } </style>"
		. "</head><body>"
		. "<div class='start form-group'>"
        . ""
		. "<input type='text' value = '$guestname' class='form-control' id='guest-name' name='guest-name' placeholder='Type your name'></input>"
		. "<input type='text' value = '$confirmation' class='form-control' id='confirmation-code' name='confirmation-code' placeholder='Type your confirmation number'></input>"
		. "<input type='text' value = '$restCode' class='form-control' id='restaurant-code' name='restaurant-code' placeholder='Type your Restaurant Code'></input>"
		. "<input type='text' value = '$membCode' class='form-control' id='guest-code' name='guest-code' placeholder='Type your Member Code'></input>"
		. "<button type='button' class='btn btn-default' id='button-submit-name'>submit</button>"
		. "</div>"
		. "<div class='wheel-wrapper' style='visibility:hidden'>"
		. $imgwheel . $logoimg
		. "<img class='marker' src='pointer.png' border='0'>"
		. "<div class='swipe-box ' id='swipe-box'></div>"
		. "<div id='win-notification'></div>"
                . "<div id='win-description'></div>"
		. "<img class='gif star-left1' src='star_3.gif' border='0' style='visibility:hidden'/>"
		. "<img class='gif star-left2' src='star_2.gif' border='0' style='visibility:hidden'/>"
		. "<img class='gif star-right' src='star_3.gif' border='0' style='visibility:hidden'/>"
		. "<img class='gif star-right' src='star_2.gif' border='0' style='visibility:hidden'/>"
		. "<img class='gif fireworks-right' src='fireworks_3.gif' border='0' style='visibility:hidden'/>"
		. "<img class='gif fireworks-right' src='fireworks_1.gif' border='0' style='visibility:hidden'/>"
		. "<audio id='sound-track' preload='auto'><source type='audio/mp3' src='wheel_soundtrack.mp3'></source>\n"
		. "</audio></div>"
		. "<script src='a_js/jquery-latest.min.js' type='text/javascript'></script>"
		. "<script src='foo.js'></script>"
		. "<script src='a_js/ajax.js' type='text/javascript'></script>"
		;
		printf("%s", $html);


printf("<script src='%s'></script>", $imgdata->showPicture($theRestaurant, "wheelscript.js?tt=" . time()));
printf("<script language='javascript' type='text/javascript'>");
printf("$(document).ready(function() {\n");

if($logipad) {
	printf("$('.marker').css({top:40, left:280});");
	printf("$('.swipe-box').css({ background: bgWebKit }).css({ background: bgMoz }); ");
	}

if($android){
	printf("$('.marker').css({top:15, left:310});");
	printf("$('.wheel').css({top:25, left:145});");
	printf("$('.swipe-box').css({top:152, left:20});");
	printf("$('.swipe-box').width(60);"); // Units are assumed to be pixels
	printf("$('.swipe-box').height(200);");
	printf("$('#win-notification').css({top:15, left:500});");
	}
printf("}); ");
printf("</script>");

?>

</body>
</html>