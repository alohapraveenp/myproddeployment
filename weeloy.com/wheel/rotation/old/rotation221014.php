<?
// Copyright (c) all right reserved

function getLogo($dirname) {

	$pattern = "/\.gif|\.jpeg|\.jpg|\.png/"; 
	$handle = opendir($dirname);
	$logoName = "";
	while($file = readdir($handle))
		if(preg_match($pattern, $file) && substr($file, 0, 5) == "Logo.") 
			{
			$logoName = trim($file);
			break;
			}	

	closedir($handle);
	return $logoName;
}


$theRestaurant = $_REQUEST['restaurant'];
if(empty($theRestaurant)) $theRestaurant = "SG_SG_R_TheFunKitchen";

$info = parse_url($_SERVER['HTTP_REFERER']);
//if($info['host'] !== $_SERVER['HTTP_HOST']) exit;

$logipad = (preg_match("/iPad/", $_SERVER['HTTP_USER_AGENT']));

define("__SERVERNAME__", $_SERVER['SERVER_NAME']);
if(preg_match("/soleris/", __SERVERNAME__))
	define("__ROOTDIR__", "/joomla");
else define("__ROOTDIR__", "");

define("__SHOWDIR__", __ROOTDIR__ . "/weeloy_media/upload/restaurant/");
define("__UPLOADDIR__", $_SERVER['DOCUMENT_ROOT'] . __SHOWDIR__);

$logoimg = "";
if(!empty($theRestaurant))
	$logoName = getLogo(__UPLOADDIR__ . $theRestaurant . "/");
if($logoName != "" && file_exists(__UPLOADDIR__ . $theRestaurant . "/" . $logoName))
	$logoimg = "<img class='logo' src='" . __SHOWDIR__ . $theRestaurant . "/" . $logoName . "'>";
else $logoimg = "<img class='logo' src='" . __SHOWDIR__ . "weeloy/logo.png'>";
 	
$imgsize = ($logipad) ? "height='550' width='550'" : "";
if(!empty($theRestaurant) && file_exists(__UPLOADDIR__ . $theRestaurant . "/wheel.png")) 
	$imgwheel = "<img class='wheel' src='" . __SHOWDIR__ . $theRestaurant . "/wheel.png' " . $imgsize . ">";
else $imgwheel = "<img class='wheel' src='" . __SHOWDIR__ . "weeloy/stdwheel.png' " . $imgsize . ">";

if(preg_match("/localhost/", __SERVERNAME__))
	{
	 $imgwheel = "<img class='wheel' src='stdwheel.png' " . $imgsize . ">";
	 $logoimg = "<img class='logo' src='logo.png'>";
	}

$html = "<html><head><title>Rotation</title>"
		. "<meta name='copyrightright' content='all right reserved (c) WEELOY PTE, 2014'>"
		. "<meta name='viewport' content='user-scalable=1.0,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0'>"
		. "<meta name='apple-mobile-web-app-capable' content='yes'>"
		. "<meta name='format-detection' content='telephone=no'>"
		. "<link rel='stylesheet' href='bootstrap3_2/css/bootstrap.min.css'>"
		. "<link rel='stylesheet' href='bootstrap3_2/css/bootstrap-theme.min.css'>"
		. "<link rel='stylesheet' type='text/css' href='common.css'>"
		. "</head><body>"
		. "<div class='start form-group'>"
		. "<input type='text' class='form-control' id='costumer-name' name='costumer-name' placeholder='Type you name'></input>"
		. "<button type='button' class='btn btn-default' id='button-submit-name'>submit</button>"
		. "</div>"
		. "<div class='wheel-wrapper' style='visibility:hidden'>"
		. $imgwheel . $logoimg
		. "<img class='marker' src='pointer.png' border='0'>"
		. "<div class='swipe-box'></div>"
		. "<div id='win-notification'></div>"
		. "<img class='gif star-left1' src='star_3.gif' border='0' style='visibility:hidden'/>"
		. "<img class='gif star-left2' src='star_2.gif' border='0' style='visibility:hidden'/>"
		. "<img class='gif star-right' src='star_3.gif' border='0' style='visibility:hidden'/>"
		. "<img class='gif star-right' src='star_2.gif' border='0' style='visibility:hidden'/>"           
		. "<img class='gif fireworks-right' src='fireworks_3.gif' border='0' style='visibility:hidden'/>"
		. "<img class='gif fireworks-right' src='fireworks_1.gif' border='0' style='visibility:hidden'/>"
		. "<audio id='sound-track'  preload='auto'><source type='audio/mp3' src='wheel_soundtrack.mp3'></source></audio></div>"
		. "<script src='http://code.jquery.com/jquery-latest.min.js' type='text/javascript'></script>"
		. "<script src='foo.js'></script>";

		printf("%s", $html);

if(preg_match("/localhost/", __SERVERNAME__))
	printf("<script src='%s'></script>", "wheelscript.js");
else if(!empty($theRestaurant) && file_exists(__UPLOADDIR__ . $theRestaurant . "/wheelscript.js")) 
	printf("<script src='%s'></script>", __SHOWDIR__ . $theRestaurant . "/wheelscript.js");
else printf("<script src='%s'></script>", __SHOWDIR__ . "weeloy/wheelscript.js");

if($logipad)
	{
	printf("<script language='javascript' type='text/javascript'>");
	printf("$(document).ready(function() {\n");
	printf("$('.marker').css({top:40, left:280});");
	printf("$('.swipe-box').css({ background: bgWebKit }).css({ background: bgMoz }); ");
	printf("}); ");
	printf("</script>");
	}
?>

</body>
</html>