function sendResult(segment,result,description,source,codeConfirmation,codeRestaurant,codeGuest){
	$.ajax({
		 url: "../../wheelConfirmation.php",
         type: "POST",
         //Form data
        data: {'action':'submit','spinsegment':segment,'spinwin':result,'spindesc':description,'spinsource':source,'code-confirmation' : codeConfirmation,'code-restaurant': codeRestaurant,'code-guest':codeGuest},
        dataType : "text",
        async: false,
        
        success: function(text)
        {
            data = text;
        }
    });
    return data;
}


function verifyCode(codeConfirmation,codeRestaurant,codeGuest){
    console.log(codeGuest);
    $.ajax({
         url: "../../wheelConfirmation.php",
         type: "POST",
         //Form data
        data: {'action': 'verify','code-confirmation' : codeConfirmation,'code-restaurant': codeRestaurant,'code-guest':codeGuest},
        dataType : "text",
        async: false,
        
        success: function(text)
        {
            data = text;
            if(data != "ok")
	            alert(data);
        }
    });
    return data;
}

