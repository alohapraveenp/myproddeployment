$(document).ready(init);

function init(){
    var wheel = new WHEELOFFORTUNE();
    wheel.init();
}

function WHEELOFFORTUNE(){

    this.cache = {};            

    this.init = function () {
        console.log('controller init...');
        _this = this;
        this.preload();
        this.configForMobile();
        //animations and audios
        this.cache.gifs = document.getElementsByClassName('gif');
        //this.cache.audios = document.getElementsByClassName('audio');
        
        //spin parameters
        this.cache.deg = 0;
        this.cache.duration = 0;

        //dom elements
        this.cache.wheel = $('.wheel');
        this.cache.wheelMarker = $('.marker');
        this.cache.wheelSpinBtn = $('.wheel');
        this.cache.box = $('.swipe-box');

        this.initEvents();



        //setup prize events
        //this.prizeEvents();
    };

	onlyonce = "";
    this.initEvents = function() {
        console.log('events init...')
        var _this = this;

        var startX, startY;
        this.cache.box.on('mouseenter',function(e){
            startX = e.pageX;
            startY = e.pageY;
        });

        var endX,endY;
        this.cache.box.on('mouseleave',function(e){
            if(!_this.cache.box.hasClass('disabled'))
            {
                endX = e.pageX;
                endY = e.pageY;
                var distance = _this.computeDistance(startX,startY,endX,endY)
                var spinParams = _this.computeSpinParam(distance);
                if (!_this.cache.wheelSpinBtn.hasClass('disabled') && spinParams.deg > 0) {
                    _this.resetSpin();
                    _this.spin(spinParams);
                }
            }
        });

        this.cache.box.on('click',function(e){
            if(!_this.cache.box.hasClass('disabled'))
            {
                var spinParams = _this.computeSpinParam(0);
                if (!_this.cache.wheelSpinBtn.hasClass('disabled')) {
                    _this.resetSpin();
                    _this.spin(spinParams);	/* {deg:13000,duration:10000} */
                }
            }
        });
        
        this.cache.box.on('swipe',function(e){
            console.log('triggered swipe event ...')
            if(!_this.cache.box.hasClass('disabled'))
            {
                
                var distance = e.distX;
                var spinParams = _this.computeSpinParam(distance);
                if (!_this.cache.wheelSpinBtn.hasClass('disabled') && spinParams.deg > 0) {
                    _this.resetSpin();
                    _this.spin(spinParams);
                }
            }
        });

        $('.swipe-box,#spin-notification').attr('unselectable','on')
            .css({'-moz-user-select':'-moz-none',
                '-moz-user-select':'none',
                '-o-user-select':'none',
                '-khtml-user-select':'none', 
                '-webkit-user-select':'none',
                '-ms-user-select':'none',
                'user-select':'none'
            })
            .bind('selectstart', function(){ return false; });  

        $('#button-submit-name').click(function(){
            if(verifyCode($('#confirmation-code').val(),$('#restaurant-code').val(),$('#guest-code').val()) === "ok"){
                $('.start').hide();
                _this.costumerName = $('#guest-name').val();
                if( _this.costumerName == '')
                	 _this.costumerName = 'Stranger';
                $('#win-notification').html('Welcome ' + _this.costumerName + '.<br/>Spin the wheel!');
                document.getElementsByClassName('wheel-wrapper')[0].style.visibility = "visible";
                $("body").css("background-color","#F5F5F5");
             }
        });

     };


    this.computeSpinParam = function(distance)
    {
        console.log('computing params');
        console.log('distance: ' + distance);
        if(distance < MIN_DISTANCE_TH){
            $('#spin-notification').text('Come on swipe!'); 
        	return {deg : 0, duration : 0};
        }
        else{ 
             var deg = ROT_DEG4 + Math.round(Math.random() * 360);
            var duration = _DURATION_;
            return {deg : deg, duration : duration};
            //console.log('error in computing spin params');
        }
        return {deg : deg, duration : duration};

    };

    this.spin = function (spinParam) {
        console.log('spinning wheel');

        var _this = this;
        // reset wheel

        //disable spin button while in progress
        this.cache.wheelSpinBtn.addClass('disabled');
        this.cache.box.addClass('disabled');

        var deg = spinParam.deg;
        var duration =  spinParam.duration;

        //transition queuing
        //ff bug with easeOutBack
        this.cache.wheel.transition({
            rotate: '0deg'
        }, 0)
            .transition({
            rotate: deg + 'deg'
        }, duration, 'easeOutExpo');
        document.getElementById('sound-track').play();

        //move marker
        _this.cache.wheelMarker.transition({
            rotate:  MARKER_ROT_DEG + 'deg'
        }, 0, 'snap');

        //a bit before the wheel finishies
        setTimeout(function(){
            $('#spin-notification').text('oooOOO!');
        },duration - duration/4);

        //just before wheel finish
        setTimeout(function () {
            //reset marker
            _this.cache.wheelMarker.transition({
                rotate: '0deg'
            }, MARKER_DOWN_TIME, 'easeOutExpo');
        }, duration - WHEEL_ENDING_TIME);

        //start animation just before finish because of delay
        setTimeout(function () {
            _this.startAnimation();
        },duration-500);
        //wheel finish
        setTimeout(function () {

        //compute segment
        var spins = Math.ceil(deg/360);
        var segment = (NUMBER_OF_SEGMENTS - Math.round((deg/15) % NUMBER_OF_SEGMENTS))%NUMBER_OF_SEGMENTS ;
        // logs
        console.log('degree: ' + deg);
        console.log('spins: ' + spins);
        console.log('segment:' + segment);

        //reset wheel
        $('#win-notification').html('Congratulations '+ _this.costumerName +'.<br/>You won ' + wheelmsg[segment] + '!');
        
        $('#swipe-box').hide();
        $('#win-description').html(wheelmsg[segment+24]);
        console.log(wheelmsg[segment]);
        var serverRes = sendResult(segment,wheelmsg[segment],wheelmsg[segment+24],'html', $('#confirmation-code').val(),$('#restaurant-code').val(),$('#guest-code').val());
        console.log("result: " + serverRes );
        onlyonce = serverRes;
        //re-enable wheel spin
            setTimeout(function(){
                _this.stopAnimation();
            }, 10000);
        }, duration);

    };

    this.computeDistance = function(x1,y1,x2,y2){
        return Math.sqrt((x1-x2)*(x1-x2)+(y1-y2)*(y1-y2));

    };

    this.allowSpin = function () {
    	if(onlyonce != "") {
    		//alert("You already spinned the wheel and you won " + onlyonce);
    		return;
    		}
        console.log('reseting...');
        this.cache.wheelSpinBtn.removeClass('disabled');
        this.cache.box.removeClass('disabled');
        this.cache.deg = 0;
        this.cache.duration = 0;
    };
    
    this.resetSpin = function(){
        $('#win-notification').text('');
        this.cache.wheel.transition({
            rotate: '0deg'
        }, 300,'snap');
    };
    
    this.startAnimation = function(){
        console.log('start animation ...');
        for(var i = 0; i < this.cache.gifs.length; i++){
                this.cache.gifs[i].style.visibility = 'visible';
        }
        document.getElementById('sound-track').currentTime = 15;
    };
    
    this.stopAnimation = function(){
        console.log('stop animation ...');
        for(var i = 0; i < this.cache.gifs.length; i++){
                this.cache.gifs[i].style.visibility = 'hidden';
        }
        _this.allowSpin();
    };
    
    $.event.special.swipe.handleSwipe = function(start,stop){ //override horizontal swipe to make it vertical
            console.log('handling swipe...');
            if ( stop.time - start.time < $.event.special.swipe.durationThreshold &&
                Math.abs( start.coords[ 0 ] - stop.coords[ 0 ] ) < $.event.special.swipe.verticalDistanceThreshold &&
                Math.abs( start.coords[ 1 ] - stop.coords[ 1 ] ) > $.event.special.swipe.horizontalDistanceThreshold - 100 ) 
            {
                start.origin.trigger( "swipe" );
            }
    };
    
    this.configForMobile = function(){
        $( document ).on( "mobileinit", function() { //because those idiots add an auto loading message..
            $.mobile.loader.prototype.options.disabled = true;
        });
        document.ontouchmove = function(event){ //disable page move
            event.preventDefault();
        }
        if(/Android/i.test(navigator.userAgent)){ //android does not support audio format
            var audio = document.getElementById("sound-track");
            var audioParent = audio.parentNode;
            audioParent.removeChild(audio);

            var video = document.createElement('video');
            video.setAttribute('src','wheel_soundtrack.mp3');
            video.setAttribute('id','sound-track');
            video.setAttribute('autobuffer','');
            //video.setAttribute('style','display:none');
            audioParent.appendChild(video);

        }
    };

    this.preload = function(){
            this.firstkey = true;
            $('#costumer-name').keypress(function(){
                if(this.firstkey){
                    $('.start').hide();
                    document.getElementById('wheel-end').load();
                    document.getElementById('wheel-sound').load();
                    this.firstkey = false;
            }
        });
    };
};


