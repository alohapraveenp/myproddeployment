<?php

header('Content-Type: image/png');

define("__SHOWDIR__", "/weeloy_media/upload/restaurant/");
define("__UPLOADDIR__", $_SERVER['DOCUMENT_ROOT'] . __SHOWDIR__);

$data = strtoupper($_REQUEST['data']);
$saveWheel = $_REQUEST['savewheel'];
$theRestaurant = $_REQUEST['restaurant'];

if(empty($theRestaurant))
{
$photoImage = imagecreatetruecolor(703, 703);
imagesavealpha($photoImage, true);
$trans_color = imagecolorallocatealpha($photoImage, 0, 0, 0, 127);
imagefill($photoImage, 0, 0, $trans_color);
$wheel = imagecreatefrompng('stdwheel.png');
imagecopy($photoImage, $wheel, 0, 0, 0, 0, 700, 700);
imagepng($photoImage);
imagedestroy($photoImage);
exit;
}

$string = explode("|", $data);


function myrand() {
	$rr = array("10", "10", "10", "10", "10", "25", "25", "25", "25", "35", "35", "10", "10", "10", "10", "10", "25", "25", "25", "25", "35", "35", "10", "10", "10", "10", "10", "25", "25", "25", "25", "35", "35", "100");
	$k = rand(0, count($rr)-1);
	return $rr[$k];
}

function writemystring($png, $deg, $deltaX, $deltaY, $color, $font_path, $font_size, $string) {
	
	$newstr = "";
	$space = "  ";
	if(preg_match("/^[0-9]*% /", $string, $match))
		{
		$string = preg_replace("/^[0-9]*% /", "", $string);
		$newstr = $match[0] . " \n\n";
		}	
	else if(preg_match("/^Free /i", $string, $match))
		{
		$string = preg_replace("/^Free /i", "", $string);
		$newstr = $match[0] . " \n\n";
		$space = "   ";
		}	
	else if(preg_match("/^Comp /i", $string, $match))
		{
		$string = preg_replace("/^Comp /i", "", $string);
		$newstr = $match[0] . " \n\n";
		$space = "   ";
		}	
	$limit = strlen($string);
	for($i = 0; $i < $limit; $i++)
		$newstr .= $space . $string[$i] . " \n";
		
	imagettftext($png, $font_size, $deg, $deltaX, $deltaY, $color, $font_path, $newstr);
	
}

putenv('GDFONTPATH=' . realpath('.'));
$font_path = 'fonts/Sancreek-Regular';

$font_path = 'fonts/cantebriggia';
$font_path = 'fonts/WHITRABT';
$font_path = 'fonts/zai_CourierPolski1941';
$font_path = 'fonts/Masquerade';
$font_path = 'fonts/Dearest';
$font_path = 'fonts/vtcbelialsbladeshadow';
$font_path = 'fonts/VastShadow-Regular';
$font_path = 'fonts/GoblinOne';
$font_size = 8;

//if(strlen($string) > 13) $font_path = 'zai_CourierPolski1941';

$photoImage = imagecreatetruecolor(703, 703);
imagesavealpha($photoImage, true);
$trans_color = imagecolorallocatealpha($photoImage, 0, 0, 0, 127);
imagefill($photoImage, 0, 0, $trans_color);
$wheel = imagecreatefrompng('wheelnolgd.png');
imagecopy($photoImage, $wheel, 0, 0, 0, 0, 703, 703);
$black = imagecolorallocate($photoImage, 0, 0, 0);


$CoordXY = array(338, 20, 425, 28, 507, 60, 574, 110, 630, 175, 667, 256, 678, 340,
672, 425, 642, 507, 592, 575, 525, 630, 445, 667, 362, 681, 278, 672, 197, 641, 128, 592,
74, 526, 35, 444, 22, 365, 32, 275, 63, 197, 110, 130, 171, 71, 252, 40, 0, 0);

$Ray = 333;
for($i = 0; $i < 24; $i++)
	{
	$deg = ($i * 15) -3;
	$deltaX = 352 + floor((sin(deg2rad($deg))) * $Ray);
	$deltaY = 20 + floor((1 - cos(deg2rad($deg))) * $Ray);
	$deg = ($i * 15) ;
	writemystring($photoImage, -$deg, $deltaX, $deltaY, $black, $font_path, $font_size, $string[$i]);
	}
	
imagepng($photoImage);
$filename = __UPLOADDIR__ . "$theRestaurant/wheel.png";

if(file_exists($filename)) unlink($filename);
if($saveWheel == "w76849361") 
	{
	imagepng($photoImage, $filename);

	// create a small version for mobile
	$filename = __UPLOADDIR__ . "$theRestaurant/wheel350.png";
	imagecopyresized($smphotoImage, $photoImage, 0, 0, 0, 0, 350, 350, 703, 703);
	imagepng($smphotoImage, $filename);
	imagedestroy($smphotoImage);
	}
imagedestroy($photoImage);

?>