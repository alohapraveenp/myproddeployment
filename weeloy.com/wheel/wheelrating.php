<?php
require_once("conf/conf.init.inc.php");
require_once("lib/wpdo.inc.php");
require_once("lib/wglobals.inc.php");
require_once("lib/class.restaurant.inc.php");
require_once("lib/class.member.inc.php");
require_once("lib/class.images.inc.php");
require_once("lib/class.wheel.inc.php");

function clean($data) { return (!empty($data)) ? preg_replace("/\W/", "", $data) : "";  }

$saveWheel = clean($_REQUEST['savewheel']);
$theRestaurant = clean($_REQUEST['restaurant']);
$type = clean($_REQUEST['type']);
$type_name = ($type == "") ? "" : $type . "_";

if(empty($theRestaurant)) exit;

if($type == "") {
	$resdata = new WY_restaurant();
	$resdata->getRestaurant($theRestaurant);
	$wheel = $resdata->wheel;
	if(empty($wheel))
		exit;
} else {
	$wh = new WY_wheel($theRestaurant, '', $type);
	$wh->readWheel();
	$wheel = $wh->wheel;
	if(empty($wheel))
		exit;
}

$stringAr = explode("|", strtoupper($wheel));

$wheelvalue = intval(substr($stringAr[0], 6));
$imgSize = 150;

if(empty($theRestaurant))
	exit;

//printf("%s", $wheelvalue); exit;

header('Content-Type: image/png');

$photoImage = imagecreatetruecolor($imgSize, $imgSize);
imagesavealpha($photoImage, true);
$trans_color = imagecolorallocatealpha($photoImage, 0, 0, 0, 127);
imagefill($photoImage, 0, 0, $trans_color);
$wheel = imagecreatefrompng('wheelrating.png');
imagecopy($photoImage, $wheel, 0, 0, 0, 0, $imgSize, $imgSize);
$black = imagecolorallocate($photoImage, 0, 0, 0);

putenv('GDFONTPATH=' . realpath('.'));
$font_path = 'fonts/Sancreek-Regular';

//$font_path = 'fonts/cantebriggia';
//$font_path = 'fonts/WHITRABT';
$font_path = 'fonts/zai_CourierPolski1941';
//$font_path = 'fonts/Masquerade';
//$font_path = 'fonts/Dearest';
//$font_path = 'fonts/vtcbelialsbladeshadow';
//$font_path = 'fonts/VastShadow-Regular';
//$font_path = 'fonts/GoblinOne';

$font_size = 38;
$deltaX = 45;
$deltaY = 90;
if($wheelvalue > 99)
	{
	$font_size = 28;
	$deltaX = 40;
	}
imagettftext($photoImage, $font_size, 0, $deltaX, $deltaY, $black, $font_path, $wheelvalue);
imagepng($photoImage);


$image = new WY_Images($theRestaurant);
$tmp_filename = $image->tmpdir . $theRestaurant . "_"  . $type_name . "wheelvalue.png";
$filename = $type_name . "wheelvalue.png";


if($image->l_file_exists($filename)){
        $image->l_unlink($filename);}

imagepng($photoImage, $tmp_filename);
imagedestroy($photoImage);

$image->l_move_file($tmp_filename, $filename, 'image/png', 'wheel');
unlink($tmp_filename);



?>