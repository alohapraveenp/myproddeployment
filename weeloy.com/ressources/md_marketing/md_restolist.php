<?

	require_once("conf/conf.init.inc.php");
	require_once("lib/wpdo.inc.php");
	require_once("lib/class.tool.inc.php");
	require_once("lib/Browser.inc.php");
	require_once("lib/class.mail.inc.php");
	require_once("lib/class.async.inc.php");
	require_once("lib/class.spool.inc.php");
	require_once("lib/wglobals.inc.php");
	
?>

<!-- Angualr Code -->

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="author" content="weeloy. https://www.weeloy.com"/>
        <meta name="copyright" content="weeloy. https://www.weeloy.com"/>  
        <meta http-equiv='expires' content='mon, 1 jan 1998 05:00:00 gmt'/>
        <meta http-equiv='pragma' content='no-cache'/>
        <meta http-equiv='pragma' content='cache-control: max-age=0'/>
        <meta http-equiv='last-modified' content='mon, 1 jan 1998 05:00:00 gmt'/>
        <meta http-equiv='cache-control' content='no-cache, must-revalidate'/>
        <meta name='robots' content='noindex, nofollow'/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
        <meta name="HandheldFriendly" content="true" />
        <title> Restaurant </title>

        <link href="../../client/bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="../../client/bower_components/bootstrap/dist/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
        <link href="../../client/bower_components/fontawesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />

        <link href="../../modules/booking/assets/css/css/bootstrap-select.css" rel="stylesheet" />
        <link href="../../modules/booking/assets/css/css/bootstrap-social.css" rel="stylesheet" />

        <script type='text/javascript' src="../../client/bower_components/angular/angular.min.js"></script>
        <script type="text/javascript" src="../../client/bower_components/jquery/dist/jquery.min.js"></script>

        <script type="text/javascript" src="../../js/jquery-ui.js"></script>
        <script type="text/javascript" src="../../client/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="../../js/bootstrap-select.js"></script>
        <script type='text/javascript' src="../../js/ui-bootstrap-tpls-0.12.1.min.js"></script>
        <script type="text/javascript" src="../../js/ngStorage.min.js"></script>
        <script type="text/javascript" src="../../js/mylocal.js"></script>

        <script> var app = angular.module("myApp", ['ui.bootstrap', 'ngLocale', 'ngStorage']);</script> 
        
        <style>
            /*CSS Styling*/
            table {
                width: 100%;
            }

            tr td:last-child{
                width:20%;
            }

            input {
               width: 100%; 
            }
            *:focus {
                outline: none;  
            }

            .modal-header, h4 {
                background-color: #5cb85c;
                color:white !important;
                text-align: center;
                font-size: 15px;
            }
        </style>

        <body ng-app="myApp">
            <div class="wrapper" ng-controller='RestoRegisterController' ng-init="moduleName = 'restoregitser';" style='margin-top:20px;'>
                <div class="container container-table">
                <!-- Verify OTP Model -->
                    <div class="modal fade bs-modal-sm" id="otpmodal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
                        <div class="modal-dialog modal-sm" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4 class="modal-title"> Verify OTP to confirm Registration</h4>
                                </div>
                                <div class="modal-body">
                                    <form role="form">
                                        <div class="form-group">
                                            <label for="otp">OTP</label>
                                            <input ng-style="error.style" type="text" class="form-control" id="txtotp" name="txtotp" ng-model="txtotp" placeholder="{{Placeholder}}" required>
                                            <input type="hidden" class="form-control" id="restname" name="restname" ng-value="restname">
                                        </div>
                                        <button type="button" class="btn btn-success btn-block" ng-click="confirmOTP(restname, txtotp)"><span class="glyphicon glyphicon-ok"></span>  Confirm</button>
                                    </form>
                                </div>
                                <!-- <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                </div> -->
                            </div>
                        </div>
                    </div>
                    <!-- Verify OTP Model finish -->
                    <div class="row ">
                        <div class="container">
                            <div class="col-lg-offset-2 col-md-offset-2 col-sm-offset-2 col-lg-8 col-md-8 col-sm-8 col-xs-12"> 
                                <p> </br></p>
                                <table border="1px solid black">
                                    <tr>
                                        <th style='text-align: center;'>Restaurant Name
                                        </th> 
                                        <th>
                                        </th>
                                    </tr>
                                    <tr ng-repeat="record in data">
                                        <td style='padding-left:20px;'>{{ record.title }}</td>
                                        <td style='padding: 5px;'><input type='button' class="btn-sm btn-primary btn-block" ng-click="register($index, record.restaurant, record.mobile, record.title)" value="Register"/></td>
                                    </tr>
                                </table>   
                            </div>

                        </div>

                    </div>
                       
                </div>
            </div>
           
            <script>
                        app.controller('RestoRegisterController', function ($scope, $http, $filter) {
                            $('#otpmodal').modal('hide');
                            $scope.error = {};
                            function filterdata () {    
                                $scope.txtotp = '';
                                var API_URL = '../../api/md_marketing/restaurant';
                                $http.post(API_URL).then(function (response) {
                                    $scope.data = response.data.data;
                                    // $scope.data = $filter('filter')($scope.data, { md_verified: 'N' });
                                    // $scope.data = $filter('orderBy')($scope.data, 'title');
                                });
                            }
                        
                            filterdata();

                            $scope.register = function (index, restaurant, mobile, title) {
                                var API_URL = '../../api/md_marketing/register';
                                $http.post(API_URL,
                                    {
                                        'restaurant': restaurant,
                                        'mobile': mobile,
                                        'title': title
                                    }).then(function (response) {
                                        $scope.restname = response.data.data;
                                        $scope.txtotp = '';
                                        $('#otpmodal').modal('show');
                                        $scope.Placeholder = 'Enter OTP';
                                    });

                            }

                            $scope.confirmOTP = function (restaurant, otp) {
                                var API_URL = '../../api/md_marketing/confirmOTP';
                                $http.post(API_URL,
                                    {
                                        'restaurant': restaurant,
                                        'otp':otp
                                    }).then(function (response) {
                                        $scope.result = response.data.data;
                                        if ($scope.result == 1) {
                                            $('#otpmodal').modal('hide');
                                            filterdata(); 
                                        } else {
                                            $scope.txtotp = '';
                                            $scope.Placeholder = response.data.errors;
                                        }
                                    }); 
                            }

                        });
                       
            </script>

        </body>
</html>


