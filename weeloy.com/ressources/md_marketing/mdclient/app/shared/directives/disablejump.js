(function(app) {
    app.directive('disablejump', ['$rootScope', function($rootScope) {
        return {
            restrict: 'AE',
            scope: true,
            link: function(scope, element, attrs) {
                element.on('click', function(e) {
                    var id = attrs.href.substring(1);
                    // window.location.hash = id;
                    var top = $('#' + id).offset().top - 100;
                    $("html, body").animate({
                        scrollTop: top,
                    }, 200); 
                    e.preventDefault();
                    return false;
                });
            },
        };
    }]);

})(angular.module('app.shared.directives.disablejump', []));
