var RestaurantEventItem = angular.module('RestaurantEventItem', []);
RestaurantEventItem.directive('restaurantEventItem', ['$rootScope', function($rootScope) {
    return {
        restrict: 'E',
        templateUrl: 'mdclient/app/shared/partial/_restaurant_event_item.html',
        replace: true,
        scope: {
            event: '=',
            openInNewTab: '@',
            mediaServer: '=',
        },
        link: function(scope, element, attrs) {
           

            var img = document.createElement('img');

            var imageSize = 500;
            var ImageWidth = imageSize;
            // default size of restaurant image is 500px;
            // default size of wheel image is 150px;

            var windowWidth = $(window).width();
            if (windowWidth < 768) {
                ImageWidth = windowWidth;
            }
            if (767 < windowWidth < 1200) {
                ImageWidth = Math.round(windowWidth * 0.8 * 0.45);
            }
            if (1199 < windowWidth) {
                ImageWidth = Math.round(windowWidth * 0.8 * 0.25);
            }
            var wheelImageWidth = Math.round(ImageWidth * 0.8 * 0.3);
            if ($rootScope.imageSize === undefined && ImageWidth > 0) {
                config.ImageSizes.forEach(function(value, key) {
                    if (value > ImageWidth && $rootScope.imageSize === undefined) {
                        imageSize = value;
                        $rootScope.imageSize = imageSize;
                    }
                });
            } else {
                imageSize = $rootScope.imageSize;
            }

            if (typeof imageSize == 'undefined') {
                $rootScope.imageSize = imageSize = '/500/';
            }
            if (imageSize === 500 || imageSize === '/500/') {
                imageSize = '/';
            } else {
                if (!(imageSize[0] == '/' && imageSize.charAt[imageSize.length - 1] == '/')) {
                    imageSize = '/' + imageSize + '/';
                }
                imageSize = imageSize.replace(/\/\//g, '/');
                if (typeof imageSize == 'undefined') {
                    imageSize = '/360/';
                }
            }
            //patch 
            //
            //
            //
            //my_image = scope.category.images[Math.floor(Math.random()*scope.category.images.length)];
            // the image will load from random server and best size match with current screen width
            var url;

            if (typeof scope.event.picture != 'undefined') {
                if (scope.mediaServer === undefined) {
                    url = '//media.weeloy.com/upload/restaurant/' + encodeURIComponent(scope.event.restaurant.trim()) + imageSize + encodeURIComponent(scope.event.picture);
                } else {
                    url = scope.mediaServer + '/upload/restaurant/' + encodeURIComponent(scope.event.restaurant.trim()) + imageSize + encodeURIComponent(scope.event.picture);
                }
            }

            img.src = url;
            if (scope.openInNewTab == 'true' && $(window).width() > 480) {
                scope.OpenRestaurantInNewTab = '_blank';
            }

            //$(element).find('.img-transparent').css('background', 'url(' + url + ')');
            // $(element).find('.img-transparent').attr('src', url);




            img.className = img.width > img.height ? 'landscape' : 'portrait';

            img.onload = function() {
                console.log($(element).find('#event-section').attr('class'));
                // $(element).find('.img-transparent').attr('src', img.src);
                $(element).find('#event-section').css({background: 'url(' + img.src + ')', 'background-size': 'cover'});
                var className = img.width > img.height ? 'landscape' : 'portrait';
                $(element).find('#event-section').addClass(className);
                if (className === 'portrait') {

                    var ImageHeight = 0;
                    var maxHeight = 360,
                        ratio = 0;
                    ImageHeight = img.height;
                    ratio = maxHeight / ImageHeight;
                    var ImagenewHeight = ImageHeight * ratio;
                    if (ImagenewHeight > 380) {
                        ImagenewHeight = 380;
                    }
                     $(element).find('#event-section').css('height', ImagenewHeight);

                } else {
                    var ImgHeight = 375;
                   $(element).find('#event-section').css('height', ImgHeight);

                }

            };

            scope.go = function(restaurant) {
                document.location.href = "/" + restaurant.internal_path + "/booknow";
                //WindowOpen(restaurant.internal_path + '/book-now');
            };

//            function WindowOpen(url) {
//                window.open(url, '_blank', 'toolbar=no, scrollbars=yes, resizable=yes, top=30, left=30, width=800, height=900');
//            }
        },
    };
}]);
