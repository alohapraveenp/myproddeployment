(function(app) {
    app.directive('breadcrumb', ['$location', function($location) {
        return {
            restrict: 'AE',
            scope: {
                data: '=',
                restaurant: '=',
                baseUrl: '=',
                basePath: '=',
            },
            templateUrl: '../app/shared/partial/_breadcrumb.tpl.html',
            link: function(scope, element, attrs) {
                var breadcrumb = [];
                var hompage = {
                    title: 'Restaurant',
                    href: scope.baseUrl + scope.basePath,
                };

                breadcrumb.push(hompage);

                var page = getPage($location.$$path);
                var href;
                var item;

                switch (page) {
                    case 'search':
                        if (scope.data !== undefined && scope.data.city !== undefined && scope.data.city.data !== undefined) {
                            if (scope.basePath === '/') {
                                href = scope.baseUrl + '/search/' + scope.data.city.data;
                            } else {
                                href = scope.baseUrl + scope.basePath + '/search/' + scope.data.city.data;
                            }
                            item = {
                                title: scope.data.city.data,
                                href: href,
                            };
                            breadcrumb.push(item);
                        }
                        break;
                    case 'restaurant':
                        scope.$watch('restaurant', function(restaurant) {
                            if (scope.restaurant !== undefined) {
                                var restaurant_details = scope.restaurant.getRestaurantId().split('_');
                                var city;
                                switch (restaurant_details[1]) {
                                    case 'SG':
                                        city = 'singapore';
                                        break;
                                    case 'HK':
                                        city = 'hong-kong';
                                        break;
                                    case 'BK':
                                        city = 'bangkok';
                                        break;
                                    case 'PK':
                                        city = 'phuket';
                                        break;
                                    case 'KL':
                                        city = 'kuala-lumpur';
                                        break;
                                    default:
                                        city = 'singapore';
                                        break;
                                }
                                if (scope.basePath === '/') {
                                    href = scope.baseUrl + '/search/' + city;
                                } else {
                                    href = scope.baseUrl + scope.basePath + '/search/' + city;
                                }
                                item = {
                                    title: scope.restaurant.getCity(),
                                    href: href,
                                };
                                breadcrumb.push(item);

                                item = {
                                    title: scope.restaurant.getTitle(),
                                    href: '#',
                                };
                                breadcrumb.push(item);
                            }
                        });
                        break;
                    case 'dining-rewards':
                        scope.$watch('restaurant', function(restaurant) {
                            if (scope.restaurant !== undefined) {
                                var restaurant_details = scope.restaurant.getRestaurantId().split('_');
                                var city;
                                switch (restaurant_details[1]) {
                                    case 'SG':
                                        city = 'singapore';
                                        break;
                                    case 'HK':
                                        city = 'hong-kong';
                                        break;
                                    case 'BK':
                                        city = 'bangkok';
                                        break;
                                    case 'PK':
                                        city = 'phuket';
                                        break;
                                    case 'KL':
                                        city = 'kuala-lumpur';
                                        break;
                                    default:
                                        city = 'singapore';
                                        break;
                                }
                                if (scope.basePath === '/') {
                                    href = scope.baseUrl + '/search/' + city;
                                } else {
                                    href = scope.baseUrl + scope.basePath + '/search/' + city;
                                }
                                item = {
                                    title: scope.restaurant.getCity(),
                                    href: href,
                                };
                                breadcrumb.push(item);

                                item = {
                                    title: scope.restaurant.getTitle(),
                                    href: scope.baseUrl + scope.basePath + scope.restaurant.getInternalPath(),
                                };
                                breadcrumb.push(item);
                                item = {
                                    title: 'Dinning Offers',
                                    href: '#',
                                };
                                breadcrumb.push(item);
                            }
                        });
                        break;
                    case 'booknow':
                        scope.$watch('restaurant', function(restaurant) {
                            if (scope.restaurant !== undefined) {
                                var restaurant_details = scope.restaurant.getRestaurantId().split('_');
                                var city;
                                switch (restaurant_details[1]) {
                                    case 'SG':
                                        city = 'singapore';
                                        break;
                                    case 'HK':
                                        city = 'hong-kong';
                                        break;
                                    case 'BK':
                                        city = 'bangkok';
                                        break;
                                    case 'PK':
                                        city = 'phuket';
                                        break;
                                    case 'KL':
                                        city = 'kuala-lumpur';
                                        break;
                                    default:
                                        city = 'singapore';
                                        break;
                                }
                                if (scope.basePath === '/') {
                                    href = scope.baseUrl + '/search/' + city;
                                } else {
                                    href = scope.baseUrl + scope.basePath + '/search/' + city;
                                }
                                item = {
                                    title: scope.restaurant.getCity(),
                                    href: href,
                                };
                                breadcrumb.push(item);

                                item = {
                                    title: scope.restaurant.getTitle(),
                                    href: scope.baseUrl + scope.basePath + scope.restaurant.getInternalPath(),
                                };
                                breadcrumb.push(item);
                                item = {
                                    title: 'Book Now',
                                    href: '#',
                                };
                                breadcrumb.push(item);
                            }
                        });
                        break;
                }


                scope.breadcrumb = breadcrumb;
            }
        };
    }]);

    function getPage(path) {
        var search_page_pattern = /(.*)search(.*)/;
        var restaurant_page_pattern = /(.*)restaurant(.*)/;
        var restaurant_reward_pattern = /(.*)restaurant(.*)\/dining-rewards/;
        var booknow_pattern = /(.*)restaurant(.*)\/booknow/;
        if (search_page_pattern.test(path)) {
            return 'search';
        }
        if (restaurant_reward_pattern.test(path)) {
            return 'dining-rewards';
        }
        if (booknow_pattern.test(path)) {
            return 'booknow';
        }
        if (restaurant_page_pattern.test(path)) {
            return 'restaurant';
        }
    }
})(angular.module('weeloy.breadcrumb', ['templates-main']));
