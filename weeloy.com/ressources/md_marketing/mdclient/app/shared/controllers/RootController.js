(function(app) {
    app.controller('RootCtrl', controller);
    app.filter('json', function() {
        return function(obj) {
            return JSON.stringify(obj);
        };
    });
    controller.$inject = ['$rootScope', '$scope', '$http', '$location', '$window', 'loginService','dailypostService', '$analytics'];

    function controller($rootScope, $scope, $http, $location, $window, loginService,dailypostService,$analytics) {
        $scope.mobile = "";
        $scope.showForm = 'loginForm';
        $scope.showLoggedinBox = false;
        $scope.loggedin =false;
        $scope.showLoggedin = function(evt) {
            $scope.showLoggedinBox = !$scope.showLoggedinBox;
            evt.stopPropagation();
        };

        if ($location.$$search.f === 'register') {
            if ($rootScope.user.email === "") {
                $('#loginModal').modal('show');
                $scope.redirect = 'rvpost';
                return false;
            } else {

                window.location.href = "https://www.weeloy.com/";
            }
        }

        $('body').click(function(evt) {
            var parent = $(evt.target).parent();
            if (parent !== undefined && parent.attr('id') != 'user-profile-btn') {
                $scope.showLoggedinBox = false;
                $scope.$apply();
            }
        });
        $scope.Login = function(user) {
            loginService.login(user.email, user.password)
                .then(function(result) {
                    $rootScope.$broadcast('UserLogin', result);
                    $scope.loggedin = true;
                    $scope.user = result;
                    $rootScope.UserSession = $scope.user;
                    //auditlog for login
                    $rootScope.page='login';
            
                    AuditLog.logevent(108,'');

                    $('#loginModal').modal('hide');
                    var msg = 'You have successfully logged in, ' + result.getFirstName();
                    Notification.show('success', msg);
                }).catch(function(error) {
                    Notification.show('warning', error);
                });
        };
        
        /******************************************/
        $scope.country = 'Singapore';
        $scope.currentcc = 10;
        $scope.countries = [{
            'a': 'Australia',
            'b': 'au',
            'c': '+61'
        }, {
            'a': 'China',
            'b': 'cn',
            'c': '+86'
        }, {
            'a': 'Hong Kong',
            'b': 'hk',
            'c': '+852'
        }, {
            'a': 'India',
            'b': 'in',
            'c': '+91'
        }, {
            'a': 'Indonesia',
            'b': 'id',
            'c': '+62'
        }, {
            'a': 'Japan',
            'b': 'jp',
            'c': '+81'
        }, {
            'a': 'Malaysia',
            'b': 'my',
            'c': '+60'
        }, {
            'a': 'Myanmar',
            'b': 'mm',
            'c': '+95'
        }, {
            'a': 'New Zealand',
            'b': 'nz',
            'c': '+64'
        }, {
            'a': 'Philippines',
            'b': 'ph',
            'c': '+63'
        }, {
            'a': 'Singapore',
            'b': 'sg',
            'c': '+65'
        }, {
            'a': 'South Korea',
            'b': 'kr',
            'c': '+82'
        }, {
            'a': 'Thailand',
            'b': 'th',
            'c': '+66'
        }, {
            'a': 'Vietnam',
            'b': 'vn',
            'c': '+84'
        }, {
            'a': '',
            'b': '',
            'c': ''
        }, {
            'a': 'Canada',
            'b': 'ca',
            'c': '+1'
        }, {
            'a': 'France',
            'b': 'fr',
            'c': '+33'
        }, {
            'a': 'Germany',
            'b': 'de',
            'c': '+49'
        }, {
            'a': 'Italy',
            'b': 'it',
            'c': '+39'
        }, {
            'a': 'Russia',
            'b': 'ru',
            'c': '+7'
        }, {
            'a': 'Spain',
            'b': 'es',
            'c': '+34'
        }, {
            'a': 'Sweden',
            'b': 'se',
            'c': '+46'
        }, {
            'a': 'Switzerland',
            'b': 'ch',
            'c': '+41'
        }, {
            'a': 'UnitedKingdom',
            'b': 'gb',
            'c': '+44'
        }, {
            'a': 'UnitedStates',
            'b': 'us',
            'c': '+1'
        }, {
            'a': '',
            'b': '',
            'c': ''
        }, {
            'a': 'Afghanistan',
            'b': 'af',
            'c': '+93'
        }, {
            'a': 'Albania',
            'b': 'al',
            'c': '+355'
        }, {
            'a': 'Algeria',
            'b': 'dz',
            'c': '+213'
        }, {
            'a': 'Andorra',
            'b': 'ad',
            'c': '+376'
        }, {
            'a': 'Angola',
            'b': 'ao',
            'c': '+244'
        }, {
            'a': 'Antarctica',
            'b': 'aq',
            'c': '+672'
        }, {
            'a': 'Argentina',
            'b': 'ar',
            'c': '+54'
        }, {
            'a': 'Armenia',
            'b': 'am',
            'c': '+374'
        }, {
            'a': 'Aruba',
            'b': 'aw',
            'c': '+297'
        }, {
            'a': 'Austria',
            'b': 'at',
            'c': '+43'
        }, {
            'a': 'Azerbaijan',
            'b': 'az',
            'c': '+994'
        }, {
            'a': 'Bahrain',
            'b': 'bh',
            'c': '+973'
        }, {
            'a': 'Bangladesh',
            'b': 'bd',
            'c': '+880'
        }, {
            'a': 'Belarus',
            'b': 'by',
            'c': '+375'
        }, {
            'a': 'Belgium',
            'b': 'be',
            'c': '+32'
        }, {
            'a': 'Belize',
            'b': 'bz',
            'c': '+501'
        }, {
            'a': 'Benin',
            'b': 'bj',
            'c': '+229'
        }, {
            'a': 'Bhutan',
            'b': 'bt',
            'c': '+975'
        }, {
            'a': 'Bolivia',
            'b': 'bo',
            'c': '+591'
        }, {
            'a': 'BosniaandHerzegovina',
            'b': 'ba',
            'c': '+387'
        }, {
            'a': 'Botswana',
            'b': 'bw',
            'c': '+267'
        }, {
            'a': 'Brazil',
            'b': 'br',
            'c': '+55'
        }, {
            'a': 'Brunei',
            'b': 'bn',
            'c': '+673'
        }, {
            'a': 'Bulgaria',
            'b': 'bg',
            'c': '+359'
        }, {
            'a': 'BurkinaFaso',
            'b': 'bf',
            'c': '+226'
        }, {
            'a': 'Burundi',
            'b': 'bi',
            'c': '+257'
        }, {
            'a': 'Cambodia',
            'b': 'kh',
            'c': '+855'
        }, {
            'a': 'Cameroon',
            'b': 'cm',
            'c': '+237'
        }, {
            'a': 'CapeVerde',
            'b': 'cv',
            'c': '+238'
        }, {
            'a': 'CentralAfricanRepublic',
            'b': 'cf',
            'c': '+236'
        }, {
            'a': 'Chad',
            'b': 'td',
            'c': '+235'
        }, {
            'a': 'Chile',
            'b': 'cl',
            'c': '+56'
        }, {
            'a': 'ChristmasIsland',
            'b': 'cx',
            'c': '+61'
        }, {
            'a': 'CocosIslands',
            'b': 'cc',
            'c': '+61'
        }, {
            'a': 'Colombia',
            'b': 'co',
            'c': '+57'
        }, {
            'a': 'Comoros',
            'b': 'km',
            'c': '+269'
        }, {
            'a': 'CookIslands',
            'b': 'ck',
            'c': '+682'
        }, {
            'a': 'CostaRica',
            'b': 'cr',
            'c': '+506'
        }, {
            'a': 'Croatia',
            'b': 'hr',
            'c': '+385'
        }, {
            'a': 'Cuba',
            'b': 'cu',
            'c': '+53'
        }, {
            'a': 'Curacao',
            'b': 'cw',
            'c': '+599'
        }, {
            'a': 'Cyprus',
            'b': 'cy',
            'c': '+357'
        }, {
            'a': 'CzechRepublic',
            'b': 'cz',
            'c': '+420'
        }, {
            'a': 'DemocraticRepCongo',
            'b': 'cd',
            'c': '+243'
        }, {
            'a': 'Denmark',
            'b': 'dk',
            'c': '+45'
        }, {
            'a': 'Djibouti',
            'b': 'dj',
            'c': '+253'
        }, {
            'a': 'EastTimor',
            'b': 'tl',
            'c': '+670'
        }, {
            'a': 'Ecuador',
            'b': 'ec',
            'c': '+593'
        }, {
            'a': 'Egypt',
            'b': 'eg',
            'c': '+20'
        }, {
            'a': 'ElSalvador',
            'b': 'sv',
            'c': '+503'
        }, {
            'a': 'EquatorialGuinea',
            'b': 'gq',
            'c': '+240'
        }, {
            'a': 'Eritrea',
            'b': 'er',
            'c': '+291'
        }, {
            'a': 'Estonia',
            'b': 'ee',
            'c': '+372'
        }, {
            'a': 'Ethiopia',
            'b': 'et',
            'c': '+251'
        }, {
            'a': 'FalklandIslands',
            'b': 'fk',
            'c': '+500'
        }, {
            'a': 'FaroeIslands',
            'b': 'fo',
            'c': '+298'
        }, {
            'a': 'Fiji',
            'b': 'fj',
            'c': '+679'
        }, {
            'a': 'Finland',
            'b': 'fi',
            'c': '+358'
        }, {
            'a': 'FrenchPolynesia',
            'b': 'pf',
            'c': '+689'
        }, {
            'a': 'Gabon',
            'b': 'ga',
            'c': '+241'
        }, {
            'a': 'Gambia',
            'b': 'gm',
            'c': '+220'
        }, {
            'a': 'Georgia',
            'b': 'ge',
            'c': '+995'
        }, {
            'a': 'Ghana',
            'b': 'gh',
            'c': '+233'
        }, {
            'a': 'Gibraltar',
            'b': 'gi',
            'c': '+350'
        }, {
            'a': 'Greece',
            'b': 'gr',
            'c': '+30'
        }, {
            'a': 'Greenland',
            'b': 'gl',
            'c': '+299'
        }, {
            'a': 'Guatemala',
            'b': 'gt',
            'c': '+502'
        }, {
            'a': 'Guernsey',
            'b': 'gg',
            'c': '+44-1481'
        }, {
            'a': 'Guinea',
            'b': 'gn',
            'c': '+224'
        }, {
            'a': 'Guinea-Bissau',
            'b': 'gw',
            'c': '+245'
        }, {
            'a': 'Guyana',
            'b': 'gy',
            'c': '+592'
        }, {
            'a': 'Haiti',
            'b': 'ht',
            'c': '+509'
        }, {
            'a': 'Honduras',
            'b': 'hn',
            'c': '+504'
        }, {
            'a': 'Hungary',
            'b': 'hu',
            'c': '+36'
        }, {
            'a': 'Iceland',
            'b': 'is',
            'c': '+354'
        }, {
            'a': 'Iran',
            'b': 'ir',
            'c': '+98'
        }, {
            'a': 'Iraq',
            'b': 'iq',
            'c': '+964'
        }, {
            'a': 'Ireland',
            'b': 'ie',
            'c': '+353'
        }, {
            'a': 'IsleofMan',
            'b': 'im',
            'c': '+44-1624'
        }, {
            'a': 'Israel',
            'b': 'il',
            'c': '+972'
        }, {
            'a': 'IvoryCoast',
            'b': 'ci',
            'c': '+225'
        }, {
            'a': 'Jersey',
            'b': 'je',
            'c': '+44-1534'
        }, {
            'a': 'Jordan',
            'b': 'jo',
            'c': '+962'
        }, {
            'a': 'Kazakhstan',
            'b': 'kz',
            'c': '+7'
        }, {
            'a': 'Kenya',
            'b': 'ke',
            'c': '+254'
        }, {
            'a': 'Kiribati',
            'b': 'ki',
            'c': '+686'
        }, {
            'a': 'Kosovo',
            'b': 'xk',
            'c': '+383'
        }, {
            'a': 'Kuwait',
            'b': 'kw',
            'c': '+965'
        }, {
            'a': 'Kyrgyzstan',
            'b': 'kg',
            'c': '+996'
        }, {
            'a': 'Laos',
            'b': 'la',
            'c': '+856'
        }, {
            'a': 'Latvia',
            'b': 'lv',
            'c': '+371'
        }, {
            'a': 'Lebanon',
            'b': 'lb',
            'c': '+961'
        }, {
            'a': 'Lesotho',
            'b': 'ls',
            'c': '+266'
        }, {
            'a': 'Liberia',
            'b': 'lr',
            'c': '+231'
        }, {
            'a': 'Libya',
            'b': 'ly',
            'c': '+218'
        }, {
            'a': 'Liechtenstein',
            'b': 'li',
            'c': '+423'
        }, {
            'a': 'Lithuania',
            'b': 'lt',
            'c': '+370'
        }, {
            'a': 'Luxembourg',
            'b': 'lu',
            'c': '+352'
        }, {
            'a': 'Macao',
            'b': 'mo',
            'c': '+853'
        }, {
            'a': 'Macedonia',
            'b': 'mk',
            'c': '+389'
        }, {
            'a': 'Madagascar',
            'b': 'mg',
            'c': '+261'
        }, {
            'a': 'Malawi',
            'b': 'mw',
            'c': '+265'
        }, {
            'a': 'Maldives',
            'b': 'mv',
            'c': '+960'
        }, {
            'a': 'Mali',
            'b': 'ml',
            'c': '+223'
        }, {
            'a': 'Malta',
            'b': 'mt',
            'c': '+356'
        }, {
            'a': 'MarshallIslands',
            'b': 'mh',
            'c': '+692'
        }, {
            'a': 'Mauritania',
            'b': 'mr',
            'c': '+222'
        }, {
            'a': 'Mauritius',
            'b': 'mu',
            'c': '+230'
        }, {
            'a': 'Mayotte',
            'b': 'yt',
            'c': '+262'
        }, {
            'a': 'Mexico',
            'b': 'mx',
            'c': '+52'
        }, {
            'a': 'Micronesia',
            'b': 'fm',
            'c': '+691'
        }, {
            'a': 'Moldova',
            'b': 'md',
            'c': '+373'
        }, {
            'a': 'Monaco',
            'b': 'mc',
            'c': '+377'
        }, {
            'a': 'Mongolia',
            'b': 'mn',
            'c': '+976'
        }, {
            'a': 'Montenegro',
            'b': 'me',
            'c': '+382'
        }, {
            'a': 'Morocco',
            'b': 'ma',
            'c': '+212'
        }, {
            'a': 'Mozambique',
            'b': 'mz',
            'c': '+258'
        }, {
            'a': 'Namibia',
            'b': 'na',
            'c': '+264'
        }, {
            'a': 'Nauru',
            'b': 'nr',
            'c': '+674'
        }, {
            'a': 'Nepal',
            'b': 'np',
            'c': '+977'
        }, {
            'a': 'Netherlands',
            'b': 'nl',
            'c': '+31'
        }, {
            'a': 'NetherlandsAntilles',
            'b': 'an',
            'c': '+599'
        }, {
            'a': 'NewCaledonia',
            'b': 'nc',
            'c': '+687'
        }, {
            'a': 'Nicaragua',
            'b': 'ni',
            'c': '+505'
        }, {
            'a': 'Niger',
            'b': 'ne',
            'c': '+227'
        }, {
            'a': 'Nigeria',
            'b': 'ng',
            'c': '+234'
        }, {
            'a': 'Niue',
            'b': 'nu',
            'c': '+683'
        }, {
            'a': 'NorthKorea',
            'b': 'kp',
            'c': '+850'
        }, {
            'a': 'Norway',
            'b': 'no',
            'c': '+47'
        }, {
            'a': 'Oman',
            'b': 'om',
            'c': '+968'
        }, {
            'a': 'Pakistan',
            'b': 'pk',
            'c': '+92'
        }, {
            'a': 'Palau',
            'b': 'pw',
            'c': '+680'
        }, {
            'a': 'Palestine',
            'b': 'ps',
            'c': '+970'
        }, {
            'a': 'Panama',
            'b': 'pa',
            'c': '+507'
        }, {
            'a': 'PapuaNewGuinea',
            'b': 'pg',
            'c': '+675'
        }, {
            'a': 'Paraguay',
            'b': 'py',
            'c': '+595'
        }, {
            'a': 'Peru',
            'b': 'pe',
            'c': '+51'
        }, {
            'a': 'Pitcairn',
            'b': 'pn',
            'c': '+64'
        }, {
            'a': 'Poland',
            'b': 'pl',
            'c': '+48'
        }, {
            'a': 'Portugal',
            'b': 'pt',
            'c': '+351'
        }, {
            'a': 'Qatar',
            'b': 'qa',
            'c': '+974'
        }, {
            'a': 'RepublicCongo',
            'b': 'cg',
            'c': '+242'
        }, {
            'a': 'Reunion',
            'b': 're',
            'c': '+262'
        }, {
            'a': 'Romania',
            'b': 'ro',
            'c': '+40'
        }, {
            'a': 'Rwanda',
            'b': 'rw',
            'c': '+250'
        }, {
            'a': 'SaintBarthelemy',
            'b': 'bl',
            'c': '+590'
        }, {
            'a': 'SaintHelena',
            'b': 'sh',
            'c': '+290'
        }, {
            'a': 'SaintMartin',
            'b': 'mf',
            'c': '+590'
        }, {
            'a': 'Samoa',
            'b': 'ws',
            'c': '+685'
        }, {
            'a': 'SanMarino',
            'b': 'sm',
            'c': '+378'
        }, {
            'a': 'SaudiArabia',
            'b': 'sa',
            'c': '+966'
        }, {
            'a': 'Senegal',
            'b': 'sn',
            'c': '+221'
        }, {
            'a': 'Serbia',
            'b': 'rs',
            'c': '+381'
        }, {
            'a': 'Seychelles',
            'b': 'sc',
            'c': '+248'
        }, {
            'a': 'SierraLeone',
            'b': 'sl',
            'c': '+232'
        }, {
            'a': 'Slovakia',
            'b': 'sk',
            'c': '+421'
        }, {
            'a': 'Slovenia',
            'b': 'si',
            'c': '+386'
        }, {
            'a': 'SolomonIslands',
            'b': 'sb',
            'c': '+677'
        }, {
            'a': 'Somalia',
            'b': 'so',
            'c': '+252'
        }, {
            'a': 'SouthAfrica',
            'b': 'za',
            'c': '+27'
        }, {
            'a': 'SouthSudan',
            'b': 'ss',
            'c': '+211'
        }, {
            'a': 'SriLanka',
            'b': 'lk',
            'c': '+94'
        }, {
            'a': 'Sudan',
            'b': 'sd',
            'c': '+249'
        }, {
            'a': 'Suriname',
            'b': 'sr',
            'c': '+597'
        }, {
            'a': 'Swaziland',
            'b': 'sz',
            'c': '+268'
        }, {
            'a': 'Syria',
            'b': 'sy',
            'c': '+963'
        }, {
            'a': 'Taiwan',
            'b': 'tw',
            'c': '+886'
        }, {
            'a': 'Tajikistan',
            'b': 'tj',
            'c': '+992'
        }, {
            'a': 'Tanzania',
            'b': 'tz',
            'c': '+255'
        }, {
            'a': 'Togo',
            'b': 'tg',
            'c': '+228'
        }, {
            'a': 'Tokelau',
            'b': 'tk',
            'c': '+690'
        }, {
            'a': 'Tonga',
            'b': 'to',
            'c': '+676'
        }, {
            'a': 'Tunisia',
            'b': 'tn',
            'c': '+216'
        }, {
            'a': 'Turkey',
            'b': 'tr',
            'c': '+90'
        }, {
            'a': 'Turkmenistan',
            'b': 'tm',
            'c': '+993'
        }, {
            'a': 'Tuvalu',
            'b': 'tv',
            'c': '+688'
        }, {
            'a': 'Uganda',
            'b': 'ug',
            'c': '+256'
        }, {
            'a': 'Ukraine',
            'b': 'ua',
            'c': '+380'
        }, {
            'a': 'UnitedArabEmirates',
            'b': 'ae',
            'c': '+971'
        }, {
            'a': 'Uruguay',
            'b': 'uy',
            'c': '+598'
        }, {
            'a': 'Uzbekistan',
            'b': 'uz',
            'c': '+998'
        }, {
            'a': 'Vanuatu',
            'b': 'vu',
            'c': '+678'
        }, {
            'a': 'Vatican',
            'b': 'va',
            'c': '+379'
        }, {
            'a': 'Venezuela',
            'b': 've',
            'c': '+58'
        }, {
            'a': 'WallisandFutuna',
            'b': 'wf',
            'c': '+681'
        }, {
            'a': 'WesternSahara',
            'b': 'eh',
            'c': '+212'
        }, {
            'a': 'Yemen',
            'b': 'ye',
            'c': '+967'
        }, {
            'a': 'Zambia',
            'b': 'zm',
            'c': '+260'
        }, {
            'a': 'Zimbabwe',
            'b': 'zw',
            'c': '+263'
        }];

        $scope.phoneindex = function(code) {
            val = $scope.mobile;
            if ($scope.mobile.length > 0) {
                $scope.mobile = $("#txt-mobile").val();
                val = $scope.mobile;
            }
            //

            val = val.trim();
            val = val.replace(/[^0-9 \+]/g, "");
            val = val.replace(/[ ]+/g, " ");

            if (val.indexOf(code + ' ') === 0) {
                val = val.replace(/^\+(\d{2,3})[ ]*\1[ ]*/, '+$1 '); // replace repetition of contry code
                $scope.mobile = val.trim();
                return $scope.mobile;
            }
            if (val.indexOf(code) === 0) {

                val = code + ' ' + val.substring(code.length);
                val = val.replace(/^\+(\d{2,3})[ ]*\1[ ]*/, '+$1 '); // replace repetition of contry code
                //return $scope.mobile = $("#txt-mobile").val();
                $scope.mobile = val.trim();
                return $scope.mobile;
            }
            if ((res = val.match(/^\+\d{2,3} /)) !== null) {
                val = val.replace(/^\+\d{2,3} /, "");
                val = code + ' ' + val;
                val = val.replace(/^\+(\d{2,3})[ ]*\1[ ]*/, '+$1 '); // replace repetition of contry code
                $("#txt-mobile").val(val.trim());
                $scope.mobile = $("#txt-mobile").val();
                return $scope.mobile;
                //return $scope.mobile = val.trim();
            }
            if ((res = val.match(/^\+\d{2,3}$/)) !== null) {

                val = val.replace(/^\+\d{2,3}/, "");
                val = code + ' ' + val;
                val = val.replace(/^\+(\d{2,3})[ ]*\1[ ]*/, '+$1 '); // replace repetition of contry code
                $("#txt-mobile").val(val.trim());
                $scope.mobile = $("#txt-mobile").val();
                return $scope.mobile;
            }
            if (val.match(/^\+/) !== null) {

                val = code + ' ' + val.substring(1);
                val = val.replace(/^\+(\d{2,3})[ ]*\1[ ]*/, '+$1 '); // replace repetition of contry code
                $("#txt-mobile").val(val.trim());
                $scope.mobile = val.trim();
                return $scope.mobile;
            }
            val = val.replace(/\+ /, "");
            val = code + ' ' + val;
            val = val.replace(/^\+(\d{2,3})[ ]*\1[ ]*/, '+$1 '); // replace repetition of contry code


            $scope.mobile = val.trim();
            return $scope.mobile;
        };
        //        $scope.cleantel = function (obj) {
        //            obj = obj.replace(/[^0-9 \+]/g, '');
        //            return obj;
        //        }
        $scope.listPhoneIndex = function(tt) {
            for (i = 0; i < $scope.countries.length; i++)
                if ($scope.countries[i].a == tt) {
                    $scope.currentcc = i;
                    break;
                }

            if (i >= $scope.countries.length) {
                alert('Invalid Country Code');
                $scope.currentcc = 10; // Singapore
            }
            $scope.curflgicon = "famfamfam-flag-" + $scope.countries[$scope.currentcc].b;

            $scope.phoneindex($scope.countries[$scope.currentcc].c);

        };
        $scope.checkvalidtel = function() {

            $scope.mobile = $("#txt-mobile").val();
            if ($scope.mobile === "") {
                $scope.mobile = $scope.countries[$scope.currentcc].c.trim() + ' ';
                return;
            }
            $scope.curflgicon = "famfamfam-flag-" + $scope.countries[$scope.currentcc].b;
            //$('#bkcountry').val($scope.countries[$scope.currentcc].a);
            $scope.phoneindex($scope.countries[$scope.currentcc].c);
        };
        //alert($scope.SignupForm);
        $scope.listPhoneIndex('Singapore');


        $scope.logout = function() {
            if ($rootScope.user === undefined || $rootScope.user === null) {
                return;
            }
            var page = ($rootScope.page!=='') ? $rootScope.page : 'home';
                    $rootScope.page='logout';
                
                    AuditLog.logevent(109,'');
         
            loginService.logout($rootScope.user.email)
                .then(function(response) {
                    if (response.status == 1) {
                        $rootScope.user = null;
                        $rootScope.loggedin = false;
                        document.location.href = BASE_URL;
                        Notification.show('success', 'You have successfully logged out');
                    }
                });
        };
        $('#loginModal').on('shown.bs.modal', function() {
            $rootScope.LoadFacebookSDK();
        });
        $rootScope.LoadFacebookSDK = function() {
            (function(d, s, id) {
                var js, fjs = d.getElementsByTagName(s)[0];
                if (d.getElementById(id))
                    return;
                js = d.createElement(s);
                js.id = id;
                js.src = "//connect.facebook.net/en_US/sdk.js";
                fjs.parentNode.insertBefore(js, fjs);
            }(document, 'script', 'facebook-jssdk'));
            window.fbAsyncInit = function() {
                FB.init({
                    appId: FB_WEBSITE_ID,
                    cookie: true, // enable cookies to allow the server to access 
                    // the session
                    xfbml: true, // parse social plugins on this page
                    version: 'v2.2' // use version 2.2
                });
            };
        };
        $scope.LoginWithFacebook = function() {
            FB.login(function(response) {
                if (response.authResponse) {
                    FB.api('/me?fields=email,first_name,last_name', function(fb_response) {
                        var params = {
                            email: fb_response.email,
                            facebookid: fb_response.id,
                            facebooktoken: FB.getAuthResponse().accessToken,
                            platform: 'web',
                        };
                        var facebookLogin = loginService.loginfacebook(params);
                        facebookLogin.then(function(result) {
                         
                           // if (result.status == 1) {
                                $rootScope.$broadcast('UserLogin', result);
                                $rootScope.page='loginfb';

                                AuditLog.logevent(108,'');
                                $rootScope.loggedin = true;
                                $scope.user = result;
                                $rootScope.UserSession = $scope.user;
                                 
                                
                    $('#loginModal').modal('hide');
                    var msg = 'You have successfully logged in, ' + result.getFirstName();
                    Notification.show('success', msg);
  
                                
//                            } else {
//                                if (response.status === 0) {
//                                    $scope.showForm = 'signupForm';
//                                    $scope.SignupUser = fb_response;
//                                }
//                            }
                        }).catch(function(result){
                                    $scope.showForm = 'signupForm';
                                    $scope.SignupUser = fb_response;
                        });
                    });
                } else {
                    console.log('User cancelled login or did not fully authorize.');
                }
            }, {
                scope: 'email'
            });
        };
        $scope.ChangePassword = function(email, old_password, new_password) {
            var changePassword = loginService.change(email, old_password, new_password, $rootScope.user.data.token);
            changePassword.then(function(response) {});
        };
        $scope.SubmitNewletter = function(email) {
            var msg = "";
            if ($(".newsletter_email").val() === '' || typeof $(".newsletter_email").val() === 'undefined') {
                msg = 'All fields are required!';
                Notification.show('success', msg);
                return false;
            }

            $http.get('api/newsletter/addEmail/' + email).success(function(response) {
                if (response.status == 1) {
                    $(".newsletter_email").val('');
                    $scope.SubscriberSuccessMessage = response.data.message;
                    $analytics.eventTrack('newsletter', {
                        category: 'users_action',
                        label: 'user_newsletter_web',
                        value: '1'
                    });
                    msg = response.data.message + email;
                    Notification.show('success', msg);
                } else {
                    msg = response.errors;
                    Notification.show('warning', msg);
                    //alert(response.errors);
                }
            });
        };
        $scope.close_newmsg = function() {
            setTimeout(function() {
                $("#newslt-alert").fadeOut('slow');

            }, 1000); // <-- time in milliseconds  
        };
        $scope.Signup = function(user, mobile) {

            if (typeof mobile != 'undefined') {
                user.mobile = mobile;
            }
            loginService.register(user).then(function(response) {

                    $rootScope.page='register';
                    AuditLog.logevent(108,'');
                if (response.status == 1) {
                    $analytics.eventTrack('register', {
                        category: 'users_action',
                        label: 'user_registration_web',
                        value: '1'
                    });
                    $scope.Login(user);
                } else {
                    alert(response.errors);
                }
            });
        };
        $scope.ForgotPassword = function(email) {
            loginService.forgot(email, 'weeloy.com').then(function(response) {
                if (response.status == 1) {
                    Notification.show('success', 'Reset password link has been sent to ' + email);
                    $('#loginModal').modal('hide');
                } else {
                    Notification.show('warning', response.errors);
                }
            });
        };
    }
})(angular.module('RootController', ['LoginService','DailypostService']));
