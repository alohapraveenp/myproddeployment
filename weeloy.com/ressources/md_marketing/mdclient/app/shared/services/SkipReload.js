var SkipReload = angular.module('SkipReload', []);
SkipReload.factory('location', ['$rootScope', '$route', '$location',
    function($rootScope, $route, $location) {
        $location.skipReload = function() {
            var lastRoute = $route.current;

            var deregister = $rootScope.$on('$locationChangeSuccess',
                function(e, absUrl, oldUrl) {
                    $route.current = lastRoute;
                    deregister();
                });
            return $location;
        };
        return $location;
    }
]);
