(function(app) {
    app.factory('API', API);
    API.$inject = [
        'RestaurantService',
        
    ]; 

    function API(
        RestaurantService
    ) {
        var service = {
            restaurant: RestaurantService
            
        };
        return service;
    }
})(angular.module('app.api', [
    'app.api.restaurant'
    
]));
