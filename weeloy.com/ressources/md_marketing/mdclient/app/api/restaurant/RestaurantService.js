(function(app) {
    app.service('RestaurantService', RestaurantService);
    RestaurantService.$inject = ['$http', '$q'];

    function RestaurantService($http, $q) {
        this.getFullInfo = function(query) {
            var API_URL = 'api/restaurantfullinfo/' + query;
            var defferred = $q.defer();
            $http.get(API_URL, {
                cache: true,
            }).success(function(response) {
                if (response.status === 1) {
                    var restaurant = new Restaurant(response.data.restaurantinfo);
                    restaurant.setMenu(response.data.menu);
                    restaurant.setChef(response.data.chef);
                    defferred.resolve(restaurant);
                } else {
                    defferred.reject('error');
                }
            });
            return defferred.promise;
        };

        this.getReviews = function(query, page) {
            if (page === undefined) {
                page = 0;
            }
            var API_URL = 'api/getreviews/' + query + '/list/' + page;
            var defferred = $q.defer();

            $http.get(API_URL, {
                cache: true
            }).success(function(response) {
                defferred.resolve(response);
            });
            return defferred.promise;
        };
        this.getRestaurantEvent = function(query) {
            var API_URL = 'api/restaurant/events/active/' + query;
            var defferred = $q.defer();
            $http.get(API_URL).success(function(response) {
                defferred.resolve(response);
            });
            return defferred.promise;
        };
        this.getRestaurantPublicEvent = function(query) {
            var API_URL = 'api/restaurant/events/active/public/' + query;
            var defferred = $q.defer();
            $http.get(API_URL).success(function(response) {
                defferred.resolve(response);
            });
            return defferred.promise;
        };
        this.getCateringPictures = function(query, limit) {
            var API_URL = 'api/restaurant/catering/gallery/' + query + '/' + limit;
            var defferred = $q.defer();
            $http.get(API_URL, {
                cache: true
            }).success(function(response) {
                defferred.resolve(response);
            });
            return defferred.promise;
        };
        this.getBestOffers = function(query, limit) {
            var API_URL = 'api/restaurant/offers/' + query + '/' + limit;
            var defferred = $q.defer();
            $http.get(API_URL, {
                cache: true
            }).success(function(response) {
                defferred.resolve(response);
            });
            return defferred.promise;
        };
        this.getRestaurantService = function(restaurant) {
            var API_URL = 'api/restaurant/service/restaurantservices/' + restaurant + '/active_only';
            var defferred = $q.defer();
            $http.get(API_URL, {
                cache: true,
            }).success(function(response) {
                if (response.status === 1) {
                    var services = [];
                    response.data.restaurantservices.forEach(function(value, key) {
                        services.push(new Service(value));
                    });
                    defferred.resolve(services);
                } else {
                    defferred.reject('error');
                }
            });
            return defferred.promise;
        };
        this.availabilities = function(restaurant) {
            var API_URL = 'api/restaurant/' + restaurant + '/availabilities';
            var defferred = $q.defer();
            $http.get(API_URL, {
                cache: false,
            }).success(function(response) {
                if (response.status === 1) {
                    defferred.resolve(response.data);
                } else {
                    defferred.reject('error');
                }
            });
            return defferred.promise;
        };
    }
})(angular.module('app.api.restaurant', []));
