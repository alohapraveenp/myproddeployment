MdWeeloyApp.config(['$routeProvider', '$locationProvider', function($routeProvider, $locationProvider) {
        
    $routeProvider
        .when('/', {
            templateUrl: '../app/components/home/_home.tpl.html',
            controller: 'HomeCtrl',
        })
        .when('/index.html', {
            templateUrl: '../app/components/home/_home.tpl.html',
            controller: 'HomeCtrl',
        })
        .when('/index.php', {
            templateUrl: '../app/components/home/_home.tpl.html',
            controller: 'HomeCtrl',
        })
        .when('/weeloy_magazine', {
            templateUrl: '../app/components/magazine/_magazine.tpl.html',
            controller: 'MagazineCtrl',
        })
        .when('/dailyspecial', {
            templateUrl: '../app/components/dailyboard/_dailyspecial.tpl.html',
            controller: 'DailySpecialCtrl',
        })
        .when('/search', {
            templateUrl: '../app/components/search_page/_search_page.tpl.html',
            controller: 'SearchCtrl',
        })
        .when('/search/:city', {
            templateUrl: '../app/components/search_page/_search_page.tpl.html',
            controller: 'SearchCtrl',
        })
        .when('/directory', {
            templateUrl: '../app/components/directory/_directory.tpl.html',
            controller: 'DirectoryCtrl',
        })

        .when('/404', {
            templateUrl: '../app/components/404_page/_404.tpl.html',
            controller: 'F404Ctrl',
        })
        
        .otherwise({
         redirectTo:"404"

        });
 
 
    $locationProvider.html5Mode({
        enabled: true,
        requireBase: false
    });
}]);