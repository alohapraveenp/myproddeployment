angular.module('templates-main', ['../app/components/dailyboard/_dailyspecial.tpl.html', '../app/components/directory/_directory.tpl.html', '../app/components/home/_home.tpl.html', '../app/components/magazine/_magazine.tpl.html', '../app/components/modals/daily_specialboard_registration.tpl.html', '../app/components/modals/post_dailyspecials.tpl.html', '../app/components/search_page/_search_page.tpl.html', '../app/shared/partial/_breadcrumb.tpl.html', '../app/shared/partial/_dailyspecial_post.tpl.html', '../app/shared/partial/_footer.tpl.html', '../app/shared/partial/_footer_1.tpl.html', '../app/shared/partial/_login_form.tpl.html', '../app/shared/partial/_menu.tpl.html', '../app/shared/partial/_menu_user.tpl.html', '../app/shared/partial/_restaurant_item.tpl.html', '../app/shared/partial/_signup_form.tpl.html']);

angular.module("../app/components/dailyboard/_dailyspecial.tpl.html", []).run(["$templateCache", function ($templateCache) {
  $templateCache.put("../app/components/dailyboard/_dailyspecial.tpl.html",
    "<style>\n" +
    "\n" +
    ".dailyspecialcontainer {\n" +
    "    float: left;\n" +
    "    height: 340px;\n" +
    "    width: 272px;\n" +
    "    margin-bottom: 15px;\n" +
    "    margin-left: 15px;\n" +
    "\n" +
    "}\n" +
    "\n" +
    ".dailyspecialimage {\n" +
    "    width: 270px;\n" +
    "    height: 160px;\n" +
    "    background-color: #f2f2f2;\n" +
    "    margin-bottom: 5px;\n" +
    "}\n" +
    "\n" +
    ".dailyrestaurantcontainer\n" +
    "{\n" +
    "  font-weight: bold;\n" +
    "  font-size: 14px;\n" +
    "}\n" +
    "\n" +
    ".dailytitlecontainer \n" +
    "{\n" +
    "    font-weight: bold;\n" +
    "    font-size: 16px;\n" +
    "}\n" +
    "\n" +
    ".dailyspecialdesc {\n" +
    "    width: 270px;\n" +
    "    height: 110px;\n" +
    "    overflow-y: auto;\n" +
    "}\n" +
    "\n" +
    ".clearboth \n" +
    "{\n" +
    "  clear: both;\n" +
    "}\n" +
    "\n" +
    ".custombtn \n" +
    "{\n" +
    "  padding: 9px 100px;\n" +
    "}\n" +
    "\n" +
    ".showdata\n" +
    "{\n" +
    "  height: 650px;\n" +
    "  width: auto;\n" +
    "  overflow-y: scroll;\n" +
    "}\n" +
    "\n" +
    ".showdata::-webkit-scrollbar {\n" +
    "  display: none;\n" +
    "}\n" +
    "\n" +
    ".mainholder{\n" +
    "    height: 100%;\n" +
    "    width: 100%;\n" +
    "    overflow: hidden;\n" +
    "}\n" +
    "\n" +
    ".weeloy-text {\n" +
    "  color: #FFFFFF;\n" +
    "  background-color: #283271 !important;\n" +
    "}\n" +
    "\n" +
    ".weeloy-text:hover {\n" +
    "    color: #FFFFFF;\n" +
    "}\n" +
    "\n" +
    ".modal-dialog-dsb {\n" +
    "    width: 800px;\n" +
    "    margin: 30px auto;\n" +
    "}\n" +
    "\n" +
    ".showdata {\n" +
    "    height: 360px;\n" +
    "    width: auto;\n" +
    "    overflow-y: scroll;\n" +
    "}\n" +
    "\n" +
    ".modal-header-dsb {\n" +
    "    min-height: 16.42857143px;\n" +
    "    padding: 15px;\n" +
    "    border-bottom: none;\n" +
    "}\n" +
    "\n" +
    ".modal-open .modal {\n" +
    "    overflow-x: hidden;\n" +
    "    overflow-y: auto;\n" +
    "}\n" +
    "\n" +
    "\n" +
    "</style>\n" +
    "\n" +
    "\n" +
    "    <section class=\"header-container\" style=\"background-image: url('images/home-banner.jpg'); background-position: center;background-size:cover;\">\n" +
    "\n" +
    "\n" +
    "        <div class=\"custom-new-search-bar container\">\n" +
    "            <nf-search class=\"col-lg-12 col-md-12 col-sm-12 col-xs-12\" hide-input-on-mobile=\"true\" placeholder=\"Str.template.SearchInputPlaceHolder\" button-text=\"Str.template.SearchBtnText\" cities=\"cities\"></nf-search>\n" +
    "        </div>\n" +
    "        <div class=\"header-text text-center\">\n" +
    "                <h5 >{{headertitle}}</h5>\n" +
    "                <span  >{{UserSession.search_city}}</span>\n" +
    "        </div>\n" +
    "    </section>\n" +
    "<section class ='early_book_section' style='margin-top:10px;'>\n" +
    "    <div id=\"section_first\" class=\"container\" >\n" +
    "        <div class='row row-eq-height showdata' when-scrolled =\"more()\">\n" +
    "             <div  ng-repeat=\"item in items\" style=' padding-top:0px;'>\n" +
    "                <div class=\"col-md-3 col-sm-6 col-lg-3 panel\" >\n" +
    "                    <div class='row' style='height:50px;margin-left:3px;'>\n" +
    "                       <p >{{item.headline}}</p>\n" +
    "                    </div>\n" +
    "                     <a ng-href=\"#\" target=\"{{ OpenRestaurantInNewTab }}\"> \n" +
    "                         <img ng-src=\"mdclient/assets/images/month/transparent.png\" class=\"img-responsive img-transparent\" style='width:100%;background: url(\"https://s3-ap-southeast-2.amazonaws.com/media.weeloy.asia/upload/dsb/{{today}}/{{item.code}}/{{item.image}}\");background:cover;background-repeat: no-repeat;'>\n" +
    "                     </a>\n" +
    "                       <div class=\"item-content\" style='margin-left:3px;'>\n" +
    "                           <p class=\"hover\" style='padding-bottom:10px;'>{{item.description}}</p>\n" +
    "                       </div>\n" +
    "                     <div class=\"dailyspecialbtn\"><center><input class=\"btn weeloy-text custombtn\" type=\"button\" name=\"\" value=\"Book Now\"></center></div>\n" +
    "   \n" +
    "                </div>\n" +
    "\n" +
    "             </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "\n" +
    "\n" +
    "\n" +
    "    <!-- Modal Popup -->\n" +
    "    <div class=\"modal fade\" id=\"dbsformmodal\" tabindex=\"-1\" role=\"dialog\" aria-hidden=\"true\">\n" +
    "      <div class=\"modal-dialog-dsb\" role=\"document\">\n" +
    "        <div class=\"modal-content\">\n" +
    "           <div  ng-include src=\"'mdclient/app/components/modals/post_dailyspecials.tpl.html'\"></div>\n" +
    "          <div class=\"modal-footer\">\n" +
    "            <button type=\"button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">Close</button>\n" +
    "          </div>\n" +
    "        </div>\n" +
    "      </div>\n" +
    "    </div>\n" +
    "\n" +
    "    <!-- Modal Popup -->\n" +
    "    <div class=\"modal fade\" id=\"dbsregistrationformmodal\" tabindex=\"-1\" role=\"dialog\" aria-hidden=\"true\">\n" +
    "      <div class=\"modal-dialog-dsb\" role=\"document\">\n" +
    "        <div class=\"modal-content\">\n" +
    "           <div  ng-include src=\"'mdclient/app/components/modals/daily_specialboard_registration.tpl.html'\"></div>\n" +
    "          <div class=\"modal-footer\">\n" +
    "            <button type=\"button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">Close</button>\n" +
    "          </div>\n" +
    "        </div>\n" +
    "      </div>\n" +
    "    </div>\n" +
    "\n" +
    "</section>\n" +
    "\n" +
    "");
}]);

angular.module("../app/components/directory/_directory.tpl.html", []).run(["$templateCache", function ($templateCache) {
  $templateCache.put("../app/components/directory/_directory.tpl.html",
    "<style>\n" +
    ".showdata {\n" +
    "    height: 360px;\n" +
    "    width: auto;\n" +
    "    overflow-y: scroll;\n" +
    "}\n" +
    "\n" +
    "\n" +
    "</style>\n" +
    "\n" +
    "\n" +
    "    <section class=\"header-container\" style=\"background-image: url('images/home-banner.jpg'); background-position: center;background-size:cover;\">\n" +
    "\n" +
    "\n" +
    "        <div class=\"custom-new-search-bar container\">\n" +
    "            <nf-search class=\"col-lg-12 col-md-12 col-sm-12 col-xs-12\" hide-input-on-mobile=\"true\" placeholder=\"Str.template.SearchInputPlaceHolder\" button-text=\"Str.template.SearchBtnText\" cities=\"cities\"></nf-search>\n" +
    "        </div>\n" +
    "        <div class=\"header-text text-center\">\n" +
    "                <h5 >{{headertitle}}</h5>\n" +
    "                <span  >{{UserSession.search_city}}</span>\n" +
    "        </div>\n" +
    "        \n" +
    "        <div class='container'>\n" +
    "         <div class=\"col-md-12 col-sm-8 col-lg-12\" >\n" +
    "                <div class=\"row\">\n" +
    "                  <input type ='text' placeholder ='A-Z' ng-model=\"mysearchaz\" name=\"mysearchaz\" ng-keyup=\"searchaz(mysearchaz)\" style=\"width:160px; height:40; float:left;margin-right:10px;\" /> \n" +
    "                  <input type ='text' placeholder ='Location' ng-model=\"mysearchloc\" name=\"mysearchloc\" ng-keyup=\"searchloc(mysearchloc)\" style=\"width:160px; height:40; float:left;margin-right:10px;\" /> \n" +
    "                  <input type ='text' placeholder ='Cuisine' ng-model=\"mysearchcus\" name=\"mysearchcus\" ng-keyup=\"searchcus(mysearchcus)\" style=\"width:160px; height:40; float:left;margin-right:10px;\" /> \n" +
    "                  <input type ='text' placeholder ='Price' ng-model=\"mysearchprice\" name=\"mysearchprice\" ng-keyup=\"searchprice(mysearchprice)\" style='width:160px; height:40; float:left;margin-right:10px;' /> \n" +
    "                   <input type ='text' placeholder ='Daily Special' ng-model=\"mysearchds\" name=\"mysearchds\" ng-keyup=\"searchds(mysearchds)\" style='width:160px; height:40; float:left;margin-right:10px;' /> \n" +
    "                   <input type ='text' placeholder ='Booknow' style='width:160px; height:40; float:left;margin-right:10px;' /> \n" +
    "                </div>\n" +
    "         </div>\n" +
    "            </div>\n" +
    "    </section>\n" +
    "\n" +
    "<section class ='early_book_section' style='margin-top:10px;'>\n" +
    "        <div id=\"section_first\" class=\"container\" >        \n" +
    "            <div class='row row-eq-height showdata' when-scrolled =\"more()\">\n" +
    "                 <div  ng-repeat=\"rest in restaurant\" style=' padding-top:0px;'>\n" +
    "                    <div class=\"col-md-12 col-sm-8 col-lg-12 panel\" >\n" +
    "                        <div class='row' style='height:50px;margin-left:3px;'>\n" +
    "                            <div style=\"width:170px; height:40; float:left;margin-right:10px;overflow: auto;\">\n" +
    "                              <span> {{rest.title}} </span> \n" +
    "                            </div> \n" +
    "                            <div style=\"width:170px; height:40; float:left;margin-right:10px;overflow: auto;\">\n" +
    "                              <span> {{rest.city}} </span> \n" +
    "                            </div> \n" +
    "                            <div style=\"width:170px; height:40; float:left;margin-right:10px;overflow: auto;\">\n" +
    "                              <span> {{rest.cuisine}} </span> \n" +
    "                            </div>\n" +
    "                            <div style=\"width:170px; height:40; float:left;margin-right:10px;overflow: auto;\">\n" +
    "                              <span> {{rest.price}} </span> \n" +
    "                            </div> \n" +
    "                            <div style=\"width:170px; height:40; float:left;margin-right:10px;overflow: auto;\">\n" +
    "                              <span> {{rest.dailyspecials}} </span> \n" +
    "                            </div> \n" +
    "                            <div style=\"width:170px; height:40; float:left;margin-right:10px;overflow: auto;\">\n" +
    "                              <span ng-if=\"rest.partner === 'partner'\">\n" +
    "                                Book Now\n" +
    "                              </span>\n" +
    "                              <span ng-if=\"rest.partner !== 'partner'\">\n" +
    "                                \n" +
    "                              </span>\n" +
    "                              \n" +
    "                            </div> \n" +
    "                          \n" +
    "                            \n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "   \n" +
    "                 </div>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </section>\n" +
    "\n" +
    "\n" +
    "\n" +
    "\n" +
    "");
}]);

angular.module("../app/components/home/_home.tpl.html", []).run(["$templateCache", function ($templateCache) {
  $templateCache.put("../app/components/home/_home.tpl.html",
    "<section class=\"header-container\" style=\"background-image: url('images/home-banner.jpg'); background-position: center;background-size:cover;\">\n" +
    "   \n" +
    "    \n" +
    "    <div class=\"custom-new-search-bar container\">\n" +
    "        <nf-search class=\"col-lg-12 col-md-12 col-sm-12 col-xs-12\" hide-input-on-mobile=\"true\" placeholder=\"Str.template.SearchInputPlaceHolder\" button-text=\"Str.template.SearchBtnText\" cities=\"cities\"></nf-search>\n" +
    "    </div>\n" +
    "    <div class=\"header-text text-center\">\n" +
    "            <h5 >{{headertitle}}</h5>\n" +
    "            <span  >{{UserSession.search_city}}</span>\n" +
    "    </div>\n" +
    "        \n" +
    "      \n" +
    "<!--    <div class=\"view-all-restaurant text-center col-lg-12 col-md-12 col-sm-12 col-xs-12\">\n" +
    "        <div id='div_view_all_resto'>\n" +
    "            <a analytics-on=\"click\" analytics-event=\"c_home_view_all1\" analytics-category=\"home_view_all1\" href=\"search/singapore\"  class=\"ng-binding\">View all restaurants in Singapore ads  {{UserSession.search_city}}</a>\n" +
    "        </div>\n" +
    "    </div>-->\n" +
    "\n" +
    "        \n" +
    " \n" +
    "</section>\n" +
    "    <section class ='early_book_section'>\n" +
    "<!--    <div class=\"ctn\">-->\n" +
    "        <div id=\"section_first\" class=\"container\" >\n" +
    "            <div class=\"header\">\n" +
    "            <h3 class=\"text-center\">Book a table and win instant offers</h3>\n" +
    "        </div>\n" +
    "            <div class='row row-eq-height'>\n" +
    "<!--            <div class=\"col-lg-12 col-md-12 col-sm-12 col-xs-12\">-->\n" +
    "                 <div  ng-repeat=\"category in Categories\" style=' padding:0px;'>\n" +
    "                     <restaurant-category-item open-in-new-tab=\"true\" media-server=\"mediaServer\" category=\"category\"></restaurant-category-item>\n" +
    "                 </div>\n" +
    "<!--             </div>-->\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </section>\n" +
    "<section id=\"popular-section\">\n" +
    "    <div class=\"container\">\n" +
    "        <div class=\"header\">\n" +
    "            <h3 class=\"text-center\">Popular Cuisines - Flavours from around the world</h3>\n" +
    "        </div>\n" +
    "        <div class=\"row row-eq-height\">\n" +
    "            <div class=\"col-md-3 col-sm-3\">\n" +
    "               <a ng-href=\"/search/singapore/cuisine/buffet\"><img  src=\"mdclient/assets/images/popular/image.png\" class=\"img-responsive\" style=\"background-image: url('mdclient/assets/images/popular/img1.jpg'); background-position: center;background-size:cover;\">\n" +
    "                   <p class=\"hover text-center\">BUFFET</p></a>\n" +
    "           </div>\n" +
    "           <div class=\"col-md-3 col-sm-3\">\n" +
    "               <a ng-href=\"/search/singapore/cuisine/brunch\"><img  src=\"mdclient/assets/images/popular/image.png\" class=\"img-responsive\" style=\"background-image: url('mdclient/assets/images/popular/img2.jpg'); background-position: center;background-size:cover;\">\n" +
    "               <p class=\"hover text-center\">BRUNCH</p></a>\n" +
    "           </div>\n" +
    "           <div class=\"col-md-3 col-sm-3\">\n" +
    "               <a ng-href=\"/search/singapore/cuisine/chinese\"><img  src=\"mdclient/assets/images/popular/image.png\" class=\"img-responsive\" style=\"background-image: url('mdclient/assets/images/popular/img3.jpg'); background-position: center;background-size:cover;\">\n" +
    "               <p class=\"hover text-center\">CHINESE CUISINE</p></a>\n" +
    "           </div>\n" +
    "           <div class=\"col-md-3 col-sm-3\">\n" +
    "               <a ng-href=\"/search/singapore/cuisine/japanese\"><img src=\"mdclient/assets/images/popular/image.png\" class=\"img-responsive\" style=\"background-image: url('mdclient/assets/images/popular/img4.jpg'); background-position: center;background-size:cover;\">\n" +
    "               <p class=\"hover text-center\">JAPANESE CUISINE</p></a>\n" +
    "           </div>\n" +
    "           <div class=\"col-md-3 col-sm-3\">\n" +
    "               <a ng-href=\"/search/singapore/cuisine/italian\"><img  src=\"mdclient/assets/images/popular/image.png\" class=\"img-responsive\" style=\"background-image: url('mdclient/assets/images/popular/img5.jpg'); background-position: center;background-size:cover;\">\n" +
    "               <p class=\"hover text-center\">ITALIAN CUISINE</p></a>\n" +
    "           </div>\n" +
    "           <div class=\"col-md-3 col-sm-3\">\n" +
    "               <a ng-href=\"/search/singapore/cuisine/french\"><img  src=\"mdclient/assets/images/popular/image.png\" class=\"img-responsive\" style=\"background-image: url('mdclient/assets/images/popular/img6.jpg'); background-position: center;background-size:cover;\">\n" +
    "               <p class=\"hover text-center\">FRENCH CUISINE</p></a>\n" +
    "           </div>\n" +
    "           <div class=\"col-md-3 col-sm-3\">\n" +
    "               <a ng-href=\"/search/singapore/cuisine/western\"><img  src=\"mdclient/assets/images/popular/image.png\" class=\"img-responsive\" style=\"background-image: url('mdclient/assets/images/popular/img7.jpg'); background-position: center;background-size:cover;\">\n" +
    "               <p class=\"hover text-center\">WESTERN CUISINE</p></a>\n" +
    "           </div>\n" +
    "           <div class=\"col-md-3 col-sm-3\">\n" +
    "               <a ng-href=\"/search/singapore/cuisine/bar\"><img  src=\"mdclient/assets/images/popular/image.png\" class=\"img-responsive\" style=\"background-image: url('mdclient/assets/images/popular/img8.jpg'); background-position: center;background-size:cover;\">\n" +
    "               <p class=\"hover text-center\">BARS & LOUNGES</p></a>\n" +
    "                </li>\n" +
    "            </ul>\n" +
    "           </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</section>\n" +
    "<section id=\"amazing-section\">\n" +
    "    <div class=\"container\">\n" +
    "        <div class=\"header\">\n" +
    "            <h3 class=\"text-center\">Amazing deals to share</h3>\n" +
    "        </div>\n" +
    "        <div>\n" +
    "            <div class=\"event-amazing\">\n" +
    "                <div class='row row-eq-height'>\n" +
    "                    <div ng-repeat=\"event in events\">\n" +
    "                    <restaurant-event-item open-in-new-tab=\"true\" media-server=\"mediaServer\" event=\"event\"></restaurant-event-item>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "        <div class=\"more-restaurant text-center col-md-12\">\n" +
    "            <a ng-href=\"allevents\" id=\"more-restau\">+ <span>see more deals</span></a>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</section>\n" +
    "<section class=\"zen-month\" style=\"background-color:white;\">\n" +
    "    <div class=\"container\">\n" +
    "        <div class=\"header\">\n" +
    "            <h3 class=\"text-center\">Featured restaurants - Selection of the month</h3>\n" +
    "        </div>\n" +
    "        \n" +
    "        <div class=\"css-creen\">\n" +
    "            <div class=\"col-md-3 col-sm-3 col-xs-12 zen-column\" ng-repeat=\"restaurant in TopRestaurants track by $index\" style=\"padding-left:0px!important;\">\n" +
    "                <restaurant-item open-in-new-tab=\"true\" media-server=\"mediaServer\" restaurant=\"restaurant\" indexcount=\"$index\"></restaurant-item>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</section>\n" +
    "\n" +
    "\n" +
    "\n" +
    "");
}]);

angular.module("../app/components/magazine/_magazine.tpl.html", []).run(["$templateCache", function ($templateCache) {
  $templateCache.put("../app/components/magazine/_magazine.tpl.html",
    "<section class=\"header-container\" style=\"background-image: url('images/home-banner.jpg'); background-position: center;background-size:cover;\">\n" +
    "        <div class=\"custom-new-search-bar container\">\n" +
    "            <nf-search class=\"col-lg-12 col-md-12 col-sm-12 col-xs-12\" hide-input-on-mobile=\"true\" placeholder=\"Str.template.SearchInputPlaceHolder\" button-text=\"Str.template.SearchBtnText\" cities=\"cities\"></nf-search>\n" +
    "        </div>\n" +
    "        <div class=\"header-text text-center\">\n" +
    "                <h5 >{{headertitle}}</h5>\n" +
    "                <span  >{{UserSession.search_city}}</span>\n" +
    "        </div>\n" +
    "</section>\n" +
    "\n" +
    "\n" +
    "<section class ='early_book_section' style='margin-top:10px;'>\n" +
    "    <div class=\"ctn\">\n" +
    "        <div id=\"section_first\" class=\"my-fluid-container\" >\n" +
    "            <div class='row-fluid' >\n" +
    "            <div class=\"col-lg-12 col-md-12 col-sm-12 \">\n" +
    "                 <div  ng-repeat=\"item in items\" style=' padding-top:0px;'>\n" +
    "                <div class=\"col-md-2 col-sm-4 col-lg-2 col-xs-12 col-half-offset\" ng-click=\"magazinedetails($index,item,$event)\" >\n" +
    "                    <div class=\"card\">\n" +
    "                            <img class=\"card-img-top img-responsive img-transparent\" ng-src=\"mdclient/assets/images/month/transparent.png\" style='width:100%;background: url(https://d1zfy3x6jx1ipx.cloudfront.net/{{item.imageUrl}});background:cover;background-repeat: no-repeat;' alt=\"Card image cap\">\n" +
    "                            <div class=\"card-block\">\n" +
    "                              <h5 class=\"card-title\" ng-bind=\"item.post_title\"></h5>\n" +
    "                              <p class=\"card-text\">{{item.description}}</p>\n" +
    "                            </div>\n" +
    "                    </div>\n" +
    "                    </div>\n" +
    "                 </div>\n" +
    "                <div class=\"modal fade\" id=\"magazineModal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"magazineModal\" aria-hidden=\"true\">\n" +
    "\n" +
    "                        <div class=\"modal-dialog\">\n" +
    "                            <div class=\"modal-content\">\n" +
    "                                <div class=\"modal-header\">\n" +
    "                                \n" +
    "                                <h4 class=\"modal-title\" id=\"myModalLabel\">Modal title</h4>\n" +
    "                                </div>\n" +
    "                                <div class=\"modal-body\">\n" +
    "                                    <div class=\"row-fluid\" ng-repeat =\"con in contentArr\"> \n" +
    "                                        <div class=\"span6\">\n" +
    "                                               <img ng-src=\"{{con.image}}\" class=\"img-responsive img-transparent\">\n" +
    "                                         <!-- left goes here -->\n" +
    "                                        </div>\n" +
    "                                        <div class=\"span6\">\n" +
    "                                            <p class=\"hover\" style='padding-bottom:10px;'>{{con.description}}</p>\n" +
    "                                         <!-- right goes here -->\n" +
    "                                        </div>\n" +
    "                                    </div>\n" +
    "                                </div>\n" +
    "                                <div class=\"modal-footer\">\n" +
    "                                    <button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">Close</button>\n" +
    "                                    <button type=\"button\" class=\"btn btn-primary\">Save changes</button>\n" +
    "                            </div>\n" +
    "                        </div>\n" +
    "                      </div>\n" +
    "            </div>\n" +
    "<!--                 <div  ng-repeat=\"item in items\" style=' padding-top:0px;'>\n" +
    "                    <div class=\"col-md-2 col-sm-4 col-lg-2 col-half-offset panel\" ng-click=\"magazinedetails(item.ID)\" >\n" +
    "                        <a ng-href=\"#\" target=\"{{ OpenRestaurantInNewTab }}\">\n" +
    "                             <img ng-src=\"mdclient/assets/images/month/transparent.png\" class=\"img-responsive img-transparent\" style='width:100%;background: url(https://d1zfy3x6jx1ipx.cloudfront.net/{{item.imageUrl}});background:cover;background-repeat: no-repeat;'>\n" +
    "                             <p ng-bind=\"item.post_title\"></p>\n" +
    "                         </a>\n" +
    "                    <div class=\"item-content\" style='margin-left:3px;border:1px solid black;'>\n" +
    "                        <p class=\"hover\" style='padding-bottom:10px;'>{{item.description}}</p>\n" +
    "                    </div>\n" +
    "                    </div>\n" +
    "                   \n" +
    "                    \n" +
    "                 </div>-->\n" +
    "             </div>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "        </div>\n" +
    "    </section>\n" +
    "");
}]);

angular.module("../app/components/modals/daily_specialboard_registration.tpl.html", []).run(["$templateCache", function ($templateCache) {
  $templateCache.put("../app/components/modals/daily_specialboard_registration.tpl.html",
    "<div class=\"modal-header-dsb\">\n" +
    "  <h1 class=\"modal-title\">Daily Special Board Registration</h1>\n" +
    "</div>\n" +
    "<div class=\"modal-body\">\n" +
    "    <div class=\"container-fluid\">\n" +
    "      <form id=\"form-border\" name=\"RegistrationForm\" ng-submit=\"saveitem(RegistrationForm)\" novalidate style=\"padding:5px;\">\n" +
    "          <div class=\"col-md-12 col-xl-12\">\n" +
    "             <div class=\"row\" ng-repeat=\"y in tabletitleContent | filter: {t: '!dontshow' }\"  style=\"font-size:12px;font-familly:Roboto\">\n" +
    "                       <div class=\"input-group\" ng-if=\"y.t === 'input'\">\n" +
    "                          <span class=\"input-group-addon input11\"><i class=\"glyphicon glyphicon-{{y.d}} input14\"></i>&nbsp;{{y.b}}</span>\n" +
    "                          <input type=\"{{y.type}}\" class=\"form-control input-sm\" ng-model=\"restaurant[y.a]\" ng-change=\"cleaninput(y.a)\" name={{y.a}} ng-required=\"{{y.r}}\"/>\n" +
    "                      </div>\n" +
    "                      <div class=\"error error-msg\" style='color:red;'>\n" +
    "                          <span ng-show=\"RegistrationForm.$submitted && RegistrationForm.{{y.a}}.$error.required\">Please enter '{{y.b}}'</span>\n" +
    "                          <span ng-show=\"RegistrationForm.$submitted && RegistrationForm.{{y.a}}.$error.url\">Invalid URL!</span>\n" +
    "                          <span ng-show=\"RegistrationForm.$submitted && RegistrationForm.{{y.a}}.$error.email\">Invalid Email!</span>\n" +
    "                      </div>\n" +
    "                      <div class=\"input-group\" ng-if=\"y.t === 'dropdown'\">\n" +
    "                          <div class='input-group-btn' dropdown >\n" +
    "                              <button type='button' class='btn btn-default dropdown-toggle btn-sm input11' data-toggle='dropdown'>\n" +
    "                                  <i class=\"glyphicon glyphicon-{{y.d}} input13\"></i>&nbsp; {{y.b}}<span class='caret'></span>\n" +
    "                              </button>\n" +
    "                              <ul class='dropdown-menu scrollable-menu'style='height: auto;max-height:120px; overflow-x: hidden;' >\n" +
    "                                  <li ng-repeat=\"p in y.val\"><a href ng-click=\"restaurant[y.a]=p;y.func()\">{{ p }}</a></li>\n" +
    "                              </ul>\n" +
    "                          </div>\n" +
    "                          <input type='text' ng-model='restaurant[y.a]' class='form-control input-sm' readonly >\n" +
    "                      </div>\n" +
    "                      <div class=\"input-group\" ng-if=\"y.t === 'array'\">\n" +
    "                          <div class='input-group-btn' dropdown >\n" +
    "                              <button type='button' class='btn btn-default dropdown-toggle btn-sm input11' data-toggle='dropdown'>\n" +
    "                                  <i class=\"glyphicon glyphicon-{{y.d}} input13\"></i>&nbsp; {{y.b}}<span class='caret'></span>\n" +
    "                              </button>\n" +
    "                              <ul class='dropdown-menu scrollable-menu'style='height: auto;max-height:120px; overflow-x: hidden;' >\n" +
    "                                  <li ng-repeat=\"p in y.val\"><a href ng-click=\"restaurant[y.a]=p;y.func()\">{{ p }}</a></li>\n" +
    "                              </ul>\n" +
    "                          </div>\n" +
    "                          <input type='text' ng-model='restaurant[y.a]' class='form-control input-sm' readonly >\n" +
    "                      </div>\n" +
    "                      <div class=\"input-group\"  ng-if=\"y.t==='checkbox'\">\n" +
    "                          <input type=\"checkbox\" ng-model=\"restaurant[y.a]\" ng-checked =\"{{restaurant[y.a]}}\"> &nbsp; {{y.b}}\n" +
    "                      </div>\n" +
    "                      <div class=\"input-group\"  ng-if=\"y.t==='textarea'\">\n" +
    "                       <span class=\"input-group-addon input11\"><i class=\"glyphicon glyphicon-{{y.d}} input14\"></i>&nbsp;{{y.b}}</span>\n" +
    "                          <textarea class=\"form-control input-sm\" ng-model=\"restaurant[y.a]\" ng-change=\"cleaninput(y.a)\"></textarea>\n" +
    "                      </div> \n" +
    "                      <div class=\"input-group\" ng-if=\"y.t === 'imagebutton'\">\n" +
    "                  <span class=\"input-group-addon\"><i class=\"glyphicon glyphicon-file\"></i> &nbsp; {{y.b}}</span>\n" +
    "                  <input type=\"file\" name=\"{{y.a}}\" id=\"{{y.a}}\" class=\"form-control input-sm\" onchange=\"angular.element(this).scope().uploadImage(this)\" ng-model=\"file\" placeholder=\"Upload Files\" accept=\"image/*\">  \n" +
    "                      </div>\n" +
    "             </div>\n" +
    "                  <div class=\"g-recaptcha col-md-6\" data-sitekey=\"6LdmpA4UAAAAAETsRaMqz8cjrUoyFCNwuuvJk5DS\" data-callback=\"correctCaptcha\" style='margin-left:0px;padding-left:0px;' ng-show=\"showCaptcha\"></div>\n" +
    "                  <input type=\"text\" style=\"position: absolute !important; left: -4000px !important; top: -4000px !important; display: none !important\" value=\"\" name=\"User\"/>\n" +
    "                  <div class=\"title col-md-4\" style='padding-top:48px; float:right;'>\n" +
    "                      <button type=\"submit\" class=\"book-button-lg btn-leftBottom-green pull-right\" >Add Your Listing Now</button>\n" +
    "                  </div>\n" +
    "          \n" +
    "              </div>\n" +
    "              </form>\n" +
    "            </div>\n" +
    "\n" +
    "        </div>\n" +
    "\n" +
    "");
}]);

angular.module("../app/components/modals/post_dailyspecials.tpl.html", []).run(["$templateCache", function ($templateCache) {
  $templateCache.put("../app/components/modals/post_dailyspecials.tpl.html",
    "<div class=\"modal-header-dsb\">\n" +
    "  <h1 class=\"modal-title\">Post A Daily Special</h1>\n" +
    "  <div class = \"dailytitlesub\"><a ng-click=\"showregistration()\">First Time? Click here.</a></div>\n" +
    "</div>\n" +
    "<div class=\"modal-body\">\n" +
    "    <div class=\"container-fluid\">\n" +
    "      <form id=\"form-border\" enctype=\"multipart/form-data\" name=\"Dailyspecialform\" ng-submit=\"saveitemnew(Dailyspecialform)\" novalidate style=\"padding:5px;\">\n" +
    "        <div class=\"row\">\n" +
    "          <div class=\"col-md-5\">Email Address<input class=\"form-control\" type=\"text\" value=\"\" ng-model=\"login.email\"></div>\n" +
    "          <div class=\"col-md-2\"></div>\n" +
    "          <div class=\"col-md-5\">Password <input class=\"form-control\" type=\"Password\" value=\"\" ng-model=\"login.password\"></div>\n" +
    "        </div>\n" +
    "        <br>\n" +
    "        <div class=\"row\">\n" +
    "          <div class=\"col-md-12\"><input class=\"form-control\" type=\"text\" placeholder=\"Headline\" ng-model=\"restaurantdsb.headline\"></div>\n" +
    "        </div>\n" +
    "        <br>\n" +
    "        <div class=\"row\">\n" +
    "          <div class=\"col-md-12\"><textarea class=\"form-control\" placeholder=\"Description\" rows=\"5\" ng-model=\"restaurantdsb.description\"></textarea></div>\n" +
    "        </div>\n" +
    "        <br>\n" +
    "        <div class=\"row\">\n" +
    "          <div class=\"form-group\">\n" +
    "            <div class=\"col-md-12\">\n" +
    "              <input type=\"file\" name=\"img\" id=\"img\" class=\"form-control input-sm\" onchange=\"angular.element(this).scope().uploadImagedsb(this)\" ng-model=\"file\" placeholder=\"Upload Files\">\n" +
    "            </div>\n" +
    "          </div>\n" +
    "        </div>\n" +
    "        <br>\n" +
    "        <div class=\"row\">\n" +
    "          <div class=\"col-md-12\"><label class=\"col-2 col-form-label\">Select up to 3 categories</label></div>\n" +
    "        </div>\n" +
    "        <br>\n" +
    "        <div class=\"row\">\n" +
    "          <div class=\"col-md-4\">\n" +
    "            <center>\n" +
    "              <select ng-model=\"restaurantdsb.selectedOption['cat1']\" \n" +
    "                      ng-options=\"cuisines.cuisine as cuisines.cuisine for cuisines in cuisinelist\">\n" +
    "                  <option value=\"\">Select Categories</option>\n" +
    "              </select>\n" +
    "            </center>  \n" +
    "          </div>\n" +
    "          <div class=\"col-md-4\">\n" +
    "            <center>\n" +
    "              <select ng-model=\"restaurantdsb.selectedOption['cat2']\" \n" +
    "                      ng-options=\"cuisines.cuisine as cuisines.cuisine for cuisines in cuisinelist\">\n" +
    "                  <option value=\"\">Select Categories</option>\n" +
    "              </select>\n" +
    "            </center>\n" +
    "          </div>\n" +
    "          <div class=\"col-md-4\">\n" +
    "            <center>\n" +
    "              <select ng-model=\"restaurantdsb.selectedOption['cat3']\" \n" +
    "                      ng-options=\"cuisines.cuisine as cuisines.cuisine for cuisines in cuisinelist\">\n" +
    "                  <option value=\"\">Select Categories</option>\n" +
    "              </select>\n" +
    "            </center> \n" +
    "          </div>\n" +
    "        </div>\n" +
    "        <br>\n" +
    "        <br>\n" +
    "        <div class=\"row\">\n" +
    "          <div class=\"col-md-12\">\n" +
    "             <center><button type=\"submit\" class=\"btn btn-success custombtn\">Add you post now</button></center>\n" +
    "          </div>\n" +
    "        </div>\n" +
    "\n" +
    "      </form>\n" +
    "      \n" +
    "    </div>\n" +
    "</div>");
}]);

angular.module("../app/components/search_page/_search_page.tpl.html", []).run(["$templateCache", function ($templateCache) {
  $templateCache.put("../app/components/search_page/_search_page.tpl.html",
    "<style type=\"text/css\">\n" +
    "  .restaurantcard\n" +
    "  {\n" +
    "    height: 250px;\n" +
    "    width: 225px;\n" +
    "    float: left;\n" +
    "    margin-left: 10px;\n" +
    "    margin-right: 10px;\n" +
    "    margin-top: 10px;\n" +
    "    margin-bottom: 15px;\n" +
    "    font-weight: bold;\n" +
    "  }\n" +
    "  .crestocontainer\n" +
    "  {\n" +
    "      max-width: 990px;\n" +
    "      overflow-y: hidden;\n" +
    "      margin-left: auto;\n" +
    "      margin-right: auto;\n" +
    "  }\n" +
    "  .customsearch{\n" +
    "      margin-top: 55px !important;\n" +
    "      height: 85px !important;\n" +
    "  }\n" +
    "  .customsearchtitlec{\n" +
    "      text-transform: uppercase;\n" +
    "      font-family: proximanova-semibold;\n" +
    "      font-size: 18px;\n" +
    "      padding-bottom: 0;\n" +
    "  }\n" +
    "  .customsearchtitle{\n" +
    "      font-size: 45px !important;\n" +
    "\n" +
    "  }\n" +
    "  .searchpage{\n" +
    "      padding-top: 10px !important;\n" +
    "  }\n" +
    "  .sinputfilters{\n" +
    "      width: 145px;\n" +
    "      height: 30px;\n" +
    "      margin-right: 10px;\n" +
    "      border-radius: 5px;\n" +
    "      border-color: white;\n" +
    "      border: solid 0px;\n" +
    "  }\n" +
    "  .filtercontainer\n" +
    "  {\n" +
    "    text-align: center;\n" +
    "  }\n" +
    "  .modal-dialog-modalmap {\n" +
    "    width: 925px;\n" +
    "    height: 545px;\n" +
    "    margin: 20px auto;\n" +
    "    position: relative;\n" +
    "    background-color: #fff;\n" +
    "    -webkit-background-clip: padding-box;\n" +
    "    background-clip: padding-box;\n" +
    "    border: 1px solid rgba(253, 253, 253, 0.2);\n" +
    "    outline: 0;\n" +
    "    border-radius: 30px;\n" +
    "}\n" +
    "\n" +
    "#map_canvas{\n" +
    "        position: inherit !important;\n" +
    "    }\n" +
    "</style>\n" +
    "<section class=\"header-container\" style=\"background-image: url('images/home-banner.jpg'); background-position: center;background-size:cover;\">\n" +
    "   \n" +
    "    \n" +
    "    <div class=\"custom-new-search-bar container searchpage\">\n" +
    "        <nf-search class=\"col-lg-12 col-md-12 col-sm-12 col-xs-12\" hide-input-on-mobile=\"true\" placeholder=\"Str.template.SearchInputPlaceHolder\" button-text=\"Str.template.SearchBtnText\" cities=\"cities\"></nf-search>\n" +
    "    </div>\n" +
    "    <div class=\"header-text text-center customsearch\">\n" +
    "            <div class=\"customsearchtitlec\">{{headertitle}}</div>\n" +
    "            <!-- <span >{{UserSession.search_city}}</span> -->\n" +
    "            <span class=\"customsearchtitle\">Singapore</span>\n" +
    "    </div>\n" +
    "     <div class='filtercontainer'>\n" +
    "            <input type ='text' placeholder ='Location' ng-model=\"search.region\" class=\"sinputfilters\"/> \n" +
    "            <input type ='text' placeholder ='Price' ng-model=\"search.pricing\" class=\"sinputfilters\" /> \n" +
    "            <input type ='text' placeholder ='Daily Specials Board'  class=\"sinputfilters\" /> \n" +
    "     </div>\n" +
    "        \n" +
    "    \n" +
    "</section>\n" +
    "<section class=\"zen-month\" style=\"background-color:white;\">\n" +
    "    <div class=\"crestocontainer\">\n" +
    "        \n" +
    "        <div class=\"css-creen\">\n" +
    "            <div class=\"restaurantcard\" ng-repeat=\"restaurant in restaurantsInMap | filter:search:strict\">\n" +
    "                <restaurant-item open-in-new-tab=\"true\" media-server=\"mediaServer\" restaurant=\"restaurant\" indexcount=\"$index\"></restaurant-item>\n" +
    "                <!-- <div id=\"map_canvas\"></div> -->\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</section>\n" +
    "\n" +
    "<div class=\"modal fade\" id=\"modalmap\" tabindex=\"-1\" role=\"dialog\" aria-hidden=\"true\">\n" +
    "  <div class=\"modal-dialog-modalmap\" role=\"document\">\n" +
    "    <div class=\"modalcontentmap\">\n" +
    "        <div id=\"map_canvas\"></div>\n" +
    "    </div>\n" +
    "  </div>\n" +
    "</div>\n" +
    "<!--<section>\n" +
    "                <div class=\"search-result-item \" ng-class=\"RestaurantItemClasses\"\n" +
    "                         ng-repeat=\"restaurant in restaurantsInMap\"\n" +
    "                         ng-if=\"$index >= skip && $index < skip + restaurantsPerPage\">\n" +
    "                        <restaurant-item restaurant=\"restaurant\" media-server=\"mediaServer\"\n" +
    "                                         open-in-new-tab=\"true\"></restaurant-item>\n" +
    "                    </div>\n" +
    "</section>-->\n" +
    "\n" +
    "\n" +
    "\n" +
    "\n" +
    "\n" +
    "");
}]);

angular.module("../app/shared/partial/_breadcrumb.tpl.html", []).run(["$templateCache", function ($templateCache) {
  $templateCache.put("../app/shared/partial/_breadcrumb.tpl.html",
    "<ul>\n" +
    "    <li ng-repeat=\"item in breadcrumb\">\n" +
    "        <a ng-href=\"{{item.href}}\">\n" +
    "            <span ng-bind=\"item.title\"></span><span class=\"fa fa-angle-right\" ng-show=\"!$last\"></span>\n" +
    "        </a>\n" +
    "    </li>\n" +
    "</ul>\n" +
    "");
}]);

angular.module("../app/shared/partial/_dailyspecial_post.tpl.html", []).run(["$templateCache", function ($templateCache) {
  $templateCache.put("../app/shared/partial/_dailyspecial_post.tpl.html",
    "<style>\n" +
    ".main-form {\n" +
    "    max-width: 550px;\n" +
    "    margin-top: 5px; \n" +
    "}\n" +
    "textarea {\n" +
    "resize: none;\n" +
    "}\n" +
    "\n" +
    ".custombtn {\n" +
    "    padding: 15px 80px;\n" +
    "}\n" +
    ".modal-body {\n" +
    "    position: relative;\n" +
    "    overflow-y: auto;\n" +
    "    \n" +
    "}\n" +
    "</style>\n" +
    "<div>\n" +
    "    <div class=\"container\" >\n" +
    "    <div class = \"dailytitle\"> <h1>Post A Daily Special</h1> </div>\n" +
    "    <div class = \"dailytitlesub\"><a>First Time? Click here.</a></div>\n" +
    "    <form id=\"form-border\" name=\"Dailyspecialform\"  novalidate style=\"padding:5px;\">\n" +
    "    <br>\n" +
    "    <br>\n" +
    "    <div class=\"row\" style=\"margin: 0 0 20px 0;font-size:12px;font-familly:Roboto\">\n" +
    "        <div class=\"container insidec\">\n" +
    "    		<div class=\"row\">\n" +
    "                <div class=\"col-md-2 col-xl-3\">\n" +
    "                    <div class=\"form-group row\">\n" +
    "                      <label class=\"col-2 col-form-label\">Email Address</label>\n" +
    "                      <input class=\"form-control\" type=\"text\" value=\"\" ng-model=\"login.email\">\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "                <div class=\"col-md-2 col-xl-2\">\n" +
    "                    \n" +
    "                </div>\n" +
    "             \n" +
    "                <div class=\"col-md-2 col-xl-3\">\n" +
    "                    <div class=\"form-group row\">\n" +
    "                      <label class=\"col-2 col-form-label\">Password</label>\n" +
    "                      <input class=\"form-control\" type=\"Password\" value=\"\" ng-model=\"login.password\">\n" +
    "                    </div>\n" +
    "                    \n" +
    "                </div>\n" +
    "                    <div class=\"col-md-1 col-xl-1\">\n" +
    "                    \n" +
    "                </div>\n" +
    "             </div>\n" +
    "\n" +
    "             <br>\n" +
    "             <div class=\"row\">\n" +
    "                <div class=\"col-md-7 col-xl-7\">\n" +
    "                    <div class=\"form-group row\">\n" +
    "                        <input class=\"form-control\" type=\"text\" placeholder=\"Headline\" ng-model=\"restaurant.headline\">\n" +
    "                    </div>\n" +
    "                    <div class=\"error error-msg\" style='color:red; display: none;'>\n" +
    "<!--                        <span ng-show=\"RegistrationForm.$submitted && RegistrationForm.{{y.a}}.$error.email\">Invalid Headline!</span>-->\n" +
    "                    </div>      \n" +
    "                </div>\n" +
    "             </div>\n" +
    "\n" +
    "             <div class=\"row\">\n" +
    "                <div class=\"col-md-7 col-xl-7\">\n" +
    "                    <div class=\"form-group row\">\n" +
    "                        <textarea class=\"form-control\" placeholder=\"Description\" rows=\"5\" ng-model=\"restaurant.description\"></textarea>\n" +
    "                    </div>\n" +
    "                    <div class=\"error error-msg\" style='color:red; display: none;'>\n" +
    "<!--                        <span ng-show=\"RegistrationForm.$submitted && RegistrationForm.{{y.a}}.$error.email\">Invalid Description!</span>-->\n" +
    "                    </div>      \n" +
    "                </div>\n" +
    "             </div>\n" +
    "\n" +
    "             <div class=\"row\">\n" +
    "                <div class=\"col-md-7 col-xl-7\">\n" +
    "                    <div class=\"form-group row\">\n" +
    "                        <input type=\"file\" name=\"img\" id=\"img\" class=\"form-control input-sm\" onchange=\"angular.element(this).scope().uploadImage(this)\" ng-model=\"file\" placeholder=\"Upload Files\">\n" +
    "                    </div>  \n" +
    "                    <div class=\"error error-msg\" style='color:red; display: none;'>\n" +
    "<!--                        <span ng-show=\"RegistrationForm.$submitted && RegistrationForm.{{y.a}}.$error.email\">Invalid File!</span>-->\n" +
    "                    </div>     \n" +
    "                </div>\n" +
    "                \n" +
    "             </div>\n" +
    "             <br>\n" +
    "             <br>\n" +
    "             <div class=\"row\">\n" +
    "                <div class=\"col-md-7 col-xl-7\">\n" +
    "                    <label class=\"col-2 col-form-label\">Select up to 3 categories</label>    \n" +
    "                </div>\n" +
    "             </div>\n" +
    "             <div class=\"row\">\n" +
    "                <div class=\"col-md-3 col-xl-3\">\n" +
    "                    \n" +
    "                    <select ng-model=\"restaurant.selectedOption['cat1']\" \n" +
    "                            ng-options=\"cuisines.cuisine as cuisines.cuisine for cuisines in cuisinelist\">\n" +
    "                        <option value=\"\">Select Categories</option>\n" +
    "                    </select>  \n" +
    "                </div>\n" +
    "                <div class=\"col-md-3 col-xl-3\">\n" +
    "                    <select ng-model=\"restaurant.selectedOption2\" \n" +
    "                            ng-options=\"cuisines.cuisine as cuisines.cuisine for cuisines in cuisinelist\">\n" +
    "                        <option value=\"\">Select Categories</option>\n" +
    "                    </select>  \n" +
    "                </div>\n" +
    "                <div class=\"col-md-3 col-xl-3\">\n" +
    "                    <select ng-model=\"restaurant.selectedOption3\" \n" +
    "                            ng-options=\"cuisines.cuisine as cuisines.cuisine for cuisines in cuisinelist\">\n" +
    "                        <option value=\"\">Select Categories</option>\n" +
    "                    </select>  \n" +
    "                </div>\n" +
    "             </div>\n" +
    "             <br>\n" +
    "             <br>\n" +
    "\n" +
    "             <div class=\"row\">\n" +
    "                \n" +
    "                <div class=\"col-md-7 col-xl-7\">\n" +
    "                    \n" +
    "                    <center><button type=\"submit\" class=\"btn btn-success custombtn\">Add you post now</button></center>\n" +
    "                </div>\n" +
    "             </div>\n" +
    "             \n" +
    "        </div>\n" +
    "    </div>\n" +
    "    </form>\n" +
    "    </div>\n" +
    "</div>");
}]);

angular.module("../app/shared/partial/_footer.tpl.html", []).run(["$templateCache", function ($templateCache) {
  $templateCache.put("../app/shared/partial/_footer.tpl.html",
    "<footer style='border-left:solid black 18px;border-right:solid black 20px;border-bottom:solid black 10px;'>\n" +
    "    <div class=\"top-section\">\n" +
    "        <div class=\"border-shadow\">\n" +
    "        </div>\n" +
    "    \n" +
    "    </div>\n" +
    "\n" +
    "<!--    <section id=\"zen-social\">\n" +
    "        <h3 ng-bind=\"Str.template.FindUs\" class=\"ng-binding\">Find us on</h3>\n" +
    "        <ul>\n" +
    "            <li><a rel=\"nofollow\" target=\"_blank\" href=\"https://www.facebook.com/weeloy.sg\" id=\"social-facebook\" title=\"follow weeloy on facebook\"><span class=\"fa fa-facebook\"></span></a></li>\n" +
    "            <li><a rel=\"nofollow\" target=\"_blank\" href=\"https://twitter.com/weeloyasia\" id=\"social-twitter\" title=\"follow weeloy on twitter\"><span class=\"fa fa-twitter\"></span></a></li>\n" +
    "            <li><a rel=\"nofollow\" target=\"_blank\" href=\"https://www.instagram.com/weeloysingapore/\" id=\"social-linkedin\" title=\"follow weeloy on instagram\"><span class=\"fa fa-instagram\"></span></a></li>\n" +
    "        </ul>\n" +
    "    </section>-->\n" +
    "    <footer id=\"zen-footer\">\n" +
    "            <div class=\"col-md-12 col-sm-12 col-xs-12 \">\n" +
    "                    <div class=\"col-md-1 col-sm-1\">\n" +
    "                       \n" +
    "                   </div>\n" +
    "                     <div class=\"col-md-2 col-sm-2\">\n" +
    "                       <a ng-href=\"/search/singapore\"><img  class=\"img-responsive\" src=\"mdclient/assets/images/location/img1.jpg\" style=\"background-image: url('mdclient/assets/images/location/img1.jpg'); background-position: center;background-size: cover;width: 100%;\" > </a>\n" +
    "                      <p class=\"hover\" style='margin-top:5px;'>SINGAPORE</p>\n" +
    "                      <p>\n" +
    "                         83 Amoy Street, Singapore 069902 <br/>\n" +
    "                         SG: +65 92478778 <br />\n" +
    "                         TH: +66 818262109 <br />\n" +
    "                         support@weeloy.com\n" +
    "                      </p>\n" +
    "                       \n" +
    "                   </div>\n" +
    "                    <div class=\"col-md-2 col-sm-2\">\n" +
    "                        <a ng-href=\"/search/bangkok\"><img class=\"img-responsive\" src=\"mdclient/assets/images/location/img2.jpg\" style=\"background-image: url('mdclient/assets/images/location/img2.jpg'); background-position: center;background-size: cover;\"></a>\n" +
    "                        \n" +
    "<!--                            <p class=\"hover\">BANGKOK</p>-->\n" +
    "                    \n" +
    "                   </div>\n" +
    "                    <div class=\"col-md-2 col-sm-2\">\n" +
    "                        <a ng-href=\"/search/phuket\"><img  class=\"img-responsive\" src=\"mdclient/assets/images/location/img3.jpg\" style=\"background-image: url('mdclient/assets/images/location/img3.jpg'); background-position: center;background-size: cover;\"></a>\n" +
    "                            <p class=\"hover\">PHUKET</p>\n" +
    "                       \n" +
    "                   </div>\n" +
    "                    <div class=\"col-md-2 col-sm-2\">\n" +
    "                       <a ng-href=\"/search/singapore\"><img  class=\"img-responsive\" src=\"mdclient/assets/images/location/contact-hk.jpg\" style=\"background-image: url('mdclient/assets/images/location/contact-hk.jpg'); background-position: center;background-size: cover;\"> </a>\n" +
    "                            <p class=\"hover\">Hong kong</p>\n" +
    "                     </div>\n" +
    "                    <div class=\"col-md-2 col-sm-2\">\n" +
    "                       <a ng-href=\"/search/singapore\"><img  class=\"img-responsive\" src=\"mdclient/assets/images/location/contact-kl.jpg\" style=\"background-image: url('mdclient/assets/images/location/contact-kl.jpg'); background-position: center;background-size: cover;\"> </a>\n" +
    "                            <p class=\"hover\">Kuala Lumpur</p>\n" +
    "                     </div>\n" +
    "                    <div class=\"col-md-1 col-sm-1\">\n" +
    "                       \n" +
    "                   </div>\n" +
    "                    \n" +
    "            </div>\n" +
    "        <div class=\"col-md-10 col-md-push-1 col-sm-10 col-sm-push-1 col-xs-12\">\n" +
    "            <div  class=\"col-md-10  col-sm-10  col-xs-10\" style=' margin-left:20%'>\n" +
    "                 <a href=\"blog\" title=\"food blog by weeloy\" onclick=\"document.location.href = 'https://www.weeloy.com/blog';return false;\" style=\"cursor: pointer\" >Weeloy Magazine</a> |\n" +
    "                \n" +
    "                 <a href=\"how-it-works\" title=\"weeloy concept how it works\" >How It Works</a> |\n" +
    "              \n" +
    "                <a href=\"contact\" title=\"weeloy contact page\" >Contact Us</a> |\n" +
    "\n" +
    "                <a href=\"partner\" title=\"weeloy partner page\">Partner With Us</a> |\n" +
    "\n" +
    "                <a target=\"_blank\" href=\"terms-and-conditions-of-service\">Terms & Condition</a> |\n" +
    "\n" +
    "                <a target=\"_blank\" href=\"privacy-policy\">Privacy Policy</a>\n" +
    "              \n" +
    "            </div>\n" +
    "<!--            <div class=\"col-md-3 col-sm-3 col-xs-12 zen-col\">\n" +
    "                <h2 class=\"title\">\n" +
    "                        <span>//</span>infomation\n" +
    "                    </h2>\n" +
    "                <ul>\n" +
    "                    <li>\n" +
    "                        <a href=\"blog\" title=\"food blog by weeloy\" onclick=\"document.location.href = 'https://www.weeloy.com/blog';return false;\" style=\"cursor: pointer\" ng-bind=\"Str.template.FooterInformationBlog\"></a>\n" +
    "                    </li>\n" +
    "                    <li>\n" +
    "                        <a href=\"how-it-works\" title=\"weeloy concept how it works\" ng-bind=\"Str.template.FooterInformationHowItWorks\"></a>\n" +
    "                    </li>\n" +
    "                    <li>\n" +
    "                        <a href=\"contact\" title=\"weeloy contact page\" ng-bind=\"Str.template.FooterInformationContactUs\"></a>\n" +
    "                    </li>\n" +
    "                    <li>\n" +
    "                        <a href=\"partner\" title=\"weeloy partner page\" ng-bind=\"Str.template.FooterInformationPartner\"></a>\n" +
    "                    </li>\n" +
    "                    <li>\n" +
    "                        <a href=\"faq\" title=\"faq weeloy\" ng-bind=\"Str.tempate.FooterInformationFaq\"></a>\n" +
    "                    </li>\n" +
    "                    <li>\n" +
    "                        <a target=\"_blank\" href=\"terms-and-conditions-of-service\" ng-bind=\"Str.template.FooterInformationTermOfService\"></a>\n" +
    "                    </li>\n" +
    "                    <li>\n" +
    "                        <a target=\"_blank\" href=\"privacy-policy\" ng-bind=\"Str.template.FooterInformationPrivacyPolicy\"></a>\n" +
    "                    </li>\n" +
    "                </ul>\n" +
    "            </div>-->\n" +
    "            <div class=\"section-mobileapp\" style=\"margin-top:20px;\">\n" +
    "                <div class=\"col-md-6  col-sm-6 col-xs-6 zen-col app\" style='margin-top:20px;margin-left:31%;'>\n" +
    "                    <div class=\"col-md-3 col-sm-3\" style='margin-bottom:10px;' >\n" +
    "                        <a class=\"mobile-picture-div\" rel=\"nofollow\" target=\"_blank\" href=\"http://itunes.apple.com/app/id973030193\" title=\"Download Weeloy iOS app\">\n" +
    "                           <img width=\"160\" height=\"49\" src=\"mdclient/assets/images/footer/apple.png\">\n" +
    "                        </a>\n" +
    "\n" +
    "                    </div>\n" +
    "                    <div class=\"col-md-3 col-sm-3\" style='margin-left:10px;'>\n" +
    "                        <a class=\"mobile-picture-div\" rel=\"nofollow\" target=\"_blank\" href=\"https://play.google.com/store/apps/details?id=com.weeloy.client\" title=\"Download Weeloy Android app\">\n" +
    "                           <img width=\"160\" height=\"49\" src=\"mdclient/assets/images/footer/android.png\">\n" +
    "                       </a>\n" +
    "\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "            <div class=\"col-md-12  col-sm-12 col-xs-12 zen-col app\" style='margin-top:20px;'>\n" +
    "                <p>\n" +
    "                    Restaurants have unique challenges, needs and workflows that require unique solutions. Our product suite is designed specifically for restaurants to streamline workflows, create operational efficiencies, eliminate administrative redundancies and allow staff and management to focus on providing an optimal dining experience for their guests.\n" +
    "                </p>\n" +
    "            </div>\n" +
    "          \n" +
    "        </div>\n" +
    "    </footer>\n" +
    "</footer>\n" +
    "");
}]);

angular.module("../app/shared/partial/_footer_1.tpl.html", []).run(["$templateCache", function ($templateCache) {
  $templateCache.put("../app/shared/partial/_footer_1.tpl.html",
    "<footer>\n" +
    "    <section id=\"zen-social\">\n" +
    "        <h3 ng-bind=\"Str.template.FindUs\" class=\"ng-binding\">Find us on</h3>\n" +
    "        <ul>\n" +
    "            <li><a rel=\"nofollow\" target=\"_blank\" href=\"https://www.facebook.com/weeloy.sg\" id=\"social-facebook\" title=\"follow weeloy on facebook\"><span class=\"fa fa-facebook\"></span></a></li>\n" +
    "            <li><a rel=\"nofollow\" target=\"_blank\" href=\"https://twitter.com/weeloyasia\" id=\"social-twitter\" title=\"follow weeloy on twitter\"><span class=\"fa fa-twitter\"></span></a></li>\n" +
    "            <li><a rel=\"nofollow\" target=\"_blank\" href=\"https://www.instagram.com/weeloysingapore/\" id=\"social-linkedin\" title=\"follow weeloy on instagram\"><span class=\"fa fa-instagram\"></span></a></li>\n" +
    "        </ul>\n" +
    "    </section>\n" +
    "    <footer id=\"zen-footer\">\n" +
    "        <div class=\"col-md-10 col-md-push-1 col-sm-10 col-sm-push-1 col-xs-12\">\n" +
    "            <div class=\"col-md-3 col-sm-3 col-xs-12 zen-col\">\n" +
    "                <h2 class=\"title\">\n" +
    "                        <span>//</span>infomation\n" +
    "                    </h2>\n" +
    "                <ul>\n" +
    "                    <li>\n" +
    "                        <a href=\"blog\" title=\"food blog by weeloy\" onclick=\"document.location.href = 'https://www.weeloy.com/blog';return false;\" style=\"cursor: pointer\" ng-bind=\"Str.template.FooterInformationBlog\"></a>\n" +
    "                    </li>\n" +
    "                    <li>\n" +
    "                        <a href=\"how-it-works\" title=\"weeloy concept how it works\" ng-bind=\"Str.template.FooterInformationHowItWorks\"></a>\n" +
    "                    </li>\n" +
    "                    <li>\n" +
    "                        <a href=\"contact\" title=\"weeloy contact page\" ng-bind=\"Str.template.FooterInformationContactUs\"></a>\n" +
    "                    </li>\n" +
    "                    <li>\n" +
    "                        <a href=\"partner\" title=\"weeloy partner page\" ng-bind=\"Str.template.FooterInformationPartner\"></a>\n" +
    "                    </li>\n" +
    "                    <li>\n" +
    "                        <a href=\"faq\" title=\"faq weeloy\" ng-bind=\"Str.tempate.FooterInformationFaq\"></a>\n" +
    "                    </li>\n" +
    "                    <li>\n" +
    "                        <a target=\"_blank\" href=\"terms-and-conditions-of-service\" ng-bind=\"Str.template.FooterInformationTermOfService\"></a>\n" +
    "                    </li>\n" +
    "                    <li>\n" +
    "                        <a target=\"_blank\" href=\"privacy-policy\" ng-bind=\"Str.template.FooterInformationPrivacyPolicy\"></a>\n" +
    "                    </li>\n" +
    "                </ul>\n" +
    "            </div>\n" +
    "            <div class=\"col-md-2 col-sm-2 col-xs-12 zen-col\">\n" +
    "                <h2 class=\"title\">\n" +
    "                    <span>//</span>OUR LOCATIONS\n" +
    "                </h2>\n" +
    "                <ul>\n" +
    "                    <li><a href=\"search/singapore\" title=\"find and reserve restaurant in singapore\">Singapore</a></li>\n" +
    "                    <li><a href=\"search/bangkok\" title=\"find and reserve restaurant in bangkok\">Bangkok</a></li>\n" +
    "                    <li><a href=\"search/phuket\" title=\"find and reserve restaurant in phuket\">Phuket</a></li>\n" +
    "                    <li><a href=\"search/hong%20kong\" title=\"find and reserve restaurant in hong-kong\">Hong Kong</a></li>\n" +
    "                </ul>\n" +
    "            </div>\n" +
    "            <div class=\"col-md-2 col-sm-2 col-xs-12 zen-col\">\n" +
    "                <h2 class=\"title\">\n" +
    "                        <span>//</span>POPULAR CUISINES\n" +
    "                    </h2>\n" +
    "                <ul>\n" +
    "                    <li><a href=\"/search/singapore/cuisine/chinese\">Chinese</a></li>\n" +
    "                    <li><a href=\"/search/singapore/cuisine/italian\">Italian</a></li>\n" +
    "                    <li><a href=\"/search/singapore/cuisine/japanese\">Japanese</a></li>\n" +
    "                    <li><a href=\"/search/singapore/cuisine/indian\">Indian</a></li>\n" +
    "                    <li><a href=\"/search/singapore/cuisine/french\">French</a></li>\n" +
    "                </ul>\n" +
    "            </div>\n" +
    "            <div class=\"col-md-2 col-sm-2 col-xs-12 zen-col app\">\n" +
    "                <h2 class=\"title\">\n" +
    "                        <span>//</span>MOBILE\n" +
    "                    </h2>\n" +
    "                <ul>\n" +
    "                    <li>\n" +
    "                        <a class=\"mobile-picture-div\" rel=\"nofollow\" target=\"_blank\" href=\"http://itunes.apple.com/app/id973030193\" title=\"Download Weeloy iOS app\">\n" +
    "                            <img width=\"160\" height=\"49\" src=\"client/assets/images/footer/apple.png\">\n" +
    "                        </a>\n" +
    "                    </li>\n" +
    "                    <li>\n" +
    "                        <a class=\"mobile-picture-div\" rel=\"nofollow\" target=\"_blank\" href=\"https://play.google.com/store/apps/details?id=com.weeloy.client\" title=\"Download Weeloy Android app\">\n" +
    "                            <img width=\"160\" height=\"56\" src=\"client/assets/images/footer/android.png\">\n" +
    "                        </a>\n" +
    "                    </li>\n" +
    "                </ul>\n" +
    "            </div>\n" +
    "            <div class=\"col-md-3 col-sm-3 col-xs-12 zen-col form\">\n" +
    "                <h2 class=\"title\"><span>//</span><span>NEWSLETTER</span></h2>\n" +
    "                <form method=\"POST\" ng-submit=\"SubmitNewletter(newletter_email)\" id=\"newsletterform\" name=\"newsletterform\" novalidate>\n" +
    "                    <input class=\"email newsletter_email\" id=\"email\" name=\"newletter_email\" ng-model=\"newletter_email\" type=\"email\" placeholder=\"{{Str.template.FooterNewsLetterPlaceHolder}}\" required=\"true\">\n" +
    "                    <input type=\"submit\" class=\"submit\" name=\"Join\" value=\"{{Str.template.FooterNewsLetterBtn}}\"></input>\n" +
    "                </form>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </footer>\n" +
    "</footer>\n" +
    "");
}]);

angular.module("../app/shared/partial/_login_form.tpl.html", []).run(["$templateCache", function ($templateCache) {
  $templateCache.put("../app/shared/partial/_login_form.tpl.html",
    "<form class=\"form-horizontal\" name=\"LoginForm\" ng-submit=\"LoginForm.$valid && Login(user)\" novalidate>\n" +
    "    <div class=\"form-group\">\n" +
    "        <input type=\"email\" class=\"form-control simple-form-control\" name=\"email\" ng-model=\"user.email\" placeholder=\"{{Str.template.TextEmail}}\" required>\n" +
    "        <div class=\"error\">\n" +
    "            <span ng-show=\"LoginForm.$submitted && LoginForm.email.$error.required\" ng-bind=\"Str.template.TextPleaseEnterYourEmail\"></span>\n" +
    "            <span ng-show=\"LoginForm.$submitted && LoginForm.email.$error.email\" ng-bind=\"Str.template.TextPleaseEnterAValidEmail\"></span>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "    <div class=\"form-group\">\n" +
    "        \n" +
    "        <input id=\"password\" name=\"password\" ng-model=\"user.password\" type=\"{{ (showPassword == true) ? 'text' : 'password' }}\" class=\"form-control simple-form-control\" placeholder=\"{{Str.template.TextPassword}}\" required>\n" +
    "        \n" +
    "        <div class=\"error\">\n" +
    "            <span ng-show=\"LoginForm.$submitted && LoginForm.password.$error.required\" ng-bind=\"Str.template.TextPleaseEnterYourPassword\"></span>\n" +
    "        </div>\n" +
    "        <div class=\"show-password\">\n" +
    "            <input id=\"show-password-ip\" type=\"checkbox\" ng-model=\"showPassword\">\n" +
    "            <label style=\"color: #333;font-weight: normal;font-family: proximanova;\" class=\"\" for=\"show-password-ip\" ng-bind=\"Str.template.TextShowPassword\"></label>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "    <div class=\"form-group\">\n" +
    "        <input type=\"submit\" class=\"book-button-sm btn-leftBottom-orange\" value=\"{{Str.template.LoginBtn}}\">\n" +
    "    </div>\n" +
    "</form>\n" +
    "");
}]);

angular.module("../app/shared/partial/_menu.tpl.html", []).run(["$templateCache", function ($templateCache) {
  $templateCache.put("../app/shared/partial/_menu.tpl.html",
    "<div class=\"collapse navbar-collapse\" id=\"bs-example-navbar-collapse-1\">\n" +
    "    <ul class=\"nav navbar-nav navbar-left\" style='padding-top:15px;'>\n" +
    "        <li>\n" +
    "            <a analytics-on=\"click\" analytics-event=\"c_header_menu_home\" analytics-category=\"header_menu_home\" href=\"/\">Home</a>\n" +
    "        </li>\n" +
    "         <li>\n" +
    "            <a analytics-on=\"click\" analytics-event=\"c_header_menu_home\" analytics-category=\"header_menu_home\" href=\"directory\">Browse</a>\n" +
    "        </li>\n" +
    "        <li>\n" +
    "            <a analytics-on=\"click\" analytics-event=\"c_header_menu_search\" analytics-category=\"header_menu_search\" href=\"search/\"><i class='glyphicon glyphicon-map-marker' style='color:#B40404' ></i> Map</a>\n" +
    "        </li>\n" +
    "        <li>\n" +
    "            <a  analytics-on=\"click\" analytics-event=\"c_header_menu_search\" analytics-category=\"header_menu_search\" href=\"dailyspecial\" ><i class='glyphicon glyphicon-bookmark' style='color:#B40404' ></i> Daily Special Borad</a>\n" +
    "        </li>\n" +
    "         <li>\n" +
    "<!--            <a href=\"blog\" title=\"food blog by weeloy\" onclick=\"document.location.href = 'https://www.weeloy.com/blog';return false;\" style=\"cursor: pointer\" ng-bind=\"Str.template.FooterInformationBlog\"></a>-->\n" +
    "            <a analytics-on=\"click\" analytics-event=\"c_header_menu_blog\" analytics-category=\"header_menu_blog\"  href='weeloy_magazine/';>Weeloy Magazine</a>\n" +
    "        </li>\n" +
    "        <li ng-show=\"loggedin === false\" >\n" +
    "            <a href=\"#\" data-toggle=\"modal\" data-target=\"#loginModal\" ng-bind=\"Str.template.LoginBtnText\" title=\"Login Weeloy restaurant reservation system\"></a>\n" +
    "        </li>\n" +
    "        <li>\n" +
    "            <a analytics-on=\"click\" analytics-event=\"c_header_menu_down_apple\" analytics-category=\"header_menu_down_apple\" href=\"https://itunes.apple.com/app/id973030193\" target=\"_blank\" rel=\"nofollow\"><span class=\"download-apple\" style=\"background: transparent url('client/assets/images/icons/mobile-icons.png') no-repeat scroll -1px 0;padding: 5px 10px\"></span></a>\n" +
    "        </li>\n" +
    "        <li>\n" +
    "            <a analytics-on=\"click\" analytics-event=\"c_header_menu_down_android\" analytics-category=\"header_menu_down_android\" href=\"https://play.google.com/store/apps/details?id=com.weeloy.client\" target=\"_blank\" rel=\"nofollow\"><span class=\"download-android\" style=\"background: transparent url('client/assets/images/icons/mobile-icons.png') no-repeat scroll -39px 0;padding: 5px 10px\"></span></a>\n" +
    "        </li>\n" +
    "        <li ng-show=\"loggedin === true\" >\n" +
    "            <a class=\"account\" id=\"user-profile-btn\" ng-click=\"showLoggedin($event)\" title=\"Your Weeloy account\">\n" +
    "                <img width=\"36\" height=\"36\" class=\"icon-ico_user-36\" src=\"https://static2.weeloy.com/images/sprites/transparent.png\" alt=\"weeloy member section\"></a>\n" +
    "        </li>\n" +
    "        <div class=\"nav navbar-nav navbar-right loggedin-menu submenu\" ng-show=\"showLoggedinBox\">\n" +
    "            <ul class=\"root\">\n" +
    "                <li>\n" +
    "                    <a href=\"mybookings\" ng-bind=\"Str.template.MenuBooking\" title=\"Weeloy restaurant reservation - my bookings\"></a>\n" +
    "                </li>\n" +
    "                <li>\n" +
    "                    <a href=\"myorders\" ng-bind=\"Str.template.MenuMyOrder\" title=\"Weeloy restaurant reservation - my  orders\"></a>\n" +
    "                </li>\n" +
    "                <li>\n" +
    "                    <a href=\"myreviews\" ng-bind=\"Str.template.MenuReview\" title=\"Weeloy restaurant reservation - my reviews\"></a>\n" +
    "                </li>\n" +
    "                <li>\n" +
    "                    <a href=\"myaccount\" ng-bind=\"Str.template.MenuAccount\" title=\"Weeloy restaurant reservation - my account\"></a>\n" +
    "                </li>\n" +
    "                <li>\n" +
    "                    <a href=\"#\" ng-click=\"logout()\" ng-bind=\"Str.template.MenuLogout\" title=\"Logout Weeloy restaurant reservation system\"></a>\n" +
    "                </li>\n" +
    "            </ul>\n" +
    "        </div>\n" +
    "    </ul>\n" +
    "    \n" +
    "</div>\n" +
    "\n" +
    "<script>\n" +
    "    $('.nav a').on('click', function(){\n" +
    "        $('.btn-navbar').click(); //bootstrap 2.x\n" +
    "        $('.navbar-toggle').click() //bootstrap 3.x by Richard\n" +
    "    });\n" +
    "</script>");
}]);

angular.module("../app/shared/partial/_menu_user.tpl.html", []).run(["$templateCache", function ($templateCache) {
  $templateCache.put("../app/shared/partial/_menu_user.tpl.html",
    "<div id=\"menu_user\">\n" +
    "    <ul>\n" +
    "        <li class=\"{{ MenuUserSelected == 'mybookings' ? 'menu-user-selected' : '' }}\">\n" +
    "            <a href=\"mybookings\">Bookings</a>\n" +
    "        </li>\n" +
    "        <li class=\"{{ MenuUserSelected == 'myorders' ? 'menu-user-selected' : '' }}\">\n" +
    "            <a href=\"myorders\">Orders</a>\n" +
    "        </li>\n" +
    "        <li class=\"{{ MenuUserSelected == 'myreviews' ? 'menu-user-selected' : '' }}\">\n" +
    "            <a href=\"myreviews\">Reviews</a>\n" +
    "        </li>\n" +
    "        <li class=\"{{ MenuUserSelected == 'myaccount' ? 'menu-user-selected' : '' }}\">\n" +
    "            <a href=\"myaccount\">Account</a>\n" +
    "        </li>\n" +
    "    </ul>\n" +
    "</div>\n" +
    "\n" +
    "");
}]);

angular.module("../app/shared/partial/_restaurant_item.tpl.html", []).run(["$templateCache", function ($templateCache) {
  $templateCache.put("../app/shared/partial/_restaurant_item.tpl.html",
    "<style type=\"text/css\">\n" +
    "    .cconts{\n" +
    "\n" +
    "    }\n" +
    "    .crestheader\n" +
    "    {\n" +
    "        height: 25px;\n" +
    "        width: 100%;\n" +
    "    }\n" +
    "    .cimg\n" +
    "    {\n" +
    "        height: 145px;\n" +
    "        width: 100%;\n" +
    "    }\n" +
    "    .img-transparent\n" +
    "    {\n" +
    "        width: 100% !important; \n" +
    "        height: 100% !important;\n" +
    "        background-position: 'center' !important;\n" +
    "        background-repeat: 'no-repeat' !important;\n" +
    "        background-size: 'cover' !important;\n" +
    "    }\n" +
    "    .ccontext \n" +
    "    {\n" +
    "        width: 100%;\n" +
    "        height: 25px;\n" +
    "        font-size: 10px;\n" +
    "        margin-top: 10px;\n" +
    "        margin-bottom: 10px;\n" +
    "    }\n" +
    "    .cbtn\n" +
    "    {\n" +
    "\n" +
    "    }\n" +
    "    .cpricing\n" +
    "    {\n" +
    "        float: left;\n" +
    "    }\n" +
    "    .cloccont\n" +
    "    {\n" +
    "        float: left;\n" +
    "        margin-right: 30px;\n" +
    "    }\n" +
    "    .cbtnholder\n" +
    "    {\n" +
    "        padding-top: 5px;\n" +
    "    }\n" +
    "\n" +
    "    .btn-leftBottom-blue-noborder {\n" +
    "        background: #283271;\n" +
    "        border: 1px solid #283271;\n" +
    "    }\n" +
    "\n" +
    "    .book-button-sm2 {\n" +
    "        width: 225px;\n" +
    "        height: 35px;\n" +
    "    }\n" +
    "\n" +
    "    .book-button-sm2 {\n" +
    "        color: #fff;\n" +
    "        font-weight: 400;\n" +
    "        padding: 7px;\n" +
    "        width: 225px;\n" +
    "        margin: 0;\n" +
    "        font-size: 14px;\n" +
    "        text-transform: uppercase;\n" +
    "    }\n" +
    "    .ccardmarker {\n" +
    "        float: right;\n" +
    "        width: 20px;\n" +
    "        height: 20px;\n" +
    "        margin-top: 5px;\n" +
    "        margin-left: 5px;\n" +
    "        cursor: pointer;\n" +
    "    }\n" +
    "    .crestheaderinside{\n" +
    "        overflow: hidden;\n" +
    "        text-overflow: ellipsis;\n" +
    "        display: -webkit-box;\n" +
    "        max-height: 20px;\n" +
    "        -webkit-line-clamp: 1;\n" +
    "        -webkit-box-orient: vertical;\n" +
    "    }\n" +
    "\n" +
    "    \n" +
    "\n" +
    "</style>\n" +
    "<div class=\"cconts\">\n" +
    "    <div class=\"crestheader\">\n" +
    "        <div class=\"crestheaderinside\">\n" +
    "            {{restaurant.title}}\n" +
    "        </div>\n" +
    "    </div>\n" +
    "    <div class=\"cimg\">\n" +
    "        <a ng-href=\"{{ restaurant.getInternalPath() }}\" target=\"{{ OpenRestaurantInNewTab }}\">\n" +
    "            <img ng-src=\"mdclient/assets/images/month/transparent.png\" class=\"img-responsive img-transparent\">\n" +
    "        </a>\n" +
    "    </div>\n" +
    "    <div class=\"ccontext\">\n" +
    "        <div class = \"ccuisine\">\n" +
    "            <span ng-repeat=\"cusine in restaurant.getCuisineAsArray()\">{{cusine}}<span ng-show=\"!$last\">, </span></span>\n" +
    "            <div class=\"ccardmarker\">\n" +
    "                <a href=\"#\" ng-click=\"loadmap(restaurant.getLatitude().lat, restaurant.getLatitude().lng)\">\n" +
    "                    <img title=\"Location\" ng-src=\"mdclient/assets/images/month/transparent.png\" style=\"background-image: url('mdclient/assets/images/locationicon.png'); background-repeat: no-repeat; background-size: cover; background-position: center; width: 100%; height: 100%;\">\n" +
    "                </a>\n" +
    "                \n" +
    "            </div>\n" +
    "            <div class=\"ccardmarker\" ng-if=\"restaurant.reviews != ''\">\n" +
    "                <img title=\"Reviews\" ng-src=\"mdclient/assets/images/month/transparent.png\" style=\"background-image: url('mdclient/assets/images/staricon.png'); background-repeat: no-repeat; background-size: cover; background-position: center; width: 100%; height: 100%;\">\n" +
    "            </div>\n" +
    "            <div class=\"ccardmarker\">\n" +
    "                <img title=\"Daily Special Board\" ng-src=\"mdclient/assets/images/month/transparent.png\" style=\"background-image: url('mdclient/assets/images/ribbonicon.png'); background-repeat: no-repeat; background-size: cover; background-position: center; width: 100%; height: 100%;\">\n" +
    "            </div>\n" +
    "            <!-- <i class=\"fa fa-map-marker ccardmarker\"></i> -->\n" +
    "        </div>\n" +
    "        <div class = \"cloc\">\n" +
    "            <!-- <span>{{restaurant.getRegion()}}<span ng-if=\"restaurant.getCity() != ''\">, </span> {{restaurant.getCity()}}</span> -->\n" +
    "            <span class=\"cloccont\">{{restaurant.getRegion()}} </span>\n" +
    "            <div class=\"cpricing\">\n" +
    "                {{restaurant.pricing}}\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "    <div class=\"cbtnholder\">\n" +
    "        <center>\n" +
    "            <input class=\"input-group-addon book-button-sm2 btn-leftBottom-blue-noborder\" type=\"button\" name=\"\" value=\"Book Now\">\n" +
    "        </center>\n" +
    "    </div>\n" +
    "<!--    <div class=\"book-now\">\n" +
    "        <a ng-href=\"{{restaurant.getBookNowPageUrl()}}\">\n" +
    "            <button analytics-on=\"click\" analytics-event=\"c_book_resto\" analytics-category=\"c_book_resto\" ng-if=\"restaurant.status=='active'\" id=\"btn_book\" class=\"book-button-sm btn-leftBottom-blue\" ng-bind=\"restaurant.book_button.label\"></button>\n" +
    "        </a>\n" +
    "        <button analytics-on=\"click\" analytics-event=\"c_cs_book_resto\" analytics-category=\"c_cs_book_resto\" ng-if=\"restaurant.status=='comingsoon'\" id=\"btn_book\" class=\"btn custom_button {{restaurant.book_button.style}}\" ng-bind=\"restaurant.book_button.label\"></button>\n" +
    "    </div>-->\n" +
    "</div>\n" +
    "");
}]);

angular.module("../app/shared/partial/_signup_form.tpl.html", []).run(["$templateCache", function ($templateCache) {
  $templateCache.put("../app/shared/partial/_signup_form.tpl.html",
    "<form name=\"SignupForm\" ng-submit=\"SignupForm.$valid && SignupUser.password == SignupUser.password_confirmation && Signup(SignupUser,mobile)\" novalidate autocomplete=\"off\">\n" +
    "    <div class=\"form-group\">\n" +
    "\n" +
    "            <input name=\"email\" ng-model=\"SignupUser.email\" type=\"text\" class=\"form-control simple-form-control\" value=\"\" placeholder=\"{{Str.template.TextEmail}}\" required>\n" +
    "        <div class=\"error\">\n" +
    "            <span ng-show=\"SignupForm.$submitted && SignupForm.email.$error.required\" ng-bind=\"Str.template.TextPleaseEnterYourEmail\"></span>\n" +
    "            <span ng-show=\"SignupForm.$submitted && !SignupForm.email.$error.required && SignupForm.email.$error.email\" ng-bind=\"Str.template.TextPleaseEnterAValidEmail\"></span>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "    <div class=\"form-group\">\n" +
    "            <input name=\"first_name\" type=\"text\" class=\"form-control simple-form-control\" ng-model=\"SignupUser.first_name\" placeholder=\"{{Str.template.TextFirstName}}\" required>\n" +
    "        <div class=\"error\">\n" +
    "            <span ng-show=\"SignupForm.$submitted && SignupForm.first_name.$error.required\" ng-bind=\"Str.template.TextPleaseEnterYourFirstname\"></span>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "    <div class=\"form-group\">\n" +
    "\n" +
    "        <input name=\"last_name\" type=\"text\" class=\"form-control simple-form-control\" ng-model=\"SignupUser.last_name\" placeholder=\"{{Str.template.TextLastName}}\" required>\n" +
    "\n" +
    "        <div class=\"error\">\n" +
    "            <span ng-show=\"SignupForm.$submitted && SignupForm.last_name.$error.required\" ng-bind=\"Str.template.TextPleaseEnteYourLastName\"></span>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "  \n" +
    "    <div class=\"form-group\">\n" +
    "        <div class=\"input-group\">\n" +
    "            <div class='input-group-btn '>\n" +
    "            <button type='button' id='itemdfcountry' style='height: 40px; border-radius: 2px;' class='btn btn-default dropdown-toggle' data-toggle='dropdown'>\n" +
    "                                               <i class=\"glyphicon glyphicon-earphone\"></i><span class='caret'></span>\n" +
    "                                            </button>\n" +
    "            <ul class='dropdown-menu scrollable-menu' style='font-size:12px;height: auto;\n" +
    "    max-height: 250px;\n" +
    "    overflow-x: hidden;'>\n" +
    "             <li ng-repeat=\"s in countries\"><a ng-if=\"s.b != '';\" href=\"javascript:;\" ng-click=\"listPhoneIndex(s.a);\"  ><i class='famfamfam-flag-{{s.b}}'></i> {{s.a}}</a></li>\n" +
    "            </ul></div>\n" +
    "<!--            <span class=\"input-group-addon\"><i class=\"glyphicon glyphicon-earphone\"></i></span>-->\n" +
    "            <input id=\"txt-mobile\" name=\"mobile\" type=\"text\" class=\"form-control simple-form-control\" ng-model=\"mobile\" placeholder=\"{{Str.template.TextMobile}}\"  ng-change='checkvalidtel();' ng-model-options=\"{updateOn: 'blur'}\" required >\n" +
    "            \n" +
    "        </div>\n" +
    "        <div class=\"error\">\n" +
    "            <span ng-show=\"SignupForm.$submitted && SignupForm.mobile.$error.required\" ng-bind=\"Str.template.TextPleaseEnterYourPhoneNumber\"></span>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "    <div class=\"form-group\">\n" +
    "            <input name=\"password\" type=\"password\" class=\"form-control simple-form-control passtype\" ng-model=\"SignupUser.password\" placeholder=\"{{Str.template.TextPassword}}\" ng-minlength=\"8\" ng-maxlength=\"12\" ng-pattern=\"/(?=.*[a-z])(?=.*[A-Z])(?=.*[^a-zA-Z])/\" required>\n" +
    "        <div class=\"error\">\n" +
    "            <span ng-show=\"SignupForm.$submitted && SignupForm.password.$error.required\" ng-bind=\"Str.template.TextPleaseEnterYourPassword\"></span>\n" +
    "            <span ng-show=\"!SignupForm.password.$error.required && (SignupForm.password.$error.minlength || SignupForm.password.$error.maxlength) && SignupForm.password.$dirty\">Passwords must be between 8 and 12 characters.</span>\n" +
    "            <span ng-show=\"!SignupForm.password.$error.required && !SignupForm.password.$error.minlength && !SignupForm.password.$error.maxlength && SignupForm.password.$error.pattern && SignupForm.password.$dirty\">Must contain one lower &amp; uppercase letter, and one non-alpha character (a number or a symbol.)</span>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "    <div class=\"form-group\">\n" +
    "            <input name=\"password_confirmation\" type=\"password\" class=\"form-control simple-form-control passtype\" ng-model=\"SignupUser.password_confirmation\" placeholder=\"{{Str.template.TextRePassword}}\" ng-minlength=\"8\" ng-maxlength=\"12\" ng-pattern=\"/(?=.*[a-z])(?=.*[A-Z])(?=.*[^a-zA-Z])/\" required>\n" +
    "        <div class=\"error\">\n" +
    "            <span ng-show=\"SignupForm.$submitted && SignupForm.password_confirmation.$error.required\" ng-bind=\"Str.template.TextPleaseEnterYourPasswordAgain\"></span>\n" +
    "            <span ng-show=\"SignupForm.$submitted && !SignupForm.password_confirmation.$error.required && SignupUser.password != SignupUser.password_confirmation\" ng-bind=\"Str.template.TextPleaseEnterYourPasswordAgain\"></span>\n" +
    "            <span ng-show=\"!SignupForm.password_confirmation.$error.required && (SignupForm.password_confirmation.$error.minlength || SignupForm.password_confirmation.$error.maxlength) && SignupForm.password_confirmation.$dirty\">Passwords must be between 8 and 12 characters.</span>\n" +
    "            <span ng-show=\"!SignupForm.password_confirmation.$error.required && !SignupForm.password_confirmation.$error.minlength && !SignupForm.password_confirmation.$error.maxlength && SignupForm.password_confirmation.$error.pattern && SignupForm.password_confirmation.$dirty\">Must contain one lower &amp; uppercase letter, and one non-alpha character (a number or a symbol.)</span>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "    <div class=\"form-group\">\n" +
    "        <input type=\"submit\" class=\"book-button-sm btn-leftBottom-orange\" value=\"{{Str.template.RegisterBtn}}\">\n" +
    "    </div>\n" +
    "</form>\n" +
    "");
}]);
