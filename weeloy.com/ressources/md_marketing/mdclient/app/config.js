var config = {
    version:'1.74',
    lang: 'en',
    restaurantsPerPage: 12, // number of restaurant will display in search page
    ImageServer: ['//media1.weeloy.com', '//media2.weeloy.com', '//media3.weeloy.com', '//media4.weeloy.com', '//media5.weeloy.com'], // list of all image server
    ImageSizes: [140, 180, 270, 300, 325, 360, 450, 500, 600, 700, 1024], // list of all size in pixel, from low to high
    WheelImageSizes: [50, 80, 100, 150], // list of all size in pixel, from low to high
    OrderPerPage: 20,
};