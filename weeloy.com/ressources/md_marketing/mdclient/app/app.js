var MdWeeloyApp = angular.module('MdWeeloyApp', [
    'ngRoute',
    'angular-cache',
    'angulartics',
    'angulartics.google.tagmanager',
    'app.api',
    'RootController',
    'HomeController',
    'SearchController',
    'DailySpecialController',
    'MagazineController',
    'DirectoryController',
    'TitleAndMetaTag',
    'weeloy.breadcrumb',
]);

MdWeeloyApp.run([
    '$rootScope',
    '$http',
    '$location',
    '$routeParams',
    'CacheFactory',
    'TitleAndMeta',
    '$analytics',
    function($rootScope, $http, $location, $routeParams, CacheFactory, TitleAndMeta, AuditLog, $analytics) {


        var navbar = document.getElementsByTagName("nav")[0];
        var style = window.getComputedStyle(navbar, null);
        var user =[],UserSession =[];

        $rootScope.base_url = BASE_URL;
        $rootScope.base_path = BASE_PATH;

        CacheFactory('LocalStorage', {
            maxAge: 15 * 24 * 60 * 60 * 1000, // Items added to this cache expire after 15 days.
            cacheFlushInterval: 15 * 24 * 60 * 60 * 1000, // This cache will clear itself every 15 days.
            deleteOnExpire: 'aggressive', // Items will be deleted from this cache right when they expire.
            storageMode: 'localStorage' // This cache will use `localStorage`.
        });

        moment.locale('en');

        if (localStorage.getItem("v") !== config.version) {
            localStorage.clear();
            localStorage.setItem('v', config.version);
        }

        $http.get('client/assets/language/' + config.lang + '.json?v=' + config.version, {
            cache: CacheFactory.get('LocalStorage'),
        }).success(function(response) {
            $rootScope.Str = response.data.str;
        });
        if ($location.$$search != undefined && $location.$$search.city != undefined) {
            UserSession.search_city = $location.$$search.city;
        };

        $rootScope.loggedin = 'loggedin';
        //$rootScope.user = new User(user);
        $rootScope.user =[];
        $rootScope.UserSession = UserSession;


        var SearchParams = {
            city: {
                param_key: null,
                data: '',
            },
            cuisine: {
                param_key: null,
                data: '',
            },
            pricing: {
                param_key: null,
                data: '',
            },
            query: {
                param_key: null,
                data: '',
            },
            page: {
                param_key: null,
                data: '',
            },
        };

        $rootScope.checkLoggedin = function() {
            if ($rootScope.loggedin === false) {
                $location.path(BASE_PATH);
            };
        };

        $rootScope.SearchParams = SearchParams;

        $rootScope.$on( "$routeChangeStart", function(event, next, current) {
            if(BASE_URL.indexOf('www.weeloy.com') !== -1 || BASE_URL.indexOf('api.weeloy.com') !== -1){
                if(typeof window._atrk_fired === "undefined"){
                    console.log('setting A from Undefined');
                    window._atrk_fired=false;
                    _atrk_opts = { atrk_acct:"n3M5n1aMp410V1", domain:"weeloy.com",dynamic: true};
                    (function() { var as = document.createElement('script'); as.type = 'text/javascript'; as.async = true; as.src = "https://d31qbv1cthcecs.cloudfront.net/atrk.js"; var s = document.getElementsByTagName('script')[0];s.parentNode.insertBefore(as, s); })();

                }else{
                    console.log('setting A from Defined');
                    window._atrk_fired = false;
                    atrk();
                }
            }
            
        });
        
        $rootScope.$on('$routeChangeSuccess', function(evt, next, current) {
            //if (next !== undefined && current !== undefined && next.$$route !== undefined && current.$$route !== undefined && next.$$route.controller === current.$$route.controller) {
            //    console.log('do not scrollTop');
           // } else {
            //    console.log(next, current);
                // Set dynamic metadata for each page
                // TitleAndMeta service was defined in /client/app/shared/services/TitleAndMetaTag.js
                // Restaurant info page meta data has been set in /client/app/components/restaurant_info_page/RestaurantInfoController.js
                // All Rewards page meta data has been set in /client/app/components/all_rewards_page/AllRewardsController.js
            	if(next === undefined || next.$$route === undefined || next.$$route.controller === undefined) 
            		{
  			console.log('Undefined controller');
                        //window.location  = '/404';
           		return;
            		}
               	
                switch (next.$$route !== undefined && next.$$route.controller) {
                    case 'HomeCtrl':
                    case 'SearchCtrl':
                        resetSearchParms();
                        getSearchParams();
                        break;

                    case 'MyBookingCtrl':
                    case 'MyOrdersCtrl':
                    case 'MyReviewCtrl':
                    case 'MyAccountCtrl':
                        $rootScope.checkLoggedin();
                        break;
                };
                TitleAndMeta.setCity($rootScope.UserSession.search_city, next.$$route.controller);

                $("html, body").animate({
                    scrollTop: 0
                }, 300);
            //}
        });


//        if ($rootScope.user != null) {
//            BasicAuthentication($rootScope.user);
//        };
        $rootScope.$on('UserLogin', function(evt, user) {
            $rootScope.user = user;
            $rootScope.loggedin = true;
            BasicAuthentication($rootScope.user);
        });

        var mediaServer = config.ImageServer[Math.floor(Math.random() * config.ImageServer.length)];
        $rootScope.mediaServer = mediaServer;

//        function BasicAuthentication(user) {
//            var Authorization = btoa(user.email + ':' + user.getToken());
//            $http.defaults.headers.common.Authorization = 'Basic ' + Authorization;
//        }

        function getSearchParams() {
            // detect SearchCity
            if (UserSession.search_city != undefined) {
                SearchParams.city = { // default search city
                    param_key: 'city',
                    data: UserSession.search_city.toLowerCase(),
                };
            } else {
                SearchParams.city = {
                    param_key: 'city',
                    data: 'singapore',
                };
            };
            // if request uri is /search/:city/(.*)
            if ($routeParams.city != undefined) {
                SearchParams.city = {
                    param_key: 'city',
                    data: $routeParams.city,
                };
            };
            // if request uri is /serch/:city/:query
            if ($routeParams.query != undefined) {
                SearchParams.query = {
                    param_key: 'query',
                    data: $routeParams.query,
                };
            };
            // request uri format /search/:city/:param_name_1/:param_1/:param_name_2/:param_2/:param_name_3/:param_3/:param_name_4/:param_4
            var param_name_pattern = /param_name_([0-9])/;
            var param_value_pattern = /param_([0-9])/;
            for (key in $routeParams) {
                var matches = key.match(param_name_pattern);
                switch ($routeParams[key]) {
                    case 'cuisine':
                        SearchParams.cuisine = {
                            param_key: 'param_' + matches[1],
                            data: $routeParams['param_' + matches[1]],
                        };
                        break;
                    case 'tags':
                        SearchParams.tags = {
                            param_key: 'param_' + matches[1],
                            data: $routeParams['param_' + matches[1]],
                        };
                        break;
                    case 'area':
                        SearchParams.area = {
                            param_key: 'param_' + matches[1],
                            data: $routeParams['param_' + matches[1]],
                        };
                        break;
                    case 'pricing':
                        SearchParams.pricing = {
                            param_key: 'param_' + matches[1],
                            data: $routeParams['param_' + matches[1]],
                        };
                        break;
                    case 'page':
                        SearchParams.page = {
                            param_key: 'param_' + matches[1],
                            data: $routeParams['param_' + matches[1]],
                        };
                        break;
                    case 'query':
                        SearchParams.query = {
                            param_key: 'param_' + matches[1],
                            data: $routeParams['param_' + matches[1]],
                        };
                        break;
                    default:
                        break;
                };
            };
            $rootScope.SearchParams = SearchParams;
        }

        function resetSearchParms() {
            SearchParams = {
                city: {
                    param_key: null,
                    data: '',
                },
                cuisine: {
                    param_key: null,
                    data: '',
                },
                tags: {
                    param_key: null,
                    data: '',
                },
                area: {
                    param_key: null,
                    data: '',
                },
                pricing: {
                    param_key: null,
                    data: '',
                },
                query: {
                    param_key: null,
                    data: '',
                },
                page: {
                    param_key: null,
                    data: '',
                },
            };
        };
        $rootScope.getSearchParams = function() {
            getSearchParams();
        }
    }
]);
