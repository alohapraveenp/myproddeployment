var MagazineService = angular.module('MagazineService', []);
MagazineService.service('Magazine', ['$http', '$q', function($http, $q) {
    this.getMagazineList = function() {
        var defferred = $q.defer();
        var API_URL = '../../api/md_marketing/getMagazine';
        $http.get(API_URL, {
            cache: true
        }).success(function(response) {
            defferred.resolve(response);
        });
        return defferred.promise;
    };

}]);
