var MagazineController = angular.module('MagazineController', ['NfSearch', 'MagazineService']);
MagazineController.controller('MagazineCtrl', [
    '$rootScope',
    '$scope',
    '$location',
    'Magazine',
    '$sce',
    '$window',
    '$http',
    function ($rootScope, $scope, $location, Magazine, $sce, $window,$http) {
        $scope.restaurant = null;
        $scope.items = [];
        $scope.itemstemp = [];
        $scope.temp = null;
        $scope.loading = true;
        $scope.magazinemodalcontent = null;
        $scope.magazinemodalcontentimages = null;
        $scope.contentArr = [];
        var index = 0;
        var clean_img_html = [];
        $scope.clean_img = [];

        Magazine.getMagazineList().then(function (response) {
            $scope.dsblist = null;
            var dsblistcheck = response.status;
            if (dsblistcheck == 1)
            {
                var dsblist = response.data;
                for (i = 0; i < dsblist.length; i++) {
                    cc = dsblist[i];
                    $scope.itemstemp[i] = cc;
                }

                $scope.more();
            }
            else
            {
                $scope.loading = false;
                alert(response.data.errors);
            }

        });

        function sleep(milliseconds) {
          var start = new Date().getTime();
          for (var i = 0; i < 1e7; i++) {
            if ((new Date().getTime() - start) > milliseconds){
              break;
            }
          }
        }

        // this function fetches a random text and adds it to array
        $scope.more = function ()
        {
            $scope.loading = false;
            //without slice for data migration (remove it when normal use)
            //temp = $scope.itemstemp.slice(index, index + 12);
            temp = $scope.itemstemp;
            //alert(temp.length);
            for (x = 0; x < temp.length; x++) {
                cc = temp[x];
                $scope.items.push(cc);
            }
            index = index + 12;

            //Data Migration Starts
            $scope.clean_content =[];
            $scope.clean_img =[];
            $scope.contentArr = [];
            $scope.contentArrx = [];
            var imgArr = [];
            var rex = /<img\s[^>]*?src\s*=\s*['\"]([^'\"]*?)['\"][^>]*?>/g;
                    
            for(x=0;x < temp.length;x++)
            {
                while (m = rex.exec(temp[x].post_content)) {
                    for (i = 0; i < m.length; i++) {
                        if (m[i].indexOf('src=') > -1) {
                        } else {
                            if (m[i].indexOf('booknow') <= -1){
                                $scope.clean_img.push(m[i]);
                                $scope.contentArr.push({
                                    'image': m[i],
                                    'id':temp[x].ID,
                                });
                            }
                        }
                    }
                }
            }
            
            var API_URL = '../../api/md_marketing/getMagazine/saveimg/';
            $http.post(API_URL, $scope.contentArr).success(function(response) 
            {  
               defferred.resolve(response);
            });

            for(x=0;x < temp.length;x++)
            {
                $scope.clean_content = temp[x].post_content.replace(/(<([^>]+)>)/ig, "");
                $scope.clean_content.replace(/&nbsp;\r?\n|\r/g, "");

                var splittedArray = $scope.clean_content.split(/#(\w+)/g);
                $scope.clean_content = [];
                for (k = 0; k < splittedArray.length; k++) 
                {
                    if (splittedArray[k].match(/[a-z]/i)) 
                    {
                        $scope.clean_content.push(splittedArray[k]);
                    }

                }
                $scope.contentArr =[];

                for (k = 0; k < $scope.clean_img.length; k++) 
                {

                    for (var j = 0; j < $scope.clean_content.length;j++) 
                    {
                        $scope.content = '' ;
                        if($scope.clean_content[j] && $scope.clean_content[j].match(/[a-z]/i))
                        {
                            $scope.content = $scope.clean_content[j];
                        }

                    }

                               
                    j++;
                                          
                }
                $scope.contentArrx.push({
                    'id':temp[x].ID,
                    'content': $scope.content
                });
            }
            var API_URL = '../../api/md_marketing/getMagazine/savecont/';
            $http.post(API_URL, $scope.contentArrx).success(function(response) 
            {  
               defferred.resolve(response);
            });
            //Data Migration Ends

        };

        $scope.magazinedetails = function (index, item, obj)
        {
        $scope.clean_content =[];
        $scope.clean_img =[];
            var imgArr = [];
            var rex = /<img\s[^>]*?src\s*=\s*['\"]([^'\"]*?)['\"][^>]*?>/g;
            while (m = rex.exec(item.post_content)) {
                for (i = 0; i < m.length; i++) {
                    if (m[i].indexOf('src=') > -1) {
                    } else {
                        if (m[i].indexOf('booknow') <= -1)
                            $scope.clean_img.push(m[i]);
                    }

                }

            }
            $scope.clean_content = item.post_content.replace(/(<([^>]+)>)/ig, "");
            $scope.clean_content.replace(/&nbsp;\r?\n|\r/g, "");

            var splittedArray = $scope.clean_content.split(/#(\w+)/g);
            $scope.clean_content = [];
            for (k = 0; k < splittedArray.length; k++) 
            {
                if (splittedArray[k].match(/[a-z]/i)) 
                {
                    $scope.clean_content.push(splittedArray[k]);
                }

            }
            $scope.contentArr =[];

            for (k = 0; k < $scope.clean_img.length; k++) 
            {

                for (var j = 0; j < $scope.clean_content.length;j++) 
                {
                    $scope.content = '' ;
                    if($scope.clean_content[j] && $scope.clean_content[j].match(/[a-z]/i))
                    {
                        $scope.content = $scope.clean_content[j];
                    }

                }

                           
                $scope.contentArr.push({
                                'image': $scope.clean_img[k],
                                'description': $scope.content
                            });
                                                    j++;
                                      
            }

            $('#magazineModal').modal('show');
        };





        $scope.show = function () {
            ModalService.showModal({
                templateUrl: 'modal.html',
                controller: "MagazineController"
            }).then(function (modal) {
                //console.log(modal);



                modal.scope.magazinemodalcontentimages = clean_img_html;
                modal.scope.magazinemodalcontent = clean_content;
                //alert($scope.magazinemodalcontent);
                modal.element.modal();
            });
        };

        // find post_content for magazine
        function objectFindByKey(array, key, value) {
            for (var i = 0; i < array.length; i++) {
                if (array[i][key] === value) {
                    return array[i];
                }
            }
            return null;
        }




    }]);
