(function(app) {
    app.controller('SearchCtrl', controller);

    controller.$inject = [
        '$rootScope',
        '$scope',
        '$location',
        '$http',
        '$routeParams',
        '$route',
        'location',
        'Search',
        'TitleAndMeta',
       //'deviceDetector',
      
    ];
    

    function controller(
        $rootScope,
        $scope,
        $location,
        $http,
        $routeParams,
        $route,
        location,
        Search,
        TitleAndMeta
        //deviceDetector
    ) {
        $scope.showSpinner = true;
        var Markers = [];
        //$scope.isMobile = deviceDetector.isMobile();
        $scope.showPagination = false;
        $scope.SearchSectionClasses = ['col-lg-8', 'col-md-8', 'col-sm-8', 'col-xs-12'];
        $scope.GoogleMapSectionClasses = ['col-lg-4', 'col-md-4', 'col-sm-4', 'hidden-xs'];
        $scope.RestaurantItemClasses = ['col-xl-6', 'col-lg-6', 'col-md-6', 'col-sm-12', 'col-xs-12'];
        $rootScope.page='search';
           //AuditLog.logevent(50,'');
        $scope.cities = [{
            id: 1,
            name: 'Singapore',
            data: 'singapore',
        }, {
            id: 2,
            name: 'Bangkok',
            data: 'bangkok',
        }, {
            id: 3,
            name: 'Phuket',
            data: 'phuket',
        }, {
            id: 4,
            name: 'Hong Kong',
            data: 'hong-kong',
        }];
        console.log("XSEARCH PRAMS " + $rootScope.SearchParams.city.data);
        // $rootScope.SearchParams was defined in /client/app/app.js line 112
        Search.getBestRestaurants($rootScope.SearchParams.city.data).then(function(response) {
            if (response.status == 1) {
                response.data[0].cuisine = response.data[0].cuisine.replace('|', ', ');
                $scope.restaurantShowCase = response.data[0];
            }
        });
        $scope.MapExpanded = false;
        $scope.pageNumbers = [];
        $scope.restaurantsPerPage = config.restaurantsPerPage;
        $scope.ExpandMap = false;
        if ($rootScope.SearchParams.page !== undefined && $rootScope.SearchParams.page.data !== undefined && $rootScope.SearchParams.page.data !== '') {
            $scope.page = parseInt($rootScope.SearchParams.page.data);
            if ($scope.page < 1) {
                //params.update($rootScope.SearchParams.page.param_key, 1);
            }
        } else {
            $scope.page = 1;
        }
        $rootScope.$on('ChangePageToOne', function() {
            $scope.setPage(1);
        });
        $scope.setPage = function(page) {
            var page_param_pattern = /(.*)\/page\/([0-9]*)(.*)/i;
            var page_param_matches = $location.$$path.match(page_param_pattern);
            var new_path;
            if (page_param_matches === null) {
                // new_path = $location.$$path + '/page/' + page;
                new_path = $location.$$path + '/' + $scope.country  + '/page/' + page;
            } else {
                if (page_param_matches[3] !== '') {
                    new_path = $location.$$path.replace(page_param_pattern, '$1/page/' + page + '/$3');
                } else {
                    new_path = $location.$$path.replace(page_param_pattern, '$1/page/' + page);
                }
            }
            var routeParamsCount = 0;
            for (var key in $routeParams) {
                routeParamsCount++;
            }
            if ($rootScope.SearchParams.page.param_key === null) {
                // new_path = $location.$$path + '/page/' + page;
                new_path = $location.$$path + '/' + $scope.country  + '/page/' + page;
                $rootScope.SearchParams.page = {
                    param_key: 'param_name_' + routeParamsCount,
                    data: page,
                };
            }
            location.skipReload().path(new_path);
            $scope.page = page;
            $scope.skip = ($scope.page - 1) * $scope.restaurantsPerPage;
            //Pagination();
            $("html, body").animate({
                scrollTop: 0
            }, 500);

            $rootScope.$broadcast('PageChanged', $scope.page);
        };

        $scope.skip = ($scope.page - 1) * $scope.restaurantsPerPage;

        console.log("SEARACH " +JSON.stringify($rootScope.SearchParams));

        Search.getRestaurant('both', $rootScope.SearchParams.city.data, $rootScope.SearchParams.cuisine.data, $rootScope.SearchParams.tags.data, $rootScope.SearchParams.area.data, $rootScope.SearchParams.pricing.data, $rootScope.SearchParams.query.data)
            .then(function(result) {
                if(result){
                    $scope.restaurants = result;
                    $scope.restaurantsInMap = result;
                    //Pagination();
                    if ($rootScope.SearchParams.query.data !== '' && result.length > 2) {
                        Search.insertSitemap($location.$$absUrl, result.length);
                    }
                    $scope.showSpinner = false;
                    $scope.showNoResultMessage = false;
                }
             
            })
            .catch(function(reason) {
                $scope.restaurants = [];
                $scope.restaurantsInMap = [];
                $scope.showNoResultMessage = true;
                $scope.showSpinner = false;
                Search.getRestaurant('both', $rootScope.SearchParams.city.data, null, null, null)
                    .then(function(result) {
                        $scope.restaurants = result;
                        $scope.restaurantsInMap = result;
                        //Pagination();
                        
                        $scope.showSpinner = false;
                    });
            });

        var cuisineListShow = [{
            cuisine: 'french',
        }, {
            cuisine: 'italian',
        }, {
            cuisine: 'international',
        }, {
            cuisine: 'asian',
        }, ];
        $http.get('api/cuisinelist', {
            cache: true,
        }).success(function(response) {
            if (response.status == 1) {
                $scope.cuisineList = {
                    cuisineListShow: cuisineListShow,
                    cuisineListCollapse: response.data.cuisine,
                };
                $rootScope.$broadcast('cuisineListLoaded', {
                    cuisineList: $scope.cuisineList
                });
            }
        });

        

        $scope.pricingArray = [{
            id: 1,
            name: 'all',
            data: '',
        }, {
            id: 2,
            name: '$',
            data: 1,
        }, {
            id: 3,
            name: '$$',
            data: 2,
        }, {
            id: 4,
            name: '$$$',
            data: 3,
        }, {
            id: 5,
            name: '$$$$',
            data: 4,
        }, ];

        // $rootScope.$on('google_map_dragend', function(evt, data) {
        //     $scope.restaurantsInMap = data.restaurantsInMap;
        //     Pagination();
        // });
        // $rootScope.$on('google_map_zoom_changed', function(evt, data) {
        //     $scope.restaurantsInMap = data.restaurantsInMap;
        //     Pagination();
        // });

        $scope.ExpandMap = function() {
            if ($scope.MapExpanded) {
                $scope.MapExpanded = false;
                $scope.SearchSectionClasses = ['col-lg-8', 'col-md-8', 'col-sm-8', 'col-xs-12'];
                $scope.GoogleMapSectionClasses = ['col-lg-4', 'col-md-4', 'col-sm-4', 'hidden-xs'];
                $scope.RestaurantItemClasses = ['col-lg-4', 'col-md-6', 'col-sm-6', 'col-xs-12'];
            } else {
                $scope.MapExpanded = true;
                $scope.SearchSectionClasses = ['col-lg-6', 'col-md-6', 'col-sm-6', 'col-xs-12'];
                $scope.GoogleMapSectionClasses = ['col-lg-6', 'col-md-6', 'col-sm-6', 'hidden-xs'];
                $scope.RestaurantItemClasses = ['col-lg-6', 'col-md-6', 'col-sm-6', 'col-xs-12'];
            }
            $rootScope.$broadcast('MapSizeChanged');
        };

        function Pagination() {
            $scope.pageNumbers = [];
            if ($scope.page == 1) {
                $scope.showPrevBtn = false;
            } else {
                $scope.showPrevBtn = true;
            }
            if ($scope.restaurantsInMap.length < $scope.restaurantsPerPage) {
                $scope.showPagination = false;
            } else {
                $scope.showPagination = true;
                var totalPages = Math.ceil($scope.restaurantsInMap.length / $scope.restaurantsPerPage);
                for (i = 1; i <= totalPages; i++) {
                    $scope.pageNumbers.push(i);
                }
                if ($scope.page == totalPages) {
                    $scope.showNextBtn = false;
                } else {
                    $scope.showNextBtn = true;
                }
            }
        }
    }
})(angular.module('SearchController', [
    'Map',
    'SearchService',
    'ExplodeCuisineFilter',
    'TitleAndMetaTag',
]));
