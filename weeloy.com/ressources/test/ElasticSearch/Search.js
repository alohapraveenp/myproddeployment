var app = angular.module('ElasticSearch', []);
app.controller('SearchController', function($scope, $http) {
	$scope.url = "https://search-search-weeloy-asia-xfqpzzz7f2q7ielntdtjbdauuq.ap-southeast-2.es.amazonaws.com";
	$scope.types = ["best_fields", "cross_fields", "most_fields", "phrase", "phrase_prefix"];
	$scope.tabletitle = ["No", "Region", "Title", "Cuisine", "Tags", "Menu", "Event", "Score"];
	$scope.query = "";
	$scope.country = "Singapore";
	$scope.city = "Singapore";
	$scope.region = "Tanjong Pagar";
	$scope.fields = "title,cuisine,tags,item_title";
	$scope.selectedType = "phrase_prefix";
	$scope.results = [];
	$scope.disable_search = false;
	$scope.orderByField = 'No';
	$scope.reverseSort = false;
	$scope.duration = 0;
	$scope.count = 0;
	$scope.processResults = function(result) {
		if (typeof result.data.hits.hits !== 'undefined' && result.data.hits.hits.length) {
			$scope.count += result.data.hits.hits.length;
			angular.forEach(result.data.hits.hits, function(value, key, obj) {
				//console.log("key: "+key+", value: "+JSON.stringify(value));
				if (value._index === "search.weeloy.asia" && typeof value._source !== 'undefined') {
					//console.log("key: "+key+", value: "+JSON.stringify(value));
					var result = {
						title: value._source.title,
						country: value._source.country,
						city: value._source.city,
						region: value._source.region,
						cuisine: value._source.cuisine,
						tags: value._source.tags,
						offer: value._source.offer,
						name: value._source.name,
						item_title: value._source.item_title,
						event: value._source.name,
						score: value._score
					}
					$scope.results.push(result);
				}
			});
			$scope.duration += result.data.took;
			var params = {
				"scroll": "10s",
				"scroll_id": result.data._scroll_id
			}
			$http.post($scope.url+'/_search/scroll', params).then($scope.processResults, 
			function errorCallback(response) {
				console.error("ES failed! error: "+JSON.stringify(response));
				$scope.disable_search = false;
			});
		} else {
			console.log("ES successfully returns "+$scope.count+"/"+result.data.hits.total+" results.");			
			$http.delete($scope.url+'/_search/scroll/_all').then(function deleteSuccess(response) {
				console.log("Successfully clear scroll API!");
			}, function deleteFail(response) {
				console.error("Failed to clear scroll API! "+JSON.stringify(response));
			});
			$scope.disable_search = false;
		}
	}
	$scope.search = function() {
		//console.log("type: "+$scope.selectedType+", query: "+$scope.query+", fields: "+$scope.fields);
		$scope.fields = $scope.fields.replace(/\s/g, '');
		var fieldArray = $scope.fields.split(','); // this will make string an array
		var params = {
			"size": 100,
		    "query": {
			    "bool": {
			    	"must": [
			    		{ "match": { "country": $scope.country } },
				    	{ "match": { "city": $scope.city } },
				        // { "match": { "is_displayed": 1 } },
				        // { "match": { "status": "active" } }
			        ]
				}
		    }
		};
		if ($scope.region.length)
			params.query.bool.must.push({
				"match": { "region": $scope.region}
			});
		if ($scope.query.length)
			params.query.bool.must.push({
				"query_string": {
					"query": $scope.query,
					"fields": fieldArray
				}
			})
		// if ($scope.query.length)
		// 	params.query.bool.must.push({
		// 		"multi_match": {
		// 		"query": $scope.query,
		// 		"type": $scope.selectedType,
		// 		"fields": fieldArray
		// 		}
		// 	});
		$scope.results = [];
		//};
		console.log("query: "+JSON.stringify(params));
		$scope.disable_search = true;
		// Send the request to ES
		$scope.duration = 0;
		$scope.count = 0;		
		$http.post($scope.url+'/_search?scroll=10s', params).then($scope.processResults, function errorCallback(response) {
			console.error("ES failed! error: "+JSON.stringify(response));
			$scope.disable_search = false;
		});
		// Process the return result set
		// Show it on the web page
	}
});