AddType 'text/html; charset=UTF-8' html
AddType application/x-font-woff2 .woff2
AddDefaultCharset UTF-8
AddCharset UTF-8 .htm .html .css .js .woff .woff2
AddType video/ogg .ogv
AddType video/mp4 .mp4
AddType video/webm .webm
AddType video/m4v .m4v

Options +FollowSymlinks  -Indexes
#CheckSpelling On

RewriteEngine on
RewriteBase /weeloy.com/








<IfModule mod_rewrite.c>
    RewriteEngine On

    <IfModule mod_proxy_http.c>
        RewriteCond %{HTTP_USER_AGENT} baiduspider|facebookexternalhit|twitterbot|rogerbot|linkedinbot|embedly|quora\ link\ preview|showyoubot|outbrain|pinterest|slackbot|vkShare|W3C_Validator [NC,OR]
        RewriteCond %{QUERY_STRING} _escaped_fragment_
        
        # Only proxy the request to Prerender if it's a request for HTML
        RewriteRule ^(?!.*?(\.js|\.css|\.xml|\.less|\.png|\.jpg|\.jpeg|\.gif|\.pdf|\.doc|\.txt|\.ico|\.rss|\.zip|\.mp3|\.rar|\.exe|\.wmv|\.doc|\.avi|\.ppt|\.mpg|\.mpeg|\.tif|\.wav|\.mov|\.psd|\.ai|\.xls|\.mp4|\.m4a|\.swf|\.dat|\.dmg|\.iso|\.flv|\.m4v|\.torrent|\.ttf|\.woff))(.*) http://service.prerender.io/https://dev.weeloy.com/$2 [P,L]
    </IfModule>
</IfModule>




RewriteRule ^restaurant/group/(.*)$ /weeloy.com/angular-client/index.php [L]




RewriteRule ^api/(.*)$			/weeloy.com/api/$1 [QSA]
RewriteRule ^tr/(.*)$                	/weeloy.com/tracking/index.php?cid=$1 [L,QSA]


RewriteRule ^$ /weeloy.com/angular-client/index.php
RewriteRule ^index.html$ andangular-client/index.php

#RewriteRule ^index.php$ /weeloy.com/angular-client/index.php

RewriteRule ^search$ /weeloy.com/angular-client/index.php [QSA]
RewriteRule ^search/(.*)$ /weeloy.com/angular-client/index.php [QSA]

RewriteRule ^restaurant/(.*)/(.*)/(.*)/book-now$ /weeloy.com/modules/booking/book_form.php?country=$1&city=$2&bktitle=$3 [QSA]
RewriteRule ^restaurant/(.*)/(.*)/(.*)/book-now/(.*)$ /weeloy.com/modules/booking/book_form.php?country=$1&city=$2&bktitle=$3&background=$4
RewriteRule ^restaurant/(.*)/(.*)/(.*)/book-now/(.*)/(.*)$ /weeloy.com/modules/booking/book_form.php?country=$1&city=$2&bktitle=$3&background=$4&panel_background=$5
RewriteRule ^restaurant/(.*)/(.*)/(.*)/section-booking$ /weeloy.com/modules/booking/section_booking.php?country=$1&city=$2&bktitle=$3

RewriteRule ^restaurant/(.*)/(.*)/book-now$ /weeloy.com/modules/booking/book_form.php?country=$1&city=$1&bktitle=$2 [QSA]
RewriteRule ^restaurant/(.*)/(.*)/book-now/(.*)/(.*)$ /weeloy.com/modules/booking/book_form.php?country=$1&city=$1&bktitle=$2&background=$3&panel_background=$4 [QSA]
RewriteRule ^restaurant/(.*)/(.*)/book-now/(.*)$ /weeloy.com/modules/booking/book_form.php?country=$1&city=$1&bktitle=$2&background=$3
RewriteRule ^restaurant/(.*)/(.*)/section-booking$ /weeloy.com/modules/booking/section_booking.php?country=$1&city=$1&bktitle=$2

RewriteRule ^restaurant/(.*)/(.*)/event/book-now$ /weeloy.com/modules/booking/book_form.php?country=$1&city=$1&bktitle=$2&type=event
RewriteRule ^restaurant/(.*)/(.*)/event/section-booking$ /weeloy.com/modules/booking/section_booking.php?country=$1&city=$1&bktitle=$2&type=event


RewriteRule ^restaurant/(.*)$ /weeloy.com/angular-client/index.php
RewriteRule ^singapore/location/(.*)$ /weeloy.com/angular-client/index.php
RewriteRule ^singapore/favorite_cuisine/(.*)$ /weeloy.com/angular-client/index.php
RewriteRule ^restaurant/group/(.*)$ /weeloy.com/angular-client/index.php
RewriteRule ^dining-rewards/(.*)$ /weeloy.com/angular-client/index.php [QSA]
RewriteRule ^mybookings$ /weeloy.com/angular-client/index.php
RewriteRule ^myorders$ /weeloy.com/angular-client/index.php
RewriteRule ^myaccount$ /weeloy.com/angular-client/index.php
RewriteRule ^myreviews$ /weeloy.com/angular-client/index.php
RewriteRule ^catering$ /weeloy.com/angular-client/index.php
RewriteRule ^checkout$ /weeloy.com/angular-client/index.php
RewriteRule ^payment-success$ /weeloy.com/angular-client/index.php
RewriteRule ^checkout/info$ /weeloy.com/angular-client/index.php
RewriteRule ^contact$ /weeloy.com/angular-client/index.php
RewriteRule ^partner$ /weeloy.com/angular-client/index.php
RewriteRule ^how-it-works$ /weeloy.com/angular-client/index.php
RewriteRule ^faq$ /weeloy.com/angular-client/index.php
RewriteRule ^terms-and-conditions-of-service$ /weeloy.com/angular-client/index.php
RewriteRule ^privacy-policy$ /weeloy.com/angular-client/index.php
RewriteRule ^download-mobile-app$ /weeloy.com/angular-client/index.php

RewriteRule ^dine-and-win-contest-2016$ /angular-client/index.php

RewriteRule ^section-booking /modules/booking/section_booking.php



RewriteRule ^404$ /weeloy.com/angular-client/index.php

RewriteRule ^index.php(.*) 404$ [L,R=301]


######### RESTAURANTS.SG  #########

RewriteCond %{HTTP_HOST} ^restaurants.sg$ [NC]
RewriteRule ^(.*)$ https://www.restaurants.sg/$1 [R=301,L]

RewriteCond %{SCRIPT_FILENAME} !\/robots\.txt [NC]
RewriteCond %{HTTP:X-Forwarded-Proto} =http [NC]
RewriteCond %{SERVER_PORT} ^80$ [NC]
RewriteRule ^(.*) https://www.restaurants.sg/$1 [L,R=301]

RewriteCond %{HTTP_HOST} ^(www.)?restaurants.sg$ [NC]
RewriteCond %{REQUEST_URI} !\/modules\/ [NC]
RewriteCond %{REQUEST_URI} !\/bower_components\/ [NC]
RewriteCond %{REQUEST_URI} !\/css\/ [NC]
RewriteCond %{REQUEST_URI} !\/js\/ [NC]
RewriteCond %{REQUEST_URI} !\/fonts\/ [NC]
RewriteRule ^(.*)$ modules/index.php [L]

######### RESTAURANTS.SG  #########



#Redirect 301 /weeloy.com/index_user.php /weeloy.com/index.php

RewriteRule ^my(.*)$        		/weeloy.com/index.php?page=my$1 [L]
RewriteRule ^info-(.*)$			/weeloy.com/info.php?page=$1 [L]
RewriteRule ^how-it-works$		/weeloy.com/index.php?page=how-it-works [L]
RewriteRule ^cancel_booking$		/weeloy.com/index.php?page=cancel_booking&b=$1&rd=1 [QSA]

RewriteRule ^report-HJdsLfhau7t32egdaPaqavbsaklgvdwaysxf$		/weeloy.com/modules/reports/reworld-media/reports.php?page=gourmand [L]
RewriteRule ^report-saJds8dsaau7gr4egDfagDvfbgdsaLmdskII$		/weeloy.com/modules/reports/reworld-media/reports.php?page=marie-france [L]
RewriteRule ^bookingdetails$                                            /weeloy.com/modules/booking/bookingdetails.php?page=bookingdetails&b=$1&rd=1 [QSA]
RewriteRule ^passbook/(.*)$                                             /weeloy.com/modules/passbook/generator.php?b=$1 [QSA]

RewriteRule ^sitemap$                           /weeloy.com//modules/sitemap/generator.php
RewriteRule ^book-restaurants-online$ /weeloy.com/modules/restaurant/bookonline.php


ErrorDocument 404 /weeloy.com/angular-client/index.php


RewriteRule ^img/upload/([0-9a-zA-Z-]+)/(.*)/([0-9a-zA-Z-]+)/(.*)$                        /weeloy.com/ressources/img_dynamic_generation/index.php?type=$1&restaurant_id=$2&size=$3&img=$4 [QSA]
RewriteRule ^img/upload/([0-9a-zA-Z-]+)/(.*)/(.*)$                        /weeloy.com/ressources/img_dynamic_generation/index.php?type=$1&restaurant_id=$2&size=500&img=$3 [QSA]


#RewriteRule ^search/restaurant/js/(.*)$			/weeloy.com/js/$1 [L]
#RewriteRule ^search/restaurant/css/(.*)$			/weeloy.com/css/$1 [L]
#RewriteRule ^search/restaurant/fonts/(.*)$			/weeloy.com/fonts/$1 [L]
#RewriteRule ^search/restaurant/images/(.*)$			/weeloy.com/images/$1 [L]
#RewriteRule ^search/restaurant/modules/booking/(.*)$			/weeloy.com/modules/booking/$1 [L]

#RewriteRule ^search/restaurant/([0-9a-zA-Z-]+)$                        /weeloy.com/index.php?page=search_restaurant&city=$1 [QSA]
#RewriteRule ^search/restaurant/([0-9a-zA-Z-]+)/([0-9a-zA-Z-]+)$                        /weeloy.com/index.php?page=search_restaurant?city=$1&restaurant=$2 [QSA]


#RewriteRule ^restaurant/(.*)/js/(.*)$			/weeloy.com/js/$2 [L]
#RewriteRule ^restaurant/(.*)/css/(.*)$			/weeloy.com/css/$2 [L]
#RewriteRule ^restaurant/(.*)/fonts/(.*)$			/weeloy.com/fonts/$2 [L]
#RewriteRule ^restaurant/(.*)/images/(.*)$			/weeloy.com/images/$2 [L]
#RewriteRule ^restaurant/(.*)/modules/booking/(.*)$			/weeloy.com/modules/booking/$2 [L]
#RewriteRule ^restaurant/(.*)/wheel/rotation/(.*)$			/weeloy.com/wheel/rotation/$2 [L]


#RewriteRule ^restaurant/(.*)_(.*)_(.*)_(.*)$		/weeloy.com/index.php?page=front_restaurant&restaurantname=$1_$2_$3_$4 [L]
#RewriteRule ^restaurant/([0-9a-zA-Z-]+)/([0-9a-zA-Z-]+)$                        /weeloy.com/index.php?page=front_restaurant&country=$1&city=$1&restaurantname=$2 [QSA]
#RewriteRule ^restaurant/([0-9a-zA-Z-]+)/([0-9a-zA-Z-]+)/wheel-details$                        /weeloy.com/index.php?page=wheel_details&country=$1&city=$1&restaurantname=$2 [QSA]


#RewriteRule ^restaurant/([0-9a-zA-Z-]+)/([0-9a-zA-Z-]+)/([0-9a-zA-Z-]+)$        /weeloy.com/index.php?page=front_restaurant&country=$1&city=$2&restaurantname=$3 [QSA]
#RewriteRule ^restaurant/([0-9a-zA-Z-]+)/([0-9a-zA-Z-]+)/([0-9a-zA-Z-]+)/wheel-details$        /weeloy.com/index.php?page=wheel_details&country=$1&city=$2&restaurantname=$3 [QSA]



# BEGIN Compress text files
<IfModule mod_deflate.c>
  <FilesMatch "\.(js\.map|css|js|x?html?|php|webm)$”>
    SetOutputFilter DEFLATE
  </FilesMatch>
</IfModule>
# END Compress text files

# BEGIN Expire headers
<IfModule mod_expires.c>
  ExpiresActive On
  ExpiresDefault "access plus 1 seconds"
  ExpiresByType image/x-icon "access plus 2592000 seconds"
  ExpiresByType video/m4v "access plus 2592000 seconds"
  ExpiresByType video/webm "access plus 2592000 seconds"
  ExpiresByType image/jpeg "access plus 2592000 seconds"
  ExpiresByType image/svg+xml "access plus 2592000 seconds"
  ExpiresByType image/png "access plus 2592000 seconds"
  ExpiresByType image/gif "access plus 2592000 seconds"
  ExpiresByType application/x-shockwave-flash "access plus 2592000 seconds"
  ExpiresByType application/font-woff "access plus 2592000 seconds"
  ExpiresByType application/font-sfnf "access plus 2592000 seconds"
  ExpiresByType text/css "access plus 604800 seconds"
  ExpiresByType text/javascript "access plus 216000 seconds"
  ExpiresByType application/javascript "access plus 216000 seconds"
  ExpiresByType application/x-javascript "access plus 216000 seconds"
  ExpiresByType text/plain "access plus 1 seconds"
  ExpiresByType text/html "access plus 1 seconds"
  ExpiresByType application/xhtml+xml "access plus 1 seconds"
</IfModule>
# END Expire headers

# BEGIN Cache-Control Headers
<IfModule mod_headers.c>
  <FilesMatch "\.(ico|jpe?g|png|gif|swf|svg)$">
    Header set Cache-Control "max-age=2592000, public"
  </FilesMatch>
  <FilesMatch "\.(mp4|m4v|webm|ogv)$">
    Header set Cache-Control "max-age=2592000, public"
    Header set Range-Control "bytes=0-9999999"
  </FilesMatch>
  <FilesMatch "\.(css)$">
    Header set Cache-Control "max-age=604800, public"
  </FilesMatch>
   <FilesMatch "\.(woff)$">
    Header set Cache-Control "max-age=604800, public"
  </FilesMatch>
   <FilesMatch "\.(ttf)$">
    Header set Cache-Control "max-age=604800, public"
  </FilesMatch>
  <FilesMatch "\.(js)$">
    Header set Cache-Control "max-age=216000, private"
  </FilesMatch>
   <FilesMatch "\.(js\.map)$">
    Header set Cache-Control "max-age=216000, private"
  </FilesMatch>
  <FilesMatch "\.(x?html?|php)$">
    Header set Cache-Control "max-age=1, private, must-revalidate"
  </FilesMatch>
</IfModule>
# END Cache-Control Headers

<FilesMatch ".(eot|ttf|otf|woff)">
	Header set Access-Control-Allow-Origin "*"
</FilesMatch>

# BEGIN Turn ETags Off
<IfModule mod_headers.c>
  Header unset ETag
</IfModule>
FileETag None
# END Turn ETags Off

# BEGIN Remove Last-Modified Header
<IfModule mod_headers.c>
  Header unset Last-Modified
    <FilesMatch "\.(jpg|jpeg|png|svg)$">
        Header set Last-Modified "Mon, 31 Aug 2009 00:00:00 GMT"
    </FilesMatch>
</IfModule>
# END Remove Last-Modified Header


<IfModule mod_headers.c>
    RequestHeader set X-Prerender-Token "lfxpkWey7OrrGKugx2QB"
</IfModule>

