#!/bin/bash

# Based on https://gist.github.com/2206527

# Be pretty
echo -e " "
echo -e " .  ____  .    ______________________________"
echo -e " |/      \|   |                              |"
echo -e "[| \e[1;31m♥    ♥\e[00m |]  | S3 MySQL Backup Script v1 |"
echo -e " |___==___|  /                © gaolinch 2016 |"
echo -e "              |______________________________|"
echo -e " "

# Basic variables
mysqlhost="weeloylarge.cweyuhtatlxl.ap-southeast-1.rds.amazonaws.com"
mysqluser="weeloy_admin"
mysqlpass="Hd%6sx?3"
bucket="s3://weeloy-mysql-backup"

# 0 -> unlimited / -1 -> don't backup
backup_days=31
backup_weeks=8
backup_months=0

# Timestamp (sortable≈ AND readable)
stamp=`date +"%s - %A %d %B %Y @ %H%M"`
weekday=`date +"%A"`
day=`date +"%d"`

# List all the databases
databases=`mysql -u $mysqluser -p$mysqlpass -h $mysqlhost -e "SHOW DATABASES;" | tr -d "| " | grep -v "\(Database\|information_schema\|performance_schema\|mysql\|test\)"`

# Excluded database - no backup for these databases
excluded_database='dadssadasd innodb tmp weeloy8635281_blogdemo weeloy88_business_dev weeloy88_dwh_dev weeloy88_session_dev'


# Feedback
echo -e "Dumping to \e[1;32m$bucket/$stamp/\e[00m"


# Loop the databases
for db in $databases; do
  excluded=0
  #check exclusions
  for e_db in $excluded_database; do
   if [ "$db" == "$e_db" ]
   then
     echo 'excluded '$db 
     excluded=1
   fi
 done;
 if [ $excluded -eq 0 ]
 then
  # Define our filenames
    filename="$stamp - $db.sql.gz"
    tmpfile="/tmp/$filename"

    daily_folder="$bucket/daily/"
    weekly_folder="$bucket/weekly/"
    monthly_folder="$bucket/monthly/"

    object_daily="$daily_folder$stamp/$filename"    
    object_weekly="$weekly_folder$stamp/$filename"
    object_monthly="$monthly_folder$stamp/$filename"

  # Feedback
  echo -e "\e[1;34m$db\e[00m"

  # Dump and zip
  echo -e "  creating \e[0;35m$tmpfile\e[00m"
  mysqldump -u $mysqluser -p$mysqlpass -h $mysqlhost --force --opt --databases "$db" | gzip -c > "$tmpfile"

  # Upload daily
  if [ $backup_days -gt -1 ]
  then
    echo -e "  uploading daily..."
    aws s3 cp "$tmpfile" "$object_daily"
  fi

  # Upload weekly
  if [ "$weekday" == "Sunday" ] && [ $backup_weeks -gt -1 ] 
  then
    echo -e "  uploading weekly..."
    aws s3 cp "$tmpfile" "$object_weekly"
  fi


  # Upload monthly
  if [ "$day" == "01" ]  && [ $backup_months -gt -1 ] 
  then
    echo -e "  uploading monthly..."
    aws s3 cp "$tmpfile" "$object_monthly"
  fi

  # Delete TMP FILE
  rm -f "$tmpfile"
fi
done;


###  clean up job - daily
i=0
nb_days=$(aws s3 ls  $daily_folder | wc -l)
if [ "$nb_days" -gt "$backup_days" ] && [ $backup_days -gt 0 ]
then
nb_remove=$(($nb_days-$backup_days))
aws s3 ls  $daily_folder | while read -r line;
  do
    if [ $nb_remove -gt $i  ] && [ $backup_days -gt 0 ]
    then
        folder_name="${line/PRE /}"
        folder_name="${folder_name//+/ }"
        folder_name="${folder_name//%40/@}"
        folder_path=$daily_folder$folder_name
        aws s3 rm --recursive "$folder_path"
    fi
  i=$((i+1))
  done;
echo 'daily backups deleted'
fi

###  clean up job - weekly
i=0
nb_days=$(aws s3 ls  $weekly_folder | wc -l)
if [ "$nb_days" -gt "$backup_weeks" ]  && [ $backup_weeks -gt 0 ]
then
nb_remove=$(($nb_days-$backup_weeks))

aws s3 ls  $weekly_folder | while read -r line;
  do
    if [ $nb_remove -gt $i  ]
    then
        folder_name="${line/PRE /}"
        folder_name="${folder_name//+/ }"
        folder_name="${folder_name//%40/@}"
        folder_path=$weekly_folder$folder_name
        aws s3 rm --recursive "$folder_path"
    fi
  i=$((i+1))
  done;
echo 'weekly backups deleted'
fi

###  clean up job - monthly
i=0
nb_days=$(aws s3 ls  $monthly_folder | wc -l)
if [ "$nb_days" -gt "$backup_months" ] && [ $backup_months -gt 0 ]
then
nb_remove=$(($nb_days-$backup_months))

aws s3 ls  $monthly_folder | while read -r line;
  do
    if [ $nb_remove -gt $i  ]
    then
        folder_name="${line/PRE /}"
        folder_name="${folder_name//+/ }"
        folder_name="${folder_name//%40/@}"
        folder_path=$monthly_folder$folder_name
        aws s3 rm --recursive "$folder_path"
    fi
  i=$((i+1))
  done;
echo 'monthly backups deleted'
fi

# Jobs a goodun
echo -e "\e[1;32mJobs a goodun\e[00m"

