<?php
namespace Task;

use Mage\Task\AbstractTask;

class PrepareConfig extends AbstractTask
{
public function getName()
{
return 'Deploy Config prod';
}

public function run()
{

$command = 'rm -rf conf/conf.mysql.inc.php';
$result = $this->runCommandRemote($command);

$command = 'mv conf/conf.mysql_prod.inc.php conf/conf.mysql.inc.php';
$result = $this->runCommandRemote($command);

$command = 'rm -rf conf/conf.init.inc.php';
$result = $this->runCommandRemote($command);

$command = 'mv conf/conf.init_prod.inc.php conf/conf.init.inc.php';
$result = $this->runCommandRemote($command);



$command = 'rm -rf .server/php/php-5.5_dev.ini';
$result = $this->runCommandRemote($command);

$command = 'rm -rf .server/php/php-5.5_prod_bean.ini';
$result = $this->runCommandRemote($command);


$command = 'rm -rf /var/www/vhosts/weeloy.com/PHP_LIB';
$result = $this->runCommandRemote($command);

$command = 'mkdir -p /var/www/vhosts/weeloy.com/PHP_LIB/';
$result = $this->runCommandRemote($command);


$command = 'mkdir -p /var/www/vhosts/weeloy.com/PHP_LIB/lib';
$result = $this->runCommandRemote($command);


$command = 'mkdir -p /var/www/vhosts/weeloy.com/PHP_LIB/conf';
$result = $this->runCommandRemote($command);


$command = 'cp -r lib/* /var/www/vhosts/weeloy.com/PHP_LIB/lib';
$result = $this->runCommandRemote($command);

$command = 'rm -rf lib';
$result = $this->runCommandRemote($command);

$command = 'cp -r conf/* /var/www/vhosts/weeloy.com/PHP_LIB/conf';
$result = $this->runCommandRemote($command);

$command = 'rm -rf conf';
$result = $this->runCommandRemote($command);

$command = 'chmod 777 tmp/';
$result = $this->runCommandRemote($command);

$command = 'mkdir -p tmp/twig_cache';
$result = $this->runCommandRemote($command);

$command = 'chmod 777 tmp/twig_cache';
$result = $this->runCommandRemote($command);

$command = 'rm -rf .htaccess';
$result = $this->runCommandRemote($command);

$command = 'rm -rf .htaccess_prod_bean';
$result = $this->runCommandRemote($command);

$command = 'rm -rf .htaccess_dev';
$result = $this->runCommandRemote($command);

$command = 'mv .htaccess_prod .htaccess';
$result = $this->runCommandRemote($command);

$command = 'rm -rf tracking/.htaccess';
$result = $this->runCommandRemote($command);

$command = 'rm -rf tracking/.htaccess_dev';
$result = $this->runCommandRemote($command);

$command = 'rm -rf tracking/.htaccess_prod_bean';
$result = $this->runCommandRemote($command);

$command = 'mv tracking/.htaccess_prod tracking/.htaccess';
$result = $this->runCommandRemote($command);


return $result;
}
}

