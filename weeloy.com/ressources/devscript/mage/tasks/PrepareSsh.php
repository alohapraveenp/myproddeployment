<?php
namespace Task;

use Mage\Task\AbstractTask;

class PrepareSsh extends AbstractTask
{
public function getName()
{
return 'Fixing file permissions';
}

public function run()
{
$command = 'eval `ssh-agent`';
$result = $this->runCommandRemote($command);
$command = 'ssh-add';
$result = $this->runCommandRemote($command);
return $result;
}
}

