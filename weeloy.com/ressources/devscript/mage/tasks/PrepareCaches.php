<?php
namespace Task;

use Mage\Task\AbstractTask;

class PrepareCaches extends AbstractTask
{
public function getName()
{
return 'Create Cache';
}

public function run()
{

$command = 'mkdir -p cache';
$result = $this->runCommandRemote($command);

$command = 'chmod 777 cache';
$result = $this->runCommandRemote($command);

$command = 'mkdir -p templates_c';
$result = $this->runCommandRemote($command);

$command = 'chmod 777 templates_c';
$result = $this->runCommandRemote($command);

return $result;
}
}

