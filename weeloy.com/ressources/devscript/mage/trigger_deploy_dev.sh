#!/bin/bash

if [ -f /var/jenkins/sem/deploy_dev_weeloy.event ]
then
    rm /var/jenkins/sem/deploy_dev_weeloy.event
    echo found trigger, starting deploy
    cd ~/weeloy.com
    git checkout dev -f
    mage deploy to:dev_weeloy
    cd -
    touch /var/jenkins/sem/deploy_dev_weeloy.finished
    chmod 777 /var/jenkins/sem/deploy_dev_weeloy.finished
fi

#if [ -f /var/jenkins/sem/deploy_sandbox_master.event ]
#then
#    rm /var/jenkins/sem/deploy_sandbox_master.event
#    echo found trigger, starting deploy
#    cd ~/eatem-web
#    git checkout master -f
#    mage deploy to:sandbox_dev
#    cd -
#    touch /var/jenkins/sem/deploy_sandbox_master.finished
#    chmod 777 /var/jenkins/sem/deploy_sandbox_master.finished
#fi

