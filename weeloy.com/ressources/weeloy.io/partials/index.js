app.config(function($routeProvider) {
    $routeProvider
    .when("/", {
    	templateUrl : "partialsmin/home.htm"
    })
    .when("/home", {
        templateUrl : "partialsmin/home.htm"
   })
    .when("/aboutus", {
        templateUrl : "partialsmin/aboutus.htm"
    })
    .when("/contact", {
        templateUrl : "partialsmin/contact.htm"
    })    
    .when("/newsletter", {
        templateUrl : "partialsmin/newsletter.htm"
    })
    .when("/termsandcond", {
        templateUrl : "partialsmin/termsandcond.htm"
    })
    .when("/policy", {
        templateUrl : "partialsmin/policy.htm"
    })
    .otherwise({
    	templateUrl : "partialsmin/main.htm"
    });
    // return $locationProvider.html5Mode(true);
});

app.config(function ($mdThemingProvider) {
        $mdThemingProvider
            .theme("default")
            .primaryPalette("cyan")
            .accentPalette("light-green");
        $mdThemingProvider.theme('dark-pink').primaryPalette('yellow').backgroundPalette('pink');
        $mdThemingProvider.theme('dark-orange').backgroundPalette('orange').dark();
        $mdThemingProvider.theme('dark-red').primaryPalette('red').dark();
        $mdThemingProvider.theme('dark-blue').backgroundPalette('blue').dark();

        $mdThemingProvider.theme('customTheme')
            .primaryPalette('blue')
            .accentPalette('orange')
            //	  .backgroundPalette('blue')
            .warnPalette('purple');
        $mdThemingProvider.theme('default')
            .primaryPalette('purple', {
                'default': '400', // by default use shade 400 from the pink palette for primary intentions
                'hue-1': '100', // use shade 100 for the <code>md-hue-1</code> class
                'hue-2': '600', // use shade 600 for the <code>md-hue-2</code> class
                'hue-3': 'A100' // use shade A100 for the <code>md-hue-3</code> class
            })
            // If you specify less than all of the keys, it will inherit from the
            // default shades
            .accentPalette('blue', {
                'default': '200' // use shade 200 for default, and keep all other shades the same
            });

        $mdThemingProvider.definePalette('amazingPaletteName', {
            '50': 'ffebee',
            '100': 'ffcdd2',
            '200': 'ef9a9a',
            '300': 'e57373',
            '400': 'ef5350',
            '500': 'f44336',
            '600': 'e53935',
            '700': 'd32f2f',
            '800': 'c62828',
            '900': 'b71c1c',
            'A100': 'ff8a80',
            'A200': 'ff5252',
            'A400': 'ff1744',
            'A700': 'd50000',
            'contrastDefaultColor': 'light', // whether, by default, text (contrast)
            // on this palette should be dark or light
            'contrastDarkColors': ['50', '100', //hues which contrast should be 'dark' by default
                '200', '300', '400', 'A100'],
            'contrastLightColors': undefined    // could also specify this if default was 'dark'
        });
        $mdThemingProvider.theme('partialsing')
            .primaryPalette('amazingPaletteName');

    });
app.config(function ($mdIconProvider) {
        $mdIconProvider
            .iconSet('social', '../client/bower_components/material-design-icons/iconsets/social-icons.svg', 24)
            //.defaultIconSet('img/icons/sets/core-icons.svg', 24)
            .fontSet('md', 'material-icons');
    });
    
app.controller('myController', ['$scope', '$http', '$filter', '$timeout', '$location', function($scope, $http, $filter, $timeout, $location) {
	var index= 0, page = 0;
	$scope.pagetitle = '';
	$scope.showview = 1;
	$scope.captcha = false;
	$scope.captchaResponse = "";
	$scope.user = { name: "", email:"", subject:"", comment:"", newsletter:false };
	$scope.request = "";
		
	$scope.webtemplate = [ "home", "aboutus", "newsletter", "contact" ];
	$scope.pagelabel = ["Home", "Aboutus", "Contact", "Newsletter"];

	$scope.checking = function(o, l, content) {
		  if(typeof o !== "string" || o.length < l) {
		  	alert("Please, enter "+content);
		  	return false;
		  	}
		  return true;
		};

	$scope.emailvalidation = function(email) {
		var mailformat = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
		// console.log('VALID', email, email.match(mailformat));
		return (!email.match(mailformat)) ? false : true;
		}
		
	$scope.onsubmit = function(what) {

		var captchaResponse, u = $scope.user;
				// console.log('onsubmit')
		if($scope.request !== "") {
			alert("your request is being processed or has been processed");
			return false;
			}


			
		if($scope.captcha === false) {
			$scope.captchaResponse = grecaptcha.getResponse();
			if($scope.captchaResponse === "") {
				alert("Are you 'robot'!?");
				return false;
				} 
			}


		$scope.captcha = true;
		// console.log('USER', u);
		if($scope.checking(u.name, 5, 'your name') !== true) 
			return false;
		if($scope.checking(u.email, 8, 'your email') !== true)
			return false;
		if($scope.emailvalidation(u.email) !== true) {
			alert("Enter a valid email");
			return false;
			}
			
		if(what === 'c') {
			if($scope.checking(u.subject, 5, 'a subject') !== true) 
				return false;
			if($scope.checking(u.comment, 8, 'the content of your request') !== true) 
				return false;
		} else { 
			u.subject = "newsletter"; 
			u.comment = ""; 
			}

		//for newsletter
		if(u.newsletter == true)
		{
			u.subject = u.subject + " with newsletter"; 
			u.comment = u.comment + " Please add me into your Newsletter."; 
		}

		// console.log('POST', window.location.href + "checkemail.php");
		$scope.request = $http({
			method: "post",
			url: "http://dev.weeloy.io/checkemail.php",
			data: { name: u.name, email: u.email, subject: u.subject, comment: u.comment, captcha: $scope.captchaResponse },
			headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
			});
		
		$scope.request.success(function (status) { 
			if(typeof status === "string" && status.length > 1 && status.indexOf('success') > -1)
				alert("You request has been sent to us"); 
			else {
				alert("You request has NOT been sent to us for the following reasons: "+status); 
				$scope.request = "";
				}
			});
		alert("Please wait as we are processing your request"); 
		return true;
	};
		
	$('#collapseEx').click(function(e) {
		$('#collapseEx').collapse('hide');
		// console.log('HIDE');
		});
	
	$scope.$on("$routeChangeStart", function (event, next, current) {
		$.getScript("https://www.google.com/recaptcha/api.js");	//?hl=fr
  //       $.getScript("http://maps.googleapis.com/maps/api/js?key=AIzaSyAUxfo6JhK218S18CPYWHMypgt4TiRU7gg");
		$scope.captcha = false;
		$scope.request = "";
		$scope.captchaResponse = "";
		$scope.user = { name: "", email:"", subject:"", comment:"" };
		$scope.pagetitle = "";
		$scope.showview = (next.templateUrl && next.templateUrl.indexOf("main") >= 0) ? 1 : 0;   
		$scope.webtemplate.map(function(ll, index) {
			if(next.templateUrl) {
				var k = next.templateUrl.indexOf(ll);
				 if(k  >= 0) {
					 $scope.pagetitle = $scope.pagelabel[index];
					  // console.log('TITLE', $scope.pagelabel[index], k, next.templateUrl);
					 }
				}
			});    			   
		$('#collapseEx').click(function(e) {
			$('#collapseEx').collapse('hide');
			// console.log('HIDE');
			});
                        
                       //?hl=fr
               //google map
                    function initMap() {
                        //console.log(document.getElementById("map-container").attr('class'));
                        var var_location = new google.maps.LatLng(1.2748708,103.8417682);
                        var var_mapoptions = {
                            center: var_location,

                            zoom: 14
                        };
                        var var_marker = new google.maps.Marker({
                            position: var_location,
                            map: var_map,
                            title: "Singapore"
                        });
                        var var_map = new google.maps.Map($("#map-container")[0],var_mapoptions);
                            
                        var_marker.setMap(var_map);
                    }
            		if(next.templateUrl == 'partials/contact.htm')
						{
							$.getScript("http://maps.googleapis.com/maps/api/js?key=AIzaSyAUxfo6JhK218S18CPYWHMypgt4TiRU7gg");
							setTimeout(function() {
                    		initMap();
                		},3000)
					}            
                    // if(next.templateUrl.indexOf("contact.htm")){
                        
                    //      $(document ).ready(function() {
                    //         initMap();
                    //       });
      
                    // }

                    //google.maps.event.addDomListener(window, 'load', initMap);
                    // console.log('ROUTE', next.templateUrl + "SDsads" + next.templateUrl.indexOf("contact.htm"));
                   
   	 });

	$scope.isRouteActive = function(route) { 
		if(route === "home") {
			var tmp = $location.path().replace(/^.*index.html/, "");
			return (tmp.length < 4 || tmp === "/main");
			}  

		return ($location.path().match(route)) ? true : false;
	};
		
	$scope.even = function(x) {
		return ((x.index % 2) !== 0);
		};

	$scope.registernewresto = function()
        {
          $('#dbsformmodalregistration').modal('show');
        };

}]);
