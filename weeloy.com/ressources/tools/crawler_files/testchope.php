<?php


echo 'CHOPE PARSER LOADED'.'<br/>';

$booker = "CHOPE";

function parse_chope($msgorg, $account) {
	$resultAr = array();
	$etape = 0;

	$msgorg = preg_replace("/=0D|=0A/", "\r", $msgorg);
	$msgorg = preg_replace("/=(\r|\n)+/", "", $msgorg);
		
	if(isset($_REQUEST["DEBUG"]) && $_REQUEST["DEBUG"] == "73")
		echo $msgorg . "<hr />";

	$msgorg = preg_replace("/\"|\'/", " ", $msgorg);
	$msgorg = preg_replace("/(\r|\n)+/", "|", $msgorg);
	$msgorg = preg_replace("/\s+/", " ", $msgorg);
	$msgorg = preg_replace("/\=20\|/", "|", $msgorg);
	$msgorg = preg_replace("/\|\|+/", "|", $msgorg);
	$msgorg = preg_replace("/.*Content-Type: text\/plain;(.*)Content-Type: text\/html;.*/", "$1", $msgorg);
	$msgorg = preg_replace("/.*Content-Type: text\/plain/", "", $msgorg);
	
	// reduce the search to plain text
	//$found = preg_match("/Content-Type: text\/plain;((?(?!Content-Type).)*)Content-Type.*/", $msgorg, $match);

	$msg = $msgorg;

	if(isset($_REQUEST["DEBUG"]) && $_REQUEST["DEBUG"] == "73")
		echo $msg . "<hr />";
	
	if(preg_match("/Content-Transfer-Encoding:\s+base64/", 	$msg, $match)) {
		echo "ENCODE 64 ";
		$data = explode("|", $msg);
		for($i = 0; $i < count($data); $i++) {
			$tmp = base64_decode($data[$i], true);
			if($tmp !== false && preg_match("/Reservation/", $tmp, $dummy)) {
				if($i > 0)
					$msg = preg_replace("/.*" . $data[$i] . "/", $data[$i], $msg);
				$msg = base64_decode ( $msg, false);				
				$msg = preg_replace("/\r|\n/", "|", $msg);
			    $msg = preg_replace("/\|\|+/", "|", $msg);
				break;
				}
			}
		echo $msg . "<hr />";
		}
		
	// search for mode	
	$keyword = array("Cancelled Reservation" => "cancellation", "Edited Reservation" => "modification", "Updated Reservation" => "modification", "New Reservation" => "booking");
	foreach($keyword as $label => $value) {
		if(preg_match("/$label/", $msg)) {
			$resultAr["processing"] = $value;
			break;
			}
		}

	// search for information	
	$keyword = array("Restaurant" => "restaurant", "Date" => "rdate", "Time" => "rtime", "Name" => "fullname", "Covers" => "cover", "Confirmation Number" => "confirmation", "Mobile" => "mobile", "Email" => "email", "Notes" => "specialrequest");
	foreach($keyword as $label => $value) {
	
		if($flgeneric) $pat = "/\|\s*" . $label . "[|]+([^|]*)/";	
		else $pat = "/\|\s*" . $label . ":?[|]*([^|]*)/";
		
		$resultAr[$value] = "";
		if(preg_match($pat, $msg, $match))
			$resultAr[$value] = (count($match) > 1) ? $match[1] : "";
		if($value == "specialrequest" && !empty($resultAr[$value])) {
	        $resultAr["specialrequest"] = preg_replace("/\(changed from.*/", "", $resultAr["specialrequest"]);
			$resultAr["specialrequest"] = str_replace("&nbsp;", " ", $resultAr["specialrequest"]);
			}
		}

	if(preg_match("/adult[^\d]+(\d)\s*child/", $msg, $match)) {
		$resultAr['children'] = (count($match) > 1) ? $match[1] : "";
		$a = intval($resultAr['cover']);
		$b = intval($resultAr['children']);
		if($a > 0 && $b > 0) {
			$resultAr["specialrequest"] = "$a adults and $b children. " . $resultAr["specialrequest"];
			$resultAr['cover'] = $a + $b;
			}
		}

	if(isset($resultAr["restaurant"])) {
		$resultAr["restaurant"] = preg_replace("/,.*/", "", $resultAr["restaurant"]);
		}

	if(!empty($resultAr["rdate"])) {
        $resultAr["rdate"] = preg_replace("/\(.*/", "", $resultAr["rdate"]);
        $resultAr["rdate"] = date("Y-m-d", strtotime($resultAr["rdate"]));
		}
		
	if(!empty($resultAr["rtime"]))
        $resultAr["rtime"] = preg_replace("/\(.*/", "", $resultAr["rtime"]);
		
	if(isset($resultAr["fullname"])) {
        $resultAr["fullname"] = preg_replace("/\(.*/", "", trim($resultAr["fullname"]));
		$tmp = explode(" ", $resultAr["fullname"]);
		if(count($tmp) > 2) {
			$resultAr["salutation"] = $tmp[0];
			$resultAr["first"] = $tmp[1];
			$resultAr["last"] = $tmp[2];
			}
		else if(count($tmp) > 1) {
			$resultAr["salutation"] = " ";
			$resultAr["first"] = $tmp[0];
			$resultAr["last"] = $tmp[1];
			}
		else if(count($tmp) > 0) {
			$resultAr["salutation"] = " ";
			$resultAr["first"] = " ";
			$resultAr["last"] = $tmp[0];
			}
		}

	if(!empty($resultAr["rtime"])) {
		$resultAr["rtime"] = preg_replace("/AM/", "", $resultAr["rtime"]);
		if(preg_match("/PM/", $resultAr["rtime"])) {
			$v = strtr($resultAr["rtime"], "PM", "  "); 
			$vAr = explode(":", $v); 
			if(intval($vAr[0]) < 12)
				$vAr[0] = intval($vAr[0])+12; 
			$resultAr["rtime"] = trim(implode(":", $vAr));
			}
		}

	if(!empty($resultAr["cover"]))
		$resultAr["cover"] = intval($resultAr["cover"]);
		
	$resultAr["status"] = 1;	

	if(isset($_REQUEST["DEBUG"]) && $_REQUEST["DEBUG"] == "73") {
		echo "<h3>" . $resultAr["processing"] . "</h3>";
		foreach($resultAr as $label => $value) {
			echo "<hr />'" . $label . "' ===> '" . $value . "'<br />";
			}
		echo "<br />version 1.1";
		}

	foreach ($resultAr as &$value) 
		$value = preg_replace("/\'|\"|\&\#39;/", "`", $value);

	if( $resultAr["restaurant"] == "Disgruntled Chef")
		$resultAr["restaurant"]  = "Disgruntled_Chef";

	print_r($resultAr);
	
	return $resultAr;
}


$msg = file_get_contents("../crawler_files/chope_a.html");
$data = parse_chope($msg, "Bacchanalia");
/*
require_once("conf/conf.init.inc.php");
require_once("lib/wpdo.inc.php");
$data = pdo_multiple_select("SELECT body FROM crawler_email WHERE host = 'chope.co' ORDER BY `ID` DESC LIMIT 100");

foreach($data as $row) {
$msg = $row['body'];
$data = parse_chope($msg, "Bacchanalia");
echo "<br /><hr />";	
}
*/
?>
