<?php

// Parser TABLEDB

echo 'TABLEDB - HUNGRY GO WHERE PARSER LOADED'.'<br/>';

$booker = "HGW";

function parsing_block($msg, $keyword, &$resultAr) {

	foreach($keyword as $label => $value) {
		$pat = "/\|" . $label . "\s+([^|]+)\|/";
		$resultAr[$value] = "";
		if(preg_match($pat, $msg, $match))
			$resultAr[$value] = (count($match) > 1) ? $match[1] : "";

		if($value == "specialrequest" && !empty($resultAr[$value]))
			$resultAr[$value] = str_replace("&nbsp;", " ", $resultAr[$value]);

		if($value == "rdate" || $value == "ordate") 
			$resultAr[$value] = date("Y-m-d", strtotime($resultAr[$value]));

		if($value == "cover" && !empty($resultAr[$value]))
			$resultAr[$value] = intval($resultAr[$value]);
			
		if($value == "rtime" || $value == "ortime")
			if(!empty($resultAr[$value])) {
				$resultAr[$value] = preg_replace("/AM/", "", $resultAr[$value]);
				if(preg_match("/PM/", $resultAr[$value])) {
					$v = strtr($resultAr[$value], "PM", "  "); 
					$vAr = explode(":", $v); 
					if(intval($vAr[0]) < 12)
						$vAr[0] = intval($vAr[0])+12; 
					$resultAr[$value] = trim(implode(":", $vAr));
					}
			}
		}
}
	
function email_parser($msgorg) {
	$resultAr = array();
	$etape = 0;

	if(isset($_REQUEST["DEBUG"]) && $_REQUEST["DEBUG"] == "73") {
		echo $account . "<hr /><br />";
		echo $msgorg . "<hr />";
		}

	$msgorg = preg_replace("/=[\r\n]+/", "", $msgorg);
	$msgorg = preg_replace("/=0D|=0A/", "\r", $msgorg);

	$msgorg = preg_replace("/\"|\'/", " ", $msgorg);
	$msgorg = preg_replace("/\<br[^>]+\>/i", "|!!", $msgorg);
	$msgorg = preg_replace("/.*\<body\>(.*)\<\/body\>.*/", "$1", $msgorg);
	$msgorg = preg_replace("/\<[^>]+\>/", "", $msgorg);
	$msgorg = preg_replace("/&nbsp;/", " ", $msgorg);
	$msgorg = preg_replace("/\r|\n/", "|", $msgorg);
	$msgorg = preg_replace("/\s+/", " ", $msgorg);
	$msgorg = preg_replace("/\=\|/", "", $msgorg);
	$msgorg = preg_replace("/\= /", "", $msgorg);
	$msgorg = preg_replace("/\=20/", "", $msgorg);
	$msgorg = preg_replace("/\=5F/", "_", $msgorg);
	$msgorg = preg_replace("/\:\|/", ":", $msgorg);
	$msgorg = preg_replace("/The original reservation ([^|]+)\|Reservation Information/", "$0 Original", $msgorg);
	$msgorg = preg_replace("/Dear ([^|]+)\s*Management,\|/", "Restaurant: $1|", $msgorg);
	$msgorg = preg_replace("/Reservation Information: (\d+) guests on /", "Customer Cover: $1|Customer Date: ", $msgorg);
	$msgorg = preg_replace("/Reservation Information: (\d+) on /", "Customer Cover: $1|Customer Date: ", $msgorg);
	$msgorg = preg_replace("/Customer Date: ([^|,]+,[^|,]+), ([^|]+)\|/", "Customer Date: $1|Customer Time: $2", $msgorg);
	$msgorg = preg_replace("/Reservation Information Original: (\d+) on /", "Original Customer Cover: $1|Original Customer Date: ", $msgorg);
	$msgorg = preg_replace("/Original Customer Date: ([^|,]+,[^|,]+), ([^|]+)\|/", "Original Customer Date: $1|Original Customer Time: $2", $msgorg);
	$msgorg = preg_replace("/Customer/", "|Customer", $msgorg);
	$msgorg = preg_replace("/Promotion\|?Title:+ /", "|Title: ", $msgorg);
	$msgorg = preg_replace("/Reservation Notes:/", "|Reservation Notes: ", $msgorg);
	$msgorg = preg_replace("/Reservation Notes: \!\!/", "", $msgorg);
	
	// reduce the search to plain text
	//$found = preg_match("/Content-Type: text\/plain;((?(?!Content-Type).)*)Content-Type.*/", $msgorg, $match);

	$msg = $msgorg;

	echo $msg . "<br/><hr/>";

	// search for mode	
	$keyword = array("CANCELLATION" => "cancellation", "UPDATES" => "modification", "New Reservation" => "booking");
	foreach($keyword as $label => $value) {
		if(preg_match("/$label/i", $msg)) {
			$resultAr["processing"] = $value;
			break;
			}
		}

	// search for information	
	$keyword = array("Restaurant:" => "restaurant", "Customer Date:" => "rdate", "Customer Time:" => "rtime", "Customer Name:" => "fullname", "Customer Cover:" => "cover", "Confirmation Number" => "confirmation", "Customer Contact:" => "mobile", "Customer Email:" => "email", "Reservation Notes:" => "specialrequest", "Title:" => "promotion");
	$orgkeyword = array("Restaurant:" => "restaurant", "Customer Date:" => "ordate", "Customer Time:" => "ortime");

	if($resultAr["processing"] !== "modification") {
		parsing_block($msg, $keyword, $resultAr);
	} else {
		$newmsg = preg_replace("/.*(UPDATED reservation details.*)/", "$1", $msg);
		$orgmsg = preg_replace("/(.*The original reservation.*UPDATED).*/", "$1", $msg);
		$orgmsg = preg_replace("/Dear\s+(.*)\s+Management,/", "|Restaurant: $1|", $orgmsg);
		parsing_block($newmsg, $keyword, $resultAr);
		parsing_block($orgmsg, $orgkeyword, $resultAr);
		}
	
	if(isset($resultAr["bookinginfo"]) && false) {
		$pat = "/\|" . $label . "([^|]*)/";
		if(preg_match($pat, $msg, $match))
		$tmp = explode(" ", $resultAr["fullname"]);
		if(count($tmp) > 2) {
			$resultAr["salutation"] = $tmp[0];
			$resultAr["first"] = $tmp[1];
			$resultAr["last"] = $tmp[2];
			}
		}

	$resultAr["mobile"] = preg_replace("/([+0-9 ]+).*/", "$1", $resultAr["mobile"]);
	if($resultAr["promotion"] != "")
		$resultAr["specialrequest"] .= (($resultAr["specialrequest"] != "") ? " | " : "") . $resultAr["promotion"];
	$resultAr["salutation"] = "";
	if(isset($resultAr["fullname"])) {
		$tmp = explode(" ", $resultAr["fullname"]);
		if(count($tmp) > 2) {
			$resultAr["salutation"] = $tmp[0];
			$resultAr["first"] = $tmp[1];
			$resultAr["last"] = $tmp[2];
			}
		else if(count($tmp) == 2) {
			$resultAr["first"] = $tmp[0];
			$resultAr["last"] = $tmp[1];
			}
		else if(count($tmp) == 1) {
			$resultAr["first"] = "-";
			$resultAr["last"] = $tmp[0];
			}
		}
	
	if($resultAr["processing"] === "booking" || $resultAr["processing"] === "cancellation") {
		if(isset($resultAr["rdate"]) && isset($resultAr["rtime"]) && isset($resultAr["first"]) && isset($resultAr["last"]) && empty($resultAr["confirmation"]))
			$resultAr["confirmation"] = substr($resultAr["first"], 0, 1) .substr($resultAr["last"], 0, 1) . date("dmyHi", strtotime($resultAr["rdate"] . ", " . $resultAr["rtime"]));
		else $resultAr["confirmation"] = "UNKOWN";
		$resultAr["confirmation"] = strtoupper($resultAr["confirmation"]);
		}
		
	if($resultAr["processing"] === "modification") {
		if(isset($resultAr["ordate"]) && isset($resultAr["ortime"]) && isset($resultAr["first"]) && isset($resultAr["last"]) && empty($resultAr["confirmation"]))
			$resultAr["confirmation"] = substr($resultAr["first"], 0, 1) .substr($resultAr["last"], 0, 1) . date("dmyHi", strtotime($resultAr["ordate"] . ", " . $resultAr["ortime"]));
		else $resultAr["confirmation"] = "UNKOWN";
		$resultAr["confirmation"] = strtoupper($resultAr["confirmation"]);
		}
			
	$resultAr["status"] = 1;	

	if(isset($_REQUEST["DEBUG"]) && $_REQUEST["DEBUG"] == "73") {
		echo "<h3>" . $resultAr["processing"] . "</h3>";
		foreach($resultAr as $label => $value) {
			echo "<hr />'" . $label . "' ===> '" . $value . "'<br />";
			}
		echo "<br />version 1.1";
		}

	foreach ($resultAr as &$value) 
		$value = preg_replace("/\'|\"|\&\#39;/", "`", $value);
	print_r($resultAr);

	//require_once("lib/class.booking.inc.php");
	//$booking = new WY_Booking();
	//$booking->getRemoteBooking("SG_SG_R_MitsubaJapaneseRestaurant", "HGW", $resultAr["confirmation"], $resultAr["mobile"], $resultAr["email"]);

	return $resultAr;
}

$msg = file_get_contents("../crawler_files/hgw_a.html");

$data = email_parser($msg);	


?>
