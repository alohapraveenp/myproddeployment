
app.service('apiService', ['$http','$q', function($http,$q){  

  	var self = this;
	var weeloy_demo_restaurant = ["SG_SG_R_TheFunKitchen", "SG_SG_R_TheOneKitchen", "FR_PR_R_LaTableDeLydia", "KR_SE_R_TheKoreanTable"];

	this.getPlatform = function(val){
		var i, profile = ["Android", "iPhone", "iPad", "Windows", "Apple"];
		
		if(val === "" || typeof val !== 'string') return "";
		if(val.indexOf("name=unknown|version=unknown|platform=Windows") >= 0)
			return "iPhoneApp";
		if(val.indexOf("name=Android") >= 0 && val.indexOf("platform=Android") >= 0)
			return "AndroidApp";
			
		for(i = 0; i < profile.length; i++)
			if(val.indexOf("platform=" + profile[i]) >= 0)
				return profile[i];
		return "";
		};
		
	this.getBrowser = function(val){
		var i, tmp, browser = ["Chrome", "Internet Explorer", "Safari", "Firefox"];
		
		if(val === "" || typeof val !== 'string') return "";
		if(val.indexOf("name=unknown|version=unknown|platform=Windows") >= 0)
			return "iPhoneApp";
		if(val.indexOf("name=Android") >= 0 && val.indexOf("platform=Android") >= 0)
			return "AndroidApp";

		for(i = 0; i < browser.length; i++)
			if(val.indexOf("name=" + browser[i]) >= 0)
				return browser[i];
		return val.replace(/\|.*$/g, '').replace(/name \= /g, '');
		};

	this.readBooking = function(restaurant, mode, options, token) {
		if(!options) options = "";	
		return $http.post('./api/visit/read/', { 'restaurant': restaurant, 'mode': mode, 'options': options, 'token' : token } ).then(function(response) {
			var x, names, i, k, tt, uu, oo, c, validOptions = ['event', 'duration', 'repeat', 'notifysmswait', 'notestext', 'notescode', 'validate'];
			if(response.data.status < 0)
				return response.data;
				
			names = response.data.data;
			names.map(function(oo, i) {
			  	try {
				oo.restaurant = oo.resto;
				oo.title = oo.restaurant.substring(8, 30);
				oo.platform = self.getPlatform(oo.browser);
				oo.navigator = self.getBrowser(oo.browser);
				oo.index = i + 1;
				oo.time = oo.time.substr(0, 5);
				oo.vtime = (parseInt(oo.time.substr(0, 2)) * 60) + (parseInt(oo.time.substr(3, 2)));
				oo.slot = oo.time.timetoslot();
				oo.slot15 = oo.time.timeto15slot();
				oo.date = oo.date.substring(0,10);
				oo.cfulldate = oo.createdate;
				oo.ctime = oo.createdate.substring(11,16);
				oo.cdate = oo.createdate.substring(0,10);
				oo.vcdate = oo.cdate.jsdate().getTime();
				oo.vdate = oo.date.jsdate().getTime();
				oo.datetime = oo.date.jsdate().getTime();
				oo.ddate = oo.date.jsdate().getDateFormat('-');
				oo.sdate = oo.ddate.substring(0, 5).replace(/-/g, "/");
				oo.mealtype = (parseInt(oo.time.substr(0, 2)) < 16) ? "lunch" : "dinner";
				oo.fullname = oo.first + ' ' + oo.last;
				oo.iplong = long2ip(oo.ip);
				oo.options = {};
				oo.validate = "";
				oo.event = "";
				oo.duration = "";
				oo.repeat = "";
				oo.lastvisit = "";
				oo.depositid = oo.booking_deposit_id;				
				oo.paymethod = (oo.payment_method === "paypal") ? oo.payment_method  : "credit card" ;
                  
				if(oo.more && typeof oo.more === "string" && oo.more !== "") {
					oo.options = self.parseObj(oo.more);
                                    
					for(x in oo.options) {
						if(oo.options.hasOwnProperty(x) && validOptions.indexOf(x) >= 0 && typeof oo.options[x] === "string") {
							oo[x] = oo.options[x];                                                   
							}
						}
					}
				if(!oo.notestext) oo.notestext = "";
				if(!oo.notescode) oo.notescode = "";
				oo.fullnotes = oo.notestext + ((oo.notestext !== "" && oo.notescode !== "") ? "," : "") + oo.notescode; 

				if(oo.pers !== "" && oo.pers.substr(0, 4) === "more") oo.pers = "99";
				else if(oo.pers === "") oo.pers = "0";
				self.setTracking(oo);
				if(oo.state === "")
					oo.state = (oo.type === "waiting") ? "waiting" : "to come";
				if(oo.state === "waiting" && oo.bkstatus === "") {
					oo.bkstatus = "notify";
					if(typeof oo.notifysmswait === "string" && parseInt(oo.notifysmswait) > 0)
						oo.bkstatus = "notify " + parseInt(oo.notifysmswait);
					}
				if (oo.wheelwin == "" && oo.bkstatus == "cancel" || oo.bkstatus == "noshow" )
                                    
					oo.wheelwin = oo.bkstatus;
				if(oo.canceldate === "0000-00-00 00:00:00" || oo.bkstatus !== "cancel") 
					oo.canceldate = "";
		 		oo.test = (oo.first.toLowerCase() === "test" || 
		 	   					oo.last.toLowerCase() === "test" || 
		 	   					oo.comment.toLowerCase() === "test" || 
		 	   					oo.comment.toLowerCase().indexOf("test ") >= 0 );
		 	   	oo.funkitchen = (weeloy_demo_restaurant.indexOf(oo.restaurant) > -1);
		 	   	self.setType(oo);
				} catch(e) { console.error('READBOOKING', e); }
			  });	
			return response;
			})
		};
	
	this.getdebugErrorCount = function(email) {
		return $http.post("../api/services.php/debugerror/count/", { 'email' : email, 'token' : token }).success(function (response) { return response.data; });
		};

	this.parseObj = function(str) {
		if(typeof str !== "string" || str === "")
			return {};
		
		str = str.replace(/’/g, "\"");
		if(str !== '')
			str = str.replace(/\s+/g, " ");
		try {
		var oo = JSON.parse(str);
		} catch(err) { console.log(err, str); return {}; }
		return (oo) ? oo : {};
		};
		
	this.setTracking = function(oo) {
		var i, tt, k, trackval = [ "remote", "waiting", "tms", "callcenter", "walkin", "website", "facebook", "partner", "cpp_credit_suisse", "weeloy" ];
		var validtype = ['booking', 'thirdparty', 'walkin', 'import'];
		
		if(validtype.indexOf(oo.type) < 0) return;

		tt = oo.tracking.trim();
		if(tt !== '') tt = tt.toLowerCase();
		if(tt.indexOf("|") < 0 && trackval.indexOf(tt) >= 0)
			return oo.type = tt;
		else if(tt.indexOf("|") >= 0) {
			ttAr = tt.split("|");
			for(i = 0; i < trackval.length; i++) 
				if(ttAr.indexOf(trackval[i]) >= 0) {
					return oo.type = trackval[i];
					}
			}
		};
					
	this.setType = function(oo) {
		oo.sudotype = oo.type;
		
		if(oo.type === 'thirdparty')
			oo.sudotype = oo.booker;
		if(oo.tracking.length > 0 && oo.tracking.search("IMASIA") > -1) {
			if(oo.tracking.search("IMASIA|facebook") > -1) oo.sudotype = "imasiafb";
			else if(oo.tracking.search("IMASIAEDM") > -1) oo.sudotype = "imasiaedm";
			else oo.sudotype = "imasia";
			}
		};
			
	this.validState = function() {
		return [ 'to come', 'seated', 'paying', 'left', 'no show' ];
		};

	this.ModalDataBooking = function() {

		var i, j, timeslotAr=[], persAr=[], hourslotAr=[], minuteslotAr=[];

		for(i = 1; i < 50; i++) 
			persAr.push(i);
		
		for(i = 9; i < 24; i++)
			for(j = 0; j < 60; j += 15) {
				timeslotAr.push((i < 10 ? '0' : '') + i + ':' + (j === 0 ? '0' : '') + j);
				}
			
		for(i = 9; i < 24; i++) 
			hourslotAr.push((i < 10 ? '0' : '') + i);
		
		for(i = 0; i < 60; i += 5) 
			minuteslotAr.push((i < 10 ? '0' : '') + i);
			
		return  {
			name: "", 
			pers: persAr, 
			timeslot: timeslotAr, 
			hourslot: hourslotAr,
			minuteslot: minuteslotAr,
			ntimeslot: 0,
			event: ['Birthday', 'Wedding', 'Reunion'],
			start_opened: false,
			opened: false,
			selecteddate: null,
			originaldate: null,
			dateFormated: "",
			theDate: null,
			today: new Date(),
			minDate: null,
			maxDate: null,
			deposit:null,
			formats: ['dd-MM-yyyy', 'dd/MMMM/yyyy', 'yyyy/MM/dd', 'dd/MM/yyyy', 'dd-MM-yyyy', 'yyyy-MM-dd', 'dd.MM.yyyy', 'shortDate', 'yyyy-MM-dd', ],
			dateOptions: { formatYear: 'yyyy', startingDay: 1 },
			formatsel: "",
			func: null,
			
			setInit: function(ddate, rtime, func, format, minDate, maxDate) {
				
				this.theDate = null;
				this.func = func;
				this.formatsel = this.formats[format];			
				this.ntimeslot = rtime.substring(0, 5);
			
				if(ddate instanceof Date === false) {
					ddate = ddate.jsdate();
					}
				this.originaldate = ddate;
				this.theDate = ddate;
				this.minDate = minDate;
				this.maxDate = maxDate;
				},
		
			setDate: function(ddate) {
				this.theDate = ddate;
				this.originaldate = ddate; // this is for the case that no data selected
				},
				
			getDate: function(sep, mode) {
				if(this.theDate && this.theDate instanceof Date)
					return (mode !== 'reverse') ? this.theDate.getDateFormat(sep) : this.theDate.getDateFormatReverse(sep);
				return (mode !== 'reverse') ? this.originaldate.getDateFormat(sep) : this.originaldate.getDateFormatReverse(sep);
				},

			getTheDate: function() {
				if(this.theDate && this.theDate instanceof Date)
					return this.theDate;
				return this.theDate = this.originaldate = this.today ;
				},
				
			getmTime: function(offset) {
				if(this.theDate && this.theDate instanceof Date) 
					return (this.theDate.getTime() + offset);
				console.log('ERROR getmTime');
				return this.today.getTime();
				},
								
			dateopen: function($event) {
				this.start_opened = true;
				this.opened = true;
				$event.preventDefault();
				$event.stopPropagation();
				},
			
			disabled: function(date, mode) {
				return false;
				},
		
			calendar: function() {
				this.theDate = this.originaldate;
				if(this.theDate instanceof Date)
					this.dateFormated = this.theDate.getDateFormat('/');
				},
		
			onchange: function() {
				this.theDate = this.originaldate;
				if(this.theDate instanceof Date)
					this.dateFormated = this.theDate.getDateFormat('/');

				if(this.func !== null)
					this.func();
				}	
			};
		};
}]);

