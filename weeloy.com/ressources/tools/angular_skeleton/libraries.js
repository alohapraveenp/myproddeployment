function long2ip(ip) {

  if (!isFinite(ip))
    return false;

  return [ip >>> 24, ip >>> 16 & 0xFF, ip >>> 8 & 0xFF, ip & 0xFF].join('.');
}

function ip2long(IP) {

  var i = 0;
  IP = IP.match(
    /^([1-9]\d*|0[0-7]*|0x[\da-f]+)(?:\.([1-9]\d*|0[0-7]*|0x[\da-f]+))?(?:\.([1-9]\d*|0[0-7]*|0x[\da-f]+))?(?:\.([1-9]\d*|0[0-7]*|0x[\da-f]+))?$/i
  ); // Verify IP format.
  if (!IP) {
    return false; // Invalid format.
  }
  // Reuse IP variable for component counter.
  IP[0] = 0;
  for (i = 1; i < 5; i += 1) {
    IP[0] += !! ((IP[i] || '')
      .length);
    IP[i] = parseInt(IP[i]) || 0;
  }
  // Continue to use IP for overflow values.
  // PHP does not allow any component to overflow.
  IP.push(256, 256, 256, 256);
  // Recalculate overflow of last component supplied to make up for missing components.
  IP[4 + IP[0]] *= Math.pow(256, 4 - IP[0]);
  if (IP[1] >= IP[5] || IP[2] >= IP[6] || IP[3] >= IP[7] || IP[4] >= IP[8]) {
    return false;
  }
  return IP[1] * (IP[0] === 1 || 16777216) + IP[2] * (IP[0] <= 2 || 65536) + IP[3] * (IP[0] <= 3 || 256) + IP[4] * 1;
}


app.filter('inversedate', function() {
	return function(input) {
	return input.substr(8,2) + "-" + input.substr(5,2) + "-" + input.substr(0,4);
	}
});
 
 
app.directive('tbtitle', [ function(){
	return {
		restrict: 'AE',
		replace: true,
		template: '<a href="javascript:" style="color:{{y.cc}}" class="{{moduleName}}_title {{moduleName}}_{{y.b}}">{{ y.b }} <i class="fa fa-caret-{{y.q}}"></a>',
		link: function(scope, elem, attrs, ctrls) { 
			elem.bind('click', function() {
				var i, ll = attrs["name"], data = scope[ll], cc = elem.css('color'), ncc, cll, cl = scope.moduleName + "_title";
				for(i = 0; i < data.length; i++)  {
					if(data[i].q === "up") { 
						cll = scope.moduleName + '_' + data[i].b;
						$("th ." +cll).html('<a href="javascript:" style="color:rgb(0, 0, 0)" class="'+cl+' '+cll+'">' + data[i].b + ' <i class="fa fa-caret-down"></a>');
						break;
						}
					}
				$("th ." +cl).css('color', 'rgb(0, 0, 0)'); 
				scope.y.q = (cc === "rgb(0, 0, 0)" || cc === "rgb(255, 165, 0)") ? "down" : "up";
				ncc = (scope.y.q == 'up') ? 'rgb(255, 165, 0)' : 'rgb(255, 0, 255)';
				cll = scope.moduleName + '_' + scope.y.b;
				elem.html('<a href="javascript:" style="color:' + ncc + '" class="'+cl+' '+cll+'">' + scope.y.b + ' <i class="fa fa-caret-' + scope.y.q + '"></a>');
				elem.css('color', ncc);	// 'orange' : 'fuchsia'
				scope.reorder(scope.y.a, scope.y.alter);
				scope.$apply();
				}); 
			}
		};
}]);

app.filter('forLoop', function() {
	return function(input, start, end) {
	for (input = []; start < end; start++) { input.push(start); }
	return input; 
	}
});


app.filter('notzero', [ function() {
	return function(n) {
		return (typeof n !== 'number' || n > 0) ? n : "";
	}
}]);


app.filter('capitalize', function() {
  return function(str, scope) {
    if (str != null && str != "")
    str = str.toLowerCase();
    return str.substring(0,1).toUpperCase()+str.substring(1);
  }
});

app.directive('buttonRender', function() {
  return {
    restrict: 'A',
    require: 'ngModel',
    link: function(scope, element, attrs, ctrl) {
      ctrl.$render = function() {
        var value = ctrl.$isEmpty(ctrl.$viewValue) ? '' : ctrl.$viewValue;
        if (element.val() !== value) {
          element.val(value);
        }
      };
    }
  };
});

app.directive('showpict', ['$compile', function($compile){
	return {
		restrict: 'AE',
		replace: true,
		link: function(scope, element, attrs){
			if(attrs.pict == '')
				return;
			$('.showpict').html(attrs.pict);
    		}
		};
}]);

app.directive('extitle', ['$compile', function($compile){
	return {
		restrict: 'AE',
		replace: true,
		link: function(scope, element, attrs){
			if(scope.x.a != "divider") element.html('<li style="margin: 0 0 8px 20px;"><a href ng-click="extraselect(x.a)"><span class="custom" id="' + scope.x.a + '" ></span>  {{ x.b }} <br /></a></li>').show();
			else element.html('<li class="divider"></li>').show();
			$compile(element.contents())(scope);
    		}
		};
}]);

app.directive("tbListingbkg", ['$compile', function($compile) { 
	var linkFunction = function( scope, el, at) { 
		if(scope.z.l !== "") {
			var content = el.html(); 		
			el.html('<div class="truncate" style="width:' + scope.z.l + 'px">' + content + '</div').show();
			$compile(el.contents())(scope);
    		}
		}; 
	return { 
		restrict: "AE", 
		link: linkFunction 
	}; 
}]);

app.filter('filterBymyDate', [ function() {
	return function(arr, start, end, field) {
		var tmpAr = [], oo;
		if (typeof arr === 'undefined' || !(arr instanceof Array) || arr.length < 1) 
			return tmpAr;
		if(start < 0) return arr;
		oo = arr[0];
		if(typeof oo[field] === 'undefined') {
			console.log('ERROR filterBymyDate undefined field', field);
			return tmpAr;
			}
		for(var i = 0; i < arr.length; i++) {
			oo = arr[i];
			if(oo[field] >= start && oo[field] <= end)
				tmpAr.push(oo);
			}
		return tmpAr;
		}
}]);


app.filter('adatereverse', [ function() {
	return function(input, f) {
		if(typeof f!=='string' || typeof input!=='string' || input.length < 10|| f !== 'date') return input;
		return input.substr(8,2) + "-" + input.substr(5,2) + "-" + input.substr(0,4);
	}
}]);

app.filter('apostrophe', [function() {
	return function(s) {
		if(typeof s === "string" && s.length > 1)
			return s.replace(/`/g, "’");
		return s;
	}
}]);

Date.prototype.getDateFormat = function(sep) {
	var d = this.getDate(), m = this.getMonth()+1, y = this.getFullYear();
	if(typeof sep !== 'string' || (sep !== '/' && sep !== '-')) sep = '-';
	return ((d <= 9) ? '0' : '') + d + sep + ((m <= 9) ? '0' : '') + m + sep + y;
	};

Date.prototype.getDateFormatReverse = function(sep) {
	var d = this.getDate(), m = this.getMonth()+1, y = this.getFullYear();
	if(typeof sep !== 'string' || (sep !== '/' && sep !== '-')) sep = '-';
	return y + sep + ((m <= 9) ? '0' : '') + m + sep + ((d <= 9) ? '0' : '') + d;
	};

String.prototype.jsdate = function() {
	var n, s, i;
	if(this.length < 10 || this.indexOf('-') === -1) 
		return new Date(2016, 0, 1);
	s = this.substring(0, 10);
	tt = s.split('-');
	for(i = 0; i < tt.length; i++) tt[i] = parseInt(tt[i]);
	tt[1] = tt[1] - 1;
	return new Date(tt[0], tt[1], tt[2], 0, 0, 0, 0);
	};

String.prototype.timetoslot = function() {
	var val = this, uu;
	
	if(typeof val === 'undefined') return -2;
	
	if(val !== "" && val.indexOf(":") > 0) {
		uu = val.substring(0, 5).split(":");
		return (parseInt(uu[0]) * 2) + Math.floor(parseInt(uu[1]) / 30);
		}
	return -1;
	};

String.prototype.timeto15slot = function() {
	var val = this, uu;
	
	if(typeof val === 'undefined') return -2;
	
	if(val !== "" && val.indexOf(":") > 0) {
		uu = val.substring(0, 5).split(":");
		return (parseInt(uu[0]) * 4) + Math.floor(parseInt(uu[1]) / 15);
		}
	return -1;
	};

Array.prototype.inObject = function(name, value) {
	for(var i = 0; i < this.length; i++) {
		oo = this[i];
		if(oo[name] && oo[name] === value)
			return i; 
		}
	return -1;
	};


app.directive('selectCreateBookingDate', [ function () {
	return {
		restrict: 'E',
		scope: {
			theday: '='
			},
		template:
			"<span ng-if='theday.showtitle === true'> selected day: <strong> {{ theday.selectedDay | adatereverse:'date' }} </strong></span>" + 
			"<div class='btn-group' uib-dropdown>" +
				"<button type='button' class='btn btn-warning btn-xs' uib-dropdown-toggle aria-expanded='false' style='font-size:11px;margin-left:15px;'>date mode <span class='caret'></span></button>" +
				"<ul class='dropdown-menu' uib-dropdown-menu role='menu'>" + 
				"<li ng-repeat='x in theday.datemodelist' class='glyphiconsize'><a href ng-class=\"{ 'glyphicon glyphicon-ok glyphiconsize': x === theday.datemode }\" ng-click='setdatemd(x);'> {{x}}</a></li>" +
				"<li class='divider'></li>" +
				"<li><a ng-click='setdatemd(null);'><i class='glyphicon glyphicon-off'></i> reset</li>" +
				"</ul>" +
			"</div>",		
		controller: function ($scope) {
			$scope.setdatemd = function(x) {
				if(!x || x === 'reset') {
					$scope.theday.selectedDay = null;
					if($scope.theday.resetfunc) 
						$scope.theday.resetfunc();
				} else  {
					$scope. theday.datemode = x;
					$scope. theday.field = ($scope. theday.datemode !== $scope. theday.datemodelist[0]) ? "vcdate" : "vdate";
					}
				};
			}
		};
}]);


// Code goes here
// it does not get minified

app.directive('dropdownMultiselect', [ function () {
	return {
		restrict: 'E',
		scope: {
			model: '=',
			list: '=',
			savefunc: '=',
			},
		template:
				"<div class='btn-group btn-xs' data-ng-class='{open: openMS}'>" +
					"<button class='btn btn-default btn-xs' data-ng-click='openDropdownMS()'>{{localtitle}}</button>" +
					"<button class='btn btn-default btn-xs dropdown-toggle' data-ng-click='openDropdownMS()'><span class='caret'></span></button>" +
					"<ul class='dropdown-menu scrollablesmall-menu' aria-labelledby='dropdownMenu'>" +
					// "<li><a data-ng-click='selectAllMS()'><span class='glyphicon glyphicon-ok green' aria-hidden='true'></span> Check All</a></li>" +
					"<li><a data-ng-click='saveMS()'>SAVE</a></li>" +
					"<li><a data-ng-click='deselectAllMS();'>RESET</a></li>" +
					"<li class='divider'></li>" +
					"<li data-ng-repeat='item in list'><a data-ng-click='toggleItemMS(item)'><span data-ng-class='getClassNameMS(item)' aria-hidden='true'></span> {{item}}</a></li>" +
					"</ul>" +
				"</div>",

		controller: function ($scope) {
			var tbname = []; 
			var booking = $scope.model.booking;
			if(typeof $scope.model.tablename === 'string' && $scope.model.tablename.length > 0) 
				tbname = $scope.model.tablename.split(",");
			$scope.localtitle = (tbname.length < 1) ? "add/reset" : tbname.join(",");
			$scope.openDropdownMS = function () { if(booking !== $scope.model.booking) { booking = $scope.model.booking; tbname = $scope.model.tablename.split(","); } $scope.openMS = !$scope.openMS; };

			$scope.saveMS = function () { $scope.openMS = false; $scope.model.tablename = tbname.join(","); $scope.savefunc($scope.model.booking, $scope.model.tablename, 'set'); };
			$scope.selectAllMS = function () {
				tbname = [];
				angular.forEach($scope.list, function (item, index) { $scope.additemMS(item);  });
				};
			$scope.deselectAllMS = function () { tbname = []; };
			$scope.toggleItemMS = function (item) {
				var ind = $scope.getIndexFound(item);
				if(ind < 0) $scope.additemMS(item);
				else $scope.removeitemMS(item, ind);
				};
			$scope.additemMS = function (item) {
				tbname.push(item); 
				tbname.sort();
				};
				
			$scope.removeitemMS = function (item, ind) {
				tbname.splice(ind, 1);
				};
				
			$scope.getClassNameMS = function (item) {
				if($scope.getIndexFound(item) < 0) 
					return ""; 
				return 'glyphicon glyphicon-ok green';
				};
				
			$scope.getIndexFound = function(item) {
				return tbname.indexOf(item); 
				};
				
			}
		};
}]);

