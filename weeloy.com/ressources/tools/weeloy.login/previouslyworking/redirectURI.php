<?

$fb_data = array ( "token" => "CAAUn9QNxfu8BAMpDqJydtxHRMNRoXNrZB5ZBdOZBTr7o3ZAtLS5kD7tribbV1LncWi2bXOxzuWqd0naRwchbrDpx677levZBSDoXcGRoV1y2GH1xTuQ74yvsHrUJ0dsDdZA4SxxgO27tlGMUZBBj3BzcAWkC33LqXJuui6J0HUZADWKtrYFrtsizqb1e6tInYTZBhSxX7KZABaxMCpswfqHRyJvErlZA50l5TMZD",
 		"username" => "Richard Kefs",
		"userlastname" => "Kefs",
		"userfirstname" => "Richard",
		"userid" => "10204408720718496",
		"timezone" => "8",
		"application" => "facebook",
		"type_action" => "Login",
		"email" => "richard@kefs.me",
		"password" => ""
		);
		
print_r($fb_data);
exit;
		
define("__LIBDIR__", $_SERVER['DOCUMENT_ROOT'] . "/../LIB/");

require_once(__LIBDIR__ . "dblib.inc.php");
require_once(__LIBDIR__ . "class.login.inc.php");
require_once(__LIBDIR__ . "class.mail.inc.php");

	$db = new db_mySQL;		// could be user by the included files below
	$db->init();

	$login = new WY_Login;
	$login->init($db);

	$profile = new WY_Profile;

	$profile->email = $_POST['email'];
	$profile->password = $_POST['password'];
	$profile->retypepassword = $_POST['retypepassword'];
	$profile->newpassword = $_POST['newpassword'];
	$profile->name = ucfirst($_POST['userlastname']);
	$profile->firstname = ucfirst($_POST['userfirstname']);
	$profile->mobile = $_POST['mobile'];
	$profile->country = $_POST['country'];
	$profile->timezone = $_POST['timezone'];
	$profile->userid = $_POST['userid'];
	$profile->token = $_POST['token'];
	$profile->socialname = $_POST['socialname'];
	$profile->application = $_POST['application'];
	$profile->extrafield = "To be defined";

	if($profile->application == "facebook" || $profile->application == "linkedin")
		$profile->extrafield = $profile->userid . " : " . $profile->token;		
			
	else if($profile->application == "twitter")
		$profile->extrafield = $profile->userid . " : " . $profile->socialname;		
			
	if(!empty($_POST['type_action']))
		{
		if($_POST['type_action'] == "Logout")
			$login->logout();
		else if($_POST['type_action'] == "Login")
			$login->process_login("Login", $profile);
		else if($_POST['type_action'] == "Register")
			$login->process_login("Register", $profile);
		else if($_POST['type_action'] == "LostPassword")
			$login->process_login("LostPassword", $profile);
		else if($_POST['type_action'] == "UpdatePassword")
			$login->process_login("UpdatePassword", $profile);
		}

	if(empty($profile->application) || $profile->application != "twitter")
		header("Location: login.php?message=" . $login->msg . "&application=" . $profile->application);
	else header("Location: ../login.php?message=" . $login->msg . "&application=" . $profile->application);

?>
