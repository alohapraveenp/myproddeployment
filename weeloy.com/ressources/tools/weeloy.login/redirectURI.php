<?php

$fb_data = array ( "token" => "CAAUn9QNxfu8BAMpDqJydtxHRMNRoXNrZB5ZBdOZBTr7o3ZAtLS5kD7tribbV1LncWi2bXOxzuWqd0naRwchbrDpx677levZBSDoXcGRoV1y2GH1xTuQ74yvsHrUJ0dsDdZA4SxxgO27tlGMUZBBj3BzcAWkC33LqXJuui6J0HUZADWKtrYFrtsizqb1e6tInYTZBhSxX7KZABaxMCpswfqHRyJvErlZA50l5TMZD",
 		"username" => "Richard Kefs",
		"userlastname" => "Kefs",
		"userfirstname" => "Richard",
		"userid" => "10204408720718496",
		"timezone" => "8",
		"application" => "facebook",
		"type_action" => "Login",
		"email" => "richard@kefs.me",
		"password" => ""
		);
		
foreach($fb_data as $key => $value)
	$_POST[$key] = $value;
	

require_once "conf/conf.init.inc.php";
require_once "lib/wpdo.inc.php";
require_once "conf/conf.session.inc.php";
require_once "lib/class.login.inc.php";
require_once "lib/class.coding.inc.php";
require_once "lib/class.mail.inc.php";
require_once "lib/class.sms.inc.php";
require_once "lib/class.spool.inc.php";
require_once "lib/class.pushnotif.inc.php";
require_once "lib/class.async.inc.php";

$login = new WY_Login;
$login->init();

$data = new WY_Arg;

// Check Facebook login
if (isset($_POST['authResponse']['userID']) && !empty($_POST['authResponse']['userID'])) {
	$_POST['userid'] = $_POST['authResponse']['userID'];
	$_POST['token'] = $_POST['authResponse']['accessToken'];
}

$data->platform = $_SESSION['login_type'];

if (isset($_REQUEST['type_action']) && $_REQUEST['type_action'] == 'Logout') {
	$_POST['type_action'] = 'Logout';
	$_SESSION['website_logged_out'] = true;
}

if (empty($_POST['type_action'])) {
	header("Location: login.php?message=Invalid Request");
}

$data->timezone = $_POST['timezone'];
$data->userid = $_POST['userid'];
$data->token = $_POST['token'];
$data->socialname = $_POST['socialname'];
$data->platform = isset($_POST['platform']) ? $_POST['platform'] : $_SESSION['login_type'];
$data->type_action = $_POST['type_action'];

if ($data->platform == "facebook" || $data->platform == "linkedin") {
	$data->extrafield = $data->userid . " : " . $data->token;
}

//else if ($data->platform == "twitter")
//    $data->extrafield = $data->userid . " : " . $data->socialname;

$reloadmess = "";
switch ($data->type_action) {
	case 'Logout':
		$login->logout($data);

		$reloadmess = "&state=reload";

		break;

	case 'Login':
		$reloadmess = "&state=login0";
		$data->email = $_POST['email'];
		$data->password = $_POST['password'];

		$_SESSION['website_logged_out'] = false;
		if ($login->process_login("Login", $data) > 0) {
			$reloadmess = "&state=loggedin";
		}

		break;

	case 'Register':
		$data->email = $_POST['email'];
		$data->lastname = ucfirst($_POST['lastname']);
		$data->firstname = ucfirst($_POST['firstname']);
		$data->mobile = $_POST['mobile'];
		$data->country = $_POST['country'];
		$data->password = $_POST['r_password'];
		$data->rpassword = $_POST['r_rpassword'];
		if ($login->process_login("Register", $data) > 0) {
			$reloadmess = "&state=registered";
		}

		break;

	case 'LostPassword':
		$data->email = $_POST['email'];
		$login->process_login("LostPassword", $data);
		break;

	case 'UpdatePassword':
		$data->email = $_POST['email'];
		$data->password = $_POST['u_password'];
		$data->rpassword = $_POST['u_rpassword'];
		$data->npassword = $_POST['u_npassword'];
		$login->process_login("UpdatePassword", $data);
		break;
	default:
}

if ($data->type_action == 'Login' && empty($login->msg)) {
	if (empty($data->platform) || $data->platform != "twitter") {
		header("Location: login.php?message=" . $login->msg . "&platform=" . $data->platform . $reloadmess);
	} else {
		header("Location: ../../login.php?message=" . $login->msg . "&platform=" . $data->platform . $reloadmess);
	}

} else {
	$_SESSION['info_message']['type'] = 'success';
	if ($data->type_action == 'Login') {
		$_SESSION['info_message']['type'] = 'danger';
	}

	if (isset($login->msg) && $login->msg != '') {
		$_SESSION['info_message']['message'] = $login->msg;
	}

	if ($_SESSION['login_type'] == 'backoffice') {
        $url = __ROOTDIR__ . "/backoffice/index.php";
	} else {
		if ($_SESSION['login_type'] == 'admin_weeloy') {
            $url = __ROOTDIR__ . "/admin_weeloy/index.php";
		} else {
            $url = __ROOTDIR__ . "/index.php";
		}
	}
	header("Location: $url");
}
//if(empty($data->platform) || $data->platform != "twitter")
//	header("Location: login.php?message=" . $login->msg . "&platform=" . $data->platform . $reloadmess);
//else header("Location: ../../login.php?message=" . $login->msg . "&platform=" . $data->platform . $reloadmess);
?>
