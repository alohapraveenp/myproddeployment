<?php
$allowcreate = false;

if (!empty($_COOKIE["weeloy_super"])) {
	if ($_COOKIE["weeloy_super"] == "creator") {
		$allowcreate = true;
	}
}

if ($_SESSION['login_type'] == 'member'
	|| $_SESSION['user']['member_type'] == 'visitor') {
	$allowcreate = true;
	$_SESSION['create_member_type'] = 'member';
}

if (defined(__ROOTDIR__) == false) {
	define(__ROOTDIR__, "../../");
}

?>

<!DOCTYPE html>
<html>
    <head>
        <title>Login</title>
        <!-- Include meta tag to ensure proper rendering and touch zooming -->
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Include bootstrap stylesheets -->
        <style>

.panel{
	border-color:#5285a0 !important;
}

.panel-heading{
	background-image:none !important;
	background-color:#5285a0 !important;
	color:#fff !important;
	border-color:#5285a0 !important;
	border-radius:0;
}
.panel-heading a{
	color: white;
}

.btn-default{
	background-image:linear-gradient(to bottom, #eee 0px, #e0e0e0 100%)!important;
}

.container_modal { margin: 0!important;
                   padding: 0!important;
                  width: 100%!important; }

.logmenu {
	float:right;
	font-size: 80%;
	position: relative;
	top:-10px
}

.drawer {
	display:none;
	padding: 0;
}


</style>
</head>

<body>
<script>

   var gStatus = 'not connected';
   
  // This is called with the results from from FB.getLoginStatus().
  function statusChangeCallback(response) {
    console.log('statusChangeCallback');
    console.log(response);
    // The response object is returned with a status field that lets the
    // app know the current login status of the person.
    // Full docs on the response object can be found in the documentation
    // for FB.getLoginStatus().
    if (response.status === 'connected') {
      // Logged into your app and Facebook.
      gStatus = 'connected';
      getInfoAPI(response.authResponse.accessToken);
    } else if (response.status === 'not_authorized') {
      // The person is logged into Facebook, but not your app.
      document.getElementById('status').innerHTML = 'Please log ' +
        'into this app.';
    } else {
      // The person is not logged into Facebook, so we're not sure if
      // they are logged into this app or not.
      document.getElementById('status').innerHTML = 'Please log ' +
        'into Facebook.';
    }
  }

  // This function is called when someone finishes with the Login
  // Button.  See the onlogin handler attached to it in the sample
  // code below.
  function checkLoginState() {
    FB.getLoginStatus(function(response) {
      statusChangeCallback(response);
    });
  }

  window.fbAsyncInit = function() {
  FB.init({
    appId      : '1590587811210971',
    status     : false,
    cookie     : true,  // enable cookies to allow the server to access 
                        // the session
    xfbml      : true,  // parse social plugins on this page
    version    : 'v2.0' // use version 2.0
  });

  // Now that we've initialized the JavaScript SDK, we call 
  // FB.getLoginStatus().  This function gets the state of the
  // person visiting this page and can return one of three states to
  // the callback you provide.  They can be:
  //
  // 1. Logged into your app ('connected')
  // 2. Logged into Facebook, but not your app ('not_authorized')
  // 3. Not logged into Facebook and can't tell if they are logged into
  //    your app or not.
  //
  // These three cases are handled in the callback function.

  FB.getLoginStatus(function(response) {
    reset();
    statusChangeCallback(response);
  });

  };

  // Load the SDK asynchronously
  (function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = '//connect.facebook.net/en_US/sdk.js';
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));

  // Here we run a very simple test of the Graph API after login is
  // successful.  See statusChangeCallback() for when this call is made.
  function getInfoAPI(token) {
    $('#token').val(token);
    //console.log(token + '\nWelcome!  Fetching your information.... ');
    FB.api('/me', function(response) {
    	$('#userid').val(response.id);
    	$('#email').val(response.email);
    	$('#userfirstname').val(response.first_name);
    	$('#userlastname').val(response.last_name);
	   	$('#username').val(response.name);
    	$('#timezone').val(response.timezone);
    	$('#application').val('facebook');
      console.log('userid = ' + $('#userid').val());
      console.log('gStatus = '+gStatus);
      console.log(JSON.stringify(response));
      //console.log('Successful login for: ' + response.name);
      //document.getElementById('status').innerHTML = 'Thanks for logging in, ' + response.name + '!';
      if(gStatus = 'connected' && response.verified == true && $('#userid').val() != '') 
      	$('#idForm').submit();
    });
  }
  
  function reset() {
	 FB.api(
		"/me/permissions",
		"DELETE",
		function (response) {
		  if (response && !response.error) {
			/* handle the result */
		  	}
		  }
		);
  }

</script>

<?
/*
<script type="text/javascript" src="https://platform.linkedin.com/in.js">
  api_key: 75ig38yk7zjetj
  lang:  en_US
  onLoad: onLinkedInLoad
  authorize: false
</script>

<script src='https://platform.twitter.com/anywhere.js?id=I2sbdNoxjnINDc2PQ9WrXoDHO&v=1' type='text/javascript'></script>
*/
?>
<br />

<form class='form-horizontal' action='<?php printf(__ROOTDIR__);?>/modules/login/redirectURI.php' role='form' id='login_form' name='login_form' method='POST' >
	<input type='hidden' id='token' name='token'>
	<input type='hidden' id='username' name='username'>
	<input type='hidden' id='userlastname' name='userlastname'>
	<input type='hidden' id='userfirstname' name='userfirstname'>
	<input type='hidden' id='userid' name='userid'>
	<input type='hidden' id='timezone' name='timezone'>
	<input type='hidden' id='application' name='application' value='html'>
	<input type='hidden' id='type_action' name='type_action' value='Login'>

	<div class="container container_modal">
		<div style="padding: 0;" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
			<div class="panel panel-info" >
				<div class="panel-heading">
					<div id='logtitle' class="panel-title">Login</div>
					<div class='logmenu'>
						<a href='javascript:;' onClick='showChangePassword();'>Change password</a> |
						<a href='javascript:;' onClick='showLostPassword();'>Forgot password?</a>
					</div>
				</div>

				<div style="padding-top:30px" class="panel-body" >

					<div style="display:none" id="login-alert" class="alert alert-danger col-sm-12"></div>

					<div style="margin-bottom: 25px" class="input-group">
						<span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
						<input id="email" type="text" class="form-control" name="email" value = <?php echo "'" . $prevemail . "'"; ?> placeholder="email" onchange='CleanEmail(this);' onKeyPress="return submitFormWithEnter(this, event, 'signin')">
					</div>

					<div id="loginbox" class='drawer'>
						<div style="margin-bottom: 25px" class="input-group">
							<span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
							<input id="password" name="password" type="password" class="form-control passtype" placeholder="password" onKeyPress="return submitFormWithEnter(this, event, 'signin')">
						</div>
					</div>

					<div id="signupbox" class='drawer'>
						<div style="margin-bottom: 25px" class="input-group">
							<span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
							<input id="firstname" name="firstname" type="text" class="form-control" value="" onchange='CleanText(this);' placeholder="firstname">
						</div>

						<div style="margin-bottom: 25px" class="input-group">
							<span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
							<input id="lastname" name="lastname" type="text" class="form-control" value="" onchange='CleanText(this);' placeholder="lastname">
						</div>

						<div style="margin-bottom: 25px" class="input-group">
							<span class="input-group-addon"><i class="glyphicon glyphicon-earphone"></i></span>
							<input id="mobile" name="mobile" type="text" class="form-control" value="" onchange='CleanTel(this);' placeholder="mobile">
						</div>

						<div style="margin-bottom: 25px" class="input-group">
							<span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
							<input id="r_password" name="r_password" type="password" class="form-control passtype" placeholder="password">
						</div>

						<div style="margin-bottom: 25px" class="input-group">
							<span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
							<input id="r_rpassword" name="rpassword" type="password" class="form-control passtype" placeholder="retypepassword">
						</div>
					</div>

					<div id="chgpassbox" class='drawer'>
						<div style="margin-bottom: 25px" class="input-group">
							<span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
							<input id="u_password" name="u_password" type="text" class="form-control passtype" placeholder=" old password">
						</div>

						<div style="margin-bottom: 25px" class="input-group">
							<span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
							<input id="u_npassword" name="u_npassword" type="text" class="form-control passtype" placeholder="new password">
						</div>

						<div style="margin-bottom: 25px" class="input-group">
							<span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
							<input id="u_rpassword" name="u_rpassword" type="text" class="form-control passtype" placeholder="retype password">
						</div>

					</div>

					<div id="showallpass" class='drawer'>
						<div id='showpass' class="checkbox showpass" style="margin-bottom: 45px" >
							<span class="character-checkbox" onclick="showPassword()"></span>
							<span class="labelp">Show password</span>
						</div>
					</div>


					<div class="form-inline" style="margin-bottom: 25px">
						<div class="form-group">
							<div class="col-sm-12 controls">
								<a href='javascript:;' id="btn-login" class="btn btn-info" >Login  </a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							</div>
						</div>
						<!--
							<div id='remember' class="checkbox">
								<span class="rm-checkbox" onclick="remberme()"></span>
								<span class="labelr">Remember me</span>
							</div>
						-->
					</div>


					<div id='registerTag' style='display:none;'>
					<div class='form-group'><div class='col-md-12 control'><div style='border-top: 1px solid#888; padding-top:15px; font-size:85%' >
					Don't have an account! <a href='#' onClick='ShowSignUp();'> Register Here </a>
					</div></div></div>

					</div>

<div id='facebook'>
	<div class='form-group'>
			<div class='col-md-12 control'>
		<fb:login-button scope="public_profile,email" data-size='large' onlogin="checkLoginState();"></fb:login-button>
	</div>
	</div>
</div>

<?php
/*
<br />
printf("<script type="IN/Login"><input type='hidden' name='linkedin-id' value='<%s js= id %s>';/></script>", "?", "?");

<br />
<span id='login'></span>
<br />
<a href='./twitteroauth/redirect.php'><img src='./twitteroauth/images/lighter.png' alt='Sign in with Twitter'/></a>
</div>
 */
?>
				</div>
			</div>
		</div>
	</div>

</form>

<script type="text/javascript">

    $( document ).ready(function() {
		email = getlgcookie("weeloy_backoffice_login");
		if(email != "" && IsEmail(email))
			$('#email').val(email);
		});
    $('#loginModal').on('shown.bs.modal', function () {
    	try {
    		 FB.XFBML.parse(document.getElementById('loginModal'));
    	} catch( e ) {
    		console.log('Cannot parse XFBML: ',e);
    	}

	});
<?

/*
function onLinkedInLoad() {
	IN.Event.on(IN, "auth", onLinkedInAuth);
}

function onLinkedInAuth() {
	IN.API.Profile("me").fields("firstName", "lastName", "id", "email-address", "phone-numbers").result(displayProfiles);
}
*/
?>

function displayProfiles(profiles) {
	response = profiles.values[0];
	console.log(JSON.stringify(response));
	$('#userid').val(response.id);
	$('#userfirstname').val(response.firstName);
	$('#userlastname').val(response.lastName);
	$('#email').val(response.emailAddress);
	$('#application').val('linkedin');
	$('#login_form').submit();
}

<?
echo "var cState = '" . $_REQUEST['state'] . "';";
printf("var allowcreate = %s;", ($allowcreate)? "true" : "false");

if ($allowcreate){ echo "function register() { $('#type_action').val('Register'); if(checkSubmit()) $('#login_form').submit(); }"; }
?>

if(cState != "register")
	ShowSignIn();
else ShowSignUp();


function signin() {
	$('#type_action').val("Login");
	if (checkSubmit())
		$('#login_form').submit();
}

function lostpassword() {
	$('#type_action').val("LostPassword");
	if (checkSubmit())
		$('#login_form').submit();
}
function updatepassword() {
	$('#type_action').val("UpdatePassword");
	if (checkSubmit())
		$('#login_form').submit();
}

function IsEmail(email) {
	var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
	return regex.test(email);
}

function remberme() {
	$('#remember').toggleClass('show');
}

if(allowcreate) $('#registerTag').show();

function ShowSignIn() {

	$('.passtype').attr('type', 'password');
	$('.showpass').removeClass('show');

	$('.drawer').hide();
	$('#showallpass').show();
	$('#loginbox').show();

	$('#btn-login').html('Login');
	$('#btn-login').unbind('click');
	$('#btn-login').click(function() { signin(); } );
	$('.panel-title').html('Login')
	$('.logmenu').html("<a href='javascript:;' onClick='showChangePassword();'>Change password</a> | <a href='javascript:;' onClick='showLostPassword();'>Forgot password?</a>")

	return false;
}

function ShowSignUp() {

	$('.passtype').attr('type', 'password');
	$('.showpass').removeClass('show');

	$('.drawer').hide();
	$('#showallpass').show();
	$('#signupbox').show();

	$('#btn-login').html("<i class='icon-hand-right'></i> &nbsp Register");
	$('#btn-login').unbind('click');
	$('#btn-login').click(function() { register(); } );
	$('.panel-title').html('Register')
	$('.logmenu').html("<a id='signinlink' href='#' onclick='ShowSignIn();'>Sign In</a>")

	return false;
}

function showChangePassword() {

	$('.passtype').attr('type', 'password');
	$('.showpass').removeClass('show');

	$('.drawer').hide();
	$('#showallpass').show();
	$('#chgpassbox').show();

	$('#btn-login').html("<i class='icon-hand-right'></i> &nbsp Save");
	$('#btn-login').unbind('click');
	$('#btn-login').click(function() { updatepassword(); } );
	$('.panel-title').html('Change password')
	$('.logmenu').html("<a id='signinlink' href='#1' onclick='ShowSignIn();'>Sign In</a>")

	return false;
}

function showLostPassword() {

	$('.drawer').hide();

	$('#btn-login').html("<i class='icon-hand-right'></i> &nbsp Retrieve");
	$('#btn-login').unbind('click');
	$('#btn-login').click(function() { lostpassword(); } );
	$('.panel-title').html('Forgot password')
	$('.logmenu').html("<a id='signinlink' href='#1' onclick='ShowSignIn();'>Sign In</a>")

	return false;
}


function showPassword() {
	if ($('#password').attr('type') != 'text') {
		$('.passtype').attr('type', 'text');
		$('.showpass').addClass('show');
	} else {
		$('.passtype').attr('type', 'password');
		$('.showpass').removeClass('show');
	}
	return false;
}

function CleanEmail(obj) { obj.value = obj.value.replace(/[!#$%^&*()=}{\]\[:;><\?/|\\]/g, ''); }
function CleanTel(obj) { obj.value = obj.value.replace(/[^0-9 \+]/g, ''); }
function CleanPass(obj) { obj.value = obj.value.replace(/[^0-9A-Za-z]/g, ''); }
function CleanText(obj) { obj.value = obj.value.replace(/[!@#$%^&*()=}{\]\[\"\':;><\?/|\\]/g, ''); }
function invalid(str) { alert(str); return false; }

function checkSubmit() {

	action = $('#type_action').val();
	$('#login_form input[type=text], input[type=password]').each(function () {
		this.value = this.value.trim();
	});

	// always
	if ($('#email').val() == "" || !IsEmail($('#email').val()))
		return invalid('Invalid email, please try again');

	if (action == "Login") {
		if ($('#password').val().length < 6)
			return invalid('Invalid password, please try again');
		setlgcookie("weeloy_backoffice_login", $('#email').val(), 7);

	}

	else if (action == "LostPassword") {
	}

	else if (action == "UpdatePassword") {
		if ($('#u_password').val() == "")
			return invalid('Invalid password , please try again');
		if ($('#u_rpassword').val() == "")
			return invalid('Invalid new password, please try again');

            var rpassword = $('#u_rpassword').val();
            if (rpassword == "")
			return invalid('Invalid new password, please try again');
            if(rpassword.length< 6){
                  return invalid('Password too short. The new password must be between 6 and 12 car');
            }

            if(rpassword.length> 12){
                  return invalid('Password too long. The new password must be between 6 and 12 car');
            }
                if ($('#u_npassword').val() == "")
			return invalid('Invalid new retypepassword, please try again');
		if ($('#u_rpassword').val() != $('#u_npassword').val())
			return invalid('Password do not match, try again');
	}

	else if (action == "Register") {
		if ($('#lastname').val() == "")
			return invalid('Invalid name, try again');
		if ($('#firstname').val() == "")
			return invalid('Invalid firstname, try again');
		if ($('#mobile').val() == "")
			return invalid('Invalid tel, try again');
		if ($('#country').val() == "")
			return invalid('Invalid country, try again');
		if ($('#r_password').val() == "")
			return invalid('Invalid password,, try again');
		if ($('#r_rpassword').val() == "")
			return invalid('Invalid retype password, try again');
		if ($('#r_password').val() != $('#r_rpassword').val())
			return invalid('password and retype password are different, try again');
	}
	return true;
}

function setlgcookie(name, value, duration){
	value = escape(value);
	if(duration){
		var date = new Date();
		date.setTime(date.getTime() + (duration * 24 * 3600*1000));
		value += "; expires=" + date.toGMTString();
	}
	document.cookie = name + "=" + value;
}

function getlgcookie(name){
	value = document.cookie.match('(?:^|;)\\s*'+name+'=([^;]*)');
	return value ? unescape(value[1]) : "";
}


</script>

<script type="text/javascript">
	function submitFormWithEnter(myfield, e, target)
	{
		var keycode;
		if (window.event)
		{
			keycode = window.event.keyCode;
		}
		else if (e)
		{
			keycode = e.which;
		}
		else
		{
			return true;
		}

		if (keycode == 13)
		{
			if (target == 'signin')
			{
				signin();
			}
			return false;
		}
		else
		{
			return true;
		}
	}
</script>

    </body>
</html>
