<?php
require_once("lib/class.upload.inc.php");
require_once("import_mbs_member.php");
$upload = new WY_Upload(sys_get_temp_dir());
$filePath = $upload->Upload();
RetrieveAndSaveProfileData('datamember.txt', 'SG_SG_R_TheFunKitchen', array("Flash Card", "Name", "Job Title / Company", "Contact Number", "Remarks", "Special Notes", "Description", "Email Address", "Mailing Address"), array("flash", "fullname", "company", "mobile", "remark", "notes", "description", "email", "address"), array("fullname", "company", "mobile", "email", "remark", "notes", "description", "content"));
 if (isset($filePath) && !empty($filePath) && file_exists($filePath) && filesize($filePath)) {
 	$type = mime_content_type($filePath);
 	echo "MIME content type: ".$type."<br/>";
	// Strip first 4 and last 5 lines
	if ($type === "application/octet-stream") {
		echo "Extracting data...<br/>";
		$lines = file($filePath);
		$lines = array_slice($lines, 4, count($lines) - 9);
		// Write to file
		$file = fopen($filePath.".xlsx", 'w');
		$filePath .= ".xlsx";
		fwrite($file, implode('', $lines));
		fclose($file);
	}
	if ($type === "application/vnd.ms-excel" || $type === "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" || $type === "application/octet-stream")
		RetrieveAndSaveProfileImages($filePath, 'B', 'C', 'E', 'J');
 } else
 	echo "Upload failed!<br/>";
?>