<?php 

require_once("conf/conf.init.inc.php");
require_once("lib/wpdo.inc.php");
require_once("conf/conf.session.inc.php");
require_once("lib/wglobals.inc.php");
require_once("lib/class.media.inc.php");
require_once("lib/class.restaurant.inc.php");
require_once("lib/class.booking.inc.php");

set_time_limit(900);

$starting = microtime(true);

$theRestaurant = "SG_SG_R_Bacchanalia";
$bkg = new WY_Booking();

$nbline = 0;
$labelAr = array("index", "RUID", "rconfirmation", "cdate", "rdate", "cover", "children", "fullname", "email", "mobile", "Tag", "request", "status", "optin", "specialnotes", "notes", "tracking", "booker", "tablename");
$field = array("cdate", "rdate", "rtime", "cover", "salutation", "firstname", "lastname", "email", "mobile", "status", "optin", "generic", "specialrequest", "tracking", "booker", "type", "tablename");
$namelabel = array("lastname", "firstname", "salutation");


foreach(file('data1.txt') as $line) {
	if($nbline++ == 0) continue;
	//if($nbline !== 2046) continue;
	$lineAr = explode(",", $line);
	for($i = 0; $i < count($field); $i++)
		${$field[$i]} = "";

	for($i = 0; $i < count($labelAr); $i++) {
		$nn = $lineAr[$i];
		$nn = preg_replace("/\"|\'/", "", $nn); 
		$nn = preg_replace("/\s+/", " ", $nn); 
		${$labelAr[$i]} = trim($nn);
		}
		
	$type = "import";

	for($i = 0; $i < count($labelAr); $i++) {
		$tt = $labelAr[$i];

		if($tt == "email" && empty($$tt))
			$$tt = "no@email.com";

		if($tt == "mobile" && !empty($$tt)) {
			$$tt = preg_replace("/\+[^+]+\+/", "+", $$tt);
			$$tt = preg_replace("/\+0\s*/", "+65 ", $$tt);
			}
		if($tt == "mobile" && (empty($$tt) || $$tt == "+65 "))
			$$tt = "+65 99999999";		

		if(($tt == "tracking" && $tracking == "chope") || ($tt == "booker" && $booker == "chope")) {
			$tracking = "remote";
			$booker = "CHOPE";
			}

		if($tt == "rconfirmation" && !empty($rconfirmation)) 
			$generic .= (!empty($generic) ? ", " : "") . "’remotebooking’:’" . $rconfirmation . "’";

		if($tt == "notes" && !empty($notes)) 
			$generic .= (!empty($generic) ? ", " : "") . "’notescode’:’" . $notes . "’";
			
		if($tt == "children" && intval($children) > 0) 
			$generic .= (!empty($generic) ? ", " : "") . "’children’:’" . $children . "’";
			
		if($tt == "specialnotes" && !empty($specialrequest)) 
			$specialrequest .= (!empty($specialrequest) ? ", " : "") . $specialnotes;
			
		if($tt == "request" && !empty($request)) 
			$specialrequest .= (!empty($specialrequest) ? ", " : "") . $request;
			
		if($tt == "fullname" && $$tt == "Walk-In") {
			$fullname = "";
			$tracking .= (!empty($tracking) ? "|" : "") . "walkin";
			}			

		if($tt == "fullname") {
			if(empty($$tt)) {
				$firstname = "nofirstname";	
				$lastname = "nolastname";
				}
			else {
				$tmpAr = array_reverse(explode(" ", $fullname));
				for($k = 0; $k < count($tmpAr) && $k < 3; $k++)
					${$namelabel[$k]} = $tmpAr[$k];
				}
			}

		if($tt == "status") {
			 if($status == "Cancelled") $status = "cancel";
			 else if($status != "cancel") $status = "";
			}

		if($tt == "tracking" && ($$tt == "Dashboard" || $$tt == "restaurant"))
			$$tt = "";
		if($tt == "status" && $$tt == "New")
			$$tt = "";
		if($tt == "cdate") {
			$$tt = preg_replace("/[^\d\-: ]/", "", $$tt);
			}
		if($tt == "rdate") {
			$$tt = preg_replace("/[^\d\-: ]/", "", $$tt);
			$tmpAr = explode(" ", $$tt);
			$rdate = $tmpAr[0];
			$rtime = $tmpAr[1];
			}
		if($tt == "tablename") {
			$$tt = preg_replace("/[^\d;]/", "", $$tt);
			$$tt = preg_replace("/;/", ",", $$tt);
			$$tt = preg_replace("/,$/", "", $$tt);
			}
		}
	
	if(!empty($generic))
		$generic = "{ " . $generic . " }";
	
	$fielddata = array();
	echo $nbline;
	for($i = 0; $i < count($field); $i++) {
		$tt = $field[$i];
		$fielddata[$field[$i]] = ${$field[$i]};
		echo ", " .  $field[$i] . "=>" . ${$field[$i]};
		}
	echo "<br /><hr /><br />";
	$bkg->import($theRestaurant, $fielddata, $rconfirmation);
	}
	printf("DONE time = %.2f", (microtime(true) - $starting) );
?>
