
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js"></script>
<script type="text/javascript">
    var Markers = [];
    var image_over = 'images/google-marker.png';
    var image_init = 'images/google-marker-grey.png';
    
    
    function initialize() {

<?php
$arg = preg_replace("/_/", " ", $map_arg);
if (!empty($arg)) {
    $argAr = explode("||", $arg);
    $value = "[\n";
    for ($i = 0, $sep = ""; $i < count($argAr); $i++, $sep = ", \n") {
        $lineAr = explode("|", $argAr[$i]);
        $value .= $sep . "[";
        for ($k = 0, $sep1 = ""; $k < 5; $k++, $sep1 = ", ") {
        	if(!isset($lineAr[$k])) $lineAr[$k] = "";
            if ($k == 0)
                $value .= $sep1 . "'" . $lineAr[$k] . "'";
            else $value .= $sep1 . $lineAr[$k];
        }
        $value .= "]";
    }
    $value .= "]";
}

printf(" var beaches = %s;\n", $value);
?>

        var infowindow = new google.maps.InfoWindow();
        var myzoom = (beaches.length > 2) ? 12 : 13;
        var delta = (beaches.length > 2) ? 0 : 0.05;
        var beach = beaches[0];
        var myLatlng = new google.maps.LatLng(beach[1] - delta, beach[2]);

        var mapOptions = {
            center: myLatlng,
            zoom: myzoom,
            //mapTypeId: google.maps.MapTypeId.TERRAIN,
            scrollwheel: false,
            navigationControl: false,
            mapTypeControl: false,
            scaleControl: false,
            draggable: true
        };

        var mymap = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

        for (var i = 0; i < beaches.length; i++) {
            beach = beaches[i];
            myLatLng = new google.maps.LatLng(beach[1], beach[2]);
            mytitle = (beaches.length > 2) ? beach[0] : beach[0] + '(' + beach[1] + ', ' + beach[2] + ')';
            zindex = beach[3];

            var marker = new google.maps.Marker({
                record_id: beach[4],
                position: myLatLng,
                map: mymap,
                title: mytitle,
                icon: image_init,
                zIndex: zindex
            });
            Markers.push(marker);
            google.maps.event.addListener(marker, 'mouseover', (function (marker, i) {
                return function () {
                    infowindow.setContent(marker.title);
                    infowindow.open(mymap, marker);
                    marker.setIcon(image_over);
                }
            })(marker, i));
            google.maps.event.addListener(marker, 'mouseout', (function (marker, i) {
                return function () {
                    marker.setIcon(image_init);
                    infowindow.close();
                }
            })(marker, i));
        }
    }
    
  $( document ).ready(function() {
    google.maps.event.addDomListener(window, 'load', initialize);
  });
    $("a.pinme").on('mouseover', function () {
        var id = $(this).attr('rel');
        changeMarker(id, image_over, 230000);
    });
    $("a.pinme").on('mouseout', function () {
        var id = $(this).attr('rel');
        changeMarker(id, image_init, 1);
    });

    function changeMarker(record_id, img, zindex) {
        for (i in Markers) {
            if (Markers[i].record_id == record_id) {
                Markers[i].setZIndex(zindex);
                Markers[i].setIcon(img);
                
            }
        }
    }
</script>