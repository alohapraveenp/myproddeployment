<?php
    
    /* Richard Kefs, Weeloy, Copyright (c) 2014 all rights reserved  */

trait CleanIng {
	public static function clean_input($data) {
	  if(!isset($data) || $data == "" ) 
		return "";

		$data = trim($data);
		$data = stripslashes($data);
		if($data != "")
			$data = preg_replace("/\'|\"/", "’", $data);	//\xc2\xb4
		return $data;
	}
	public static function clean_number($data) {
	  if(!isset($data) || $data == "" ) 
		return "";

	  $data = trim($data);
	  $data = stripslashes($data);
	  if($data != "")
		$data = preg_replace( "/[^0-9\.,]/", '', $data );  
  
	  return $data;
	}

	public static function clean_tel($data) {
	  if(!isset($data) || $data == "" ) 
		return "";

	  $data = trim($data);
	  $data = stripslashes($data);
	  if($data != "")
		  $data = preg_replace("/\s+/", " ", $data);
	  if($data != "")
		$data = preg_replace( "/[^0-9\+ ]/", '', $data );  
  
	  return $data;
	}

	public static function cleanpattern($data, $pat) {
	  if(!isset($data) || $data == "" || !isset($pat) || $pat == "" ) 
		return "";

		return str_replace($pat, "", $data);

		}
		
	public static function clean_text($data) {
	  if(!isset($data) || $data == "" ) 
		return "";

	  $data = trim($data);
	  $data = stripslashes($data);
	  if($data != "")
		$data = preg_replace("/\'|\"/", "’", $data);	//\xc2\xb4
	  if($data != "")
		$data = preg_replace( "/[^0-9a-zA-Z\-_\.\@\+\|,;:\(\)\?\*’ ]/", '', $data );    
	  if($data != "")
		  $data = preg_replace("/\s+/", " ", $data);

	  $data = trim($data);  
	  return $data;
	}

	public static function email_validation($email) {		
		if(empty($email))
			return -1;
		if(!preg_match("/([\w\-]+\@[\w\-]+\.[\w\-]+)/", $email))
			return -1;
		return 1;
	}
}

    
function getConnection($db = NULL) {
    try {
        include('../conf/conf.mysql.inc.php');
        
        switch ($db){
            case 'site_data':
                $dbh = new PDO("mysql:host=$dbhost;dbname=$dbname", $dbuser, $dbpass);
                break;
            default :
                $dbh = new PDO("mysql:host=$dbhost;dbname=$dbname", $dbuser, $dbpass);
                break;
        }
        $dbh->exec("set names utf8");
        $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        //$dbh->setAttribute(PDO::MYSQL_ATTR_USE_BUFFERED_QUERY, false);
        return $dbh;
	} catch (PDOException $e) {
		error_log("Database connection failed! ".$e->getMessage());
	}
    return null;
}

function pdo_error($e, $funcname, $sql) {
	$trace = preg_replace("/\'|\"/", "`", $e->getTraceAsString());
	$sql = preg_replace("/\'|\"/", "`", $sql);
	$traceAr = $e->getTrace();
	$file = preg_replace("/^.*\//", "", $traceAr[0]['file']);
	$msg = $e->getMessage() . " - " . $file . " - " . $traceAr[0]['line'];
	
	error_log("pdo_error @".$funcname." sql: ".$sql. " Exception: ".print_r($e, true));
}

function pdo_single_select($sql, $db = '') {

	try {
		$db = getConnection($db);
		$stmt = $db->query($sql);
		if($data = $stmt->fetch(PDO::FETCH_ASSOC)) {
			return $data;
			}
	   } catch(PDOException $e) {
	   	pdo_error($e, "pdo_single_select", $sql);
	}
	return array();
}

function pdo_single_select_with_params($sql, $parameters, $db = '') {
    try {
        $db = getConnection($db);
        $stmt = $db->prepare($sql);
        $stmt->execute($parameters);
        if ($data = $stmt->fetch(PDO::FETCH_ASSOC)) {
            return $data;
        }
    } catch (PDOException $e) {
        pdo_error($e, "pdo_single_select", $sql);
    }
    return false;
}

function pdo_single_select_index($sql, $db = '') {

	try {
		$db = getConnection($db);
		$stmt = $db->query($sql);
		if($data = $stmt->fetch(PDO::FETCH_NUM)) {
			return $data;
			}
	   } catch(PDOException $e) {
	   	pdo_error($e, "pdo_single_select_index", $sql);
		}
	return array();
}

function pdo_multiple_select($sql, $db = '') {

	try {
		$db = getConnection($db);
		$stmt = $db->query($sql);
		if($data = $stmt->fetchAll(PDO::FETCH_ASSOC))
			return $data;
	} catch(PDOException $e) {
		pdo_error($e, "pdo_multiple_select", $sql);
	}
	return array();
}

function pdo_multiple_select_with_params($sql, $parameters, $db = '') {

	try {
		$db = getConnection($db);
        $stmt = $db->prepare($sql);
        $stmt->execute($parameters);
		if($data = $stmt->fetchAll(PDO::FETCH_ASSOC)) {
			return $data;
		}
	} catch(PDOException $e) {
		pdo_error($e, "pdo_multiple_select", $sql);
	}
	return array();
}

function pdo_multiple_select_index($sql, $db = '') {

	try {
		$db = getConnection($db);
		$stmt = $db->query($sql);
		if($data = $stmt->fetchAll(PDO::FETCH_NUM)) {
			$row = array();
			foreach($data as $tmp)
				$row[] = $tmp[0];
			return $row;
			}
	   } catch(PDOException $e) {
		pdo_error($e, "pdo_multiple_select_index", $sql);
		}
	return array();
}

function pdo_multiple_select_numindex($sql, $db = '') {

	try {
		$db = getConnection($db);
		$stmt = $db->query($sql);
		if($data = $stmt->fetchAll(PDO::FETCH_NUM)) {
			return $data;
			}
	   } catch(PDOException $e) {
		pdo_error($e, "pdo_multiple_select_index", $sql);
		}
	return array();
}

function pdo_update($sql, $parameters = NULL, $db = '') {

	try {
		$db = getConnection($db);
                $stmt = $db->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
                $stmt->execute($parameters);
                $affected_rows = 1;
		//$affected_rows = $db->exec($sql);
	   } catch(PDOException $e) {
	        pdo_error($e, "pdo_column", $sql);
               //PHIL gestion des erreurs.
        	return "error ". $e->getMessage() . ' -> ' . $sql;               
		}
	return $affected_rows;
}

function pdo_column($sql) {

    try {
        $db = getConnection();
		$stmt = $db->query($sql);
        return $stmt->fetchAll(PDO::FETCH_COLUMN, 0);
    } catch(PDOException $e) {
        pdo_error($e, "pdo_column", $sql);
    }
	return array();
}

function pdo_exec($sql, $db = '') {

	try {
		$db = getConnection($db);
		$affected_rows = $db->exec($sql);
	   } catch(PDOException $e) {
	        pdo_error($e, "pdo_exec", $sql);
               //PHIL gestion des erreurs.
        	return "error ". $e->getMessage() . ' -> ' . $sql;               
		}
	return $affected_rows;
}

function pdo_exec_with_params($sql, $parameters, $db = '') {

	try {
            
            $db = getConnection($db);
            $stmt = $db->prepare($sql);
            $affected_rows = $stmt->execute($parameters);

	   } catch(PDOException $e) {
	        pdo_error($e, "pdo_exec", $sql);
               //PHIL gestion des erreurs.
        	return "error ". $e->getMessage() . ' -> ' . $sql;               
		}
	return $affected_rows;
}

function pdo_insert_unique($sql, $in_sql, $db = '') {

   try {
		$db = getConnection($db);
		$stmt = $db->query($sql);
		$row_count = $stmt->rowCount();
		if($row_count > 0)
			return -1;			
		$result = $db->exec($in_sql);
		return 1;
	   } catch(PDOException $e) {
        pdo_error($e, "pdo_insert_unique", $sql);
		}
}

function pdo_insert($sql, $parameters = NULL) {

   try {
		$db = getConnection();
		if(!isset($parameters) || $parameters == ""){
			$stmt = $db->prepare($sql);
			$stmt->execute();
		}else{
			$stmt = $db->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
			$stmt->execute($parameters);
		}
		$id = $db->lastInsertId();
		return $id;
	   } catch(PDOException $e) {
        pdo_error($e, "pdo_insert", $sql);
		}
	return -1;
}



function pdo_multiple_obj($sql) {

    try {
        $db = getConnection();
  		$stmt = $db->query($sql);
        return $stmt->fetchAll(PDO::FETCH_OBJ);
    } catch(PDOException $e) {
        pdo_error($e, "pdo_multiple_obj", $sql);
    }
	return NULL;
}

function pdo_settimezone($db = '') {
	// timezone names are not set up Asia/Singapore
	return pdo_exec("SET time_zone = '+8:00'", $db);
}

function list_table_field($table) {

    try {
		$db = getConnection();
		$stmt = $db->query("DESCRIBE $table");
		return $stmt->fetchAll(PDO::FETCH_COLUMN);	
    } catch(PDOException $e) {
        pdo_error($e, "list_table_field", "");
    }
	return NULL;
}

function list_table_names() {

    try {
		$db = getConnection();
		$stmt = $db->query("SHOW TABLES");
		return $stmt->fetchAll(PDO::FETCH_COLUMN);	
    } catch(PDOException $e) {
        pdo_error($e, "list_table_names", "");
    }
	return NULL;
}


function clean_input($data) {
  if(!isset($data) || $data == "" ) 
	return "";

  $data = trim($data);
  $data = stripslashes($data);
  if($data != "")
	  $data = preg_replace("/\'|\"/", "’", $data);	//\xc2\xb4,  ”
  //$data = htmlspecialchars($data);
  return $data;
}

function clean_number($data) {
  if(!isset($data) || $data == "" ) 
	return "";

  $data = trim($data);
  $data = stripslashes($data);
  if($data != "")
  	$data = preg_replace( "/[^0-9\-\+\.\,]/", '', $data );  
  
  return $data;
}

function clean_tel($data) {
  if(!isset($data) || $data == "" ) 
	return "";

  $data = trim($data);
  $data = stripslashes($data);
  if($data != "")
  	$data = preg_replace( "/[^0-9\+ ]/", '', $data );  
  
  return $data;
}

function clean_text($data) {
  if(!isset($data) || $data == "" ) 
	return "";

  $data = trim($data);
  $data = stripslashes($data);
  if($data != "")
  	$data = preg_replace( "/[^0-9a-zA-Z-_\.\@ ]/", '', $data );  
  
  if($data != "")
	  $data = preg_replace("/\s+/", " ", $data);
  $data = trim($data);
  
  return $data;
}

function getnchoice($divider, $limit) {

	$choice[] = rand(0, $limit - 1);
	for($k = 0; count($choice) < $divider && $k < 100; $k++) {
	 	$m = rand(0, $limit - 1);
		if(!in_array($m, $choice))
			$choice[] = $m;
		}
		
	return $choice;
}

function w_checkdate($date1, $date2) {

	// $date2 >= $date1
	
	$cyear = intval(date("Y"));
	if(preg_match('/\d{4}-\d{1,2}-\d{1,2}/', $date1) == false)
		return -1;
	if(preg_match('/\d{4}-\d{1,2}-\d{1,2}/', $date2) == false)
		return -2;
	
	$d1 = explode("-", $date1);
	$d2 = explode("-", $date2);
	$year1 = intval($d1[0]);
	$month1 = intval($d1[1]);
	$day1 = intval($d1[2]);
	$year2 = intval($d2[0]);
	$month2 = intval($d2[1]);
	$day2 = intval($d2[2]);
	
	if($cyear != $year1 || ($cyear != $year2 && $cyear != $year2 - 1))
		return -3;
	if(checkdate($month1, $day1, $year1) == false)
		return -4;
	if(checkdate($month2, $day2, $year2) == false)
		return -5;

	if(mktime(0, 0, 0, $month1, $day1, $year1) > mktime(0, 0, 0, $month2, $day2, $year2))
		return -6;
	
	return 1;
}

function email_validation($email) {		
	if(empty($email))
		return -1;
	if(!preg_match("/([\w\-]+\@[\w\-]+\.[\w\-]+)/", $email))
		return -1;
	return 1;
	}

?>