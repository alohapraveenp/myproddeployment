<?php
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST, OPTIONS');
header('Access-Control-Allow-Headers: origin, x-requested-with, content-type');

// require_once "conf/conf.init.inc.php";
// require_once "lib/wpdo.inc.php";
require_once (($_SERVER['DOCUMENT_ROOT']).'/lib/wpdo.inc.php');
// require_once "lib/wglobals.inc.php";

// require_once "../lib/class.translation.inc.php";
require_once (($_SERVER['DOCUMENT_ROOT']).'/lib/class.translation.inc.php');

require 'Slim/Slim.php';

\Slim\Slim::registerAutoloader();

$app = new \Slim\Slim();

$app->group('/translation', function () use ($app) {
	// error_log('message ' . ($_SERVER['DOCUMENT_ROOT']). 'api/services_routes/translation/translation.php');
	// header("Access-Control-Allow-Origin: http://translation.weeloy.com");
    require ('services_routes/translation/translation.php');
});

$app->run();
    
    /* add platform */
/*
 $status: 0/-1 : fail 1: success
 */
function format_api($status, $data, $count, $errors, $header_status = 200) {  
	set_header_status($header_status);
	
	$resultat['status'] = $status;
	$resultat['data'] = $data;
	$resultat['count'] = $count;
	$resultat['errors'] = $errors;
	return json_encode($resultat);
}
    
function set_header_status($code) {
	$status = array(
		100 => 'Continue',
		101 => 'Switching Protocols',
		200 => 'OK',
		201 => 'Created',
		202 => 'Accepted',
		203 => 'Non-Authoritative Information',
		204 => 'No Content',
		205 => 'Reset Content',
		206 => 'Partial Content',
		300 => 'Multiple Choices',
		301 => 'Moved Permanently',
		302 => 'Found',
		303 => 'See Other',
		304 => 'Not Modified',
		305 => 'Use Proxy',
		306 => '(Unused)',
		307 => 'Temporary Redirect',
		400 => 'Bad Request',
		401 => 'Unauthorized',
		402 => 'Payment Required',
		403 => 'Forbidden',
		404 => 'Not Found',
		405 => 'Method Not Allowed',
		406 => 'Not Acceptable',
		407 => 'Proxy Authentication Required',
		408 => 'Request Timeout',
		409 => 'Conflict',
		410 => 'Gone',
		411 => 'Length Required',
		412 => 'Precondition Failed',
		413 => 'Request Entity Too Large',
		414 => 'Request-URI Too Long',
		415 => 'Unsupported Media Type',
		416 => 'Requested Range Not Satisfiable',
		417 => 'Expectation Failed',
		500 => 'Internal Server Error',
		501 => 'Not Implemented',
		502 => 'Bad Gateway',
		503 => 'Service Unavailable',
		504 => 'Gateway Timeout',
		505 => 'HTTP Version Not Supported');
        
        header("HTTP/1.1 $code " . $status[$code]);
        return true;
    }
?>