missionBlock = [
	{ index: [0], title: "Pilotage de la Performance", anchor: "card1" },
	{ index: [1], title: "Stratégie et Offre", anchor: "card2" },
	{ index: [2,3,4], title: "Organisation et Transformation", anchor: "card3" },
	{ index: [5,6], title: "Systèmes d'Information", anchor: "card4" },
	{ index: [7,8], title: "Gestion de RH", anchor: "card5" },
	{ index: [9, 10, 11], title: "Relation Client", anchor: "card6" }
	];

missionData = [    
	{ 
	img: "mission15.jpg",
	title: "Pilotage de la Performance", 
	subtitle: "Evaluation des politiques publiques et de l'action de l'administration", 
	citation: "« Mesurer l’impact des politiques publiques, l’efficacité de l’action de l’administration et son efficience »", 
	contenu: [ "Structuration du schéma directeur d'une collectivité (missions, objectifs stratégiques, actions)",
			"Identification et élaboration des indicateurs de résultat et de suivi de l’activité",
			"Planification pluri-annuelle des moyens",
			"Définition du cadre de gestion et élaboration du système de pilotage"]
	},
	{ 
	img: "mission22.jpg",
	title:"Stratégie et Offre", 
	subtitle: "Modélisation et Business Plan", 
	citation: "« Modéliser les couts et les résultats d’une nouvelle activité en identifiant l’impact de telle ou telle composante du marché sur le résultat final »", 
	contenu: ["Définition de la stratégie associée au lancement de la nouvelle activité", 
		"Dimensionnement du marché (taux de pénétration, cibles client, part de marché)", 
		"dentification et modélisation des couts associés aux moyens nécessaires à l’activité", 
		"Simulation des conditions d’entrée sur le marché (investissements, fonctionnement) et du Taux de retour sur Investissement", 
		"Analyse et mise à jour du business plan d'organisation" ]
	},
	{ img: "mission3.jpg",
	title:"Organisation et Transformation I", 
	subtitle: "Modélisation et Business Plan", 
	citation: "« Identifier les dysfonctionnements et formuler des recommandations »", 
	contenu: ["Analyse de l’existant (organisation, compétences, moyens, activité, résultats, …)", 
		"Redéfinition des missions / de la stratégie", 
		"Identification des dysfonctionnements et des avancées majeures", 
		"Evaluation du management des équipes et du pilotage de l’activité", 
		"Formulation de recommandations d’évolution", 
		"Présentation des conclusions du diagnostic à la direction, aux collaborateurs et à leur encadrement" ]
	},
	{ 
	img: "mission4.jpg",
	title:"Organisation et Transformation II", 
	subtitle: "Diagnostic d’organisation et de fonctionnement", 
	citation: "« Revisiter les processus métier dans un objectif d’amélioration de la performance »", 
	contenu: ["Cartographie des processus existants", 
		"Formalisation des procédures et instructions de travail", 
		"Proposition de réorganisation des processus métier", 
		"Adaptation de l’organisation à ces nouveaux processus", 
		"Mise en place d’indicateur de mesure de la performance" ],
	extra:[1, 2, 3]
	},
	{ 
	img: "mission5.jpg",
	title:"Organisation et Transformation III", 
	subtitle: "Analyse et refonte de processus", 
	citation: "« Mobiliser votre organisation vers de nouveaux objectifs en veillant à la bonne appropriation des changements par vos collaborateurs »", 
	contenu: ["Identification de la situation de départ, des problématiques posées", 
		"Définition des orientations stratégiques et de la cible organisationnelle", 
		"Animation de groupes permettant d’identifier les attentes et freins au changement", 
		"Matérialisation des risques encourus à moyens termes en cas d’immobilisme", 
		"Mobilisation des collaborateurs autours de chantiers et groupes de travail", 
		"Validation concertée des nouvelles orientations par les parties prenantes", 
		"Communication interne", 
		"Suivi et matérialisation des succès", 
		"Pilotage et reporting au comité de direction" ]
	},
	{ 
	img: "mission18.jpg",
	title:"Systemes d'Information I", 
	subtitle: "Schéma directeur informatique", 
	citation: "« Donner une représentation schématique des applications, infrastructures et données gérées et la faire évoluer pour correspondre aux nouveaux besoins »", 
	contenu: ["Cartographie des processus métiers principaux avec identification des entrées et sorties", 
		"Cartographie des applications existantes et de leur inter relations", 
		"Description de l’infrastructure matériel et logiciel", 
		"Identification de l’évolution des besoins", 
		"Formalisation de la stratégie et des objectifs attendus en matière d’informatique", 
		"Evaluation des moyens et solutions techniques nécessaires" ]
	},
	{ 
	img: "mission7.jpg",
	title:"Systemes d'Information II", 
	subtitle: "Cahier des charges informatique", 
	citation: "« Décrire de façon détaillée les fonctionnalités, données et règles de gestion attendues d’un nouvel applicatif pour qu’il puisse s’intégrer au sein de l’environnement technique déjà existant »", 
	contenu: ["Expression des besoins fonctionnels des utilisateurs classés et ordonnés", 
		"Liste des fonctionnalités, des données et des règles de gestion associées", 
		"Description technique de la solution et qualité de service attendue", 
		"Identifications des contraintes techniques ou fonctionnelles liées à l’existant", 
		"Conditions de marché (délai, paiement, pénalités, gestion des droits...)" ]
	},
	{ 
	img: "mission8.jpg",
	title:"Gestion de RH I",
	subtitle: "Recrutement de cadres dirigeants", 
	citation: "« Identifier et sélectionner les talents qui seront moteurs de croissance pour nos clients »", 
	contenu: ["Définition du profil de poste recherché", 
		"Proposition d'une stratégie de recherche", 
		"Elaboration de l'annonce", 
		"Dépouillement et sélection des curriculum vitae", 
		"Conduite des entretiens et sélection des candidats" ]
	},
	{ 
	img: "mission9.jpg",
	title:"Gestion de RH II",
	subtitle: "Ingénierie de formation", 
	citation: "« Accélérer la courbe d'apprentissage et permettre l'appropriation du changement à tous les niveau de l'organisation »", 
	contenu: ["Structuration d'un ‘plan support utilisateurs’ (définition des objectifs pédagogiques, identification des cibles, des contenus et des moyens)", 
		"Construction du dispositif de formation", 
		"Elaboration des supports de formation", 
		"Identification de l’évolution des besoins", 
		"Dispense des formations" ]
	},
	{ 
	img: "mission6.jpg",
	title:"Relation Client I",
	subtitle: "Centres de Contacts Clients", 
	citation: "« Organiser le traitement de vos contacts clients de façon à améliorer la qualité de la relation avec ces derniers »", 
	contenu: ["Analyse de l’organisation existante", 
		"Construction de scénarios d’organisation", 
		"Conception de l’organisation cible", 
		"Dimensionnent de centres d’appels", 
		"Déploiement du projet de mise en place de la nouvelle organisation" ]
	},
	{ 
	img: "mission11.jpg",
	title:"Relation Client II",
	subtitle: "Analyse de la qualité de la relation client", 
	citation: "« Mettre le client au coeur de vos préoccupations et de votre organisation »", 
	contenu: ["Mise en place de clients mystères permettant d'identifier les failles de la relation client", 
		"Questionnaires de satisfaction", 
		"Analyse des échanges avec le client tout au long de sa vie de client au sein de l'entreprise", 
		"Atelier interne d'identification des améliorations à apporter" ]
	},
	{ 
	img: "mission19.jpg",
	title:"Relation Client III",
	subtitle: "Politique de service client", 
	citation: "« Définir les critères sur lesquels vous engager en termes de qualité du service offert à vos clients »", 
	contenu: ["Analyse des niveaux de service actuels", 
		"Construction des indicateurs de qualité", 
		"Définition des niveaux d'engagement par critère de qualité de service", 
		"Formalisation de la politique de service client et de la charte client associée" ]
	}
	];
	
categories = [{ "icon":"fa fa-university", "title":"Services publics", "content":"Administrations, Recherche, Services à la population"}, { "icon":"fa fa-plane", "title":"Transport", "content":"Urbain, Aérien"}, { "icon":"fa fa-tree", "title":"Aménagement  du territoire", "content":"Agriculture"}, { "icon":"fa fa-life-ring", "title":"Santé / social", "content":"Habitat, Santé"}, { "icon":"fa fa-bolt", "title":"Utilities", "content":"Eau / Energie, Télécommunication, Déchets"}, { "icon":"fa fa-joomla", "title":"Industrie", "content":"Industrie métallurgique, Automobile, Agro alimentaire, Maintenance industrielle, BTP"}, { "icon":"fa fa-sitemap", "title":"Supply Chain", "content":"Achat, Logistique, Gestion d’entrepôts, Postal, Distribution"}, { "icon":"fa fa-bed", "title":"Tourisme", "content":"Hôtellerie, Restauration"}, { "icon":"fa fa-recycle", "title":"Services", "content":"Banques et assurances, Informatique, Distribution, Professions libérales"} ];

equipeData = [
	{  pict: "nosmas1.jpg", name: "Eric Nosmas", status: "Associé", description: "Diplômé du Magistère de Paris-Dauphine, Eric a rejoint notre cabinet début 2001, après 7 années passées chez Accenture Paris, et en a pris la direction en 2008. Il est spécialisé en conduite du changement et en pilotage de projet d’organisation et de systèmes d’information et a conduit de grands projets de transformation d’organisation.", email: "enosmas@neo.nc" },
	{  pict: "chardin.jpg", name: "Olivier CHARDIN", status: "Associé", description: "De formation ingénieur (Agro / eaux et forêts), Olivier bénéficie d’une expérience de 18 ans dans le conseil en organisation et management. Olivier a rejoint le cabinet en 2005, après 8 années passées chez Bossard Consultants puis Cap Gemini Consulting, Olivier nous apporte son expertise en matière de conduite du changement et de pilotage de projet.", email: "ochardin@neo.nc" },
	{  pict: "photo cv florian.jpg", name: "Florian DE ROBERT", status: "Sénior Consultant", description: "Diplômé des Arts et Métiers (ENSAM Paristech) et d’un Master en Management (IAE Aix en Provence), Florian bénéficie d’une expérience de 5 ans dans le conseil, notamment sur des thématiques liées aux systèmes d’information, au pilotage de projet, à l’accompagnement de start-up (modélisation économique, business plans…) et à l’amélioration de la performance.", email: "fderobert@neo.nc" },
	{  pict: "k.choufani-web.jpg", name: "Khalid CHOUFANI", status: "Sénior Consultant", description: " Ingénieur de formation avec une spécialisation en management des systèmes d’information. Khalid dispose d’une expérience de 5 ans dans le conseil, notamment en matière de pilotage de projet, d’assistance à maitrise d’ouvrage ou de problématiques nécessitant une expertise fonctionnelle dans le secteur tertiaire. ", email: "kchoufani@neo.nc" },
	{  pict: "c.alindado-web.jpg", name: "Christophe ALINDADO", status: "Sénior Consultant", description: "De formation commerciale (Télécom et Management SudParis) et disposant d’une spécialisation en management des systèmes d’information (Majeur : Ingénierie d’Affaires Internationales), Christophe est notamment intervenu sur des missions d’audit de systèmes d’information, de conception de sites web, de reenginering de processus ou d’amélioration de la performance.", email: "calindado@neo.nc" },
	{  pict: "cargy-web2.jpg", name: "Claire ARGY", status: "Sénior Consultante", description: "Diplômée de l’Ecole Nationale des Ponts et Chaussées et disposant d’une spécialisation en économie & finance (University of California, Berkeley), Claire bénéficie d’une expérience en stratégie d’investissement (fonds de Private Equity métropolitain) et de 3 années dans le métier du conseil. Claire est notamment intervenue sur des missions de conseil en matière d’e-administration, d’organisation et de pilotage de projet.", email: "cargy@neo.nc" },
	{  pict: "tgoimier.jpg", name: "Tom GOIMIER", status: "Consultant Junior", description: "Ingénieur aéronautique de formation (ISAE-SUPAERO), Tom s’est spécialisé dans le conseil en management dans l’Industrie suite à ses études. Tom est notamment intervenu sur des missions d’excellence des opérations dans l’agro-alimentaire, d’optimisation de la performance et d’accompagnement au pilotage et à la gestion de projets industriels.", email: "tgoimier@neo.nc" }
	];

galleryDataOld = [ "img/pict2/bellaing-906526_1280.jpg", "img/pict2/forest-71864_1280.jpg", "img/pict2/miners-1046846_1280.jpg", "img/pict2/work-1817674_1280.jpg", "img/pict2/working-1056583_1280.jpg", "img/pict2/loading-646934_1280.jpg", "img/pict2/construction-757577_1280.jpg", "img/pict2/box-784261_1280.jpg", "img/pict2/plane-352733_1280.jpg"];
galleryData = [ "img/pict3/servicepub_1.jpg", "img/pict3/amenagement_2.jpg", "img/pict3/logistics_3.jpg", "img/pict3/transport_4.jpg", "img/pict3/telecom_5.jpg", "img/pict3/tourisme_6.jpg", "img/pict3/agriculture_7.jpg", "img/pict3/industrie2_8.jpg", "img/pict3/industrie_9.jpg"];

serviceline1 = [
	{ href: "offre/card1", pict: "img/pict2/stock.jpg", title: "Pilotage de la performance", icon: "fa fa-line-chart", color: "light-green-text", button: "btn btn-light-green", list: ["Structuration de la stratégie en objectifs", "Evaluation de l’action publique", "Indicateurs de performance", "Contrôle de gestion", "Activity Based Management"]  },
	{ href: "offre/card2", pict: "img/pict2/strategy.jpg", title: "Stratégie et offres", icon: "fa fa-bullseye", color: "light-blue-text", button: "btn btn-info", list: ["Etudes stratégiques", "Lancement de nouvelles activités", "Business Plan / Modélisation", "Analyse concurrentielle", "Diagnostics territoriaux", "Schémas directeurs stratégiques"]  },
	{ href: "offre/card3", pict: "img/pict2/openspace.jpg", title: "Organisation", icon: "fa fa-cogs", color: "teal-text", button: "btn btn-default", list: ["Diagnostic d’organisation", "Transformation d’entreprise", "Analyse / refonte de processus", "Rédaction de procédures"]  }
	];
	
serviceline2 = [
	{ href: "offre/card4", pict: "img/pict2/computer.jpg", title: "Systèmes d’information", icon: "fa fa-database", color: "orange-text", button: "btn btn-light-green", list: ["Schéma Directeur informatique", "Diagnostic des SI", "Cahier des charges", "Pilotage de projet informatique", "Assistance fonctionnelle", "Urbanisation, MCD", "Conception de site internet"]  },
	{ href: "offre/card5", pict: "img/pict2/workplace.jpg", title: "Ressources humaines et Conduite du changement", icon: "fa fa-child", color: "purple-text", button: "btn btn-info", list: ["Recrutement", "Modèle de compétences", "Processus et modèles d’évaluation", "Ingénierie de formation", "Accompagnement du changement"]  },
	{ href: "offre/card6", pict: "img/pict2/callcenter.jpg", title: "Gestion de la Relation Client", icon: "fa fa-heart", color: "red-text", button: "btn btn-default", list: ["Analyse marketing, Infocentre", "Fidélisation client", "Plate-forme téléphonique / CRC"]  }
	];
	
serviceData = [ serviceline1, serviceline2 ];

clientslogo = [ "img/pictclients/1_govncaledo.png", "img/pictclients/2_provincesud.png", "img/pictclients/3_villenoumea.png", "img/pictclients/4_caledomap.png", "img/pictclients/5_officetourisme.png", "img/pictclients/6_cafat.png", "img/pictclients/7_afc.png", "img/pictclients/8_econum.png", "img/pictclients/9_almameto.png", "img/pictclients/10_erame.png", "img/pictclients/11_koniambo.png", "img/pictclients/12_montagnat.png", "img/pictclients/13_smt.png", "img/pictclients/14_cotrans.png", "img/pictclients/15_aircaledo.png", "img/pictclients/16_aircalin.png", "img/pictclients/17_carsud.png", "img/pictclients/18_vale.png", "img/pictclients/19_smsp.png", "img/pictclients/20_bluescope.png", "img/pictclients/21_ace.png", "img/pictclients/22_cci.png", "img/pictclients/23_agriterri.png", "img/pictclients/24_opt.png", "img/pictclients/25_laggon.png", "img/pictclients/26_logo4c.png", "img/pictclients/27_saintvincent.png", "img/pictclients/28_falconbridge.png", "img/pictclients/29_clpi.png", "img/pictclients/30_signnoumea.png", "img/pictclients/31_eec.png", "img/pictclients/32_montdore.png", "img/pictclients/33_fontainemontdore.png", "img/pictclients/34_nickelmining.png", "img/pictclients/35_emc.png", "img/pictclients/36_mobil.png", "img/pictclients/37_asdetrefle.png", "img/pictclients/38_iac.png", "img/pictclients/39_sic.png", "img/pictclients/40_semagglo.png", "img/pictclients/41_banquencaledo.png", "img/pictclients/42_societegeneral.png", "img/pictclients/43_csb.png", "img/pictclients/44_sofinor.png", "img/pictclients/45_biblibernheim.png", "img/pictclients/46_cocoge.png", "img/pictclients/47_ramada.png", "img/pictclients/48_glphotels.png", "img/pictclients/49_banquetahiti.png", "img/pictclients/50_banquesocredo.png", "img/pictclients/51_sodil.png", "img/pictclients/52_casinonoumea.png", "img/pictclients/53_cuenet.png", "img/pictclients/54_spot.png", "img/pictclients/55_caisselcr.png", "img/pictclients/56_ifrecor.png", "img/pictclients/57_bci.png", "img/pictclients/58_sodemo.png", "img/pictclients/59_sunray.png", "img/pictclients/60_scie.png", "img/pictclients/61_sharp.png", "img/pictclients/grandshotels.png", "img/pictclients/rai.png", "img/pictclients/tati.png" ];;app.config(function($routeProvider) {
    $routeProvider
    .when("/", {
        templateUrl : "partials/main.htm"
    })
    .when("/main", {
        templateUrl : "partials/main.htm"
    })
    .when("/offre", {
        templateUrl : "partials/offre.htm"
    })
    .when("/reference", {
        templateUrl : "partials/reference.htm"
    })
    .when("/equipe", {
        templateUrl : "partials/equipe.htm"
    })
    .when("/recrutement", {
        templateUrl : "partials/recrutement.htm"
    })
    .when("/contact", {
        templateUrl : "partials/contact.htm"
    })
    .otherwise({
    	templateUrl : "partials/main.htm"
    })
;
});

app.controller('myController', ['$scope', '$http', '$filter', '$timeout', '$location', '$anchorScroll', function($scope, $http, $filter, $timeout, $location, $anchorScroll) {

	var index= 0, page = 0;
	$scope.mobileflg = (/Mobi/.test(navigator.userAgent)) || (/iPhone|iPad|iPod|Android/i.test(navigator.userAgent));
	$scope.useragent = navigator.userAgent + ' mobile=' + (($scope.mobileflg) ? 'YES' : 'NO');

	$scope.pagetitle = '';
	$scope.showview = 1;
	$scope.mAr = missionData;
	$scope.mBr = missionBlock;
	$scope.equipe = equipeData;
	$scope.service = serviceData;
	$scope.gallery = galleryData;
	$scope.clients = clientslogo;
	$scope.categories = categories;
	$scope.captcha = false;
	$scope.captchaResponse = "";
	$scope.user = { name: "", email:"", subject:"", comment:"" };
	$scope.request = "";
		
	$scope.webtemplate = [ "main", "offre", "reference", "equipe", "recrutement", "contact" ];
	$scope.pagelabel = ["Accueil", "L’Offre", "Clients", "L’equipe", "Le Recrutement", "Contact"];

	$scope.pageScrollto = function(url) {
		var tt = url.split("/");
		console.log('SCROLL', tt[0], url);
		$location.path("/" + tt[0]);
	        $location.hash(tt[1]);

	        $anchorScroll();
		};
		
	$scope.checking = function(o, l, content) {
		  if(typeof o !== "string" || o.length < l) {
		  	alert("renseignez "+content);
		  	return false;
		  	}
		  return true;
		};

	$scope.emailvalidation = function(email) {
		var mailformat = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
		console.log('VALID', email, email.match(mailformat));
		return (!email.match(mailformat)) ? false : true;
		}
		
	$scope.onsubmit = function(what) {
		var captchaResponse, u = $scope.user;
		
		$scope.captchaResponse = "none";
		if($scope.request !== "") {
			alert("votre demande est traitée actuellement ou a déjà été traitée");
			return false;
			}
			
		if($scope.captcha === false && false) {
			$scope.captchaResponse = grecaptcha.getResponse();
			if($scope.captchaResponse === "") {
				alert("Etes-vous un 'robot'!?");
				return false;
				} 
			}

		$scope.captcha = true;
		if($scope.checking(u.name, 3, 'votre nom') !== true) 
			return false;
		if($scope.checking(u.email, 8, 'votre email') !== true)
			return false;
		if($scope.emailvalidation(u.email) !== true) {
			alert("Renseignez un email valid");
			return false;
			}
			
		if(what === 'c') {
			// if($scope.checking(u.subject, 5, 'le suject') !== true)  return false;
			// if($scope.checking(u.comment, 8, 'l’object de votre demande') !== true) return false;
		} else { 
			u.subject = "newsletter"; 
			u.comment = ""; 
			}

		$scope.request = $http({
			method: "post",
			url: "./checkemail.php",
			data: { name: u.name, email: u.email, subject: u.subject, comment: u.comment, captcha: $scope.captchaResponse },
			headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
			});
		
		$scope.request.success(function (status) { 
			if(typeof status === "string" && status.length > 1 && status.indexOf('success') > -1)
				;//alert("Votre demande nous a été envoyée "); 
			else {
				alert("Votre demande n’a pas été envoyée "+status); 
				$scope.request = "";
				}
			});
		alert("Votre demande nous a été envoyée "); 
		return true;
	};
		
	$scope.$on("$routeChangeStart", function (event, next, current) {
		$.getScript("https://www.google.com/recaptcha/api.js?hl=fr");
		$scope.captcha = false;
		$scope.request = "";
		$scope.captchaResponse = "";
		$scope.user = { name: "", email:"", subject:"", comment:"" };
		$scope.pagetitle = "";
		$scope.showview = (next.templateUrl && next.templateUrl.indexOf("main") >= 0) ? 1 : 0;   
		$scope.webtemplate.map(function(ll, index) {
			if(next.templateUrl) {
				var k = next.templateUrl.indexOf(ll);
				 if(k  >= 0) {
					 $scope.pagetitle = $scope.pagelabel[index];
					  console.log('TITLE', $scope.pagelabel[index], k, next.templateUrl);
					 }
				}
			});    			   
		$('#collapseEx').collapse('hide');
		$('#collapseEx').collapse('hide');
		console.log('ROUTE', next.templateUrl);
   	 });

	$scope.isRouteActive = function(route) { 
		if(route === "accueil") {
			var tmp = $location.path().replace(/^.*index.html/, "");
			return (tmp.length < 4 || tmp === "/main");
			}
		return ($location.path().match(route)) ? true : false;
	};
		
	$scope.mAr.map(function(o) { o.index = index++ });
	
	$scope.even = function(x) {
		return ((x.index % 2) !== 0);
		}

}]);