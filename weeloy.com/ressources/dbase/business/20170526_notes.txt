CREATE TABLE `notes` (
  `ID` int(11) NOT NULL,
  `restaurant` varchar(64) NOT NULL,
  `application` varchar(8) NOT NULL,
  `mode` varchar(16) NOT NULL,
  `content` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `notes`
--
ALTER TABLE `notes`
  ADD PRIMARY KEY (`ID`),
  ADD UNIQUE KEY `restaurant` (`restaurant`,`application`,`mode`) USING BTREE;

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `notes`
--
ALTER TABLE `notes`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;