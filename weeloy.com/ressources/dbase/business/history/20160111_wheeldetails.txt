ALTER TABLE `wheeldetails` ADD `type` VARCHAR(16) NOT NULL AFTER `order_value`;
ALTER TABLE `wheel` ADD `type` VARCHAR(16) NOT NULL AFTER `wheeldescriptions`;

CREATE TABLE IF NOT EXISTS wheelold LIKE wheel;
INSERT wheelold SELECT * FROM wheel where status = 'deleted';
DELETE FROM wheel where status = 'deleted';

update `wheeloffer` set type='EXTRA';

ALTER TABLE `wheeldetails` CHANGE `restaurant_id` `restaurant` VARCHAR(56) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL;

# reconstruct offers -> execute tools:  localhost:88888/weeloy.com/ressources/tools/generateofferwheel.php
# reimport wheel, wheeloffer, wheelold tables from prod ... wheeldetails is not use
