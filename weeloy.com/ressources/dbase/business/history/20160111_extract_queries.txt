
--
-- Table structure for table `extract_queries`
--

CREATE TABLE IF NOT EXISTS `extract_queries` (
`ID` int(11) NOT NULL,
  `label` varchar(64) NOT NULL,
  `query` text NOT NULL,
  `status` varchar(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `extract_queries`
--
ALTER TABLE `extract_queries`
 ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `extract_queries`
--
ALTER TABLE `extract_queries`
MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;