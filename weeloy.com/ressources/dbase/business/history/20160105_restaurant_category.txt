CREATE TABLE `restaurant_category` ( `id` INT(11) NOT NULL AUTO_INCREMENT , `restaurant` VARCHAR(128) NOT NULL , `category` TEXT NOT NULL , `status` VARCHAR(15) NOT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;
ALTER TABLE `restaurant_category` ADD `morder` INT(15) NOT NULL AFTER `category`;
ALTER TABLE `restaurant_category` ADD `is_display` TEXT NOT NULL AFTER `category`;