CREATE TABLE `media` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  `object_type` varchar(12) NOT NULL,
  `restaurant` varchar(128) NOT NULL,
  `media_type` varchar(8) NOT NULL,
  `path` varchar(128) NOT NULL,
  `morder` int(4) NOT NULL,
  `status` varchar(8) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2281 ;
