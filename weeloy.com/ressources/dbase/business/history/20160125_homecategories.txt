--
-- Table structure for table `home_categories`
--

drop table home_categories;

CREATE TABLE `home_categories` (
  `id` int(11) NOT NULL,
  `title` varchar(50) NOT NULL,
  `tag` varchar(50) NOT NULL,
  `description` text NOT NULL,
  `link` varchar(50) NOT NULL,
  `images` text NOT NULL,
  `status` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

ALTER TABLE `home_categories` CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT;