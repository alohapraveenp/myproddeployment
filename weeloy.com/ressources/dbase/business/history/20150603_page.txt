
DROP TABLE IF EXISTS `page`;
CREATE TABLE IF NOT EXISTS `page` (
`id` mediumint(8) unsigned NOT NULL,
  `group` varchar(16) NOT NULL,
  `name` varchar(24) NOT NULL,
  `path` varchar(64) DEFAULT NULL,
  `breadcrumb` varchar(128) NOT NULL,
  `lang_files` varchar(64) NOT NULL,
  `sidebox` varchar(6) NOT NULL,
  `content_twig` varchar(96) NOT NULL,
  `title` varchar(69) NOT NULL,
  `desc` varchar(180) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=62 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `page`
--

INSERT INTO `page` (`id`, `group`, `name`, `path`, `breadcrumb`, `lang_files`, `sidebox`, `content_twig`, `title`, `desc`) VALUES
(28, 'member', 'myaccount', 'member', '', 'member_section', '', 'myaccount.html.twig', 'Account | find best restaurant | weeloy.com', ''),
(55, '', '404', NULL, '', '', '', '404.html.twig', 'Error page not found | find best restaurant | weeloy.com', ''),
(56, 'member', 'mydashboard', 'member', '', 'member_section', '', 'mydashboard.html.twig', 'Deashboard | find best restaurant | weeloy.com', ''),
(54, 'member', 'myprofile', 'member', '', 'member_section', '', 'myprofile.html.twig', 'Profile | find best restaurant | weeloy.com', ''),
(57, 'member', 'mybookings', 'member', '', 'member_section', '', 'mybookings.html.twig', 'Bookings | book restaurant | weeloy.com', 'Manage your booking, Spin the wheel, book your favorite restaurant and get rewards Weeloy.com'),
(58, 'member', 'myreviews', 'member', '', 'member_section', '', 'myreviews.html.twig', 'Reviews | manage reviews | weeloy.com', 'Manage your booking, Spin the wheel, book your favorite restaurant and get rewards Weeloy.com'),
(59, '', 'front_restaurant', 'front_restaurant', '', '', '', 'front_restaurant.html.twig', 'Book restaurant Now | Weeloy', 'Book restaurant Now | Weeloy'),
(60, '', 'search_restaurant', 'search_restaurant', '', 'search_section', '', 'search_restaurant.html.twig', 'Search', 'Restaurant'),
(61, '', 'home', 'home', '', 'home', '', 'home.html.twig', 'Book Best Restaurants in Singapore and Get Rewarded with Weeloy', 'Book Best Restaurants in Singapore with Weeloy and discover exclusive deals and promotions. Simply book your table and spin the wheel at your restaurant in Singapore');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `page`
--
ALTER TABLE `page`
 ADD PRIMARY KEY (`id`), ADD KEY `name` (`name`) USING BTREE;

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `page`
--
ALTER TABLE `page`
MODIFY `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=62;