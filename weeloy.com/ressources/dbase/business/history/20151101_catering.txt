-- phpMyAdmin SQL Dump
-- version 4.2.10
-- http://www.phpmyadmin.net
--
-- Host: localhost:3306
-- Generation Time: Nov 03, 2015 at 07:47 PM
-- Server version: 5.5.38
-- PHP Version: 5.6.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `richard`
--

-- --------------------------------------------------------

--
-- Table structure for table `catering`
--

CREATE TABLE `catering` (
`ID` int(11) NOT NULL,
  `restaurant` varchar(64) NOT NULL,
  `orderID` varchar(16) NOT NULL,
  `itemindex` int(11) NOT NULL,
  `cdate` datetime NOT NULL,
  `rdate` date NOT NULL,
  `total` float NOT NULL,
  `tax` float NOT NULL,
  `currency` set('SGD','BAHT','USD','EURO') NOT NULL,
  `deliverymode` set('pickup','delivery') NOT NULL,
  `status` varchar(8) NOT NULL,
  `title` varchar(8) NOT NULL,
  `firstname` varchar(32) NOT NULL,
  `lastname` varchar(32) NOT NULL,
  `email` varchar(32) NOT NULL,
  `mobile` varchar(16) NOT NULL,
  `address` varchar(128) NOT NULL,
  `zip` varchar(8) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `catering`
--


--
-- Indexes for dumped tables
--

--
-- Indexes for table `catering`
--
ALTER TABLE `catering`
 ADD PRIMARY KEY (`ID`), ADD KEY `orderid` (`orderID`), ADD KEY `restaurant` (`restaurant`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `catering`
--
ALTER TABLE `catering`
MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;