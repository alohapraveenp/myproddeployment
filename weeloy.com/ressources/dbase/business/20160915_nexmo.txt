-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: weeloy-business.cweyuhtatlxl.ap-southeast-1.rds.amazonaws.com
-- Generation Time: Sep 15, 2016 at 06:16 PM
-- Server version: 5.6.19-log
-- PHP Version: 5.5.38

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

CREATE TABLE IF NOT EXISTS `nexmo_archive` (
`ID` int(11) NOT NULL,
  `value` text NOT NULL,
  `type` varchar(36) NOT NULL,
  `fixline` varchar(16) NOT NULL,
  `fromline` varchar(16) NOT NULL,
  `convID` varchar(84) NOT NULL,
  `cdate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8;

ALTER TABLE `nexmo_archive`
 ADD PRIMARY KEY (`ID`);

ALTER TABLE `nexmo_archive`
MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;

CREATE TABLE IF NOT EXISTS `nexmo_test` (
`ID` int(11) NOT NULL,
  `value` text NOT NULL,
  `type` varchar(36) NOT NULL,
  `fixline` varchar(16) NOT NULL,
  `fromline` varchar(16) NOT NULL,
  `convID` varchar(84) NOT NULL,
  `cdate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8;

ALTER TABLE `nexmo_test`
 ADD PRIMARY KEY (`ID`);

ALTER TABLE `nexmo_test`
MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;
