<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="author" content="weeloy. https://www.weeloy.com">
<meta name="copyright" content="weeloy. https://www.weeloy.com">  
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1">
<title>Weeloy - Login System</title>
<link rel="icon" href="/favicon.ico" type="image/gif" sizes="16x16">
<link href="../css/bootstrap33.min.css" rel="stylesheet" type="text/css" />
<link href="../css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
<link href="../css/jquery-ui.css" rel="stylesheet" type="text/css">
<link href="../css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<link href="../css/admin-style.css" rel="stylesheet" type="text/css"/>
<link href="../css/bootstrap-social.css" rel="stylesheet" type="text/css"/>
<link href="../css/login.css" rel="stylesheet" type="text/css">
<link href="../css/modal.css" rel="stylesheet" type="text/css">
<link href="../css/login-modal.css" rel="stylesheet" type="text/css"/>

<link href='https://fonts.googleapis.com/css?family=Dosis:400,600,700bold|Roboto:400,900,700italic,700,500italic,500,400italic' rel='stylesheet' type='text/css'>

<script type="text/javascript" src="../js/jquery.min.js"></script>
<script type="text/javascript" src="../js/angular.min.js"></script>
<script type="text/javascript" src="../js/jquery-ui.js"></script>
<script type="text/javascript" src="../js/bootstrap.min.js"></script>
<script type="text/javascript" src="../js/ui-bootstrap-tpls-0.12.1.min.js"></script>
<script type="text/javascript" src="../js/facebookProvider.js"></script>
<script type="text/javascript" src="../js/facebookRun.js"></script>
<script type="text/javascript" src="../js/loginService.js"></script>
<script type="text/javascript" src="../js/ngStorage.min.js"></script>
<script type="text/javascript" src="../js/formControl.js"></script>
<script type="text/javascript" src="../backoffice/inc/libService.js"></script>
<script type="text/javascript" src="loginController.js"></script>



<!--[if lt IE 9]>
<script src="js/html5.js"></script>
<script src="js/selectivizr-min.js"></script>
<script src="js/modernizr.js"></script>
<link rel="stylesheet" type="text/css" href="css/ie.css" media="screen" />
<![endif]-->
</head>


<script> var app = angular.module('loginsession',['ui.bootstrap', 'FacebookProvider', 'ngStorage']); </script>

<body ng-app="backoffice" ng-controller="loginController" ng-init="moduleName='login';">

<nav class="navbar navbar-default navbar-static-top bottom-border">
<div class="logo"><img width="90px" src="../images/logo_w.png" alt="weeloy-best-restaurant-logo" /></div>
  <div class="container">
	<div id="navbar" class="navbar-collapse collapse" style='padding:10px 0 0 0;'>
 	  <ul class="nav navbar-nav navbar-right">
		  <li ng-if="logaction == 'login'"><a href ng-click="loginout(logaction)"><span class="glyphicon glyphicon-log-in"></span> {{logaction}} </a></li>
		  <li ng-if="logaction != 'login'"><a href ng-click="loginout(logaction)"><span class="glyphicon glyphicon-log-out"></span> {{logaction}} </a></li>
		  <li>&nbsp; &nbsp; &nbsp; &nbsp;</li>
		  <li id="sessiontime" class="small"></li>
 	  	  <li>&nbsp; &nbsp; &nbsp; &nbsp;</li>
 	  </ul>
	</div>
  </div>
</nav>
 okokokokokoko
<div id="right">
	<content-item ng-repeat="item in translatedata" content="item" myTemplates="templateData"></content-item>
</div>
    

<div id="fb-root"></div>


<script>

<?php 
echo "var cookiename = '" . getCookiename('translation') . "';\n"; /* connected to the alog.js script file */ 
printf("var tablette = %s;", ($browser->isTablet()) ? 'true': 'false');
?>

$(document).ready(function() { }); 


</script>

</body>
</html>
