<?php
$allowcreate = false;

if (!empty($_COOKIE["weeloy_super"])) {
    if ($_COOKIE["weeloy_super"] == "creator") {
        $allowcreate = true;
    }
}

if ($_SESSION['login_type'] == 'member' || $_SESSION['user']['member_type'] == 'visitor') {
    $allowcreate = true;
    $_SESSION['create_member_type'] = 'member';
}

if (defined(__ROOTDIR__) == false) {
    define(__ROOTDIR__, "../../");
}

//$browser = new Browser;
//$is_mobile = $browser->isMobile();
$is_mobile = false;

//input_filter
$platform = filter_input(INPUT_GET, 'platform', FILTER_SANITIZE_STRING);
$state = filter_input(INPUT_GET, 'state', FILTER_SANITIZE_STRING);

$platform = (!empty($platform)) ? $platform : get_valid_login_type('');

if ($platform == "backoffice" || $platform == "admin_weeloy")
    $allowcreate = false;

if (!empty($state) && $state == "register")
    $allowcreate = true;

$action = "'" . __ROOTDIR__ . "/modules/login/redirectURI.php'";
?>

<!DOCTYPE html>
<html>
    <head>
        <!-- Include meta tag to ensure proper rendering and touch zooming -->
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Include bootstrap stylesheets -->
        <style>

            .panel{
                border-color:#5285a0 !important;
            }

            .panel-heading{
                background-image:none !important;
                background-color:#5285a0 !important;
                color:#fff !important;
                border-color:#5285a0 !important;
                border-radius:0;
            }
            .panel-heading a{
                color: white;
            }

            .btn-default{
                background-image:linear-gradient(to bottom, #eee 0px, #e0e0e0 100%)!important;
            }

            .container_modal { margin: 0!important;
                               padding: 0!important;
                               width: 100%!important; }

            .drawer {
                display:none;
                padding: 0;
            }

            .or-spacer {
                margin-bottom: 35px;
                margin-top: 43px;
                position: relative;
            }
            .or-spacer .mask {
                overflow: hidden;
                height: 20px;
            }
            .or-spacer .mask:after {
                content: '';
                display: block;
                margin: -25px auto 0;
                width: 100%;
                height: 25px;
                border-radius: 125px / 12px;
                box-shadow: 0 0 4px black;
            }
            .or-spacer span {
                width: 50px;
                height: 50px;
                position: absolute;
                bottom: 100%;
                margin-bottom: -25px;
                left: 50%;
                margin-left: -25px;
                border-radius: 100%;
                box-shadow: 0 2px 4px #999;
                background: white;
            }
            .or-spacer span i {
                position: absolute;
                top: 4px;
                bottom: 4px;
                left: 4px;
                right: 4px;
                border-radius: 100%;
                border: 1px dashed #aaa;
                text-align: center;
                line-height: 40px;
                font-style: normal;
                color: #999;
            }
            /*
            body.modal-open {
                position: fixed;
            }
            */
        </style>
    </head>

    <body>

        <div class="container_modal" style="">
            <?php if ($is_mobile == false) {
                echo "<div class='col-xs-2'></div>";
            } ?>
            <div style="padding: 0;" class="mainbox <?php if ($is_mobile == false) {
                echo "<div class='col-xs-8'></div>";
            } else {
                echo "<div class='col-xs-12'></div>";
            } ?> ">
                <div class="panel panel-info" style='text-align:center; border:solid 5px; margin-bottom: 0px' >
                    <div style="padding-top:30px" class="panel-body" >
                        <h1 style='margin-bottom: 20px' id='title'>Welcome</h1>

                        <div class='loginbox'>
                            <div id='facebook' class='login-form' style="display:none; ">
                                <a class="btn btn-lg btn-social btn-facebook" id='loginBtn' href="javascript:;" title="Log in using your Facebook account">
                                    <i class="fa fa-facebook"></i>Login with Facebook</a>
                                <div id="status_fb"></div>
                            </div>

                            <div class="or-spacer">
                                <div class="mask"></div>
                                <span><i>or</i></span>
                            </div>
                        </div>

                        <form class='form-horizontal' action= <?php echo $action; ?> role='form' id='login_form' name='login_form' method='POST' >
                            <input type='hidden' id='fbtoken' name='fbtoken'>
                            <input type='hidden' id='fbemail' name='fbemail'>
                            <input type='hidden' id='fbname' name='fbname'>
                            <input type='hidden' id='fblastname' name='fblastname'>
                            <input type='hidden' id='fbfirstname' name='fbfirstname'>
                            <input type='hidden' id='fbuserid' name='fbuserid'>
                            <input type='hidden' id='fbtimezone' name='fbtimezone'>
                            <input type='hidden' id='userid' name='userid'>
                            <input type='hidden' id='token' name='token'>
                            <input type='hidden' id='timezone' name='timezone'>
                            <input type='hidden' id='application' name='application' value='html'>
                            <input type='hidden' id='socialname' name='socialname' value=''>
                            <input type='hidden' id='type_action' name='type_action' value='Login'>
                            <input type='hidden' id='secret' name='secret' value='7XXJWRB7YTOSGOVL'>
<?php
printf("<input type='hidden' id='platform' name='platform' value='%s'>", $platform);
?>

                            <div style="display:none" id="login-alert" class="alert alert-danger"></div>
                            <div class="email-container">
                                <div style="margin-bottom: 25px" class="input-group">
                                    <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                                    <input id="email" type="text" class="form-control" name="email" value = <?php echo "'" . $prevemail . "'"; ?> placeholder="email" onchange='CleanEmail(this);' onKeyPress="return submitFormWithEnter(this, event, 'signin')">
                                </div>
                                <div style="margin-bottom: 25px" class="input-group loginbox" >
                                    <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                                    <input id="password" name="password" type="password" class="form-control passtype" placeholder="password" onKeyPress="return submitFormWithEnter(this, event, 'signin')">
                                </div>
                            </div>
                            <?php if ($platform == 'backoffice') { ?>
                                <div class="google_otp" style='display:none;'>
                                    <div style="margin-bottom: 25px" class="input-group " >
                                        <span class="input-group-addon"><i class="glyphicon glyphicon-ok"></i></span>
                                        <input id="otp" name="otp" type="text" class="form-control " placeholder="google auth code"  onKeyPress="return submitFormWithEnter(this, event, 'signin')" autofocus />
                                    </div>
                                </div>
                            <?php } ?>

                            <div id="signupbox" class='drawer'>
                                <div style="margin-bottom: 20px" class="input-group">
                                    <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                                    <input id="firstname" name="firstname" type="text" class="form-control" value="" onchange='CleanText(this);' placeholder="firstname">
                                </div>

                                <div style="margin-bottom: 20px" class="input-group">
                                    <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                                    <input id="lastname" name="lastname" type="text" class="form-control" value="" onchange='CleanText(this);' placeholder="lastname">
                                </div>

                                <div style="margin-bottom: 20px" class="input-group">
                                    <span class="input-group-addon"><i class="glyphicon glyphicon-earphone"></i></span>
                                    <input id="mobile" name="mobile" type="text" class="form-control" value="" onchange='CleanTel(this);' placeholder="mobile">
                                </div>

                                <div style="margin-bottom: 20px" class="input-group">
                                    <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                                    <input id="r_password" name="r_password" type="password" class="form-control passtype" placeholder="password">
                                </div>

                                <div style="margin-bottom: 20px" class="input-group">
                                    <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                                    <input id="r_rpassword" name="rpassword" type="password" class="form-control passtype" placeholder="retypepassword">
                                </div>
                            </div>

                            <div id="chgpassbox" class='drawer'>
                                <div style="margin-bottom: 20px" class="input-group">
                                    <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                                    <input id="u_password" name="u_password" type="text" class="form-control passtype" placeholder=" old password">
                                </div>

                                <div style="margin-bottom: 20px" class="input-group">
                                    <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                                    <input id="u_npassword" name="u_npassword" type="text" class="form-control passtype" placeholder="new password">
                                </div>

                                <div style="margin-bottom: 20px" class="input-group">
                                    <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                                    <input id="u_rpassword" name="u_rpassword" type="text" class="form-control passtype" placeholder="retype password">
                                </div>

                            </div>

                            <div id="show-password" style="margin-bottom: 20px; margin-top: -25px" class="input-group">
                                <label class="checkbox-inline"><input type="checkbox" id='dddPassword' name='dddPassword' onclick="showPassword()">Show password</label>
                            </div>

                            <div class="form-inline" style="margin-bottom: 20px">
                                <div class="input-group">
                                    <a href='javascript:;' id="btn-login" class="btn btn-info" style='padding-left:30px;padding-right:30px;'>Login</a>
                                </div>
                            </div>
                            <div style="margin-bottom: 10px;" class="form-inline logmenu">
                                <a href='javascript:;' onClick='showLostPassword();' id="showpassword">Forgot password</a>
                                <a href='javascript:;' onClick='showChangePassword();' id="changepassword">Change password</a>
                            </div>
                            <div style="margin-bottom: 20px;" class="form-inline" id="reset_auth">
                                <?php if ($platform == 'backoffice') { ?>
                                    <a href='javascript:;' onClick='resetwofactauth();' id="showauthreset" style="display:none;">Reset Authentication</a>
                               <?php } ?>
                            </div>
                            <div id='registerTag' style='display:none;'>

                                <div style='border-top: 1px solid#888; padding-top:15px; font-size:85%' >
                                    Don't have an account! <a href='#' onClick='ShowSignUp();' id="register"> Register Here </a>
                                </div>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
<?php if ($is_mobile == false) {
    echo "<div class='col-xs-2'></div>";
} ?>
        </div>



        <script>

            submitflg = false;
            var twoauthflg = false;

            if ($("#platform").val() === 'backoffice') {
                twoauthflg = true;
            }
            function getUserData() {
                FB.api('/me', function (response) {
                    $('#status_fb').val('Hello ' + response.name);
                    if (response.name != "") {
                        console.log(response);
                        $('#fbuserid').val(response.id);
                        $('#fbemail').val(response.email);
                        $('#fbname').val(response.name);
                        $('#fbfirstname').val(response.first_name);
                        $('#fblastname').val(response.last_name);
                        $('#fbtimezone').val(response.timezone);
                    }
                    if ($('#fbuserid').val() != '' && submitflg) {
                        $('#application').val('facebook');
                        updateinput();
                        $('#login_form').submit();
                    }
                });
            }

            window.fbAsyncInit = function () {
                //SDK loaded, initialize it
                FB.init({
                    appId: '1590587811210971',
                    xfbml: true,
                    version: 'v2.2'
                });
                //check user session and refresh it
                FB.getLoginStatus(function (response) {
                    if (response.status === 'connected') {
                        //user is authorized
                        //document.getElementById('loginBtn').style.display = 'none';
                        if (typeof response.authResponse !== 'undefined')
                            $('#fbtoken').val(response.authResponse.accessToken);
                        submitflg = false;
                        getUserData();
                    } else {
                        //user is not authorized
                    }
                });
            };
            //load the JavaScript SDK
            (function (d, s, id) {
                var js, fjs = d.getElementsByTagName(s)[0];
                if (d.getElementById(id)) {
                    return;
                }
                js = d.createElement(s);
                js.id = id;
                js.src = "//connect.facebook.net/en_US/sdk.js";
                fjs.parentNode.insertBefore(js, fjs);
            }(document, 'script', 'facebook-jssdk'));
            //add event listener to login button
            document.getElementById('loginBtn').addEventListener('click', function () {
                //do the login
                FB.login(function (response) {
                    if (response.authResponse) {
                        //user just authorized your app
                        //document.getElementById('loginBtn').style.display = 'none';
                        if (typeof response.authResponse !== 'undefined')
                            $('#fbtoken').val(response.authResponse.accessToken);
                        if ($('#fbtoken').val().length > 10 && $('#type_action').val() == "Login")
                            submitflg = true;
                        getUserData();
                        if ($('#fbtoken').val().length > 10 && $('#type_action').val() == "Register")
                            updateinput();
                    }
                }, {scope: 'email,public_profile', return_scopes: true});
            }, false);</script>

        <script type="text/javascript">

            $(document).ready(function () {
                email = getlgcookie("weeloy_backoffice_login");
                if (email != "" && IsEmail(email))
                    $('#email').val(email);
            });

            $('#loginModal').on('shown.bs.modal', function () {
                try {
                    FB.XFBML.parse(document.getElementById('loginModal'));
                } catch (e) {
                    console.log('Cannot parse XFBML: ', e);
                }

            });
<?php
echo "var cState = '" . $state . "';";
printf("var allowcreate = %s;", ($allowcreate) ? "true" : "false");
if ($allowcreate) {
    echo "function register() { $('#type_action').val('Register'); if(checkSubmit()) $('#login_form').submit(); }";
}
?>
            if (cState != "register")
                ShowSignIn();
            else
                ShowSignUp();
            function updateinput() {
                $('#email').val($('#fbemail').val());
                $('#firstname').val($('#fbfirstname').val());
                $('#lastname').val($('#fblastname').val());
                $('#timezone').val($('#fbtimezone').val());
                $('#userid').val($('#fbuserid').val());
                $('#token').val($('#fbtoken').val());
            }

            function signin() {
                $('#type_action').val("Login");
                if (checkSubmit())
                    $('#login_form').submit();
            }

            function lostpassword() {
                $('#type_action').val("LostPassword");
                if (checkSubmit())
                    $('#login_form').submit();
                }
            function updatepassword() {
                $('#type_action').val("UpdatePassword");
                if (checkSubmit())
                    $('#login_form').submit();
            }

            function IsEmail(email) {
                var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
                return regex.test(email);
            }

            function remberme() {
                $('#remember').toggleClass('show');
            }
          

            if (allowcreate)
                $('#registerTag').show();
            function ShowSignIn() {
                $('#title').html('Welcome');
                $('.passtype').attr('type', 'password');
                $('.showpass').removeClass('show');
                $('.drawer').hide();
                $('#showallpass').show();
                $('#loginbox').show();
                $('.loginbox').show();
                $('#loginbox_facebook').show();
                $('#facebook').show();
                $('#type_action').val("Login");
                $('#btn-login').html('Login');
                $('#btn-login').unbind('click');
                $('#btn-login').click(function () {
                    signin();
                });
                $('.panel-title').html('Login')
                $('.logmenu').html("<a href='javascript:;' onClick='showLostPassword();'>Forgot password?</a><br/><a href='javascript:;' onClick='showChangePassword();'>Change password</a>");

                return false;
            }

            function ShowSignUp() {
                $('#title').html('Register Now');
                $('.passtype').attr('type', 'password');
                $('.showpass').removeClass('show');
                $('.drawer').hide();
                $('#showallpass').show();
                $('#signupbox').show();
                $('#facebook').show();
                $('#type_action').val("Register");
                $('#btn-login').html("<i class='icon-hand-right'></i> &nbsp Register");
                $('#btn-login').unbind('click');
                $('#btn-login').click(function () {
                    register();
                });
                $('.panel-title').html('Register')
                $('.logmenu').html("<a id='signinlink' href='#' onclick='ShowSignIn();'>You prefer to Login?</a>");

                return false;
            }

            function showChangePassword() {

                $('.passtype').attr('type', 'password');
                $('.showpass').removeClass('show');
                $('.loginbox').hide();
                $('.drawer').hide();
                $('#facebook').hide();
                $('#showallpass').show();
                $('#chgpassbox').show();
                $('#btn-login').html("<i class='icon-hand-right'></i> &nbsp Save");
                $('#btn-login').unbind('click');
                $('#btn-login').click(function () {
                    updatepassword();
                });
                $('.panel-title').html('Change password')
                $('.logmenu').html("<a id='signinlink' href='#1' onclick='ShowSignIn();'>Sign In</a>")

                return false;
            }

            function showLostPassword() {
                $('#title').html('Password');
                $('.drawer').hide();
                $('.loginbox').hide();
                $('#facebook').hide();
                $('#showallpass').hide();
                $('#btn-login').html("<i class='icon-hand-right'></i> &nbsp Retrieve");
                $('#btn-login').unbind('click');
                $('#btn-login').click(function () {
                    lostpassword();
                });
                $('.panel-title').html('Forgot password');
                $('.logmenu').html("<a id='signinlink' href='#1' onclick='ShowSignIn();'>You prefer to Login?</a>");

                return false;
            }
            function resetwofactauth() {
                $('#title').html('Reset Google Authentication');
                $("#otp").attr("placeholder", "Email");
                $('.drawer').hide();
                $(".google_otp").hide();
                $('.loginbox').hide();
                $('#facebook').hide();
                $('#showallpass').hide();
                $('#btn-login').html("<i class='icon-hand-right'></i> &nbsp Retrieve");
                $('#btn-login').hide();
                $("#showauthreset").hide();
                $('#btn-login').unbind('click');
                 resetAuthentication();
//                $('.panel-title').html('key in email');
                $('#reset_auth').html("A reset email has been sent to youremail.");

                return false;
            }
          


            function showPassword() {
                if ($('#password').attr('type') != 'text') {
                    $('.passtype').attr('type', 'text');
                    $('.showpass').addClass('show');
                } else {
                    $('.passtype').attr('type', 'password');
                    $('.showpass').removeClass('show');
                }
                return false;
            }

            function CleanEmail(obj) {
                obj.value = obj.value.replace(/[!#$%^&*()=}{\]\[:;><\?/|\\]/g, '');
            }
            function CleanTel(obj) {
                obj.value = obj.value.replace(/[^0-9 \+]/g, '');
            }
            function CleanPass(obj) {
                obj.value = obj.value.replace(/[^0-9A-Za-z]/g, '');
            }
            function CleanText(obj) {
                obj.value = obj.value.replace(/[!@#$%^&*()=}{\]\[\"\':;><\?/|\\]/g, '');
            }
            function invalid(str) {
                alert(str);
                return false;
            }
            function resetAuthentication(){
                $('#type_action').val("resetAuth");
                if (checkSubmit())
                    setTimeout(function () { $('#login_form').submit(); }, 1000);
                    
            }

            function checkSubmit() {

                action = $('#type_action').val();
                $('#login_form input[type=text], input[type=password]').each(function () {
                    this.value = this.value.trim();
                });
                // always
                if ($('#email').val() == "" || !IsEmail($('#email').val()))
                    return invalid('Invalid email, please try again');
                if (action == "Login") {
                    if ($('#password').val().length < 6)
                        return invalid('Invalid password, please try again');
                    removelgcookie("weeloy_backoffice_login");
                    setlgcookie("weeloy_backoffice_login", $('#email').val(), 7);
                }
                if (action == "Login" && $("#platform").val() === 'backoffice' && $("#otp").val() === '') {
                    var apiurl = "../api/login/user/" + $('#email').val();
                    var res = '';
                    $.ajax({
                        url: apiurl,
                        type: "POST",
                       data: {
                             email: $('#email').val(),
                             password: $('#password').val()
                           },
                        success: function (response, textStatus, jqXHR) {
                            if (response.status == 1) {
                                     if (response.data.is_resswitchauth === 1 && response.data.is_twofactauth === '1') {
                                        if(response.data.isValidPwd){
                                            $('.google_otp').css('display', 'block');
                                            $("#showauthreset").css('display', 'block');
                                            $(".email-container").css('display', 'none');
                                            $('.logmenu').css('display', 'none');
                                            $("#show-password").css('display', 'none');
                                            return false;
                                        }else{
                                             return invalid('Invalid password, please try again');
                                        }
                                    } else {
                                          $('#login_form').submit();
                                     
                                    }
                                

                                //}else{return true; }
                            }
                        }
                    });
 

                } else if (action == "Login" && $("#platform").val() === 'backoffice' && $("#otp").val() !== '') {
                    $('#login_form').submit();
                }
                else if (action == "LostPassword") {
                    return true;
                }
                else if (action == "resetAuth") {
                    return true;
                }

                /*                else if (action == "UpdatePassword") {
                 if ($('#u_password').val() == "")
                 return invalid('Invalid password , please try again');
                 if ($('#u_rpassword').val() == "")
                 return invalid('Invalid new password, please try again');
                 var rpassword = $('#u_rpassword').val();
                 if (rpassword == "")
                 return invalid('Invalid new password, please try again');
                 if (rpassword.length < 6) {
                 return invalid('Password too short. The new password must be between 6 and 12 car');
                 }
                 
                 if (rpassword.length > 12) {
                 return invalid('Password too long. The new password must be between 6 and 12 car');
                 }
                 if ($('#u_npassword').val() == "")
                 return invalid('Invalid new retypepassword, please try again');
                 if ($('#u_rpassword').val() != $('#u_npassword').val())
                 return invalid('Password do not match, try again');
                 }
                 */
                else if (action == "Register") {
                    if ($('#lastname').val() == "")
                        return invalid('Invalid name, try again');
                    if ($('#firstname').val() == "")
                        return invalid('Invalid firstname, try again');
                    if ($('#mobile').val() == "")
                        return invalid('Invalid tel, try again');
                    if ($('#country').val() == "")
                        return invalid('Invalid country, try again');
                    if ($('#r_password').val() == "")
                        return invalid('Invalid password,, try again');
                    if ($('#r_rpassword').val() == "")
                        return invalid('Invalid retype password, try again');
                    if ($('#r_password').val() != $('#r_rpassword').val())
                        return invalid('password and retype password are different, try again');
                    return true;
                } else {
                    return true;
                }
                //
            }

            function setlgcookie(name, value, duration) {
                value = escape(value);
                if (duration) {
                    var date = new Date();
                    date.setTime(date.getTime() + (duration * 24 * 3600 * 1000));
                    value += "; expires=" + date.toGMTString();
                }
                document.cookie = name + "=" + value;
            }

            function getlgcookie(name) {
                value = document.cookie.match('(?:^|;)\\s*' + name + '=([^;]*)');
                return value ? unescape(value[1]) : "";
            }

            //Removes the cookies
            function removelgcookie(name) {
                setlgcookie(name, '', -1);
            }


        </script>

        <script type="text/javascript">
            function submitFormWithEnter(myfield, e, target)
            {
                var keycode;
                if (window.event)
                {
                    keycode = window.event.keyCode;
                }
                else if (e)
                {
                    keycode = e.which;
                }
                else
                {
                    return true;
                }

                if (keycode == 13)
                {
                    if (target == 'signin')
                    {
                        signin();
                    }
                    return false;
                }
                else
                {
                    return true;
                }
            }
        </script>

    </body>
</html>
