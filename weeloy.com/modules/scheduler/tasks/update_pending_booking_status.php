<?php

require_once("conf/conf.init.inc.php");
require_once("lib/wpdo.inc.php");

require_once("lib/class.booking.inc.php");
require_once("lib/class.notification.inc.php");
require_once("lib/class.mail.inc.php");

$now = (strstr($_SERVER['HTTP_HOST'], 'localhost:8888')) ? "NOW()" : "DATE_ADD(NOW(), INTERVAL 8 HOUR)";

 //UPDATE FOR BURNT ENDS RESTAURANT
$sql = "UPDATE booking SET status = 'expired' WHERE cdate <= CONVERT_TZ(DATE_SUB(NOW(),INTERVAL 15 MINUTE),'+00:00','+08:00')
                AND status  = 'pending_payment' 
                AND restaurant = 'SG_SG_R_BurntEnds'";
pdo_exec($sql);

$sql = "UPDATE booking SET status = 'expired' WHERE cdate <= CONVERT_TZ(DATE_SUB(NOW(),INTERVAL 1440 MINUTE),'+00:00','+08:00')
                AND status = 'pending_payment' 
                AND restaurant = 'SG_SG_R_Esquina'";
pdo_exec($sql);

$sql = "UPDATE booking SET status = 'expired' WHERE cdate <= CONVERT_TZ(DATE_SUB(NOW(),INTERVAL 720 MINUTE),'+00:00','+08:00')
                AND status = 'pending_payment' 
                AND restaurant = 'SG_SG_R_Bacchanalia'";

pdo_exec($sql);

$sql = "UPDATE booking SET status = 'expired' WHERE cdate <= CONVERT_TZ(DATE_SUB(NOW(),INTERVAL 2880 MINUTE),'+00:00','+08:00')
                AND status = 'pending_payment' 
                AND (restaurant LIKE 'SG_SG_R_Nouri')";

pdo_exec($sql);


$sql = "UPDATE booking SET status = 'expired' WHERE cdate <= CONVERT_TZ(DATE_SUB(NOW(),INTERVAL 60 MINUTE),'+00:00','+08:00')
                AND status = 'pending_payment' 
                AND (restaurant LIKE 'SG_SG_R_SilkRoad' OR restaurant LIKE 'SG_SG_R_Element')";

pdo_exec($sql);

// pollen
$sql = "SELECT confirmation,cdate,rdate FROM booking WHERE status = 'pending_payment' AND restaurant = 'SG_SG_R_Pollen' ";
$pending_list = pdo_multiple_select($sql);

if(count($pending_list) >0){
    foreach ($pending_list as $b) {
        $bkdate = $b['rdate'];
        $intDay = (strtotime("$bkdate") - strtotime("{$b['cdate']}") )/ (60*60*24);
        if($intDay > 3){
             $sql = "UPDATE booking SET status = 'expired' WHERE cdate <= CONVERT_TZ(DATE_SUB(NOW(),INTERVAL 2880 MINUTE),'+00:00','+08:00')
                AND status = 'pending_payment' 
                AND restaurant = 'SG_SG_R_Pollen'
                AND confirmation = '{$b['confirmation']}'";
              pdo_exec($sql);
               
        }else{

             $sql = "UPDATE booking SET status = 'expired' WHERE cdate <= CONVERT_TZ(DATE_SUB(NOW(),INTERVAL 720 MINUTE),'+00:00','+08:00')
                AND status = 'pending_payment' 
                AND restaurant = 'SG_SG_R_Pollen'
                AND confirmation = '{$b['confirmation']}'";
              pdo_exec($sql);
        }

    }
    
}





//$sql = "UPDATE booking SET status = 'expired' WHERE DATE_ADD(cdate, INTERVAL 4 HOUR) > '$now'
//                AND status = 'pending_payment' 
//                AND restaurant = 'SG_SG_R_Pollen'";


$sql = "SELECT confirmation FROM booking WHERE status = 'expired' AND (restaurant LIKE 'SG_SG_R_TheFunKitchen' OR restaurant LIKE 'SG_SG_R_Bacchanalia') AND cdate > CONVERT_TZ(DATE_SUB(NOW(),INTERVAL 242 MINUTE),'+00:00','+08:00')";

$expired_list = pdo_multiple_select($sql);

$booking = new WY_Booking();
$notification = new WY_Notification();

$totalcount = count($expired_list);

if($totalcount > 0 ){
     $loguserid = "0";
     $logger = new WY_log("website");
    foreach ($expired_list as $b) {
        $booking->getBooking($b['confirmation']);
        $notification->notify($booking, 'expired');
        $logger->LogEvent($loguserid, 721, $b['confirmation'], 'website', '', date("Y-m-d H:i:s"));
    }
}

echo "success";

?>