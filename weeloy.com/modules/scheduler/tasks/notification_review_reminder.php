<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

        //scheduled task - Send notification reminder on coming soon bookings

	require_once("conf/conf.init.inc.php");
	require_once("lib/wpdo.inc.php");
        
        require_once("lib/class.booking.inc.php");
        require_once("lib/class.notification.inc.php");        
        require_once("lib/class.mail.inc.php");
	require_once("lib/class.sms.inc.php");
        require_once("lib/class.pushnotif.inc.php");
	require_once("lib/class.async.inc.php");
	require_once("lib/class.spool.inc.php");
        require_once 'conf/conf.sqs.inc.php';
        require_once("lib/class.review.inc.php");
        require_once("lib/class.analytics.inc.php");

	
        $datetime = new DateTime('today');
        $datetime->modify('-2 day');
        

        $sql = "SELECT b.confirmation FROM booking b LEFT JOIN review r ON  r.confirmation = b.confirmation
        WHERE status LIKE '' AND rdate = '".$datetime->format('Y-m-d')."' AND r.confirmation IS NULL";
       
    
        
        $booking_list = pdo_multiple_select($sql);
        $totalcount=count($booking_list);
    
        $booking = new WY_Booking();
        $review = new WY_Review();
        $notification = new WY_Notification();
        $message_sqs = new SQS();

        $message_sqs->setQueue('activitylog-booking-reminder');
        $message_sqs->setBody('Post Review Reminder');
        $message_sqs->setAttribute('type', 'review-reminder', 'String');
        $message_sqs->setAttribute('category', 'Email', 'String');
        $message_sqs->setAttribute('count',$totalcount , 'String');
        $message_sqs->sendMessage();
        
        foreach($booking_list as $b){
            $booking->getBooking($b['confirmation']);
            $review->getReview($b['confirmation']);
            $booking->review = $review; 
            $notification->notify($booking, 'reminder_review');
            
            $logger = new WY_log("website");
            $loguserid = (isset($_SESSION['user']['user_id'])) ? $_SESSION['user']['user_id'] : "0"; 
            $logger->LogEvent($loguserid, 712, $b['confirmation'], 'website', '', date("Y-m-d H:i:s"));
        }

         $datetime = new DateTime('today');
         $datetime->modify('-1 day');
         $sql = "SELECT b.confirmation FROM booking b LEFT JOIN review r ON  r.confirmation = b.confirmation
             WHERE status LIKE '' AND rdate = '".$datetime->format('Y-m-d')."' AND  b.restaurant IN ('SG_SG_R_Bacchanalia') AND r.confirmation IS NULL";

            $baccha_list = pdo_multiple_select($sql);

            $totalcount = count($baccha_list);
            foreach($baccha_list as $b){
                $booking->getBooking($b['confirmation']);
                $review->getReview($b['confirmation']);
                $booking->review =$review; 
                $notification->notify($booking, 'reminder_review');
//            $logger = new WY_log("website");
//            $loguserid = (isset($_SESSION['user']['user_id'])) ? $_SESSION['user']['user_id'] : "0"; 
//            $logger->LogEvent($loguserid, 712, $b['confirmation'], 'website', '', date("Y-m-d H:i:s"));
        }
         
  
        
        
        echo 'success';

