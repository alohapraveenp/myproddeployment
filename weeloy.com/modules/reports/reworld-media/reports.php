<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html><head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta http-equiv="refresh" content="300">
        <meta http-equiv='expires' content='mon, 1 jan 1998 05:00:00 gmt'>
        <meta http-equiv='pragma' content='no-cache'>
        <meta http-equiv='pragma' content='cache-control: max-age=0'>
        <meta http-equiv='last-modified' content='mon, 1 jan 1998 05:00:00 gmt'>
        <meta http-equiv='cache-control' content='no-cache, must-revalidate'>
        <meta name='robots' content='noindex, nofollow'>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
        <meta name="HandheldFriendly" content="true" />
        <title>Reworld media</title>
        <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
        <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css" />

        <script type="text/javascript" src="js/jquery.min.js"></script>
        <script type='text/javascript' src="js/angular.min.js"></script>
        <script type='text/javascript' src="modules/reports/reworld-media/reports-reworld.controller.js"></script>
    </head>
    <body ng-app="myApp" style="margin-top: 10px;">
        <div class="container">
            <div class="row">
        <div id='booking' ng-controller="MainController" ng-init="moduleName='reworldmedia'; typeBooking = true; bookingTitle" >
            <div class="container mainbox">    
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <div class="panel-title" > {{ bookingTitle}} </div>
                    </div>

                    <div class="panel-body" style="padding:20px">
                    <div >
                        <span style="font-size: 20px;margin-bottom: 10px;margin-top: 10px;color: inherit;font-family: inherit;font-weight: 500;line-height: 1.1;">
                            {{gourmand_direct_bookings.length}} Direct bookings </span>
                            (on {{ bookingTitle}} website) &nbsp;&nbsp;
                        <button class="btn" ng-show="!custom_direct" ng-click="toggleCustom('direct')">Show details</button>
                        <button class="btn" ng-show="custom_direct" ng-click="toggleCustom('direct')">Hide details</button>
                    </div>
                    
                    <table  ng-show="custom_direct" width='100%' class="table table-condensed table-striped" style='font-size:12px;'>
                        <thead>
                            <tr>
                                <th ng-repeat="y in tabletitle"><tbtitle name="{{y.b}}"/></th>
                        </tr>
                        </thead>
                        <tbody>
                            <tr ng-repeat="x in filteredPeople = (gourmand_direct_bookings| filter:searchText) | orderBy:predicate:reverse">
                                <td>{{ x.index}}</td>
                                <td>{{ x.cdate}}</td>
                                <td>{{ x.status}}</td>
                                <td>{{ x.lastname}}</td>
                                <td>{{ x.rdate}}</td>
                                <td>{{ x.rtime}}</td>
                            </tr>
                    </table>

                    <div id="nb_redirect">
                        <br>
                        <div style="font-size: 20px;margin-bottom: 10px;margin-top: 10px;color: inherit;font-family: inherit;font-weight: 500;line-height: 1.1;">
                            {{gourmand_number_redirect}} Redirects from {{ bookingTitle}} to Weeloy</div>
                        
                    </div>
                    
                    <br>
                    
                    <span style="font-size: 20px;margin-bottom: 10px;margin-top: 10px;color: inherit;font-family: inherit;font-weight: 500;line-height: 1.1;">
                        {{gourmand_indirect_bookings.length}} Indirect bookings</span>
                        (on Weeloy website)&nbsp;&nbsp;
                        <button class="btn" ng-show="!custom_indirect" ng-click="toggleCustom('indirect')">Show details</button>
                        <button class="btn" ng-show="custom_indirect" ng-click="toggleCustom('indirect')">Hide details</button>
                    </div>
                   
                        
                   
                    <table  ng-show="custom_indirect"  width='100%' class="table table-condensed table-striped" style='font-size:12px;'>
                        <thead>
                            <tr>
                                <th ng-repeat="y in tabletitle"><tbtitle name="{{y.b}}"/></th>
                        </tr>
                        </thead>
                        <tbody>
                            <tr ng-repeat="x in filteredPeople = (gourmand_indirect_bookings| filter:searchText) | orderBy:predicate:reverse">
                                <td>{{ x.index}}</td>
                                <td>{{ x.cdate}}</td>
                                <td>{{ x.status}}</td>
                                <td>{{ x.lastname}}</td>
                                <td>{{ x.rdate}}</td>
                                <td>{{ x.rtime}}</td>
                            </tr>
                    </table>
                </div>
            </div>
            
            </div>
        </div>
    </div>
        </div>
    </body>
</html>