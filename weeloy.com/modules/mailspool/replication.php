<?php

	require_once("conf/conf.init.inc.php");
	require_once("lib/wpdo.inc.php");
	require_once("lib/class.async.inc.php");
	require_once("lib/class.replication.inc.php");
	
	$statusId = (isset($_REQUEST['statusId'])) ? $_REQUEST['statusId'] : "";
		
	$replicate = new WY_Replication;
    $replicate->execute_replication($statusId);
    $replicate->execute_replication_backlog();

?>