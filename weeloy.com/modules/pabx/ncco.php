<?php

require_once("conf/conf.init.inc.php");
require_once("conf/conf.mysql.inc.php");
require_once("lib/wpdo.inc.php");
require_once("lib/class.pabx.inc.php");
require_once("lib/class.debug.inc.php");
require_once("lib/class.restaurant.inc.php");

$method = $_SERVER['REQUEST_METHOD'];
if($method == "GET") {
	$to = $_REQUEST['to']; //The endpoint being called
	$from = $_REQUEST['from']; //The endpoint you are calling from
	$uuid = $_REQUEST['conversation_uuid']; //The unique ID for this Call
	$debug = (isset($_REQUEST['DEBUG'])) ? intval($_REQUEST['DEBUG']) : 0;
		
	$pabx = new WY_Pabx($debug);	
	$pabx->initiatePabx($to, $from, $uuid);
} else WY_debug::recordDebug("ERROR", "PABX", "Not supported method " . $method . " " . print_r($_REQUEST, true));

?>
