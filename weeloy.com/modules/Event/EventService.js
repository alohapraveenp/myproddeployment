var event = angular.module('EventServiceModule', []);
event.service('EventService', ['$http', '$q', function($http, $q) {
    this.getHomeEvent = function($restaurant) {
        var defferred = $q.defer();
        var API_URL = 'api/home/getEvents/' + $restaurant;
        $http.get(API_URL, {
            cache: true,
        }).success(function(response) {
            defferred.resolve(response);
        });
        return defferred.promise;
    };
    
}]);