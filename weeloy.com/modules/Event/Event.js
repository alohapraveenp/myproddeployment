var event = angular.module('Event', ['ngRoute', 'EventServiceModule']);
event.config(function($locationProvider) { $locationProvider.html5Mode(true);});
event.controller('EventController', ['$scope', '$location', '$routeParams', 'EventService', function($scope, $location, $routeParams, EventService) {
    console.log("EventController. routeParams: "+JSON.stringify($routeParams)+" again: "+JSON.stringify($location.search()));
    //$scope.restaurant = $routeParams.restaurant;
    $scope.restaurant = $location.search().restaurant;
    $scope.config = {
        version:'1.74',
        lang: 'en',
        restaurantsPerPage: 12, // number of restaurant will display in search page
        ImageServer: ['//media1.weeloy.com', '//media2.weeloy.com', '//media3.weeloy.com', '//media4.weeloy.com', '//media5.weeloy.com'], // list of all image server
        ImageSizes: [140, 180, 270, 300, 325, 360, 450, 500, 600, 700, 1024], // list of all size in pixel, from low to high
        WheelImageSizes: [50, 80, 100, 150], // list of all size in pixel, from low to high
        OrderPerPage: 20,
    };
    EventService.getHomeEvent($scope.restaurant).then(function(response) {
        $scope.events = response.data.event;
    }).catch(function(error) {
        console.error("getHomeEvent() error: "+JSON.stringify(error));
    });    
    }
]);
event.directive('restaurantEventItemnew', function() {
    console.log("RestaurantEventItemNew directive");
    return {
        restrict: 'E',
        templateUrl: 'client/app/shared/partial/_restaurant_event_item_new.html',
        replace: true,
        scope: {
            event: '=',
            openInNewTab: '@',
            mediaServer: '=',
        },
        link: function(scope, element, attrs) {
            var img = document.createElement('img');
            var imageSize = 270;
            var ImageWidth = 500;
            // default size of restaurant image is 500px;
            // default size of wheel image is 150px;
            //console.log("RestaurantEventItem scope: "+JSON.stringify(scope));
            var windowWidth = $(window).width();
            if (windowWidth < 768)
                ImageWidth = windowWidth;
            if (767 < windowWidth < 1200)
                ImageWidth = Math.round(windowWidth * 0.8 * 0.45);
            if (1199 < windowWidth)
                ImageWidth = Math.round(windowWidth * 0.8 * 0.25);
            var wheelImageWidth = Math.round(ImageWidth * 0.8 * 0.3);
            // if (scope.imageSize === undefined && ImageWidth > 0) {
            //     scope.config.ImageSizes.forEach(function(value, key) {
            //         if (value > ImageWidth && scope.imageSize === undefined) {
            //             imageSize = value;
            //             scope.imageSize = imageSize;
            //         }
            //     });
            // } else 
            //     imageSize = scope.imageSize;
            // if (typeof imageSize == 'undefined')
            //     scope.imageSize = imageSize = '/500/';
            if (imageSize === 500 || imageSize === '/500/')
                imageSize = '/';
            else {
                if (!(imageSize[0] == '/' && imageSize.charAt[imageSize.length - 1] == '/'))
                    imageSize = '/' + imageSize + '/';
                imageSize = imageSize.replace(/\/\//g, '/');
                if (typeof imageSize == 'undefined')
                    imageSize = '/360/';
            }
            console.log("imageSize: "+imageSize+", scope.imageSize: "+scope.imageSize);
            //patch 
            //
            //
            //
            //my_image = scope.category.images[Math.floor(Math.random()*scope.category.images.length)];
            // the image will load from random server and best size match with current screen width
            var url;

            if (typeof scope.event.picture != 'undefined') {
                if (scope.mediaServer === undefined) {
                    url = '//media.weeloy.com/upload/restaurant/' + encodeURIComponent(scope.event.restaurant.trim()) + imageSize + encodeURIComponent(scope.event.picture);
                } else {
                    url = scope.mediaServer + '/upload/restaurant/' + encodeURIComponent(scope.event.restaurant.trim()) + imageSize + encodeURIComponent(scope.event.picture);
                }
            }

            img.src = url;
            if (scope.openInNewTab == 'true' && $(window).width() > 480) {
                scope.OpenRestaurantInNewTab = '_blank';
            }
            //$(element).find('.img-transparent').css('background', 'url(' + url + ')');
            // $(element).find('.img-transparent').attr('src', url);
            img.className = img.width > img.height ? 'landscape' : 'portrait';
            img.onload = function() {
                console.log("IMAGE SRC " + img.src);
                $(element).find('.img-transparent').css({background: 'url(' + img.src + ')', 'background-size': 'cover','background-position': 'center'});
                var className = img.width > img.height ? 'landscape' : 'portrait';
                $(element).find('.img-transparent').addClass(className);
                if (className === 'portrait') {

                    var ImageHeight = 0;
                    var maxHeight = 350,
                        ratio = 0;
                    ImageHeight = img.height;
                    ratio = maxHeight / ImageHeight;
                    var ImagenewHeight = ImageHeight * ratio;
                    if (ImagenewHeight > 375) {
                        ImagenewHeight = 375;
                    }
                    console.log("IMAGE HEIGHT" +ImagenewHeight);
                     //$(element).find('.img-transparent').css('height', ImagenewHeight);

                } else {
                    var ImgHeight = 340;
                  //$(element).find('.img-eventabsolute').css('height', ImgHeight);

                }

            };
            scope.go = function(restaurant) {
                document.location.href = "/" + restaurant.internal_path + "/booknow";
                //WindowOpen(restaurant.internal_path + '/book-now');
            };

//            function WindowOpen(url) {
//                window.open(url, '_blank', 'toolbar=no, scrollbars=yes, resizable=yes, top=30, left=30, width=800, height=900');
//            }
        },
    };
});