<?php
require_once("conf/conf.init.inc.php");
require_once("lib/wpdo.inc.php");
require_once('conf/conf.session.inc.php');
require_once("lib/wglobals.inc.php");
require_once("lib/class.images.inc.php");
require_once("lib/class.restaurant.inc.php");
require_once("lib/class.booking.inc.php");
require_once("lib/class.mail.inc.php");
require_once("lib/class.sms.inc.php");
require_once("lib/class.pushnotif.inc.php");
require_once("lib/class.spool.inc.php");
require_once("lib/class.async.inc.php");
require_once("lib/Browser.inc.php");
require_once("lib/class.qrcode.inc.php");
require_once("lib/class.analytics.inc.php");
require_once("lib/class.media.inc.php");
require_once("lib/class.coding.inc.php");
require_once("lib/class.login.inc.php");
require_once("lib/class.transitory.inc.php");
require_once("lib/class.member.inc.php");
require_once ("lib/class.notification.inc.php");

$arglist = array('bksalutation', 'bklast', 'bkfirst', 'bkemail', 'bkcover', 'bkdate', 'bktime', 'bkmobile', 'bkcountry', 'bkphone', 'bkmobilecountry', 'bkrestaurant', 'bktitle', 'bkparam', 'bkspecialrequest', 'brwsr_type', 'bkpage', 'bktracking', 'data', 'booker', 'company', 'hotelguest', 'babychair');
foreach ($arglist as $label) {
	$$label = $_REQUEST[$label];
    $$label = preg_replace("/\'|\"/", "’", $$label);
    $$label = preg_replace("/\s/", ' ', $$label);
    }
    
$fullfeature = preg_match("/TheFunKitchen/", $bkrestaurant) || in_array($_SESSION['user']['member_type'], $fullfeature_member_type_allowed) || ($_SESSION['user']['member_type'] == 'weeloy_sales' && $res->status == 'demo_reference' || $res->status == 'active');

if ($fullfeature)
    $fullfeatflag = "'fullfeature=true;'";
else
    $fullfeatflag = "'fullfeature=false;'";

if (empty($bkrestaurant)) {
   // header("location: https://www.weeloy.com");
   // exit;
}

$resto = new WY_restaurant;
$resto->getRestaurant($bkrestaurant);

$mediadata = new WY_Media($bkrestaurant);
$logo = $mediadata->getLogo($bkrestaurant);

if (empty($resto->restaurant)) {
   // header("location: https://www.weeloy.com");
   // exit;
}

$action = __ROOTDIR__ . "/modules/booking/book_form.php";
$booker = "";
$logger = new WY_log("website");
$logger->LogEvent($_SESSION['user']['id'], 820, '', $resto->restaurant, '', date("Y-m-d H:i:s"));

$bkparamdecoded = detokenize($bkparam);
//if (substr($bkparamdecoded, 0, 3) != date('M') || microtime(true) - floatval(substr($bkparamdecoded, 3)) > 600)
    //header("Location: " . $action . "?bkrestaurant=" . $bkrestaurant . "&bktitle=" . $bktitle . "&bktracking=" . $bktracking . "&data=" . $data . "&timeout=y");

$restaurant_restaurant_tnc = $resto->restaurant_tnc;

$booking = new WY_Booking();

$transitory = new WY_Transitory;
$transitory->read('BOOKING-' . $bkparam);

if ($transitory->result > 0) {
    $prev_confirmation = preg_replace("/BOOKING-/", "", $transitory->content);
    $booking->getBooking($prev_confirmation);
}

if (empty($booking->restaurant)) {

    $booking->restaurant = $bkrestaurant;
    $dateAr = explode("/", $bkdate);
    $bkDBdate = $dateAr[2] . "-" . $dateAr[1] . "-" . $dateAr[0];  // be carefull msqyl Y-m-d, html d/m/Y  qrc Ymd
    $language = "en";

	$optin = '0';
	$bktype = "booking";
	$bkstatus = "";
	$bkstate = "to come";
    $bkextra = $bkproduct = $booking_deposit_id = $hotelguest = "";
	$amobile = $bkmobilecountry . ' ' . str_replace(' ', '', $bkmobile);
    $status = $booking->createBooking($bkrestaurant, $bkDBdate, $bktime, $bkemail, $amobile, $bkcover, $bksalutation, $bkfirst, $bklast, $bkcountry, $language, $bkspecialrequest, $bktype, $bktracking, $booker, $company, $hotelguest, $bkstate, $optin, $booking_deposit_id, $bkstatus, $bkextra, $bkproduct);

    if ($status < 0) {
      //  header("Location: " . $action . "?bkrestaurant=" . $bkrestaurant . "&bktitle=" . $bktitle . "&bktracking=" . $bktracking);
      //  exit;
    }

    $bkconfirmation = $booking->confirmation;
    if ($booking->getBooking($bkconfirmation) == false) {
      //  header("Location: ./" . $action . "?" . $_SERVER['QUERY_STRING']);
      //  exit;
    }

    $transitory->create('BOOKING-' . $bkparam, $booking->confirmation);

    //qrcode generartion
    $path_tmp = explode(':', get_include_path());
    $qrcode_filename = $path_tmp[count($path_tmp) - 1] . 'tmp/' . $bkconfirmation . '.png';
    $qrcode = new QRcode();
    $qrcode->png('{"membCode":"' . $booking->membCode . '"}', $qrcode_filename);
    //file_put_contents($qrcode_filename,$qrcode->image(8));
    $notification = new WY_Notification();
    $notification->notify($booking, 'booking');
} else {  // this is a reload
    $bkconfirmation = $booking->confirmation;
    $bktime = $booking->rtime;
    $bkemail = $booking->email;
    $bkmobile = $booking->mobile;
    $bkcover = $booking->cover;
    $bksalutation = $booking->salutation;
    $bkfirst = $booking->firstname;
    $bklast = $booking->lastname;
    $bkcountry = $booking->country;
    $language = $booking->language;
    $bkspecialrequest = $booking->specialrequest;
}

//qrcode delete
//unlink($qrcode_filename);
//error_log("<br><br>BOOKING = " . $status . " " . $booking->msg);

$QRCodeArg = "&bkrestaurant=$bkrestaurant&bkconfirmation=$bkconfirmation";

$typeBooking = ($resto->is_bookable && ($resto->status == 'active' || $resto->status == 'demo_reference')) ? "true " : "false ";
$is_listing = false;
if (empty($res->is_wheelable) || !$res->is_wheelable) {
    $is_listing = true;
}


$signinandreview = (!preg_match('/CALLCENTER|WEBSITE|GRABZ|facebook/', $tracking)) ? "You can <a href='https://www.weeloy.com' target=_blank>SIGN In</a> using your email to view all your bookings and leave your reviews" : "";
?>


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html><head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta http-equiv='expires' content='mon, 1 jan 1998 05:00:00 gmt'>
        <meta http-equiv='pragma' content='no-cache'>
        <meta http-equiv='pragma' content='cache-control: max-age=0'>
        <meta http-equiv='last-modified' content='mon, 1 jan 1998 05:00:00 gmt'>
        <meta http-equiv='cache-control' content='no-cache, must-revalidate'>
        <meta name='robots' content='noindex, nofollow'>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
        <meta name="HandheldFriendly" content="true" />
        <title>Weeloy Code</title>
        <link href="../../css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="../../css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
        <link href="../../css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href='https://fonts.googleapis.com/css?family=Montserrat|Unkempt|Roboto:400,900,700italic,700,500italic,500,400italic' rel='stylesheet' type='text/css'>
<script type='text/javascript' src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.15/angular.min.js"></script>
<script type="text/javascript" src="//code.jquery.com/jquery-1.11.2.min.js"></script>
<script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.0.2/js/bootstrap.min.js"></script>
<script type='text/javascript' src="../../js/ui-bootstrap-tpls-0.12.1.min.js"></script>


        <style>
            body {
                font-family: Roboto; 
                font-size: 14px;
                width:90%;
            }
            div .input-group {
                margin-bottom: 15px;
            }

            div .row {
                margin-bottom: 10px;
                margin-right: 0px;
                margin-left: 0px;
            }


            .termsconditions {
                font-family: Roboto; 
                font-size: 12px;
            }

            img {
                display:block;
                margin:0 auto;
                padding-bottom:20px;
            }


            div .separation {
                vertical-align:middle;
                border-right:4px solid #5285a0;
                height:100%;
            }

<?php if ($brwsr_type != "mobile") echo ".mainbox { width:100%} "; ?>

            .panel{
                border-color:#5285a0 !important;
            }

            .panel-heading{
                background-image:none !important;
                background-color:#5285a0 !important;
                color:#fff !important;
                border-color:#5285a0 !important;
                border-radius:0;
            }

            .btn-default{
                background-image:linear-gradient(to bottom, #eee 0px, #e0e0e0 100%)!important;
            }


        </style>
<?php include_once("ressources/analyticstracking.php") ?>
    </head>
    <body ng-app="myApp"  style="width: 100%;min-height: 410px;background-color: transparent;">

        <div id='booking' ng-controller='MainController'  >
            <div class="container mainbox" style=' padding-left: 0px;padding-right: 0px;'>    
                <div class="panel panel-info" >
                    <div style="   padding: 6px 15px 0;background: #303030 none repeat scroll 0 0;color: white;" class="panel-body" >
                        <div style="margin: 0px 0px 5px; padding: 10px; color: green; background-color: white;" id="congrats">
                            <img style='display: inline-block;padding-bottom: 0 !important;' width="30px" src="../../images/validation.png" heigh="30px">
                            <p style="display: inline-block; margin: 5px; vertical-align: middle;">Congratulations</p>
                        </div>
                        <div class='row'>Your booking is confirmed</div>
                        <div class='row'>Confirmation: <?php echo $booking->confirmation;?> </div>
                        
                        <div class='row'>You will receive all details by email and sms.</div>
                        <div class='row' >
                            <table style="width:100%;text-align: center"><tr>
                                    <td>
                                        <div><span>Add to Calendar</span></div>
                                        <img width="160px" alt="" src="" id="img_calendar">
                                    </td>
                                    
                                </tr></table>
                        </div>
                    </div>
                </div>
            </div>                     
        </div>  


        <script>

<?php
echo "var bkrestaurant = '" . $bkrestaurant . "';";
echo "var confirmation = '" . $booking->confirmation . "';";
echo "var tracking = '" . $booking->tracking . "';";
echo "var hoteltitle = '" . $booking->restaurantinfo->title . "';";
echo "var typeBooking = $typeBooking;";
printf("var is_listing = %s;", $is_listing ? 'true' : 'false');
?>

            var app = angular.module("myApp", []);

            app.controller('MainController', function ($scope, $http) {

                $scope.bookingTitle = (typeBooking) ? "BOOKING CONFIRMED" : "REQUESTED CONFIRMED";
                $scope.confirmationMessage = (typeBooking) ? "Your booking is confirmed at " : "Your request has been sent to ";
                $scope.confirmationMessage += hoteltitle;
                $scope.confirmationMessage2 = (typeBooking) ? "We are happy to confirm your reservation " : "Your reservation number is ";
                $scope.confirmationMessage2 += confirmation;
                $scope.confirmationMessage3 = "You will receive the details of your reservation by email and SMS";
                $scope.listTags = (typeBooking) ? "Instant Reservation" : "No reservartion";
                $scope.listTags2 = (typeBooking) ? "" : "First come, first serve";
                $scope.listTags3 = (!is_listing) ? "Spin the Wheel" : "Enjoy incredible";
                $scope.listTags4 = (!is_listing) ? "at the restaurant" : " promotion";
            });

            $(document).ready(function () {
                url_calendar = '../qrcode/QRCode.php?type=1&bkrestaurant=' + bkrestaurant +'&bkconfirmation='+ confirmation;
                url_contact = '../qrcode/QRCode.php?type=0&bkrestaurant='+ bkrestaurant +'&bkconfirmation='+ confirmation;
                $("#img_calendar").attr('src', url_calendar);
                $("#img_contact").attr('src', url_contact);
            });

        </script>
    </body>
</html>

