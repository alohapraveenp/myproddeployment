<?php

namespace App\Request;

class Request
{
    public static function input()
    {
        $rawData = file_get_contents("php://input");
        return json_decode($rawData);
    }
}
