<?php

require 'conf/conf.init.inc.php';
require 'conf/conf.mysql.inc.php';
require_once("lib/wpdo.inc.php");

require 'lib/class.paypal.inc.php';
require './vendor/autoload.php';
require_once "lib/class.mail.inc.php";
require_once "lib/class.payment.inc.php";
require_once "lib/class.event.inc.php";



use App\Database\Database;
use App\Models\DepositBooking;
use App\Models\Event;
use App\Request\Request;
use App\Response\Response;
use Illuminate\Database\Capsule\Manager as Capsule;

/**
 * event booking database name
 *
 * @const string
 */
define('EVENT_BOOKING_TABLE', 'event_booking');
define('BOOKING_DEPOSIT_TABLE', 'booking_deposit');
define('PAYPAL_MOD', 'sandbox');

//ini_set('display_errors', 'On');

/**
 * create new Capsule manager instance
 *
 * @var Illuminate\Database\Capsule\Manager
 */

$mailer = new JM_Mail;
$capsule = new Capsule;

$capsule->addConnection([
    'driver'    => 'mysql',
    'host'      => $dbhost,
    'database'  => $dbname,
    'username'  => $dbuser,
    'password'  => $dbpass,
    'charset'   => 'utf8',
    'collation' => 'utf8_unicode_ci',
    'prefix'    => '',
]);

$capsule->setAsGlobal();
$capsule->bootEloquent();



if (isset($_REQUEST['action'])) {
    switch ($_REQUEST['action']) {
        case 'create_database':
            /**
             * create new database for event booking task
             * will be disabled in production
             */
            Database::create();
            break;
        case 'drop_database':
            /**
             * drop event booking database
             * will be disabled in production
             */
            Database::drop();
            break;
        case 'insert':
           
            /**
             * insert new event booking record
             */
   
            $data = Request::input();
           
            doPayment($data);
            break;
        case 'booking_deposit_ipn':
            /**
             * insert new event booking record
             */
            $data = Request::input();
            CheckPaypalNotification($data);
            break;
        case 'booking_deposit':
            depositPayment();
            break;
            // case 'booking_deposit_confirm':
            //     $deposit                  = DepositBooking::find($_GET['id']);
            //     $data                     = json_decode($deposit->data);
            //     $data->booking_deposit_id = $_GET['id'];
            //     $client                   = new GuzzleHttp\Client([
            //         'base_uri' => __BASE_URL__,
            //         'headers'  => [
            //             'Content-Type' => RequestOptions::FORM_PARAMS,
            //         ],
            //     ]);
            //     $response = $client->request('POST', '/modules/booking/confirmation.php', ['form_params' => (array) $data]);
            //     $body     = $response->getBody();
            //     echo $body->getContents();
            //     break;
        case 'booking_deposit_stripe':
            depositByStripe();
            break;
    }
}

function doPayment($data)
{
    $pay               = new WY_Payment();

    $event                  = new Event();
    $event->restaurant      = $data->restaurant_id;
    $event->event_id        = $data->event_id;
    $event->pax   = $data->pax;
    $event->amount          = $data->amount;
    $event->curency         = $data->curency;
    $event->status          = 'pending';
    $event->time            = $data->DeliveryDate;
    $event->special_request = $data->special_request;
    $event->company         = $data->company;
    $event->first_name      = $data->first_name;
    $event->last_name       = $data->last_name;
    $event->email           = $data->email;
    $event->phone           = $data->phone;
   
     
    $wyevent = new WY_Event();
    $orderId =$wyevent->getUniqueCodeByEvent();
    $event->orderID         = $orderId;
  
    $event->save();
    



    // Response::success($event);
    $restaurant       = $data->restaurant_id;
    $payment           = new WY_Paypal($restaurant);

    $restpayment       = array('paypal_id' =>'payment1@weeloy.com');
    $internationalPath = '';
    $crSales           = $data->amount * 0.10;
    $crSales           = number_format($crSales, 2);
    $respay            = number_format(($data->amount - $crSales), 2);

    //create the pay request
    $createReceiver = array(
        "actionType"         => "PAY",
        "currencyCode"       => "SGD",
        "receiverList"       => array(
            "receiver" => array(
                array(
                    "amount" => $data->amount,
                    "email"  => $restpayment['paypal_id'],
                 )
//                ),
//                array(
//                    "amount"  => $data->amount,
//                    "email"   => "vs.kala-facilitator@weeloy.com", //weeloypaypal email   //philippe.benedetti-buyer@weeloy.com
//                    "primary" => true,
//                ),
            ),
        ),
        "returnUrl"          => $data->returnUrl . $orderId ."?dspl_h=f&dspl_f=f&bktracking=WEBSITE",
        "cancelUrl"          => $data->cancelUrl,
        "ipnNotificationUrl" => $data->ipnNotificationUrl,
        "requestEnvelope"    => $payment->envelope,
    );
    $response = $payment->_paypalSend($createReceiver, "Pay", $payment->apiUrl);
    

 
        if ($response['responseEnvelope']['ack'] == "Success") {
            $event->paykey   = $response['payKey'];
            $event->receiver = json_encode($createReceiver);
            $event->save();
            
            $pay->savePayment($data->amount,'event_booking',$data->event_id,$restaurant,$response['payKey'],'paypal','pending','','','pay');
            $paypal_url = $payment->paypalUrl . $response['payKey'];
            Response::success(['paypal_url' => $paypal_url]);
        }
}

function CheckPaypalNotification($data)
{
        $header['from'] = array('reservation@weeloy.com' => 'Weeloy.com');
        $header['replyto'] = array('info@weeloy.com');
        $raw_post_data  = file_get_contents('php://input');
        $raw_post_array = explode('&', $raw_post_data);
        $myPost         = array();

    foreach ($raw_post_array as $keyval) {
        $keyval = explode('=', $keyval);
        if (count($keyval) == 2) {
            $myPost[$keyval[0]] = urldecode($keyval[1]);
        }
    }
    // read the IPN message sent from PayPal and prepend 'cmd=_notify-validate'
    $req = 'cmd=_notify-validate';
    if (function_exists('get_magic_quotes_gpc')) {
        $get_magic_quotes_exists = true;
    }
    foreach ($myPost as $key => $value) {
        if ($get_magic_quotes_exists == true && get_magic_quotes_gpc() == 1) {
            $value = urlencode(stripslashes($value));
        } else {
            $value = urlencode($value);
        }
        $req .= "&$key=$value";
    }

    // Step 2: POST IPN data back to PayPal to validate
    if (defined(PAYPAL_MOD) && PAYPAL_MOD == 'sandbox') {
        $ch = curl_init('https://www.sandbox.paypal.com/cgi-bin/webscr');
    } else {
        $ch = curl_init('https://www.sandbox.paypal.com/cgi-bin/webscr');
    }
    curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $req);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
    curl_setopt($ch, CURLOPT_FORBID_REUSE, 1);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Connection: Close'));
    // In wamp-like environments that do not come bundled with root authority certificates,
    // please download 'cacert.pem' from "http://curl.haxx.se/docs/caextract.html" and set
    // the directory path of the certificate as shown below:
    // curl_setopt($ch, CURLOPT_CAINFO, dirname(__FILE__) . '/cacert.pem');
    if (!($res = curl_exec($ch))) {
        // error_log("Got " . curl_error($ch) . " when processing IPN data");
        curl_close($ch);
        exit;
    }

    if (strcmp($res, "VERIFIED") == 0) {
        $log           = json_encode($_POST);
         
        $event         = Event::where('paykey', '=', $_POST['pay_key'])->first();
        $event->status = 'paid';
        $event->save();
    } else if (strcmp($res, "INVALID") == 0) {
        // $log = "INVALID\n";
    }
    curl_close($ch);
    try {
        $file = __DIR__ . '/log.txt';
        file_put_contents($file, $log);
        echo $file;
    } catch (Exception $e) {
        echo var_dump($e);
    }
}

function depositPayment()
{
    $logger = new WY_log("website");
    $pay               = new WY_Payment();
    $deposit          = new DepositBooking;
    $deposit->data    = json_encode($_POST);
    $deposit->amount  = $_POST['resBookingDeposit'];
    $deposit->curency = $_POST['resCurrency'];
    $token            = md5(time());
    $deposit->token   = $token;
    $deposit->payment_method   = 'paypal';
    $deposit->object_id =$_POST['confirmation'];
   
    $deposit->save();

    $restaurant        = $_POST['restaurant'];
    $payment           = new WY_Paypal($restaurant);
    $res               = new WY_restaurant();
    $restpayment       = $res->getRestaurantPaymentId($restaurant);
    $internationalPath = $res->getRestaurantInternalPath($restaurant);
    $crSales           = $_POST['resBookingDeposit'] * 0.10;
    $crSales           = number_format($crSales, 2);
    $respay            = number_format(($_POST['resBookingDeposit'] - $crSales), 2);
    //booking details
    if(empty($_POST['city'])){
        $_POST['city'] ='singapore';
    }
    
    if(preg_match("/walkin|CALLCENTER|tms|waiting|WEBSITE|facebook|GRABZ/i", $_POST['tracking'])){
         $returnUrl ="/modules/booking/deposit_confirm_redirect.php?booking_deposit_id=$deposit->id&token=$token&paytype=paypal&reaction=bkdepsoit"; 
    }else{
        $returnUrl ="/modules/booking/deposit_confirm_redirect.php?city={$_POST['city']}&title={$_POST['title']}&booking_deposit_id=$deposit->id&token=$token&paytype=paypal&reaction=bkconfirmation";
    }
      

    //create the pay request
    $createReceiver = array(
        "actionType"         => "PAY",
        "currencyCode"       => $_POST['resCurrency'],
        "receiverList"       => array(
            "receiver" => array(
                array(
                    "amount" => $_POST['resBookingDeposit'],
                    "email"  => "suhadev@gmail.com",
                )
            ),
        ),
        "returnUrl"          => __BASE_URL__ .$returnUrl,
        "cancelUrl"          => __BASE_URL__ . $returnUrl.'&actionType=cancel',
        "ipnNotificationUrl" => __BASE_URL__ . '/modules/event_booking/event_booking.php?action=booking_deposit_ipn',
        "requestEnvelope"    => $payment->envelope,
    );

    $deposit->receiver = json_encode($createReceiver);
    $deposit->save();
//     $payKey ='AP-6SD77277FY187810D';
//     $paypal_url = $payment->paypalUrl .'AP-6SD77277FY187810D';
//     $payArr =array('email'=>$restpayment['paypal_id'],'amount'=>$respay,'payUrl'=>$paypal_url);
//     $sql = pdo_exec("UPDATE booking SET booking_deposit_id = '$payKey' WHERE confirmation = '{$_POST['confirmation']}' ");
//       
//    echo  json_encode($payArr);die;
   
      //$paypal_url ="http://localhost:8888/weeloy.com/modules/booking/deposit_confirmation.php";
//     echo "<script>  window.open('$paypal_url','_blank','toolbar=no, scrollbars=yes, copyhistory=no, resizable=no, top=100, left=800, width=685, height=785');</script>";
    //$logger->LogEvent($_SESSION['user']['user_id'], 1000, 1, $_POST['confirmation'],'paypal', date("Y-m-d H:i:s"));
    $response = $payment->_paypalSend($createReceiver, "Pay", $payment->apiUrl);

    
    if ($response['responseEnvelope']['ack'] == "Success") {
        //$logger->LogEvent($_SESSION['user']['user_id'], 1000, 2, $_POST['confirmation'],'paypal', date("Y-m-d H:i:s"));
        $deposit->paykey = $response['payKey'];
        $deposit->save();
        $pay->savePayment($_POST['resBookingDeposit'],'booking_deposit',$_POST['confirmation'],$restpayment['paypal_id'],$response['payKey'],'paypal','PAY',0,0,'PAY');
        $paykey =$response['payKey'];
          
        $sql = pdo_exec("UPDATE booking SET booking_deposit_id = '$paykey' WHERE confirmation = '{$_POST['confirmation']}' ");
        $paypal_url = $payment->paypalUrl . $response['payKey'];
          $payArr = array('email'=>$restpayment['paypal_id'],'amount'=>$respay,'payUrl'=>$paypal_url);
            echo  json_encode($payArr);
            exit;
        //echo "<script> var winOpener =($paypal_url,'blank','toolbar='no', scrollbars=yes, copyhistory=no, resizable='no', top=100, left=800, width=685, height=785');</script>";
        //header('location: ' . $paypal_url);
    }
}
function depositByStripe(){
    

    
    require_once("lib/composer/vendor/stripe/stripe-php/init.php");
    $logger = new WY_log("website");
    $pay= new WY_Payment(); 
    $deposit          = new DepositBooking();
    $deposit->data    = json_encode($_POST);
    $deposit->amount  = $_POST['resBookingDeposit'];
    $deposit->curency = $_POST['resCurrency'];
    $paytoken            = md5(time());
    $deposit->token   = $paytoken;
   
    $deposit->save();
    $restaurant = $_POST['restaurant'];
    $deposit->receiver = $_POST['restaurant'];
    $deposit->save();
    //$logger->LogEvent($_SESSION['user']['user_id'], 1000, 1, $_POST['confirmation'],'stripe', date("Y-m-d H:i:s"));
 


    $stripe = array(
      "secret_key"      => "sk_test_0rGTeyhuIlv29sjHdTorZJw1",
      "publishable_key" => "pk_test_aG4RNDkBJI81YsT2ljQGVnuW"
    );

    \Stripe\Stripe::setApiKey($stripe['secret_key']);
    
    $token  = $_POST['stripeToken'];
    
    if(isset($token)){
       // $logger->LogEvent($_SESSION['user']['user_id'], 1000, 2, $_POST['confirmation'],'stripe', date("Y-m-d H:i:s"));
            $customer = \Stripe\Customer::create(array(
                'email' => $_POST['email'],
                'source'  => $token
            ));
            if(isset($customer)&& $customer->id !== ""){
                $deposit->paykey = $customer->id;
                $deposit->save();
           //     $logger->LogEvent($_SESSION['user']['user_id'], 1000, 3, $_POST['confirmation'],'stripe', date("Y-m-d H:i:s"));
                $pay->savePayment($_POST['resBookingDeposit'],'booking_deposit',$_POST['confirmation'],$restaurant,$customer->id,'stripe','cre_customer',0,0,'token_created');
                $paykey =$customer->id;
                $sql = pdo_exec("UPDATE booking SET booking_deposit_id = '$paykey' WHERE confirmation = '{$_POST['confirmation']}' ");
                $returnUrl = __BASE_URL__ . '/modules/booking/deposit_confirmation.php?booking_deposit_id=' . $deposit->id . '&token=' . $paytoken .'&paytype=stripe&status=COMPLETED';

    //            $charge = \Stripe\Charge::create(array(
    //                    'customer' => $customer->id,
    //                    'amount'   => $_POST['resBookingDeposit'],
    //                    'currency' => $_POST['resCurrency']
    //            ));

            }else{
             //   $logger->LogEvent($_SESSION['user']['user_id'], 1000, 3, $_POST['confirmation'],'stripe-failed', date("Y-m-d H:i:s"));  
                 $returnUrl = __BASE_URL__ . '/modules/booking/deposit_confirmation.php?booking_deposit_id=' . $deposit->id . '&token=' . $paytoken .'&paytype=stripe&&actionType=cancel';
            }
       
    }else{
            $returnUrl = __BASE_URL__ . '/modules/booking/deposit_confirmation.php?booking_deposit_id=' . $deposit->id . '&token=' . $paytoken .'&paytype=stripe&&actionType=cancel'; 
        }
         header('location: ' . $returnUrl);
//         echo "<script>  window.opener.top.location.href =$returnUrl</script>";
//    
//    window.opener.top.location.href = url+location.search;

//  echo '<h1>Successfully charged $50.00!</h1>';

}
