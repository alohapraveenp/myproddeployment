<?php

require_once("conf/conf.init.inc.php");
require_once("lib/wpdo.inc.php");
require_once("lib/class.restaurant.inc.php");

$res = new WY_restaurant();

$base = 'https://www.weeloy.com/';

$xml = new SimpleXMLElement("<?xml version=\"1.0\" encoding=\"utf-8\" ?><urlset></urlset>");
//$xml->addAttribute('xmlns', 'http://www.sitemaps.org/schemas/sitemap/0.9');

$xml->addAttribute('xmlns', 'http://www.sitemaps.org/schemas/sitemap/0.9');
$xml->addAttribute('xsi:schemaLocation', 'http://www.sitemaps.org/schemas/sitemap/0.9http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd');

$now_date = date(DATE_ATOM);

//top url
$top_url = array();
$top_url[] = '';
$top_url[] = 'search/singapore';
$top_url[] = 'search/bangkok';
$top_url[] = 'search/phuket';
$top_url[] = 'search/hongkong';



$top_url[] = 'singapore/favorite_cuisine/french-cuisine';
$top_url[] = 'singapore/favorite_cuisine/italian-cuisine';
$top_url[] = 'singapore/favorite_cuisine/japanese-cuisine';
$top_url[] = 'singapore/favorite_cuisine/chinese-cuisine';
$top_url[] = 'singapore/favorite_cuisine/indian-cuisine';
$top_url[] = 'singapore/favorite_cuisine/korean-cuisine';
$top_url[] = 'singapore/location/sentosa';
$top_url[] = 'singapore/location/tanjong-pagar';
$top_url[] = 'singapore/location/clarke-quay';
$top_url[] = 'singapore/location/boat-quay';
$top_url[] = 'singapore/location/raffles-place';
$top_url[] = 'singapore/location/bukit-timah';
$top_url[] = 'singapore/location/club-street';
$top_url[] = 'singapore/location/orchard';
$top_url[] = 'singapore/location/suntec-city';
$top_url[] = 'singapore/location/vivo-city';
$top_url[] = 'singapore/location/club-street';




$top_url[] = 'dine-and-win-contest-2016';


$url = '';

foreach ($top_url as $url){
    $url_record = $xml->addChild('url');
    $url_record->addChild('loc', $base.$url);
    $url_record->addChild('lastmod', $now_date);
    $url_record->addChild('changefreq', "daily");
}

//top url
$top2_url = array();
$top2_url[] = 'how-it-works';
$top2_url[] = 'contact';
$top2_url[] = 'partner';
$top2_url[] = 'faq';
$top2_url[] = 'terms-and-conditions-of-service';
$top2_url[] = 'privacy-policy';
$top2_url[] = 'allevents';
$top2_url[] = 'chinese-new-year-2016-restaurant-menu';
$top2_url[] = 'valentines-day-2016-restaurant-menu';


$url = '';

foreach ($top2_url as $url){
    $url_record = $xml->addChild('url');
    $url_record->addChild('loc', $base.$url);
    $url_record->addChild('lastmod', $now_date);
    $url_record->addChild('changefreq', "daily");
}



//RESTAURANT PAGES

// get all restaurants for singapore
$sql = "SELECT restaurant,is_wheelable FROM restaurant WHERE status IN ('active') AND is_displayed = 1 order by wheelvalue DESC";
$data = pdo_multiple_select($sql);


$link = '';
foreach ($data as $d){
    $link = $base.$res->getRestaurantInternalPath($d['restaurant']);
    $url_record = $xml->addChild('url');
    $url_record->addChild('loc', $link);
    $url_record->addChild('lastmod', $now_date);
    $url_record->addChild('changefreq', "daily");
}

foreach ($data as $d){
   if($d['is_wheelable']==1){
        $link = $base.$res->getRestaurantInternalPath($d['restaurant']).'/dining-rewards';
        $url_record = $xml->addChild('url');
        $url_record->addChild('loc', $link);
        $url_record->addChild('lastmod', $now_date);
        $url_record->addChild('changefreq', "daily");
    }
}

foreach ($data as $d){
    $link = $base.$res->getRestaurantInternalPath($d['restaurant']).'/book-now';
    $url_record = $xml->addChild('url');
    $url_record->addChild('loc', $link);
    $url_record->addChild('lastmod', $now_date);
    $url_record->addChild('changefreq', "daily");
}



// FROM sitemap

$sql = "SELECT url FROM sitemap ORDER BY nb_res, occur";
$data = pdo_multiple_select($sql);

foreach ($data as $d){
     $url=explode('?',$d['url']);
     $url[0] = urldecode($url[0]);
     $wd ="the fun kitchen";
     $ok ="the one kitchen";
    if (stripos($url[0],$wd) === false && stripos($url[0],$ok) === false) {
      if(!empty($url[0]) && strpos($url[0],'&')< 1){
            $url_record = $xml->addChild('url');
            $url_record->addChild('loc', $url[0]);
            $url_record->addChild('lastmod', $now_date);
            $url_record->addChild('changefreq', "daily");      
      }
    }
}
$rootdir = __SERVERROOT__ . __ROOTDIR__ . '/';
    $file = $rootdir.'sitemap.xml';
  file_put_contents($file, $xml->asXML());
  
  echo "SuccessFully generated sitemap";
//Header('Content-type: text/xml');
//print($xml->asXML());

