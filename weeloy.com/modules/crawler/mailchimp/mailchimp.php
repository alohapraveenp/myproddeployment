<?php
require_once("lib/class.marketing_campaign.inc.php");
require_once("lib/composer/vendor/php-email-parser/PlancakeEmailParser.php");
require_once("simple_html_dom.php");
require_once("lib/class.marketing_campaign.inc.php");
require_once("lib/class.string.inc.php");
echo 'MailChimp PARSER LOADED'.'<br/>';

function parse_mailchimp($email, $account = "") {
	$result = false;
	$parser = new PlancakeEmailParser($email);
	$data['title'] = substr(WY_StringProcessor::FilterOutNonAlphaNumeric($parser->getSubject()),0,64);
	error_log(__FILE__." ".__FUNCTION__." ".__LINE__.": title(".strlen($data['title'])."): ".$data['title']);
	$data['date'] = $parser->getHeader("Date");
	$data['date'] = date("Y-m-d", strtotime($data['date']));
	error_log(__FILE__." ".__FUNCTION__." ".__LINE__.": date: ".$data['date']);
	$html = str_get_html($parser->getHTMLBody());
	foreach($html->find('a') as $url) {
		if (strpos($url->href, "us11.campaign-archive") !== false || strpos($url->href, "mailchi.mp/weeloy") !== false) {
			$data['url'] = $url->href;
    		error_log(__FILE__." ".__FUNCTION__." ".__LINE__.": url(".strlen($url->href)."): ".$url->href);
    		break;
		}
	}
	if (!empty($data['url'])) {
		$mailchimp = new WY_MarketingCampaign();
		$data['description'] = "";
		$data['tag'] = "";
		$data['image']['name'] = "";
    	if ($mailchimp->processMailChimp($data, null)) {
    		$result = true;
    		error_log(__FILE__." ".__FUNCTION__." ".__LINE__." successful!");
    	} else
    		error_log(__FILE__." ".__FUNCTION__." ".__LINE__." failed!");
	}
	return $result;
}
?>