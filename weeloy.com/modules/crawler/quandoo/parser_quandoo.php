<?php

$booker = "QUANDOO";

echo $booker . ' PARSER LOADED'.'<br/>';

function parse_quandoo($msgorg, $account = "") {
	$resultAr = array();
	$etape = 0;

	if(isset($_REQUEST["DEBUG"]) && $_REQUEST["DEBUG"] == "73") {
		echo $account . "<hr /><br />";
		echo $msgorg . "<hr />";
		}
	
	$msgorg = preg_replace("/\"|\'/", " ", $msgorg);
	$msgorg = preg_replace("/\<br[^>]+\>/i", "\r", $msgorg);
	
	$msgorg = preg_replace("/=[\r\n]+/", "", $msgorg);
	$msgorg = preg_replace("/=0D|=0A/", "\r", $msgorg);

    $msgorg = preg_replace("/=(\r|\s|\|)+(\r|\s|\|)*/", "", $msgorg);
        
    $msgorg = preg_replace("/.*\<body\>(.*)\<\/body\>.*/", "$1", $msgorg);
	$msgorg = preg_replace("/\<[^>]+\>/", "", $msgorg);
	$msgorg = preg_replace("/&nbsp;/", " ", $msgorg);
	$msgorg = preg_replace("/\r|\n/", "|", $msgorg);
	$msgorg = preg_replace("/\s+/", " ", $msgorg);
	$msgorg = preg_replace("/\=\|/", "", $msgorg);
    $msgorg = preg_replace("/\|\=20\|/", "|", $msgorg);
	$msgorg = preg_replace("/\|+\s*\|*/", "|", $msgorg);
	$msgorg = preg_replace("/:\|/", ":", $msgorg);
	$msgorg = preg_replace("/.*(Login: .*)Do you have any questions.*/", "|$1", $msgorg);
	$msgorg = preg_replace("/The original reservation ([^|]+)\|Reservation Information/", "$0 Original", $msgorg);
	$msgorg = preg_replace("/Dear ([^|]+)\s+Management,\|/", "Restaurant: $1|", $msgorg);
 	
	$msgorg = preg_replace("/Reservation Information: (\d+) guests on /", "Customer Cover: $1|Customer Date: ", $msgorg);
	$msgorg = preg_replace("/Reservation Information: (\d+) on /", "Customer Cover: $1|Customer Date: ", $msgorg);
	$msgorg = preg_replace("/Customer Date: ([^|,]+,[^|,]+), ([^|]+)\|/", "Customer Date: $1|Customer Time: $2", $msgorg);
	$msgorg = preg_replace("/Reservation Information Original: (\d+) on /", "Original Customer Cover: $1|Original Customer Date: ", $msgorg);
	$msgorg = preg_replace("/Original Customer Date: ([^|,]+,[^|,]+), ([^|]+)\|/", "Original Customer Date: $1|Original Customer Time: $2", $msgorg);
	
	// reduce the search to plain text
	//$found = preg_match("/Content-Type: text\/plain;((?(?!Content-Type).)*)Content-Type.*/", $msgorg, $match);

	$msg = $msgorg;

	//echo $msg . "<br/><hr/>";

	// search for mode	
	$keyword = array("A reservation has been cancelled" => "cancellation", "UPDATES" => "modification", "New Reservation" => "booking");
	foreach($keyword as $label => $value) {
		if(preg_match("/$label/i", $msg)) {
			$resultAr["processing"] = $value;
			break;
			}
		}

	// search for information	
	$keyword = array("Login:" => "restaurant", "When:" => "rdate", "Customer Time:" => "rtime", "For:" => "fullname", "Guests:" => "cover", "Res. ID.:" => "confirmation", "Phone:" => "mobile", "E-Mail:" => "email", "Info:" => "specialrequest", "Offer:" => "Offer");
	foreach($keyword as $label => $value) {
		$pat = "/\|" . $label . "\s*([^|]+)\|/";
		//if($label = "Notes") $pat = "/|Notes\s+([^=]+)?(?(1)\s+)=(?:[029]{2})/";
		$resultAr[$value] = "";
		if(preg_match($pat, $msg, $match))
			$resultAr[$value] = (count($match) > 1) ? $match[1] : "";
		if($value == "specialrequest" && !empty($resultAr[$value]))
			$resultAr[$value] = str_replace("&nbsp;", " ", $resultAr[$value]);
		}

	if(isset($resultAr["fullname"])) {
		$tmp = explode(" ", trim($resultAr["fullname"]));
		$n = count($tmp);
		$resultAr["salutation"] = "";
		if($n > 1) {
			$resultAr["last"] = $tmp[$n - 1];
			$resultAr["first"] = trim(substr($resultAr["fullname"], 0, strlen($resultAr["fullname"]) - strlen($resultAr["last"])));
			}
		}

	if(!empty($resultAr["rdate"])) {
		$temp = explode("at", $resultAr["rdate"]);
		$resultAr["rtime"] = trim($temp[1]);
		$temp = explode(".", trim($temp[0]));
		$resultAr["rdate"] = $temp[2] . "-" . $temp[1] . "-" . $temp[0];
		}
	
	if(!empty($resultAr["Offer"])) {
		if(!empty($resultAr["specialrequest"]))
			$resultAr["specialrequest"] .= ", ";		
		$resultAr["specialrequest"] .= "OFFER: " . $resultAr["Offer"];		
		}
		
	if($resultAr["processing"] === "booking") {
		}
	if($resultAr["processing"] === "cancellation") {
		}
	if($resultAr["processing"] === "modification") {
		}
	
	if(!empty($resultAr["cover"]))
		$resultAr["cover"] = intval($resultAr["cover"]);
		
	$resultAr["status"] = 1;	

	if(isset($_REQUEST["DEBUG"]) && $_REQUEST["DEBUG"] == "73") {
		echo "<h3>" . $resultAr["processing"] . "</h3>";
		foreach($resultAr as $label => $value) {
			echo "<hr />'" . $label . "' ===> '" . $value . "'<br />";
			}
		echo "<br />version 1.1";
		}

	foreach ($resultAr as &$value) 
		$value = preg_replace("/\'|\"|\&\#39;/", "`", $value);

	print_r($resultAr);
	
	return $resultAr;
}

//Quandoo booking
//$msg = file_get_contents("../../../ressources/tools/quandoo.html");
//$data = email_parser($msg);	

?>
