// multi purpose, app will differentiate chat from general purpose ....
// Read: param { channel: restaurant_name, mode: mode, token: token } mode: "h" for history, "t" for today, or a date (yyyy-mm-dd)
// Write: param { channel: restaurant_name, content: text, mode: mode, token: token } mode: "h" for history, "t" for today, or a date (yyyy-mm-dd)

app.service('chatService',['$http',function($http){

	var chatService = this;
	this.read = function(params, route){
		var prefix = (typeof route === "string") ? route : "",
			url = prefix + "../../api/services.php/notes/read/";
		params.app = "chat";		
		return $http.post(url, params)
    			.then(function(response) {return response.data;});
	};

	this.write = function(params, route){
		var prefix = (typeof route === "string") ? route : "",
			url = prefix + "../../api/services.php/notes/write/";		
		params.app = "chat";		
		return $http.post(url, params)
    			.then(function(response) {return response.data;});
	};
	
}]);
