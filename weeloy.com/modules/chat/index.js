app.controller('ChatController', ['$scope','$http','$timeout','$window','chatService', '$localStorage', function($scope,$http,$timeout,$window,chatService,$localStorage) {
   
   var AWS = require('aws-sdk');
   var AWSIoTData = require('aws-iot-device-sdk');

   var AWSConfiguration = require('./aws-configuration.js');
   var msg = '';
   var key = ["bkrestaurant", "email", "token"];
   var mp = ["restaurant", "email", "token"];

   $scope.messageHistory = '';
   $scope.rmsg = '';
   $scope.restaurant = $scope.token = $scope.email = "";
   
   arg = window.location.search;
   var doms =  window.location.hostname;
   doms = doms.split(".");
   doms = doms[0];
   if(arg.length > 10) 
      arg = arg.substring(1, 99);
   argAr = arg.split('&');

   argAr.map(function(ll, index) {
      if(index >= key.length) return;
      $scope[v] = "";
      var m = key[index], pat= key[index] + "=", v = mp[index];
      if(ll.search(pat) > -1)
         $scope[v] = ll.substring(pat.length);
   });

   // Create a client id to use when connecting to AWS IoT.
   $scope.clientId = 'mqtt-explorer-' + (Math.floor((Math.random() * 100000) + 1));
   $scope.nickname = $scope.email.substring(0, $scope.email.lastIndexOf("@"));
   $scope.currentlySubscribedTopic = doms+"-"+$scope.restaurant;
   $scope.mode = "T";
   // Initialize our configuration.
   AWS.config.region = AWSConfiguration.region;
   AWS.config.credentials = new AWS.CognitoIdentityCredentials({
      IdentityPoolId: AWSConfiguration.poolId
   });
   // Create the AWS IoT device object.  Note that the credentials must be 
   // initialized with empty strings; when we successfully authenticate to
   // the Cognito Identity Pool, the credentials will be dynamically updated.
   const mqttClient = AWSIoTData.device({
      // Set the AWS region we will operate in.
      region: AWS.config.region,
      clientId: $scope.clientId,
      protocol: 'wss',
      maximumReconnectTimeMs: 8000,
      debug: true,
      accessKeyId: '',
      secretKey: '',
      sessionToken: ''
   });

   // Attempt to authenticate to the Cognito Identity Pool.
   var cognitoIdentity = new AWS.CognitoIdentity();
   AWS.config.credentials.get(function(err, data) {
      if (!err) {
         var params = {
            IdentityId: AWS.config.credentials.identityId
         };
         cognitoIdentity.getCredentialsForIdentity(params, function(err, data) {
            if (!err) {
               mqttClient.updateWebSocketCredentials(data.Credentials.AccessKeyId,
                  data.Credentials.SecretKey,
                  data.Credentials.SessionToken);
            } else {
               alert('error retrieving credentials: ' + err);
            }
         });
      } else {
         alert('error retrieving identity: ' + err);
      }
   });

   
   $window.mqttClientConnectHandler = function() {
      console.log("Connected");
      document.getElementById("connecting-div").style.display = 'none';
      document.getElementById("chat_window").style.display = 'block';
      messageHistory = '';
      $scope.showdata("T");
      //check if there is a history msg for today
      // Subscribe to our current topic.
      mqttClient.subscribe($scope.currentlySubscribedTopic);
   };

   $window.mqttClientReconnectHandler = function() {
      console.log("ReConnecting");
      document.getElementById("connecting-div").style.display = 'none';
   };

   // Utility function to determine if a value has been defined.
   $window.isUndefined = function(value) {
      return typeof value === 'undefined' || typeof value === null;
   };

   $window.mqttClientMessageHandler = function(topic, payload) {
      var tmp = '';
      $scope.rmsg = JSON.parse(payload.toString());
      tmp = $scope.rmsg.Nickname;
      tmp = tmp.split(".");
      if($scope.rmsg.Nickname != $scope.nickname)
      {
         msg = "<div class='message right appeared'>"+
                  "<li class='message'>"+
                     "<div class='text_wrapper'><div class='text'>"+tmp[0]+" <span class='timestyle'> "+$scope.rmsg.Time+" </span> > "+$scope.rmsg.Message+"</div>"+
                     "</div>"+
                  "</li>"+
               "</div>";
         $('#messagearea').append(msg);

         $('#messagearea').animate({scrollTop: $('#messagearea').prop("scrollHeight")}, 800);
         
      }
   };

   //
   // Handle the UI for the current topic subscription
   //
   $window.updateSubscriptionTopic = function() {
      mqttClient.subscribe($scope.currentlySubscribedTopic);
   };
   //
   // Handle the UI to clear the history window
   //
   $window.clearHistory = function() {
      if (confirm('Delete message history?') === true) {
         //document.getElementById('subscribe-div').innerHTML = '<p><br></p>';
         messageHistory = '';
      }
   };
   //
   // Handle the UI to update the topic we're publishing on
   //
   $window.updatePublishTopic = function() {
      updateSubscriptionTopic();
   };

   function checkTime(i) {
     if (i < 10) {
       i = "0" + i;
     }
     return i;
   }
   //
   // Handle the UI to update the data we're publishing
   //
   $window.updatePublishData = function() {

      var today = new Date();
      var h = today.getHours();
      var m = today.getMinutes();
      var publishText = document.getElementById('publish-data').value;
      var publishText2 = document.getElementById('publish-data').value;
      var publishText3 = '';
      var tmpsmgs = publishText2;
      var sendtime = "("+h+":"+checkTime(m)+")";
      
      if($.trim(publishText).length > 0){
            publishText = {
               "Time" : sendtime,
               "Nickname": $scope.nickname,
               "Message": publishText
            }
            publishText = JSON.stringify(publishText);
            publishText2 = $scope.nickname+" ("+h+":"+checkTime(m)+")"+" > "+publishText2;
            publishText3 = $scope.nickname;
            publishText3 = publishText3.split(".");

            tmpsmgs = publishText3[0]+" <span class='timestyle'> ("+h+":"+checkTime(m)+")"+" </span> > "+tmpsmgs;
            mqttClient.publish($scope.currentlySubscribedTopic, publishText);
            document.getElementById('publish-data').value = '';
            //insert into the table
            chatService.write({channel: $scope.restaurant, mode: $scope.mode , content: publishText2, token: $scope.token}).then(function(response) {
              
            });
            $localStorage.today = $localStorage.today +"|||"+publishText2;
            //insert into text area
            var msg = "";
            msg = "<div class='message left appeared'>"+
                     "<li class='message'>"+
                        "<div class='text_wrapper'><div class='text'>"+tmpsmgs+"</div>"+
                        "</div>"+
                     "</li>"+
                  "</div>"; 
            $('#messagearea').append(msg);
            $('#messagearea').animate({scrollTop: $('#messagearea').prop("scrollHeight")}, 800);
      }
      else
      {
         document.getElementById('publish-data').value = '';
      }

      

   };



   $scope.showdata = function(mode)
   {
      var tmpmsg = '';
      var tmpnick = '';
      var tmptime = '';
      var cc = '';
      var cnick = '';
      if(mode != 'T'){
         cc = $localStorage.history; 
         document.getElementById('entrypoint').style.display = 'none';
      }
      else{
         cc = $localStorage.today;
         document.getElementById('entrypoint').style.display = 'block';
      }
      $('#messagearea').html('');
      
      cc = cc.split("|||");
      $scope.dateshow = cc[0];
      console.log($scope.dateshow);
      for(i=1;i<cc.length;i++)
      {  
         tmpmsg = cc[i];
         tmpmsg = tmpmsg.split(" > ");
         cnick = tmpmsg[0].split(" (");
         tmpnick = tmpmsg[0].split(".");

         tmpnick = tmpnick[0].split(" (");
         tmptime = tmpmsg[0].split(" (");

         if(cnick[0] != $scope.nickname)
         {
            msg = "<div class='message right appeared'>"+
                     "<li class='message'>"+
                        "<div class='text_wrapper'><div class='text'>"+tmpnick[0]+" <span class='timestyle'> ("+tmptime[1]+" </span> > "+tmpmsg[1]+"</div>"+
                        "</div>"+
                     "</li>"+
                  "</div>";
            $('#messagearea').append(msg);
            $('#messagearea').animate({scrollTop: $('#messagearea').prop("scrollHeight")}, 0);
         }
         else
         {
            msg = "<div class='message left appeared'>"+
                     "<li class='message'>"+
                        "<div class='text_wrapper'><div class='text'>"+tmpnick[0]+" <span class='timestyle'> ("+tmptime[1]+" </span> > "+tmpmsg[1]+"</div>"+
                        "</div>"+
                     "</li>"+
                  "</div>";
            $('#messagearea').append(msg);
            $('#messagearea').animate({scrollTop: $('#messagearea').prop("scrollHeight")}, 0);
            
         }
      }
   };

   //Store Locale Storage
   $scope.loadtolocale = function()
   {
      console.log("Called");
      chatService.read({channel: $scope.restaurant, mode: "T", token: $scope.token}).then(function(response) 
      {
         $localStorage.today = response.data.content;
      });

      chatService.read({channel: $scope.restaurant, mode: "History", token: $scope.token}).then(function(response) 
      {
         $localStorage.history = response.data.content;
      });
      

   }

   $scope.loadtolocale();
   

   //sleep function
   $scope.sleep = function()
   {
      if(confirm("Are you sure you want to sleep TMS Chat?") == false)
         return;      
      else
      mqttClient.unsubscribe($scope.currentlySubscribedTopic);
   }
   //restart function
   $scope.restart = function()
   {
      mqttClient.subscribe($scope.currentlySubscribedTopic);
      $scope.showdata('T');
   }

   //
   // Install connect/reconnect event handlers.
   //
   mqttClient.on('connect', window.mqttClientConnectHandler);
   mqttClient.on('reconnect', window.mqttClientReconnectHandler);
   mqttClient.on('message', window.mqttClientMessageHandler);

   //
   // Initialize divs.
   //
   document.getElementById('connecting-div').style.display = 'block';
   document.getElementById('connecting-div').innerHTML = '<center><h2>Connecting to TMS Chat...<h2></center>';

}]);

