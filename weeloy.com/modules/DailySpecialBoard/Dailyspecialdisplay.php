<style>
body
{
  overflow:hidden;
}

.dailyspecialcontainer {
    float: left;
    height: 340px;
    width: 272px;
    margin-bottom: 15px;
    margin-left: 15px;

}

.dailyspecialimage {
    width: 270px;
    height: 160px;
    background-color: #f2f2f2;
    margin-bottom: 5px;
}

.dailyrestaurantcontainer
{
  font-weight: bold;
  font-size: 14px;
}

.dailytitlecontainer 
{
    font-weight: bold;
    font-size: 16px;
}

.dailyspecialdesc {
    width: 270px;
    height: 110px;
    overflow-y: auto;
}

.clearboth 
{
  clear: both;
}

.custombtn 
{
  padding: 9px 100px;
}

.showdata
{
  height: 650px;
  width: auto;
  overflow-y: scroll;
}

.showdata::-webkit-scrollbar {
  display: none;
}

.mainholder{
    height: 100%;
    width: 100%;
    overflow: hidden;
}

.weeloy-text {
  color: #FFFFFF;
  background-color: #283271 !important;
}

.weeloy-text:hover {
    color: #FFFFFF;
}


</style>
<div ng-controller="DailyspecialBoardController" ng-init="moduleName='dsb';">
    <div class="container-fluid">
    <br>
    <div class="container-fluid">
      <div class="mainholder">
        <div class = "showdata" when-scrolled ="more()">
          <div data-ng-repeat ="item in items">
            <div class="dailyspecialcontainer">
              <div class="dailytitlecontainer">{{item.headline}}</div>
              <div class="dailyspecialimage"><img src="{{item.image}}" /> </div>
              <div class="dailyspecialdesc">{{item.description}}</div>
              <div class="dailyspecialbtn"><center><input class="btn weeloy-text custombtn" type="button" name="" value="Book Now"></center></div>
            </div>
          </div>
          <div class="clearboth"></div>
          <div data-ng-show = "loading"> Loading... </div>
        </div>
      </div>
    </div>
</div>