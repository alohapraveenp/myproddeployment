<?php
    require_once("conf/conf.init.inc.php");
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html;" />
        <meta name="author" content="weeloy. https://www.weeloy.com"/>
        <meta name="copyright" content="weeloy. https://www.weeloy.com"/>  
        <meta http-equiv='expires' content='mon, 1 jan 1998 05:00:00 gmt'/>
        <meta http-equiv='pragma' content='no-cache'/>
        <meta http-equiv='pragma' content='cache-control: max-age=0'/>
        <meta http-equiv='last-modified' content='mon, 1 jan 1998 05:00:00 gmt'/>
        <meta http-equiv='cache-control' content='no-cache, must-revalidate'/>
        <meta name='robots' content='noindex, nofollow'/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
        <meta name="HandheldFriendly" content="true" />
        <title>Daily Special Board Restaurant Registration</title>
        <base href="<?php echo __ROOTDIR__; ?>/" />
        <link href="client/bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="client/bower_components/bootstrap/dist/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
        <link href="client/bower_components/fontawesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="client/bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css" rel="stylesheet" />
        <link href="modules/DailySpecialBoard/Registration.css" rel="stylesheet" />
		<script type="text/javascript" src="client/bower_components/jquery/dist/jquery.min.js"></script>
        <script type="text/javascript" src="js/jquery-ui.js"></script>
        <script type='text/javascript' src="client/bower_components/angular/angular.min.js"></script>
        <script type="text/javascript" src="client/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="client/bower_components/ui-bootstrap-tpls-0.14.2.min.js"></script>
        <script type="text/javascript" src="client/bower_components/moment/min/moment.min.js"></script>
        <script type="text/javascript" src="js/ngStorage.min.js"></script>
        <script type="text/javascript" src="js/mylocal.js"></script>
        <script type="text/javascript" src="client/bower_components/ng-file-upload/ng-file-upload-shim.min.js"></script>
        <script type="text/javascript" src="client/bower_components/ng-file-upload/ng-file-upload-all.min.js"></script>
        <script src="https://www.google.com/recaptcha/api.js" async defer></script>
        <script>
        function correctCaptcha() {
            console.log('correctCaptcha');
            return true;
        }
        var app = angular.module('dsb', ['ui.bootstrap', 'ngLocale', 'ngStorage', 'ngFileUpload']);
        </script>
		<script type="text/javascript" src="modules/DailySpecialBoard/RegistrationShort.js"></script> 
    </head>
<body ng-app="dsb" style="background:transparent;">
<?php include_once("RegistrationShort.php") ?>
</body>
</html>