<style>
.main-form {
    max-width: 550px;
    margin-top: 5px; 
}

textarea {
resize: none;
}

.custombtn {
    padding: 15px 80px;
}
</style>
<div ng-controller="DailyspecialController" ng-init="moduleName='dsb';">
    <div class="container" style="padding-left:30px;padding-right:7px; margin-left:25%" >
    <div class = "dailytitle"> <h1>Post A Daily Special</h1> </div>
    <div class = "dailytitlesub"><a>First Time? Click here.</a></div>
    <form id="form-border" name="Dailyspecialform" ng-submit="saveitem(Dailyspecialform)" novalidate style="padding:5px;">
    <br>
    <br>
    <div class="row" style="margin: 0 0 20px 0;font-size:12px;font-familly:Roboto">
        <div class="container insidec">
    		 <div class="row">
                <div class="col-md-3 col-xl-3">
                    <div class="form-group row">
                      <label class="col-2 col-form-label">Email Address</label>
                      <input class="form-control" type="text" value="" ng-model="login.email">
                    </div>
                    <div class="error error-msg" style='color:red; display: none;'>
                        <span ng-show="RegistrationForm.$submitted && RegistrationForm.{{y.a}}.$error.email">Invalid Email!</span>
                    </div>
                </div>

                <div class="col-md-1 col-xl-1">
                    
                </div>
                <div class="col-md-3 col-xl-3">
                    <div class="form-group row">
                      <label class="col-2 col-form-label">Password</label>
                      <input class="form-control" type="Password" value="" ng-model="login.password">
                    </div>
                    <div class="error error-msg" style='color:red; display: none;'>
                        <span ng-show="RegistrationForm.$submitted && RegistrationForm.{{y.a}}.$error.email">Invalid Password!</span>
                    </div>
                </div>
             </div>

             <br>
             <div class="row">
                <div class="col-md-7 col-xl-7">
                    <div class="form-group row">
                        <input class="form-control" type="text" placeholder="Headline" ng-model="restaurant.headline">
                    </div>
                    <div class="error error-msg" style='color:red; display: none;'>
                        <span ng-show="RegistrationForm.$submitted && RegistrationForm.{{y.a}}.$error.email">Invalid Headline!</span>
                    </div>      
                </div>
             </div>

             <div class="row">
                <div class="col-md-7 col-xl-7">
                    <div class="form-group row">
                        <textarea class="form-control" placeholder="Description" rows="5" ng-model="restaurant.description"></textarea>
                    </div>
                    <div class="error error-msg" style='color:red; display: none;'>
                        <span ng-show="RegistrationForm.$submitted && RegistrationForm.{{y.a}}.$error.email">Invalid Description!</span>
                    </div>      
                </div>
             </div>

             <div class="row">
                <div class="col-md-7 col-xl-7">
                    <div class="form-group row">
                        <input type="file" name="img" id="img" class="form-control input-sm" onchange="angular.element(this).scope().uploadImage(this)" ng-model="file" placeholder="Upload Files">
                    </div>  
                    <div class="error error-msg" style='color:red; display: none;'>
                        <span ng-show="RegistrationForm.$submitted && RegistrationForm.{{y.a}}.$error.email">Invalid File!</span>
                    </div>     
                </div>
                
             </div>
             <br>
             <br>
             <div class="row">
                <div class="col-md-7 col-xl-7">
                    <label class="col-2 col-form-label">Select up to 3 categories</label>    
                </div>
             </div>
             <div class="row">
                <div class="col-md-3 col-xl-3">
                    
                    <select ng-model="restaurant.selectedOption['cat1']" 
                            ng-options="cuisines.cuisine as cuisines.cuisine for cuisines in cuisinelist">
                        <option value="">Select Categories</option>
                    </select>  
                </div>
                <div class="col-md-3 col-xl-3">
                    <select ng-model="restaurant.selectedOption2" 
                            ng-options="cuisines.cuisine as cuisines.cuisine for cuisines in cuisinelist">
                        <option value="">Select Categories</option>
                    </select>  
                </div>
                <div class="col-md-3 col-xl-3">
                    <select ng-model="restaurant.selectedOption3" 
                            ng-options="cuisines.cuisine as cuisines.cuisine for cuisines in cuisinelist">
                        <option value="">Select Categories</option>
                    </select>  
                </div>
             </div>
             <br>
             <br>

             <div class="row">
                
                <div class="col-md-7 col-xl-7">
                    
                    <center><button type="submit" class="btn btn-success custombtn">Add you post now</button></center>
                </div>
             </div>
             
        </div>
    </div>
    </form>
    </div>
</div>