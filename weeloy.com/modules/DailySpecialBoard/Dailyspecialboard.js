app.controller('DailyspecialBoardController', ['$scope','$http','$timeout', 'Upload', function($scope,$http,$timeout, Upload) {

  $scope.restaurant = null;
  
  $scope.items = [];
  $scope.itemstemp = [];
  $scope.temp=null;
  
  $scope.loading = true;  

  var index = 0;
  $scope.getdailyspecial = function()
  {

    $http.get('api/md_marketing/getdailyspecial/').then( function(response) {  
        $scope.dsblist = null;
        
        dsblistcheck = response.data.status;
        if(dsblistcheck == 1)
        {
          dsblist = response.data.data.row;
          for (i = 0; i < dsblist.length; i++) {
                cc = dsblist[i].content;
                $scope.itemstemp[i] = cc;
              }
              $scope.more();  
        }
        else
        {
          $scope.loading = false;
          alert(response.data.errors);
        }
        
    }); 
  }

  $scope.getdailyspecial();

  // this function fetches a random text and adds it to array
  $scope.more = function()
  {
    $scope.loading = false;
    temp = $scope.itemstemp.slice(index, index+10);
        //alert(temp.length);
        for (x = 0; x < temp.length; x++) {
              cc = temp[x];
              $scope.items.push(cc);
        }
        index=index+10;

  };

}]);

// we create a simple directive to modify behavior of <div>
app.directive("whenScrolled", function(){
  return{
    
    restrict: 'A',
    link: function(scope, elem, attrs){
    
      raw = elem[0];

      elem.bind("scroll", function(){
        if(raw.scrollTop+raw.offsetHeight+5 >= raw.scrollHeight){
          scope.loading = true;          
        // we can give any function which loads more elements into the list
          scope.$apply(attrs.whenScrolled);
        }
      });
    }
  }
});