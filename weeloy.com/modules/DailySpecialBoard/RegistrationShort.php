<style>
.main-form {
    max-width: 550px;
    margin-top: 5px; 
}
.outercont {
    padding-left: 20px;
    width: 600px;
    height: 900px;
}
.headertitle{
    font-size: 18px;
    margin-bottom: 5px;
    margin-top: 10px;
}

.headertitlesub{
    font-size: 12px;
}

.innerconter{
    width: 660px;
    height: 600px;
    margin-top: 20px;
}

.custcol{
    /*width: 565px;*/
    height: 30px;
    margin-bottom: 10px;
}

.custcol1{
    /*width: 565px;*/
    height: 30px;
    margin-bottom: 55px;
}

.custcol2{
    /*width: 565px;*/
    height: 45px;
    margin-bottom: 10px;
}

.custcol3{
    /*width: 565px;*/
    height: 100px;
    margin-bottom: 35px; 
}

.custcol4{
    /*width: 565px;*/
    height: 30px;
    margin-bottom: 40px;
}

.custcol5{
    /*width: 565px;*/
    height: 55px;
    margin-bottom: 10px;
}

.custcol6 {
    /*width: 565px;*/
    height: 55px;
    margin-bottom: 10px;
    margin-top: 60px;
}

.custcol7{
    /*width: 565px;*/
    height: 30px;
    margin-bottom: 10px;
}

.custcol8{
    /*width: 565px;*/
    height: 30px;
    margin-bottom: 10px;
}

.objcont {
    width: 440px;
    height: 30px;
    float: left;
}

.objcont2 {
    width: 440px;
    height: 100px;
    float: left;
}

.objcont3 {
    width: 400px;
    height: 55px;
    float: left;
}

.titlecontainer {
    width: 120px;
    height: 30px;
    text-align: right;
    float: left;
    margin-right: 10px;
}

.titlecontainer1 {
    width: 70px;
    height: 30px;
    text-align: right;
    float: left;
}

.titlecontainer2 {
    width: 110px;
    height: 30px;
    text-align: right;
    float: left;
}

.custcoltitle {
    text-align: right;
    font-size: 12px;
    margin-top: 7px;
}

.textholder {
    border-radius: 5px;
    border: solid 1px #e0dede;
    width: 100%;
    height: 30px;
}

.textholder1{
    border-radius: 5px;
    border: solid 1px #e0dede;
    width: 100%;
    height: 45px;
}

.textholder2{
    height: 100px;
    border-radius: 5px;
    border: solid 1px #e0dede;
    width: 100%;
    resize: none;
}

    

.objholdercust {
    float: left;
    margin-right: 10px;
    width: 215px;
}

.objholdercust2{
    float: left;
    width: 215px;
}

.objholdercust3 {
    float: left;
    width: 440px;
    height: 100px;
}

.objholdercustleft{
    float: left;
    margin-right: 10px;
    width: 98px;
}

.objholdercustright{
    float: left;
    width: 332px;
}

.objholdercustc1{
    float: left;
    margin-right: 10px;
    width: 140px;
    border-radius: 5px;
    border: solid 1px #e0dede;
    height: 30px;
}
.objholdercustc2{
    float: left;
    width: 140px;
    border-radius: 5px;
    border: solid 1px #e0dede;
    height: 30px;
}

.objholdercusto{
    float: left;
    width: 215px;
    height: 45px;
    margin-right: 10px;
    border-radius: 5px;
    border: solid 1px #e0dede;
    height: 45px;
}

.objholdercusto1{
    float: left;
    width: 215px;
    height: 45px;
    border-radius: 5px;
    border: solid 1px #e0dede;
    height: 45px;
}

.objholderlisting{
    float: left;
    width: 215px;
    height: 30px;
    margin-left: 55px;
    border-radius: 5px;
    border: solid 1px #e0dede;
}

.objholderlisting1{
    float: left;
    width: 215px;
    height: 30px;
    margin-left: 80px;
}

.objholder{
    float: left;
    width: 440px;
}

.custchkbox {
    width: 24px;
    height: 24px;
    float: left;
    margin-right: 10px;
    margin-left: 75px;
}

.custchkboxtext{
    float: left;
    margin-top: 5px;
    font-size: 10px;
}

.customul{
    font-size: 10px;
}

.custobtn{
    width: 365px;
    height: 50px;
    background-color: #089e0d;
    color: #ffffff;
    border: none;
}

.customcap{
    margin-left: 0px;
    padding-left: 140px; 
    margin-bottom: 10px;
}


.dsbbannerholder{
    max-width: 625px;
    height: 185px;
    margin-left: auto;
    margin-right: auto;
    margin-bottom: 10px;
}
.dsbbannerimg {
    width: 100%;
    text-align: center;
    margin-top: 5px;
}
.titleboard{
    text-align: center;
    color: #fff;
    padding-top: 60px;
}
.dsbtext {
    font-size: 15px;
}

.dsbtextc {
    font-size: 25px;
}
.regcont{
 width: 650px;
 height: 900px;
 margin-left: auto;
 margin-right: auto;
}

.dsbtitle {
    font-size: 35px;
    margin-top: 20px;
}

.dsbsubtitle {
    text-align: left;
    font-size: 15px;
    width: 540px;
}

.arial {
    font-family: Arial, Helvetica, sans-serif;
    text-align: left;
}
.titletop {
    margin-top: 20px;
    font-family: Arial, Helvetica, sans-serif;
    font-size: 30px;
}

/*Check box custom*/
label {
    display: inline;
}
 
.regular-checkbox {
    display: none;
}
 
.regular-checkbox + label {
    background-color: #ffffff;
    border: 1px solid #cacece;
   
    padding: 9px;
    border-radius: 3px;
    display: inline-block;
    position: relative;
}
 
.regular-checkbox + label:active, .regular-checkbox:checked + label:active {
    box-shadow: 0 1px 2px rgba(0,0,0,0.05), inset 0px 1px 3px rgba(0,0,0,0.1);
}
 
.regular-checkbox:checked + label {
    background-color: #ffffff;
    border: 1px solid #adb8c0;
    
    color: #99a1a7;
}
 
.regular-checkbox:checked + label:after {
    content: '\2714';
    font-size: 14px;
    position: absolute;
    top: 0px;
    left: 3px;
    color: #99a1a7;
}
 
 
.big-checkbox + label {
    padding: 11px;
}
 
.big-checkbox:checked + label:after {
    font-size: 18px;
    left: 4px;
}

/*dropdown*/
.select-style {
    padding: 0;
    margin: 0;
    width: 138px;
    height: 28px;
    border-radius: 5px;
    overflow: hidden;
    background-color: #fff;
    color: #c7c4c4;
    font-size: 11px;
    
}

.select-style2 {
    padding: 0;
    margin: 0;
    width: 214px;
    overflow: hidden;
    text-align: center;
    
}

.select-style2 select {
    text-align: center;
    padding: 5px 50px;
    width: 110%;
    border: none;
    box-shadow: none;
    background-color: transparent;
    background-image: none;
    -webkit-appearance: none;
       -moz-appearance: none;
            appearance: none;
}

.errorinput{
    color: red;
    float: left;
}

.select-style select {
    font-size: 11px;
    padding: 5px 8px;
    width: 130%;
    border: none;
    box-shadow: none;
    background-color: transparent;
    background-image: none;
    -webkit-appearance: none;
       -moz-appearance: none;
            appearance: none;
}

.select-style select:focus {
    outline: none;
}

.select-style2 select:focus {
    outline: none;
}

/*custom file input*/
input[type="file"] {
    display: none;
}
.custom-file-upload {
    display: inline-block;
    cursor: pointer;
}

.custfiletext {
    text-align: center;
    font-size: 12px;
    font-weight: normal;
    width: 215px;
    color: #c7c4c4;
}

.customtext{
    margin-top: 8px;
}

.customtext1{
    font-size: 10px;
}

.btntext {
    width: 315px;
    margin-top: 55px;
}


::-webkit-input-placeholder { /* WebKit, Blink, Edge */
    color:    #c7c4c4;
    font-size: 11px;
}
:-moz-placeholder { /* Mozilla Firefox 4 to 18 */
   color:    #c7c4c4;
   opacity:  1;
   font-size: 11px;
}
::-moz-placeholder { /* Mozilla Firefox 19+ */
   color:    #c7c4c4;
   opacity:  1;
   font-size: 11px;
}
:-ms-input-placeholder { /* Internet Explorer 10-11 */
   color:    #c7c4c4;
   font-size: 11px;
}

input{
    padding: 10px;
}

.submitted .ng-invalid{
        border: 1px solid red;
    }

@media screen and (max-width : 610px) {
    .outercont {
        padding-left: 0px;
        width: 100%;
    }

    .innerconter {
        width: 100%;
        height: 1150px;
        margin-top: 55px;
    }

    .titlecontainer {
        width: 100px;
        height: 30px;
        text-align: right;
        float: left;
        margin-right: 10px;
    }

    .custcoltitle {
        text-align: left;
        font-size: 12px;
        margin-top: 7px;
    }

    .titlecontainer {
        width: 100%;
        height: 30px;
        text-align: left;
        float: left;
        margin-right: 0px; 
    }

    .objcont {
        width: 100%;
        height: 33px;
        float: left;
    }

    .objcont2 {
        width: 100%;
        height: 100px;
        float: left;
    }

    .objcont3 {
        width: 100%;
        height: 55px;
        float: none;
    }

    .objholdercust {
        float: left;
        margin-right: 0px; 
        width: 50%;
    }
    .objholdercust2 {
        float: left;
        width: 50%;
    }

    .objholdercustleft {
        float: left;
        margin-right: 0px; 
        width: 100%;
    }

    .objholdercustright {
        float: left;
        width: 100%;
    }

    .custcol {
        width: 100%;
        height: 60Px;
        margin-bottom: 10px;
    }

    .custcol2 {
        width: 100%;
        height: 125px;
        margin-bottom: 10px;
    }

    .custcol3 {
        width: 100%;
        height: 110px;
        margin-bottom: 35px;
    }

    .custcol4 {
        width: 100%;
        height: 50px;
        margin-bottom: 40px;
    }

    .custcol5 {
        width: 100%;
        height: 55px;
        margin-bottom: 10px;
    }

    .custcol6 {
        width: 100%;
        height: 55px;
        margin-bottom: 10px;
        margin-top: 60px;
    }

    .custcol7 {
        width: 100%;
        height: 110px;
        margin-bottom: 10px;
    }

    .custcol8 {
        height: 90Px;
        margin-bottom: 10px;
    }

    .objholdercustc1 {
        float: left;
        margin-right: 0px; 
        width: 100%;
        border-radius: 5px;
        border: solid 1px #e0dede;
        height: 30px;
    }

    .objholdercustc2 {
        float: left;
        width: 100%;
        border-radius: 5px;
        border: solid 1px #e0dede;
        height: 30px;
    }

    .select-style {
        padding: 0;
        margin: 0;
        width: 100%;
        height: 28px;
        border-radius: 5px;
        overflow: hidden;
        background-color: #fff;
        color: #c7c4c4;
        font-size: 11px;
    }

    .objholdercusto {
        float: left;
        width: 100%;
        height: 45px;
        margin-right: 10px;
        border-radius: 5px;
        border: solid 1px #e0dede;
        height: 45px;
    }

    .objholdercusto1 {
        float: left;
        width: 100%;
        height: 45px;
        border-radius: 5px;
        border: solid 1px #e0dede;
        height: 45px;
    }

    .objholdercust3 {
        float: left;
        width: 100%;
        height: 100px;
    }

    .objholderlisting {
        float: left;
        width: 100%;
        height: 30px;
        margin-left: 0px; 
        border-radius: 5px;
        border: solid 1px #e0dede;
    }



    .select-style2 {
        padding: 0;
        margin: 0;
        width: 100%;
        overflow: hidden;
        text-align: center;
    }

    .select-style2 select {
        text-align: center;
        padding: 5px 50px;
        width: 60%;
        border: none;
        box-shadow: none;
        background-color: transparent;
        background-image: none;
        -webkit-appearance: none;
        -moz-appearance: none;
        appearance: none;
    }

    .customcap {
        margin-left: 0px;
        padding-left: 0px; 
        margin-bottom: 10px;
    }

    .custobtn {
        width: 100%;
        height: 50px;
        background-color: #089e0d;
        color: #ffffff;
        border: none;
    }

    .custom-file-upload {
        display: inline-block;
        cursor: pointer;
        width: 100%;
    }

    .custfiletext {
        text-align: center;
        font-size: 12px;
        font-weight: normal;
        width: 100%;
        color: #c7c4c4;
    }

    .objholder {
        float: left;
        width: 100%;
    }

}


</style>
<div ng-controller="DSBRegistrationController" ng-init="moduleName='dsb';">
    <div class="outercont">
        <div class="dsbbannerimg">
            <img src="images/weeloy-logo-150.jpg" width="100px" height="100px" />
            <div class="titletop">Daily Specials Board</div>
            <br>
            <div class="arial">
                The Daily Specials Board is a free platform for restaurants, bars and cafes to tell people about what is going on and why they should visit. If you would like to have your restaurant included, please take a moment to register.
            </div>
        </div>
        <form id="form-border" name="RegistrationForm" ng-submit="saveitem(RegistrationForm)" ng-class="{'submitted': submitted}" style="padding:5px;">
            <div class="innerconter">

                <div class="custcol">
                    <div class="titlecontainer">
                        <div class="custcoltitle">Restaurant Name</div>
                    </div>
                     
                    <div class="objcont">
                        <div class="objholder">
                            <input class="textholder" type="text" name="name" ng-model="restaurant['name']" ng-change="cleaninput(name)"  required/>  
                        </div>
                    </div>
                </div>

                <div class="custcol">
                    <div class="titlecontainer">
                        <div class="custcoltitle">URL / Facebook Page</div>
                    </div>
                     
                    <div class="objcont">
                        <div class="objholder">
                            <input class="textholder" type="text" name="website" ng-model="restaurant['website']" ng-change="cleaninput(title)" required/>
                        </div>
                        
                    </div>
                </div>
                
                <div class="custcol">
                    <div class="titlecontainer">
                        <div class="custcoltitle">Your Name</div>
                    </div>
                     
                    <div class="objcont">
                        <div class="objholdercust">
                            <input class="textholder" type="text" name="firstname" placeholder="First Name" ng-model="restaurant['firstname']" ng-change="cleaninput(firstname)" required/>
                            
                        </div>
                        <div class="objholdercust2">
                            <input class="textholder" type="text" name="lastname" placeholder="Last Name" ng-model="restaurant['lastname']" ng-change="cleaninput(lastname)" required/>
                            
                        </div>
                    </div>
                </div>


                <div class="custcol">
                    <div class="titlecontainer">
                        <div class="custcoltitle">Email Address</div>
                    </div>
                     
                    <div class="objcont">
                        <div class="objholder">
                            <input class="textholder" type="text" name="email" ng-model="restaurant['email']" ng-change="cleaninput(email)"  required/>
                        </div>
                        
                    </div>
                </div>

                <div class="custcol">
                    <div class="titlecontainer">
                        <div class="custcoltitle">Phone Number</div>
                    </div>
                     
                    <div class="objcont">
                        <div class="objholder">
                            <input class="textholder" type="text" name="phone" ng-model="restaurant['phone']" ng-change="cleaninput(phone)"  required/>
                        </div>
                        
                    </div>
                </div>

                <div class="custcol">
                    <div class="titlecontainer">
                        <div class="custcoltitle">City</div>
                    </div>
                     
                    <div class="objcont">
                        <div class="objholder">
                            <input class="textholder" type="text" name="city" ng-model="restaurant['city']" ng-change="cleaninput(city)"  required/>
                        </div>
                        
                    </div>
                </div>

                <div class="custcol">
                    <div class="titlecontainer">
                        <div class="custcoltitle">Country</div>
                    </div>
                     
                    <div class="objcont">
                        <div class="objholder">
                            <input class="textholder" type="text" name="country" ng-model="restaurant['country']" ng-change="cleaninput(country)"  required/>
                        </div>
                        
                    </div>
                </div>

                <div class="custcol1">
                    <div class="titlecontainer">
                        <div class="custcoltitle"></div>
                    </div>
                     
                    <div class="objcont">
                        <div class="objholder">
                            <!-- <input class="custchkbox" type="checkbox"/> <div style="font-size: 10px;">I am the authorized representative of this business.</div> -->

                            <div>
                                <div class="custchkbox"><input type="checkbox" id="checkbox-2-1" class="regular-checkbox big-checkbox" ng-model="restaurant['owner']"   ng-required="false"/><label for="checkbox-2-1"></label></div>
                                <div class="custchkboxtext">I am the authorized representative of this business.</div>
                            </div>
                        </div>
                        
                    </div>
                </div>

                <div class="custcol5">
                    <div class="titlecontainer1">
                        <div class="custcoltitle"></div>
                    </div>
                     
                    <div class="objcont3">
                        <div class="objholder">
                            <ul class="customul">
                                <li>You will receive a confirmation via email with a link to your profile page.</li>
                                <li>You will have the opportunity to add additional information to your profile.</li>
                                <li>You may edit your information at any time.</li>
                                <li>You may delete your listing at any time.</li>
                            </ul>
                        </div>
                        
                    </div>
                </div>

                <div class="custcol">
                    <div class="g-recaptcha col-md-12 customcap" data-sitekey="6LdmpA4UAAAAAETsRaMqz8cjrUoyFCNwuuvJk5DS" data-callback="correctCaptcha" ng-show="showCaptcha">
                    
                    </div>
                </div>

                <div class="custcol6">
                    <div class="titlecontainer2">
                        <div class="custcoltitle"></div>
                    </div>
                     
                    <div class="objcont">
                        <div class="objholder">
                            <input type="submit" value ="Add Your Restaurant Now" class="custobtn" ng-click="submitted= true;">
                        </div>
                        <div class="btntext">
                            Once you have sent us your information you will receive an email confirmation.
                        </div>
                    </div>
                </div>


        	</div>	 
        </form>
    </div>
</div>