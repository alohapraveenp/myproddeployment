<?php
    require_once("conf/conf.init.inc.php");
    require_once("lib/class.restaurant.inc.php");

    if (isset($_GET['mrs']) && $_GET['mrs'] != '') {
        // echo $_GET['mrs'];
        $mrs = filter_input( INPUT_GET, 'mrs', FILTER_SANITIZE_URL );
    } else {
        echo "No params";
    }

    if (isset($_GET['mrv']) && $_GET['mrv'] != '') {
        // echo $_GET['mrs'];
        $mrv = filter_input( INPUT_GET, 'mrv', FILTER_SANITIZE_URL );
    } else {
        echo "No review params ";
    }

    if ($mrv == 'SE') {
        $grade = 4;  
    } else if ($mrv == 'SD') {
        $grade = 3;
    } else if ($mrv == 'SC') {
        $grade = 2;
    }

    $res = new WY_restaurant;
    $res->getRestaurant($mrs);

    $bkgcustomcolor = "";
    // if (!empty($res->bkgcustomcolor) && preg_match("/website|facebook/i", $bktracking)) {
    if (!empty($res->bkgcustomcolor)) {    
        if (strlen($res->bkgcustomcolor) > 3 && preg_match("/\|/", $res->bkgcustomcolor)) {
            $tt = explode("|", $res->bkgcustomcolor);
            if (strlen($tt[0]) > 2) {
                $bkgcustomcolor = $tt[0];
            }
        }
    }

?>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; />
        <meta name="author" content="weeloy. https://www.weeloy.com"/>
        <meta name="copyright" content="weeloy. https://www.weeloy.com"/>  
        <meta http-equiv='expires' content='mon, 1 jan 1998 05:00:00 gmt'/>
        <meta http-equiv='pragma' content='no-cache'/>
        <meta http-equiv='pragma' content='cache-control: max-age=0'/>
        <meta http-equiv='last-modified' content='mon, 1 jan 1998 05:00:00 gmt'/>
        <meta http-equiv='cache-control' content='no-cache, must-revalidate'/>
        <meta name='robots' content='noindex, nofollow'/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
        <meta name="HandheldFriendly" content="true" />
        
        <title>Customer Reviews</title>

        
        <base href="<?php echo __ROOTDIR__; ?>/" />

        <link href="client/bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="client/bower_components/bootstrap/dist/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
        <link href="client/bower_components/fontawesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="modules/review_widget/review_widget.css" rel="stylesheet" type="text/css"/>
        <link href="client/bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css" rel="stylesheet" />
        <script type="text/javascript" src="client/bower_components/jquery/dist/jquery.min.js"></script>
        <script type="text/javascript" src="js/jquery-ui.js"></script>
        <script type='text/javascript' src="client/bower_components/angular/angular.min.js"></script>
        <script type="text/javascript" src="client/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="client/bower_components/ui-bootstrap-tpls-0.14.2.min.js"></script>
        <script type="text/javascript" src="client/bower_components/moment/min/moment.min.js"></script>
        <script type="text/javascript" src="js/ngStorage.min.js"></script>
        <script type="text/javascript" src="js/mylocal.js"></script>

        <script> var app = angular.module("myApp", ['ui.bootstrap', 'ngLocale', 'ngStorage']);
            
            // $( document ).ready(function() {
            //     setGraph();  
            // });

            // function setGraph() {
            //     setTimeout(function () { 
            //         $('.horizontal .progress-fill span').each(function(){
            //         var percent = $(this).html();
            //         $(this).parent().css('width', percent);
            //         });
            //     }, 1200); 
            // }

        </script> 

        <style>

            <?php
//                 if(!empty($bktracking) && preg_match("/GRABZ/i", $bktracking)){$bkgcustomcolor = '#000000';}
                if (!empty($bkgcustomcolor)) {
                    echo <<<DELIMITER
                        h2, h4, h5 { background-color: $bkgcustomcolor; color: #ffffff;}
                        .cnt {background: transparent;}
                        .panel { border-color: $bkgcustomcolor !important; }
                        .panel-heading { background-color: $bkgcustomcolor !important; border-color: $bkgcustomcolor !important; }
                        .caret { color: $bkgcustomcolor; }
                        @media (min-width: 530px) {
                            .separation { border-right: 4px solid $bkgcustomcolor; height: 100%; }
                            .separation-left { border-left: 4px solid $bkgcustomcolor; }
                        }
DELIMITER;
                }
            ?>

        </style>

      
    </head>
      
    <body ng-app="myApp" style="background:transparent;">

        <div class="container " ng-controller='ReviewController' ng-init="moduleName = 'review_widget';" >

            <div class="col-sm-12 col-md-8 col-lg-6 mainbox" style='background-color:white;max-width:600px!important;' >
<!--                <h4>Reviews</h4>-->
                <div class='row'>
                    <div style='border-bottom: 1px #bfbfbf solid; margin-left:15px; padding-left: 20px; padding-right: 0px;'>
                        Total <span ng-bind="totalcount" style='margin-left:0px;padding-left:0px;'></span> <span ng-show="totalcount > 1">Reviews</span><span ng-show="totalcount == 1">Review</span>
                        <span class="stars stars{{avgscore| number:0}} pull-right" style='padding-top:0px;margin-top: -1px;'> </span>
                        <span class="pull-right" ng-bind="scoredesc" style='padding-top:0px;' ></span>

                    </div>
                    <table style='margin-top:0px!important;'>
                        <tr ng-if="grade5prcnt > 0">
                           <td style="width: 20%">
                             5 Star
                           </td> 
                            <td style="width: 60%">
                                <div class="progress-bar horizontal">
                                    <div class="progress-track">
                                        <div class="progress-fill" style="width: {{grade5prcnt}}%">
                                            <span>{{ grade5prcnt | number: 0 }}%</span>
                                        </div>
                                    </div>
                                </div>
                    
                           </td>
                            <td style="width: 20%">
                            Total : {{grade5}}
                            </td>
                        </tr>
                        <tr ng-if="grade4prcnt > 0">
                           <td style="width: 20%">
                             4 Star
                           </td> 
                            <td style="width: 60%">
                                <div class="progress-bar horizontal">
                                    <div class="progress-track">
                                        <div class="progress-fill" style="width: {{grade4prcnt}}%">
                                            <span>{{ grade4prcnt | number: 0 }}%</span>
                                        </div>
                                    </div>
                                </div>
                            </td>
                            <td style="width: 20%">
                            Total : {{grade4}}
                            </td>
                        </tr>
                        <tr ng-if="grade3prcnt > 0">
                           <td style="width: 20%">
                             5 Star
                           </td> 
                            <td style="width: 60%">
                                <div class="progress-bar horizontal">
                                    <div class="progress-track">
                                        <div class="progress-fill" style="width: {{grade3prcnt}}%">
                                            <span>{{ grade3prcnt | number: 0 }}%</span>
                                        </div>
                                    </div>
                            </div>
                           </td>
                            <td style="width: 20%">
                            Total : {{grade4}}
                            </td>
                        </tr>
                    </table>
                        <ul class ='list-unstyled'>
                            <li ng-repeat="review in reviews | startFrom:(pager.currentPage - 1) * pageSize | limitTo:pageSize">
                                <div class="cnt">
                                    <div class='review-label' >
                                        <span style="float:left;padding-top:10px;padding-right:20px;width:30%!important" >
                                            <span  ng-if='review.user_name' style='width:10px!important;overflow: hidden;'>
                                                <span ng-bind="review.user_name" style='width:10px!important;overflow: hidden;'></span>, 
                                            </span>

                                            <span  ng-if='review.time'>
                                                <span ng-bind="review.time" style='padding-left:10px;'></span>, 
                                            </span>
                                        </span> 
                                        <p maxlength="15" href ng-if="review.comment !== ''" data-toggle="popover" data-placement="top" title="{{review.comment}}" onmouseenter="$(this).popover('show')" onmouseleave="$(this).popover('hide')"  style='float: left;padding-right:10px;padding-top:7px;'>{{review.comment}}</p>
                                        <div class="stars stars{{review.score | number:0}}" style='float:right;padding-top:15px;padding-left:25px;'>

                                        </div>
                                        <br style="clear: left;" />
                                    </div>
                               </div>   
                            </li>
                        </ul>
                        <div class='pagination-section' style="margin-top: 15px; padding-top: 10px;">
             
                                <ul ng-if="numberOfPages > 1" class="pagination" style='margin-left:70px;' >
  
                                    <li ng-class="{hide:pager.currentPage < 3 || numberOfPages === 3}">
                                        <a ng-click="setPage(1)"><span aria-hidden="true">&laquo;</span></a>
                                    </li>
                                    <li ng-class="{hide:pager.currentPage <= 1}">
                                        <a ng-click="setPage(pager.currentPage - 1)"><span class="glyphicon glyphicon-menu-left" aria-hidden="true"></span></a>
                                    </li>
                                    <li ng-class="{hide:pager.currentPage < 3 || numberOfPages === 3}">
                                        <a ><span class="glyphicon glyphicon-option-horizontal" aria-hidden="true"></span></a>
                                    </li>
                                    <li ng-repeat="n in pager.pages" ng-class="{active:pager.currentPage === n}">
                                        <a ng-click="setPage(n)">{{n}}</a>
                                    </li> 
                                    <li ng-class="{hide:pager.currentPage === 1 || pager.currentPage + 1 >= numberOfPages}">
                                        <a ><span class="glyphicon glyphicon-option-horizontal" aria-hidden="true"></span></a>
                                    </li>               
                                    <li ng-class="{hide:pager.currentPage === numberOfPages}">
                                        <a ng-click="setPage(pager.currentPage + 1)"><span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span></a>
                                    </li>
                                    <li ng-class="{hide:pager.currentPage === numberOfPages || numberOfPages <= 3}">
                                        <a ng-click="setPage(numberOfPages)"><span aria-hidden="true">&raquo;</span></a>
                                    </li>
                                </ul>
                        </div>
                </div>
           </div>
        </div>


    
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
<!--                <table style='margin-top:0px!important;'>
                     <tr> 
                    
                        
                     </tr> 
                    <div class="col-sm-12 col-md-12 col-lg-12">
                    <tr ng-if="grade5prcnt > 0">
                        <td style="width: 20%">
                            5 Star
                        </td>

                        <td style="width:60%;">
                            <div class="progress-bar horizontal">
                                <div class="progress-track">
                                    <div class="progress-fill" style="width: {{grade5prcnt}}%">
                                        <span>{{ grade5prcnt | number: 0 }}%</span>
                                    </div>
                                </div>
                            </div>
                        </td> 

                        <td style="width: 20%">
                            Total : {{grade5}}
                        </td>
                    </tr>

                    <tr ng-if="grade4prcnt > 0">
                        <td style="width: 20%;">
                            4 Star
                        </td>

                        <td style='width:60%; '>
                            <div class="progress-bar horizontal">
                                <div class="progress-track">
                                    <div class="progress-fill" style="width: {{grade4prcnt}}%">
                                        <span>{{ grade4prcnt | number: 0 }}%</span>
                                    </div>
                                </div>
                            </div>
                        </td> 
                        
                        <td style='width: 20% ;'>
                            Total : {{grade4}}
                        </td>
                      
                    </tr>
                    <tr ng-if="grade3prcnt > 0" >
                        <td style="width: 20%;">
                            3 Star
                        </td>
                        
                        <td style="width:60%">
                            <div class="progress-bar horizontal">
                                <div class="progress-track">
                                    <div class="progress-fill" style="width: {{grade3prcnt}}%">
                                        <span>{{ grade3prcnt | number: 0 }}%</span>
                                    </div>
                                </div>
                            </div>
                        </td> 

                        <td style="width: 20%">
                            Total : {{grade3}}
                        </td>
                      
                    </tr>

                    </table>
                    </div>
                
                <div class='review-section'>
                        <div class="col-sm-12 col-md-12 col-lg-12">
                                <ul class ='list-unstyled'>
                                    <li ng-repeat="review in reviews | startFrom:(pager.currentPage - 1) * pageSize | limitTo:pageSize">
                                        <div class="cnt">
                                            <div class='review-label' >
                                                <span style="float:left;padding-top:10px;padding-right:20px;width:30%!important" >
                                                    <span  ng-if='review.user_name' style='width:10px!important;overflow: hidden;'>
                                                        <span ng-bind="review.user_name" style='width:10px!important;overflow: hidden;'></span>, 
                                                    </span>

                                                    <span  ng-if='review.time'>
                                                        <span ng-bind="review.time" style='padding-left:10px;'></span>, 
                                                    </span>
                                                </span> 
                                                <p maxlength="15" href ng-if="review.comment !== ''" data-toggle="popover" data-placement="top" title="{{review.comment}}" onmouseenter="$(this).popover('show')" onmouseleave="$(this).popover('hide')"  style='float: left;padding-right:10px;padding-top:7px;'>{{review.comment}}</p>
                                                <div class="stars stars{{review.score | number:0}}" style='float:right;padding-top:15px;padding-left:25px;'>

                                                </div>
                                                <br style="clear: left;" />
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                        </div>
                 
                            <div class='pagination-section' style="margin-top: 15px; padding-top: 10px;">
                                 pager 
                                <ul ng-if="numberOfPages > 1" class="pagination" style='margin-left:70px;' >
  
                                    <li ng-class="{hide:pager.currentPage < 3 || numberOfPages === 3}">
                                        <a ng-click="setPage(1)"><span aria-hidden="true">&laquo;</span></a>
                                    </li>
                                    <li ng-class="{hide:pager.currentPage <= 1}">
                                        <a ng-click="setPage(pager.currentPage - 1)"><span class="glyphicon glyphicon-menu-left" aria-hidden="true"></span></a>
                                    </li>
                                    <li ng-class="{hide:pager.currentPage < 3 || numberOfPages === 3}">
                                        <a ><span class="glyphicon glyphicon-option-horizontal" aria-hidden="true"></span></a>
                                    </li>
                                    <li ng-repeat="n in pager.pages" ng-class="{active:pager.currentPage === n}">
                                        <a ng-click="setPage(n)">{{n}}</a>
                                    </li> 
                                    <li ng-class="{hide:pager.currentPage === 1 || pager.currentPage + 1 >= numberOfPages}">
                                        <a ><span class="glyphicon glyphicon-option-horizontal" aria-hidden="true"></span></a>
                                    </li>               
                                    <li ng-class="{hide:pager.currentPage === numberOfPages}">
                                        <a ng-click="setPage(pager.currentPage + 1)"><span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span></a>
                                    </li>
                                    <li ng-class="{hide:pager.currentPage === numberOfPages || numberOfPages <= 3}">
                                        <a ng-click="setPage(numberOfPages)"><span aria-hidden="true">&raquo;</span></a>
                                    </li>
                                </ul>
                                 pager 
                            </div>
                         
                       

                </div>

            </div>

        </div>      -->

        <script>
               
            app.controller('ReviewController', function ($scope, $http) {
                        
                $scope.currentPage = 1;
                $scope.pageSize = 10;

                $scope.getPager = function (totalItems, currentPage, pageSize) {
        
                    // default to first page
                    currentPage = currentPage || 1;

                    // default page size is 10
                    // pageSize = pageSize || 10;

                    // calculate total pages
                    // var totalPages = Math.ceil(totalItems / pageSize);

                    var startPage, endPage;
                    if ($scope.numberOfPages <= 3) {
                        // less than 10 total pages so show all
                        startPage = 1;
                        endPage = $scope.numberOfPages;
                    } else {
                        // more than 10 total pages so calculate start and end pages
                        if (currentPage <= 2) {
                            startPage = 1;
                            endPage = 3;//$scope.numberOfPages;;
                        } else if (currentPage + 1 >= $scope.numberOfPages) {
                            startPage = $scope.numberOfPages - 2;
                            endPage = 3;//$scope.numberOfPages;
                        } else {
                            startPage = currentPage - 1;
                            endPage = currentPage + 1;
                        }
                    }

                    // calculate start and end item indexes
                    // var startIndex = (currentPage - 1) * pageSize;
                    // var endIndex = Math.min(startIndex + pageSize - 1, totalItems - 1);

                    // create an array of pages to ng-repeat in the pager control

                    $scope.range = function(start, count) {                        
                        return Array.apply(0, Array(count))
                            .map(function (element, index) { 
                              return index + start;  
                        });
                    };

                    if ($scope.numberOfPages < 3) {
                        endPage = $scope.numberOfPages;
                    } else {
                        endPage = 3;
                    }

                    var pages = $scope.range(startPage, endPage);
          
                    // return object with all pager properties required by the view
                    // console.log("currentPage " + currentPage);
                    return {
                        totalItems: totalItems,
                        currentPage: currentPage,
                        // pageSize: pageSize,
                        // totalPages: totalPages,
                        startPage: startPage,
                        endPage: endPage,
                        // startIndex: startIndex,
                        // endIndex: endIndex,
                        pages: pages
                    };
                }

                $scope.setPage =  function (page) {
                    if (page < 1 || page > $scope.numberOfPages) {
                        return;
                    }   

                    // get pager object from service
                    $scope.pager = $scope.getPager($scope.totalcount, page, $scope.pageSize);

                    // get current page of items
                    // vm.items = vm.dummyItems.slice(vm.pager.startIndex, vm.pager.endIndex + 1);
                }

                $scope.getReviewDetails = function(x) {
                    
                    $scope.grade5 = 0;
                    $scope.grade4 = 0;
                    $scope.grade3 = 0;

                    $scope.grade5prcnt = 0;
                    $scope.grade4prcnt = 0;
                    $scope.grade3prcnt = 0;
                    $scope.gradeprg =[];

                    $scope.totalcount = 0;

                    $scope.reviews = Array();

                    var API_URL = 'api/getwidgetreviews/' + "<?php echo $mrs;?>";
                    
                    $http.get(API_URL).then(function (response) {
                       if(response){
    
                        $scope.avgscore = response.data.data.score;
                        $scope.scoredesc = response.data.data.score_desc;

                        var tmpReview = response.data.data.reviews;
                          

                        for(var i = 0; i < tmpReview.length; i++ ) {
                            if (tmpReview[i].score > parseInt(<?php echo $grade;?>)) {
                                moment.locale('en');
                                tmp = tmpReview[i].user_name;
                                try {
                                    if(typeof tmp === "string" && tmp.length > 0) {
                                        tmp = tmp.trim().replace(/ .*$/g, "").substring(0,7);
                                    }
                                } catch(e) { console.log(e.message); }
                                tmpReview[i].user_name = tmp; 
                                tmpReview[i].time = moment(tmpReview[i].post_date).format('MMM YYYY');
                                $scope.reviews.push(tmpReview[i]);
                            }
                        }
                        $scope.totalcount = $scope.reviews.length;

                        for(var i = 0; i < $scope.reviews.length; i++ ) {
                            if (x == 'All Review') {
                                if($scope.reviews[i].score > 4) {
                                    $scope.grade5 = $scope.grade5 + 1;
                                } else if ($scope.reviews[i].score > 3 && $scope.reviews[i].score <= 4 ) {
                                    $scope.grade4 = $scope.grade4 + 1;
                                } else if ($scope.reviews[i].score > 2 && $scope.reviews[i].score <= 3 ) {
                                    $scope.grade3 = $scope.grade3 + 1;

                                }
                            } 
                        }

                      
                        if($scope.reviews){
                            $scope.numberOfPages = Math.ceil($scope.reviews.length/$scope.pageSize); 
                        }
                        $scope.grade5prcnt = 100 * ($scope.grade5 /  $scope.totalcount);
                        $scope.grade4prcnt = 100 * ($scope.grade4 /  $scope.totalcount)
                        $scope.grade3prcnt = 100 * ($scope.grade3 /  $scope.totalcount)

                        $scope.setPage(1);
                        }
                    });

                };

                $scope.getReviewDetails('All Review');


            });

            app.filter('startFrom', function() {
                return function(input, start) {
                    if(input) {
                        start = +start; //parse to int
                        return input.slice(start);
                    }
                    return [];
                }
            });

        </script>

        </body>
</html>
