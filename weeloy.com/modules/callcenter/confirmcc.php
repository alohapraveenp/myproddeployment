<?php

//print_r($_REQUEST); exit;

require_once("conf/conf.init.inc.php");
require_once("lib/wpdo.inc.php");
require_once('conf/conf.session.inc.php');
require_once("lib/wglobals.inc.php");
require_once("lib/class.images.inc.php");
require_once("lib/class.restaurant.inc.php");
require_once("lib/class.booking.inc.php");
require_once("lib/class.notification.inc.php");
require_once("lib/class.mail.inc.php");
require_once("lib/class.sms.inc.php");
require_once("lib/class.pushnotif.inc.php");
require_once("lib/class.spool.inc.php");
require_once("lib/class.async.inc.php");
require_once("lib/Browser.inc.php");
require_once("lib/class.qrcode.inc.php");
require_once("lib/class.analytics.inc.php");
require_once("lib/class.media.inc.php");
require_once("lib/class.coding.inc.php");
require_once("lib/class.login.inc.php");
require_once("lib/class.transitory.inc.php");
require_once("lib/class.member.inc.php");
require_once("lib/class.allote.inc.php");

	
$arglist = array('bksalutation', 'bklast', 'bkfirst', 'bkemail', 'bkcover', 'bkdate', 'bktime', 'bkmobile', 'bkcountry', 'bkrestaurant', 'bktitle', 'bkparam', 'bkspecialrequest', 'brwsr_type', 'bkpage', 'bktracking', 'data', 'booker', 'company', 'hotelguest', 'dontnotify', 'ccrequired', 'bktms', 'genflag', 'bkerror', 'bkextra', 'bkproduct', 'bknotes');
foreach ($arglist as $label) {
    if (!isset($_REQUEST[$label]))
        $_REQUEST[$label] = "";
	$$label = preg_replace("/\'|\"/", "’", $_REQUEST[$label]);
    $$label = preg_replace("/\s+/", ' ', $$label);
	}

$fullfeature = preg_match("/TheFunKitchen/", $bkrestaurant) || in_array($_SESSION['user']['member_type'], $fullfeature_member_type_allowed) || ($_SESSION['user']['member_type'] == 'weeloy_sales' && $res->status == 'demo_reference' || $res->status == 'active');

if ($fullfeature)
	$fullfeatflag = "'fullfeature=true;'";
else $fullfeatflag = "'fullfeature=false;'";

if(empty($bkrestaurant)) {
	header("location: https://www.weeloy.com");
	exit;
	}
$resto = new WY_restaurant;
$resto->getRestaurant($bkrestaurant);

$mediadata = new WY_Media($bkrestaurant);
$logo = $mediadata->getLogo($bkrestaurant);

if(empty($resto->restaurant)) {
	header("location: https://www.weeloy.com");
	exit;
	}

$action = "callcenter.php"; 
$logger = new WY_log("backoffice");
 $loguserid = (isset($_SESSION['user_backoffice']['id'])) ? $_SESSION['user_backoffice']['id'] : "unknown";
$logger->LogEvent($loguserid, 820, '', $resto->restaurant, '', date("Y-m-d H:i:s"));

$token = tokenize(date("Y-m-d") . ";" . $bkemail);

$bkparamdecoded = detokenize($bkparam);
$originalUrl = $action . "?bkrestaurant=" . $bkrestaurant . "&bktitle=" . $bktitle . "&bktracking=" . $bktracking . "&bkproduct=" . $bkproduct . "&data=" . $data . "&bktms=" . $bktms . "&genflag=" . $genflag . '&langue=' . $bklangue . '&bksalutation=' . $bksalutation . '&bklast=' . $bklast . '&bkfirst=' . $bkfirst . '&bkemail=' . $bkemail . '&bkcover=' . $bkcover . '&bkdate=' . $bkdate . '&bktime=' . $bktime . '&bkmobile=' . $bkmobile;
if(substr($bkparamdecoded, 0, 3) != date('M') || microtime(true) - floatval(substr($bkparamdecoded, 3)) > 600)	
	header("Location: " . $originalUrl . "&timeout=y");

$restaurant_restaurant_tnc = $resto->restaurant_tnc;

$booking = new WY_Booking();
$notif = new WY_Notification();

$transitory = new WY_Transitory;
$transitory->read('BOOKING-' . $bkparam);

if($transitory->result > 0) {
	$prev_confirmation = preg_replace("/BOOKING-/", "", $transitory->content);
	$booking->getBooking($prev_confirmation);
	}
	
$dateAr = explode("/", $bkdate);
$bkDBdate = $dateAr[2] . "-" . $dateAr[1] . "-" . $dateAr[0];  // be carefull msqyl Y-m-d, html d/m/Y  qrc Ymd
$todayflg = ( intval(date("Y")) == intval($dateAr[2]) && intval(date("m")) == intval($dateAr[1]) && intval(date("d")) == intval($dateAr[0]) );

if(empty($booking->restaurant)) {

	$booking->restaurant = $bkrestaurant;
	$language = "en";

	$bkstate = ($bktms == "87") ? "autoattribute" : "to come";
	$optin = '0';

	if(empty($bktracking) && !empty($_COOKIE['weeloy_be_tracking']))
		$bktracking = $_COOKIE['weeloy_be_tracking'];

	if((intval($genflag) & 1) == 1) {
		$bktracking = (empty($bktracking)) ? "walkin" : $bktracking . "|" . "walkin";
		}

	if((intval($genflag) & 2) == 2) {
		$bktracking = (empty($bktracking)) ? "waiting" : $bktracking . "|" . "waiting";
		$bkstate = ""; // no state for waiting
		}

	if($bktms == "87") {
		$bktracking = (empty($bktracking)) ? "tms" : $bktracking . "|" . "tms";
		}
		
	if($dontnotify == "17") 
		$bktracking = (empty($bktracking)) ? "DNB" : $bktracking . "|" . "DNB";

	$bktype = "booking";
	$booking_deposit_id = $bkstatus = "";
        $requiredccDetails = false;
        $waivedccDetails = false;
        $respolicy = new WY_Restaurant_policy;
        // CALLCENTER - kala added this line for payment booking
        if($ccrequired == '1'){
           // if($bkrestaurant == 'SG_SG_R_Bacchanalia'  || $bkrestaurant == 'SG_SG_R_Pollen' || $bkrestaurant == 'SG_SG_R_Nouri' || $bkrestaurant == 'SG_SG_R_Esquina'){
            if(WY_restaurant::s_checkccpayment($bkrestaurant) > 0){
                $requiredccDetails = $respolicy->isRequiredccDetails($bkrestaurant,$bktime,$bkcover,$bkDBdate);
                if($requiredccDetails){$bkstatus = "pending_payment";};
            }
            else{
                if(WY_restaurant::s_checkbkdeposit($bkrestaurant) > 0) {
                   $requiredccDetails = false;
                   // $bkstatus = "pending_payment";
                }
            }
        }else{
           
            if(WY_restaurant::s_checkbkdeposit($bkrestaurant) > 0) {
                $waivedccDetails = true;
            }
        }
        
//        if($ccrequired == '0'){
//             error_log("Ideposit flg " .WY_restaurant::s_checkbkdeposit($bkrestaurant));
//  
//            if(WY_restaurant::s_checkbkdeposit($bkrestaurant) > 0) {
//                   error_log("IN CONDITION");
//                $waivedccDetails = true;
//            }
//            
//        }
//        if($bkrestaurant == 'SG_SG_R_TheFunKitchen' || $bkrestaurant == 'SG_SG_R_Bacchanalia' ){
//            $requiredccDetails = $respolicy->isRequiredccDetails($bkrestaurant,$bktime,$bkcover);
//           
//        }
        
         
	$status = $booking->createBooking($bkrestaurant, $bkDBdate, $bktime, $bkemail, $bkmobile, $bkcover, $bksalutation, $bkfirst, $bklast, $bkcountry, $language, $bkspecialrequest, $bktype, $bktracking, $booker, $company, $hotelguest, $bkstate, $optin, $booking_deposit_id, $bkstatus, $bkextra, $bkproduct);

	if($status < 0)  {
		header("Location: " . $originalUrl . "&bkerror=" . $booking->msg);
		exit;
		}

	$bkconfirmation = $booking->confirmation;
	if ($booking->getBooking($bkconfirmation) == false) {
		header("Location: ./" . $action . "?" . $_SERVER['QUERY_STRING']  . "&bkerror=Unable to create the reservation:" . $booking->msg);
		exit;
		}

	$transitory->create('BOOKING-' . $bkparam , $booking->confirmation);

	if(!empty($bknotes)) {
			$booking->setBooking($bkrestaurant, $bkconfirmation, $bknotes, 'notestext', null);
			}
		
	//qrcode generartion
	$path_tmp = explode(':',get_include_path());
	$qrcode_filename = $path_tmp[count($path_tmp)-1].'tmp/'.$bkconfirmation.'.png';
	$qrcode = new QRcode();
	$qrcode->png('{"membCode":"'.$booking->membCode.'"}', $qrcode_filename);
	//file_put_contents($qrcode_filename,$qrcode->image(8));
 
        // CALLCENTER notification on booking_pending pending payment

	if($dontnotify != "17"){
            if ($requiredccDetails){
                $notif->notify($booking, 'booking_pending');
                $logger->LogEvent($loguserid, 700, $bkconfirmation, 'CALLCENTER', '', date("Y-m-d H:i:s"));
            }else{
                if($waivedccDetails){
                    $booking->notifyflg = 'waived';
                    $booking->addtoGeneric('{"more":"{’ccwaived’:’1’}"}');
                }
		$notif->notify($booking, 'booking');
                $logger->LogEvent($loguserid, 701, $bkconfirmation, 'CALLCENTER', '', date("Y-m-d H:i:s"));
            }
        }
	
	}
else {		// this is a reload
	$bkconfirmation = $booking->confirmation;
	$bktime = $booking->rtime;
	$bkemail = $booking->email; 
	$bkmobile = $booking->mobile;
	$bkcover = $booking->cover;
	$bksalutation = $booking->salutation;
	$bkfirst = $booking->firstname;
	$bklast = $booking->lastname;
	$bkcountry = $booking->country;
	$language = $booking->language;
	$bkspecialrequest = $booking->specialrequest;
	}

//qrcode delete
//unlink($qrcode_filename);

//error_log("<br><br>BOOKING = " . $status . " " . $booking->msg);

$restoSync = ($resto->checkTMSSync() != 0 && $todayflg) ? 1 : 0;

error_log("SYNC " . ($todayflg ? 1 : 0) . " " . ($resto->checkTMSSync() ? 1 : 0));

$QRCodeArg = "&bkrestaurant=$bkrestaurant&bkconfirmation=$bkconfirmation";

$typeBooking =  ($resto->is_bookable && ($resto->status == 'active' || $resto->status == 'demo_reference')) ? "true " : "false ";
$is_listing = false;
if(empty($res->is_wheelable) || !$res->is_wheelable) {
	$is_listing = true;
	}


$signinandreview = (!preg_match('/CALLCENTER|WEBSITE|GRABZ|facebook/', $bktracking)) ? "You can <a href='https://www.weeloy.com' target=_blank>SIGN In</a> using your email to view all your bookings and leave your reviews" : "";

$notescode = "";
if($bkextra !== "") {
	$bkextra_match = array("area", "purpose", "notescode");
	foreach($bkextra_match as $label) {
		if(preg_match("/" . $label . "=([^|]+)/", $bkextra, $match)) {
			$bkspecialrequest .= ((!empty($bkspecialrequest)) ? ", " : "") . $match[1];
			if($label === "notescode")
				$notescode = $match[1];
			}
		}
	}

?>


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html><head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv='expires' content='mon, 1 jan 1998 05:00:00 gmt'>
<meta http-equiv='pragma' content='no-cache'>
<meta http-equiv='pragma' content='cache-control: max-age=0'>
<meta http-equiv='last-modified' content='mon, 1 jan 1998 05:00:00 gmt'>
<meta http-equiv='cache-control' content='no-cache, must-revalidate'>
<meta name='robots' content='noindex, nofollow'>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
<meta name="HandheldFriendly" content="true" />
<title>Weeloy Code</title>
<link href="../../css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="../../css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
<link href="../../css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<link href="bookstyle.css" rel="stylesheet" type="text/css" />
<link href='https://fonts.googleapis.com/css?family=Montserrat|Unkempt|Roboto:400,900,700italic,700,500italic,500,400italic' rel='stylesheet' type='text/css'>
<script type="text/javascript" src="../../js/jquery.min.js"></script>
<script type="text/javascript" src="../../js/bootstrap.min.js"></script>
<script type='text/javascript' src="../../js/angular.min.js"></script>

<?
if($restoSync) {
printf("<script type='text/javascript' src='../rltimeio/ortc.js'></script>");
printf("<script type='text/javascript' src='../rltimeio/lib.js'></script>");
}
?>

<style>
<?php if ($brwsr_type != "mobile") echo ".mainbox { margin: 10px 0 20px 0; width:550px; } "; ?>

</style>

</head>
<body ng-app="myApp" style="margin: 0; padding:5px 0 0 5px;">

	<div id='booking' ng-controller='MainController' ng-cloak >

		<div class="container mainbox">    
			<div class="panel panel-info" style="height:470px;">
				<div class="panel-heading">
					<div class="panel-title" style='width:300px;'> {{ bookingTitle }} </div>
				</div>     

				<p style="font-family: Roboto; padding-top:0px;margin:0 20px 0 20px" ><span style="font-size: 12px;">
					<strong>{{confirmationMessage2}}</strong><br/>{{confirmationMessage3}}</span></p>
				
				<div style="padding-top:10px" class="panel-body" >

						<div class='col-xs-5 separation' style="height:360px;">
						<center><img ng-src='{{imglogo}}' max-width='120' max-height='120' style="width:150px;"></center><br>
							<table width='90%'>
								<tr ng-repeat="x in glyphtag"><td><p class='glyphicon glyphicon-ok checkmark'></p><br/>&nbsp;</td><td class='pcheckmark'> {{x}} <br/>&nbsp;</td></tr>
							</table>
						<div class='row' ng-if="genflag === 1">&nbsp; walk-in guest</div>
						<div class="input-group" ng-if="tmsflg === 0">
							<a style='width:100px;color:white' href ng-click='newbooking();' class="btn btn-primary"> MORE </a>
						</div>
						<br /><br /><br />
						</div>
						<div class='col-xs-7' style="font-size:12px;height:18px">
						<div class="alert alert-info alert-label" style="padding-top:5px;padding-bottom:5px"> <strong>BOOKING DETAILS</strong></div>
						<div class='row'>&nbsp; Restaurant:  {{ btitle }}</div>
						<div class='row'>&nbsp; Date:    {{ bdate }}</div>
						<div class='row'>&nbsp; Time:   {{ btime }}</div>
						<div class='row'>&nbsp; Number of guests:   {{ bcover }}</div>
						<div class="alert alert-info alert-label" style="padding-top:5px;padding-bottom:5px"> <strong>PERSONAL DETAILS</strong></div>
						<div class='row'>&nbsp; Name:   {{ bsalutation }} {{ bfirst }} {{ blast }}</div>
						<div class='row'>&nbsp; Email:   {{ bemail }}</div>
						<div class='row'>&nbsp; Mobile:   {{ bmobile }}</div>
						<div class='row' ng-if="bspecialrequest != ''">&nbsp; Request:   {{ bspecialrequest }}</div>
						<div class='row' ng-if="bnotes != ''">&nbsp; internal notes:   {{ bnotes }}</div>
						<div class='row' style='margin-left:5px'>
							<table><tr>
									<td><a href='javascript:openWin(1);'><br/>QRCode Agenda</a></td>
									<td width='50'>&nbsp;</td>
									<td><a href='javascript:openWin(0);'><br/>QRCode Contact</a></td>
							</tr></table><br />
						</div>
						</div>
				</div>
			</div>                     
		</div>  
	</div>
</div>

<script>

<?php

echo "var confirmation = '" . $booking->confirmation . "';";
echo "var tracking = '" . $booking->tracking . "';";
echo "var hoteltitle = '" . $booking->restaurantinfo->title . "';";
echo "var typeBooking = $typeBooking;"; 

echo "var imglogo = '$logo';";
echo "var typeBooking = $typeBooking;";
echo "var restaurant = '$bkrestaurant';";
echo "var action = '$action';";
echo "var btitle = '$bktitle';";
echo "var bdate = '$bkdate';";
echo "var btime = '$bktime';";
echo "var bcover = '$bkcover';";
echo "var bsalutation = '$bksalutation';";
echo "var bfirst = '$bkfirst';";
echo "var blast = '$bklast';";
echo "var bemail = '$bkemail';";
echo "var bmobile = '$bkmobile';";
echo "var bspecialrequest = '$bkspecialrequest';";
echo "var bnotes = '$bknotes';";
echo "var bkextra = '$bkextra';";
echo "var errmsg = '';";
echo "var logo = '$logo';";
echo "var restoSync = '" . $restoSync . "';";
echo "var token = '" . $token . "';";
echo "var notescode = '" . $notescode . "';";
echo "var tracking = '" . $bktracking . "';";
printf("var is_listing = %s;", $is_listing ? 'true' : 'false');
echo "var prevurl = '$originalUrl';";
?>

var app = angular.module("myApp", []);

app.controller('MainController', function ($scope, $http, $location) {

	restoSync = parseInt(restoSync);
	$scope.btitle = btitle;
	$scope.bdate = bdate;
	$scope.btime = btime;
	$scope.bcover = bcover;
	$scope.bsalutation = bsalutation;
	$scope.bfirst = bfirst;
	$scope.blast = blast;
	$scope.bemail = bemail;
	$scope.bmobile = bmobile;
	$scope.bspecialrequest = bspecialrequest;
	$scope.bnotes = bnotes;
	$scope.errmsg = errmsg;
	$scope.imglogo = imglogo;
	$scope.genflag = <? echo intval($genflag); ?>;
	$scope.tmsflg = <? echo intval($bktms); ?>;
	$scope.bkextra = "";

	if(typeof bspecialrequest === "string" && bspecialrequest.length > 0)
		bspecialrequest = bspecialrequest.replace(/\'|\"/g, "’");

	$scope.bookingTitle = (typeBooking) ? "BOOKING CONFIRMED" : "REQUESTED CONFIRMED";
	$scope.confirmationMessage = (typeBooking) ? "Your booking is confirmed at " : "Your request has been sent to ";
	$scope.confirmationMessage += hoteltitle;        
	$scope.confirmationMessage2 = (typeBooking) ? "We are happy to confirm your reservation " : "Your reservation number is ";
	$scope.confirmationMessage2 += confirmation;        
	$scope.confirmationMessage3 = "You will receive the details of your reservation by email and SMS";    
	$scope.listTags = (typeBooking) ? "Instant Reservation" : "No reservartion";
	$scope.listTags1 = "free booking";
	$scope.listTags2 = (typeBooking) ? "" : "First come, first serve";
	$scope.listTags3 = (!is_listing) ? "Spin the Wheel" : "Enjoy incredible";
	$scope.listTags4 = (!is_listing) ? "at the restaurant" : " promotion";

	$scope.glyphtag = [ $scope.listTags1, $scope.listTags + ' ' + $scope.listTags2, $scope.listTags3 + ' ' + $scope.listTags4 ];
	$scope.glyphtag.pop(); // don't show the 'spin the weel'

	if(restoSync === 1) {
		bdate = bdate.split("/").reverse().join("-");
		var msg = "reservation;" + confirmation + ";date=" + bdate + "|time=" + btime + "|cover=" + bcover + "|last=" + blast + "|first=" + bfirst + "|email=" + bemail + "|phone=" + bmobile + "|comment=" + bspecialrequest + "|notescode=" + notescode + "|tracking=" + tracking;
		setTimeout(function() { var sync = subrealtimeio(restaurant, token, msg); }, 100);
		}

	$scope.newbooking = function() {
		var newurl  = "";
		prevurl.split("&").map(function(ll, index) {  
			var tt; 
			if(index === 0) newurl += ll; 
			else {
				tt = ll.split("=");
				if(["bksalutation", "bklast", "bkfirst", "bkemail", "bkcover", "bkdate", "bktime", "bkmobile"].indexOf(tt[0]) < 0)
					newurl += '&' + ll; 
				}
			});
		window.location.assign(newurl);
		};
});

	
function openWin(type) {
	var arg = <?php echo "'$QRCodeArg'"; ?> ;
			window.open('../qrcode/newQRCode.php?type=' + type + arg, 'QRCode', 'toolbar=no, scrollbars=no, resizable=no, top=30, left=30, width=330, height=330');
}

$(document).ready(function () {

});

</script>
</body>
</html>

