<?php
require_once 'conf/conf.init.inc.php';
require_once 'conf/conf.mysql.inc.php';
require_once("lib/wpdo.inc.php");
require_once 'lib/class.payment_stripe.inc.php';
require_once 'lib/class.restaurant.inc.php';
require_once("lib/class.analytics.inc.php");
require_once("lib/class.booking.inc.php");
require_once('conf/conf.session.inc.php');

        $booking = new WY_Booking();
        $res = new WY_restaurant;
        $stripe = new WY_Payment_Stripe();

        $logger = new WY_log("website");
        $loguserid = (isset($_SESSION['user']['user_id'])) ? $_SESSION['user']['user_id'] : "0";

        $amount = $_POST['amount'];
        $ref_id = $_POST['confirmation'];
        $paymentType = $_POST['paymentType'];
        $token = $_POST['stripeToken'];
        
        $booking->getBooking($ref_id);
        $restaurant = $booking->restaurant;
        $email = $booking->email;
        $res->getRestaurant($restaurant);
        if($restaurant == 'SG_SG_R_Esquina' ){
            $paymentType = 'deposit';
        }
        if ($booking->status == 'pending_payment' && empty($booking->deposit_id)) {
            $state = $booking->status;
        } else {
            if (!empty($booking->deposit_id) && $booking->status == '') {
                $state = 'confirmed';
                header("Location: " . __BASE_URL__ . "/modules/booking/deposit/deposit_confirmation_confirmed.php?bkconfirmation=".$ref_id);
                //      echo "<script>$('#booking_form').submit();</script>";
            }
        }
        
        $credentials = $res->getRestaurantPaymentId($restaurant);
        
        $logger->LogEvent($loguserid, 805, $res->ID, $booking->bookid,'', date("Y-m-d H:i:s"));
      
        if($paymentType === 'carddetails'){ 
           $returnObj = $stripe->createBookingPaymentDetails($restaurant,$ref_id,$email,$amount,$token); 
           $logger->LogEvent($loguserid, 705, $ref_id, 'website', '', date("Y-m-d H:i:s"));
        }else{
             $returnObj = $stripe->stripeBookingDeposit($restaurant,$ref_id,$email,$amount,$token);
             $logger->LogEvent($loguserid, 705, $ref_id, 'website', '', date("Y-m-d H:i:s"));
        }
        if(isset($returnObj)){
           $state = $returnObj['status'] == 'COMPLETED' ? 'confirmed'  : 'pending';
           if($state == 'pending'){
               $url =  __ROOTDIR__ ."/modules/booking/deposit/deposit_payment_method.php?action=bktrack_pendingpayment&bkemail=".$email.'&refid='.$ref_id;
           }else{
               $url =  __ROOTDIR__ ."/modules/booking/deposit/deposit_confirmation_confirmed.php?bkconfirmation=".$ref_id;
           }
           
           header("Location: ".$url);
        }
        
 
//    if (isset($_POST['stripeToken'])) {
//
//
//        $email = $_POST['email'];
//        $amount = $_POST['amount'];
//        $restaurant = $_POST['restaurant'];
//        $confirmation = $_POST['ref_id'];
//        $paymentType= $_POST['payment_type'];
//        $stripe = new WY_Payment_Stripe();
//        $cardnumber = trim($_POST['cardnumber']);
//        $token =$_POST['stripeToken'];
//       
//        if($paymentType==='carddetails'){ 
//           $returnObj = $stripe->createBookingPaymentDetails($restaurant,$confirmation,$email,$amount,$token); 
//        }else{
//             $returnObj = $stripe->stripeBookingDeposit($restaurant,$confirmation,$email,$amount,$token);
//        }
//        if(isset($returnObj)){
//           $state = $returnObj['status'] == 'COMPLETED' ? 'confirmed'  : 'pending';
//           if($state == 'pending'){
//                $page =1;
//                if(isset($_COOKIE['paymentpage'.$confirmation])){
//
//                  $page= $_COOKIE['paymentpage'.$confirmation];
//                  $page = $page+1;
//                }
//                setcookie("paymentpage".$confirmation,$page, time() + (60 * 20), "/");
//           
//                    if($page < 6){
//                        $action ='/modules/booking/deposit/deposit_payment_method.php?action=bktrack_pendingpayment&bkemail='.$booking->email.'&refid='.$booking->confirmation ;
//                     }else{
//                         $action = "/modules/booking/book_form_section.php";
//                     }
//           
//                      header("Location: ".$action);
////                setcookie("page".$confirmation,$page, time() + (60 * 20), "/");
//          
//           }else{
//                header("Location: stripe_valid_credit_card.php?state=".$state."&payment_type=".$paymentType);
//             }
//        }
//    
//    
//    }

?>
