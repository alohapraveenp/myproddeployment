<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

    require_once 'lib/class.payment_paypal.inc.php';
    require_once 'lib/class.payment.inc.php';
    
    if(empty($_REQUEST["refid"])) {
	printf("Invalid Payment");
	exit;	
    }


    $payment = new WY_Payment();
    $response = $payment->getPaymentDetails($_REQUEST["refid"],'object_id');
    if(isset($response)){
        $pay_object = new WY_Payment_Paypal($response['restaurant'], $response['amount']);
        $paymentStatus = $pay_object->getPaymentResponse($response['payment_id']);
        if($paymentStatus == 'COMPLETED'){
           $payment->updatePaypalStatus($paymentStatus,$_REQUEST["refid"]); 
        }
        
        header("Location: ../../backoffice/index.php");
    }

?>