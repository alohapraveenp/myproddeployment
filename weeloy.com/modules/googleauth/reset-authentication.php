<?php
if (empty($_REQUEST["email"])) {
    printf("Invalid authentication. Exiting");
    exit;
}
$email = $_REQUEST["email"];

?>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="author" content="weeloy. https://www.weeloy.com"/>
        <meta name="copyright" content="weeloy. https://www.weeloy.com"/>  
        <meta http-equiv='expires' content='mon, 1 jan 1998 05:00:00 gmt'/>
        <meta http-equiv='pragma' content='no-cache'/>
        <meta http-equiv='pragma' content='cache-control: max-age=0'/>
        <meta http-equiv='last-modified' content='mon, 1 jan 1998 05:00:00 gmt'/>
        <meta http-equiv='cache-control' content='no-cache, must-revalidate'/>
        <meta name='robots' content='noindex, nofollow'/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
        <meta name="HandheldFriendly" content="true" />
        <title> Reset Two Fact Authentication</title>



        <link href="../../client/bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="../../client/bower_components/bootstrap/dist/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
        <link href="../../client/bower_components/fontawesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />

        <link href="../../modules/booking/assets/css/css/bootstrap-select.css" rel="stylesheet" />
        <link href="../../modules/booking/assets/css/css/bootstrap-social.css" rel="stylesheet" />

        <script type='text/javascript' src="../../client/bower_components/angular/angular.min.js"></script>
        <script type="text/javascript" src="../../client/bower_components/jquery/dist/jquery.min.js"></script>

        <script type="text/javascript" src="../../js/jquery-ui.js"></script>
        <script type="text/javascript" src="../../client/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="../../js/bootstrap-select.js"></script>
        <script type='text/javascript' src="../../js/ui-bootstrap-tpls-0.12.1.min.js"></script>
        <script type="text/javascript" src="../../js/ngStorage.min.js"></script>
        <script type="text/javascript" src="../../js/mylocal.js"></script>

        <script> var app = angular.module("myApp", ['ui.bootstrap', 'ngLocale', 'ngStorage']);</script> 
        <style>
            html, body, .container-table {
                height: 100%;
            }
            .container-table {
                display: table;
            }
            .center {
                margin-left: 40%;
                padding: 10px;
            } 
            
        </style>
        <body ng-app="myApp">
            <div class="wrapper" ng-controller='ResetAuthController' ng-init="moduleName = 'resetauth';" style='margin-top:20px;'>
                <div class="container container-table"  >
                    <div class="row ">
                            <div class="container">
                                <div class="col-lg-offset-2 col-md-offset-2 col-sm-offset-2 col-lg-8 col-md-8 col-sm-8 col-xs-12 my-booking"> 
                                    <div id='msg-alert' class="alert" style="display:none;">
                                    </div>
                                    <div id='review-container' class ='panel-body' style='display:block; margin-left: 99px;padding-left:20px;'>
<!--                                        <div class="form-group" ng-if="success_update_bo !='backoffice'">
                                            <label>Please enter Email</label>
                                            <input type="text" class="form-control" name="email" ng-model="email" required style='width:250px;'/>
                                        </div>
                                        <div id="targetSectionRev" style='margin-left: 50px;' ng-if="success_update_bo !='backoffice'" >
                                            <div  style="margin-top:10px;margin-left:10px;padding-left:20px;">
                                                <input type="button" ng-click="ResetNow()" class="btn btn_default btn_submit_review" value="Submit" style="border-radius:0px;" >
                                            </div>
                                        </div>-->
                                         <div ng-if="success_update_bo =='backoffice'" id="success-div-backoffice"><img width="50" ng-src="../../client/assets/images/loading_spinner.gif">Your Authentication has been updated, redirecting to <a href="https://www.weeloy.com/backoffice">backoffice</a></div>
                                    </div>
                                        <br />
                                        <br />
                                        <br />
                                        <br />
                                        <br />
                                        <br />
                                        <br />
                                    </div>


<!--             <review-section  id="targetSectionRev" class="currentpanel" rdata-review="reviewList" ></review-section>-->

                                </div>
                                <div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">
                                    <p></p>
                                </div>
                            </div>

                       
                    </div>
                </div>
            </div>

            <script>
                        app.controller('ResetAuthController', function ($scope, $http) {
                            $scope.email = <?php echo "'" . $email . "';"; ?>;
                            $scope.ResetNow = function () {
                                var apiurl = "../../api//login/resetgooleauth/" + $scope.email;
                                $http.post(apiurl,
                                        {
                                          'email':$scope.email,
                                        }).then(function (response) {
                                            $scope.success_update_bo = 'backoffice';
                                             setTimeout(function () { window.location.href ='https://www.weeloy.com/backoffice' }, 1000);
                                       });
 
                            };
                            $scope.ResetNow();
    
                        });
                      
            </script>


        </body>
</html>

