<!DOCTYPE html>
<?php
require_once("conf/conf.init.inc.php");
require_once("lib/wpdo.inc.php");
require_once("lib/wglobals.inc.php");
//error_reporting(E_ALL);
//ini_set("display_errors", '1');

header('Location:https://www.weeloy.com/event/singapore/food-selfie-contest-2015/');

$id = @$_REQUEST['picture'];
if( $id == 24 ){$id = 20;}
$sql = "SELECT * FROM _2015_photo_contest WHERE id = '$id' ";
$row = pdo_single_select($sql);
$siteUrl = __SERVERNAME__ . __ROOTDIR__ . "/event/singapore/food-selfie-contest-2015/";
$fbId = WEBSITE_FB_APP_ID;
$mediaUrl = __S3HOST__;
?>

<html>
    <head>
        <meta property="og:type" content="website" />
        <meta property="og:title" content="Weeloy Food-Selfie Contest" />
        <meta property="og:description" content="Dine around Singapore  & Share your Food Selfie Be a food explorer, Challenge youself and your buddies to dine at various restaurants listed on Weeloy.com! It’s easy to snap, share and win!Winning your dream holiday is as easy as 1, 2, 3!" />
        <meta property="og:image:width" content="450"/>
        <meta property="og:image:height" content="298"/>
        <meta name="og:image" content="<?php echo $mediaUrl ?><?php echo $row['uploadurl']; ?>"/>
        <meta property="og:url" content="<?php echo $siteUrl ?>viewpicture/?picture=<?php echo $id ?>" />
        <title>Weeloy</title>
        <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="css/bootstrap-theme.min.css">
        <link rel="stylesheet" type="text/css" href="css/style-new1.css">
        <link rel="stylesheet" type="text/css" href="../../../css/styles_front.css">
        <link rel="stylesheet" type="text/css" href="../../../css/new_layout.css">

        <script type="text/javascript" src="js/jquery-1.9.1.min.js" ></script>
        <link rel="stylesheet" href="css/popupwindow.css">
        <link href='http://fonts.googleapis.com/css?family=Lora:400,700' rel='stylesheet' type='text/css'>
        <script src="js/popupwindow.js"></script>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <meta name="viewport" content="width=device-width, initial-scale=1">
        <script>
            function validateEmail(email) {
                var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                return re.test(email);
            }
            function verifyForm() {

                var res = true;
                if (!validateEmail($('#email').val()))
                {
                    $('#div-email').attr("class", "input-group has-error");
                    res = false;
                }
                else
                    $('#div-email').removeClass("has-error");
                if ($('#confirmation').val().length < 3)
                {

                    $('#div-confirmation').attr("class", "input-group has-error");
                    res = false;
                }
                else
                    $('#div-confirmation').removeClass("has-error");
                return res;
            }
            function addFiles(evt) {
                var res = verifyForm();
                if (res === false) {
                    //$("html, body").animate({scrollTop: "0px"});
                    return false;
                } else {


                    var formData = new FormData($(this).parents('form')[0]);
                    $.ajax({
                        url: 'upload.php',
                        type: 'POST',
                        xhr: function () {
                            var myXhr = $.ajaxSettings.xhr();
                            return myXhr;
                        },
                        beforeSend: function (xhr) {

                            //$(placeholder).addClass('loading');
                            $("#ajaxloader").show();
                            $("#errorAjax").html('');
                        },
                        success: function (data) {

                            var data = JSON.parse(data);
                            if (data.status == 'success') {
                                document.getElementById('pageNo').value = 1;
                                $("#upload-container").css('display', 'none');
                                $("#allGalleries").html(data.html);
                                $('#allGalleries ul:lt(' + x + ')').fadeIn('slow');
                                $(".form-control").val('');
                                $('html, body').animate({
                                    scrollTop: $("#allGalleries").offset().top
                                }, 2000);
                                $(".fileUpload").show();
                                $("#loadMoreCenter").show();
                                FB.XFBML.parse();
                            } else {

                                $("#errorAjax").html(data.message);
                            }
                            //window.location.reload();


                        },
                        complete: function () {
                            $("#ajaxloader").hide();
                        },
                        data: formData,
                        cache: false,
                        contentType: false,
                        processData: false
                    });
                }
                return false;
            }
            ;
            jQuery(document).ready(function ($) {
                var fileInput = $('#upload');
                fileInput.on('click', addFiles);
                size_li = $("#allGalleries ul").size();
                x = 1;
                $('#allGalleries ul:lt(' + x + ')').fadeIn('slow');
                $('#loadMore').click(function () {
                    x = (x + 1 <= size_li) ? x + 1 : size_li;
                    $('#allGalleries ul:lt(' + x + ')').fadeIn('slow');
                    if (x == size_li)
                        $("#loadMoreCenter").hide();
                });
//                $('#myfile').change(function (e) {
//                    $("#uploadForm").submit();
//                });

                $(".fileUpload").click(function () {

                    $("#upload-container").css('display', 'block');
                    $(this).hide();
                });

                window.fbAsyncInit = function () {
                    FB.init({
                        appId: "<?php echo $fbId ?>", //weeloy localhost fb appId
                        status: true,
                        version: 'v2.2',
                        cookie: true,
                        xfbml: true
                    });
                    FB.Event.subscribe('edge.create', function (response) {
                        console.log(response);

                    });
                }; //end fbAsyncInit

                // Load the SDK Asynchronously
                (function (d, s, id) {
                    var js, fjs = d.getElementsByTagName(s)[0];
                    if (d.getElementById(id))
                        return;
                    js = d.createElement(s);
                    js.id = id;
                    js.src = "//connect.facebook.net/en_US/sdk.js";
                    fjs.parentNode.insertBefore(js, fjs);
                }(document, 'script', 'facebook-jssdk'));

            });
            function fbshareglobal(obj) {
                FB.ui(obj, function (response) {
                    //                $("#feedform_user_message").html('2323232');
                    console.log(JSON.stringify(response));
                });
            }

            function fbshare(ele) {
                //link = "<?php echo $siteUrl ?>" +"viewpicture.php&picture=" +ele;
                var id = ele,
                        link = $("#pic_" + id).attr('for'),
                        description = " Hey Friends,\n Help me win a Holiday trip for 2 to Phuket \n Vote for me now @ Weeloy Food-Selfie Contest!",
                        caption = "Weeloy Food-Selfie Contest";
                var obj = {
                    method: 'feed',
                    link: link,
                    picture: $("#pic_" + id).attr('src'),
                    name: caption,
                    caption: '#foodselfiecontest2015,#weeloy',
                    description: description,
                    display: 'popup'
                }
                console.log(JSON.stringify(obj));
                fbshareglobal(obj);

                return false;
            }
            function fbshareFriends() {

                var obj = {
                    method: 'share',
                    href: '<?php echo $siteUrl ?>viewpicture.php?picture=<?php echo $id ?>',
//                    link: "<?php echo $siteUrl ?>",
//                    picture: "http://localhost:8888/weeloy.com/modules/photo_contest/images/background.jpg",
//                    name: "Weeloy Food-Selfie Contest",
//                    caption: "Weeloy Food-Selfie Contest",
//                    description:"Dine around Singapore  & Share your Food Selfie Be a food explorer, Challenge youself and your buddies to dine at various restaurants listed on Weeloy.com! It’s easy to snap, share and win!Winning your dream holiday is as easy as 1, 2, 3!" ,
                                display: 'popup'
                            }
                            console.log(JSON.stringify(obj));
                            fbshareglobal(obj);

                            return false;
                        }
                        function openPopup(id) {
                            $('#pop-up-' + id).popUpWindow({action: "open", size: "uploads-popup medium"});
                        }
        </script>
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
        <div id="fb-root"></div>

        <header>
            <div class="navbar-fixed-top navbar-fixed-top-index " >
                <div class="logo-container--fix" style="width: 25%;display: inline-block;  vertical-align: middle;">
                    <h1 class="logo" style=" font-size: 25px;margin: 5px; padding: 0;">
                        <a href="../../../index.php" target="_blank" > <img src="../../../images/logo_w_t_small.png" alt=""></a></h1>
                </div>
            </div>
            <div class=" container-fluid header-back">
                <div class="container">

                </div>
            </div>
        </header>
        <section class="heading">
            <div class=" container-fluid">
                <div class="container">
                </div>
            </div>
        </section>
        <section class="main-content">
            <div class="container">
                <div class="row box-padding">
                    <h1>Winning your dream holidays is as easy as 1, 2, 3!</h1>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <div class="row text-center" style="margin-bottom: 20px;">
                            <div class="image-red "><img src="https://static4.weeloy.com/images/food-selfie-content-2015/step1-2.png" width="120" height="119" class="image-red-img"></div>
                            <div class="content-box col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                <h2>Book with Weeloy.com</h2>
                                <p>Pick a restaurant and book a date.</p>
                            </div>
                        </div>

                        <div class="row text-center"  style="margin-bottom: 20px;">
                            <div class="image-red"><img src="https://static3.weeloy.com/images/food-selfie-content-2015/step2-2.png" width="120" height="119" class="image-red-img"></div>
                            <div class="content-box col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                <h2>Share your memory</h2>
                                <p>Submit your favourite food selfie from the restaurant with your best memories.</p>
                            </div>
                        </div>

                        <div class="row text-center" style="margin-bottom: 20px;">
                            <div class="image-red"><img src="https://static2.weeloy.com/images/food-selfie-content-2015/step3-2.png" width="120" height="119" class="image-red-img"></div>
                            <div class="content-box col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                <h2>Get the most votes & win prizes</h2>
                                <p>The highest vote wins! Share your entry and get as many votes as possible.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 border-image div-class"><img src="https://static.weeloy.com/images/food-selfie-content-2015/subimage_light.jpg" class="img-responsive lagrge-img img-thumbnail"></div>
                </div>
            </div>
        </section>

        <section class="prizes">
            <div class=" container-fluid">
                <div class="container">
                    <div class="row">
                        <h1>Prizes</h1>
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 prizess-head">
                            <h1>Top Prize</h1>
                            <li class="list-image-wrapper"><img src="https://static.weeloy.com/images/food-selfie-content-2015/samples/top-prize.jpg" width="100%" height="auto"></li>
                            <div class="prize-content">
                                <li>The entry with the highest votes win a 3 Days 2 Nights Phuket trip with return airfare & Movenpick Resort stay.</li>


                                <div class="prize-box"><h1>About Movenpick Resort & Spa</h1>
                                    <p>Situated at the heart of Karon Beach, Phuket. Movenpick Phuket Resort and & Spa offers stunning view of the Karon Beach </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 prizess-head">
                            <h1 class="running-up">Runner Up</h1>
                            <li class="list-image-wrapper"><img src="https://static.weeloy.com/images/food-selfie-content-2015/samples/runnerup_light.jpg" width="100%" height="auto"></li>
                            <div class="prize-content">
                                <li>The 2nd highest voted entry win a Dinner for 2 at Shutters (Amara Sanctuary Resort Sentosa)</li>

                                <div class="prize-box"><h1>About Shutters</h1>
                                    <p>Shutters is a modern restaurant located at the Amara Sanctuary Resort Sentosa serving lunch and dinner International buffets complemented by an all-day dining menu in between. </p>
                                </div>


                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 prizess-head">
                            <h1 class="prrticipation">Participation Prizes</h1>
                            <li class="list-image-wrapper"><img src="https://static.weeloy.com/images/food-selfie-content-2015/samples/participation.jpg" width="100%" height="auto"></li>
                            <div class="prize-content">
                                <li>$25 Dining Vouchers for Top 20 participants from our list of restaurant partners </li>
                                <div class="prize-box">  <h1>AN EXCITING DINING EXPERIENCE AWAITS YOU</h1>
                                    <p style='font-size:13px'>Absinthe &#183; Artistry  &#183;  Cato &#183; Drury Lane Cafe &#183;  Da Paolo  &#183; Forlino &#183; Giardino Italian Pizzerie & Bar &#183; Giardino Pizza Bar & Grill &#183; Koskos &#183; L'Entrecote &#183; Match &#183; Pluck &#183; Sabio By The Sea &#183; Scrumptious At The Turf  &#183; Shin Minori Japanese &#183; SPRMRKT &#183; The Latin Quarter &#183; The Steakhouse &#183; Xin Yue</p>
<!--                                        <p>The Steakhouse &#183; Giardino Italian Pizzeria & Bar &#183;  Giardino Pizza Bar & Grill &#183; Pluck &#183; Shin Minori Japanese &#183; Absinthe &#183; Artistry &#183; Koskos &#183; Match &#183; Sear &#183; SPRMRKT &#183; Forlino &#183; Sabio By the Sea &#183; Pisco &#183; Bumbo Rum Club &#183; L'Entrecote
                                    </p>-->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="photocallery">
            <div class=" container-fluid">
                <div class="container">
                    <div class="row">
                        <h1>Photo Gallery</h1>
                        <div id="allGalleries" style="text-align:center;">
                            <?php
                            $rowcount = count($row);
                            if ($rowcount > 0) {
                                echo '<ul>';
                                //while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
                                ?>
                                <?php $uploadUrl = $mediaUrl . $row['uploadurl']; ?>
                                <li class="col-lg-6 col-lg-offset-3 text-center photocallery-head"> 
                                    <div class="gallary-header "  >
                                        <span style="margin:10px;"><?php echo $row['displayname'] ?> @ <?php echo $row['restaurant_name'] ?></span>
                                    </div> 
                                    <div class="list-image-wrapper"><img src="<?php echo $uploadUrl; ?>" ></div> 
                                    <h1 onClick="javascript:openPopup('<?php echo $row['id']; ?>');">Vote Now</h1>
                                    <div id="pop-up-<?php echo $row['id']; ?>" class="pop-up-display-content">
                                        <div class="likeImg">
                                            <img id="pic_<?php echo $row['id']; ?>"src="<?php echo $uploadUrl; ?>" for ="<?php echo $siteUrl ?>viewpicture.php?picture=<?php echo $row['id'] ?>"  width="100%" height="auto">
                                            <div class="fblikemiddle"> 
                                                <div style="padding-bottom:10px;"><b><?php echo $row['displayname'] ?> @  <?php echo $row['restaurant_name'] ?> + <?php echo $row['wheelwin'] ?> </b></div>
                                                <div class="fb-like" data-href="<?php echo $siteUrl ?>viewpicture.php/?picture=<?php echo $row['id'] ?>" data-layout="button_count" data-action="like" data-show-faces="false" data-share="false"></div>

                                                <img class ="fb-share" id="img-<?php echo $row['id']; ?>" for="<?php echo $siteUrl ?>viewpicture.php?picture=<?php echo $row['id'] ?>" src="images/facebook_share1.png" height="24px" width="70px;" onclick="fbshare(<?php echo $row['id'] ?>);" />

                                            </div>
                                        </div>
                                    </div>
                                </li>

                                <?php
                                echo '<li class="clearfix"></li></ul><ul>';
                            }
                            echo '<li class="clearfix"></li></ul>';
//                        } else {
//                            $url = "http://localhost:8888/weeloy.com/modules/photo-contest/";
//                        }
                            ?>


                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </section>
        <section class="buttons">
            <div class=" container-fluid">
                <div class="container">
                    <div class="row">
                        <center>
                            <h1>Want to find out more?</h1>
                        </center>

                        <center id="loadMoreCenter" style="padding-bottom:15px;">

                            <a href="index.php" ><input type="button" name="input" id="loadMores" class="view-more btn btn-responsive" value="View More Entries"></a>
                        </center>

                            <center>
                                <a href="tc.php"> <input type="button" name="input" id="inputid" class="faq btn btn-responsive" value="Contest Terms & Conditions"></a>
                            </center>
                            <center>
                                <a href="https://www.weeloy.com/how-it-works" target="_blank" ><input type="button" name="input" id="inputid" class="learn btn btn-responsive" value="Learn About Weeloy"></a>
                            </center>


                    </div>
                </div></div>

        </section>

        <footer class="pre-footer">
            <div class=" container-fluid">
                <div class="container" style='padding-right: 0px; padding-left: 0px;'>
                    <div class="row text-center">
                        <h1>In Partnership with:</h1>
                        <li class="col-lg-12 col-md-12 col-sm-12 col-xs-12  text-center" style="margin-bottom:15px;">
                            <!--<a href='../../../restaurant/thailand/phuket/mint' alt='mint movenpick restaurant on weeloy' target="_blank">-->
                            <img src="https://static.weeloy.com/images/food-selfie-content-2015/partnerlogo/movenpick1.jpg"  class="pre-footer-images">
                            <!--</a>-->
                            <a href='../../../restaurant/singapore/shutters?utm_source=food-selfie-contest-2015&utm_medium=SG_SG_R_ShinMinoriJapaneseRestaurant&utm_campaign=fsc2015_0001bu' alt='shutters restaurant on weeloy' target="_blank">
                                <img src="https://static.weeloy.com/images/food-selfie-content-2015/partnerlogo/amara_singapore.jpg"  class="pre-footer-images">
                            </a>    

                            <a href='../../../restaurant/singapore/absinthe?utm_source=food-selfie-contest-2015&utm_medium=SG_SG_R_AbsintheRestaurant&utm_campaign=fsc2015_0001a' alt='absinthe restaurant on weeloy' target="_blank"><img src="https://static.weeloy.com/images/food-selfie-content-2015/partnerlogo/absinthe_logo.png"  class="pre-footer-images"></a>

                            <a href='../../../restaurant/singapore/shin-minori-japanese-restaurant?utm_source=food-selfie-contest-2015&utm_medium=SG_SG_R_ShinMinoriJapaneseRestaurant&utm_campaign=fsc2015_0001b' alt='shin minori japanese restaurant on weeloy' target="_blank"><img src="https://static.weeloy.com/images/food-selfie-content-2015/partnerlogo/shin_minori.png"  class="pre-footer-images"></a>
                            <a href='../../../restaurant/singapore/bumbo-rum-club?utm_source=food-selfie-contest-2015&utm_medium=SG_SG_R_BomboRumClubRestaurant&utm_campaign=fsc2015_0001c' alt='bumbo-rum-club on weeloy' target="_blank"><img src="https://static.weeloy.com/images/food-selfie-content-2015/partnerlogo/rum-club.png"  class="pre-footer-images"></a>
                            <a href='../../../restaurant/singapore/drury-lane?utm_source=food-selfie-contest-2015&utm_medium=SG_SG_R_DruryLaneRestaurant&utm_campaign=fsc2015_0001d' alt='drury-lane on weeloy' target="_blank"><img src="https://static.weeloy.com/images/food-selfie-content-2015/partnerlogo/drury_lane.jpg"  class="pre-footer-images"></a>
                            <a href='../../../restaurant/singapore/giardino-pizza-bar-grill?utm_source=food-selfie-contest-2015&utm_medium=SG_SG_R_GiardinoPizzaBarGrillRestaurant&utm_campaign=fsc2015_0001e' alt='giardino pizza bar grill restaurant on weeloy' target="_blank"><img src="https://static.weeloy.com/images/food-selfie-content-2015/partnerlogo/giardino_pizza_logo.png"  class="pre-footer-images"></a>
                            <a href='../../../restaurant/singapore/artistry-cafe?utm_source=food-selfie-contest-2015&utm_medium=SG_SG_R_ArtistryCafeRestaurant&utm_campaign=fsc2015_0001f' alt='artistry cafe restaurant on weeloy' target="_blank"><img src="https://static.weeloy.com/images/food-selfie-content-2015/partnerlogo/artistry_logo.png"  class="pre-footer-images"></a>
                            <a href='../../../restaurant/singapore/forlino?utm_source=food-selfie-contest-2015&utm_medium=SG_SG_R_ForlinoRestaurant&utm_campaign=fsc2015_0001g' alt='forlino restaurant on weeloy' target="_blank"><img src="https://static.weeloy.com/images/food-selfie-content-2015/partnerlogo/Forlino.png"  class="pre-footer-images"></a>
                            <a href='../../../restaurant/singapore/koskos?utm_source=food-selfie-contest-2015&utm_medium=SG_SG_R_KoskosRestaurant&utm_campaign=fsc2015_0001h' alt='koskos restaurant on weeloy' target="_blank"><img src="https://static.weeloy.com/images/food-selfie-content-2015/partnerlogo/koskos.png"  class="pre-footer-images"></a>
                            <a href='../../../restaurant/singapore/giardino-pizzeria-bar?utm_source=food-selfie-contest-2015&utm_medium=SG_SG_R_GiardinoPizzriaBarRestaurant&utm_campaign=fsc2015_0001i' alt='giardino pizzeria bar restaurant on weeloy' target="_blank"><img src="https://static.weeloy.com/images/food-selfie-content-2015/partnerlogo/giardino_italian_logo.png"  class="pre-footer-images"></a>
                            <a href='../../../restaurant/singapore/match?utm_source=food-selfie-contest-2015&utm_medium=SG_SG_R_MatchRestaurant&utm_campaign=fsc2015_0001j' alt='match restaurant on weeloy' target="_blank"><img src="https://static.weeloy.com/images/food-selfie-content-2015/partnerlogo/match_Logo.jpg"  class="pre-footer-images"></a>
                            <a href='../../../restaurant/singapore/scrumptious-at-the-turf?utm_source=food-selfie-contest-2015&utm_medium=SG_SG_R_ScrumptiousAtTheTurfRestaurant&utm_campaign=fsc2015_0001k' alt='scrumptious-at-the-turf restaurant on weeloy' target="_blank"><img src="https://static.weeloy.com/images/food-selfie-content-2015/partnerlogo/scrumptious.png"  class="pre-footer-images"></a>
                            <a href='../../../restaurant/singapore/pluck?utm_source=food-selfie-contest-2015&utm_medium=SG_SG_R_PluckRestaurant&utm_campaign=fsc2015_0001l' alt='pluck restaurant on weeloy' target="_blank"><img src="https://static.weeloy.com/images/food-selfie-content-2015/partnerlogo/pluck_Logo.png"  class="pre-footer-images"></a>  
                            <a href='../../../restaurant/singapore/sabio-by-the-sea?utm_source=food-selfie-contest-2015&utm_medium=SG_SG_R_SabioByTheSeaRestaurant&utm_campaign=fsc2015_0001m' alt='sabio by the sea restaurant on weeloy' target="_blank"><img src="https://static.weeloy.com/images/food-selfie-content-2015/partnerlogo/sabiobythesea.png"  class="pre-footer-images"></a>
                            <a href='../../../restaurant/singapore/da-paolo-bistro-bar?utm_source=food-selfie-contest-2015&utm_medium=SG_SG_R_DaPaoloBistroBarRestaurant&utm_campaign=fsc2015_0001n' alt='da-paolo-bistro-bar restaurant on weeloy' target="_blank"><img src="https://static.weeloy.com/images/food-selfie-content-2015/partnerlogo/da_paolo.png"  class="pre-footer-images"></a>
                            <a href='../../../restaurant/singapore/cato?utm_source=food-selfie-contest-2015&utm_medium=SG_SG_R_CatoRestaurant&utm_campaign=fsc2015_0001o' alt='cato on weeloy' target="_blank"><img src="https://static.weeloy.com/images/food-selfie-content-2015/partnerlogo/cato.png"  class="pre-footer-images"></a>
                            <a href='../../../restaurant/singapore/lentrecote?utm_source=food-selfie-contest-2015&utm_medium=SG_SG_R_LentrecoteRestaurant&utm_campaign=fsc2015_0001p' alt='lentrecote restaurant on weeloy' target="_blank"><img src="https://static.weeloy.com/images/food-selfie-content-2015/partnerlogo/lentrecote.png"  class="pre-footer-images"></a>
                            <a href='../../../restaurant/singapore/sprmrkt?utm_source=food-selfie-contest-2015&utm_medium=SG_SG_R_SprmrktRestaurant&utm_campaign=fsc2015_0001q' alt='sprmrkt restaurant on weeloy' target="_blank"><img src="https://static.weeloy.com/images/food-selfie-content-2015/partnerlogo/sprmrkt.png"  class="pre-footer-images"></a>
                            <a href='../../../restaurant/singapore/the-latin-quarter?utm_source=food-selfie-contest-2015&utm_medium=SG_SG_R_theLatinQuarterRestaurant&utm_campaign=fsc2015_0001r' alt='the-latin-quarter on weeloy' target="_blank"><img src="https://static.weeloy.com/images/food-selfie-content-2015/partnerlogo/latin_quarter.jpg"  class="pre-footer-images"></a>
                            <a href='../../../restaurant/singapore/the-steak-house?utm_source=food-selfie-contest-2015&utm_medium=SG_SG_R_TheSteakHouseRestaurant&utm_campaign=fsc2015_0001s' alt='the steak house restaurant on weeloy' target="_blank"><img src="https://static.weeloy.com/images/food-selfie-content-2015/partnerlogo/the_steakhouse_logo.png"  class="pre-footer-images"></a>
                            <a href='../../../restaurant/singapore/xin-yue?utm_source=food-selfie-contest-2015&utm_medium=SG_SG_R_XinYueRestaurant&utm_campaign=fsc2015_0001t' alt='xin-yue restaurant on weeloy' target="_blank"><img src="https://static.weeloy.com/images/food-selfie-content-2015/partnerlogo/xinyue.png"  class="pre-footer-images"></a>
                        </li>


                    </div>
                </div>
            </div>
        </footer> 
    </body>
    <footer>
        <div class="follow-us">
            <div class="container">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <h3>Find us on</h3>
                    <ul>
                        <li><a  target="_blank"  href="https://www.facebook.com/weeloy.sg"  id='social-facebook'><span class="fa fa-facebook"></span></a></li>
                        <li><a  target="_blank"  href="https://twitter.com/weeloyasia"  id='social-twitter'><span class="fa fa-twitter"></span></a></li>
                        <li><a  target="_blank"  href="https://www.linkedin.com/company/weeloy-pte-ltd"  id='social-linkedin'><span class="fa fa-linkedin"></span></a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="footer-bg">
            <div class="container">
                <div class="footer-col col-md-3 col-sm-6 col-xs-12">
                    <h3>INFORMATION</h3>
                    <ul class="footer-col-content">
                        <li><a href="../../../blog" id='info-blog'>Food blog</a></li>
                        <li><a href="../../../how-it-works" id='info-hiw'>How It Works</a></li>
                        <li><a href="../../../info-contact" id='info-contact'>Contact Us</a></li>
                        <li><a href="../../../info-partner" id='info-partner'>Partner</a></li>
                        <li><a href="../../../info-faq" id='info-faq'>FAQ's</a></li>
                        <li><a target="_blank" href="https://www.weeloy.com/templates/tnc/termsservices.php" id='info-tns'>Terms and Conditions of Service</a></li>
                        <li><a target="_blank" href="https://www.weeloy.com/templates/tnc/termsconditions.php" id='info-tnc'>Privacy policy and Terms</a></li>
                    </ul>
                </div>
                <div class="footer-col col-md-3 col-sm-6 col-xs-12">
                    <h3>OUR LOCATIONS</h3>
                    <ul class="footer-col-content">
                        <li style="color:#fff">Singapore</li>
                        <li style="color:#fff">Malaysia</li>
                        <li style="color:#fff">Thailand</li>
                    </ul>
                </div>
                <div class="footer-col col-md-3 col-sm-6 col-xs-12">
                    <h3>MOBILE</h3>
                    <div class="footer-col-content">
                        <p>
                            <a  rel="nofollow" class="mobile-picture-div" target="_blank" href="http://itunes.apple.com/app/id973030193">
                                <img width="196" height="60" src="../../../images/home_picture/app-store-badge_en.png" alt="Available on the App Store">
                            </a>
                        </p>
                        <p>
                            <a rel="nofollow"  class="mobile-picture-div" target="_blank" href="https://play.google.com/store/apps/details?id=com.weeloy.client">
                                <img width="196" height="60" src="../../../images/home_picture/play-store-badge_en.png" alt="Get it on Google Play">
                            </a>
                        </p>
                    </div>
                </div>
                <!--      <div class="footer-col col-md-3 col-sm-6 col-xs-12">
                          <h3>NEWSLETTER</h3>
                          <div class="footer-col-content">
                              <form method="POST" action="" id="newsletterform" name="newsletterform">
                                  <input id="email" name="email" type="text" placeholder="e-mail address" />
                                  <input id="submit-follow" onclick="verifFormNewsLetter(newsletterform);" type="button" value="Join" />
                                  <br/>
                                  <br/>
                                  <p>
                                      <span class="" type="label" id="newsletter_message"></span>
                                  </p>
                              </form>
                          </div>
                      </div>-->
            </div>
            <div class="col-log-12 col-md-12 col-sm-12 col-xs-12 copyright">
                <p class="text-center">Copyright &copy; 2015 Weeloy. All Rights Reserved.</p>
            </div>
        </div>
    </footer>

    <script src="js/bootstrap.min.js"></script>


</html>


