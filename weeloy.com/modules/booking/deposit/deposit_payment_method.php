<?php
require_once("conf/conf.init.inc.php");
require_once("lib/wpdo.inc.php");
require_once('conf/conf.session.inc.php');
require_once("lib/wglobals.inc.php");
require_once("lib/class.restaurant.inc.php");
require_once("lib/class.booking.inc.php");
require_once("lib/Browser.inc.php");
require_once("lib/class.analytics.inc.php");
require_once("lib/class.media.inc.php");
require_once("lib/class.coding.inc.php");
require_once("lib/class.login.inc.php");
require_once("lib/class.transitory.inc.php");
require_once("lib/class.member.inc.php");
require_once("lib/class.allote.inc.php");
require_once("lib/class.marketing_campaign.inc.php");
require_once ('lib/class.payment.inc.php');


$url = __BASE_URL__ ;
$url_get_parameters='';
//$arglist = array('bklangue', 'bksalutation', 'bklast', 'bkfirst', 'bkemail', 'bkcover', 'bkdate', 'bktime', 'bkmobile', 'bkcountry', 'bkrestaurant', 'bktitle', 'bkparam', 'bkspecialrequest', 'brwsr_type', 'bkpage', 'bktracking', 'data', 'booker', 'company', 'hotelguest', 'babychair', 'signed_request', 'optin', 'booking_deposit_id', 'resBookingDeposit', 'resCurrency', 'bkproduct', 'bkproductid', 'bkpage1');
//foreach ($arglist as $label) {
//    if (!isset($_REQUEST[$label]))
//        $_REQUEST[$label] = "";
//    $$label = preg_replace("/\'|\"/", "’", $_REQUEST[$label]);
//    $url_get_parameters .= '&'.$label.'=' . $$label;
//}

$arglist = array('bklangue', 'bksalutation', 'bklast', 'bkfirst', 'bkemail', 'bkcover', 'bkdate', 'bktime', 'bkmobile', 'bkcountry', 'bkrestaurant', 'bktitle', 'bkparam', 'bkspecialrequest', 'brwsr_type', 'bkpage', 'bktracking', 'data', 'booker', 'company', 'hotelguest', 'babychair', 'signed_request', 'bkextra', 'bkchild', 'mealtype',  'booking_deposit_id', 'resBookingDeposit','bkproduct', 'bkproductid', 'bkpage1', 'restaurantselected');
foreach ($arglist as $label) {
    if (!isset($_REQUEST[$label]))
        $_REQUEST[$label] = "";
    $$label = $_REQUEST[$label];
    $$label = preg_replace("/\'|\"/", "’", $$label);
    $$label = preg_replace("/\s+/", " ", $$label);
    
    $url_get_parameters .= '&'.$label.'=' . $$label;
}


$booking = new WY_Booking();
 $payment = new WY_Payment();


$error_message = false;
$iswhitelabel = false;

$logger = new WY_log("website");
$loguserid = (isset($_SESSION['user']['user_id'])) ? $_SESSION['user']['user_id'] : "0";
    error_log("PAYMENT PAGE  DATE  :" .$bkdate);
   

if(isset($_REQUEST['action']) &&  $_REQUEST['action'] == 'bktrack_pendingpayment' ){
   
    $booking->getBooking($_REQUEST['refid']);
    if($booking->result < 0) {
            $url =  __ROOTDIR__ ."/modules/booking/deposit/deposit_payment_error.php?error_no=2";
             header("location: $url");
             exit;
    	}
    	
        $bkrestaurant = $booking->restaurant;
        if($bkrestaurant === 'SG_SG_R_TheFunKitchen' || $bkrestaurant === 'SG_SG_R_Bacchanalia' ){
                $depositdetails = $booking->getBookingDepositBaccha($bkrestaurant,$_REQUEST['refid']);
           }else{
               $depositdetails = $payment->getBookingppDetails($bkrestaurant,$_REQUEST['refid']);
        }

      
        $resBookingDeposit = $depositdetails['amount'];
        $resCurrency = $depositdetails['currency'];
        $action = __ROOTDIR__ . "/modules/booking/book_form.php?bkrestaurant =".$bkrestaurant;
        if ($booking->status =='expired' || $booking->status =='cancel' || $booking->status =='' ) {
              $error_message = true;
        }

        $orderdate = explode('-', $booking->rdate);
        $booking->rdate = $orderdate[0]."/".$orderdate[1]."/".$orderdate[2];
        if(isset($_REQUEST['mode']) && $_REQUEST['mode'] == 'failed'){
            $error_message = true;
        }
        if ($booking->status =='' ) {
            $url =  __ROOTDIR__ ."/modules/booking/deposit/deposit_confirmation_confirmed.php?bkconfirmation=".$_REQUEST['refid'];
             header("location: $url");
             exit;
        }
      $logger->LogEvent($loguserid, 720, $_REQUEST['refid'], 'pending_page_link', '', date("Y-m-d H:i:s"));
        
    }
    if(isset($_REQUEST['action']) &&  $_REQUEST['action'] =='pending_payment' ){
       $bkrestaurant = $_REQUEST['restaurant']; 
    }

    if (empty($bkrestaurant)) {
       header("location: $url");
        exit;
    }

////indentify secure url
//
//if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == "on") { 
//   $stripe_form ='page';
//} else { 
//    $stripe_form ='popup';
//}
//
// $stripe_form ='popup';

$fbId = WEBSITE_FB_APP_ID;
$res = new WY_restaurant;
$getRest = $res->getRestaurant($bkrestaurant);


//check if deposit flag active or not

 if ($res->checkbkdeposit() == 0 ){
      header("location: $action");
     exit;
 }
        
$isPaypalActive = ($res->checkPaypalActive() > 0) ? 1 : 0;
$isCreditCardInfoActive = ($res->checkCreditCardDetails() > 0) ? 1 : 0;
$isAdyen = ($res->checkadyen() > 0) ? 1 : 0;
$isReddot = ($res->checkreddot() > 0) ? 1 : 0;


$partialdesc = mb_strimwidth($getRest['description'], 0, 200, "...");
if ($partialdesc !== "")
    $partialdesc = preg_replace("/\r\n/", "", $partialdesc);

$mediadata = new WY_Media($bkrestaurant);
$logo = $mediadata->getLogo($bkrestaurant);
$notification = new WY_Notification();


if (empty($res->restaurant)) {
    header("location: https://www.weeloy.com");
    exit;
}


if($bkpage1 == 'booking_form_section'){
    $url_parameters = base64_encode( substr($url_get_parameters, 1));
    $action = __ROOTDIR__ . "/modules/booking/book_form_section.php?bkrestaurant= $bkrestaurant&p=$url_parameters";
}else{
    $action = __ROOTDIR__ . "/modules/booking/book_form.php";
}


$booker = "";


$restaurant_restaurant_tnc = $res->restaurant_tnc;
$transitory = new WY_Transitory;
$transitory->read('BOOKING-' . $bkparam);

if ($transitory->result > 0) {
    $prev_confirmation = preg_replace("/BOOKING-/", "", $transitory->content);
    $booking->getBooking($prev_confirmation);
}
if (isset($_REQUEST['action']) && $_REQUEST['action'] === 'pending_payment') {

    $booking->getBooking($_REQUEST['deposit_id']);

//    if($bkrestaurant === 'SG_SG_R_TheFunKitchen' || $bkrestaurant === 'SG_SG_R_Bacchanalia' ){
//         $depositdetails = $booking->getBookingDepositBaccha($bkrestaurant,$_REQUEST['refid']);
//    }else{
//       $depositdetails = $booking->getBookingDepositDetails($_REQUEST['refid']);
//    }
    $depositdetails = $payment->getBookingppDetails($bkrestaurant,$refid);

     $resBookingDeposit = $depositdetails['amount'];
     $resCurrency = $depositdetails['currency'];
    
   
    $time_left = $booking->getDateInterval($booking->cdate, '', 'h');
      if ($booking->status =='') {

     echo "<script type='text/javascript'> window.top.location.href ='$url'; </script>";
//        header("location: $url");
 
        exit;
     }

//    if ($booking->status !== 'pending_payment' || $time_left > 20) {
//        echo "<script type='text/javascript'> window.top.location.href ='https://www.weeloy.com'; </script>";
////        header("location: https://www.weeloy.com");
//        exit;
//    }
}


if (empty($booking->restaurant)) {

    $booking->restaurant = $bkrestaurant;
    $dateAr = explode("/", $bkdate);
    $bkDBdate = $dateAr[2] . "-" . $dateAr[1] . "-" . $dateAr[0];  // be carefull msqyl Y-m-d, html d/m/Y  qrc Ymd
    $bkdate = $bkDBdate;
    $language = $bklangue;

    if (empty($bktracking) && !empty($_COOKIE['weeloy_be_tracking'])) {
        $bktracking = $_COOKIE['weeloy_be_tracking'];
    }

    if (isset($_COOKIE['weelredir_' . $bkrestaurant])) {
        $bktracking = (empty($bktracking)) ? "redirect" : $bktracking . "|" . "redirect";
    }

    //if(isset(filter_input(INPUT_COOKIE, 'weelredir30', FILTER_SANITIZE_STRING))) {
    if (isset($_COOKIE['weelredir30_bis'])) {
        $campaign = new WY_MarketingCampaign();
        //$campaign_id = 'EDM1'; //need to extarct campaign id
        $token = $_COOKIE['weelredir30_bis'];
        $campaign_id = $campaign->getCampaignId($token, $bkemail);

        $bktracking = (empty($bktracking)) ? "marketing" . "$campaign_id" : $bktracking . "|" . "marketing" . "$campaign_id";
    }

    $bkstatus = "";
    if (isset($resBookingDeposit)) {
        $bkstatus = 'pending_payment';
    }

	$bktype = "booking";
        $bkstate = "to come";
        $status = $booking->createBooking($bkrestaurant, $bkDBdate, $bktime, $bkemail, $bkmobile, $bkcover, $bksalutation, $bkfirst, $bklast, $bkcountry, $language, $bkspecialrequest, $bktype, $bktracking, $booker, $company, $hotelguest, $bkstate, $optin, $booking_deposit_id, $bkstatus, $bkextra, $bkproduct,$bkchild);
        
    if ($status < 0) {
        $url_parameters = base64_encode( substr($url_get_parameters . "&bkerror=$booking->msg" , 1));
        $action = __ROOTDIR__ . "/modules/booking/book_form_section.php?bkrestaurant=$bkrestaurant&p=$url_parameters";
        header("Location: " . $action); // . "?bkrestaurant=" . $bkrestaurant . "&bktitle=" . $bktitle . "&bktracking=" . $bktracking . "&bkproduct=" . $bkproduct . "&data=" . $data . "&signed_request=" . $signed_request . "&bkerror=" . $booking->msg . "&langue=" . $language . '&bksalutation=' . $bksalutation . '&bklast=' . $bklast . '&bkfirst=' . $bkfirst . '&bkemail=' . $bkemail . '&bkcover=' . $bkcover . '&bkdate=' . $bkdate . '&bktime=' . $bktime . '&bkmobile=' . $bkmobile);
        exit;
    }

    $bkconfirmation = $booking->confirmation;
    if($booking->getBooking($bkconfirmation) == false){
        $url_parameters = base64_encode( substr($url_get_parameters . "&bkerror=$booking->msg" , 1));
        $action = __ROOTDIR__ . "/modules/booking/book_form_section.php?bkrestaurant=$bkrestaurant&p=$url_parameters";
        header("Location: " . $action); // . "?bkrestaurant=" . $bkrestaurant . "&bktitle=" . $bktitle . "&bktracking=" . $bktracking . "&bkproduct=" . $bkproduct . "&data=" . $data . "&signed_request=" . $signed_request . "&bkerror=" . $booking->msg . "&langue=" . $language . '&bksalutation=' . $bksalutation . '&bklast=' . $bklast . '&bkfirst=' . $bkfirst . '&bkemail=' . $bkemail . '&bkcover=' . $bkcover . '&bkdate=' . $bkdate . '&bktime=' . $bktime . '&bkmobile=' . $bkmobile);
        exit;   
    }

    $transitory->create('BOOKING-' . $bkparam, $booking->confirmation);
    if ($bkstatus !== 'pending_payment') {
        //qrcode generartion
        $path_tmp = explode(':', get_include_path());
        $qrcode_filename = $path_tmp[count($path_tmp) - 1] . 'tmp/' . $bkconfirmation . '.png';
        $qrcode = new QRcode();
        $qrcode->png('{"membCode":"' . $booking->membCode . '"}', $qrcode_filename);
        //file_put_contents($qrcode_filename,$qrcode->image(8));
        //$booking->notifyBooking($qrcode_filename);
        //$notification->notify($booking, 'booking');
    }
   
     $logger->LogEvent($loguserid, 719, $bkconfirmation, 'pending_page', '', date("Y-m-d H:i:s"));
} else {

 
    $bkconfirmation = $booking->confirmation;
    $bktime = $booking->rtime;
    $bkemail = $booking->email;
    $bkmobile = $booking->mobile;
    $bkcover = $booking->cover;
    $bksalutation = $booking->salutation;
    $bkfirst = $booking->firstname;
    $bklast = $booking->lastname;
    $bkcountry = $booking->country;
    $language = $booking->language;
    $bkspecialrequest = $booking->specialrequest;
    $bkdate = $booking->rdate;
    $bklangue = $language;
    $res_country = $booking->restaurantinfo->country;
    $bkproduct = $booking->product;
    $bktracking = $booking->tracking;
         if ($booking->status =='' ) {
          $url =  __ROOTDIR__ ."/modules/booking/deposit/deposit_confirmation_confirmed.php?bkconfirmation=".$bkconfirmation;
           header("location: $url");
           exit;
        }
        if ($booking->status =='expired' || $booking->status =='cancel') {
              $error_message = true;
        }
    $rdate = date('Y-m-d',strtotime($bkdate));
    $bkdate = $rdate;
//  $dateAr = explode("/", $bkdate);
//  $bkDBdate = $dateAr[2] . "-" . $dateAr[1] . "-" . $dateAr[0];  // be carefull msqyl Y-m-d, html d/m/Y  qrc Ymd
//  $bkdate = $bkDBdate;
  
}


$bkspecialrequest = htmlentities($bkspecialrequest);
$resCountry = strtolower($getRest['country']);

if(preg_match("/walkin|CALLCENTER|tms|waiting|WEBSITE|facebook|GRABZ/i", $booking->tracking)){
    $iswhitelabel = true;
}
 $redirectUrl =__BASE_URL__ ."/modules/booking/book_form.php?bkrestaurant=$bkrestaurant"; 
//$redirectUrl = __BASE_URL__ . "/restaurant/$resCountry/";
    if($iswhitelabel){
        $redirectUrl =$redirectUrl."&bktracking=".$bktracking;
    }

if($bkrestaurant == 'SG_SG_R_BurntEnds' || $bkrestaurant == 'SG_SG_R_TheOneKitchen'){
    $redirectUrl = __BASE_URL__ . "/modules/booking/book_form_section.php?bkrestaurant=$bkrestaurant&bktracking=$bktracking";
}


$logger->LogEvent($loguserid, 804, $getRest['ID'], $booking->bookid, '', date("Y-m-d H:i:s"));



//qrcode delete
//unlink($qrcode_filename);
//error_log("<br><br>BOOKING = " . $status . " " . $booking->msg);

$QRCodeArg = "&bkrestaurant=$bkrestaurant&bkconfirmation=$bkconfirmation&bktracking=$bktracking";

$typeBooking = ($res->is_bookable && ($res->status == 'active' || $res->status == 'demo_reference')) ? "true " : "false ";
$is_listing = false;
if (empty($res->is_wheelable) || !$res->is_wheelable) {
    $is_listing = true;
}

$bkgcustomcolor = "";

$credentials = $res->getRestaurantPaymentId($bkrestaurant);

if (!empty($res->bkgcustomcolor) && preg_match("/website|facebook|CALLCENTER/i", $bktracking))
    if (strlen($res->bkgcustomcolor) > 3 && preg_match("/\|/", $res->bkgcustomcolor)) {
        $tt = explode("|", $res->bkgcustomcolor);
        if (strlen($tt[0]) > 2)
            $bkgcustomcolor = $tt[0];
    }
error_log('BKGCOLOR ' . $bkgcustomcolor . " " . (preg_match("/website|facebook|CALLCENTER/i", $bktracking) ? 1 : 0) . " " . $res->bkgcustomcolor);
?>


<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta http-equiv='expires' content='mon, 1 jan 1998 05:00:00 gmt'/>
        <meta http-equiv='pragma' content='no-cache'/>
        <meta http-equiv='pragma' content='cache-control: max-age=0'/>
        <meta http-equiv='last-modified' content='mon, 1 jan 1998 05:00:00 gmt'/>
        <meta http-equiv='cache-control' content='no-cache, must-revalidate'/>
        <meta name='robots' content='noindex, nofollow'/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
        <meta name="HandheldFriendly" content="true" />
        <title>Payment Method - Weeloy</title>
        <base href="<?php echo __ROOTDIR__; ?>/"/>
        <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
        <link href="css/bootstrap-select.css" rel="stylesheet" />
        <link href="css/bootstrap-social.css" rel="stylesheet" />
        <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="modules/booking/assets/css/bookingform.css?v=1" rel="stylesheet" />

        <script type="text/javascript" src="js/jquery.min.js"></script>
        <script type="text/javascript" src="js/bootstrap.min.js"></script>
        <script type='text/javascript' src="js/angular.min.js"></script>
        <script type="text/javascript" src="js/ngStorage.min.js"></script>
        <script type="text/javascript" src="https://js.stripe.com/v2/"></script>
        <style>


            <?php //if ($brwsr_type != "mobile") echo ".mainbox { margin: 10px 30px 20px 1px; width:550px; } ";   ?>

            <?php
            if(!empty($bktracking) && preg_match("/GRABZ/i", $bktracking)){$bkgcustomcolor = '#000000';}
// booking customization
            if (!empty($bkgcustomcolor)) {

echo <<<DELIMITER
h1 { background-color: $bkgcustomcolor; }
.panel { border-color: $bkgcustomcolor !important; }
.panel-heading { background-color: $bkgcustomcolor !important; border-color: $bkgcustomcolor !important; }
.caret { color: $bkgcustomcolor; }
@media (min-width: 530px) {
	.separation { border-right: 4px solid $bkgcustomcolor; height: 100%; }
	.separation-left { border-left: 4px solid $bkgcustomcolor; }
}
DELIMITER;
            }
            ?>

        </style>
        <style type="text/css">
            fieldset.optionGroup
            {
                float: none;
                margin-left: 25%;
                margin-bottom: 15px;
            }
            fieldset.optionGroup label
            {
                display: inline;
                float: none;
                width: 100px;
            }
            fieldset.optionGroup input
            {
                float:none;
                margin: 0px;
                width: 20px;
                position:relative;
                top:-16px;
            }
            fieldset.optionGroup div
            {
                margin-bottom: 10px;
            }
            fieldset.optionGroup img
            {
                display:inline; 
                margin: auto;
                padding-bottom: 0px; 
                vertical-align: inherit;
            }
        </style>
    </head>
    <body ng-app="myApp"  style="background: transparent none repeat scroll 0 0;"  class="ng-cloak">

        <!-- tracking code for GTM -->

        <?php
        $isTest = false;
        if (strpos($bkspecialrequest, 'test') !== false) {
            $isTest = true;
        }
        if (strpos($bkfirst, 'test') !== false) {
            $isTest = true;
        }
        if (strpos($bklast, 'test') !== false) {
            $isTest = true;
        }
        if (strpos($bkrestaurant, 'SG_SG_R_TheFunKitchen') !== false) {
            $isTest = true;
        }
        if (strpos($bkrestaurant, 'SG_SG_R_TheOneKitchen') !== false) {
            $isTest = true;
        }
        if (!$isTest) {
            ?>
            <script>

                        dataLayer = [];
                        dataLayer.push({
                            'pax': <?php echo $bkcover; ?>,
                            'reservation_id': '<?php echo $bkconfirmation; ?>',
                            'reserved_by': '<?php
        if (isset($_SESSION['user']['email']))
            echo 'user';
        else
            echo 'guest';
        ?>',
                            'reserver_name': '<?php echo $bkfirst . ' ' . $bklast; ?>',
                            'reserver_loginid': '<?php echo $bkemail; ?>',
                            'reservation_date': '<?php echo $bkdate; ?>',
                            'reservation_time': '<?php echo $bktime; ?>',
                            'restaurant_name': '<?php echo $bktitle; ?>',
                            'restaurant_id': '<?php echo $bkrestaurant; ?>',
                            'tracking': '<?php echo $bktracking; ?>'
                        });
            </script>
        <?php } ?>

        <?php include_once("ressources/analyticstracking.php") ?>


        <div id='booking' ng-controller='MainController'>

            <div class="container mainbox">    
                <div class="panel panel-info"  >
                    <div class="panel-heading">
                        <div class="panel-title" style='width:300px;'> PAYMENT METHOD </div>
                    </div>  
                    <div ng-if="isError "> 
                        <p class="slogan"  class='text-warning'>We are unable to process your credit card payment. Your Reservation session expired. Please make a new servation.
                        </p>
                    </div>
                        <span ng-if="!isError" >
                            <p class="slogan" style="font-size:20px" ng-if ="restaurant !== 'SG_SG_R_Pollen'">
                                <strong>Final step - Credit card guarantee</strong>
                            </p>
                            <p class="slogan" ng-if ="restaurant !== 'SG_SG_R_Pollen'">To confirm your booking, the restaurant needs your credit card details. Credit card information is stored by a specialised secure payment gateway and cannot be retrieve by the restaurant.</p>
                        </span>
<!--                    <p class="slogan" ng-if="!hasPayment">To confirm your booking, select your deposit method and proceed to deposit</p>-->


                    <div class="panel-body" ng-if="!isError "  >
                        <div  class='row' style="margin-left:5px" >
<!--                            <h1 >Booking Details</h1>
                            <div class="Payment_details">
                                <span>Restaurant:</span> {{hoteltitle}}<br /><br />

                                <span>Date:</span> {{pdate}}<br /><br />
                                <span>Time:</span> {{ptime}}<br /><br />
                                <span>Guests:</span> {{pcover}}<br /><br />
                            </div>-->
<!--                            <h1 >Charge</h1>
                            <div class="Payment_details">
                                <span>Amount:</span><strong> <span style="font-size:13px;">{{resCurrency}}</span> {{resBookingDeposit}}</strong><br /><br />
                            </div>-->
                            <div  class='row' style="margin-left:5px">
                                <h1>Payment method</h1>
                                <div class="Payment_method">
                                     <p>To confirm the booking, the restaurant needs your credit card details. The credit card information are stored by a specialized payment gateway and nobody can retrieve and use your card details.</p>
                                    <fieldset class="optionGroup">
<!--                                        <div>
                                            <label ng-if='creditcardinfo === 0'>
                                                <input type="radio" name="payment-method" id="creditcard"  value='creditcard' checked="checked"/> 
                                                <img src="images/visa-master-amex.jpg" alt="creditcard"  style="display:inline; margin: auto;padding-bottom: 0px; vertical-align: inherit;" />
                                            </label>
                                        </div>-->
                                        <div>
                                            <label ng-if='paypaloptionavailable === 1 && creditcardinfo === 0 && reddot === 0 && adyen === 0 '>
                                                <input type="radio" name="payment-method" value='paypal' id="paypal" />
                                                <img src="images/paypal.png" alt="paypal" />
                                            </label>
                                        </div>
                                        <div>
                                            <label  ng-if='creditcardinfo === 1' >
                                                <input type="radio" name="payment-method" value='carddetails' id="creditcardinfo" checked="checked"/>
                                                <img src="https://static.weeloy.com/images/visa-master-amex.jpg" alt="creditcard"  style="display:inline; margin: auto;padding-bottom: 0px; vertical-align: inherit;" />
                                                
                                            </label>
                                        </div>
                                        <div>
                                            <label  ng-if='paypaloptionavailable === 0 && creditcardinfo === 0 && adyen === 0 && reddot === 1 ' >
                                                <input type="radio" name="payment-method" value='reddot' id="creditcardinfo" checked="checked"/>
                                                <img src="https://static.weeloy.com/images/visa-master.jpg" alt="creditcard"  style="display:inline; margin: auto;padding-bottom: 0px; vertical-align: inherit;" />
                                                
                                            </label>
                                        </div>
                                        <div>
                                            <label  ng-if='paypaloptionavailable === 0 && creditcardinfo === 0 && reddot === 0 && adyen === 1 ' >
                                                <input type="radio" name="payment-method" value='adyen' id="creditcardinfo" checked="checked"/>
                                                <img src="images/visa-master-amex.jpg" alt="creditcard"  style="display:inline; margin: auto;padding-bottom: 0px; vertical-align: inherit;" />
                                                
                                            </label>
                                        </div>
                                    </fieldset>
                                     
<!--                                    <form id='creditcard_stripe' id="credit_card_selection"  target="_blank" action='https://payment.restaurants.sg/modules/payment/stripe/stripe_payment_form.php?action=booking_deposit_stripe' method="post" >-->
                                     
                               
                                    
                                </div>
                                <div class="payment-card" ng-if="payCard === true">

                                </div>
                            </div>

                            <div  class='row' ng-if="paymenttnc" style="margin-left:5px">
                                <h1 >Restaurant Cancellation Policy</h1>
<!--                                <p >Cancellations can be made free until {{freerange}} before the arriving timing. Cancellations made after the time period below will be charged the following fee:  </p>-->
    
                                <p ng-if ="restaurant !== 'SG_SG_R_Pollen'"> {{paymenttnc}}. </p>
                                    <div ng-if ="restaurant === 'SG_SG_R_Pollen'"> 
                                        <p ng-repeat="str in paymenttncAr">{{ str }} </p>
                                 </div>
<!--                                <div  ng-if='!chkcpflg' ng-repeat ="p in cancelpolicy">
                                   <p ng-if="p['percentage'] == 100">{{p['duration']}}{{resCurrency}} {{resBookingDeposit}}. 
                                   <p ng-if="p['percentage'] == 0">{{p['duration']}}</p>
                                   <p ng-if="p['percentage'] != 0 && p['percentage']!= 100">{{p['duration']}} </p>
                                </div>
                                <div ng-if='!chkcpflg' ng-repeat ="p in cancelmsg">
                                <span  style="font-size:13px;" ng-if="p">* {{p}}</span>
                                </div>-->
                                <br/>
                            </div>
<!--                                <div class="input-group" ng-if="bacchanaliapolicy.length >0 && (restaurant ==='SG_SG_R_TheFunKitchen' || restaurant ==='SG_SG_R_Bacchanalia') ">
                                     <h1 >Restaurant Booking Cancellation Policy</h1>
                                      <p >{{bacchanaliapolicy}}.</p>
                                </div>
                                <div class="input-group" ng-if="medinipolicy.length >0 && (restaurant ==='SG_SG_R_TheOneKitchen') ">
                                     <h1 >Restaurant Booking Cancellation Policy</h1>
                                      <p >{{medinipolicy}}.</p>
                                </div>-->
                                <div id='buttons' style="width:100%;">
                                <div style="float:left;">
                                    <input type='button' class='btn btn-blue' value='Cancel the booking' ng-click="bookingCancel();" />

                                </div>
                               <div ng-if='creditcardinfo === 1' >
                                    <div style="float:right;">
                                         <form id='creditcard_stripe' id="credit_card_selection"  action='modules/payment/stripe/stripe_payment_form.php?action=booking_deposit_stripe' method="post" >
                                            <input type="hidden" id="resBookingDeposit" name="amount" value="{{resBookingDeposit}}"/>
                                            <input type="hidden" id="confirmation" name="confirmation" value="{{confirmation}}"/>
                                            <input type="hidden" id="email" name="email" value="{{pemail}}"/>
                                            <input type="hidden" id="paymentType" name="paymentType" value="carddetails"/>

                                         <input  
                                             id="submit-button"
                                            type="submit" 
                                            value="CONTINUE"
                                            data-key="<?php echo $credentials['stripe_public_key'] ?>"
                                            data-panel-label ="Submit"
                                            data-email="<?php echo $bkemail ?>"
                                            class=" book-button-sm btn-leftBottom-orange ng-binding"
                                            style='padding:5px 25px;'
                                         /> 
                                         </form>
                                    </div>
                               </div>
                                <div ng-if='creditcardinfo === 0' >
                                    <input id='continue_btn' type="button" class="book-button-sm btn-leftBottom-orange ng-binding" value='CONTINUE' ng-click="depsoitPayment();" style='float:right;padding:5px 25px;!important'/>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>  
            </div>
        </div>
        <script src="https://checkout.stripe.com/v2/checkout.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>

        <script>

<?php
echo "var confirmation = '" . $booking->confirmation . "';";
echo "var tracking = '" . $booking->tracking . "';";
echo "var hoteltitle = '" . $booking->restaurantinfo->title . "';";
echo "var restaurant = '$bkrestaurant';";
echo "var typeBooking = $typeBooking;";
echo "var brwsr_type = '$brwsr_type';";
echo "var imglogo = '$logo';";
echo "var pproduct = '$bkproduct';";
echo "var pdate = '$bkdate';";
echo "var ptime = '$bktime';";
echo "var pcover = '$bkcover';";
echo "var bkchild = '$bkchild';";
echo "var pfirst = '$bkfirst';";
echo "var plast = '$bklast';";
echo "var pemail = '$bkemail';";
echo "var pmobile = '$bkmobile';";
echo "var langue = '$bklangue';";
echo "var optin = '$optin';";
echo "var topic = 'BOOKING';";
echo "var partialpath = '" . $getRest['internal_path'] . "';";
echo "var partialdesc = '" . $partialdesc . "';";
echo "var fbId = '" . $fbId . "';";
echo "var mcode = '" . $booking->membCode . "';";
echo "var city = '" . $booking->restaurantinfo->city . "';";
echo "var paypaloptionavailable = $isPaypalActive;";
echo "var redirectUrl = '$redirectUrl';";
echo "var isCreditCardInfoActive = $isCreditCardInfoActive;";
echo "var error_message ='$error_message';";
echo "var iswhitelabel = '" . $iswhitelabel . "';"; 
echo "var base_url ='$url';";
echo "var resBookingDeposit ='" . $resBookingDeposit . "';";
echo "var resCurrency ='" . $resCurrency . "';";
echo "var generic ='" . $booking->generic . "';";

//Payment Flag

echo "var isAdyen = $isAdyen;";
echo "var isReddot = $isReddot;";


//if (isset($_POST['resBookingDeposit']) && isset($_POST['resCurrency'])) {
//    echo "resBookingDeposit = '" . $resBookingDeposit . "';";
//    echo "resCurrency = '" . $_POST['resCurrency'] . "';";
//}
//if (isset($_REQUEST['action']) && isset($depositdetails) && ($_REQUEST['action'] == 'pending_payment' || $_REQUEST['action'] == 'bktrack_pendingpayment')) {
//   
//    echo "resBookingDeposit = '" . $depositdetails['amount'] . "';";
//    echo "resCurrency = '" . $depositdetails['currency'] . "';";
//}

printf("var flgsg = %s;", (!preg_match('/CALLCENTER|WEBSITE|facebook|GRABZ/', $bktracking)) ? 'true' : 'false');
printf("var is_listing = %s;", $is_listing ? 'true' : 'false');
?>

var app = angular.module("myApp", ['ngStorage']);

app.controller('MainController', function ($scope, $http, $locale, $timeout, $localStorage) {

	$scope.hasDeposit = false;
	$scope.payCard = false;
        $scope.isError = (error_message ==='1') ? true : false;
	$scope.hasPayment = false;
	$scope.tracking = tracking;
	$scope.restaurant = restaurant;
	$scope.langue = langue;
	$scope.pcheckmark = ($scope.langue != 'cn') ? 'pcheckmark' : 'pcheckmarkcn';
	$scope.imglogo = imglogo;
	$scope.brwsr_type = brwsr_type;
	$scope.hoteltitle = hoteltitle;
	$scope.confirmation = confirmation;
	$scope.flgsg = flgsg;
	$scope.spincode = (flgsg && mcode != '' && mcode.length == 4 && mcode != '0000') ? '(spin:' + mcode + ')' : '';

	$scope.bookingtitlecf = (typeBooking) ? "BOOKING DEPOSIT PAYMENT" : "REQUESTED CONFIRMED";
	$scope.confirmsg = (typeBooking) ? "Your booking is pending  " : "Your request has been sent to ";

	$scope.confirmsg2 = (typeBooking) ? "We are happy to confirm your reservation" : "Your reservation number is ";
	$scope.confirmsg3 = "You will receive the details of your reservation by email and SMS";
	$scope.confirmsg4 = "You can sign in using your email to view all your bookings and leave your reviews";
	$scope.bacchanalia = "Your table has been blocked, we are sending you an e-mail to reconfirm the booking";
	$scope.listTags0 = "Free Booking";
	$scope.listTags1 = "";
	$scope.listTags2 = (typeBooking) ? "Instant Reservation" : "No reservartion";
	$scope.listTags3 = (typeBooking) ? "" : "First come, first serve";
	$scope.listTags4 = (!is_listing) ? "Win" : "Enjoy incredible";
	$scope.listTags5 = (!is_listing) ? "for every bookings" : " promotion";

	$scope.pproduct = pproduct;
        $scope.pdate = pdate;
	$scope.ptime = ptime;
	$scope.pcover = pcover;
	$scope.pfirst = pfirst;
	$scope.plast = plast;
	$scope.pemail = pemail;
	$scope.pmobile = pmobile;
	$scope.pname = pfirst + ' ' + plast;
	$scope.paypaloptionavailable = paypaloptionavailable;
	$scope.creditcardinfo = isCreditCardInfoActive;
    $scope.adyen = isAdyen;
	$scope.reddot = isReddot;
    $scope.generic = generic;
    $scope.extrachoice = [];var validOptions = ['event', 'duration', 'repeat', 'notifysmswait', 'notestext', 'notescode', 'validate', 'orders','choice'];

   
	
        $scope.parseObj = function(str) {
            if(typeof str !== "string" || str === "")
                return {};
            
            str = str.replace(/’/g, "\"");
            if(str !== '')
                str = str.replace(/\s+/g, " ");
            try {
            var oo = JSON.parse(str);
            } catch(err) { console.log(err, str); return {}; }
            return (oo) ? oo : {};
        };



     if($scope.generic && typeof $scope.generic === "string" && $scope.generic !== "") {
        $scope.extrachoice =$scope.parseObj($scope.generic);   
        Object.keys($scope.extrachoice).map(function(x) { if(validOptions.indexOf(x) >= 0) $scope.extrachoice[x] = $scope.extrachoice[x]; });                              
      }
      
        //$scope.stripe_form =stripe_form;
        $scope.iswhitelabel = (iswhitelabel==='1') ? true : false;
        //angular.copy(locales['en'], $locale);
	if (resBookingDeposit !== 'undefined') {
   
		$scope.resBookingDeposit = resBookingDeposit;
		$scope.resCurrency = resCurrency;
		$scope.hasDeposit = true;
	}

	$scope.redirectUrl = redirectUrl;	
		  
	$scope.langdata = $localStorage.langdata;
	

	var restaurant_name = $scope.restaurant.substr(8);
         
	restaurant_name = restaurant_name.replace(/_/g, '');     
	restaurant_name = restaurant_name.replace(/([A-Z])/g, '-$1');      
	restaurant_name = restaurant_name.replace(/[-]+/, '-');
	restaurant_name = restaurant_name.toLowerCase();

	if (restaurant_name.charAt(0) == '-') {
		restaurant_name = restaurant_name.substr(1);
	}
	$scope.restitle = restaurant_name;


	if ($scope.langdata && $scope.langdata.version && $scope.langdata.version.lb) {
//                                            if ($scope.langdata.version.lb == restaurant  )
			$scope.zang = $scope.langdata;
	}



	//$scope.checkmark = [{"label1": $scope.zang['listtag1'].vl, "label2": " ", "glyph": true}, {"label1": $scope.zang['listtag2'].vl, "label2": $scope.zang['listtag3'].vl, "glyph": true}, {"label1": $scope.zang['listtag4'].vl, "label2": $scope.zang['listtag5'].vl, "glyph": true}];
	if (flgsg == false)
		//$scope.checkmark.pop(); // don't show "win everytime
	//$scope.bookinginfo = [{"label": "divider", "value": $scope.zang['bookingdetail'].vl}, {"label": $scope.zang['datetag'].vl, "value": $scope.pdate}, {"label": $scope.zang['timetag'].vl, "value": $scope.ptime}, {"label": $scope.zang['guesttag'].vl, "value": $scope.pcover}, {"label": "divider", "value": $scope.zang['personaldetail'].vl}, {"label": $scope.zang['nametag'].vl, "value": $scope.pname}, {"label": $scope.zang['emailtag'].vl, "value": $scope.pemail}, {"label": $scope.zang['mobiletag'].vl, "value": $scope.pmobile}, {"label": $scope.zang['requesttag'].vl, "value": $scope.prequest}];
	$scope.fbshareclik = function () {

		var fbObj = fbshare();
		FB.ui(fbObj, function (response) {
			console.log(JSON.stringify(response));
		});

	}


	$scope.depositCheckout = function (mode) {
            $scope.hasPayment = true;
            var API_URL = "api/v2/payment/makepayment/deposit" ;
            $http({
                    url: API_URL,
                    method: "POST",
                    data: $.param({
                            'restaurant':$scope.restaurant,
                            'payment_type': mode,
                            'amount':$scope.resBookingDeposit,
                            'reference_id':$scope.confirmation,
                            'mode'        :'booking'
                        }),
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded'
                        }
                    }).then(function (response) {
                          var result = response.data,url;
                        if(result.data.payment){
                            if(mode === 'paypal'){
                                url = result.data.payment.paypalUrl;
                            
                            document.cookie = 'wee_navigation=p; expires=Fri, 3 Aug 2031 20:47:11 UTC; path=/';
                            var newWin = window.open(url, '_blank', 'toolbar=no, scrollbars=yes, copyhistory=no, resizable=no, top=100, left=100, width=950, height=785');
                            if (!newWin || newWin.closed || typeof newWin.closed == 'undefined') {
                                    document.cookie = 'wee_navigation=np; expires=Fri, 3 Aug 2031 20:47:11 UTC; path=/';
                                    var newWin2 = window.open(url, '_top', 'toolbar=no, scrollbars=yes, copyhistory=no, resizable=no, top=100, left=100, width=950, height=785');
                                    if (!newWin2 || newWin2.closed || typeof newWin2.closed == 'undefined') {
                                            alert('Hey! We have detected a pop-blocker. For this transaction, we need to open the paypal secured form in a pop-up. Please allow pop-up for this transaction and click ok');
                                    }
                            }
 
                            }else{
                                var data =  new Object();
                                data.action = result.data.payment.action,
                                data.amount = $scope.resBookingDeposit,
                                data.email = $scope.pemail,
                                data.order_number = $scope.confirmation;
                                data.merchant_id = result.data.payment.merchant_id;   //'1000089330';
                                data.currency_code = result.data.payment.currency_code;
                                data.merchant_reference = result.data.payment.merchant_reference,
                                data.return_url  = result.data.payment.return_url,
                                data.signature = result.data.payment.signature,
                                data.transaction_type ='Sale',
                                data.key = result.data.payment.key;   //'9560b02e6b27f41fc691e53b8bf6c47e0b81731f';
                                var form = document.createElement('form');
                                form.action = result.data.payment.reddot_url;             //'http://test.reddotpayment.com/connect-api/cgi-bin-live';
                                form.method = 'POST';
                                $.each(data, function (key, value) {
                                    var element = document.createElement("Input");
                                    element.setAttribute("type", "hidden");
                                    element.setAttribute("name", key);
                                    element.setAttribute("value", value);
                                    var div = document.createElement("div");
                                    form.appendChild(element);
                                });
                                document.body.appendChild(form);
                                form.submit();
                            }
                                
                            }
                
            });
		
	}
//	$scope.cancelpolicy = function () {
//		   var payment_method =($scope.creditcardinfo === 1) ?'carddetails' : "credit card" ;
//		return $http.post("api/services.php/cancelpolicy/list",
//				{
//					 'restaurant':$scope.restaurant,
//					 'type': payment_method,
//					 'amount':$scope.resBookingDeposit,
//                                         'product':$scope.pproduct
//				  
//				}).then(function (response) {
//			var data = response.data.data;
//                        console.log("cancel"+JSON.stringify(response.data.data));
//
//			if (typeof data !== 'undefined') {
//				$scope.cancelmsg = data.message_array;
//				$scope.freerange =data.lastRange;
//				$scope.cancelpolicy = data.range;
//			}
//		});
//
//	};

	$scope.bookingCancel = function () {
            if (confirm("Please confirm the cancellation of reservation " + $scope.confirmation + " ?") == false)
			return;

		return $http.post("api/booking/pendingpayment/cancel/",
				{
					'restaurant': $scope.restaurant,
					'confirmation': $scope.confirmation,
				}).then(function (response) {


			if (response.data.status === 1) {
				window.location.href = $scope.redirectUrl;
			}

		});
	}
        
//        $scope.bacchanacancelpolicy = function () {
//
//            var uu = $scope.pdate.split('-');
//                       var date = uu[2]+"-"+uu[1] +"-"+uu[0];
//            var API_URL = "api/v2/restaurant/cancelpolicy/getPolicy";
//			
//                    $http({
//                            url: API_URL,
//                            method: "POST",
//                            data: $.param({
//                                    'restaurant': $scope.restaurant,
//                                    'date' 	: date,
//                                    'time'	: $scope.ptime,
//                                    'pax' : $scope.pcover
//                        }),
//                    headers: {
//                        'Content-Type': 'application/x-www-form-urlencoded'
//                    }
//                    }).then(function (response) {
//			var data = response.data.data;
//                        if (typeof data !== 'undefined' ) {
//                        $scope.bacchanaliapolicy = data.priceDetails.message;  
//                     }
//
//		});
//                
//
//        };
        $scope.paymentpolicy = function () {
            
            var payment_method =($scope.creditcardinfo === 1) ?'carddetails' : "paypal" ;

            var uu = $scope.pdate.split('-');
            var date = uu[2]+"-"+uu[1] +"-"+uu[0];
            if(typeof $scope.pproduct === 'undefined' || $scope.pproduct === ''){
                if($scope.extrachoice && $scope.extrachoice.choice) {
                     $scope.pproduct = $scope.extrachoice.choice;
                }
                
            }
             var API_URL = "api/v2/restaurant/cancelpolicy/read";
                    $http({
                            url: API_URL,
                            method: "POST",
                            data: $.param({
                                    'restaurant': $scope.restaurant,
                                    'type': payment_method,
                                    'amount': $scope.resBookingDeposit,
                                    'product' :$scope.pproduct,
                                    'date' 	: date,
                                    'time'	: $scope.ptime,
                                    'cover' : $scope.pcover
                        }),
                        
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    }
                    }).then(function (response) {
                        var data = response.data.data;
                        $scope.chkcpflg = false;
                        if (data) {
        
                            $scope.paymenttnc = data.priceDetails.message;
                              if($scope.restaurant === "SG_SG_R_Pollen") {
                            	 //data.priceDetails.message = $scope.frontinfo;
                           	 $scope.paymenttncAr = $scope.paymenttnc.split('-'); 
                               }
                  
//                            if($scope.restaurant === 'SG_SG_R_BurntEnds' ){
//                                $scope.paymenttnc = data.priceDetails.message;
//                                $scope.freerange = data.priceDetails.lastRange;
//                                $scope.cancelpolicy = data.priceDetails.range;
//                            }else{
//                
//                                $scope.paymenttnc = data.priceDetails.message; 
//                                $scope.chkcpflg = true;
//                            }
//                            //if(data.priceDetails.requiredccdetails){
//                                if($scope.restaurant === 'SG_SG_R_TheFunKitchen' || $scope.restaurant === 'SG_SG_R_Bacchanalia'){
//                                    console.log("SDSD" +data.priceDetails.message);
//                                    $scope.paymenttnc = data.priceDetails.message; 
//                                    $scope.chkcpflg = true;
//                                }else {
//                                    $scope.paymenttnc = data.priceDetails.message;
//                                    $scope.freerange = data.priceDetails.lastRange;
//                                    $scope.cancelpolicy = data.priceDetails.range;
//                                    
//                                   
//                                }
                                //$scope.medinipolicy = data.priceDetails.message; 
                            //}
                                
                        }
            });

        };
        $scope.paymentpolicy();

        
//        if($scope.restaurant === 'SG_SG_R_TheFunKitchen' || $scope.restaurant === 'SG_SG_R_Bacchanalia' ){
//                $scope.bacchanacancelpolicy();
//            }
//        if($scope.restaurant === 'SG_SG_R_TheOneKitchen' ){
//            $scope.paymentpolicy();
//        }else{
//            $scope.cancelpolicy();
//            }
	

	$scope.payCard = function () {
		$scope.payCard = true;
	};
        
        $scope.targetPopup = function(){
            var params = '&';
            params += 'resBookingDeposit=' + $scope.resBookingDeposit + '&';
            params += 'resCurrency=' + $scope.resCurrency + '&';
            params += 'restaurant=' + $scope.restaurant + '&';
            params += 'confirmation=' + $scope.confirmation + '&';
            params += 'action= carddetails' ;
            //params += 'action=' + $('#action').val() + '&';
            
            //url = 'https://payment.restaurants.sg/modules/payment/stripe/stripe_payment_form.php?action=booking_deposit_stripe' + params;
            //url = 'modules/payment/stripe/stripe_payment_form.php?action=booking_deposit_stripe' + params;
            url = 'modules/payment/adyen/adyen_secure_form.php?action=booking_deposit_stripe'+params;
            var newWin2 = window.open(url);
            //window.location.href = 'modules/booking/deposit/deposit_check_status_new.php?action=pending_payment'+params;   
            //$("#creditcard_stripe").submit();
            return true;
       }

	$scope.depsoitPayment = function () {
            var mode = $("input:radio[name=payment-method]:checked").val();
                if (mode === 'paypal') {
                    $scope.depositCheckout(mode);
                }else if (mode === 'adyen') {
                    $scope.targetPopup();
                }else if (mode === 'reddot') {
                    $scope.depositCheckout(mode);
                }else{
                    return;
                } 
        };
	
        
        
        
	function fbshare() {
		var obj = {
			method: 'feed',
			link: "https://www.weeloy.com/" + partialpath,
			picture: $scope.imglogo,
			name: $scope.hoteltitle,
			caption: 'New booking at ' + $scope.hoteltitle + '. Now enjoy rewards with Weeloy',
			description: partialdesc,
			display: 'popup'
		};
		return obj;

	}
	function sortMethod(a, b) {
		var x = a.name.toLowerCase();
		var y = b.name.toLowerCase();
		return ((x < y) ? -1 : ((x > y) ? 1 : 0));
	}


});

String.prototype.capitalize = function () {
	return this.replace(/^./, function (match) {
		return match.toUpperCase();
	});
};



function openWin(type) {
	var arg = <?php echo "'$QRCodeArg'"; ?>;
	window.open('modules/qrcode/QRCode.htm?type=' + type + arg, 'QRCode', 'toolbar=no, scrollbars=no, resizable=no, top=30, left=30, width=330, height=330');
}


$(document).ready(function () {

       
            $(':submit').on('click', function(event) {
                event.preventDefault();
                var $button = $(this),
                    $form = $button.parents('form');
                   
                var opts = $.extend({}, $button.data(), {
                    token: function(result) {
                          $(":submit").attr("disabled", true);

                        $form.append($('<input>').attr({ type: 'hidden', name: 'stripeToken', value: result.id }));
                         $form.get(0).submit();
                    }
                });
                StripeCheckout.open(opts);
            });
       

	window.fbAsyncInit = function () {
		FB.init({
			appId: fbId, //weeloy localhost fb appId
			status: true,
			cookie: true,
			xfbml: true
		});

	}; //end fbAsyncInit

	// Load the SDK Asynchronously
	(function (d) {
		var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
		if (d.getElementById(id)) {
			return;
		}
		js = d.createElement('script');
		js.id = id;
		js.async = true;
		js.src = "//connect.facebook.net/en_US/all.js";
		ref.parentNode.insertBefore(js, ref);
	}(document)); //end loadSDF
});


        </script>


    </body>
</html>

