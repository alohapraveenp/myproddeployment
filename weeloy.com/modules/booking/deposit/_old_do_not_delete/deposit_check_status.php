<?php
    //include 'angular-client/includes/header.php';
    require_once("lib/class.booking.inc.php");
    require_once "lib/class.notification.inc.php";
    require_once("lib/class.restaurant.inc.php");
    require_once("lib/class.qrcode.inc.php");
   
    require_once("lib/class.media.inc.php");
    require_once("lib/class.coding.inc.php");
    require_once('lib/class.paypal.inc.php');
    require_once('lib/class.payment.inc.php');
    require_once("conf/conf.session.inc.php");

    $booking = new WY_Booking();
    $payment = new WY_Payment();
    $logger = new WY_log("website");
        $notification = new WY_Notification();
    $res = new WY_restaurant;
    //localhost:8888/weeloy.com/modules/booking/deposit/deposit_check_status.php?action=checking&resBookingDeposit=30&resCurrency=SGD&restaurant=SG_SG_R_TheOneKitchen&confirmation=TheOnEPVVSPU&email=philippe.benedetti@weeloy.com&tracking=cpp_credit_suisse&city=&title=the-one-kitchen&bkdate=2016-06-30

    $booking = new WY_Booking();
    $payment = new WY_Payment();
    $logger = new WY_log("website");
    $notification = new WY_Notification();
    $res = new WY_restaurant;
    $params = array();
     parse_str(@$_SERVER['QUERY_STRING'], $params);
    $fbId = WEBSITE_FB_APP_ID;
    $paymentShow =false;  
    $showCancel =false;
    $showpending =false;
    $showconfirmation =false;
    $loguserid = (isset($_SESSION['user']['user_id'])) ? $_SESSION['user']['user_id'] : "unknown";
    $baseUrl =__BASE_URL__ ;
    $iswhitelabel = false;

    
    $booking->getBooking($params['confirmation']);
    //var_dump($booking);die;
    $params['action'] = $booking->status;
    
    $params['booking_deposit_id'] = $booking->deposit_id;
    
   if(isset($params['booking_deposit_id']) && isset($params['token']) && isset($params['actionType']) ) {
  
      
        $bkDeposit = $payment->geDepositById($params['booking_deposit_id']); 
        $bkconfirmation = $bkDeposit['object_id'];
        //$logger->LogEvent($_SESSION['user']['user_id'], 1000, 3, $bkconfirmation,'paypal-cancel', date("Y-m-d H:i:s"));
        $booking->getBooking($bkconfirmation); 
        $bkrestaurant =$booking->restaurant;
        $getRest = $res->getRestaurant($booking->restaurant);
        $bookingid = (isset($booking->bookid)) ? $booking->bookid : $bkconfirmation;
        $logger->LogEvent($loguserid, 806, $getRest['ID'], '$bookingid','', date("Y-m-d H:i:s"));
      
        
//    if($booking->status!==''){
        $payment->updatePayStatus($bkconfirmation,$bkDeposit['paykey'],'CANCEL');
        
       
        
        $showCancel = true;
        $typeBooking = ($res->is_bookable && ($res->status == 'active' || $res->status == 'demo_reference')) ? "true " : "false ";
       
        if(preg_match("/walkin|CALLCENTER|tms|waiting|WEBSITE|facebook|GRABZ/i", $booking->tracking)){
            $iswhitelabel = true;
        }
         $isCreditCardInfoActive = ($res->checkCreditCardDetails() > 0) ? 1 : 0;
   }
   
    else if(isset($params['booking_deposit_id']) && isset($params['token']) ) {
       
       
             
    
        $bkDeposit = $payment->geDepositById($params['booking_deposit_id']); 
 
            if($params['paytype']=='paypal'){
                $paypal  = new WY_Paypal();
                $transStatus = $paypal->getPaymentDetails($bkDeposit['paykey']);
            }
  
            if($params['paytype']=='stripe'){
              
               $transStatus = $params['status'];
            }
   
    

            $bkconfirmation = $bkDeposit['object_id'];
            $booking->getBooking($bkconfirmation); 
            $bkrestaurant =$booking->restaurant;
            $getRest = $res->getRestaurant($booking->restaurant);
            $bookingid = (isset($booking->bookid)) ? $booking->bookid : $bkconfirmation;

            $logger->LogEvent($loguserid, 806, $getRest['ID'],$bookingid,'', date("Y-m-d H:i:s"));
            $bktime = $booking->rtime;
            $bkemail = $booking->email;
            $bkmobile = $booking->mobile;
            $bkcover = $booking->cover;
            $bksalutation = $booking->salutation;
            $bkfirst = $booking->firstname;
            $bklast = $booking->lastname;
            $bkcountry = $booking->country;
            $language = $booking->language;
            $bkspecialrequest = $booking->specialrequest;
            $bktitle = $booking->restaurantinfo->title;
            $bktracking =$booking->tracking;
            $bkconfirmation = $booking->confirmation;
            
            $isCreditCardInfoActive = ($res->checkCreditCardDetails() > 0) ? 1 : 0;
             if(preg_match("/walkin|CALLCENTER|tms|waiting|WEBSITE|facebook|GRABZ/i", $booking->tracking)){
                    $iswhitelabel = true;
              }

        if(true || $transStatus==='COMPLETED'){
            
//    if($booking->status!==''){
           

            $updateTrans =$payment->updatePayStatus($bkconfirmation,$bkDeposit['paykey'],$transStatus,$params['paytype']);

            

             $mediadata = new WY_Media($booking->restaurant);

             $logo = $mediadata->getLogo($booking->restaurant);

             $resTitle = $getRest->title;
             $payStatus = $bkDeposit['status'];
             $date = $booking->rdate;
             $time = $booking->rtime;
            $path_tmp = explode(':', get_include_path());

            $qrcode_filename = $path_tmp[count($path_tmp) - 1] . 'tmp/' . $bkconfirmation . '.png';
            
            $qrcode = new QRcode();
            $qrcode->png('{"membCode":"' . $booking->membCode . '"}', $qrcode_filename);
         
            if($updateTrans===1){
             
                  
                $notification->notify($booking, 'booking');
            }
            $paymentShow =true;
            $typeBooking = ($res->is_bookable && ($res->status == 'active' || $res->status == 'demo_reference')) ? "true " : "false ";
            $partialdesc = mb_strimwidth($getRest['description'], 0, 200, "...");
            if ($partialdesc !== "")
                $partialdesc = preg_replace("/\r\n/", "", $partialdesc);

            $resCurrency =$bkDeposit['curency'];
            $resBookingDeposit=$bkDeposit['amount'];
            $showconfirmation =true;
          
            
          
     }else{

         $bkconfirmation = $bkDeposit['object_id'];
         $booking->getBooking($bkconfirmation);  
        

         $typeBooking = ($res->is_bookable && ($res->status == 'active' || $res->status == 'demo_reference')) ? "true " : "false ";
//    if($booking->status!==''){
         $logger->LogEvent($loguserid, 806, $getRest['ID'],$bookingid,'', date("Y-m-d H:i:s"));
         $payment->updatePayStatus($bkconfirmation,$bkDeposit['paykey'],$transStatus);
         $showCancel = true;
     }
        
        
            
     }if(isset($params['action']) && $params['action'] =='pending_payment'){
            $bkconfirmation = $params['confirmation'];
            $booking->getBooking($bkconfirmation); 
            if(preg_match("/walkin|CALLCENTER|tms|waiting|WEBSITE|facebook|GRABZ/i", $booking->tracking)){
                $url =  __BASE_URL__ .'/modules/booking/deposit/deposit_payment_method.php?action=pending_payment&deposit_id='.$bkconfirmation.'&restaurant='.$booking->restaurant;
            }else{
               $url = __BASE_URL__ ."/".$booking->restaurantinfo->internal_path."/".$bkconfirmation."/deposit_pendingpayment_method?action=pending_payment&mode=bklist"; 
            }
            
            $booking->payment_link =$url;
            $notification->notify($booking, 'booking_pending');
            
         
            $getRest = $res->getRestaurant($booking->restaurant);
            $typeBooking = ($res->is_bookable && ($res->status == 'active' || $res->status == 'demo_reference' )) ? "true " : "false ";
            $mediadata = new WY_Media($booking->restaurant);
             $logo = $mediadata->getLogo($booking->restaurant);
            $bkrestaurant =$booking->restaurant;
            $bookingid = (isset($booking->bookid)) ? $booking->bookid : $bkconfirmation;
            $logger->LogEvent($loguserid, 806, $getRest['ID'],$bookingid, 'pending_payment', date("Y-m-d H:i:s"));
            $bktime = $booking->rtime;
            $bkemail = $booking->email;
            $bkmobile = $booking->mobile;
            $bkcover = $booking->cover;
            $bksalutation = $booking->salutation;
            $bkfirst = $booking->firstname;
            $bklast = $booking->lastname;
            $bkcountry = $booking->country;
            $language = $booking->language;
            $bkspecialrequest = $booking->specialrequest;
            $bktitle = $booking->restaurantinfo->title;
            $bktracking =$booking->tracking;
            $bkconfirmation = $booking->confirmation;
            $getRest = $res->getRestaurant($booking->restaurant);
            $isCreditCardInfoActive = ($res->checkCreditCardDetails() > 0) ? 1 : 0;
            $showpending = true;
         
                         
            
         
     }
    $is_listing = false;
    if (empty($res->is_wheelable) || !$res->is_wheelable) {
        $is_listing = true;
    }

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


//header('location: ' . $url);
?>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv='expires' content='mon, 1 jan 1998 05:00:00 gmt'/>
<meta http-equiv='pragma' content='no-cache'/>
<meta http-equiv='pragma' content='cache-control: max-age=0'/>
<meta http-equiv='last-modified' content='mon, 1 jan 1998 05:00:00 gmt'/>
<meta http-equiv='cache-control' content='no-cache, must-revalidate'/>
<meta name='robots' content='noindex, nofollow'/>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
<meta name="HandheldFriendly" content="true" />
<title>Booking Confirmation - Weeloy</title>
<base href="<?php echo __ROOTDIR__; ?>/"/>
<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
<link href="css/bootstrap-select.css" rel="stylesheet" />
<link href="css/bootstrap-social.css" rel="stylesheet" />
<link href="css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<link href="modules/booking/assets/css/bookingform.css?v=1" rel="stylesheet" >



<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type='text/javascript' src="js/angular.min.js"></script>
<script type="text/javascript" src="js/ngStorage.min.js"></script>
<style>


<?php //if ($brwsr_type != "mobile") echo ".mainbox { margin: 10px 30px 20px 1px; width:550px; } ";  ?>

<?php
if(!empty($bktracking) && preg_match("/GRABZ/i", $bktracking)){$bkgcustomcolor = '#000000';}
// booking customization
if (!empty($bkgcustomcolor)) {

echo <<<DELIMITER
h1 { background-color: $bkgcustomcolor; }
.panel { border-color: $bkgcustomcolor !important; }
.panel-heading { background-color: $bkgcustomcolor !important; border-color: $bkgcustomcolor !important; }
.caret { color: $bkgcustomcolor; }
@media (min-width: 530px) {
	.separation { border-right: 4px solid $bkgcustomcolor; height: 100%; }
	.separation-left { border-left: 4px solid $bkgcustomcolor; }
}
DELIMITER;
		}
		?>

</style>

</head>
<body ng-app="myApp"  style="background: transparent none repeat scroll 0 0;">
 

<!-- tracking code for GTM -->

<?php
$isTest = false;
if (strpos($bkspecialrequest, 'test') !== false) {
	$isTest = true;
}
if (strpos($bkfirst, 'test') !== false) {
	$isTest = true;
}
if (strpos($bklast, 'test') !== false) {
	$isTest = true;
}
if (strpos($bkrestaurant, 'SG_SG_R_TheFunKitchen') !== false) {
	$isTest = true;
}
if (strpos($bkrestaurant, 'SG_SG_R_TheOneKitchen') !== false) {
	$isTest = true;
}
if (!$isTest && $showCancel===false && $showpending ===false) {
	?>
	<script>

				dataLayer = [];
                                
				dataLayer.push({
					'pax': <?php echo $bkcover; ?>,
					'reservation_id': '<?php echo $bkconfirmation; ?>',
					'reserved_by': '<?php
if (isset($_SESSION['user']['email']))
	echo 'user';
else
	echo 'guest';
?>',
					'reserver_name': '<?php echo $bkfirst . ' ' . $bklast; ?>',
					'reserver_loginid': '<?php echo $bkemail; ?>',
					'reservation_date': '<?php echo $bkdate; ?>',
					'reservation_time': '<?php echo $bktime; ?>',
					'restaurant_name': '<?php echo $bktitle; ?>',
					'restaurant_id': '<?php echo $bkrestaurant; ?>',
					'tracking': '<?php echo $bktracking; ?>'
				});
	</script>
<?php } ?>

<?php include_once("ressources/analyticstracking.php") ?>
<?php include_once("_bkdepositform.php") ?>


<script>

<?php
echo "var confirmation = '" . $booking->confirmation . "';";
echo "var tracking = '" . $booking->tracking . "';";
echo "var hoteltitle = '" . $booking->restaurantinfo->title . "';";
echo "var restaurant = '$booking->restaurant';";
echo "var typeBooking = '" . $typeBooking . "';";
echo "var brwsr_type = '$brwsr_type';";
//
echo "var imglogo = '$logo';";
echo "var pproduct = '$booking->product';";
echo "var pdate = '$booking->rdate';";
echo "var ptime = '$booking->rtime';";
echo "var pcover = '$booking->cover';";
echo "var pfirst = '$booking->firstname';";
echo "var plast = '$booking->lastname';";
echo "var pemail = '$booking->email';";
echo "var pmobile = '$booking->mobile';";
echo "var prequest = '$booking->specialrequest';";
echo "var langue = '$booking->language';";
echo "var topic = 'BOOKING';";
echo "var partialpath = '" . $getRest['internal_path'] . "';";
echo "var partialdesc = '" . $partialdesc . "';";
echo "var fbId = '" . $fbId . "';";
echo "var mcode = '" . $booking->membCode . "';";
echo "var resBookingDeposit ='".$bkDeposit['amount']. "';";
echo "var resCurrency='".$bkDeposit['curency']. "';";
echo "var isCancel = '" . $showCancel . "';";
echo "var transaction_id = '$booking->deposit_id';"; 
echo "var iswhitelabel = '" . $iswhitelabel . "';"; 
echo "var isCreditCardInfoActive = '" .$isCreditCardInfoActive . "';";
echo "var showconfirmation = '" . $showconfirmation . "';";
echo "var showpending = '" . $showpending . "';";

  $resCurrency =$bkDeposit['curency'];
        $resBookingDeposit=$bkDeposit['amount'];
printf("var flgsg = %s;", (!preg_match('/CALLCENTER|WEBSITE|facebook|GRABZ/', $bktracking)) ? 'true' : 'false');
printf("var is_listing = %s;", $is_listing ? 'true' : 'false');
?>

var app = angular.module("myApp", ['ngStorage']);

app.controller('MainController', function ($scope, $http, $locale, $timeout, $localStorage) {
$scope.showCancel =false;
		//angular.copy(locales['en'], $locale);
		if(resBookingDeposit !== undefined) {
			$scope.resBookingDeposit = resBookingDeposit;
			$scope.resCurrency = resCurrency;
			$scope.hasDeposit = true;
		}
                if(isCancel==='1'){
                    $scope.showCancel = true;
                }
                if(showconfirmation ==='1'){
                    $scope.showconfirmation = true;
                }
                if(showpending ==='1'){
                   $scope.showpending = true;
                }
               
             
               $scope.transactionid = transaction_id;

		$scope.restaurant = restaurant;
		$scope.langue = langue;
		$scope.pcheckmark = ($scope.langue != 'cn') ? 'pcheckmark' : 'pcheckmarkcn';
		$scope.imglogo = imglogo;
		$scope.brwsr_type = brwsr_type;
		$scope.hoteltitle = hoteltitle;
		$scope.confirmation = confirmation;
		$scope.flgsg = flgsg;
		$scope.spincode = (flgsg && mcode != '' && mcode.length == 4 && mcode != '0000') ? '(spin:' + mcode + ')' : '';

		$scope.bookingtitlecf = (typeBooking) ? "BOOKING CONFIRMED" : "REQUESTED CONFIRMED";
		$scope.confirmsg = (typeBooking) ? "Your booking is confirmed at " : "Your request has been sent to ";
                $scope.confirmsg5 = (typeBooking) ? "Your booking is pending at " : "Your request has been sent to ";

		$scope.confirmsg2 = (typeBooking) ? "We are happy to confirm your reservation" : "Your reservation number is ";
		$scope.confirmsg3 = "You will receive the details of your reservation by email and SMS";
		$scope.confirmsg4 = "You can sign in using your email to view all your bookings and leave your reviews";
		$scope.bacchanalia = "Your table has been blocked, we are sending you an e-mail to reconfirm the booking";
		$scope.listTags0 = "Free Booking";
		$scope.listTags1 = "";
		$scope.listTags2 = (typeBooking) ? "Instant Reservation" : "No reservartion";
		$scope.listTags3 = (typeBooking) ? "" : "First come, first serve";
		$scope.listTags4 = (!is_listing) ? "Win" : "Enjoy incredible";
		$scope.listTags5 = (!is_listing) ? "for every bookings" : " promotion";
               
                $scope.pproduct = pproduct;
		$scope.pdate = pdate;
		$scope.ptime = ptime;
		$scope.pcover = pcover;
		$scope.pfirst = pfirst;
		$scope.plast = plast;
		$scope.pemail = pemail;
		$scope.pmobile = pmobile;
		$scope.prequest = prequest;
		$scope.pname = pfirst + ' ' + plast;
                $scope.iswhitelabel = (iswhitelabel==='1') ? true : false;
                $scope.isCreditCardInfoActive = isCreditCardInfoActive;
                 
                 
         
                $scope.cancelpolicy = function () {
                    var payment_method =($scope.isCreditCardInfoActive === 1) ?'carddetails' : "credit card" ;
              
            
                    return $http.post("api/services.php/cancelpolicy/list",
                            {
                                'restaurant':$scope.restaurant,
                                'type': payment_method,
                                'amount':$scope.resBookingDeposit,
                                'product':$scope.pproduct
                            }).then(function (response) {
                        var data = response.data.data;

                        if (typeof data !== 'undefined') {
                            $scope.tnc = data.message;
                            $scope.cancelpolicy = data.range;
                           $scope.freerange = data.lastRange;
                        }
                    });

                };
              
              $scope.cancelpolicy();
               //change the date formate
                var date = new Date($scope.pdate);
                var dobArr = date.toDateString().split(' ');
                var rdate = dobArr[2] + ' ' + dobArr[1] + ' ' + dobArr[3];
                $scope.pdate =rdate;
//                var rdate = $scope.pdate.split('-');
//                $scope.pdate = rdate[0]+"/"+rdate[1]+"/"+rdate[2];
                //rdate[0]), parseInt(rdate[1] - 1), parseInt(rdate[2]), rtime[0], rtime[1]

		$scope.langdata = $localStorage.langdata;
		if (typeof $scope.langdata !== "undefined" && $scope.langdata.version !== "undefined" && $scope.langdata.version.lb !== "undefined") {
			if ($scope.langdata.version.lb == restaurant)
				$scope.zang = $scope.langdata;
		}

		if (typeof $scope.zang === "undefined")
			$scope.zang = {
				'datetag': {lb: "Date", vl: "Date", f: "c"}, 'timetag': {lb: "time", vl: "time", f: "c"}, 'lunchtag': {lb: "lunch", vl: "Lunch", f: "c"}, "dinnertag": {"lb": "dinner", "vl": "Dinner", f: "c"}, "guesttag": {"lb": "number of guests", "vl": "number of guests", f: "c"}, "titletag": {"lb": "title", "vl": "Title", f: "c"}, "firsttag": {"lb": "firstname", "vl": "First name", f: "c"}, "lasttag": {"lb": "lastname", "vl": "Last name", f: "c"}, "nametag": {"lb": "name", "vl": "Name", f: "c"}, "emailtag": {"lb": "email", "vl": "Email", f: "c"}, "mobiletag": {"lb": "mobile", "vl": "Mobile", f: "c"}, "requesttag": {"lb": "special request", "vl": "Special request", f: "c"},
				'bookingdetail': {lb: "booking details", vl: "booking details", f: "u"},
				'personaldetail': {lb: "personal details", vl: "personal details", f: "u"},
				'buttonmodify': {lb: "modify", vl: "modify", f: "n"},
				'confinfo': {lb: "Please confirm your information", vl: "Please confirm your information", f: "c"},
				'buttonbook': {lb: $scope.buttonLabel, vl: $scope.buttonLabel, f: "u"},
				'bookingtitlecf': {lb: $scope.bookingtitlecf, vl: $scope.bookingtitlecf, f: "u"},
				'confirmsg': {lb: $scope.confirmsg, vl: $scope.confirmsg, f: "n"},
				'confirmsg2': {lb: $scope.confirmsg2, vl: $scope.confirmsg2, f: "n"},
				'confirmsg3': {lb: $scope.confirmsg3, vl: $scope.confirmsg3, f: "n"},
				'confirmsg4': {lb: $scope.confirmsg4, vl: $scope.confirmsg4, f: "n"},
				'bacchanalia': {lb: $scope.bacchanalia, vl: $scope.bacchanalia, f: "n"},
				'timeouttag': {lb: "timeout", vl: "timeout", f: "u"},
				'listtag1': {lb: "free booking", vl: "free booking", f: "c"},
				'listtag2': {lb: $scope.listTags2, vl: $scope.listTags2, f: "c"},
				'listtag3': {lb: $scope.listTags3, vl: $scope.listTags3, f: "c"},
				'listtag4': {lb: $scope.listTags4, vl: $scope.listTags4, f: "c"},
				'listtag5': {lb: $scope.listTags5, vl: $scope.listTags5, f: "n"}
			};

		$scope.checkmark = [{"label1": $scope.zang['listtag1'].vl, "label2": " ", "glyph": true}, {"label1": $scope.zang['listtag2'].vl, "label2": $scope.zang['listtag3'].vl, "glyph": true}, {"label1": $scope.zang['listtag4'].vl, "label2": $scope.zang['listtag5'].vl, "glyph": true}];
		if (flgsg == false)
			$scope.checkmark.pop(); // don't show "win everytime
		$scope.bookinginfo = [{"label": "divider", "value": $scope.zang['bookingdetail'].vl}, {"label": $scope.zang['datetag'].vl, "value": $scope.pdate}, {"label": $scope.zang['timetag'].vl, "value": $scope.ptime}, {"label": $scope.zang['guesttag'].vl, "value": $scope.pcover}, {"label": "divider", "value": $scope.zang['personaldetail'].vl}, {"label": $scope.zang['nametag'].vl, "value": $scope.pname}, {"label": $scope.zang['emailtag'].vl, "value": $scope.pemail}, {"label": $scope.zang['mobiletag'].vl, "value": $scope.pmobile}, {"label": $scope.zang['requesttag'].vl, "value": $scope.prequest}];
		$scope.fbshareclik = function () {

			var fbObj = fbshare();
			FB.ui(fbObj, function (response) {
				console.log(JSON.stringify(response));
			});

		}
		function fbshare() {
			var obj = {
				method: 'feed',
				link: "https://www.weeloy.com/" + partialpath,
				picture: $scope.imglogo,
				name: $scope.hoteltitle,
				caption: 'New booking at ' + $scope.hoteltitle + '. Now enjoy rewards with Weeloy',
				description: partialdesc,
				display: 'popup'
			};
			return obj;

		}
		function sortMethod(a, b) {
			var x = a.name.toLowerCase();
			var y = b.name.toLowerCase();
			return ((x < y) ? -1 : ((x > y) ? 1 : 0));
		}


	});

	String.prototype.capitalize = function () {
		return this.replace(/^./, function (match) {
			return match.toUpperCase();
		});
	};

	function openWin(type) {
		var arg = <?php echo "'$QRCodeArg'"; ?>;
		window.open('modules/qrcode/QRCode.htm?type=' + type + arg, 'QRCode', 'toolbar=no, scrollbars=no, resizable=no, top=30, left=30, width=330, height=330');
	}

</script>


    </body>
</html>


