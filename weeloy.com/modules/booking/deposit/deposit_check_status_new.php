<?php
require_once("conf/conf.init.inc.php");
require_once("lib/wpdo.inc.php");
require_once('conf/conf.session.inc.php');
require_once("lib/class.booking.inc.php");
require_once("lib/class.restaurant.inc.php");


$booking = new WY_Booking();
$res = new WY_restaurant;
if (isset($_GET['action'])) {
    $resCurrency = $_GET['resCurrency'];
    $email = $_GET['email'];
    $resBookingDeposit = $_GET['resBookingDeposit'];
    $restaurant = $_GET['restaurant'];
    $confirmation = $_GET['confirmation'];
    $city = $_GET['city'];
    $title = $_GET['title'];
    $bktracking = $_GET['tracking'];
    $paymentType = $_GET['stripetype'];
    $bkdate = $_GET['bkdate'];
    $getRest = $res->getRestaurant($restaurant); 
    $booking->getBooking($confirmation);
    //$res->getRestaurant($restaurant);
    //$logger->LogEvent($loguserid, 805, $res->ID, $booking->bookid,'', date("Y-m-d H:i:s"));
}

// var_dump($booking->status);
//echo "<script>$('#booking_form').submit();</script>";

//count number of trial payment page

//$pagestatus ='';
//
//if(isset($_COOKIE['page'.$confirmation])){
//    $page = $_COOKIE['page'.$confirmation];
//    $pagestatus = 'payment_page';
// unset($_COOKIE['page'.$confirmation]);
////
// setcookie('page'.$confirmation,null, -1, "/"); 
//    
//}
 


if ($booking->status == 'pending_payment' && empty($booking->deposit_id)) {
    $state = $booking->status;
} else {
    if (!empty($booking->deposit_id) && $booking->status == '') {
        $state = 'confirmed';
        //      echo "<script>$('#booking_form').submit();</script>";
    }
}


$bkgcustomcolor = "";
if (!empty($res->bkgcustomcolor) && preg_match("/website|facebook/i", $bktracking)) {
    if (strlen($res->bkgcustomcolor) > 3 && preg_match("/\|/", $res->bkgcustomcolor)) {
        $tt = explode("|", $res->bkgcustomcolor);
        if (strlen($tt[0]) > 2) {
            $bkgcustomcolor = $tt[0];
        }
    }
}
//$booking->getBooking($confirmation);
//var_dump($booking);die;
?>


<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta http-equiv='expires' content='mon, 1 jan 1998 05:00:00 gmt'/>
        <meta http-equiv='pragma' content='no-cache'/>
        <meta http-equiv='pragma' content='cache-control: max-age=0'/>
        <meta http-equiv='last-modified' content='mon, 1 jan 1998 05:00:00 gmt'/>
        <meta http-equiv='cache-control' content='no-cache, must-revalidate'/>
        <meta name='robots' content='noindex, nofollow'/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
        <meta name="HandheldFriendly" content="true" />
        <title>Booking Confirmation - Weeloy</title>
        <base href="<?php echo __ROOTDIR__; ?>/"/>
        <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
        <link href="css/bootstrap-select.css" rel="stylesheet" />
        <link href="css/bootstrap-social.css" rel="stylesheet" />
        <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="modules/booking/assets/css/bookingform.css?v=1" rel="stylesheet" >

            <script type="text/javascript" src="js/jquery.min.js"></script>
            <script type="text/javascript" src="js/bootstrap.min.js"></script>
            <script type='text/javascript' src="js/angular.min.js"></script>
            <script type="text/javascript" src="js/ngStorage.min.js"></script>
            <style>


                <?php //if ($brwsr_type != "mobile") echo ".mainbox { margin: 10px 30px 20px 1px; width:550px; } ";   ?>

                <?php
                if(!empty($bktracking) && preg_match("/GRABZ/i", $bktracking)){$bkgcustomcolor = '#000000';}
// booking customization
                if (!empty($bkgcustomcolor)) {

                    echo <<<DELIMITER
h1 { background-color: $bkgcustomcolor; }
.panel { border-color: $bkgcustomcolor !important; }
.panel-heading { background-color: $bkgcustomcolor !important; border-color: $bkgcustomcolor !important; }
.caret { color: $bkgcustomcolor; }
@media (min-width: 530px) {
	.separation { border-right: 4px solid $bkgcustomcolor; height: 100%; }
	.separation-left { border-left: 4px solid $bkgcustomcolor; }
}
DELIMITER;
                }
                ?>
                img{
                    display:inline;   
                    height: auto;
                    margin: 0;
                    max-height: none;
                    max-width: none;
                    padding-bottom: 0px;
                    width: auto;
                }
            </style>
    </head>
    <body style="background: transparent none repeat scroll 0 0;">
        <form id="booking_form" method="POST" action="modules/booking/deposit/deposit_confirmation_confirmed.php">
            <input type="hidden" name='bkconfirmation' value="<?php echo $booking->confirmation ?>" />
            <input type="hidden" name='bkrestaurant' value="<?php echo $booking->restaurant ?>" />
            <input type="hidden" name='bklangue' value="<?php echo $booking->langue ?>" />
            <input type="hidden" name='bksalutation' value="<?php echo $booking->salutation ?>" />
            <input type="hidden" name='bklast' value="<?php echo $booking->lastname ?>" />
            <input type="hidden" name='bkfirst' value="<?php echo $booking->firstname ?>" />
            <input type="hidden" name='bkemail' value="<?php echo $booking->email ?>" />
            <input type="hidden" name='bkcover' value="<?php echo $booking->cover ?>" />
            <input type="hidden" name='bkdate' value="<?php echo $booking->rdate ?>" />
            <input type="hidden" name='bktime' value="<?php echo $booking->rtime ?>" />
            <input type="hidden" name='bkmobile' value="<?php echo $booking->mobile ?>" />
            <input type="hidden" name='bkcountry' value="<?php echo $booking->country ?>" />
            <input type="hidden" name='bkspecialrequest' value="<?php echo $booking->specialrequest ?>" />
            <input type="hidden" name='booking_deposit_id' value="<?php echo $booking->deposit_id ?>" />
            <input type="hidden" name='resBookingDeposit' value="<?php echo $booking->deposit_id ?>" />
            <input type="hidden" name='resCurrency' value="<?php echo $booking->currency ?>" />
            <input type="hidden" name='actionType' value="<?php echo $booking->currency ?>" />

            <input type="hidden" name='bkproduct' value="<?php echo $booking->product ?>" />
        </form>
        <div id='booking' ng-controller='MainController'  >
            <div class="container mainbox">   

                <div class="panel panel-info" ng-if='showCancel' >
                    <div class="panel-heading" >
                        <div class="panel-title" style='width:300px;'>Thank you for booking at Burnt Ends</div>
                    </div>     
                    <div class="panel-body" >
                        <p class="slogan">You booking is pending.</p>
                        <p class="slogan">We are waiting for your payment/credit card details transaction has been.</p>

                        <div style="width:100%; text-align: center">
                            <div  >
                                <img style="  padding: 10px 10px 24px;" src="modules/booking/assets/images/spinner_50.gif"/>
                            </div>
                            <p style="float:right;font-size:12px;" > Automatic reload every 5 seconds</p> 
                            <!--If you prefer manual refresh, use this button.-->
                            <!--<a  style="float:right" id='reload' href="#" onClick="window.location.reload();return false;"><img style="width:25px; padding: 0;margin: 0;" src='modules/booking/assets/images/reload.jpeg'></img></a>-->
                        </div>

                    </div>
                </div>
            </div>
        </div>                     

        <?php
        
        if ($state == 'confirmed') {
            echo "<script>$('#booking_form').submit();</script>";
        }
      
        ?>
        <script type="text/javascript">

        </script>
            <script>
                    var time = new Date().getTime();
                    $(document.body).bind("mousemove keypress", function (e) {
                        time = new Date().getTime();
                    });

                    function refresh() {
                        if (new Date().getTime() - time >= 600)
                            window.location.reload(true);
                        else
                            setTimeout(refresh, 600);
                    }

                    setTimeout(refresh, 5000);
        </script>
    </body>
</html>



