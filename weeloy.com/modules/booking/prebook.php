<?php
require_once("conf/conf.init.inc.php");
require_once("lib/wpdo.inc.php");
require_once("lib/wglobals.inc.php");
require_once('conf/conf.session.inc.php');
require_once("lib/class.media.inc.php");
require_once("lib/class.restaurant.inc.php");
require_once("lib/class.booking.inc.php");
require_once("lib/class.promotion.inc.php");
require_once("lib/class.analytics.inc.php");
require_once("lib/class.coding.inc.php");
require_once("lib/class.member.inc.php");
    $params = array();
    parse_str(@$_SERVER['QUERY_STRING'], $params);
    

if(isset($params['booking_deposit_id']) && isset($params['token'])) {
 
    $sql = "SELECT * FROM booking_deposit WHERE id = \"{$params['booking_deposit_id']}\" AND token = \"{$params['token']}\"";
    $db = getConnection();
    $stmt = $db->prepare($sql);
    $stmt->execute();
    $deposit = $stmt->fetch(PDO::FETCH_OBJ);
    $action = __BASE_URL__ . '/modules/booking/confirmation.php';
    
    if(isset($deposit)) {
        ?>
        <script>
         window.onload = function() {
            var data = <?php echo $deposit->data ?>; 
       
           data.booking_deposit_id = "<?php echo $params['booking_deposit_id'] ?>";
            console.log(data.booking_deposit_id);
            var form = document.createElement('form');
            form.method = 'POST';
            form.action = '<?php echo $action ?>';
        
            for(var key in data) {
                 var input = document.createElement('input');
                 input.type = 'hidden';
                 input.name = key;
                 input.value = data[key];
                 form.appendChild(input);
            }
            console.log(form);
            form.submit();
        }
        </script>
        <?php
    }
    die();
}

$IPaddr = ip2long($_SERVER['REMOTE_ADDR']);

$url_get_parameters = "";
$arglist = array('bklangue', 'bksalutation', 'bklast', 'bkfirst', 'bkemail', 'bkcover', 'bkdate', 'bktime', 'bkmobile', 'bkcountry', 'bkrestaurant', 'bktitle', 'bkparam', 'bkspecialrequest', 'brwsr_type', 'bkpage', 'bktracking', 'data', 'booker', 'company', 'hotelguest', 'babychair', 'signed_request', 'bkextra', 'bkchild', 'mealtype',  'booking_deposit_id', 'resBookingDeposit','bkproduct', 'bkproductid', 'bkpage1', 'restaurantselected');
foreach ($arglist as $label) {
    if (!isset($_REQUEST[$label]))
        $_REQUEST[$label] = "";
    $$label = $_REQUEST[$label];
    $$label = preg_replace("/\'|\"/", "’", $$label);
    $$label = preg_replace("/\s+/", " ", $$label);
    
    $url_get_parameters .= '&'.$label.'=' . $$label;
}


$restaurantselected = 1; // always, this is for modification

$bkpage = intval($bkpage) + 1;

$fullfeature = preg_match("/TheFunKitchen/", $bkrestaurant) || preg_match("/TheOneKitchen/", $bkrestaurant) || in_array($_SESSION['user']['member_type'], $fullfeature_member_type_allowed) || ($_SESSION['user']['member_type'] == 'weeloy_sales' && $res->status == 'demo_reference' || $res->status == 'active');

if (empty($bkrestaurant)) {
    header("location: https://www.weeloy.com/");
    exit;
}

$res = new WY_restaurant;
$res->getRestaurant($bkrestaurant);
$optinbkg = $res->optinBkg();
$resCountry = $res->country;
 $isCreditCardInfoActive = ($res->checkCreditCardDetails() > 0) ? 1 : 0;
$errmsg = "";
if($res->checkduplicateBkg()) {
	$bkg = new WY_Booking();
	$bkg->duplicate($bkrestaurant, $bkdate, $bkemail, $bkmobile);
	if($bkg->result > 0) {
		$errmsg = "You already have a booking (" . $bkg->confirmation . ") for the same day, at " . substr($bkg->rtime, 0, 5) . " for " . $bkg->cover . " persons";
		}
	}

if (empty($res->restaurant)) {
    header("location: https://www.weeloy.com");
    exit;
}
$payDeposit =0;
// if the booking session is valid and not over 600s

if($bkpage1 == 'booking_form_section'){
    $url_parameters = base64_encode( substr($url_get_parameters, 1));
    $action = __ROOTDIR__ . "/modules/booking/book_form_section.php?bkrestaurant=$bkrestaurant&p=$url_parameters";

}else{
    $action = __ROOTDIR__ . "/modules/booking/book_form.php";
}

$hreftnc = __ROOTDIR__ . "/modules/templates/tnc/fullterms.php?bkrestaurant=$bkrestaurant&bktracking=$bktracking";
if(preg_match("/website|facebook/i", $bktracking)) {
	$hreftnc = __ROOTDIR__ . "/modules/templates/tnc/privatetnc.php?bkrestaurant=$bkrestaurant&bktracking=$bktracking";
	}
	
$bkparamdecoded = detokenize($bkparam);
if (substr($bkparamdecoded, 0, 3) != date('M') || microtime(true) - floatval(substr($bkparamdecoded, 3)) > 600){
    if($bkpage1 != 'booking_form_section'){
        header("Location: " . $action . "?bkrestaurant=" . $bkrestaurant . "&bktitle=" . $bktitle . "&bktracking=" . $bktracking . "&data=" . $data . "&timeout=y");    
    }else{
        header("Location: " . $action . "&timeout=y");
    }
}

$mediadata = new WY_Media();
$logo = $mediadata->getLogo($bkrestaurant);


$llorder = ($res->lastorderlunch != "12:00") ? $res->lastorderlunch : "";
$ldorder = ($res->lastorderdiner != "18:00") ? $res->lastorderdiner : "";
$bkinfo = $bkcritical = "";
if($res->bookinfo != "") $bkinfo = preg_replace("/\r|\n/", "|||", $res->bookinfo);
if($res->bookcritical != "") $bkcritical = preg_replace("/\r|\n/", "|||", $res->bookcritical);

if($res->deposit_cancel_tnc  != "") $bkinfo .= '||||||'.preg_replace("/\r|\n/", "|||", $res->deposit_cancel_tnc);


$typeBooking = ($res->is_bookable && ($res->status == 'active' || $res->status == 'demo_reference')) ? "true " : "false ";
$is_listing = false;
if (empty($res->is_wheelable) || !$res->is_wheelable) {
    $is_listing = true;
}

if(isset($_POST['resBookingDeposit']) && $_POST['resBookingDeposit'] != ''){
        $payDeposit = 57;
    }

$bkgcustomcolor = "";
if (!empty($res->bkgcustomcolor) && preg_match("/website|facebook/i", $bktracking)) {
	$bkgcustomcolor = $res->bkgcustomcolor;
}

$logger = new WY_log("website");
$loguserid = (isset($_SESSION['user']['user_id'])) ? $_SESSION['user']['user_id'] : "unknown";
$logger->LogEvent($loguserid, 802, $res->ID,'' , '', date("Y-m-d H:i:s"));

?>


<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv='expires' content='mon, 1 jan 1998 05:00:00 gmt'/>
<meta http-equiv='pragma' content='no-cache'/>
<meta http-equiv='pragma' content='cache-control: max-age=0'/>
<meta http-equiv='last-modified' content='mon, 1 jan 1998 05:00:00 gmt'/>
<meta http-equiv='cache-control' content='no-cache, must-revalidate'/>
<meta name='robots' content='noindex, nofollow'/>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
<meta name="HandheldFriendly" content="true" />
<title>Verify booking information - Weeloy</title>

<base href="<?php echo __ROOTDIR__; ?>/" />

<link href="client/bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="client/bower_components/bootstrap/dist/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
<link href="client/bower_components/fontawesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<link href="css/bootstrap-formhelpers.min.css" rel="stylesheet" media="screen" />
<link href="modules/booking/assets/css/bookingform.css?v=1" rel="stylesheet" >

<script src="client/bower_components/jquery/dist/jquery.min.js" type='text/javascript'></script>
<script src="client/bower_components/angular/angular.min.js" type='text/javascript'></script>
<script src="client/bower_components/bootstrap/dist/js/bootstrap.min.js" type='text/javascript'></script>
<script src="js/ngStorage.min.js" type='text/javascript'></script>
<script src="client/bower_components/angulartics/src/angulartics.js" type='text/javascript'></script>
<script src="client/bower_components/angulartics/src/angulartics-gtm.js" type='text/javascript'></script>
<style>

.waitprogress { cursor: wait !important;  }

<?php
        if (!empty($bkgcustomcolor)) {
		include_once('book_custom.php');
            }
?>

<?php //if ($brwsr_type != "mobile") echo ".mainbox { margin: 10px 30px 20px 1px; width:550px; } "; ?>

</style>

<script>
var app = angular.module("myApp", [
	'ngStorage',
	'angulartics',
	'angulartics.google.tagmanager']);
</script>

<?php include_once("ressources/analyticstracking.php") ?>
</head>
    <body ng-app="myApp"  style="background: transparent none repeat scroll 0 0;" class="ng-cloak">
        <?php if($payDeposit == 57) {?>
         <form class='form-horizontal' action='modules/booking/deposit/deposit_payment_method.php' role='form' id='book_form' name='book_form' method='POST'   enctype='multipart/form-data'> 
        <?php }else {?>
        <form class='form-horizontal' action='modules/booking/confirmation.php' role='form' id='book_form' name='book_form' method='POST'  enctype='multipart/form-data'> 
        <?php }?>
         
            <?php
            reset($arglist);
            while(list($index, $label) = each($arglist))
            printf("<input type='hidden' id='%s' name='%s' value='%s'>", $label, $label, $$label);
            ?>

            <div id='booking' ng-controller='MainController' ng-init=" ;" >

                <div class="modal fade" id="remoteModal" tabindex="-1" role="dialog" aria-labelledby="remoteModalLabel" aria-hidden="true" >  
                    <div class="modal-dialog" >  
                        <div class="modal-content"></div>  
                    </div>  
                </div>  

                <div class="container mainbox">    
                    <div class="panel panel-info" ng-show="showprebook" >
                        <div class="panel-heading">
                            <div class="panel-title" >{{ zang['bookingtitle'].vl | uppercase }} </div>
                        </div>     

                        <p class="slogan" ng-if="display_checkmarks">
                            <strong>{{slogan}}</strong>
                        </p>
                        
                        <p class="slogan" style="font-size:16px" ng-if="!display_checkmarks">
                            <strong>{{zang['step'].vl }} 2/3 - {{zang['slogan'].vl }}</strong>
                        </p>
                        <p class="slogan"ng-if="!display_checkmarks">{{zang['slogan_sub'].vl }}</p>
                        
			<div ng-if="errmsg!=''" class="alert alert-danger"> <strong>Duplicates!</strong> {{ errmsg }}</div>
                        <div class="panel-body" >
                            <div class='left-column'>
                                <img ng-src='{{imglogo}}' height='120px' id='theLogo' name='theLogo'>
                                    <div class="book-form-lg">
                                        
                                        <table width='90%' ng-if="display_checkmarks">
                                            <tr ng-repeat="x in checkmark">
                                                <td>
                                                    <span class='glyphicon glyphicon-ok checkmark'></span> 
                                                    <br />&nbsp;
                                                </td>
                                                <td ng-class='pcheckmark'>
                                                    <span ng-bind="x.label1"></span>
                                                    <span ng-bind="x.label2"></span> 
                                                    <br />&nbsp;
                                                </td>
                                        </table>
                                    </div>
                            </div>

                            <div class='right-column separation-left no-mobile'>
                                <div style="margin-left:3px;">
                                    <div class="modal fade" id="remoteModal" tabindex="-1" role="dialog" aria-labelledby="remoteModalLabel" aria-hidden="true">  
                                        <div class="modal-dialog" style="background-color: #fff">  
                                            <div class="modal-content" style='font-size:11px;'></div>  
                                        </div>  
                                    </div>  
                                    <div ng-repeat="x in bookinginfo" class='row'>
                                        <div class="input-group special-request-span" ng-if="x.value != '' && x.label != 'divider'">
                                            <span > {{x.label}} : {{x.value}} </span>
                                    </div>
                                        <h1 ng-if="x.label == 'divider'" ng-bind="x.value | uppercase"></h1>
                                    </div>
                                    
<!--                                    <div ng-if="hasDeposit && cancelpolicy.length>0">
                                        <h1 ng-bind="zang['depositdetail'].vl | uppercase"></h1>
                                       
                                        <div  ng-repeat ="p in cancelpolicy track by $index">
                                            <p ng-if="p['percentage']==100">{{p['duration']}}SGD {{resBookingDeposit}}. 
                                            <p ng-if="p['percentage']==0">{{p['duration']}}</p>
                                            <p ng-if="p['percentage']!=0 && p['percentage']!=100">{{p['duration']}} </p>
                                        </div>
                                        
                                    </div>-->
                                    
                                    
                                </div>
                            </div> 
                             <div ng-if="hasDeposit">
                                <input type="hidden" name="resBookingDeposit" value="{{resBookingDeposit}}">
                                <input type="hidden" name="resCurrency" value="{{resCurrency}}">
                             </div>
                          
                            <div style="clear:both"></div>


                                <div style="margin-top: 20px">
                                    <pre ng-if="bkcritical!=''" style="font-size: 11px;color:red">{{bkcritical}} </pre>

                                    <table width='100%'>
                                        <tr>
                                            <td align='left'> <a href ng-click="submitmodify();" >{{zang['buttonmodify'].vl}}</a></td>
                                            <td align='right'>
                                                <a href ng-click="submitonce();" class="book-button-sm btn-leftBottom-orange" ><span ng-if="!hasDeposit">{{zang['buttonconfirm'].vl}}</span><span ng-if="hasDeposit">CONTINUE</span> </a>
<!--                                            <a ng-click="depositCheckout()"   class="book-button-sm btn-leftBottom-orange" ng-if="hasDeposit"> Pay the deposit  </a>-->
                                            </td>

                                        </tr>
                                    </table>
                                    <br />
                                    <span class="ttc" ng-if='flgcc'>{{zang['tmc1'].vl}}   {{zang['canceltmc'].vl}}
                                        <a href= "<?php echo $hreftnc; ?>" id='buttonterms' data-toggle="modal" data-target="#remoteModal" >{{zang['tmc2'].vl}} <span ng-if="hasDeposit"> & {{canceltmc}}</span></a>
                                    </span>
                                    
                                    <div class="checkbox ttc" ng-if='optin'>
                                        <hr />
                                        <label><input type="checkbox" ng-value='1' id='optin' name='optin' ng-model="optininput">{{ optintext}}</label>
                                    </div>
                             </div>
                            
                            <div ng-if="bkinfo[0] !== '' || ldorder !== '' || llorder !== ''" class="termsconditions termsconditions-confirmation">
                                <h1 ng-if="bkinfo !== ''">{{zang['restau_conditions'].vl}}</h1>
                                <p ng-if="llorder !== ''" >
                                        <span class='glyphicon glyphicon-ok checkmark'></span>
                                        <span class="pcheckmark"> {{lorderlunch}} {{ llorder}}</span>
                                    </p> 

                                    <p ng-if="ldorder !== ''">
                                        <span class='glyphicon glyphicon-ok checkmark'></span>
                                        <span class="pcheckmark"> {{lorderdiner}} {{ ldorder}}</span>
                                    </p>

                                <p ng-if="bkinfo[0] !== ''" ng-repeat="bki in bkinfo track by $index">
                                    {{bki}}
                                    </p>
<!--                                    <h6 ng-if="hasDeposit && cancelpolicy.length > 0" >Restaurant Booking Deposit Refund Policy</h6>
                                     <p ng-if="hasDeposit && freerange!=''">Cancellations can be made free until {{freerange}} before the arriving timing. Cancellations made after the time period below will be charged the following fee:  </p>
                                    <p  ng-if="hasDeposit" ng-repeat ="p in cancelpolicy" style="font-size: 11px;">
                                        {{p['duration']}} :  &nbsp;&nbsp;<strong>{{p['percentage']}} </p>
                            
                                 <p ng-if="hasDeposit && tnc!=''" style="font-size: 11px;">{{tnc}} </p>-->
                                </div> 
                            
<!--                                  <p ng-if="bkinfo[0] !== ''" ng-repeat="bki in bkinfo">{{bki}}</p>-->
                                   
                                </div>                     

                        </div>  
                    </div>
  
                </div>
            </div>
        </form>

<script type="text/javascript" src="modules/booking/book_form.js?23"></script>
<script type="text/javascript" src="modules/booking/book_translate.js?23"></script>

<script>


<?php
echo "var timeout='$timeout';";
echo "var brwsr_type = '$brwsr_type';";
echo "var imglogo = '$logo';";
echo "var typeBooking = $typeBooking;";
echo "var restaurant = '$bkrestaurant';";
echo "var title = '$bktitle';";
echo "var action = '$action';";
echo "var pproduct = '$bkproduct';";
echo "var pdate = '$bkdate';";
echo "var ptime = '$bktime';";
echo "var pcover = '$bkcover';";
echo "var bkchild = '$bkchild';";
echo "var bkextra = '$bkextra';";
echo "var psalutation = '$bksalutation';";
echo "var pfirst = '$bkfirst';";
echo "var plast = '$bklast';";
echo "var pemail = '$bkemail';";
echo "var pmobile = '$bkmobile';";
echo "var langue = '$bklangue';";
echo "var topic = 'BOOKING';";
echo "var prequest = '$bkspecialrequest';\n\n";
echo "var errmsg = '$errmsg';";
echo "var llorder = '$llorder';";
echo "var ldorder = '$ldorder';";
echo "var bkinfo = '$bkinfo';";
echo "var bkcritical = '$bkcritical';";
echo "var payDeposit = $payDeposit;";
echo "var isCreditCardInfoActive = $isCreditCardInfoActive;";

echo "var resCountry = '$resCountry';";
echo "var resBookingDeposit = '';";
echo "var resCurrency;";
if(isset($_POST['resBookingDeposit']) && trim($_POST['resBookingDeposit']) != '') {
    echo "resBookingDeposit = \"{$_POST['resBookingDeposit']}\";";
    echo "resCurrency = \"{$_POST['resCurrency']}\";";
}
/*
* Event Booking ===========
*/
if(isset($_POST['event']) && trim($_POST['event']) != '') {
	echo "var event_data = " . json_encode($_POST['event']) . ";";
	echo 'var BASE_URL = "' . __BASE_URL__ . '";';
}

// ========================
echo "var tracking = '$bktracking';";
printf("var optinbkg = %s;", ($optinbkg) ? "true" : "false");


printf("var flgsg = %s;", (!preg_match('/CALLCENTER|WEBSITE|facebook|GRABZ/i', $bktracking)) ? 'true' : 'false');
printf("var flgcc = %s;", (!preg_match('/CALLCENTER/i', $bktracking)) ? 'true' : 'false');
printf("var flgwb = flgcc;");
//printf("var flgwb = %s;", (preg_match('/WEBSITE/', $bktracking)) ? 'true' : 'false');
printf("var is_listing = %s;", $is_listing ? 'true' : 'false');
?>

String.prototype.capitalize = function () {
	return this.replace(/^./, function (match) {
		return match.toUpperCase();
	});
};

app.config(function ($analyticsProvider) {
	// turn off automatic tracking
	$analyticsProvider.virtualPageviews(false);
});


app.controller('MainController', function ($scope, $http, $locale, $timeout, $localStorage, HttpServiceAvail, booktranslate) {

	if(resBookingDeposit !== '') {
		$scope.resBookingDeposit = resBookingDeposit;
		$scope.resCurrency = resCurrency;
		$scope.hasDeposit = true;
	}
    
    $scope.restaurant = restaurant;    
	$scope.onlyonce = true;
        $scope.showpayprocess = false;
        $scope.showprebook =true;
	//	$scope.buttonClass = (typeBooking) ? 'btn-danger' : 'btn-green';
	$scope.errmsg = errmsg;
	$scope.imglogo = imglogo;
	$scope.brwsr_type = brwsr_type;
	$scope.langue = langue;
	$scope.pcheckmark = ($scope.langue != 'cn') ? 'pcheckmark' : 'pcheckmarkcn';
	$scope.title = title;
        $scope.pproduct = pproduct;
	$scope.pdate = pdate;
	$scope.ptime = ptime;
	$scope.pcover = pcover;
	$scope.psalutation = psalutation;
	$scope.pfirst = pfirst;
	$scope.plast = plast;
	$scope.pname = psalutation + ' ' + pfirst + ' ' + plast;
	$scope.pemail = pemail;
	$scope.pmobile = pmobile;
	$scope.prequest = prequest;
	$scope.llorder = llorder;
	$scope.ldorder = ldorder;
	//$scope.bkinfo = bkinfo.replace(/\|\|\|/g, "\n");
        $scope.bkinfo = bkinfo.split("||||||");
        
	$scope.bkcritical = bkcritical.split("||||||");

       
	$scope.showinfo = "";
	$scope.lorderlunch = "Last order for lunch is ";
	$scope.lorderdiner = "Last order for dinner is ";

      
	if (flgwb)
		$scope.showinfo = $scope.bkinfo + llorder + ldorder;

	$scope.buttonLabel = (typeBooking) ? 'book now' : 'request your spin code';
	$scope.bookingTitle = (typeBooking) ? "book your table" : "get your spin code";
	$scope.buttonConfirm = 'Confirm';
	$scope.timeouttag = "time out";
	$scope.listTags0 = "Free Booking";
	$scope.listTags1 = "";
	$scope.listTags2 = (typeBooking) ? "Instant Reservation" : "No reservartion";
	$scope.listTags3 = (typeBooking) ? "" : "First come, first serve";
	$scope.listTags4 = (!is_listing) ? "Win for every booking" : "Enjoy incredible promotion";

	$scope.tmc1 = "By confirming, I agree to the ";
	$scope.tmc2 = "terms and conditions";
    $scope.canceltmc = "the cancellation fee";
	$scope.flgsg = flgsg;
	$scope.flgcc = flgcc;   

	$scope.restau_conditions = "Conditions";

        $scope.slogan2 = 'Verify your information';
        $scope.slogan2_sub = 'To confirm your booking, you must accept terms and conditions';

	$scope.optin = (tracking !== "" && tracking.toLowerCase().indexOf("website") >= 0) ? optinbkg : false;
	$scope.optintext = "I would like to be informed of " + $scope.title + "'s latest promotions/offers";

	$scope.langdata = $localStorage.langdata;
	$scope.zang = null;
	if ($scope.langdata && $scope.langdata.version && $scope.langdata.version.lb) {
		if ($scope.langdata.version.lb === restaurant)
			$scope.zang = $scope.langdata;
	}

	if (!$scope.zang) {
		$scope.zang = booktranslate.getobj($scope);
		}

    $scope.display_checkmarks = ($scope.restaurant !== 'SG_SG_R_BurntEnds' && $scope.restaurant !== 'SG_SG_R_TheOneKitchen' ) ? true : false;

	$scope.slogan = ($scope.flgsg && resCountry === "Singapore") ? $scope.zang['confinfo'].vl : "Please confirm your information ";

	booktranslate.checkmark($scope, flgsg);
	$scope.bookinginfo = booktranslate.bookinginfo($scope);
    $scope.creditcardinfo = isCreditCardInfoActive;    
	
	$scope.depositCheckout = function() {
	   var form = $('#book_form');
		$('body').addClass('waitprogress');
		$('a').addClass('waitprogress'); 
                $scope.showpayprocess = true;
                $scope.showprebook =false;
                console.log($scope.showpayprocess);
		//form.attr('action', 'modules/event_booking/event_booking.php?action=booking_deposit');
                //form.submit();
	};
        
	$scope.getcancelpolicy = function () {
		 var payment_method =($scope.creditcardinfo === 1) ?'carddetails' : "credit card" ;
		 console.log("payment_method"+ payment_method +"<>="+$scope.creditcardinfo);

		HttpServiceAvail.getcancelpolicy($scope.restaurant,payment_method, $scope.resBookingDeposit, $scope.pproduct).then(function (response) {
			var data = response.data.data;
			if (typeof data === 'undefined' || response.data.statusText !== "OK")
				return;
			$scope.tnc = data.message;
			$scope.cancelpolicy = data.range;
			$scope.freerange = data.lastRange;			
			});
		};
		
	if($scope.hasDeposit || true) {
		 $scope.getcancelpolicy();
	}

	$scope.submitonce = function() {

		if($scope.onlyonce === true)
			$('#book_form').submit();
			$scope.onlyonce = false;
		};
		               
	$scope.submitmodify = function() {
		$('#book_form').attr('action', action);
	
		if($scope.onlyonce === true)
			$('#book_form').submit();

		$scope.onlyonce = false;
	}

    });

        </script>
    </body>
</html>

