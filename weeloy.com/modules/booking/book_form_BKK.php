<?php include_once 'book_form.inc.php'; ?>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="author" content="weeloy. https://www.weeloy.com">
<meta name="copyright" content="weeloy. https://www.weeloy.com">  
<meta http-equiv='expires' content='mon, 1 jan 1998 05:00:00 gmt'>
<meta http-equiv='pragma' content='no-cache'>
<meta http-equiv='pragma' content='cache-control: max-age=0'>
<meta http-equiv='last-modified' content='mon, 1 jan 1998 05:00:00 gmt'>
<meta http-equiv='cache-control' content='no-cache, must-revalidate'>
<meta name='robots' content='noindex, nofollow'>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
<meta name="HandheldFriendly" content="true" />
<title>Book your table now - Weeloy Code - Weeloy.com</title>

<base href="<?php echo __ROOTDIR__; ?>/">

<link href="client/bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="client/bower_components/bootstrap/dist/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
<link href="client/bower_components/fontawesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />

<link href="modules/booking/assets/css/css/bootstrap-select.css" rel="stylesheet" >
<link href="modules/booking/assets/css/css/bootstrap-social.css" rel="stylesheet" >
<link href="modules/booking/assets/css/css/famfamfam-flags.css" rel="stylesheet" >
<link href="modules/booking/assets/css/css/dropdown.css" rel="stylesheet" >
<link href="modules/booking/assets/css/bookingform.css" rel="stylesheet" >

<script type='text/javascript' src="client/bower_components/angular/angular.min.js"></script>
<script type="text/javascript" src="client/bower_components/jquery/dist/jquery.min.js"></script>
<script type='text/javascript' src="client/bower_components/angulartics/src/angulartics.js"></script>
<script type='text/javascript' src="client/bower_components/angulartics/src/angulartics-gtm.js"></script>
<script type="text/javascript" src="js/jquery-ui.js"></script>
<script type="text/javascript" src="client/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/bootstrap-select.js"></script>
<script type='text/javascript' src="js/ui-bootstrap-tpls-0.12.1.min.js"></script>
<script type="text/javascript" src="js/dayroutine.js"></script>
<script type="text/javascript" src="js/ngStorage.min.js"></script>
<script type="text/javascript" src="js/mylocal.js"></script>
<style>

.selectwidthauto { width:auto !important; }
body { margin: 2px 0 0 2px; }
.calendarbk { position: relative; }
.calendarbk .dropdown-menu { left: auto !important; right: 0px;}			
<?php if ($brwsr_type != "mobile") echo ".mainbox { margin: 10px 30px 20px 1px; width:550px; } "; ?>
<?php if ($_REQUEST['krht'] == "83") echo ".container  { margin: 0 0 0 0; }" ?>

<?php 
if(!empty($bktracking) && preg_match("/GRABZ/i", $bktracking)){$bkgcustomcolor = '#000000';}
// booking customization
if(!empty($bkgcustomcolor)) {

echo <<<DELIMITER
	h1 { background-color: $bkgcustomcolor; }
	.separation { border-right: 4px solid $bkgcustomcolor; height: 100%; }
	.separation-left { border-left: 4px solid $bkgcustomcolor; }
	.panel { border-color: $bkgcustomcolor !important; }
	.panel-heading { background-color: $bkgcustomcolor !important; border-color: $bkgcustomcolor !important; }
	.caret { color: $bkgcustomcolor; }
DELIMITER;

	}
?>

</style>
<?php include_once("ressources/analyticstracking.php") ?>
</head>

<body ng-app="myApp" ng-cloak>
<form class='form-horizontal' action='<?php echo $actionfile ?>' role='form' id='book_form' name='book_form' method='POST' enctype='multipart/form-data'>

	<div id='bookingCntler' ng-controller='MainController' ng-init="typeBooking = true;" >

		<div class="container mainbox">    
			<div class="panel panel-info">
				<div class="panel-heading">
					<div class="panel-title" > 
						<table width='{{browserwidth}}%'>
							<tr><td>{{ zang['bookingtitle'].vl | uppercase}} <span ng-if='timeout'>({{zang['timeouttag'].vl}})</span></td><td align='right'>
							<div class="dropdown">
								<button type='button' id='itemdfcountry' class='btn btn-default btn-xs dropdown-toggle' data-toggle='dropdown'>
                                                                    <i class="{{curlangicon}}"></i>&nbsp; <strong>{{curlang}}</strong> 
                                                                    <span class='caret'></span>
                                                                </button>
								<ul class='dropdown-menu scrollable-menu-all' style='font-size:12px;'>
									<li ng-repeat="s in languages track by $index"  class='showall'><a ng-if="s.b != '';" href="javascript:;" ng-click="setmealtime($index, 5);" ><i class='famfamfam-flag-{{s.b}}'></i> {{s.a}}</a><hr ng-if="s.b == '';" /></li>
								</ul>                               
							</div>
							</td></tr>
						</table>
					</div>
				</div>  

				<div class="panel-body" >
					<?php
					reset($hiddenlist);
					while (list($label, $value) = each($hiddenlist))
						printf("<input type='hidden' id='%s' name='%s' value='%s'>", $label, $label, $value);
					printf("<input type='hidden' id='%s' name='%s' value='%s'>", "signed_request", "signed_request", $signed_request);
					?>


					<?php if ($brwsr_type == "mobile") : ?>
						<div>				
							<span class="slogan" ng-if="flgsg"><strong> {{zang['slogan'].vl}}</strong></span>
							<img ng-src='{{imglogo}}' id='theLogo' name='theLogo' >
							<?php if (!empty($selectresto)) printf("<p align='center'>{{zang['restaurants'].vl| uppercase}}<br/>%s</p><br/><br/>", $selectresto); ?>

						<?php else : ?>
							<p class="slogan" ng-if="flgsg"><strong> {{zang['slogan'].vl}}</strong></p>

							<?php if (!empty($selectresto)) printf("<p align='center'>{{zang['restaurants'].vl| uppercase}}<br/>%s</p><br/><br/>", $selectresto); ?>
							<div class='col-xs-5'>
								<img ng-src='{{imglogo}}' height='120px' id='theLogo' name='theLogo'><br>
								<table width='90%'>
									<tr ng-repeat="x in checkmark"><td><span class='glyphicon glyphicon-ok checkmark'></span> <br />&nbsp;</td><td ng-class='pcheckmark'><span >{{x.label1}} {{ x.label2}} <br />&nbsp;</td>
								</table><br/>

								<a class="btn btn-sm btn-social btn-facebook" id='loginBtn' ng-click="facebkclik();" title="fill in using your Facebook account"><i class="fa fa-facebook"></i>{{ zang['facebookfillin'].vl}}</a>
							</div>
							<div class='col-xs-7 separation-left'>

							<?php endif; ?>

							<div style="margin-left:3px;">
							<div class="modal fade" id="remoteModal" tabindex="-1" role="dialog" aria-labelledby="remoteModalLabel" aria-hidden="true">  
								<div class="modal-dialog" style="background-color: #fff">  
									<div class="modal-content" style='font-size:11px;'></div>  
								</div>  
							</div>  

							<h1>{{zang['bookingdetail'].vl| uppercase}}</h1>
							
							<div class="input-group calendarbk">
								<span class="input-group-addon" ng-click="opendropdown($event, 'date')">
                                                                    <i class="glyphicon glyphicon-calendar"></i> &nbsp; 
                                                                    <span class='caret'></span>
                                                                </span>
								<input type="text" class="form-control input" id='bkdate' name='bkdate' datepicker-popup="{{format}}" datepicker-options="dateOptions" ng-model="bkdate" ng-click="opendropdown($event, 'date')" 
									   is-open="dateopened" min-date="minDate" max-date="maxDate" date-disabled="disabled(date, mode)" ng-required="true" ng-change='updatendays();' close-text="{{ zang['close'].vl}}" current-text="{{ zang['today'].vl}}" clear-text="{{ zang['clear'].vl}}" readonly />
							</div>

							<div class='input-group'>
								<div class='input-group-btn' dropdown is-open="mtimeopened">
									<button type='button' class='btn book-dropdown' data-toggle='dropdown' ng-click="opendropdown($event, 'mtime')">
										&nbsp;<i class="glyphicon glyphicon-time"></i>&nbsp; <span class='caret' ></span>
									</button>
									<ul class='dropdown-menu scrollable-menu'>
										<li style='color:#5285a0'>&nbsp;  {{zang.lunchtag.vl}}  &nbsp;<span class='caret'></span></li>
										<li ng-repeat="x in lunch"><a href="javascript:;" ng-click="setmealtime(x, 1);">{{ x}}</a></li>
										<li>&nbsp;  -------------------- </li>
										<li style='color:#5285a0'>&nbsp;  {{zang.dinnertag.vl}}  &nbsp;<span class='caret'></span></li>
										<li ng-repeat="y in dinner"><a href="javascript:;" ng-click="setmealtime(y, 1);">{{ y}}</a></li>
									</ul>
								</div>
								<input type='text' ng-model="bktime" class='form-control input' id='bktime' name='bktime' ng-click="opendropdown($event, 'mtime')" readonly>
							</div>

							<div class='input-group'>
								<div class='input-group-btn' dropdown  is-open="coveropened">
									<button type='button' class='btn book-dropdown' data-toggle='dropdown' ng-click="opendropdown($event, 'cover')">
										&nbsp;<i class="glyphicon glyphicon-cutlery"></i>&nbsp; <span class='caret'></span>
									</button>
									<ul class='dropdown-menu scrollable-menu'>
										<li ng-repeat="p in npers"><a href="javascript:;" ng-click="setmealtime(p, 2);">{{ p}}</a></li>
									</ul></div>
								<input type='text' ng-model='bkcover' class='form-control input' id='bkcover' name='bkcover' ng-click="opendropdown($event, 'cover')"  readonly >
							</div>


							<h1>{{ zang['personaldetail'].vl | uppercase}}</h1>


							<div class='input-group' ng-show="langue != 'jp';">
								<div class='input-group-btn' dropdown is-open="titleopened">
									<button type='button' id='itemdfsalut' class='btn book-dropdown' data-toggle='dropdown' ng-click="opendropdown($event, 'title')" >
										&nbsp;<i class="glyphicon glyphicon-tag"></i>&nbsp; <span class='caret'></span>
									</button>
									<ul class='dropdown-menu'>
										<li ng-repeat="s in salutations"><a href="javascript:;" ng-click="setmealtime(s, 3);">{{ s}}</a></li>
									</ul>
								</div>
								<input type='text' ng-model='bksalutation' class='form-control input' id='bksalutation' name='bksalutation' ng-click="opendropdown($event, 'title')" readonly>
							</div>

							<div class="input-group">
								<span class="input-group-addon"><i class="glyphicon glyphicon-user"></i>&nbsp;&nbsp;&nbsp;&nbsp;</span>
								<input type="text" ng-model='bkfirst' class="form-control input" id='bkfirst' name='bkfirst' placeholder="{{zang.firsttag.vl}}" ng-change='bkfirst = cleantext(bkfirst);'>                                        
							</div>

							<div class="input-group">
								<span class="input-group-addon"><i class="glyphicon glyphicon-user"></i>&nbsp;&nbsp;&nbsp;&nbsp;</span>
								<input type="text" ng-model='bklast' class="form-control input" id='bklast' name='bklast' placeholder="{{zang.lasttag.vl}}" ng-change='bklast = cleantext(bklast);'>                                        
							</div>

							<div class="input-group">
								<span class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i>&nbsp;&nbsp;&nbsp;&nbsp;</span>
								<input type="text" ng-model='bkemail' class="form-control input" id='bkemail' name='bkemail' placeholder="{{zang.emailtag.vl}}" ng-change='bkemail = cleanemail(bkemail);'>                                        
							</div>
                                                        <label id='error-bkemail-empty' name='error-bkemail-empty' style="display:none; color:red; margin-bottom: 25px;">Empty email</label>
                                                        
							<div class="input-group">
								<div class='input-group-btn '>
									<button type='button' id='itemdfcountry' class='btn book-dropdown' data-toggle='dropdown'>
										&nbsp;<i class="glyphicon glyphicon-earphone"></i>&nbsp;<span class='caret'></span>
									</button>
									<ul class='dropdown-menu scrollable-menu' style='font-size:12px;'>
										<li ng-repeat="s in countries"><a ng-if="s.b != '';" href="javascript:;" ng-click="setmealtime(s.a, 4);" ><i class='famfamfam-flag-{{s.b}}'></i> {{s.a}}</a><hr ng-if="s.b == '';" /></li>
									</ul></div>
								<input type="text" ng-model='bkmobile' class="form-control input" id='bkmobile' name='bkmobile' placeholder="{{zang.mobiletag.vl}}" ng-change='checkvalidtel();'>                            
								<input type="hidden" ng-model='bkcountry' id='bkcountry' name='bkcountry'>                            
							</div>

							<div class="input-group">
								<span class="input-group-addon"><i class="glyphicon glyphicon-info-sign"></i>&nbsp;&nbsp;&nbsp;&nbsp;</span>
								<textarea ng-if="promocode === false" type="text" ng-model='bkspecialrequest' class="form-control input" id='bkspecialrequest' name='bkspecialrequest' rows="2"  placeholder="{{zang.requesttag.vl}}" ng-change='bkspecialrequest = cleantext(bkspecialrequest);'></textarea>
								<textarea ng-if="promocode === true" type="text" ng-model='bkspecialrequest' class="form-control input" id='bkspecialrequest' name='bkspecialrequest' rows="2"  placeholder="{{zang.requestpromotag.vl}}" ng-change='bkspecialrequest = cleantext(bkspecialrequest);'></textarea>
							</div>
                                                        <label id='error-message' name='error-message' style=" display:none;color:red; margin-bottom: 25px;">Required: date, time, pax, email, first name, last name, mobile </label>
							<div class="input-group nomarginbottom" >
								<a href="javascript:;" ng-click="checksubmit();" class="book-button-sm btn-leftBottom-orange"> {{ zang['buttonbook'].vl}} </a>
							</div>
                                                        
						</div>

						<div class='row' ng-if="brwsr_type === 'mobile'" style="margin: 0 0 0 10px;">
							<hr>
							<a class="btn btn-sm btn-social btn-facebook" id='loginBtn' ng-click="facebkclik();" title="fill in using your Facebook account" style='margin-left:5px;'><i class="fa fa-facebook"></i>{{ zang['facebookfillin'].vl}}</a>
							<hr>
							<ul>
								<li ng-repeat="x in checkmark"><p class='glyphicon glyphicon-ok checkmark' ng-if='x.glyph'></p><span ng-class='pcheckmark'> {{x.label1}} <br /> {{ x.label2}}<br/></span></li>
							</ul>
						</div>
						</div>
					</div>  
				</div>
			</div>

		</div>

</form>

<?php include_once 'book_form.script.php'; ?>

</body>

</html>