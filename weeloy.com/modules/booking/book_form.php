<?php include_once 'book_form.inc.php';


//checking multiAllotment
// $mutipleProdAllote = ($res->checkMultiProductAlllote() != 0) ? 1: 0;
//        if($mutipleProdAllote == 1){
//           $section = (isset($_REQUEST['bktracking'])) ? 'book-now-section' :'booknow-section';
//            $sectionUrl =  __BASE_URL__ .'/restaurant/'.strtolower($res->country).'/'.preg_replace('/ /', "", $bktitle).'/'.$section;
//            
//            // $sectionUrl = __BASE_URL__ . '/' . $res->internal_path.'/'.$section;
//            
//           echo "<script> window.top.location.href ='$sectionUrl';</script>";
//             
//        }

?>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="author" content="weeloy. https://www.weeloy.com"/>
        <meta name="copyright" content="weeloy. https://www.weeloy.com"/>  
        <meta http-equiv='expires' content='mon, 1 jan 1998 05:00:00 gmt'/>
        <meta http-equiv='pragma' content='no-cache'/>
        <meta http-equiv='pragma' content='cache-control: max-age=0'/>
        <meta http-equiv='last-modified' content='mon, 1 jan 1998 05:00:00 gmt'/>
        <meta http-equiv='cache-control' content='no-cache, must-revalidate'/>
        <meta name='robots' content='noindex, nofollow'/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
        <meta name="HandheldFriendly" content="true" />
        <title>Book your table now</title>

        <base href="<?php echo __ROOTDIR__; ?>/" />

        <link href="client/bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="client/bower_components/bootstrap/dist/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
        <link href="client/bower_components/fontawesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />

        <link href="modules/booking/assets/css/css/bootstrap-select.css" rel="stylesheet" />
        <link href="modules/booking/assets/css/css/bootstrap-social.css" rel="stylesheet" />
        <link href="modules/booking/assets/css/css/famfamfam-flags.css" rel="stylesheet" />
        <link href="modules/booking/assets/css/css/dropdown.css" rel="stylesheet" />
        <link href="modules/booking/assets/css/bookingform.css?v=2" rel="stylesheet" />

        <script type='text/javascript' src="client/bower_components/angular/angular.min.js"></script>
        <script type="text/javascript" src="client/bower_components/jquery/dist/jquery.min.js"></script>
        <script type='text/javascript' src="client/bower_components/angulartics/src/angulartics.js"></script>
        <script type='text/javascript' src="client/bower_components/angulartics/src/angulartics-gtm.js"></script>
        <script type="text/javascript" src="js/jquery-ui.js"></script>
        <script type="text/javascript" src="client/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="client/bower_components/ui-bootstrap-tpls-0.14.2.min.js"></script>
        <script type="text/javascript" src="js/bootstrap-select.js"></script>
        <script type="text/javascript" src="js/dayroutine.js"></script>
        <script type="text/javascript" src="js/ngStorage.min.js"></script>
        <script type="text/javascript" src="js/mylocal.js"></script>
        <style>
.disableddiv {
    pointer-events: none;
    opacity: 0.4;
}

<?php
        if (!empty($bkgcustomcolor)) {
		include_once('book_custom.php');
            }
?>

        </style>
        <?php include_once("ressources/analyticstracking.php") ?>
    </head>

    <body ng-app="myApp" ng-cloak style="background: transparent;">

        <form class='form-horizontal' action='<?php echo $actionfile ?>' role='form' id='book_form' name='book_form' method='POST' enctype='multipart/form-data'>

            <div id='bookingCntler' ng-controller='MainController' ng-init="typeBooking = true;" >

                <div class="container mainbox" style="padding-left:7px;padding-right:7px;">    
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <div class="panel-title" > 
                                <div id="parent">
                                    <div class="right">
                                        <div class="dropdown dropdown-language-container" uib-dropdown >
                                            <button type='button' id='itemdfcountry' class='btn btn-xs dropdown-language' uib-dropdown-toggle >
                                                <i class="{{curlangicon}}"></i>&nbsp; <span ng-bind="curlang"></span> 
                                                <span class='caret'></span>
                                            </button>
                                            <ul class='dropdown-menu dropdown-menu-language scrollable-menu-all' uib-dropdown-menu style='font-size:12px;'>
                                                <li ng-repeat="s in languages track by $index"  class='showall'>
                                                    <a ng-if="s.b != '';" href="javascript:;" ng-click="setmealtime($index, 5);" >
                                                        <i class='famfamfam-flag-{{s.b}}'></i> {{s.a}}</a>
                                                    <hr ng-if="s.b == '';" />
                                                </li>
                                            </ul>                               
                                        </div>
                                    </div>
                                    <div class="left">
                                        <span ng-bind="zang['bookingtitle'].vl | uppercase"></span>
                                        <span ng-if='timeout'>
                                            (
                                            <span ng-bind="zang['timeouttag'].vl"></span>
                                            )
                                        </span>
                                    </div>  
                                </div>
                            </div>
                        </div>  

                        <div class="panel-body">
                            <?php
                            reset($hiddenlist);
                            while (list($label, $value) = each($hiddenlist))
                                printf("<input type='hidden' id='%s' name='%s' value='%s'>", $label, $label, $value);
                            printf("<input type='hidden' id='%s' name='%s' value='%s'>", "signed_request", "signed_request", $signed_request);
                            ?>

                            <p class="slogan" ng-if="flgsg"><strong> <span ng-bind="zang['slogan'].vl"></span></strong></p>

                            <?php if (!empty($selectresto)) printf("<p style='text-align:center;font-size:21px;color:%s' >%s</p><br/><br/>", empty($empty_option) ? "navy": "red", $selectresto); ?>
                            <div class='left-column'  ng-class="{disableddiv : need_restaurant_selection}" >
                                <img ng-src='{{imglogo}}' height='120px' id='theLogo' name='theLogo'>
                                    <div ng-if="showfrontinfo" style='font-size:12px;margin:10px 0 30px 0;'> <strong> {{ frontinfo}} </strong></div>

                                    <div class="book-form-lg">

                                        <table width='90%'>
                                            <tr ng-repeat="x in checkmark">
                                                <td>
                                                    <span class='glyphicon glyphicon-ok checkmark'></span> 
                                                    <br />&nbsp;
                                                </td>
                                                <td ng-class='pcheckmark'>
                                                    <span ng-bind="x.label1"></span>
                                                    <span ng-bind="x.label2"></span> 
                                                    <br />&nbsp;
                                                </td>
                                        </table>

                                        <a class="btn btn-sm btn-social btn-facebook" id='loginBtn' ng-click="facebkclik();" title="fill in using your Facebook account"><i class="fa fa-facebook"></i><span ng-bind="zang['facebookfillin'].vl"></span></a>
                                        <br />
                                    </div>
                            </div>

                            <div class='right-column separation-left no-mobile' ng-class="{disableddiv : need_restaurant_selection}">
                                <div style="margin-left:3px;">
                                    <div class="modal fade" id="remoteModal" tabindex="-1" role="dialog" aria-labelledby="remoteModalLabel" aria-hidden="true">  
                                        <div class="modal-dialog" style="background-color: #fff">  
                                            <div class="modal-content" style='font-size:11px;'></div>  
                                        </div>  
                                    </div>  

                                    <h1 ng-bind="zang['bookingdetail'].vl | uppercase"></h1>

                                    <div class="input-group calendarbk">
                                        <span class="input-group-addon" ng-click="opendropdown($event, 'date', 1)">
                                            <i class="glyphicon glyphicon-calendar"></i> &nbsp; 
                                            <span class='caret'></span>
                                        </span>
                                        <input type="text" class="form-control input" id='bkdate' name='bkdate' 
                                               ng-model="dpDate" 
                                               ng-click="opendropdown($event, 'date', 1)" 
                                               ng-required="true" 
                                               ng-change='updatendays();' 
                                               uib-datepicker-popup="{{format}}" 
                                               datepicker-options="dateOptions" 
                                               is-open="dropdown.date_opened" 
                                               min-date="minDate" 
                                               max-date="maxDate" 
                                               date-disabled="disabled(date, mode)" 
                                               close-text="{{ zang['close'].vl}}" 
                                               current-text="{{ zang['today'].vl}}" 
                                               clear-text="{{ zang['clear'].vl}}" 
                                               clear-button="hidden"
                                               readonly />
                                    </div>

                                    <div class='input-group'>
                                        <div class='input-group-btn' uib-dropdown  is-open='dropdown.mtime_opened' >
                                            <button type='button' class='btn book-dropdown'  uib-dropdown-toggle ng-click="opendropdown($event, 'mtime', 0)">
                                                &nbsp;<i class="glyphicon glyphicon-time"></i>&nbsp; <span class='caret' ></span>
                                            </button>
                                            <ul class='dropdown-menu scrollable-menu' uib-dropdown-menu>
                                                <li style='color:#5285a0'>&nbsp;  {{zang.lunchtag.vl}}  &nbsp;</li>
                                                <li ng-repeat="x in lunch"><a href="javascript:;" ng-click="setmealtime(x, 1);">{{ x}}</a></li>
                                                <li ng-if="teatime.length > 0">&nbsp;  -------------------- </li>
                                                <li ng-if="teatime.length > 0" style='color:#5285a0'>&nbsp;  {{zang.teatimetag.vl}}  &nbsp;</li>
                                                <li ng-if="teatime.length > 0" ng-repeat="x in teatime"><a href="javascript:;" ng-click="setmealtime(x, 1);">{{ x}}</a></li>
                                                <li>&nbsp;  -------------------- </li>
                                                <li style='color:#5285a0'>&nbsp;  {{zang.dinnertag.vl}}  &nbsp;</li>
                                                <li ng-repeat="x in dinner"><a href="javascript:;" ng-click="setmealtime(x, 1);">{{ x}}</a></li>
                                            </ul>
                                        </div>
                                        <input type='text' ng-model="bktime" class='form-control input' id='bktime' name='bktime' ng-click="opendropdown($event, 'mtime', 1)" readonly>
                                    </div>

                                    <div class='input-group'>
                                        <div class='input-group-btn' uib-dropdown  is-open='dropdown.cover_opened'>
                                            <button type='button' class='btn book-dropdown' uib-dropdown-toggle ng-click="opendropdown($event, 'cover', 0)">
                                                &nbsp;<i class="glyphicon glyphicon-cutlery"></i>&nbsp; <span class='caret'></span>
                                            </button>
                                            <ul class='dropdown-menu scrollable-menu' uib-dropdown-menu >
                                                <li ng-repeat="p in npers"><a href="javascript:;" ng-click="setmealtime(p, 2);">{{ p}}</a></li>
                                            </ul>
                                        </div>
                                        <input type='text' ng-model='bkcover' class='form-control input' id='bkcover' name='bkcover' ng-click="opendropdown($event, 'cover', 1)"  readonly >
                                    </div>


                                    <h1 ng-bind="zang['personaldetail'].vl | uppercase"></h1>


                                    <div class='input-group' ng-show="langue != 'jp';">
                                        <div class='input-group-btn' uib-dropdown   is-open='dropdown.title_opened'>
                                            <button type='button' class='btn book-dropdown' uib-dropdown-toggle ng-click="opendropdown($event, 'title', 0)" >&nbsp;<i class="glyphicon glyphicon-tag"></i>&nbsp; <span class='caret'></span></button>
                                            <ul class='dropdown-menu' uib-dropdown-menu >
                                                <li ng-repeat="s in salutations track by $index"><a href="javascript:;" ng-click="setmealtime(s, 3);">{{ s}}</a></li>
                                            </ul>
                                        </div>
                                        <input type='text' ng-model='bksalutation' class='form-control input' id='bksalutation' name='bksalutation' ng-click="opendropdown($event, 'title', 1)" readonly>
                                    </div>

                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i>&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                        <input type="text" ng-model='bkfirst' class="form-control input" id='bkfirst' name='bkfirst' placeholder="{{zang.firsttag.vl}}" ng-change='bkfirst = cleantext(bkfirst);'>                                        
                                    </div>

                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i>&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                        <input type="text" ng-model='bklast' class="form-control input" id='bklast' name='bklast' placeholder="{{zang.lasttag.vl}}" ng-change='bklast = cleantext(bklast);'>                                        
                                    </div>

                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i>&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                        <input type="text" ng-model='bkemail' class="form-control input" id='bkemail' name='bkemail' placeholder="{{zang.emailtag.vl}}" ng-change='bkemail = cleanemail(bkemail);'>                                        
                                    </div>

                                    <label id='error-bkemail-empty' name='error-bkemail-empty' style="display:none; color:red; margin-bottom: 25px;">Empty email</label>

                                    <div class="input-group">
                                        <div class='input-group-btn' uib-dropdown >
                                            <button type='button' id='itemdfcountry' class='btn book-dropdown' uib-dropdown-toggle >&nbsp;<i class="glyphicon glyphicon-earphone"></i>&nbsp;<span class='caret'></span></button>
                                            <ul class='dropdown-menu scrollable-menu' uib-dropdown-menu style='font-size:12px;'>
                                                <li ng-repeat="s in countries"><a ng-if="s.b != '';" href="javascript:;" ng-click="setmealtime(s.a, 4);" ><i class='famfamfam-flag-{{s.b}}'></i> {{s.a}}</a><hr ng-if="s.b == '';" /></li>
                                            </ul>
                                        </div>
                                        <input type="text" ng-model='bkmobile' class="form-control input" id='bkmobile' name='bkmobile' placeholder="{{zang.mobiletag.vl}}" ng-change='checkvalidtel();'/>
                                        <input type="hidden" ng-model='bkcountry' id='bkcountry' name='bkcountry'/>
                                    </div>

                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="glyphicon glyphicon-info-sign"></i>&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                        <textarea ng-if="promocode === false" type="text" ng-model='bkspecialrequest' class="form-control input" id='bkspecialrequest' name='bkspecialrequest' rows="2"  placeholder="{{zang.requesttag.vl}}" ng-change='bkspecialrequest = cleantext(bkspecialrequest);'></textarea>
                                        <textarea ng-if="promocode === true" type="text" ng-model='bkspecialrequest' class="form-control input" id='bkspecialrequest' name='bkspecialrequest' rows="2"  placeholder="{{zang.requestpromotag.vl}}" ng-change='bkspecialrequest = cleantext(bkspecialrequest);'></textarea>
                                    </div>
                            
                                		
                                   <div class="input-group" ng-if="hasDeposit && chkcpflg">
                                        <h1  ng-bind="zang['depositdetail'].vl | uppercase"></h1>
                                            <p ng-if ='showchoice && $scope.choiceobj.length >0' > <select ng-model="bkchoice" id='bkchoice' ng-options="p for p in choiceobj.data" ng-change="setselect(bkchoice, 'choice');"><option value="">{{ choiceobj.title }} </option></select> </p>
                                            <p ng-if ="restaurant !== 'SG_SG_R_Pollen'"> {{paymenttnc}}. </p>
                                            <div ng-if ="restaurant === 'SG_SG_R_Pollen'" style='font-size:11px;'> 
                                            	<p ng-repeat="str in paymenttncAr">{{ str }} </p>
                                            </div>
            <!--

                                        <div  ng-if ='!chkbachaflg && !chkmediniflg ' ng-repeat ="p in cancelpolicy">
                                           
                                            <p ng-if="p['percentage']==100">{{p['duration']}}{{resCurrency}} {{resBookingDeposit}}. 
                                            <p ng-if="p['percentage']==0">{{p['duration']}}</p>
                                             <p ng-if="p['percentage']!=0 && p['percentage']!=100">{{p['duration']}} </p>
                                        </div>
            -->
                                        
                                        <input type="hidden" value="{{resBookingDeposit}}" name="resBookingDeposit"/>
                                        <input type="hidden" value="{{resCurrency}}" name="resCurrency"/>
                                        <input type="hidden" value="{{mealtype}}" name="mealtype"/>
        <!-- 
                                        <input type="hidden" value="{{bkproduct}}" name="bkproduct"/>
        -->
                                    </div>
                                    <label id='error-message' name='error-message' style=" display:none;color:red; margin-bottom: 25px;">Required: date, time, pax, email, first name, last name, mobile </label>
                                    <div class="input-group nomarginbottom booking-button-div" >
                                        <a href ng-click="checksubmit();" class="book-button-sm btn-leftBottom-orange" ng-bind="zang['buttonbook'].vl"></a>
                                    </div>

                                </div>

                                <div class='row book-form-xs facebook-button-div' >
                                    <a class="btn btn-sm btn-social btn-facebook" id='loginBtn' ng-click="facebkclik();" title="fill in using your Facebook account" style='margin-left:5px;'><i class="fa fa-facebook"></i><span ng-bind="zang['facebookfillin'].vl"></span></a>
                                    <ul>
                                        <li ng-repeat="x in checkmark" class="li-checkmark">
                                            <p class='glyphicon glyphicon-ok checkmark' ng-if='x.glyph'></p>
                                            <span ng-class='pcheckmark'>
                                                <span ng-bind="x.label1"></span>
                                                <span ng-bind="x.label2"></span>
                                            </span>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>  
                    </div>
                </div>

            </div>

        </form>

        <?php include_once 'book_form.script.php'; ?>

    </body>

</html>