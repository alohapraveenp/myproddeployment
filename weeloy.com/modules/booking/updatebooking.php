<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

include 'angular-client/includes/header.php';
require_once("lib/class.member.inc.php");
require_once 'lib/class.booking.inc.php';
require_once("lib/class.media.inc.php");
require_once("lib/class.payment.inc.php");
require_once("lib/wpdo.inc.php");
require_once("lib/class.restaurant_cancel_policy.inc.php");

if(empty($_REQUEST["uid"]) || empty($_REQUEST["refid"])) {
	printf("Invalid Booking. Exiting");
	exit;	
	}

$booking = new WY_Booking();

$email = $booking->base64url_decode($_REQUEST['uid']);
$confirmation = $booking->base64url_decode($_REQUEST['refid']);


                                     
$booking->getBooking($confirmation);
$res = new WY_restaurant;
$payment = new WY_Payment();
$res_policy = new WY_Restaurant_policy;
$res->getRestaurant($booking->restaurant);


if($booking->result < 0) {
    printf("Invalid Booking %s. Exiting", $confirmation);
    exit;
        
}
if($booking->status==='cancel') {
    printf("This reservation is cancelled", $confirmation);
    exit;
        
}
	
$rdate = $booking->rdate;
$rtime = $booking->rtime;
$npers = $booking->cover;
$theRestaurant = $booking->restaurant;
$specialreq = $booking->specialrequest;
$status =$booking->status;
$firstname =$booking->firstname;
$lastname =$booking->lastname;
$deposit_id = $booking->deposit_id;
$maxpax = intval($booking->restaurantinfo->dfmaxpers);
$minpax = intval($booking->restaurantinfo->dfminpers);
$bookingid =$booking->bookid;
 $bkproduct = $booking->product;
 $generic = $booking->generic;
 
 $AvailperPax = ($res->perPaxBooking() != 0) ? "1" : "0";
 

    if ($minpax < 1 || $minpax > 3) {
        $minpax = 1;
    }

    if ($maxpax < 4) {
        $maxpax = 4;
    }
    
    $langue = $booking->language;
    if($langue == "") $langue = "en";  
    //getting deposit details by deposit Id
    $isPayment = 0;
    $totalcharge = 0;
    $tnc_amount = 0;
    $payment_method="";
    $requiredccDetails = false;
    $isAllowModifyflg = 12;
    if(WY_restaurant::s_checkbkdeposit($theRestaurant) > 0){
            $isAllowModifyflg = 11;
        if(WY_restaurant::s_checkmodifypayment($theRestaurant) > 0){
            $isAllowModifyflg = 12;
        }
    }

    if(!empty($booking->deposit_id) && ($booking->deposit_id !== NULL)){ 
        $requiredccDetails = true;
        $depositdetails = $payment->getDepositDetails($booking->deposit_id,$confirmation);
        $paymentDetails = $res_policy->getCancelPaymentDetails($confirmation);
        if($theRestaurant == 'SG_SG_R_Pollen'){
               $isAllowModifyflg = 11;
        }
    
//            if($booking->restaurant == 'SG_SG_R_TheFunKitchen' || $booking->restaurant == 'SG_SG_R_Bacchanalia' ){
//             $paymentDetails = $res_policy->getResChargeDetails($booking->deposit_id,$confirmation);
//            }
//            else if($booking->restaurant == 'SG_SG_R_TheOneKitchen'  ){
//                $paymentDetails = $payment->getBookingDepDetails($confirmation);
//            }else{
//             $paymentDetails = $booking->getDepositDetailsByID($booking->deposit_id,$booking->rdate,$booking->rtime,$booking->restaurant,$depositdetails['payment_method'],$confirmation);
//            }

        if(count($paymentDetails)>0){
            $isPayment = 1;
            $depositStatus = $paymentDetails['status'];
            $totalcharge = $paymentDetails['amount'];
            $tnc_amount = $paymentDetails['refund_amount'];
            $payment_method = $paymentDetails['payment_method'];
  
            if(($theRestaurant == 'SG_SG_R_BurntEnds' || $theRestaurant == 'SG_SG_R_TheOneKitchen') && $paymentDetails['cancelflg'] == 16){
                 printf("Should you require any assistance cancelling a booking, please email the restaurant", $booking->restaurantinfo->email);
                exit;
            }

       }

    }
    
    
  
    $isWaived = 0;

    if($booking->restaurant == 'SG_SG_R_TheFunKitchen' || $booking->restaurant == 'SG_SG_R_Bacchanalia' ){

        $requiredccDetails = $res_policy->isRequiredccDetails($booking->restaurant,$booking->rtime,$booking->cover,$rdate);

         if($requiredccDetails && $tnc_amount == 0){
             $isPayment = 1;
         }
         $options = $booking->generic;
         $text_vars = preg_match("/’ccwaived’:’1’/", $options, $matches);
         $temp = array_shift( $matches );
         $k= preg_replace('/\s+/', '',$temp);
         $waived =  explode(":",$k);

          if($waived[0] == "’ccwaived’"){
                $isWaived = 1;
                $isPayment = 0;
                $isAllowModifyflg = 11;
          }

    }

?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="author" content="weeloy. https://www.weeloy.com"/>
<meta name="copyright" content="weeloy. https://www.weeloy.com"/>  
<meta http-equiv='expires' content='mon, 1 jan 1998 05:00:00 gmt'/>
<meta http-equiv='pragma' content='no-cache'/>
<meta http-equiv='pragma' content='cache-control: max-age=0'/>
<meta http-equiv='last-modified' content='mon, 1 jan 1998 05:00:00 gmt'/>
<meta http-equiv='cache-control' content='no-cache, must-revalidate'/>
<meta name='robots' content='noindex, nofollow'/>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
<meta name="HandheldFriendly" content="true" />
<title>Book your table now - Weeloy Code - Weeloy.com</title>

<base href="<?php echo __ROOTDIR__; ?>/" />

<link href="client/bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="client/bower_components/bootstrap/dist/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
<link href="client/bower_components/fontawesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />

<link href="modules/booking/assets/css/css/bootstrap-select.css" rel="stylesheet" />
<link href="modules/booking/assets/css/css/bootstrap-social.css" rel="stylesheet" />
<link href="modules/booking/assets/css/css/famfamfam-flags.css" rel="stylesheet" />
<link href="modules/booking/assets/css/css/dropdown.css" rel="stylesheet" />

<script type='text/javascript' src="client/bower_components/angular/angular.min.js"></script>
<script type="text/javascript" src="client/bower_components/jquery/dist/jquery.min.js"></script>

<script type="text/javascript" src="js/jquery-ui.js"></script>
<script type="text/javascript" src="client/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/bootstrap-select.js"></script>
<script type='text/javascript' src="js/ui-bootstrap-tpls-0.12.1.min.js"></script>
<script type="text/javascript" src="js/ngStorage.min.js"></script>
<script type="text/javascript" src="js/mylocal.js"></script>

<script> var app = angular.module("myApp", ['ui.bootstrap', 'ngLocale', 'ngStorage']); </script> 
<script type="text/javascript" src="modules/booking/book_form.js?13"></script>
<script type="text/javascript" src="https://checkout.stripe.com/checkout.js"></script>
<script type="text/javascript" src="https://js.stripe.com/v2/"></script>

<style>
body{

	padding-top:1%;
	position: relative;

}
#confirm-container p{
    text-align:center;
    margin-top: 0%;
    font-size:18px;
}
ul{
	list-style-type: none
}
.header-text{
	text-align:center;
}



#invite-alert{
	margin-left:40px;
	margin-bottom:20px;  
}
.col-center-block {
	margin-top: 30px;
	width:600px;
	height:550px;
	margin-left: auto ;
	margin-right: auto ;
	padding: 2px 2px;
	border: 1px solid #aaa;
	margin-bottom:30px;
}
.col-center-block  h2{
   text-align:center;   
}
input[type="radio"]  {
	display:inline-block;

	margin:-1px 4px 0 0;
	cursor:pointer;
}
.input-group{
	margin-bottom: 8px;
}
.survey-conatiner{
	margin-bottom: 10px;
	margin-left: 20px;
	margin-right: 20px;
}
.cancel-policy{
   margin-top: 20px;
   margin-bottom: 10px;
   margin-left: 20px;
  
}
.cancel-policy-list{
   margin-top: 20px;
   margin-bottom: 10px;
   margin-left: 35px;
  
}
.confirm-container{
	margin:30px;
	height:450px;
}
 .btncancel,.btnconfirmCancel,.btnupdate{
	margin-left: 30px;
	float:right;
	background: #2BA8CF none repeat scroll 0 0;
	border: 1px solid #2BA8CF;
	color: #fff;
	border-radius: 2px;
	padding: 1% 5%;
	margin-bottom: 10px;
	-webkit-border-radius: 0px;
	-moz-border-radius: 0px;
	margin-bottom:15px;
/*        background: #2BA8CF none repeat scroll 0 0;
	border: 1px solid #2BA8CF;
	color: #fff;
	border-radius: 2px;
	margin-bottom: 10px;
	margin-top:30px;
	-webkit-border-radius: 0px;
	-moz-border-radius: 0px;
	float:right;
	padding-top:10px;
	padding-right:10px;
	padding-bottom:10px;
	margin-right:10px;*/
}
.error-msg {
	color: #a94442 !important;
	background-color: #f2dede;
	border-color: #ebccd1;
	padding-left:2%;
	padding-bottom:1%;
	padding-top:1%;
}
.success-msg {
	color: #3c763d !important;
	background-color: #dff0d8;
	border-color: #d6e9c6;
	padding-left:2%;
	padding-bottom:1%;
	padding-top:1%;
}
</style>

<body ng-app="myApp">
	<div class="container" ng-controller='UpdateController' ng-init="moduleName = 'update';  ">
		<div class="col-lg-offset-2 col-md-offset-2 col-sm-offset-2 col-lg-8 col-md-8 col-sm-8 col-xs-12 my-booking">
		  <div class ="panel-body" style="border: solid 1px black;">
			<div id="vignette_cancel" class="vignette review_vignette subitem" > 
                     
				<div class="header-text" ng-if="bkupdate" ><h3>{{zang.buttonmodify.vl}}</h3></div><br />
                                <div class="header-text" ng-if="bkcancel || bkcancelconfirm" ><h3>{{zang.buttoncancel.vl}}</h3></div><br />
                                
                                <div ng-show='bkupdate'>
					<form ng-submit="mydata.submit()" >
						<div class="input-group" style='padding:20px 20px 20px 20px;' >
							<span class="input-group-addon input-info">{{zang.firsttag.vl}} <i class="glyphicon glyphicon-info-sign"></i></span>
							<input  type="text" ng-model='mydata.firstname' class="form-control input" id='bkspecialrequest' name='bkfirstname' rows="3"  placeholder="" /> 
						</div>
						<div class="input-group" style='padding:20px 20px 20px 20px;' >
							<span class="input-group-addon input-info">{{zang.lasttag.vl}} <i class="glyphicon glyphicon-info-sign"></i></span>
							<input type="text" ng-model='mydata.lastname' class="form-control input" id='bkspecialrequest' name='bklastname' rows="3"  placeholder=""  />
						</div>
						<div class='input-group'  style='padding:20px 20px 20px 20px;'>
							<div class='input-group-btn xls' dropdown  is-open="coveropened"  >
								<button type='button' class='btn btn-default dropdown-toggle sm'   data-toggle='dropdown' >
									<i class="glyphicon glyphicon-cutlery"></i>&nbsp;<span class='caret'></span>
								</button>
								<ul class='dropdown-menu scrollable-menu'>
<!--									<li ng-repeat="p in mydata.pers"><a href="javascript:;" ng-click="mydata.npers = p;">{{ p}}</a></li>-->
                                                                        <li ng-repeat="p in mydata.pers"><a href="javascript:;" ng-click="setmealtime(p, 2);">{{ p}}</a></li>
                                                                        
								</ul>
							</div>
							<input type='text'is-open="coveropened"  ng-model='mydata.npers' class='form-control input sm' placeholder="number of persons"  ng-click="opendropdown($event, 'cover')" readonly >
						</div>
						<div class="input-group xls calendarbk" style='padding:20px 20px 20px 20px;'>
									<span class="input-group-addon" ng-click="opendropdown($event, 'date')">
										<i class="glyphicon glyphicon-calendar"></i> &nbsp; 
										<span class='caret'></span>
									</span>
									<input type="text" class="form-control input" id='bkdate' name='bkdate' 
										   ng-model="mydata.selecteddate" 
										   ng-click="opendropdown($event, 'date')" 
										   ng-required="true" 
										   ng-change='updatendays();' 
										   datepicker-popup="{{format}}" 
										   datepicker-options="dateOptions" 
										   is-open="dateopened" 
										   min-date="mydata.minDate" 
										   max-date="mydata.maxDate" 
										  close-text="close" 
										   readonly />
						</div>

						<div class="input-group"  style='padding:20px 20px 20px 20px;'>
							<div class='input-group-btn xls' dropdown is-open="mtimeopened">
								<button type='button' class='btn btn-default dropdown-toggle sm'  data-toggle='dropdown'><i class="glyphicon glyphicon-time"></i>&nbsp;<span class='caret'></span>
								</button>
								<ul class='dropdown-menu scrollable-menu' style='font-size:12px;height: auto;
									max-height: 200px;
									overflow-x: hidden;'>
									<li style='color:#5285a0'>&nbsp;lunch  &nbsp;<span class='caret'></span></li>
<!--									<li ng-repeat="x in lunch"><a href ng-click="mydata.ntimeslot = x;">{{x}}</a></li>-->
                                                                        <li ng-repeat="x in lunch"><a href ng-click="setmealtime(x, 1);">{{x}}</a></li>
									<li>&nbsp;  -------------------- </li>
									<li style='color:#5285a0'>&nbsp;  dinner  &nbsp;<span class='caret'></span></li>
<!--									<li ng-repeat="y in dinner"><a href ng-click="mydata.ntimeslot = y;">{{y}}</a></li>-->
                                                                        <li ng-repeat="y in dinner"><a href ng-click="setmealtime(y, 1);">{{y}}</a></li>
								</ul>
							</div>
							<input type='text' is-open="mtimeopened" ng-model='mydata.ntimeslot' class='form-control input sm' placeholder="arrival time" readonly >
						</div>
						<div class="input-group" style='padding:20px 20px 20px 20px;' >
							<span class="input-group-addon input-info">{{zang.requesttag.vl}} <i class="glyphicon glyphicon-info-sign"></i></span>
							<textarea type="text" ng-model='mydata.specialrequest' class="form-control input" id='bkspecialrequest' name='bkspecialrequest' rows="3"  placeholder="" ></textarea>
						</div>
						<br />
						<a id="bookingcancel" class="btn btn-blue"   ng-click="showcancel()"  style="border-radius:2px;color:#2AA8CF">{{zang.buttoncancel.vl}}</a>
						<button type="submit" class="btn btn-blue btnupdate" >{{zang.buttonmodify.vl}}</button>
                                                
						<!--    <button type="button" class="btn btn-warning" ng-click="mydata.cancel()">Cancel</button><br />&nbsp;<br/>-->
					</form>
                                </div>
						<div id="updatemessage" ng-show='bkupdatemsg' style="margin-left:15px;margin-right:15px;text-align:center">
							 <h4> {{zang.bkgupdated.vl}}.</h4> <!-- Your reservation has been Updated. -->
						</div>
						<div id="updatemessagefail" ng-show='bkupdatemsgfail' style="margin-left:15px;margin-right:15px;text-align:center;color:red;">
							 <h4> {{zang.bkgupdatedfail.vl}}.</h4> <!-- Your reservation has been Updated. -->
						</div>
						<div id="updatemessage" ng-show='bkcancelmessage' style="margin-left:15px;margin-right:15px;text-align:center">
						 <h4> {{zang.bkgcanceled.vl}}.</h4> <!-- Your reservation has been cancelled. -->
						</div>
						<div id="updatemessage" ng-show='noupdate' style="margin-left:15px;margin-right:15px;text-align:center">
						 <h4>OOPS !! Sorry ! Unable to update your reservation . <br/>Possible reason is that the date of reservation has already passed </h4>
						</div>

				  <!-- cancelbooking !-->
				  <div class="survet-form" ng-show="bkcancel" >
						<div class="survey-conatiner" >
							<p style="color:#2AA8CF">{{zang.bkgreason.vl}}.</p></br> 
							<input  type="radio" name="cl-servey"  ng-model='cancelServey' value="Unable to attend on this date"  > {{zang.bkgenable.vl}} </br></br>
							<input  type="radio" name="cl-servey" ng-model="cancelServey" value="My Friend(s) cancelled our Date"  > {{zang.bkgfriend.vl}} </br></br>
							<input  type="radio" name="cl-servey" ng-model="cancelServey" value="I no longer want to dine at this Restaurant" > {{zang.bkgnolonger.vl}} </br></br>
							<input  type="radio" name="cl-servey" ng-model="cancelServey" value=" I wish to change my Booking"  >  {{zang.bkgchgbkg.vl}} </br></br>
							<input  type="radio" name="cl-servey" ng-model="cancelServey" value="Other"  /> {{zang.bkgother.vl}} : <input id="othermsg" ng-model="$parent.other" value="" type="text" name="other" /></br>
						</div>
                                                <div class="cancel-policy" ng-if ='isPayment===1'>
                                                    <h4 style='text-align:center;'>Restaurant Cancel Policies</h4><br />
                                               
                                                    <div  ng-repeat ="p in cancelpolicy" style='padding-left:5px;' ng-if='restaurant !="SG_SG_R_TheFunKitchen"  || restaurant !="SG_SG_R_Bacchanalia" '>
                                                           <p ng-if="p['percentage']==100">{{p['duration']}}SGD {{totalcharge}}. 
                                                            <p ng-if="p['percentage']==0">{{p['duration']}}</p>
                                                    </div>
                                                     <div ng-if='restaurant =="SG_SG_R_TheFunKitchen"  || restaurant =="SG_SG_R_Bacchanalia" ' style="padding-left:5px;"> 
                                                        <span >{{policybaccahanalia}}</span>
                                                    </div>
                                                    <p  style="font-size:13px;padding-left:5px;" ng-if ="restaurant !== 'SG_SG_R_Pollen'">{{cancelmsg}}</p>
                                                      <div ng-if ="restaurant === 'SG_SG_R_Pollen'"> 
                                                            <p ng-repeat="str in paymenttncAr">{{ str }} </p>
                                                     </div>
                                                </div>
						<button type="button" class="btn btn-blue btncancel" ng-click="cancelConfirmation($index,item);" >{{zang.buttoncancel.vl}}</button>
						 <a id="bookingcancel" ng-if="isPayment === 0 && isWaived === 0 && isModifyflg == 12 " class="btn btn-blue"  data-target="targetSectionmodif_{{$index}}" ng-click="backtoupdate()"  style="border-radius:2px;color:#2AA8CF"">{{zang.backto.vl}} '{{zang.buttonmodify.vl}}'</a>
                                                 <a id="bookingcancel" ng-if="isPayment === 1 && bacchaflg === true" class="btn btn-blue"  data-target="targetSectionmodif_{{$index}}" ng-click="backtoupdate()"  style="border-radius:2px;color:#2AA8CF"">{{zang.backto.vl}} '{{zang.buttonmodify.vl}}'</a>
                                        
					</div>
					<div class="confirmation-details" ng-show="bkcancelconfirm" >
						<div id="confirm-conatiner" >
						   <ul>
							  <li>  Restaurant : <span id='res_title'>{{title}} </span></li></br>
							  <li>  Confirmation : <span id='res_confirmid' ng-model="confirmation">{{confirmation}} </span></li></br>
							  <li>  Date : <span> {{date}}</span></li></br>
							  <li>  Time : <span > {{rtime}}</span></li></br>
							  <li>  Number of guests : <span > {{cover}}</span></li></br>
                                                        
 						   </ul>
                                               
                                                    <div class="cancel-policy" ng-if ='isPayment === 1'>
                                                        <h4 style='text-align:center;' ng-if='tnc_amount >0'>Restaurant Cancel Policies</h4><br />
                                                         <p  ng-if ='isPayment === 1 && tnc_amount >0 && payment_method.trim() !=="carddetails"' style='margin-left:10px;font-size:14px;' >you will be refund amount : SGD <span>  {{tnc_amount }}</span></p></br >
                                                    <p  ng-if ='isPayment === 1 && payment_method.trim()==="carddetails" && tnc_amount >0' style='margin-left:10px;font-size:14px;' >A fee of  <strong> SGD <span>  {{tnc_amount }}</span> </strong> will be charged upon cancellation of this reservation.</p></br >
                                                
                                               
                                                    <p  ng-if ='isPayment === 1 && payment_method.trim() === "carddetails" && tnc_amount ===0' style='margin-left:10px;font-size:14px;' >Due to the cancel policies, this cancel is FREE of charge.</p></br >
                                                    
                                                    </div>
      
                                                    
						</div>
                                          
						<a id="bookingcancel" class="btn btn-blue"  ng-if='isPayment === 0 && isAllowModifyflg === 1' data-target="targetSectionmodif_{{$index}}" ng-click="backtoupdate()"  style="border-radius:2px;color:#2AA8CF"">{{zang.backto.vl}} '{{zang.buttonmodify.vl}}'</a>
                                                <a id="bookingcancel" ng-if="isPayment===1 && bacchaflg === true" class="btn btn-blue"  data-target="targetSectionmodif_{{$index}}" ng-click="backtoupdate()"  style="border-radius:2px;color:#2AA8CF"">{{zang.backto.vl}} '{{zang.buttonmodify.vl}}'</a>
                                                <div ng-if='isPayment===1'><button type="button" id="cancel-refund" class="btnconfirmCancel" ng-click="cancelbookingrefund();">Confirm Cancel</button></div>
                                                <div ng-if='isPayment===0'><button type="button" class="btnconfirmCancel" ng-click="cancelbooking();">Confirm Cancel</button></div>
						
					</div>
					
			</div>
			</div> 

		</div>

	</div>
<script type='text/javascript' src='js/tradService.js'></script>
<script>


function DataBooking() {

	var i, j, timeslotAr = [],
			persAr = [],
			hourslotAr = [],
			minuteslotAr = [];

//                            for (i = 1; i < 10; i++)
//                                persAr.push(i);

	for (i = 9; i < 24; i++)
		for (j = 0; j < 60; j += 15) {
			timeslotAr.push((i < 10 ? '0' : '') + i + ':' + (j === 0 ? '0' : '') + j);
		}

	for (i = 9; i < 24; i++)
		hourslotAr.push((i < 10 ? '0' : '') + i);

	for (i = 0; i < 60; i += 5)
		minuteslotAr.push((i < 10 ? '0' : '') + i);

	return {
		name: "",
		npers: 0,
		selecteddate: null,
		ntimeslot: 0,
		specialrequest: "",
		timeslot: timeslotAr,
		hourslot: hourslotAr,
		minuteslot: minuteslotAr,
		pers: persAr,
		opers: 0,
		otime: '',
		odate: '',
		event: ['Birthday', 'Wedding', 'Reunion'],
		opened: false,
		minDate: null,
		maxDate: null,
		dateOptions: {
				startingDay: 1
				},
              
		setInit: function (ddate, rtime, npers, name, specialrequest,firstname,lastname,minpax,maxpax) {
					for (i = minpax; i <= maxpax; i++)
				 		persAr.push(String(i));

			this.pers = persAr;
			this.timeslot = timeslotAr;
			this.hourslot = hourslotAr;
			this.minuteslot = minuteslotAr;
			this.ntimeslot = rtime.substring(0, 5);
			this.name = name;
			this.npers = npers;
			this.specialrequest = specialrequest;
			this.firstname = firstname;
			this.lastname = lastname;

			if (ddate instanceof Date === false) {
				var t0, t1;
				t0 = ddate.split('-');
				t1 = this.ntimeslot.split(':');
				this.selecteddate = new Date(parseInt(t0[0]), parseInt(t0[1]) - 1, parseInt(t0[2]), parseInt(t1[0]), parseInt(t1[1]), 0);
			
			} else
				this.selecteddate = ddate;
   
			this.selecteddate = ddate;
			this.odate = this.selecteddate;
			this.otime = this.ntimeslot;
			this.opers = npers;

			this.minDate = new Date();
			this.maxDate = new Date();
			this.maxDate.setTime(this.maxDate.getTime() + (60 * 24 * 3600 * 1000)); // remainingday  120 days
	   
			},

		calendar: function () {
			var cdate = this.selecteddate;
			this.selecteddate = ((cdate.getDate() <= 9) ? '0' : '') + cdate.getDate() + "/" + ((cdate.getMonth() <= 8) ? '0' : '') + (cdate.getMonth() + 1) + "/" + cdate.getFullYear();
			console.log("calender="+this.selecteddate);
			},
			
		ischange: function () {
			return !(this.npers === this.opers && this.ntimeslot === this.otime && this.selecteddate === this.odate);
			}
		};
}


app.controller('UpdateController', function ($scope, $http, $locale, $localStorage, tradService,HttpServiceAvail) {
    $scope.bkupdate =true;
	$scope.mydata = new DataBooking();
	//console.log(JSON.stringify($scope.mydata));
	$scope.error12 = 'not available for lunch';
	$scope.error13 = 'not available for dinner';
	var dropdown_label = ['cover', 'date', 'mtime'];

	for (i = 0; i < dropdown_label.length; i++)
		$scope[dropdown_label[i] + 'opened'] = false;

	$scope.langue = <?php echo "'" . $langue . "';"; ?>
	$scope.restaurant = <?php echo "'" . $theRestaurant . "';"; ?>;
	$scope.rdate =<?php echo "'" . $rdate . "';"; ?>;
       	$scope.rtime =<?php echo "'" . $rtime . "';"; ?>;
	$scope.cover =<?php echo "'" . $npers . "';"; ?>;
	$scope.confirmation =<?php echo "'" . $confirmation . "';"; ?>;
	$scope.specialrequest =<?php echo "'" . $specialreq . "';"; ?>;
	$scope.email =<?php echo "'" . $email . "';"; ?>;
	$scope.firstname = <?php echo "'" . $firstname . "';"; ?>;
	$scope.lastname = <?php echo "'" . $lastname . "';"; ?>;
	$scope.minpax =<?php echo $minpax . ";"; ?>;
	$scope.maxpax =<?php echo $maxpax . ";"; ?>;
        $scope.isPayment =<?php echo $isPayment ?>;
        $scope.totalcharge =<?php echo $totalcharge ?>;
        $scope.tnc_amount =<?php echo $tnc_amount ?>;
        $scope.deposit_id =<?php echo "'".$deposit_id." ';"; ?>;
        $scope.payment_method =<?php echo "'".$payment_method." ';"; ?>;
        $scope.bookingid =<?php echo "'" . $bookingid . "';"; ?>;
        $scope.pproduct = <?php echo "'" . $bkproduct . "';"; ?>; 
        $scope.status = <?php echo "'" . $status . "';"; ?>;
        $scope.isWaived = <?php echo $isWaived ?>;
        $scope.generic = <?php echo "'" . $generic . "';"; ?>;
        $scope.isModifyflg = <?php echo $isAllowModifyflg . ";"; ?>;
    	$scope.extrachoice = [];var validOptions = ['event', 'duration', 'repeat', 'notifysmswait', 'notestext', 'notescode', 'validate', 'orders','choice'];

   
	
        $scope.parseObj = function(str) {
            if(typeof str !== "string" || str === "")
                return {};
            
            str = str.replace(/’/g, "\"");
            if(str !== '')
                str = str.replace(/\s+/g, " ");
            try {
            var oo = JSON.parse(str);
            } catch(err) { console.log(err, str); return {}; }
            return (oo) ? oo : {};
        };



     
        
        
        var AvailperPax = <?php echo "'" . $AvailperPax . "';"; ?>; 



        $scope.bacchaflg = false;
        
        //set default meal type
        if($scope.rtime !== undefined ){
            var time = $scope.rtime.split(':');

            $scope.mealtype = (time[0]>16) ? 'dinner' :'lunch';
        }

	$scope.reset = function(item) {
		$scope.bkupdate = false;
		$scope.bkupdatemsg = false
		$scope.bkupdatemsgfail = false;
		$scope.bkcancel = false;
		$scope.bkcancelconfirm = false;
		$scope.bkcancelmessage = false;
		$scope.noupdate = false;
		$scope[item] = true;	
        };
	
    
      

	var rdate = $scope.rdate.split('-');
		var rtime = $scope.rtime.split(':');
                var month;
               
                 if(rdate[1]<= 9){month = '0'+parseInt(rdate[1])}else{month=parseInt(rdate[1]);}
		$scope.rdate = parseInt(rdate[0])+"-"+ month+"-"+parseInt(rdate[2]);
		
		var d = new Date(parseInt(rdate[0]), parseInt(rdate[1]-1), parseInt(rdate[2]), rtime[0], rtime[1]);
		var currentTime = new Date();
  
		if (currentTime.getTime() - d.getTime() > 0) {
			$scope.passed = true;
		} else {
			$scope.passed = false;
		}

        
            if($scope.status!=="" || $scope.passed===true  ){
                    $scope.reset('noupdate'); 
            }else{
                    $scope.reset('bkupdate');  
            }

	
  
	$scope.showcancel = function(){
            
	   $scope.reset('bkcancel'); 
	}
	$scope.backtoupdate =function(){
            $scope.reset('bkupdate');  
	}
        

	var API_URL = 'api/restaurant/allote/' + $scope.restaurant;
	$http.get(API_URL).then(function (response) {
	   if(response && typeof response !=='undefiend'){
			$scope.lunchdata = response.data.data.lunchdata;
			$scope.dinnerdata = response.data.data.dinnerdata;
			var nday = $scope.numberOfDay($scope.rdate);
			$scope.updateTimerScope(nday, $scope.rtime);
                         
		}
	});

        if($scope.restaurant === 'SG_SG_R_Bacchanalia'){
                $scope.bacchaflg = true;
                
                if($scope.tnc_amount > 0 ){
                    $scope.bacchaflg = false;
                }else if($scope.isWaived === 1){
                    $scope.bacchaflg = false;
                     
                }
            var API_URL  = 'api/payment/stripe/credentials/'+$scope.restaurant;
            $http.get(API_URL).then(function (response) {
                if(response.data.data !=='undefined'){
                    $scope.publishable_key = response.data.data.publishable_key;
                }

            });
        }

    $scope.cancelpolicy = function(){
    	if($scope.generic && typeof $scope.generic === "string" && $scope.generic !== "") { 
        	$scope.extrachoice = $scope.parseObj($scope.generic);   
        	Object.keys($scope.extrachoice).map(function(x) { if(validOptions.indexOf(x) >= 0) $scope.extrachoice[x] = $scope.extrachoice[x]; });      
        	if($scope.extrachoice && $scope.extrachoice.choice) {
            		 $scope.pproduct = $scope.extrachoice.choice;
            	}                      
      	}
  
            if($scope.restaurant === "SG_SG_R_Bacchanalia" ){ 
                 var API_URL = "api/v2/restaurant/cancelpolicy/getPolicy";
                $http({
                    url: API_URL,
                    method: "POST",
                    data: $.param({
                            'restaurant': $scope.restaurant,
                            'date' 	: $scope.rdate,
		            		'time'	: $scope.rtime,
                            'pax' : $scope.cover
                }),
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
             }).success(function(response) {
                    var data = response.data;
                        if (typeof data !== 'undefined' ) {
                           $scope.policybaccahanalia = data.priceDetails.message;  
                           $scope.updatecharge = data.priceDetails.charge * $scope.cover;
                           if(data.priceDetails.requiredccdetails){
                              $scope.isPayment = 1;
                            }
                           if($scope.isWaived === 1){
                               $scope.isPayment = 0;
                               //$scope.bacchaflg === false;
                           }

                        }

                       
              });
            }
            else if($scope.restaurant === "TH_BK_R_Medinii" || $scope.restaurant === "TH_BK_R_BangkokHeightz" ){
                $scope.isPayment = 0;
                $scope.isWaived  = 1;
            }
//            else if($scope.restaurant === "SG_SG_R_TheFunKitchen" || $scope.restaurant === "SG_SG_R_Esquina"){
//                return $http.post("api/services.php/cancelpolicy/bkdepsoit",
//                    {
//                        'restaurant': $scope.restaurant,
//                        'type':  $scope.payment_method,
//                        'amount' :$scope.totalcharge,
//                        'product':$scope.pproduct,
//                        'date' 	: $scope.rdate,
//		        'time'	: $scope.rtime,
//                        'pax' : $scope.cover,
//                        'payment_id' :  $scope.deposit_id
//                    }).then(function (response) {
//                        var data = response.data.data;
//                        if(typeof data!=='undefined'){
//                            $scope.cancelmsg = data.message;
//                            $scope.cancelpolicy = data.range;
//                            $scope.freerange =data.lastRange;
//                        }
//                 });
//                
//            }else{ 
                else {
                return $http.post("api/services.php/cancelpolicy/list",
                    {
                        'restaurant': $scope.restaurant,
                        'type':  $scope.payment_method,
                        'amount' :$scope.totalcharge,
                        'product':$scope.pproduct,
                        'date' 	: $scope.rdate,
		        'time'	: $scope.rtime,
                        'pax' : $scope.cover
                    }).then(function (response) {
                        var data = response.data.data;
                        if($scope.restaurant === 'SG_SG_R_Esquina' && $scope.isPayment === 0){
                            $scope.isWaived  = 1;
                        }
                        if(data){
                            $scope.cancelmsg = data.message;
                            
                            if($scope.restaurant === "SG_SG_R_Pollen" || $scope.restaurant === "SG_SG_R_LaTableDeLydia" ) {
                            	 //data.priceDetails.message = $scope.frontinfo;
                           	 $scope.paymenttncAr = data.message.split('-'); 
                            }
                            $scope.cancelpolicy = data.range;
                            $scope.freerange = data.lastRange;
                            
                        }
                 });
            }
                   
        };
//        if($scope.isPayment === 1 && $scope.bacchaflg === false){
//            $scope.showcancel();
//            $scope.cancelpolicy();
//        } else if ($scope.isPayment === 1 && $scope.bacchaflg === true && $scope.isWaived === 0){
//             $scope.cancelpolicy();
//        }else if($scope.bacchaflg === false && $scope.isPayment === 0 && $scope.isWaived === 1 ){
//             $scope.showcancel();
//        }else if($scope.bacchaflg === false && $scope.isPayment === 0 && $scope.isWaived === 1 && $scope.restaurant === 'SG_SG_R_BurntEnds' ){
//             $scope.showcancel();
//        }else if($scope.isPayment === 0  && ($scope.restaurant === 'SG_SG_R_Esquina' || $scope.restaurant === 'SG_SG_R_Pollen' || $scope.restaurant === 'SG_SG_R_Nouri') ){
//               $scope.isWaived = 1;
//               $scope.showcancel();
//        }
        
        if($scope.isModifyflg === 11 ){
              $scope.showcancel();
              $scope.cancelpolicy();
        }else if($scope.isPayment === 1 && $scope.isModifyflg === 12){
              $scope.cancelpolicy();
        }


    $scope.mydata.setInit($scope.rdate, $scope.rtime, $scope.cover, $scope.confirmation,$scope.specialrequest,$scope.firstname,$scope.lastname,$scope.minpax,$scope.maxpax);

	$scope.numberOfDay = function (aDate) {
		var dateAr = aDate.split('-');
		var date = new Date(parseInt(dateAr[0]), parseInt(dateAr[1] - 1), parseInt(dateAr[2]), 23, 59, 59);
		return Math.floor(((date.getTime() - Date.now()) / (24 * 60 * 60 * 1000)));
	};

	$scope.updateTimerScope = function (n, bktime) {
		var nchar_lunch, nchar_dinner, index, patlunch, patdinner, curval, found, localtime, limit, i, k, log;

		if (n < 0) {
			n = 0;
		}
		$scope.curday = n;
		$scope.lunch = [];
		$scope.dinner = [];
		nchar_lunch = 4;
		nchar_dinner = 4;
		index = n * nchar_lunch;
		patlunch = parseInt('0x' + $scope.lunchdata.substring(index, index + nchar_lunch));
		index = n * nchar_dinner;
		patdinner = parseInt('0x' + $scope.dinnerdata.substring(index, index + nchar_dinner));
                		
		curval = bktime;
		found = 0;
		localtime = "";
		limit = nchar_lunch * 4;
		for (log = i = 0, k = 1; i < limit; i++, k *= 2)
			if ((patlunch & k) === k) {
				log++;
				ht = (9 + Math.floor(i / 2)) + ':' + ((i % 2) * 3) + '0';
				if (ht === curval)
					found = 1;
				$scope.lunch.push(ht);
			}
        
	
		if (log === 0) {
            $scope.lunch.push($scope.error12);		// 'not available for lunch'
			localtime = $scope.error12;
            $scope.mydata.ntimeslot ="";
		}

		limit = nchar_dinner * 4;
		//if(limit > 17) limit = 16;	// minight
		for (log = i = 0, k = 1; i < limit; i++, k *= 2)
			if ((patdinner & k) === k) {
				log++;
				ht = (16 + Math.floor(i / 2)) + ':' + ((i % 2) * 3) + '0';
				if (ht === curval)
					found = 1;
				$scope.dinner.push(ht);
			}
			
	
		if (log === 0) {
			$scope.dinner.push('not available for dinner');		//'not available for dinner'
			localtime = 'not available for dinner';
                    $scope.mydata.ntimeslot ="";
			}

		curval = bktime;
             
                if($scope.dinner.indexOf(curval) < 0){
                     $scope.mealtype = 'lunch';
                }else{
                    $scope.mealtype = 'dinner';
                }

                 
		if (localtime.substr(0, 5) === 'close') {
			curtime = localtime;
			$scope.bktime = curtime;
		}
		else if (curval.substr(0, 5) === 'close' || found === 0) {
			$scope.bktime = '';
		}
                
	};
	
	$scope.opendropdown = function ($event, selector) {

		$scope.cleardropdown(selector);

		$scope[selector + 'opened'] = ($scope[selector + 'opened']) ? false : true;
			if (selector === "date" ) {
				$event.preventDefault();
				$event.stopPropagation();
			}
	};
	
	$scope.cleardropdown = function (selector) {

			for (i = 0; i < dropdown_label.length; i++)
				if (selector != dropdown_label[i]) {
						$scope[dropdown_label[i] + 'opened'] = false;
				}

	};
        $scope.auditlog = function(page,others,action){
            
            var API_URL ="api/v2/system/tracking/log";
               $http({
                    url: API_URL,
                    method: "POST",
                    data: $.param({
                        'action': action,
                        'event' :$scope.bookingid,
                        'page' :page,
                        'others' :others
                    }),
                     headers:  {'Content-Type': 'application/x-www-form-urlencoded'}
                    }).then(function(response) { });
            
        };
        
        
       //
       $scope.setmealtime = function (tt, section) {
         
		switch (section) {
			case 1: 
                                // time
				$scope.mydata.ntimeslot = tt;
                                $scope.rtime = tt;
                                
                                if($scope.dinner.indexOf($scope.rtime) < 0){
                                    $scope.mealtype = 'lunch';
                                }else{
                                    $scope.mealtype = 'dinner';
                                }
                              
				if (AvailperPax == '1')
					$scope.checkPerPaxData();
				break;

			case 2: 	// number of pers
				$scope.mydata.npers = String(tt);
                                $scope.cover = String(tt);
//                                if($scope.mealtype === 'lunch' &&  $scope.mydata.npers < 5){
//                                        $scope.bacchaflg = false;
//                                }else{
//                                    $scope.bacchaflg =true;
//                                }
				if (AvailperPax == '1')
					$scope.checkPerPaxData();
				break;
			default:
                            
		}
             
                if($scope.bacchaflg === true && $scope.isWaived === 0){
                     $scope.cancelpolicy();
                }
                if($scope.mealtype === 'lunch' && parseInt($scope.cover) < 5 ){
                    $scope.bacchaflg = false;
                }
      
//                 
	};
           //$scope.auditlog('update_booking','','50');

	//update booking submit function
       $scope.mydata.submit = function() {
              $scope.auditlog('update_booking','','103');

	   if(typeof $scope.mydata.ntimeslot !== "string" || 
                    $scope.mydata.ntimeslot.trim() === "" || 
                    $scope.mydata.ntimeslot === 'not available for lunch' || 
                    $scope.mydata.ntimeslot === 'not available for dinner'){

                    $scope.msg = "Date and time Invalid!..";
                    alert($scope.msg);
                    return false;
	        }
            //stripe Payment integration
            
            var handler = StripeCheckout.configure({
                         key: $scope.publishable_key,   //'pk_test_aG4RNDkBJI81YsT2ljQGVnuW',
                    token: function (token) {
               
                       if($scope.totalcharge > $scope.updatecharge || $scope.totalcharge === $scope.updatecharge ){
                           $scope.updateccdeails = false;
                       }else if($scope.totalcharge < $scope.updatecharge){
                           $scope.updateccdeails = true;
                       }
                      var API_URL = 'api/booking/update/payment_booking';
                      $http({
                                url: API_URL,
                                method: "POST",
                                data: $.param({
                                        'token': token.id,
                                        'current_charge' : $scope.updatecharge,
                                        'confirmation':$scope.confirmation,
                                        'payment_type' :$scope.payment_method,
                                        'npers': $scope.mydata.npers,
                                        'selecteddate': $scope.mydata.selecteddate,
                                        'ntimeslot': $scope.mydata.ntimeslot,
                                        'specialrequest':$scope.mydata.specialrequest,
                                        'firstname':$scope.mydata.firstname,
                                        'lastname':$scope.mydata.lastname,
                                        'mealtype' :$scope.mealtype,
                                        'restaurant' : $scope.restaurant,
                                         'email' :$scope.email,
                                        'type':'edmupdate'
                                }),
                                headers: {
                                        'Content-Type': 'application/x-www-form-urlencoded'
                                }
                    }).then(function(response) {
                        if(response.data && response.data.status > 0)
                            $scope.reset('bkupdatemsg');
                        else 
                           $scope.reset('bkupdatemsgfail');
                        
                    });

                    }
             //   col-lg-offset-2 col-md-offset-2 col-sm-offset-2 col-lg-8 col-md-8 col-sm-8 col-xs-12 my-booking
                });
            if($scope.isPayment === 1 && $scope.bacchaflg === true){
             
                handler.open({
                    email: $scope.email,
                    panelLabel: 'Submit'
                });
                
             //end payment intgration
              
            }else{
              var API_URL = 'api/booking/update';

                            var cdate = $scope.mydata.selecteddate;

                            $http({
                                    url: API_URL,
                                    method: "POST",
                                    data: $.param({
                                            confirmation: $scope.confirmation,
                                            npers: $scope.mydata.npers,
                                            selecteddate: $scope.mydata.selecteddate,
                                            ntimeslot: $scope.mydata.ntimeslot,
                                            specialrequest:$scope.mydata.specialrequest,
                                            firstname:$scope.mydata.firstname,
                                            lastname:$scope.mydata.lastname,
                                            type:'edmupdate'
                                    }),
                                    headers: {
                                            'Content-Type': 'application/x-www-form-urlencoded'
                                    }
                    }).then(function(response) { if(response.data && response.data.status > 0) $scope.reset('bkupdatemsg'); else $scope.reset('bkupdatemsgfail');
                    });
            }
	};

	$scope.updatendays = function () {
			var nday;
			var bdate = $scope.mydata.selecteddate;
	//data.selecteddate = ((cdate.getDate() <= 9) ? '0' : '') + cdate.getDate() + "/" + ((cdate.getMonth() <= 8) ? '0' : '') + (cdate.getMonth() + 1) + "/" + cdate.getFullYear();
			bdate= bdate.getFullYear()+"-" +  ((bdate.getMonth() <= 8) ? '0' : '') + (bdate.getMonth() + 1) + "-" + ((bdate.getDate() <= 9) ? '0' : '') + bdate.getDate();
			$scope.mydata.selecteddate = bdate;
			$scope.bkdate =bdate;
			nday = $scope.numberOfDay($scope.bkdate);
			$scope.updateTimerScope(nday, $scope.mydata.ntimeslot);
                        $scope.rdate = $scope.bkdate;
                        if (AvailperPax == '1')
			$scope.checkPerPaxData();
                    if($scope.isPayment === 1 && $scope.bacchaflg === true){
                        $scope.cancelpolicy();
                     }

	};
	
	$scope.cancelConfirmation =function(){
		var  slName = "cl-servey";
		 $scope.msg = $scope.zang.bkgselreason.vl + ".!!"//"Please select the main reason for your cancellation.!!";
		$scope.servey = $('input[name=' + slName + ']:radio:checked').val();
		if ($scope.servey === undefined) {
			alert($scope.msg);
			 return false;
		}
		 if ($scope.servey === 'Other') {
			console.log("servey="+$scope.servey);
			if ($scope.other === "" || $scope.other === undefined) {
				 alert($scope.msg);
				return false;    
			}
			else {
				$scope.servey = $scope.other;
			}
		}
		var API_URL = 'api/confirmation/' + $scope.confirmation + '/' + $scope.email;
			$http.get(API_URL, {
				cache: true,
			}).then(function(response) { 
                            $scope.reset('bkcancelconfirm');
				$scope.title =response.data.data.title;
				$scope.rdate =response.data.data.date;
				$scope.cover =response.data.data.pers;
				 var days = ['Sun','Mon','Tue','Wed','Thuy','Fri','Sat'];

				var months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
				var dateAr = $scope.rdate.split('-');
	   
					var date = new Date(parseInt(dateAr[0]), parseInt(dateAr[1]-1), parseInt(dateAr[2]));
					var dayofweek = days[ date.getDay() ];
					var month = months[ date.getMonth() ];
					$scope.date =dayofweek+", "+parseInt(dateAr[0])+" "+ month +" "+ parseInt(dateAr[2]);
				  
                    });
		
        };
		
		



	$scope.cancelbooking =function(){
            $scope.auditlog('update_booking','','101');
		var API_URL = 'api/visit/cancel/' + $scope.restaurant + '/' +$scope.confirmation + '/' +$scope.email;
				$http({
					url: API_URL,
					method: "POST",
					data: $.param({
						reason: $scope.survey,
						confirmation: $scope.confirmation,
						restaurant: $scope.restaurant,
						email: $scope.email,
					   
					}),
					headers: {
						'Content-Type': 'application/x-www-form-urlencoded'
					}
				}).then(function(response) { $scope.reset('bkcancelmessage');});
		
	};
        $scope.cancelbookingrefund =function(){
            $scope.auditlog('update_booking','cancel','101');
            $("#cancel-refund").attr('disabled','true');
            
		var API_URL = 'api/visit/payment/cancel/';
				$http({
					url: API_URL,
					method: "POST",
					data: $.param({
						reason: $scope.survey,
						confirmation: $scope.confirmation,
						restaurant: $scope.restaurant,
						email: $scope.email,
                                                deposit:$scope.deposit_id,
                                                amount :$scope.tnc_amount,
                                                payment_method:$scope.payment_method,
					   
					}),
					headers: {
						'Content-Type': 'application/x-www-form-urlencoded'
					}
				}).then(function(response) {
                                    if(response.data.status===1){
                                        $scope.reset('bkcancelmessage');
                                    }else{
                                        alert("OOOPS! Something Wrong" + response.data.message);
                                    }
                                    
                                    
                        
                             });
		
	};

	$scope.topic = "BOOKING";
	$scope.langcurdate = new Date().getDateFormatReverse("-");
	$scope.langversion = "1.3";
	
	$scope.setLang = function (lang, topic) {
	
		$scope.allowlang = ['en', 'fr', 'cn', 'th', 'my', 'es', 'it', 'kr', 'jp', 'hk', 'de', 'pt', 'vi', 'ru'];
		if ($scope.allowlang.indexOf(lang) < 0) {
			alert('language not supported: ' + lang);
			return;
		}

		angular.copy(locales[lang], $locale);
		// changes dt to apply the $locale changes

		tradService.translate(lang, topic, $scope.zang).then(function (response) {
			$localStorage.langdata = $scope.zang;
		});

	};
	
	angular.copy(locales['en'], $locale);
	
	$scope.langdata = $localStorage.langdata;
	if (!$scope.langdata || !$scope.langdata.topic || !$scope.langdata.topic.lb || $scope.langdata.topic.lb !== $scope.topic || $scope.langdata.topic.vl !== $scope.langue || $scope.langdata.version.lb !== $scope.langversion)
		$scope.langdata = $localStorage.langdata = "";
	else $scope.zang = $scope.langdata;

	if (typeof $scope.zang === "undefined") {
		var zreason = "Please select the reason for your update/cancel here. If you have more than one reason for update/cancellation, please tell us the main reason",
			zenable = "Unable to attend on this date",
			friend = "My Friend(s) cancelled our Date",
			nolonger = "I no longer want to dine at this Restaurant",
			other = "Other",
			chgbkg = "I wish to change my Booking",
			bkmod = "Your reservation has been updated",
			bkmodfail = "Your reservation has NOT been updated",
			bkcan = "Your reservation has been cancelled",
			selreason = "Please select the main reason for your cancellation";
				
		$scope.zang = {
			'topic':  {"lb": $scope.topic, "vl": $scope.langue, "f": "c"},
			'version': {"lb": $scope.langversion, "vl": $scope.langcurdate, "f": "c"},
			'lunchtag': {"lb": "lunch", "vl": "Lunch", "f": "c"}, "dinnertag": {"lb": "dinner", "vl": "Dinner", "f": "c"}, "guesttag": {"lb": "guests", "vl": "Guests", "f": "c"}, "titletag": {"lb": "title", "vl": "Title", "f": "c"}, "firsttag": {"lb": "firstname", "vl": "First name", "f": "c"},
			'datetag': {"lb": "Date", "vl": "Date", "f": "c"}, 'timetag': {"lb": "time", "vl": "time", "f": "c"}, "nametag": {"lb": "name", "vl": "name", "f": "c"},
			'lasttag': {"lb": "lastname", "vl": "Last name", "f": "c"}, "emailtag": {"lb": "email", "vl": "Email", "f": "c"}, "mobiletag": {"lb": "mobile", "vl": "Mobile", "f": "c"}, "requesttag": {"lb": "special request", "vl": "Special request", "f": "c"}, "requestpromotag": {"lb": "special request / Promotion Codes", "vl": "Special Request / Promotion Codes", "f": "c"},
			'today': {"lb": "today", "vl": "Today", "f": "c"}, 'clear': {"lb": "clear", "vl": "clear", "f": "c"}, 'close': {"lb": "close", "vl": "close", "f": "c"},
			'bookingdetail': {"lb": "booking details", "vl": "booking details", "f": "u"},
			'personaldetail': {"lb": "personal details", "vl": "personal details", "f": "u"},
			'slogan': {"lb": $scope.slogan, "vl": $scope.slogan, "f": "n"},
			'confinfo': {"lb": $scope.slogan, "vl": $scope.slogan, "f": "c"},
			'bookingtitlecf': {"lb": $scope.bookingtitlecf, "vl": $scope.bookingtitlecf, "f": "u"},
			'facebookfillin': {"lb": "Book with Facebook", "vl": "Book with Facebook", "f": "c"},
			'buttonbook': {"lb": $scope.buttonLabel, "vl": $scope.buttonLabel, "f": "u"},
			'buttonconfirm': {"lb": $scope.buttonConfirm, "vl": $scope.buttonConfirm, "f": "u"},
			'bookingtitle': {"lb": $scope.bookingTitle, "vl": $scope.bookingTitle, "f": "u"},
			'buttonmodify': {"lb": "Modify booking", "vl": "Modify booking", "f": "n"},
			'buttoncancel': {"lb": "Cancel booking", "vl": "Cancel booking", "f": "n"},
			'bkgupdated': {"lb": bkmod, "vl": bkmod, "f": "n"},
			'bkgupdatedfail': {"lb": bkmodfail, "vl": bkmodfail, "f": "n"},
			'bkgcanceled': {"lb": bkcan, "vl": bkcan, "f": "n"},
			'bkgreason': {"lb": zreason, "vl": zreason, "f": "n"},
			'bkgenable': {"lb": zenable, "vl": zenable, "f": "n"},
			'bkgfriend': {"lb": friend, "vl": friend, "f": "n"},
			'bkgnolonger': {"lb": nolonger, "vl": nolonger, "f": "n"},
			'bkgchgbkg': {"lb": chgbkg, "vl": chgbkg, "f": "n"},
			'bkgselreason': {"lb": selreason, "vl": selreason, "f": "n"},
			'bkgother': {"lb": other, "vl": other, "f": "n"},
			'backto': {"lb": "Back to", "vl": "Back to", "f": "n"},
			'timeouttag': {"lb": "time out", "vl": "time out", "f": "u"},
			'listtag1': {"lb": "free booking", "vl": "free booking", "f": "c"},
			'listtag2': {"lb": $scope.listTags2, "vl": $scope.listTags2, "f": "c"},
			'listtag3': {"lb": $scope.listTags3, "vl": $scope.listTags3, "f": "c"},
			'listtag4': {"lb": $scope.listTags4, "vl": $scope.listTags4, "f": "c"},
			'listtag5': {"lb": $scope.listTags5, "vl": $scope.listTags5, "f": "n"},
			'confirmsg': {"lb": $scope.confirmsg, "vl": $scope.confirmsg, "f": "n"},
			'confirmsg2': {"lb": $scope.confirmsg2, "vl": $scope.confirmsg2, "f": "n"},
			'confirmsg3': {"lb": $scope.confirmsg3, "vl": $scope.confirmsg3, "f": "n"},
			'confirmsg4': {"lb": $scope.confirmsg4, "vl": $scope.confirmsg4, "f": "n"},
			'bacchanalia': {"lb": $scope.bacchanalia, "vl": $scope.bacchanalia, "f": "n"},
			'restau_conditions': {"lb": "Conditions", "vl": "Conditions", "f": "u"},
			'mr': {"lb": "Mr.", "vl": "Mr.", "f": "c"},
			'mrs': {"lb": "Mrs.", "vl": "Mrs.", "f": "c"},
			'ms': {"lb": "Ms.", "vl": "Ms.", "f": "c"},
            'miss': {"lb": "Miss.", "vl": "Miss.", "f": "c"},
			'msg1': {"lb": $scope.msg1, "vl": $scope.msg1, "f": "n"},
			'error0': {"lb": $scope.error0, "vl": $scope.error0, "f": "c"},
			'error1': {"lb": $scope.error1, "vl": $scope.error1, "f": "c"},
			'error2': {"lb": $scope.error2, "vl": $scope.error2, "f": "c"},
			'error3': {"lb": $scope.error3, "vl": $scope.error3, "f": "c"},
			'error4': {"lb": $scope.error4, "vl": $scope.error4, "f": "c"},
			'error5': {"lb": $scope.error5, "vl": $scope.error5, "f": "c"},
			'error6': {"lb": $scope.error6, "vl": $scope.error6, "f": "c"},
			'error7': {"lb": $scope.error7, "vl": $scope.error7, "f": "c"},
			'error8': {"lb": $scope.error8, "vl": $scope.error8, "f": "c"},
			'error9': {"lb": $scope.error9, "vl": $scope.error9, "f": "c"},
			'error10': {"lb": $scope.error10, "vl": $scope.error10, "f": "c"},
			'error11': {"lb": $scope.error11, "vl": $scope.error11, "f": "c"},
			'error12': {"lb": $scope.error12, "vl": $scope.error12, "f": "n"},
			'error13': {"lb": $scope.error13, "vl": $scope.error13, "f": "n"},
			'tmc1': {"lb": $scope.tmc1, "vl": $scope.tmc1, "f": "c"},
			'tmc2': {"lb": $scope.tmc2, "vl": $scope.tmc2, "f": "c"}
		};
		
		$scope.setLang($scope.langue, $scope.topic);
	}
        
        $scope.checkPerPaxData = function () {
		var dateflg, timeflg, persflg, tt, dd;
		
		//$scope.readAllote($scope.nday, $scope.bkcover);
		tt = $scope.bktime.replace(/[^0-9:]+/, "");
		if(tt === "" || typeof tt !== 'string')
			return;
			        
		else if(tt.length < 4)
			return $scope.invalid("Invalid time, set a valid time", 'bktime');        
		
		$scope.available = 0;
		
		dateflg = ($scope.bkdate && (($scope.bkdate instanceof Date) || (typeof $scope.bkdate === "string" && $scope.bkdate.length > 7)));
		timeflg = (typeof $scope.bktime === "string" && $scope.bktime.length > 3);
		persflg = (typeof $scope.bkcover === "string" && parseInt($scope.bkcover) > 0);
		if ((dateflg && timeflg && persflg) == false)
			return;

		if(parseInt($scope.bkcover) === 1) {
			$scope.available = 1;
			return;
			}

		HttpServiceAvail.checkAvail($scope.restaurant, $scope.bkdate, $scope.bktime, $scope.bkcover, $scope.bkproduct).then(function (response) {
			if (response.data == "1")
				$scope.available = 1;
			else {
				if (response.count > 0) $scope.invalid($scope.zang['error10'].vl + ". " + response.count + " " + $scope.zang['error11'].vl);
				else $scope.invalid($scope.zang['error9'].vl); 
				}
		});
	}
});

</script>
</body>

</html>

