<?php
    require_once("conf/conf.init.inc.php");
    require_once("lib/class.booking.inc.php");

    require_once("lib/class.images.inc.php");
    require_once("lib/class.media.inc.php");
    require_once("lib/Browser.inc.php");

    require_once("lib/wpdo.inc.php");

$qString_array = array();
parse_str(@$_SERVER['QUERY_STRING'], $qString_array);

$fb_link = WEBSITE_FB_APP_ID;

if (count($qString_array) == 0) {
    $validPage = false;
}
  
$booking = new WY_Booking();

$bookingId = $booking->base64url_decode($qString_array['b']);
$urlconfirmation = $booking->base64url_decode($qString_array['rd']);
if (isset($qString_array['efg'])) {
    $socialFlag = $booking->base64url_decode($qString_array['efg']);
}

$urlconfirmation = $booking->base64url_decode($qString_array['rd']);


$details = $booking->getBookingId($bookingId, array('include_reviews' => true));
$bkstatus = "confirmed";
$canViewFb = false;
if (isset($bookingId) && $bookingId != "") {
    if ($urlconfirmation == $details->confirmation) {
        $bkrestaurant = $details->restaurantinfo->restaurant;
        $title = $details->restaurantinfo->title;
        $bktime = $details->rtime;
        $bkdate = $details->rdate;
        $bkguests = $details->cover;
        $name = $details->firstname . " " . $details->lastname;
        $email = $details->email;
        $mobile = $details->mobile;
        $status = $details->status;
        $resAddress = $details->restaurantinfo->address;
        $validPage = true;
        $mediadata = new WY_Media($details->restaurantinfo->restaurant);
        $logo = $mediadata->getLogo($bkrestaurant);
        $image = $mediadata->getDefaultPicture($details->restaurantinfo->restaurant);
        $description = "I've made a restaurant reservation at " . $title . " & would like to invite you along! Please add this to your calendar. Look forward to our fun moment together! ";
        if ($status === 'cancel') {
            $bkstatus = "cancelled";
            $description = "I'm sorry to withdraw my dining invitation at " . $title . " Have a great day!";
        }
        $rdate = date("F j, Y, g:i a", mktime(substr($bktime, 0, 2), substr($bktime, 3, 4), 0, intval(substr($bkdate, 5, 2)), intval(substr($bkdate, 8, 2)), intval(substr($bkdate, 0, 4))));

        if($socialFlag ===$rdate){ $canViewFb = true; }
      
        } else {
            $validPage = false;
            $message = "Invalid Url";
        }
        } else {
        $validPage = false;
        $message = "Invalid Url";
        }


//var_dump($details);

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html id="public" xmlns:fb="http://ogp.me/ns/fb#"> <head>
        <meta property="og:type" content="article" />
    <?php if (@$title) { ?>
        <meta property="og:title" content="<?php echo $title ?>" />
    <?php } else { ?>
        <meta property="og:title" content="Book Restaurant Get Promotion | Place to eat Singapore | Weeloy" />
    <?php } ?>

    <?php if (@$description) { ?>
        <meta property="og:description" content="<?php echo $description ?>" />
    <?php } else { ?>
        <meta property="og:description" content="Experience a new way of enjoying food with Weeloy.com Discover exclusive promotions in your favorite restaurant and book the best place to eat in Singapore" />
    <?php } ?>
        <meta property="og:image:width" content="450"/>
        <meta property="og:image:height" content="298"/>
        <meta property="og:image" content="<?php echo $image ?>" />
        <meta property="og:url" content="<?php echo $fb_link ?><?php echo $qString_array['b'] ?>&rd=<?php echo $qString_array['rd'] ?>" />
        <meta property="og:locale" content="en_US" /> 
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta http-equiv='expires' content='mon, 1 jan 1998 05:00:00 gmt'>
        <meta http-equiv='pragma' content='no-cache'>
        <meta http-equiv='pragma' content='cache-control: max-age=0'>
        <meta http-equiv='last-modified' content='mon, 1 jan 1998 05:00:00 gmt'>
        <meta http-equiv='cache-control' content='no-cache, must-revalidate'>
        <meta name='robots' content='noindex, nofollow'>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
        <meta name="HandheldFriendly" content="true" />
        <title>Weeloy Code</title>
        <link href="../../../css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="../../../css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
        <link href="../../../css/bootstrap-select.css" rel="stylesheet" >
        <link href="../../../css/bootstrap-social.css" rel="stylesheet" >
        <link href="../../../css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="../../../css/bookstyle.css" rel="stylesheet" type="text/css" />
        <link href='https://fonts.googleapis.com/css?family=Montserrat|Unkempt|Roboto:400,900,700italic,700,500italic,500,400italic' rel='stylesheet' type='text/css'>
        <script type="text/javascript" src="../../../js/jquery.min.js"></script>
        <script type="text/javascript" src="../../../js/bootstrap.min.js"></script>

        <style>
<?php if ($brwsr_type != "mobile") echo ".mainbox { margin: 10px 30px 20px 55px; width:550px; } "; ?>
        </style>
        <script>


            window.fbAsyncInit = function () {
                FB.init({
                    appId: <?php echo $fb_link ?>, //weeloy localhost fb appId
                    status: true,
                    cookie: true,
                    xfbml: true
                });

            }; //end fbAsyncInit

            // Load the SDK Asynchronously
            (function (d) {
                var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
                if (d.getElementById(id)) {
                    return;
                }
                js = d.createElement('script');
                js.id = id;
                js.async = true;
                js.src = "//connect.facebook.net/en_US/all.js";
                ref.parentNode.insertBefore(js, ref);
            }(document)); //end loadSDF

            function fbsend() {

                var obj = {
//			
                    method: 'send',
                    link: "https://www.weeloy.com/modules/booking/bookingdetails.php/?b=<?php echo $qString_array['b'] ?>&rd=<?php echo $qString_array['rd'] ?>",
                    picture: "<?php echo $image ?>",
                    name: 'New booking at <?php echo $title ?>  Now enjoy rewards with Weeloy',
                    display: 'iframe'
                };
                FB.ui(obj, function (response) {
                    console.log(JSON.stringify(response));
                });
            }
            function fbshare() {

                var obj = {
                    method: 'feed',
                    link: "https://www.weeloy.com/modules/booking/bookingdetails.php/?b=<?php echo $qString_array['b'] ?>&rd=<?php echo $qString_array['rd'] ?>",
                    picture: "<?php echo $image ?>",
                    caption: 'New booking at <?php echo $title ?>  Now enjoy rewards with Weeloy',
                    description: "Experience a new way of enjoying food with Weeloy.com Discover exclusive promotions in your favorite restaurant and book the best place to eat in Singapore",
                    display: 'popup'
                };
                FB.ui(obj, function (response) {
                    console.log(JSON.stringify(response));
                });
            }
        </script>
    </head>
<?php if ($validPage == true): ?>
    <body>
        <div id='booking'   >
            <div class="container mainbox">    
                <div class="panel panel-info"  >
                    <div class="panel-heading">
                        <div class="panel-title" style='width:300px;'>BOOKING DETAILS </div>
                    </div> 

                    <p class="slogan"><h4>&nbsp;&nbsp;Booking is <?php echo $bkstatus ?> at&nbsp  "<?php echo $title ?>"</h4> </p>
                    <p  style="padding-left:10px;margin-right: 5px;" >        
                        <?php echo $description ?> </br></br>
                    </p>
                    <img  src="<?php echo $image ?>" width='500' height='250' id='theLogo' name='theLogo'></center>
                    <div class="panel-body" >
                        <div  class='row' style="margin-left:2px;margin-right:0px;"><div  class="alert alert-info alert-label">BOOKING DETAILS</div></br></br>
                            <div class="details-container"style="position: float;margin-left:5%">
                                <span >Date : <?php echo $bkdate ?>   </span></br></br>
                                <span >Time : <?php echo $bktime ?>   </span></br></br>
                                <span >Guests : <?php echo $bkguests ?>  </span></br></br>
                            </div>
                            <div  class="alert alert-info alert-label">RESERVED BY</div></br>
                            <span style="position: float;margin-left:5%" >Name : <?php echo $name ?>   </span></br></br>
                        </div>
                    </div>
                    <?php if($socialFlag): ?>
                    <?php if($canViewFb ==true) :?>
                        <div  class="alert alert-label"><button class ="btn btn-lg btn-social btn-facebook" style="margin-right:5px;" onClick="fbshare();" ><i class="fa fa-facebook" ></i>Share On Facebook</button> <button class ="btn btn-lg btn-social btn-facebook" onClick="fbsend();" ><i class="fa fa-facebook"></i>Invite Friends</button>  </div></br>
                         <?php endif ?>
                    <?php endif ?>

                </div>

            </div>

        </div>                     

    </body>
<?php else : ?>
    <body class="error">
        <div class="container">
            <div class="col-lg-8 col-lg-offset-2 text-center">
                <h2>InValid Url</h2>
            </div><!-- /.col-lg-8 col-offset-2 -->
        </div>
   <script>window.location = "../../../404";</script>
</body>

<?php endif; ?>
</html>

