


<script> var app = angular.module("myApp", [
    'ui.bootstrap', 
    'ngLocale', 
    'ngStorage', 
    'angulartics',
    'angulartics.google.tagmanager']); 

    app.config(function ($analyticsProvider) {
        // turn off automatic tracking
        $analyticsProvider.virtualPageviews(false);
    });

</script>
<script type="text/javascript" src="js/tradService.js"></script>
<script type="text/javascript" src="modules/booking/book_form.js?23"></script>
<script type="text/javascript" src="modules/booking/book_translate.js?23"></script>

<script>

<?php

 

/**
 * Event Booking variables=======
 */
echo "var showEventBooking = {$showEventBooking};";

//===============================
echo "var tracking = '$bktracking';";
printf("var fullfeature = %s;", ($fullfeature) ? "true" : "false");
echo "var restaurant = '$bkrestaurant';";
echo "var minpax = $minpax;";
echo "var maxpax = $maxpax;";
echo "var maxpaxcc = 99;";
echo "var cstate = '$cstate';";

echo "var curndays = '$ndays';";
echo "var curcountry = '$bkcountry';";
echo "var typeBooking = $typeBooking;";
echo "var timeout='$timeout';";
echo "var brwsr_type = '$brwsr_type';";
echo "var imglogo = '$logo';";
echo "var langue = '$bklangue';";
echo "var Bookchild = '$Bookchild';"; 
echo "var AvailperPax = '$AvailperPax';"; 
echo "var startingPage = '$startingPage';"; 
echo "var resCountry = '$resCountry';";
echo "var preverror = '$bkerror';";
echo "var resBookingDepositPerPaxflg = $resBookingDepositPerPaxflg;";
echo "var resBookingDepositPerPax;";
echo "var resCurrency;";
echo "var pmobile = '$bkmobile';";
echo "var isBooking15mn = '$isBooking15mn';";

echo "var need_restaurant_selection = 0;";
if(!empty($selectresto)  && empty($restaurantselected)){
    echo "var need_restaurant_selection = 1;";
}

if(isset($resBookingDepositPerPaxflg) && $resBookingDepositPerPaxflg == 97) {
	echo "resBookingDepositPerPax = ".json_encode($resBookingDepositPerPax).";";
	echo "resCurrency = '$resCurrency';";
}

echo "var isCreditCardInfoActive = $isCreditCardInfoActive;";
echo "var isDeposit = $isDeposit ;";
echo "var isPaypalActive = $isPaypalActive;";
echo "var tokendata = '$data&signed_request=$signed_request';";
printf("var promocodebkg = %s;", ($promocode) ? "true" : "false");
printf("var optinbkg = %s;", ($optinbkg) ? "true" : "false");

printf("var is_listing = %s;", $is_listing ? 'true' : 'false');
printf("var remainingday = %d;", $bookingwindow - 1);	// 90 or 180 days
printf("var flgsg = %s;", (!preg_match('/CALLCENTER|WEBSITE|facebook|GRABZ/', $bktracking)) ? 'true' : 'false');
printf("var flgwb = %s;", (preg_match('/WEBSITE|facebook|GRABZ/', $bktracking)) ? 'true' : 'false');
printf("var bkextra = '%s';", $bkextra);
printf("var defaultLangue = '%s';", $_SERVER['HTTP_ACCEPT_LANGUAGE']);
echo "var bkversion=3.14;";
echo "var arglist = [";
$sep = "";
for (reset($arglist);
list($index, $label) = each($arglist);
$sep = ", ") {
echo $sep . "'" . $label . "', '" . $$label . "'";
}
echo "];";
?>

var token = "cc0987kj86hmns";

Array.prototype.searchFor = function(needle) {
    for (var i=0; i<this.length; i++)
        if (this[i].indexOf(needle) == 0)
            return i;
    return -1;
};

$(document).ready(function() { 	$('.selectpicker').selectpicker(); });

app.controller('MainController', function ($scope, $http, $locale, $timeout, $location, $localStorage, tradService, HttpServiceAvail, booktranslate) {

	var i, tmp, topic = "BOOKING";
	$scope.restaurant = restaurant;

	booktranslate.setlocal('en');

	$scope.bkextra = bkextra;

	$scope.onlyonce = true;
	$scope.showarea = false;
	$scope.showpurpose = false;
	$scope.showchoice = false
	
	$scope.bkarea = $scope.bkpurpose = $scope.bkchoice = "";
	$scope.showfrontinfo = false;
	$scope.isBooking15mn = parseInt(isBooking15mn);
	
	$scope.frontinfo = <? printf("'%s';", $frontinfo); ?>
	$scope.listarea = <? printf("'%s';", $listarea); ?>
	$scope.listpurpose = <? printf("'%s';", $listpurpose); ?>
	$scope.choice = <? printf("'%s';", $choice); ?>
		
	$scope.bkemail = $scope.bklast = $scope.bkfirst = $scope.bkmobile = $scope.userid = $scope.token = "";
	$scope.nday = curndays;
	$scope.available = 1;
	$scope.imglogo = imglogo;
	$scope.dpDate = new Date();
	$scope.timeout = (timeout == "y");
	$scope.flgsg = flgsg;
	$scope.resCountry = resCountry;
	$scope.lunch = [];
	$scope.dinner = [];
	$scope.teatime = [];
	$scope.dropdown = {};
	$scope.ddlabel = ['date', 'time', 'mtime', 'cover', 'title'];
	
	$scope.bkcover = (minpax < 2) ? 2 : minpax;

	$scope.bktime = "";
	$scope.bkproduct = "";	
	$scope.defaultPax = 1;
        
	//set value by default - based on default time = 19:00
	$scope.mealtype = 'dinner';
	
	$scope.is_test_server = (window.location.hostname.indexOf("test8910.weeloy.com") > -1);

	$scope.brwsr_type = brwsr_type;
	$scope.browserwidth = (brwsr_type !== 'mobile') ? 80 : 70;
	$scope.buttonClass = (typeBooking) ? 'btn-danger' : 'btn-green';


	$scope.cleardropdown = function (selector) {
		$scope.ddlabel.forEach(function(ll) { if (ll !== selector) $scope.dropdown[ll + '_opened'] = false; });
	}

	$scope.opendropdown = function ($event, selector, flg) {
		var l = selector + '_opened';
		$scope.cleardropdown(selector);	// close all the others	
		if(flg === 1) 
			$scope.dropdown[l] = !$scope.dropdown[l];
		$event.preventDefault();
		$event.stopPropagation();
	};

	$scope.cleardropdown('');

	if(typeBooking)
		$scope.buttonLabel = 'book now';
	else if(!is_listing) $scope.buttonLabel = 'request your spin code';
	else $scope.buttonLabel = 'booking on request';
	
	$scope.buttonConfirm = 'Confirm';
	$scope.bookingTitle = "book your table"; // (typeBooking) ? "book your table" : "get your spin code";

	$scope.listTags2 = (typeBooking) ? "Instant Reservation" : "No reservartion";
	$scope.listTags3 = (typeBooking) ? "" : "First come, first serve";
	$scope.listTags4 = (!is_listing) ? "Win for every booking" : "Enjoy incredible";
	$scope.listTags5 = (!is_listing) ? "" : " promotion";

	$scope.slogan = "You are not far from your next FUN dining out experience";
        $scope.creditcardinfo = isCreditCardInfoActive;
        $scope.palpalFlg = isPaypalActive;

	/********************************************************************************************************************************/
	//PHOTOCONTEST PURPOSE

	//if($scope.resCountry === 'Singapore') 
		//$scope.slogan = "Join the Weeloy Food-Photo Contest & Win a 3D2N holiday to Phuket for 2.";
                 //$scope.slogan = "$20 off your First UBER ride when you book a restaurant with Weeloy.";
	/********************************************************************************************************************************/

	$scope.facebookfillin = "Book with Facebook";

	$scope.bookingtitlecf = (typeBooking) ? "BOOKING CONFIRMED" : "REQUESTED CONFIRMED";
	$scope.bacchanalia = "Your table has been blocked, we are sending you an e-mail to reconfirm the booking";
	$scope.confirmsg = (typeBooking) ? "Your booking is confirmed at " : "Your request has been sent to ";
	$scope.confirmsg2 = (typeBooking) ? "We are happy to confirm your reservation" : "Your reservation number is ";
	$scope.confirmsg3 = "You will receive the details of your reservation by email and SMS";
	$scope.confirmsg4 = "You can sign in using your email to view all your bookings and leave your reviews";

	$scope.error0 = 'invalid date. Try again';
	$scope.error1 = 'set the time'; // 'invalid time. Try again';
	$scope.error2 = 'invalid number of persons. Try again';
	$scope.error3 = 'invalid email. Try again';
	$scope.error4 = 'invalid lastname. Try again';
	$scope.error5 = 'invalid firstname. Try again';
	$scope.error6 = 'invalid mobile number. Try again';
	$scope.error7 = 'not available at that time. Try again';
	$scope.error8 = 'the restaurant is close that day. Try another time/day';
	$scope.error9 = 'there is not enough seats for this request. Modify your search';
	$scope.error10 = 'too many guests';
	$scope.error11 = 'seats left';
	$scope.error12 = 'not available for lunch';
	$scope.error13 = 'not available for dinner';
	$scope.error14 = 'not available for afternoon tea';
	$scope.msg1 = 'how many persons ?';

	$scope.tmc1 = "By confirming, I agree to";
	$scope.tmc2 = "the terms and conditions";

	$scope.promocode = (flgwb) ? promocodebkg : false;
	$scope.optin = (flgwb) ? optinbkg : false;

        $scope.need_restaurant_selection = need_restaurant_selection;
	
//	if(flgwb === false) $scope.frontinfo = "";
	
	$scope.langdata = $localStorage.langdata;
	$scope.zang = null;
	if (cstate === "again")
		if ($scope.langdata && $scope.langdata.version && $scope.langdata.version.lb) {
			if ($scope.langdata.version.lb === restaurant)
				$scope.zang = $scope.langdata;
		}

	if (!$scope.zang) {
		$scope.zang = booktranslate.getobj($scope);
		}

	$localStorage.langdata = $scope.zang;
	$scope.langue = langue;

	booktranslate.checkmark($scope, flgsg);
	booktranslate.salutations($scope);
	
	if (AvailperPax === '1' && startingPage == "1") {
		$scope.bkcover = "1";
		$scope.available = 1;
	}

	if (AvailperPax === '1') {
		$scope.defaultPax = 1;  //$scope.bkcover;
		}
		
	for (i = 0; i < arglist.length; i += 2)
		if (arglist[i + 1] != '') {
			if ($('#' + arglist[i]).is(':checkbox'))
				$('#' + arglist[i]).click();
			else $scope[arglist[i]] = arglist[i + 1];
		}

	$scope.dpDate = HttpServiceAvail.getDate($scope.bkdate);
	$scope.curday = 0;
	$scope.lunchdata = "";
	$scope.dinnerdata = "";
	$scope.npers = [];
	for (i = minpax; i <= maxpax; i++)
		$scope.npers.push(String(i));

	$scope.countries = [{'a': 'Australia', 'b': 'au', 'c': '+61'}, {'a': 'China', 'b': 'cn', 'c': '+86'}, {'a': 'Hong Kong', 'b': 'hk', 'c': '+852'}, {'a': 'India', 'b': 'in', 'c': '+91'}, {'a': 'Indonesia', 'b': 'id', 'c': '+62'}, {'a': 'Japan', 'b': 'jp', 'c': '+81'}, {'a': 'Malaysia', 'b': 'my', 'c': '+60'}, {'a': 'Myanmar', 'b': 'mm', 'c': '+95'}, {'a': 'New Zealand', 'b': 'nz', 'c': '+64'}, {'a': 'Philippines', 'b': 'ph', 'c': '+63'}, {'a': 'Singapore', 'b': 'sg', 'c': '+65'}, {'a': 'South Korea', 'b': 'kr', 'c': '+82'}, {'a': 'Thailand', 'b': 'th', 'c': '+66'}, {'a': 'Vietnam', 'b': 'vn', 'c': '+84'}, {'a': '', 'b': '', 'c': ''}, {'a': 'Canada', 'b': 'ca', 'c': '+1'}, {'a': 'France', 'b': 'fr', 'c': '+33'}, {'a': 'Germany', 'b': 'de', 'c': '+49'}, {'a': 'Italy', 'b': 'it', 'c': '+39'}, {'a': 'Russia', 'b': 'ru', 'c': '+7'}, {'a': 'Spain', 'b': 'es', 'c': '+34'}, {'a': 'Sweden', 'b': 'se', 'c': '+46'}, {'a': 'Switzerland', 'b': 'ch', 'c': '+41'}, {'a': 'UnitedKingdom', 'b': 'gb', 'c': '+44'}, {'a': 'UnitedStates', 'b': 'us', 'c': '+1'}, {'a': '', 'b': '', 'c': ''}, {'a': 'Afghanistan', 'b': 'af', 'c': '+93'}, {'a': 'Albania', 'b': 'al', 'c': '+355'}, {'a': 'Algeria', 'b': 'dz', 'c': '+213'}, {'a': 'Andorra', 'b': 'ad', 'c': '+376'}, {'a': 'Angola', 'b': 'ao', 'c': '+244'}, {'a': 'Antarctica', 'b': 'aq', 'c': '+672'}, {'a': 'Argentina', 'b': 'ar', 'c': '+54'}, {'a': 'Armenia', 'b': 'am', 'c': '+374'}, {'a': 'Aruba', 'b': 'aw', 'c': '+297'}, {'a': 'Austria', 'b': 'at', 'c': '+43'}, {'a': 'Azerbaijan', 'b': 'az', 'c': '+994'}, {'a': 'Bahrain', 'b': 'bh', 'c': '+973'}, {'a': 'Bangladesh', 'b': 'bd', 'c': '+880'}, {'a': 'Belarus', 'b': 'by', 'c': '+375'}, {'a': 'Belgium', 'b': 'be', 'c': '+32'}, {'a': 'Belize', 'b': 'bz', 'c': '+501'}, {'a': 'Benin', 'b': 'bj', 'c': '+229'}, {'a': 'Bhutan', 'b': 'bt', 'c': '+975'}, {'a': 'Bolivia', 'b': 'bo', 'c': '+591'}, {'a': 'BosniaandHerzegovina', 'b': 'ba', 'c': '+387'}, {'a': 'Botswana', 'b': 'bw', 'c': '+267'}, {'a': 'Brazil', 'b': 'br', 'c': '+55'}, {'a': 'Brunei', 'b': 'bn', 'c': '+673'}, {'a': 'Bulgaria', 'b': 'bg', 'c': '+359'}, {'a': 'BurkinaFaso', 'b': 'bf', 'c': '+226'}, {'a': 'Burundi', 'b': 'bi', 'c': '+257'}, {'a': 'Cambodia', 'b': 'kh', 'c': '+855'}, {'a': 'Cameroon', 'b': 'cm', 'c': '+237'}, {'a': 'CapeVerde', 'b': 'cv', 'c': '+238'}, {'a': 'CentralAfricanRepublic', 'b': 'cf', 'c': '+236'}, {'a': 'Chad', 'b': 'td', 'c': '+235'}, {'a': 'Chile', 'b': 'cl', 'c': '+56'}, {'a': 'ChristmasIsland', 'b': 'cx', 'c': '+61'}, {'a': 'CocosIslands', 'b': 'cc', 'c': '+61'}, {'a': 'Colombia', 'b': 'co', 'c': '+57'}, {'a': 'Comoros', 'b': 'km', 'c': '+269'}, {'a': 'CookIslands', 'b': 'ck', 'c': '+682'}, {'a': 'CostaRica', 'b': 'cr', 'c': '+506'}, {'a': 'Croatia', 'b': 'hr', 'c': '+385'}, {'a': 'Cuba', 'b': 'cu', 'c': '+53'}, {'a': 'Curacao', 'b': 'cw', 'c': '+599'}, {'a': 'Cyprus', 'b': 'cy', 'c': '+357'}, {'a': 'CzechRepublic', 'b': 'cz', 'c': '+420'}, {'a': 'DemocraticRepCongo', 'b': 'cd', 'c': '+243'}, {'a': 'Denmark', 'b': 'dk', 'c': '+45'}, {'a': 'Djibouti', 'b': 'dj', 'c': '+253'}, {'a': 'EastTimor', 'b': 'tl', 'c': '+670'}, {'a': 'Ecuador', 'b': 'ec', 'c': '+593'}, {'a': 'Egypt', 'b': 'eg', 'c': '+20'}, {'a': 'ElSalvador', 'b': 'sv', 'c': '+503'}, {'a': 'EquatorialGuinea', 'b': 'gq', 'c': '+240'}, {'a': 'Eritrea', 'b': 'er', 'c': '+291'}, {'a': 'Estonia', 'b': 'ee', 'c': '+372'}, {'a': 'Ethiopia', 'b': 'et', 'c': '+251'}, {'a': 'FalklandIslands', 'b': 'fk', 'c': '+500'}, {'a': 'FaroeIslands', 'b': 'fo', 'c': '+298'}, {'a': 'Fiji', 'b': 'fj', 'c': '+679'}, {'a': 'Finland', 'b': 'fi', 'c': '+358'}, {'a': 'FrenchPolynesia', 'b': 'pf', 'c': '+689'}, {'a': 'Gabon', 'b': 'ga', 'c': '+241'}, {'a': 'Gambia', 'b': 'gm', 'c': '+220'}, {'a': 'Georgia', 'b': 'ge', 'c': '+995'}, {'a': 'Ghana', 'b': 'gh', 'c': '+233'}, {'a': 'Gibraltar', 'b': 'gi', 'c': '+350'}, {'a': 'Greece', 'b': 'gr', 'c': '+30'}, {'a': 'Greenland', 'b': 'gl', 'c': '+299'}, {'a': 'Guatemala', 'b': 'gt', 'c': '+502'}, {'a': 'Guernsey', 'b': 'gg', 'c': '+44-1481'}, {'a': 'Guinea', 'b': 'gn', 'c': '+224'}, {'a': 'Guinea-Bissau', 'b': 'gw', 'c': '+245'}, {'a': 'Guyana', 'b': 'gy', 'c': '+592'}, {'a': 'Haiti', 'b': 'ht', 'c': '+509'}, {'a': 'Honduras', 'b': 'hn', 'c': '+504'}, {'a': 'Hungary', 'b': 'hu', 'c': '+36'}, {'a': 'Iceland', 'b': 'is', 'c': '+354'}, {'a': 'Iran', 'b': 'ir', 'c': '+98'}, {'a': 'Iraq', 'b': 'iq', 'c': '+964'}, {'a': 'Ireland', 'b': 'ie', 'c': '+353'}, {'a': 'IsleofMan', 'b': 'im', 'c': '+44-1624'}, {'a': 'Israel', 'b': 'il', 'c': '+972'}, {'a': 'IvoryCoast', 'b': 'ci', 'c': '+225'}, {'a': 'Jersey', 'b': 'je', 'c': '+44-1534'}, {'a': 'Jordan', 'b': 'jo', 'c': '+962'}, {'a': 'Kazakhstan', 'b': 'kz', 'c': '+7'}, {'a': 'Kenya', 'b': 'ke', 'c': '+254'}, {'a': 'Kiribati', 'b': 'ki', 'c': '+686'}, {'a': 'Kosovo', 'b': 'xk', 'c': '+383'}, {'a': 'Kuwait', 'b': 'kw', 'c': '+965'}, {'a': 'Kyrgyzstan', 'b': 'kg', 'c': '+996'}, {'a': 'Laos', 'b': 'la', 'c': '+856'}, {'a': 'Latvia', 'b': 'lv', 'c': '+371'}, {'a': 'Lebanon', 'b': 'lb', 'c': '+961'}, {'a': 'Lesotho', 'b': 'ls', 'c': '+266'}, {'a': 'Liberia', 'b': 'lr', 'c': '+231'}, {'a': 'Libya', 'b': 'ly', 'c': '+218'}, {'a': 'Liechtenstein', 'b': 'li', 'c': '+423'}, {'a': 'Lithuania', 'b': 'lt', 'c': '+370'}, {'a': 'Luxembourg', 'b': 'lu', 'c': '+352'}, {'a': 'Macao', 'b': 'mo', 'c': '+853'}, {'a': 'Macedonia', 'b': 'mk', 'c': '+389'}, {'a': 'Madagascar', 'b': 'mg', 'c': '+261'}, {'a': 'Malawi', 'b': 'mw', 'c': '+265'}, {'a': 'Maldives', 'b': 'mv', 'c': '+960'}, {'a': 'Mali', 'b': 'ml', 'c': '+223'}, {'a': 'Malta', 'b': 'mt', 'c': '+356'}, {'a': 'MarshallIslands', 'b': 'mh', 'c': '+692'}, {'a': 'Mauritania', 'b': 'mr', 'c': '+222'}, {'a': 'Mauritius', 'b': 'mu', 'c': '+230'}, {'a': 'Mayotte', 'b': 'yt', 'c': '+262'}, {'a': 'Mexico', 'b': 'mx', 'c': '+52'}, {'a': 'Micronesia', 'b': 'fm', 'c': '+691'}, {'a': 'Moldova', 'b': 'md', 'c': '+373'}, {'a': 'Monaco', 'b': 'mc', 'c': '+377'}, {'a': 'Mongolia', 'b': 'mn', 'c': '+976'}, {'a': 'Montenegro', 'b': 'me', 'c': '+382'}, {'a': 'Morocco', 'b': 'ma', 'c': '+212'}, {'a': 'Mozambique', 'b': 'mz', 'c': '+258'}, {'a': 'Namibia', 'b': 'na', 'c': '+264'}, {'a': 'Nauru', 'b': 'nr', 'c': '+674'}, {'a': 'Nepal', 'b': 'np', 'c': '+977'}, {'a': 'Netherlands', 'b': 'nl', 'c': '+31'}, {'a': 'NetherlandsAntilles', 'b': 'an', 'c': '+599'}, {'a': 'NewCaledonia', 'b': 'nc', 'c': '+687'}, {'a': 'Nicaragua', 'b': 'ni', 'c': '+505'}, {'a': 'Niger', 'b': 'ne', 'c': '+227'}, {'a': 'Nigeria', 'b': 'ng', 'c': '+234'}, {'a': 'Niue', 'b': 'nu', 'c': '+683'}, {'a': 'NorthKorea', 'b': 'kp', 'c': '+850'}, {'a': 'Norway', 'b': 'no', 'c': '+47'}, {'a': 'Oman', 'b': 'om', 'c': '+968'}, {'a': 'Pakistan', 'b': 'pk', 'c': '+92'}, {'a': 'Palau', 'b': 'pw', 'c': '+680'}, {'a': 'Palestine', 'b': 'ps', 'c': '+970'}, {'a': 'Panama', 'b': 'pa', 'c': '+507'}, {'a': 'PapuaNewGuinea', 'b': 'pg', 'c': '+675'}, {'a': 'Paraguay', 'b': 'py', 'c': '+595'}, {'a': 'Peru', 'b': 'pe', 'c': '+51'}, {'a': 'Pitcairn', 'b': 'pn', 'c': '+64'}, {'a': 'Poland', 'b': 'pl', 'c': '+48'}, {'a': 'Portugal', 'b': 'pt', 'c': '+351'}, {'a': 'Qatar', 'b': 'qa', 'c': '+974'}, {'a': 'RepublicCongo', 'b': 'cg', 'c': '+242'}, {'a': 'Reunion', 'b': 're', 'c': '+262'}, {'a': 'Romania', 'b': 'ro', 'c': '+40'}, {'a': 'Rwanda', 'b': 'rw', 'c': '+250'}, {'a': 'SaintBarthelemy', 'b': 'bl', 'c': '+590'}, {'a': 'SaintHelena', 'b': 'sh', 'c': '+290'}, {'a': 'SaintMartin', 'b': 'mf', 'c': '+590'}, {'a': 'Samoa', 'b': 'ws', 'c': '+685'}, {'a': 'SanMarino', 'b': 'sm', 'c': '+378'}, {'a': 'SaudiArabia', 'b': 'sa', 'c': '+966'}, {'a': 'Senegal', 'b': 'sn', 'c': '+221'}, {'a': 'Serbia', 'b': 'rs', 'c': '+381'}, {'a': 'Seychelles', 'b': 'sc', 'c': '+248'}, {'a': 'SierraLeone', 'b': 'sl', 'c': '+232'}, {'a': 'Slovakia', 'b': 'sk', 'c': '+421'}, {'a': 'Slovenia', 'b': 'si', 'c': '+386'}, {'a': 'SolomonIslands', 'b': 'sb', 'c': '+677'}, {'a': 'Somalia', 'b': 'so', 'c': '+252'}, {'a': 'SouthAfrica', 'b': 'za', 'c': '+27'}, {'a': 'SouthSudan', 'b': 'ss', 'c': '+211'}, {'a': 'SriLanka', 'b': 'lk', 'c': '+94'}, {'a': 'Sudan', 'b': 'sd', 'c': '+249'}, {'a': 'Suriname', 'b': 'sr', 'c': '+597'}, {'a': 'Swaziland', 'b': 'sz', 'c': '+268'}, {'a': 'Syria', 'b': 'sy', 'c': '+963'}, {'a': 'Taiwan', 'b': 'tw', 'c': '+886'}, {'a': 'Tajikistan', 'b': 'tj', 'c': '+992'}, {'a': 'Tanzania', 'b': 'tz', 'c': '+255'}, {'a': 'Togo', 'b': 'tg', 'c': '+228'}, {'a': 'Tokelau', 'b': 'tk', 'c': '+690'}, {'a': 'Tonga', 'b': 'to', 'c': '+676'}, {'a': 'Tunisia', 'b': 'tn', 'c': '+216'}, {'a': 'Turkey', 'b': 'tr', 'c': '+90'}, {'a': 'Turkmenistan', 'b': 'tm', 'c': '+993'}, {'a': 'Tuvalu', 'b': 'tv', 'c': '+688'}, {'a': 'Uganda', 'b': 'ug', 'c': '+256'}, {'a': 'Ukraine', 'b': 'ua', 'c': '+380'}, {'a': 'UnitedArabEmirates', 'b': 'ae', 'c': '+971'}, {'a': 'Uruguay', 'b': 'uy', 'c': '+598'}, {'a': 'Uzbekistan', 'b': 'uz', 'c': '+998'}, {'a': 'Vanuatu', 'b': 'vu', 'c': '+678'}, {'a': 'Vatican', 'b': 'va', 'c': '+379'}, {'a': 'Venezuela', 'b': 've', 'c': '+58'}, {'a': 'WallisandFutuna', 'b': 'wf', 'c': '+681'}, {'a': 'WesternSahara', 'b': 'eh', 'c': '+212'}, {'a': 'Yemen', 'b': 'ye', 'c': '+967'}, {'a': 'Zambia', 'b': 'zm', 'c': '+260'}, {'a': 'Zimbabwe', 'b': 'zw', 'c': '+263'}];
	$scope.currentcc = -1;
	if(curcountry !== '') 
		$scope.currentcc = $scope.countries.inObject('a', curcountry);
	if($scope.currentcc < 0)
		$scope.currentcc = 10;


	/* if($location.host() != "www.weeloy.com") */
	$scope.allowlang = ['en', 'fr', 'cn', 'th', 'my', 'es', 'it', 'kr', 'jp', 'hk', 'de', 'pt', 'vi', 'ru', 'zz'];
	$scope.languages = [{'a': 'English', 'b': 'us', 'c': 'en'}, {'a': '華文', 'b': 'cn', 'c': 'cn'}, {'a': '文言', 'b': 'hk', 'c': 'hk'}, {'a': 'ภาษาไทย', 'b': 'th', 'c': 'th'}, {'a': 'Bahasa Melayu', 'b': 'my', 'c': 'my'}, {'a': '日本語', 'b': 'jp', 'c': 'jp'}, {'a': '한국어', 'b': 'kr', 'c': 'kr'}, {'a': 'Tiếng Việt', 'b': 'vn', 'c': 'vi'}, {'a': 'Русский', 'b': 'ru', 'c': 'ru'}, {'a': 'Français', 'b': 'fr', 'c': 'fr'}, {'a': 'Español', 'b': 'es', 'c': 'es'}, {'a': 'Italiano', 'b': 'it', 'c': 'it'}, {'a': 'Deutsch', 'b': 'de', 'c': 'de'}, {'a': 'Portuguese', 'b': 'pt', 'c': 'pt'}];
	if ($location.host() != "www.weeloy.com" || restaurant == 'SG_SG_R_TheFunKitchen') {
		// $scope.languages.push({ 'a':'Deutsch', 'b':'de', 'c':'de' });
	}

	matchlg = /langue\=(..)/.exec($location.absUrl());
	if(matchlg instanceof Array && matchlg.length > 1 && $scope.allowlang.indexOf(matchlg[1].toLowerCase()) > -1)
		$scope.langue = matchlg[1].toLowerCase();

	if($scope.langue === "zz") 	{
		$scope.langue = normaliseLangue(defaultLangue, $scope.allowlang);
		}
	if($scope.allowlang.indexOf($scope.langue) === -1)
		$scope.langue = "en";
		
	$scope.curlangindex = 0;
	$scope.curlangicon = "famfamfam-flag-" + $scope.languages[$scope.curlangindex].b;
	$scope.curlang = $scope.languages[$scope.curlangindex].a;

	if($scope.restaurant === "SG_SG_R_MitsubaJapaneseRestaurant") {
		if($scope.frontinfo !== "")
			$scope.showfrontinfo = true;
		}
                
                
        //deposit section
    $scope.hasDeposit = false;       
  
	if(resBookingDepositPerPaxflg === 97) {
		$scope.resBookingDepositPerPax = resBookingDepositPerPax[$scope.mealtype];
		$scope.resCurrency = resCurrency;
		$scope.hasDeposit = true;
		//$scope.chkmediniflg = true;
		//$scope.chkbachaflg = $scope.getBaccflg();
			
		}
		
	if(isDeposit && $scope.restaurant === "SG_SG_R_TheOneKitchen" ) {
		$scope.resCurrency = resCurrency;
		$scope.hasDeposit = true;
		//$scope.chkmediniflg = true;
		}
	$scope.resBookingDeposit = $scope.resBookingDepositPerPax * $scope.bkcover;
	
        
	//==================================        
		
	$scope.getselection = function(data) {
		var tmp, label, oo;
		
		tmp = data.split(",");
		label = tmp[0];
		tmp.splice(0, 1);
		oo = { title: label, data: tmp.slice(0) }
		return oo;
		};
		
	if($scope.restaurant !== "SG_SG_R_MadisonRooms") {
		if($scope.bkextra !== "") {
			tmp = $scope.bkextra.split("|");
			i = tmp.searchFor('area=') ;
			if(i >= 0) $scope.bkarea = tmp[i].replace('area=', '');
			i = tmp.searchFor('purpose=') ;
			if(i >= 0) $scope.bkpurpose = tmp[i].replace('purpose=', '');
			i = tmp.searchFor('choice=') ;
			if(i >= 0) $scope.bkchoice = tmp[i].replace('choice=', '');
			}

		if($scope.listarea !== "") {
			$scope.showarea = true;
			$scope.areaobj = $scope.getselection($scope.listarea);
			}
		if($scope.listpurpose !== "") {
			$scope.showpurpose = true;
			$scope.purposeobj = $scope.getselection($scope.listpurpose);
			}
		if($scope.choice !== "") {
			$scope.showchoice = true;
			$scope.choiceobj = $scope.getselection($scope.choice);
			}
		}

	$scope.getDiffDay = function (rdate, evtdate,hh, mn) {
		if(rdate.length < 10 || rdate.indexOf("/") < 0) 
			return -2;
		var ee = rdate.split('/');
		var uu = evtdate.split('/');
		 
		return parseInt((new Date(parseInt(uu[2]), parseInt(uu[1])-1, parseInt(uu[0]), hh, mn, 0).getTime() - new Date(parseInt(ee[2]), parseInt(ee[1])-1, parseInt(ee[0]), hh, mn, 0).getTime()) / (24 * 3600 * 1000));
	};

	$scope.setselect = function(dd, type) {
		$scope.mealflg = 40;
		if(type === 'area') $scope.bkarea = dd;
		if(type === 'purpose') $scope.bkpurpose = dd;
		if(type === 'choice') {
			$scope.bkchoice ='';
			if(dd && dd !== 'Select set menu' && dd !== 'Select buffet menu'){
				var tmp = dd.split("_"),
					label = tmp[0],
	             	ev_date ='14/02/2017',
	                dateDiff = $scope.getDiffDay($scope.bkdate, ev_date,0, 0);
	        if($scope.restaurant == 'TH_BK_R_Medinii' || $scope.restaurant == 'TH_BK_R_BangkokHeightz'){
	        		if(dateDiff == 0 &&  $scope.mealtype == 'dinner' && $scope.bkcover >2 ){
	        			alert("This Menu only available for 2 paxs");
	        		}
	        }
            if(tmp[1].tolowerCase == 'dinner' || tmp[1] == 'LUNCH' ){
            	$scope.mealflg = 41;
            }
        	if($scope.mealflg == 41 && tmp[1].toLowerCase() != $scope.mealtype ){
            	alert("This menu only available on "+ tmp[1].toLowerCase());
    
            }else{
            	$scope.bkchoice = dd;
            	$scope.bkproduct  = dd;
            	$scope.paymentpolicy();
            }
                    
        }
    	}	   
                
		};

		
	$scope.setvalue = function(label, val) {
		$scope[label] = val;
		};
		
	$scope.readAllote = function(ndays, pax) {
		HttpServiceAvail.readAllote($scope.restaurant, pax, $scope.bkproduct).then(function (response) {
			$scope.lunchdata = response.data.lunchdata;
			$scope.dinnerdata = response.data.dinnerdata;
			$scope.updateTimerScope(ndays);
			});
		};

	$scope.phoneindex = function (code) {
		var i, val;
		               
		if ($scope.bkmobile.length > 0)
			$scope.bkmobile = $("#bkmobile").val();
			
		if($scope.bkmobile.length === 0 && typeof pmobile === 'string' && pmobile.length > 0)
			$scope.bkmobile = pmobile;

		val = $scope.bkmobile;

		if(!val || val === '') {
			$scope.bkmobile = code;
			return;
			}


		val = val.trim();
		val = val.replace(/[^0-9 \+]/g, "");
		val = val.replace(/[ ]+/g, " ");

		if (val.indexOf(code + ' ') == 0) {
			val = val.replace(/^\+(\d{2,3})[ ]*\1[ ]*/, '+$1 '); // replace repetition of contry code
			return $scope.bkmobile = val.trim();
		}

		if (val.indexOf(code) == 0) {
			val = code + ' ' + val.substring(code.length);
			val = val.replace(/^\+(\d{2,3})[ ]*\1[ ]*/, '+$1 '); // replace repetition of contry code
			return $scope.bkmobile = val.trim();
		}

		if ((res = val.match(/^\+\d{2,3} /)) != null) {
			val = val.replace(/^\+\d{2,3} /, "");
			val = code + ' ' + val;
			val = val.replace(/^\+(\d{2,3})[ ]*\1[ ]*/, '+$1 '); // replace repetition of contry code
			return $scope.bkmobile = val.trim();
		}

		if ((res = val.match(/^\+\d{2,3}$/)) != null) {
			val = val.replace(/^\+\d{2,3}/, "");
			val = code + ' ' + val;
			val = val.replace(/^\+(\d{2,3})[ ]*\1[ ]*/, '+$1 '); // replace repetition of contry code
			return $scope.bkmobile = val.trim();
		}

		if (val.match(/^\+/) != null) {
			val = code + ' ' + val.substring(1);
			val = val.replace(/^\+(\d{2,3})[ ]*\1[ ]*/, '+$1 '); // replace repetition of contry code
			return $scope.bkmobile = val.trim();
		}

		val = val.replace(/\+ /, "");
		val = code + ' ' + val;
		val = val.replace(/^\+(\d{2,3})[ ]*\1[ ]*/, '+$1 '); // replace repetition of contry code
		return $scope.bkmobile = val.trim();
	};
        
	$scope.phoneindex($scope.countries[$scope.currentcc].c);
	$scope.curflgicon = "famfamfam-flag-" + $scope.countries[$scope.currentcc].b;
        
	$scope.numberOfDay = function(aDate) {
		var dateAr = aDate.split('/');
		var date = new Date(dateAr[2], parseInt(dateAr[1]) - 1, dateAr[0], 23, 59, 59);
		return Math.floor(((date.getTime() - Date.now()) / (24 * 60 * 60 * 1000)));
	}

	$scope.checkPerPaxData = function () {
		var dateflg, timeflg, persflg, tt, dd;
		
		//$scope.readAllote($scope.nday, $scope.bkcover);
		tt = $scope.bktime.replace(/[^0-9:]+/, "");
		if(tt === "" || typeof tt !== 'string')
			return;
			        
		else if(tt.length < 4)
			return $scope.invalid("Invalid time, set a valid time", 'bktime');        
		
		$scope.available = 0;
		
		dateflg = ($scope.bkdate && (($scope.bkdate instanceof Date) || (typeof $scope.bkdate === "string" && $scope.bkdate.length > 7)));
		timeflg = (typeof $scope.bktime === "string" && $scope.bktime.length > 3);
		persflg = (typeof $scope.bkcover === "string" && parseInt($scope.bkcover) > 0);
		if ((dateflg && timeflg && persflg) == false)
			return;

		if(parseInt($scope.bkcover) === 1) {
			$scope.available = 1;
			return;
			}

		HttpServiceAvail.checkAvail($scope.restaurant, $scope.bkdate, $scope.bktime, $scope.bkcover, $scope.bkproduct).then(function (response) {
			if (response.data == "1")
				$scope.available = 1;
			else {
				if (response.count > 0) $scope.invalid($scope.zang['error10'].vl + ". " + response.count + " " + $scope.zang['error11'].vl);
				else $scope.invalid($scope.zang['error9'].vl); 
				}
		});
	}

	$scope.updatendays = function() {
  
		var dd;
		
		$scope.bkdate = $scope.dpDate.getDateFormat('/');
		dd = HttpServiceAvail.normDate($scope.bkdate, false, '/');
		$scope.nday = $scope.numberOfDay(dd);
		$scope.updateTimerScope($scope.nday);

		if (AvailperPax == '1')
			$scope.checkPerPaxData();                    
                $scope.paymentpolicy();
	};

	$scope.updateTimerScope = function (n) {
		var weekday, mealobj, curval, nchar_lunch, nchar_dinner, index, found, patlunch, patdinner, patteatime, noavaillunch, noavaildinner, localtime, isBooking15mn;
		
		noavaillunch = $scope.zang['error12'].vl;
		noavaildinner = $scope.zang['error13'].vl;
		noavailteatime = $scope.error14 // $scope.zang['error14'].vl;
		weekday = $scope.dpDate.getDay() % 7;
		console.log('WEEKDAY', weekday);
		
		$scope.curday = n;
		$scope.lunch = [];
		$scope.dinner = [];
		$scope.teatime = [];
		
		nchar_lunch = 4;
		nchar_dinner = 4;
		index = n * nchar_lunch;
		patlunch = parseInt('0x' + $scope.lunchdata.substring(index, index + nchar_lunch));
		index = n * nchar_dinner;
		patdinner = parseInt('0x' + $scope.dinnerdata.substring(index, index + nchar_dinner));

		curval = $scope.bktime;
		found = 0;
		localtime = "";
		isBooking15mn = ($scope.isBooking15mn == 1);
		mealobj = [ 
			{ type: 'lunch', start: 9, limit: (nchar_lunch * 4) - 2, pattern: patlunch, result:$scope.lunch, noavail: noavaillunch },
			{ type: 'dinner', start: 16, limit: (nchar_dinner * 4), pattern: patdinner, result:$scope.dinner, noavail: noavaildinner }
			];
		
		if($scope.restaurant === "SG_SG_R_Pollen" && weekday !== 2) {	//SG_SG_R_Pollen and not Tuesday
			patteatime = ((patlunch >> 12) & 0x3) | ((patdinner & 0x3) << 2); 
			patdinner >>= 2;
			$scope.teatime = [];
			mealobj = [ 
				{ type: 'lunch', index: 0, start: 9, limit: (nchar_lunch * 4) - 4, pattern: patlunch, result:$scope.lunch, noavail: noavaillunch },
				{ type: 'teatime', start: 15, limit: 4, pattern: patteatime, result:$scope.teatime, noavail: noavailteatime },
				{ type: 'dinner', start: 17, limit: (nchar_dinner * 4) - 2, pattern: patdinner, result:$scope.dinner, noavail: noavaildinner }
				];
			}
			
		mealobj.forEach(function(oo) {
			var i, k, log, ht, limit = oo.limit, start = oo.start, pat = oo.pattern;
			for (log = i = 0, k = 1; i < limit; i++, k *= 2)
				if ((pat & k) == k) {
					log++;
					ht = (start + Math.floor(i / 2)) + ':' + ((i % 2) * 3) + '0';
					if (ht == curval) found = 1;
					oo.result.push(ht);
					if(isBooking15mn) {
						ht = (start + Math.floor(i / 2)) + ':' + (((i % 2) * 3)+1) + '5';
						if(ht == curval) found = 1;
						oo.result.push(ht);
						}
				}
			if (log == 0) {
				oo.result.push(oo.noavail);		// 'not available for lunch'
				localtime = oo.noavail;
				}
			else if(oo.type === "dinner" && isBooking15mn)
				oo.result.pop();
			});

		// temporay
		if($scope.teatime.length > 0 && (!$scope.zang.teatimetag || typeof $scope.zang.teatimetag.vl !== "string"))
			zang.teatimetag.vl = "Afternoon Tea";
				
		curval = $('#bktime').val();
		if (localtime.substr(0, 5) == 'close') {
			$scope.bktime = localtime;
		}
		else if (curval.substr(0, 5) == 'close' || found == 0) {
			$scope.bktime = '';
		}
	};

	$scope.setmealtime = function (tt, section) {
                        
		switch (section) {
			case 1: 
                                // time
				$scope.bktime = tt;
                                
				if($scope.dinner.indexOf($scope.bktime) < 0){
					$scope.mealtype = 'lunch';
				}else{
					$scope.mealtype = 'dinner';
				}
	  
				if(resBookingDepositPerPaxflg === 97){
					$scope.resBookingDepositPerPax = resBookingDepositPerPax[$scope.mealtype];
					//$scope.bacchanaliaPrice($scope.mealtype,$scope.bkcover);
											
				}
				//$scope.chkbachaflg = $scope.getBaccflg();
				if (AvailperPax == '1')
					$scope.checkPerPaxData();
				break;

			case 2: 	// number of pers
				if (tt.substring(0, 4) == "more") {
					if ((pers = prompt("How many persons ?", 10)) != null) {
						pers = parseInt(pers);
						if (pers > 9 && pers < maxpaxcc)
							tt = pers;
						else
							tt = $scope.bkcover;
					}
				}
				$scope.bkcover = String(tt);
				//$scope.chkbachaflg = $scope.getBaccflg();
				if (AvailperPax == '1')
					$scope.checkPerPaxData();
				break;

			case 3: 	// salutation
				$scope.bksalutation = tt;
				break;

			case 4: 	// phone
				for (i = 0; i < $scope.countries.length; i++)
					if ($scope.countries[i].a == tt) {
						$scope.currentcc = i;
						break;
					}

				if (i >= $scope.countries.length) {
					$scope.invalid('Invalid Country Code');
					$scope.currentcc = 10; // Singapore
				}
				$scope.curflgicon = "famfamfam-flag-" + $scope.countries[$scope.currentcc].b;
				$('#bkcountry').val($scope.countries[$scope.currentcc].a);
				$scope.phoneindex($scope.countries[$scope.currentcc].c);
				break;

			case 5:		// langue
				if ($scope.setFlg($scope.languages[tt].c))
					$scope.setLang($scope.languages[$scope.curlangindex].c, 'flag');
				break;

			default:
		}
                if($scope.hasDeposit === true){
                    $scope.paymentpolicy();
                }
	};

	
		
	$scope.setFlg = function (lg) {
		for (var i = 0; i < $scope.languages.length; i++)
			if ($scope.languages[i].c == lg)
				break;
		if (i >= $scope.languages.length)
			return false;

		$scope.curlangindex = i;
		$scope.curlangicon = "famfamfam-flag-" + $scope.languages[$scope.curlangindex].b;
		$scope.curlang = $scope.languages[$scope.curlangindex].a;
		$('#bklangue').val(lg);
		$scope.langue = lg;
		$scope.pcheckmark = ($scope.langue != 'cn') ? 'pcheckmark' : 'pcheckmarkcn';

		return true;
	}

	$scope.checkvalidtel = function () {
		$scope.bkmobile = $scope.cleantel($scope.bkmobile);
		if ($scope.bkmobile === "") {
			$scope.bkmobile == $scope.countries[$scope.currentcc].c.trim() + ' ';
			return;
		}

		$scope.curflgicon = "famfamfam-flag-" + $scope.countries[$scope.currentcc].b;
		$('#bkcountry').val($scope.countries[$scope.currentcc].a);
		$scope.phoneindex($scope.countries[$scope.currentcc].c);
	};

	$scope.setLang = function (lang, action) {
		var curIndex;
		
		if ($scope.allowlang.indexOf(lang) < 0) {
			$scope.invalid('language not supported: ' + lang);
			return;
		}
		// droddown closed
		$scope.dateopened = false;
		// changes $locale
		
		booktranslate.setlocal(lang);

		// changes dt to apply the $locale changes
		$scope.langue = lang;
		$scope.pcheckmark = ($scope.langue != 'cn') ? 'pcheckmark' : 'pcheckmarkcn';
		curIndex = $scope.salutations.indexOf($scope.bksalutation);
		
		tradService.translate(lang, topic, $scope.zang).then(function (response) {
			booktranslate.checkmark($scope, flgsg);
			booktranslate.salutations($scope);

			if(action = 'flag') {
				if(curIndex < 0 || curIndex >= $scope.salutations.length)
					curIndex = 0;
				$scope.bksalutation = $scope.salutations[curIndex];
				}
			$localStorage.langdata = $scope.zang;

			if ($scope.lunch[0] == $scope.zang['error12'].lb)
				$scope.lunch[0] = $scope.zang['error12'].vl;
			if ($scope.dinner[0] == $scope.zang['error13'].lb)
				$scope.dinner[0] = $scope.zang['error13'].vl;
			});
		};

    $scope.getDateformate = function(rdate){
        var bkdate,date;
            if(rdate === '' ){
                bkdate = $scope.bkdate;
            }else{
                bkdate = rdate;
            }
           var uu = bkdate.split('/');
         return  date = uu[2]+"-"+uu[1] +"-"+uu[0];
    }
    

    $scope.paymentpolicy = function () {
    	if($scope.showchoice &&  $scope.bkextra != '' && $scope.bkproduct == ''){

    		$scope.bkproduct = $scope.bkextra;
    		$scope.bkchoice = $scope.bkextra;
    	}
        var payment_method =($scope.creditcardinfo === 1) ?'carddetails' : "paypal" ;
        var date =  $scope.getDateformate($("#bkdate").val());
            HttpServiceAvail.getPayemntPolicy($scope.restaurant,payment_method,$scope.resBookingDeposit,$scope.bkproduct,date,$scope.bktime,$scope.bkcover).then(function (response) {
                var data = response.data;
                $scope.chkcpflg = false;
                if (data && data.priceDetails) {
                        if(data.priceDetails.requiredccdetails){
                            $scope.chkcpflg = true;
                           $scope.paymenttnc = data.priceDetails.message; 
                            if(restaurant === "SG_SG_R_Pollen") {
                            	 //data.priceDetails.message =$scope.frontinfo;
                           	$scope.paymenttnc = data.priceDetails.message; 
                           	$scope.paymenttncAr = $scope.paymenttnc.split('-');; 
                            	 }
                             $scope.getPrice = data.priceDetails.charge;  
                            $scope.resBookingDepositPerPax = $scope.getPrice;
                            $scope.resBookingDeposit = $scope.resBookingDepositPerPax * $scope.bkcover;
	                        if(data.priceDetails.isSplConfig == false){
	                           		$scope.showchoice = false;
	                        }
                            if(data.priceDetails.isSplConfig){
                            	$scope.showchoice = true;
                            	$scope.resBookingDeposit = $scope.getPrice;
                            	if($scope.bkproduct != ''){
	                            	if(parseInt($scope.bkcover)  < parseInt(data.priceDetails.pax )){
	                            		$scope.bkchoice ='';
	                            		alert("This Menu requires minimum " + data.priceDetails.pax + " pax" );
	                            	}
	                            }
                            }                            
                        }else{
                            $scope.chkmediniflg = false;
                        }
                    }
            });

    };

    $scope.rescancelpolicy = function () {
        var payment_method =($scope.creditcardinfo === 1) ?'carddetails' : "paypal" ;
            HttpServiceAvail.getcancelpolicy($scope.restaurant, payment_method, $scope.resBookingDeposit,$scope.bkproduct).then(function (response) {
                var data = response.data.data;

                if (data && data.message) {
                    $scope.tnc = data.message;
                    $scope.freerange =data.lastRange;
                    $scope.cancelpolicy = data.range;
                }
            });

    };

    if($scope.hasDeposit === true){
        $scope.paymentpolicy();
        }
            
    	$scope.formats = ['dd-MM-yyyy', 'dd/MMMM/yyyy', 'yyyy/MM/dd', 'dd/MM/yyyy', 'dd.MM.yyyy', 'shortDate', 'fullDate'];
	$scope.format = $scope.formats[3];
	$scope.minDate = new Date();
	$scope.maxDate = new Date();
	if(cstate !== "again" && restaurant === "SG_SG_R_Nouri" && new Date(2017, 5, 19).getTime() > new Date().getTime()) {	// SG_SG_R_Nouri 19 Juin
		$scope.minDate = new Date(2017, 5, 19);
		$scope.dpDate = new Date(2017, 5, 19);
		$scope.bkdate = $scope.dpDate.getDateFormat('/');
		dd = HttpServiceAvail.normDate($scope.bkdate, false, '/');
		$scope.nday = curndays = $scope.numberOfDay(dd);
		}
	$scope.maxDate.setTime($scope.maxDate.getTime() + (remainingday * 24 * 3600 * 1000));	// remainingday  90 days

	$scope.disabled = function (date, mode) {
		return false;
	};

	$scope.dateOptions = {
		formatYear: 'yy',
		startingDay: 1,
   		showWeeks: false,
   		popupPlacement: 'left',
		ignoreReadonly: true
	};

	$scope.readAllote(curndays, $scope.defaultPax);
	
	if($scope.langue != "en") {
		$scope.setFlg($scope.langue);
		$scope.setLang($scope.langue, 'none');
		}		

	$scope.checksubmit = function () {

		var dd;
		
		$scope.phoneindex($scope.countries[$scope.currentcc].c);

		$('#book_form input[type=text], input[type=password]').each(function () {
			this.value = this.value.trim();
		});

		$scope.bkextra = "";
		if(typeof $scope.bkarea === 'string' && $scope.bkarea !== "")
			$scope.bkextra += (($scope.bkextra !== "") ? "|" : "") + "area="+$scope.bkarea;
		if(typeof $scope.bkpurpose === 'string' && $scope.bkpurpose !== "")
			$scope.bkextra += (($scope.bkextra !== "") ? "|" : "") + "purpose="+$scope.bkpurpose;
		if(typeof $scope.bkchoice === 'string' && $scope.bkchoice !== "")
			$scope.bkextra += (($scope.bkextra !== "") ? "|" : "") + "choice="+$scope.bkchoice;
	
		$('#bkextra').val($scope.bkextra);
		
		mandatoryAr = ['bktime', 'bkcover', 'bkemail', 'bklast', 'bkfirst', 'bkmobile'];

		for (i = 0; i < mandatoryAr.length; i++) {	// manage the autocomplete !!! 
			$scope[mandatoryAr[i]] = $('#' + mandatoryAr[i]).val();
		}

		for (i = 0; i < mandatoryAr.length; i++) {
			if (typeof $scope[mandatoryAr[i]] === 'undefined' || $scope[mandatoryAr[i]] == "")
				return $scope.invalid($scope.zang['error' + (i+1)].vl, mandatoryAr[i]);
		}

		dd = HttpServiceAvail.normDate($scope.bkdate, false, '/');
		var nday = $scope.numberOfDay(dd);
		if (isNaN(nday) || nday > remainingday || nday < 0) {
			return $scope.invalid($scope.zang['error0'].vl, 'bkdate');
		}

		if ($scope.bktime.match(/(^\d{2}\:\d{2}$)|(^9\:\d{2}$)/) == null)
			return $scope.invalid($scope.zang['error1'].vl, 'bktime');
		if (!$scope.IsEmail($scope.bkemail))
			return $scope.invalid($scope.zang['error3'].vl + ': ' + $scope.bkemail, 'bkemail');
		if ($scope.bkmobile.length < 5)
			return $scope.invalid($scope.zang['error6'].vl, 'bkmobile');

		if ($scope.available == 0)
			return $scope.invalid($scope.zang['error10'].vl); // $scope.zang['error7'].vl

		if (tracking != '') {
			removelgcookie("weeloy_be_tracking");
			//setlgcookie("weeloy_be_tracking", tracking, 1);
		}
		if($scope.restaurant == 'SG_SG_R_SilkRoad' || $scope.restaurant == 'SG_SG_R_Element' ){
            if($scope.bkchoice == '' && $scope.chkcpflg){
                return $scope.invalid('Please select your menu option');
            } 
        }
        if($scope.restaurant == 'TH_BK_R_Medinii' || $scope.restaurant == 'TH_BK_R_BangkokHeightz' ){
            var ev_date ='14/02/2017',
	        dateDiff = $scope.getDiffDay($scope.bkdate, ev_date,0, 0);
    		if(dateDiff == 0 &&  $scope.mealtype == 'dinner'){
    			if($scope.bkchoice == '')
					return $scope.invalid('Please select your menu option');
    		    if($scope.bkcover > 2 )
    			   return $scope.invalid('This Menu only available for 2 paxs');
    		}

        }

		$scope.bkdate = dd;
		$('#bkdate').val(dd);
		
		$('#book_form').attr('action', 'modules/booking/prebook.php');

		book_form.submit();

		$scope.onlyonce = false;
		return false;
	};

	$scope.depositCheckout = function() {
		$scope.checksubmit();
	};

	$scope.IsEmail = function (email) {
		var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
		return regex.test(email);
	}
	$scope.cleanemail = function (obj) {
		obj = obj.replace(/[!#$%^&*()=}{\]\[\"\':;><\?/|\\]/g, '');
		return obj;
	}
	$scope.cleantel = function (obj) {
		obj = obj.replace(/[^0-9 \+]/g, '');
		return obj;
	}
	$scope.cleanpass = function (obj) {
		obj = obj.replace(/[\'\"]/g, '');
		return obj;
	}
	$scope.cleantext = function (obj) {
		obj = obj.replace(/[!@#$%^&*()=}{\]\[\"\':;><\?/|\\]/g, '');
		return obj;
	}
	$scope.invalid = function (str, id) {
		if(tracking != 'WEBSITE' && tracking != 'facebook' && tracking != 'GRABZ' && false){
			alert(str);
		}else{
			$('#error-message').text(str);
			$('#error-message').show();
		}	
                
		if (typeof id === "string" && id !== "")
			$('#' + id).focus();
		
		$timeout( function() { $('#error-message').text(""); }, 10000);
	}

	$scope.setVariable = function (varname, varval) {
		for (i = 0; i < varname.length; i++)
			if (typeof $scope[varname[i]] !== "undefined" && typeof varval[i] !== "undefined") {
				$scope[varname[i]] = varval[i];
			}
		$scope.$apply();
	};
        
    
    if(preverror != '') {
    	$scope.invalid(preverror);
    	preverror = "";
    	}  
        
		
	$scope.facebkclik = function () {
		FB.login(function (response) {
			if (response.authResponse) {
				//user just authorized your app
				//document.getElementById('loginBtn').style.display = 'none';
				if (typeof response.authResponse !== 'undefined')
					$('#fbtoken').val(response.authResponse.accessToken);

				if ($('#fbtoken').val().length > 10)
					submitflg = true;
				getUserData();
			}
		}, {scope: 'email,public_profile', return_scopes: true});
	}
})
.directive('datepickerPopup', function () {  // this is the important bit:
	return {
		restrict: 'EAC',
		require: 'ngModel',
		link: function (scope, element, attr, controller) {
			//remove the default formatter from the input directive to prevent conflict
			controller.$formatters.shift();
		}
	}
});

if(self === top && cstate === "new") {	//
	var w = (screen.availWidth < 650) ? screen.availWidth - 30 : 650;
	var h = (screen.availHeight < 850) ? screen.availHeight - 30 : 850;
	self.moveTo(10, 20);
	self.resizeTo(w, h);
}

<?php
echo "var apidfb = '" . $facebookappid . "';";
?>

	submitflg = false;
	function getUserData() {
        FB.api('/me?fields=id,email,first_name,last_name', function (response) {
			if (response.name != "") {
				$('#fbuserid').val(response.id);
				$('#fbemail').val(response.email);
				$('#fbname').val(response.name);
				$('#fbfirstname').val(response.first_name);
				$('#fblastname').val(response.last_name);
				$('#fbtimezone').val(response.timezone);
			}
			if ($('#fbuserid').val() != '' && submitflg) {
				$('#application').val('facebook');
				updateinput();
				getWeeloyUser();
			}
		});
	}

	window.fbAsyncInit = function () {
		//SDK loaded, initialize it
		FB.init({
			appId: apidfb,
			xfbml: true,
			version: 'v2.2'
		});

		//check user session and refresh it
		FB.getLoginStatus(function (response) {
			if (response.status === 'connected') {
				//user is authorized
				//document.getElementById('loginBtn').style.display = 'none';
				if (typeof response.authResponse !== 'undefined')
					$('#fbtoken').val(response.authResponse.accessToken);

				submitflg = false;
				getUserData();
			} else {
				//user is not authorized
			}
		});
	};

	//load the JavaScript SDK
	(function (d, s, id) {
		var js, fjs = d.getElementsByTagName(s)[0];
		if (d.getElementById(id)) {
			return;
		}
		js = d.createElement(s);
		js.id = id;
		js.src = "//connect.facebook.net/en_US/sdk.js";
		fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));


	/*
	 document.getElementById('loginBtndd').addEventListener('click', function() { 	FB.login(function(response) { 		if (response.authResponse) { 			if(typeof response.authResponse !== 'undefined') 				$('#fbtoken').val(response.authResponse.accessToken);  			if($('#fbtoken').val().length > 10) 				submitflg = true; 			getUserData(); 		} 	}, {scope: 'email,public_profile', return_scopes: true}); }, false); 
	 */

</script>

<script>

function updateinput() {
	var email = $('#fbemail').val();
	var first = $('#fbfirstname').val();
	var last = $('#fblastname').val();
	var user = $('#fbuserid').val();
	var token = $('#fbtoken').val();
	angular.element($('#bookingCntler')).scope().setVariable(['bkemail', 'bkfirst', 'bklast', 'userid', 'token'], [email, first, last, user, token]);
}

function getWeeloyUser() {

	var userid, email, token;

	email = $('#fbemail').val();
	userid = $('#fbuserid').val();
	token = $('#fbtoken').val();

	if (tracking == "facebook")
		userid = userid + "_facebook_weeloy";
	apiurl = "../../api/facebookuser/" + email + "/" + userid + "/" + token;
	$.ajax({
		url: apiurl,
		type: "GET",
		success: function (response, textStatus, jqXHR) {
			if (response.status == 0) {
				alert(response.errors);
				return;
			}
			if (typeof response.data.mobile !== "undefined" && response.data.mobile !== "" && response.data.mobile.length > 7) {
				mobile = response.data.mobile.substring(0, 3) + ' ' + response.data.mobile.substring(3);
				angular.element($('#bookingCntler')).scope().setVariable(['bkmobile'], [mobile]);
			}
			if (typeof response.data.country !== "undefined" && response.data.country !== "" && response.data.country.length > 5) {
				country = response.data.country;
				if (country == 'Singapore' || country == 'Thailand' || country == 'Malaysia' || country == 'Hong Kong')
					angular.element($('#bookingCntler')).scope().setVariable(['bkcountry'], [country]);
			}
		},
		error: function (jqXHR, textStatus, errorThrown) { /* alert("Unknown Error. " + textStatus); */
		}
	});
}


          

function termsconditions() {
	$('#remember').toggleClass('show');
}

</script>
