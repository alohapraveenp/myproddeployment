String.prototype.capitalize = function () {
	return this.replace(/^./, function (match) {
		return match.toUpperCase();
	});
};

function setlgcookie(name, value, duration) {
	value = escape(value);
	if (duration) {
		var date = new Date();
		date.setTime(date.getTime() + (duration * 1 * 3600 * 1000));
		value += "; expires=" + date.toGMTString();
	}
	document.cookie = name + "=" + value;
}

function getlgcookie(name) {
	value = document.cookie.match('(?:^|;)\\s*' + name + '=([^;]*)');
	return value ? unescape(value[1]) : "";
}

//Removes the cookies
function removelgcookie(name) {
	setlgcookie(name, '', -1);
}

function normaliseLangue(lg, allowlang) {
	if(lg.length < 2) return "";
	lg = lg.toLowerCase();
	if(allowlang.indexOf(lg.substring(0,2)) > -1 && lg !== "zz")
		return lg.substring(0,2);

	switch(lg.substring(0,5)) {
		case 'zh-cn' : 
		case 'zh-sg' : 
				return 'cn';
		case 'zh-tw' : 
		case 'zh-hk' : 
				return 'hk';
		}

	switch(lg.substring(0,2)) {
		case 'ko' : return 'kr';
		case 'ja' : return 'jp';
		case 'ms' : return 'my';
		case 'zh' : return 'cn';
		}

	return "";			
}

function reloadcc(value) { 

	var arg = "", id = [ "bksalutation", "bkfirst", "bklast", "bkemail", "bkmobile" ];
	id.map(function(item) { arg += "&" + item + "=" + $('#'+ item).val(); console.log(arg) } );

    query  = window.location.search.replace(/bkrestaurant=[^&]+&/, "bkrestaurant=" + value + "&");

	if(query.search(value) >= 0) {		
		return window.location.href = 'modules/callcenter/callcenter.php' + query + arg;
		}

	if(query.search("bkrestaurant") < 0) {
		return window.location.href = 'modules/callcenter/callcenter.php' + window.location.search + '&bkrestaurant=' + value + "&dummy=0" + arg; 
		}

	//if(query.search("bkrestaurant") < 0) return window.location.href = 'modules/callcenter/callcenter.php' + "?bktracking=CALLCENTER&data=" + token + '&bkrestaurant=' + value + '&bktms=' + bktms; 
}

function reloadbk(value, tracking) { 
	var root = (window.location.hostname.search("localhost") < 0) ? "/" : "";
	var arg = "", id = [ "bksalutation", "bkfirst", "bklast", "bkemail", "bkmobile" ];
	id.map(function(item) { arg += "&" + item + "=" + $('#'+ item).val(); console.log(arg) } );
	
	window.location.href = root + 'modules/booking/book_form.php?data=' + tokendata + '&bkrestaurant=' + value + '&bktracking=' + tracking  + '&restaurantselected=1' + arg;
}

Date.prototype.getDateFormat = function(sep) {
	var d = this.getDate(), m = this.getMonth()+1, y = this.getFullYear();
	if(typeof sep !== 'string' || (sep !== '/' && sep !== '-')) sep = '-';
	return ((d <= 9) ? '0' : '') + d + sep + ((m <= 9) ? '0' : '') + m + sep + y;
}

Date.prototype.getDateFormatReverse = function(sep) {
	var d = this.getDate(), m = this.getMonth()+1, y = this.getFullYear();
	if(typeof sep !== 'string' || (sep !== '/' && sep !== '-')) sep = '-';
	return y + sep + ((m <= 9) ? '0' : '') + m + sep + ((d <= 9) ? '0' : '') + d;
}
	
Array.prototype.inObject = function(name, value) {
	for(var i = 0; i < this.length; i++) {
		oo = this[i];
		if(oo[name] && oo[name] === value)
			return i; 
		}
	return -1;
	};


app.service('HttpServiceAvail',['$http', '$location','$q', function ($http, $location,$q) {

	this.loginfo = function(category, content) {
		return $http.post("api/log/info/",
			{
			'category': category,
			'content' 	: content
			}).then(function(response) { return response.data;});
		};

	this.checkDayAvail = function(restaurant, product, time, pax) {
		if(typeof product !== "string") product = "";
		return $http.post("api/restaurant/dayavailable/",
			{
			'restaurant': restaurant,
			'product': product,
			'time' 	: time,
			'pers' 	: pax
			}).then(function(response) { return response.data;});
		};

	this.getcancelpolicy = function (restaurant, payment_method, resBookingDeposit, pproduct) {
		return $http.post("api/services.php/cancelpolicy/list",
				{
					'restaurant':restaurant,
					'type': payment_method,
					'amount': resBookingDeposit,
					'product':pproduct
				}).then(function (response) { return response.data; });
		};
                
                
        this.getPayemntPolicy = function (restaurant, payment_method, resBookingDeposit,pproduct,bkdate,bktime,bkcover) {
                              
                var API_URL = "api/v2/restaurant/cancelpolicy/read";
                    var defferred = $q.defer();
                      $http({
                            url: API_URL,
                            method: "POST",
                            data: $.param({
                                    'restaurant':restaurant,
                                    'type': payment_method,
                                    'amount': resBookingDeposit,
                                    'product':pproduct,
                                    'date' : bkdate,
                                    'time' : bktime,
                                    'cover' :bkcover
                        }),
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded'
                        }
                     }).success(function(response) {
                        defferred.resolve(response);
                    });
                     return defferred.promise;
            };

	this.readmadisonmember = function(restaurant) {
		return $http.post("api/madison/member/read/",
			{
				'restaurant': restaurant,
				'token'	: 'callcenter'
            }).then(function (response) { return response.data; });
		};


	this.readPabx = function(to) {
		return $http.post("api/services.php/pabx/read/",
			{
				'to' : to,
				'token'	: 'callcenter'
            }).then(function (response) { return response.data; });
		};

	this.read1Profile = function(restaurant, email, phone) {
		return $http.post("api/services.php/profile/read/one/",
			{
				'phone' : phone,
				'email' : email,
				'restaurant': restaurant,
				'token'	: 'callcenter'
            }).then(function (response) { return response.data; });
		};

	this.updatesubProfile = function(restaurant, email, systemid, subprof) {
		return $http.post("api/services.php/profilesub/update/",
			{
				'email' : email,
				'restaurant': restaurant,
				'systemid': systemid,
				'subprof': subprof,
				'token'	: token
            }).then(function (response) { return response.data; });
		};

	this.getallote = function(restaurant) {
		var url = 'api/restaurant/allote/' + restaurant;
		return $http.get(url).then(function(response) {
			return response.data;
			});
		};

	this.readAllote = function(restaurant, pax, product, platform) {
		if(typeof product !== "string") product = "";
		if(typeof platform !== "string") platform = "";
		return $http.post("api/restaurant/allote/",
				{
				'restaurant': restaurant,
				'product': product,
				'platform': platform,
				'pax': pax
				}).then(function (response) { return response.data; })
		};

	this.readcodebooking = function(restaurant, email) {
		var url = "api/services.php/profile/readcodebkg/";
		return $http.post(url,
			{
				'email' : email,
				'restaurant': restaurant,
				'token'	: token
            }).then(function (response) { return response.data; });
		};

	this.bookingemail = function(restaurant, email) {
		return $http.post("api/booking/email/",
			{
				'restaurant': restaurant,
				'email' 	: email,
				'token'		: token
			}).then(function(response) { return response.data;})
		};

	this.bookingmobile = function(restaurant, mobile) {
		return $http.post("api/booking/mobile/",
			{
				'restaurant': restaurant,
				'mobile' 	: mobile,
				'token'		: token
			}).then(function(response) { return response.data;})
		};
		
	this.checkAvail = function(restaurant, date, time, pax, product, platform) {
		if(typeof product !== "string") product = "";
		if(typeof platform !== "string") platform = "";
		date = this.normDate(date, true, '-');
		return $http.post("api/restaurant/availslot/",
				{
					'restaurant': restaurant,
					'date': date,
					'time': time,
					'pers': pax,
					'product': product,
					'platform': platform,
					'token': token
				}).then(function (response) { return response.data; })
		};

	this.normDate = function (dd, reverse, sep) {
		if(typeof dd !== "string" && dd instanceof Date === false) 
			dd = new Date();

		if(sep !== '-' && sep !== '/') sep = '-';
		if (typeof dd !== "string")
			return (reverse) ? dd.getDateFormatReverse(sep) : dd.getDateFormat(sep);
			
		dd = (sep == '-') ? dd.replace(/\//g, "-") : dd.replace(/-/g, "/");
		aa = dd.split(sep);
		return (reverse) ? aa[2] + sep + aa[1] + sep + aa[0] :  aa[0] + sep + aa[1] + sep + aa[2];
		};
   
	this.getDate = function (dd) {
		if(typeof dd !== "string" || dd.length < 10) 
			return new Date();

		sepAr = dd.match(/\/|-/);
		if(sepAr instanceof Array === false || sepAr.length < 1)
			return new Date();
		
		sep = sepAr[0];	
		aa = dd.split(sep);
		if(aa instanceof Array === false || aa.length < 3)
			return new Date();

		aa.map(function(v, index, arr) { arr[index] = parseInt(arr[index]); if(index === 1) arr[index]--; });		
		return (aa[0] < 100 ) ? new Date(aa[2], aa[1], aa[0], 0, 0, 0, 0) :  new Date(aa[0], aa[1], aa[2], 0, 0, 0, 0);
		};
   
    this.bacchanaliacancelpolicy = function(restaurant,date,time,pax){
           
           var API_URL = "api/v2/restaurant/cancelpolicy/getPolicy";
            var defferred = $q.defer();
              $http({
                    url: API_URL,
                    method: "POST",
                    data: $.param({
                            'restaurant': restaurant,
                            'date' 	: date,
		            'time'	: time,
                            'pax' : pax
                }),
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
             }).success(function(response) {
                defferred.resolve(response);
            });
             return defferred.promise;

       }
        
      
	
}]);
