<div class="plot-container">
<div id='flot-piechart' class="plot-placeholder"></div>
</div>

<script>

var Piechartobj = function(data) {

	var that = {};
	
	that.cyear = 2015;
	that.placeholderName = "#flot-piechart";

	that.dataSet = data;
	
	that.options = {
		series: {
			pie: {
				show: true,
				label: {
					show: true,
					radius: 180,
					formatter: function (label, series) {
						return "<div style='border:1px solid grey;font-size:8pt;text-align:center;padding:5px;color:white;'>" + label + " : " + Math.round(series.percent) + "</div>";
						},
					background: {
						opacity: 0.8,
						color: '#000'
						}
					}
				}
			},
		legend: {
			show: false
			},
		grid: {
			hoverable: true
			}
		};

	that.options1 = {
		series: {
			pie: {
				show: true,
				tilt: 0.5
				}
			}
		};

	that.options2 = {
		series: {
			pie: {
				show: true,
				// innerRadius: 0.5,
				label: {
					show: true
					}
				}
			}
		};
	return that;
	};

var closePie = 1;
function startPie() {
	var i, j, l, p, names, tmp, elAr, data, oo, totalOb = {}, color, myPiedata;

	if(closePie < 0) return;
	console.log('not ready closePie ' + closePie);
	
	if(closePie++ > 20) {
		closePie = -1;
		}

	if(wl_stats_data_booking === null) {
		setInterval(startPie, 2000);
		return;
		}

	closePie = -1;
	if(!(wl_stats_data_booking instanceof Array) || wl_stats_data_booking.length < 1) {
		console.log('no data closePie ' + closePie);
		return;
		}
		
	data = wl_stats_data_booking.slice(0);
	tmp = Object.keys(data[0]);
	names = [];
	for (i = 0; i < tmp.length; i++)
		if(['rweek', 'cweek', 'index', 'valid', 'tms', 'callcenter', 'booking', 'lunch', 'dinner', 'win'].indexOf(tmp[i]) == -1)
			names.push(tmp[i]);
			
	for (i = 0; i < data.length; i++) {
		oo = data[i];
		for(j = 0; j < names.length; j++) {
			l = names[j];
			x = oo[l];
			if(!(l in totalOb)) totalOb[l] = 0;
			totalOb[l] += oo[l];
			}
		}

	color = ["#005CDE", "#00A36A", "#7D0096", "#992B00", "#DE000F", "#ED7B00", "#0062FF", "#009933", "#FF9900", "#660099"];
	color = ["BlueViolet", "red", "blue", "Coral", "green", "CadetBlue", "BurlyWood", "Aquamarine", "yellow", "#FF9900", "#660099"];
	names = Object.keys(totalOb);
	myPiedata = [];
	total = totalOb['transaction']
	for(i = j = 0; i < names.length; i++) {
		if(names[i] === 'transaction')
			continue;
		p = totalOb[names[i]];
		if(((p * 100) / total) >= 1)
			myPiedata.push( { label:names[i], data: totalOb[names[i]], color: color[j++]  } ) 
		}
	var oo = new Piechartobj(myPiedata);
	$.plot($(oo.placeholderName), oo.dataSet, oo.options2);
	}
	
startPie();

</script>

