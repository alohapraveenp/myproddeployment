<script>

var Linechartobj = function() {

	var that = {};
	
	that.cyear = 2015;
	that.placeholderName = "#flot-linechart";
	that.gd = function (year, month, day) {
		return new Date(year, month, day).getTime();
		};

<?php

	//$theRestaurant = "SG_SG_R_TheFunKitchen"; it is passed dynamically
	$sdate = "2015-01-01";
	$edate = "2015-12-31";
	$year = 2015;
	
	$stats = new WY_stats($theRestaurant, $sdate, $edate);
	$data = $stats->bookinglinechart();

	$limit = count($data);
	//for($i = 0; $i < $limit; $i++, $sep = ", ") { }

	$callcntr = array(900, 755, 669, 888, 725, 999, 754, 968, 897);
	$website = array(300, 359, 452, 455, 652, 485, 525, 666, 542);
	$weeloy = array(250, 259, 342, 366, 455, 365, 455, 521, 512);
	$walking = array(125, 232, 235, 245, 325, 412, 252, 325, 121);
	$cancel = array(111, 222, 212, 323, 210, 312, 175, 285, 159);
	$noshow = array(222, 415, 213, 214, 236, 352, 263, 253, 312);

	for($i = 0; $i < 8; $i++)
		$booking[$i] = ($callcntr[$i] + $website[$i] + $weeloy[$i] + $walking[$i]) - ($cancel[$i] + $noshow[$i]);
	
	$data1 = $data2 = $data3 = $data4 = $data5 = $data6 = $data7 = "";
	for($i = 0, $sep = ""; $i < 8; $i++, $sep = ", ") {
		$data1 .= $sep . "[that.gd(that.cyear, $i, 1)," . $booking[$i] . "]";
		$data2 .= $sep . "[that.gd(that.cyear, $i, 1)," . $callcntr[$i] . "]";
		$data3 .= $sep . "[that.gd(that.cyear, $i, 1)," . $website[$i] . "]";
		$data4 .= $sep . "[that.gd(that.cyear, $i, 1)," . $weeloy[$i] . "]";
		$data5 .= $sep . "[that.gd(that.cyear, $i, 1)," . $walking[$i] . "]";
		$data6 .= $sep . "[that.gd(that.cyear, $i, 1)," . $cancel[$i] . "]";
		$data7 .= $sep . "[that.gd(that.cyear, $i, 1)," . $noshow[$i] . "]";
		}
	echo "that.data1 = [" . $data1 . "];\n";
	echo "that.data2 = [" . $data2 . "];\n";
	echo "that.data3 = [" . $data3 . "];\n";
	echo "that.data4 = [" . $data4 . "];\n";
	echo "that.data5 = [" . $data5 . "];\n";
	echo "that.data6 = [" . $data6 . "];\n";
	echo "that.data7 = [" . $data7 . "];\n";

?>
			
	that.previousPoint = null;
	that.previousLabel = null;
	that.monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
	
	that.dataset = [
		{ color: "black", label: " &nbsp;booking &nbsp;", data: that.data1, yaxis: 2, points: { symbol: "triangle"} },
		{ color: "blue", label: " &nbsp;ccenter &nbsp;", data: that.data2, yaxis: 2, points: { symbol: "square"} },
		{ color: "yellow", label: " &nbsp;website &nbsp;", data: that.data3, yaxis: 2, points: { symbol: "circle"} },
		{ color: "purple", label: " &nbsp;weeloy &nbsp;", data: that.data4, yaxis: 2, points: { symbol: "square"} },
		{ color: "green", label: " &nbsp;walking &nbsp;", data: that.data5, yaxis: 2, points: { symbol: "circle"} },
		{ color: "cyan", label: " &nbsp;cancel &nbsp;", data: that.data6, yaxis: 2, points: { symbol: "square"} },
		{ color: "red", label: " &nbsp;noshow &nbsp;", data: that.data7, yaxis: 2, points: { symbol: "triangle"} }
		];

	that.options = {
		series: {
			lines: {
				show: true
				},
			points: {
				radius: 3,
				fill: true,
				show: true
				}
			},
		xaxis: {
			mode: "time",
			tickSize: [1, "month"],
			tickLength: 0,
			axisLabel: that.cyear,
			axisLabelUseCanvas: true,
			axisLabelFontSizePixels: 12,
			axisLabelFontFamily: 'Helvetica',
			axisLabelPadding: 30
			},
		yaxes: [{
			axisLabel: "Booking type",
			axisLabelUseCanvas: true,
			axisLabelFontSizePixels: 12,
			axisLabelFontFamily: 'Helvetica',
			axisLabelPadding: 3,
			tickFormatter: function (v, axis) {
				return $.formatNumber(v, { format: "#,###" });
				}
			}, {
			position: "right",
			axisLabel: "Bookings",
			axisLabelUseCanvas: true,
			axisLabelFontSizePixels: 13,
			axisLabelFontFamily: 'Helvetica',
			axisLabelPadding: 30
			}
	  		],
		legend: {
			noColumns: 0,
			labelBoxBorderColor: "#000000",
			position: "ne"
			},
		grid: {
			hoverable: true,
			borderWidth: 2,
			borderColor: "#633200",
			backgroundColor: { colors: ["#ffffff", "#EDF5FF"] }
			},
		colors: ["#FF0000", "#0022FF"]
		};
	return that;
	};
	
	var linechartBooking = new Linechartobj();
	
	$(document).ready(function () {
		var oo = linechartBooking;
		
		oo.placeholder = $(oo.placeholderName);

		$.plot(oo.placeholder, oo.dataset, oo.options);
		oo.placeholder.UseTooltip();
		oo.placeholder.resize();

		$(".demo-container").resizable({
			maxWidth: 900,
			maxHeight: 800,
			minWidth: 450,
			minHeight: 250
		});

	});


	$.fn.UseTooltip = function () {
		$(this).bind("plothover", function (event, pos, item) {
			var oo = linechartBooking;
			if (item) {
				if ((oo.previousLabel != item.series.label) || (oo.previousPoint != item.dataIndex)) {
					oo.previousPoint = item.dataIndex;
					oo.previousLabel = item.series.label;
					$("#tooltip").remove();

					var x = item.datapoint[0];
					var y = item.datapoint[1];

					var color = item.series.color;
					var month = new Date(x).getMonth();

					//console.log(item);

					if (item.seriesIndex == 0) {
						showTooltip(item.pageX,
						item.pageY,
						color,
						"<strong>" + item.series.label + "</strong><br>" + oo.monthNames[month] + " : <strong>" + y + "</strong>");
					} else {
						showTooltip(item.pageX,
						item.pageY,
						color,
						"<strong>" + item.series.label + "</strong><br>" + oo.monthNames[month] + " : <strong>" + y + "</strong>");
					}
				}
			} else {
				$("#tooltip").remove();
				oo.previousPoint = null;
			}
		});
	};

	function showTooltip(x, y, color, contents) {
		$('<div id="tooltip">' + contents + '</div>').css({
			position: 'absolute',
			display: 'none',
			top: y - 40,
			left: x - 40,
			border: '2px solid ' + color,
			padding: '3px',
			'font-size': '9px',
			'border-radius': '5px',
			'background-color': '#fff',
			'font-family': 'Roboto, Montserrat, Helvetica, Tahoma, sans-serif',
			opacity: 0.9
		}).appendTo("body").fadeIn(200);
	}
	</script>
	<br>
	<div class="plot-container">
    <div id="flot-linechart" class="plot-placeholder"></div> 
	</div>
