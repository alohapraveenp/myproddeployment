<?php

function indicator($value, $title, $subtitle, $ranges) {

	$inThousand = "";
	if($value > 10000) {
		$value = floor($value / 1000);
		$inThousand = ", in thousands";
		}
	$valueMax = $value + 30;
	$order = pow(10, strlen(strval($value)) - 1); 
	$start = (floor($value / $order) * $order) - ($order / 2); 
	$end = (floor($value / $order)+1) * $order; 
	if(($end - $value) < ($order / 2)) $end += ($order / 2); $middle = floor(($start + $end)/2);
	if($valueMax > $end) $valueMax = $end;
	$marker = $valueMax + 20;
	if($marker > $end) 
		$marker = $end;

	if(count($ranges) == 6) {
		$start = $ranges[0];
		$middle = $ranges[1];
		$end = $ranges[2];
		$value = $ranges[3];
		$marker = $ranges[4];
		$valueMax = $ranges[5];
		}
		
	return '{"title":"' . $title . '","subtitle":"' . $subtitle . $inThousand . '","ranges":[' . $start . ',' . $middle . ',' . $end . '],"measures":[' . $value . ', ' . $valueMax . '],"markers":[' . $marker . ']}';
}

	$year = 2016;

	$sdate = $year . "-01-01";
	$edate = $year . "-12-31";

	$resto = new WY_restaurant();
	$resto->getRestaurant($theRestaurant);

	$stats = new WY_stats($theRestaurant, $sdate, $edate);
	$stats->bullet();
	
	$height = 330;
	$ranges = array();
	if($resto->restogeneric !== '' && preg_match("/’bullet’:’([^’]+)’/", $resto->restogeneric, $match)) {
		$ranges = explode("|", $match[1]);
		}

	$dataObj = "";
	$dranges = (count($ranges) == 7) ? array($ranges[0] * $ranges[6], $ranges[1] * $ranges[6], $ranges[2] * $ranges[6], $stats->booking, $ranges[5], $ranges[6]) : array();
	$dataObj .= indicator($stats->booking, "Bookings", "count", $dranges) . ","; 

	$av = $stats->avgorder;
	$dranges = (count($ranges) == 7) ? array($ranges[0] * $ranges[6] * $av, $ranges[1] * $ranges[6] * $av, $ranges[2] * $ranges[6] * $av, $stats->booking * $av, $ranges[5] * $av, $ranges[6] * $av) : array();
	$dataObj .= indicator($stats->revenue, "Sales", "SGD$", $dranges) . ","; 

	$av = $stats->avgorder * 0.6; // 60% profit
	$dranges = (count($ranges) == 7) ? array($ranges[0] * $ranges[6] * $av, $ranges[1] * $ranges[6] * $av, $ranges[2] * $ranges[6] * $av, $stats->booking * $av, $ranges[5] * $av, $ranges[6] * $av) : array();
	$dataObj .= indicator($stats->profit, "Profit", "SGD$", $dranges) . ","; 
	//$dataObj .= indicator($stats->avgorder, "Sales/Booking", "SGD$") . ","; 

	$av = 3;
	$dranges = (count($ranges) == 7) ? array($ranges[0] * $ranges[6] * $av, $ranges[1] * $ranges[6] * $av, $ranges[2] * $ranges[6] * $av, $stats->cover, $ranges[5] * $av, $ranges[6] * $av) : array();
	$dataObj .= indicator($stats->cover, "Pax", "count", $dranges); 

	$av = 3 * 0.1; // 10% of repeat customers
	$dranges = (count($ranges) == 7) ? array($ranges[0] * $ranges[6] * $av, $ranges[1] * $ranges[6] * $av, $ranges[2] * $ranges[6] * $av, $stats->cover, $ranges[5] * $av, $ranges[6] * $av) : array();
	if($stats->ucover > 0 && ($stats->cover / $stats->ucover) < 10) {
		$dataObj .= "," . indicator($stats->ucover, "Repeat Clients", "count", $dranges);
		$height = 400;
		}
	
	//$dataObj = '{"title":"Revenue","subtitle":"US$, in thousands","ranges":[150,225,300],"measures":[220,270],"markers":[250]}, ';
	//$dataObj .= '{"title":"Profit","subtitle":"SGD$","ranges":[20,25,30],"measures":[21,23],"markers":[26]}, ';
	//$dataObj .= '{"title":"Order Size","subtitle":"US$, average","ranges":[350,500,600],"measures":[100,320],"markers":[550]}, ';
	//$dataObj .= '{"title":"New Customers","subtitle":"count","ranges":[1400,2000,2500],"measures":[1000,1650],"markers":[2100]}, ';
	//$dataObj .= '{"title":"Satisfaction","subtitle":"out of 5","ranges":[3.5,4.25,5],"measures":[3.2,4.7],"markers":[4.4]}';
	
	$dataObj = "[ " . $dataObj . "]";

printf("<div class='plot-container' style='height:%dpx'>", $height);
?>


<!--
<div class="plot-container" style="height:400px">
-->
<br>
<div id='bulletchart'></div>
</div>
<script>

//var dataObj = '[ {"title":"Revenue","subtitle":"US$, in thousands","ranges":[150,225,300],"measures":[220,270],"markers":[250]}, {"title":"Profit","subtitle":"%","ranges":[20,25,30],"measures":[21,23],"markers":[26]}, {"title":"Order Size","subtitle":"US$, average","ranges":[350,500,600],"measures":[100,320],"markers":[550]}, {"title":"New Customers","subtitle":"count","ranges":[1400,2000,2500],"measures":[1000,1650],"markers":[2100]}, {"title":"Satisfaction","subtitle":"out of 5","ranges":[3.5,4.25,5],"measures":[3.2,4.7],"markers":[4.4]} ]';

<?php echo "var dataObj = '" . $dataObj . "';" ?>

var data = JSON.parse(dataObj);

var margin = {top: 5, right: 40, bottom: 20, left: 120},
    width = 800 - margin.left - margin.right,
    height = 50 - margin.top - margin.bottom;

var chart = d3.bullet()
    .width(width)
    .height(height);

//d3.json("bullets.json", function(error, data) {
  var svg = d3.select("#bulletchart").selectAll("svg")
      .data(data)
    .enter().append("svg")
      .attr("class", "bullet")
      .attr("width", width + margin.left + margin.right)
      .attr("height", height + margin.top + margin.bottom)
    .append("g")
      .attr("transform", "translate(" + margin.left + "," + margin.top + ")")
      .call(chart);

  var title = svg.append("g")
      .style("text-anchor", "end")
      .attr("transform", "translate(-6," + height / 2 + ")");

  title.append("text")
      .attr("class", "title")
      .text(function(d) { return d.title; });

  title.append("text")
      .attr("class", "subtitle")
      .attr("dy", "1em")
      .text(function(d) { return d.subtitle; });

/*
  d3.selectAll("button").on("click", function() {
    svg.datum(randomize).call(chart.duration(1000)); // TODO automatic transition
  });


function randomize(d) {
  if (!d.randomizer) d.randomizer = randomizer(d);
  d.ranges = d.ranges.map(d.randomizer);
  d.markers = d.markers.map(d.randomizer);
  d.measures = d.measures.map(d.randomizer);
  return d;
}

function randomizer(d) {
  var k = d3.max(d.ranges) * .2;
  return function(d) {
    return Math.max(0, d + k * (Math.random() - .5));
  };
}

*/

</script>
