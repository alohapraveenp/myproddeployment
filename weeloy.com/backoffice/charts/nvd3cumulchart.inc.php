<div ng-controller="CumulChartCtrl" class="plot-container" id = 'CumulChartDiv' style='height:600px'>  
<h3 ng-if="errormsg"> {{errormsg}} </h3>
<nvd3 options="options" data="data" api="api"></nvd3>
<br><button  type="button" class="btn btn-primary" ng-click="togglemode();">change to {{ccmode}} </button>
</div>

<script>

app.controller('CumulChartCtrl', function($scope, $timeout) {
	
	$scope.year = new Date().getFullYear();
	$scope.restaurant = chrtresto;
	$scope.restotitle;
	$scope.email = chrtemail;
	$scope.type = "c";	// by create date
	$scope.field = ($scope.type === "c") ? "cweek" : "rweek";	// by create date
	$scope.nodata = false;
	$scope.errormsg = "";
	$scope.prebooking = [];
	$scope.booking = [];
	$scope.data = [];
	$scope.cmode = 'bookings';
	$scope.ccmode = 'pax';
	$scope.Title = 'Cumulated Reservation Performance';
	$scope.subTitle = 'this chart shows the different type of bookings and pax received by a period of one week';
	$scope.caption = '<b>Figure 1.</b> <span style="color: darkred;">The x axis </span>represent the week of the year, <span style="text-decoration: underline;">28 being the 28th week of that year.</span> Booking numbers are cummulated. <i>It is all transactions, minus cancellations and noshows.</i>';	
	
	$scope.analyticsObj = function(index, cweek, rweek) {
		return {
			index: index,
			cweek: cweek,
			rweek: rweek,
			lunch: 0,
			dinner: 0,
			transaction: 0,
			booking: 0,
			facebook: 0,
			website: 0,
			weeloy: 0,
			tms: 0,
			callcenter: 0,
			cctms: 0,
			walkin: 0,
			waiting: 0,
			thirdparty: 0,
			wheelwin: 0,
			cancel: 0,
			valid: 0,
			win: 0,
			noshow: 0
			};	 			
		};

        $scope.options = {
            chart: {
                type: 'lineChart',
				height: $("#CumulChartDiv").height() - 190,
				width: $("#CumulChartDiv").width(),
				margin : {
					top: 20,
					right: 20,
					bottom: 40,
					left: 55
                	},
                	
                x: function(d){ return d[0]; },
                
                y: function(d){ return d[1]; },
                                
                duration: 300,
                
                useInteractiveGuideline: true,
                
				xAxis: {
					axisLabel: 'weeks of the year ' + $scope.year
					},
				
				yAxis: {
					axisLabel: 'Bookings',
					tickFormat: function(d){
						return d3.format('')(d);
						},
					axisLabelDistance: -10
					},
            },
            
			title: {
				enable: true,
				text: ''
				},
			
			subtitle: {
				enable: true,
				text: '',
				css: {
					'text-align': 'center',
					'margin': '10px 13px 30px 7px'
					}
				},
			
			caption: {
				enable: true,
				html: ' ',
				css: {
					'text-align': 'justify',
					'font-size': '11px',
					'margin': '20px 13px 0px 7px'
					}
				}
        };

	$scope.togglemode = function() {
		$scope.cmode = ($scope.cmode !== "pax") ? "pax" : "bookings";
		$scope.ccmode = ($scope.cmode !== "pax") ? "pax" : "bookings";
		$scope.chartinit($scope.cmode);
		};
		
       
	$scope.buidData = function(data) {
		var i, oo, op, week, minweek, maxweek, obj = [], enable = {};
		var bkgtype = { 
				booking: { data: [], label:'bookings', color:'#FFE4C4', area:true, strokeWidth:3, classed:'' }, 
				weeloy: { data: [], label:'weeloy', color:'#0000FF', area:false, strokeWidth:'', classed:'' }, 
				website: {data: [], label:'website', color:'#000000', area:false, strokeWidth:'', classed:'' },
				cctms: { data: [], label:'callcenter/tms', color:'#2ca02c', area:false, strokeWidth:'', classed:'' },
				thirdparty: {data: [], label:'thirdparty', color:'#DC143C', area:false, strokeWidth:'', classed:'' },
				facebook: {data: [], label:'facebook', color:'#008B8B', area:false, strokeWidth:'', classed:'' },
				walkin: {data: [], label:'walkin', color:'#DC143C', area:false, strokeWidth:'', classed:'' },
				cancel: {data: [], label:'cancel', color:'#7FFF00', area:false, strokeWidth:'', classed:'' },
				noshow: {data: [], label:'noshow', color:'#FF7F50', area:false, strokeWidth:'', classed:'' },
				lunch: {data: [], label:'lunch', color:'#FFD700', area:false, strokeWidth:'', classed:'dashed' },
				dinner: {data: [], label:'dinner', color:'#BDB76B', area:false, strokeWidth:'', classed:'dashed' }
			  };
						
		//Data is represented as an array of {x,y} pairs.
		names = Object.keys(bkgtype);
		minweek = 99;
		maxweek = 0;
		for (i = 1; i < data.length; i++) {
			op = data[i-1];
			oo = data[i];
			week = oo[$scope.field];
			if(week > maxweek) maxweek = week;
			if(week < minweek) minweek = week;
			for(j = 0; j < names.length; j++) {
				l = names[j];
				x = bkgtype[l];
				oo[l] += op[l];
				x.data.push([ week, oo[l]]);
				if(!(l in enable)) enable[l] = 0;
				enable[l] = oo[l];
				}
			}

		if(maxweek - minweek < 2) {
			$scope.errormsg = "NO ENOUGH DATA (at least 2 weeks, and x bookings)";
			return null;
			}
			
		for(j = 0; j < names.length; j++) {
			l = names[j];
			x = bkgtype[l];
			if(enable[l] > 5)
				obj.push({ values: x.data, key: x.label, color: x.color, area: x.area, strokeWidth: x.strokeWidth, classed: x.classed });
			}
			
		return obj;
		};

	$scope.togglemode = function() {
		var data;
		
		$scope.cmode = ($scope.cmode !== "pax") ? "pax" : "bookings";
		$scope.ccmode = ($scope.cmode !== "pax") ? "pax" : "bookings";

		if($scope.booking === null)
			return;

		$scope.options.title.html = $scope.Title + ' in <span style="color:navy">' + $scope.cmode + '</span>';
		if($scope.restotitle !== "")
			$scope.options.title.html += ' for <span style="color: darkred;">' + $scope.restotitle + '</span>';
		$scope.options.subtitle.html = $scope.subTitle;
		$scope.options.caption.html = $scope.caption;
		$scope.options.chart.yAxis.axisLabel = ($scope.cmode !== "pax") ? "Bookings" : "Pax";
		$scope.options.caption.enable = true;

		$scope.data = ($scope.cmode !== "pax") ? $scope.booking : $scope.pax;

		$timeout(function() { console.log('refreshing'); }, 1000);
		$scope.api.update();
		};
		       
 	$scope.chartinit = function(mode, data) { 
		$scope.restotitle = wl_resto_title;
		$scope.booking = $scope.buidData(wl_stats_data_booking);	
		$scope.pax = $scope.buidData(wl_stats_data_pax);		

		$scope.cmode = "pax";
		$scope.togglemode();

		if($scope.booking === null)
			return;
			
		};
 
var closeCumul = 1;
function startCumul() {
	var i, j, l, p, names, tmp, elAr, data, oo, totalOb = {}, color, myPiedata;

	if(closeCumul < 0) return;
	console.log('not ready closeCumul ' + closeCumul);

	if(closeCumul++ > 20) {
		closeCumul = -1;
		}
		
	if(wl_stats_data_booking === null) {
		setInterval(startCumul, 2000);
		return;
		}

	closeCumul = -1;
	if(!(wl_stats_data_booking instanceof Array) || wl_stats_data_booking.length < 1) {
		console.log('no data closeCumul ' + closeCumul);
		return;
		}
		
	$scope.chartinit();
	}
	
startCumul();
	

});

</script>