(function(app) {
    app.factory('API', API);
    API.$inject = [
        'BlocklistConfigurationService',
        'TemplateConfigurationService',
    ]; 

    function API(
        BlocklistConfigurationService,
        TemplateConfigurationService
    ) {
        var service = {
            blocklistconfiguration:BlocklistConfigurationService,
            templateconfiguration: TemplateConfigurationService
        };
        return service;
    }
})(angular.module('app.api', [
    'app.api.blocklist.blocklistconfiguration',
    'app.api.templatemanagement.templateconfiguration'
]));


