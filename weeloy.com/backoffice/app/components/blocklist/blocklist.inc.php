<?php $isTheFunKitchen = (preg_match("/TheFunKitchen/", $theRestaurant)); ?>

<style>
   .js .inputfile {
    width: 0.1px;
    height: 0.1px;
    opacity: 0;
    overflow: hidden;
    position: absolute;
    z-index: -1;
}

.inputfile  label {
    max-width: 80%;
    font-size: 1.25rem;
    /* 20px */
    font-weight: 700;
    text-overflow: ellipsis;
    white-space: nowrap;
    cursor: pointer;
    display: inline-block;
    overflow: hidden;
    padding: 0.625rem 1.25rem;
    /* 10px 20px */
}

.no-js .inputfile  label {
    display: none;
}

.inputfile:focus  label,
.inputfile.has-focus   label {
    outline: 1px dotted #000;
    outline: -webkit-focus-ring-color auto 5px;
}
</style>

<div class="container">
    <div class="row">
        <div class="col-md-12 left-sec">
            <div ng-controller="blockListConfigurationController" >
                <input type='hidden' id='restaurant' value ="<?php echo $theRestaurant ?>" />
                <input type='hidden' id='email' value ="<?php echo $_SESSION['user_backoffice']['email'] ?>" />

                <div id='listing' ng-show='!createModeDisplay'>
                    <div class="form-group"  style="margin-bottom:25px;">
                        <div class="col-md-4">
                            <div class="input-group col-md-4">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>
                                <input type="text" class="form-control input-sm" ng-model="searchText" ng-change="paginator.setPage(0);" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" style='width:200px;'/> 
                            </div>
                        </div>

                        <div class="col-md-2" >
                        <span   class="btn btn-success btn-sm"  onclick="document.getElementById('file_upload').click();" style='color:white;width:200px;'><i class='glyphicon glyphicon-file' ></i>Import <input id="file_upload" type="file" name='files' custom-on-change="importlist" class="inputfile" style="display: none;"> </span>
                        </div>
                        <div class="col-md-2"></div>
                        <div class="col-md-4">
                            <a href ng-click="switchview('create', '');" class="btn btn-success btn-sm" style='color:white;width:200px;'><span class='glyphicon glyphicon-certificate'></span> &nbsp;Create a New Blocklist</a>
                        </div>      
                    </div>
                    <div style=" clear: both;"></div>
                   
                    <table width='100%' class="table table-condensed table-striped" style='font-size:12px;'>
                        <thead>
                            <th ng-repeat="y in tabletitle"><tbtitle var="{{y.a}}" name='tabletitle' module='moduleName'/></th>
                            <th>updatestatus</th>
                        <th> &nbsp; </th>
                       
                        </thead>
                        <tbody style='font-family:helvetica;font-size:12px;'>
                            <tr ng-repeat="x in blockedlist = (blockedlist| filter:searchText | orderBy:predicate:reverse)" style='font-family:helvetica;font-size:11px;'>
                                <td ng-repeat="y in tabletitle">
                                    <span ng-if ="y.a != 'updatestatus' && y.a != 'status' "  >{{ x[y.a] | adatereverse:y.c }}</span>
                                    <span ng-if ="y.a != 'updatestatus' && y.a == 'status' "  ng-style="x[y.a] === 'allowed' && {'color':'green'}  || x[y.a] === 'blocked' && {'color':'red'}" ><b>{{ x[y.a] | adatereverse:y.c }}</b></span>
                                </td>
                                <td><a href ng-click="switchstatus('update', x)" style='color:blue;'><span >{{ x['changestatus'] | adatereverse:y.c }} </span></a></td>
                                 <td ><a href popover="{{x.reason}}" data-popover-trigger="mouseenter" ><span ng-if="x.reason !== '' "><i class ='glyphicon glyphicon-asterisk'></i></span></a></td>

                            </tr><tr><td colspan='{{tabletitle.length + 3}}'></td></tr>
                        </tbody>
                    </table>
                    <div ng-if="filteredBlock.length >= paginator.getRowperPage()" class="col-md-12" align="center" style='font-size:11px' ng-include="'inc/paginator.html'"></div>

                </div>
                <div class="col-md-12" ng-show='createModeDisplay'>

                    <a href class='btn btn-info btn-sm customColor' ng-click="switchview('list', '')"><span class='glyphicon glyphicon-step-backward'></span> &nbsp;Back to listing</a>
                    <br />
                    <div class="col-md-2"></div>
                    <div class="col-md-8">


                        <form ng-submit="savecontact();" name="MyForm">
                            
                            <div class="row" ng-repeat="y in tabletitleContent| filter: {a: '!value' }" style="margin: 0 0 20px 0;font-size:12px;font-familly:Roboto">
                                <div class="input-group" ng-if="y.t === 'input'">
                                    <span class="input-group-addon input11"><i class="glyphicon glyphicon-{{y.d}} input14"></i>&nbsp; {{y.b}}</span>
                                    <input type="text" class="form-control input-sm" ng-model="selectedItem[y.a]" ng-change="cleaninput(y.a)" ng-readonly="y.a === 'name' && action === 'update'" >
                                </div> 
                                <div class="input-group" ng-if="y.t === 'inputmobile'">
                                    <span class="input-group-addon input11"><i class="glyphicon glyphicon-{{y.d}} input14"></i>&nbsp; {{y.b}}</span>
                                    <input type="text" class="form-control input-sm" ng-model="selectedItem[y.a]" ng-change="cleaninput(y.a)" ng-readonly="y.a === 'name' && action === 'update'" >
                                </div>
                                    <p ng-if="y.t === 'inputmobile'" style='margin-left:20px;margin-top:5px;color:#808080'>Don't forget to put International prefix for mobile no.<b>(ex:+65 97773370)</b> <br /> </p>
                                <div class="input-group" ng-if="y.t === 'textarea'">
                                     <span class="input-group-addon input11"><i class="glyphicon glyphicon-info-sign input14"></i>&nbsp; {{y.b}}</span>
                                        <textarea type="text" ng-model='selectedItem[y.a]' class="form-control input" id='blreason' name='reason' rows="2"  placeholder="Reason for blocking"></textarea>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <input type="submit" id="submit" class="btn btn-success btn-sm" value="Save" />
    <!--                            //<a href ng-click='savecontact();' c style='color:white;width:200px;'><span class='glyphicon glyphicon-save'></span> Save </a>-->
                            </div>
                        </form>
                    </div>
                    <div class="col-md-2"></div>
                </div>
            </div>
        </div>
    </div>

</div>



<script type="text/javascript" src="app/components/blocklist/blocklistConfigurationController.js"></script>




