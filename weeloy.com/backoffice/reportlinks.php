<div class="row">
  <div class="col-md-4">
	<div class="white-bg NoMargin">
	  <h2>Useful Links</h2>
	  <ul class="usefullinks">
		<li><a href="">Latest News</a></li>
		<li><a href="">Recent Blogs</a></li>
		<li><a href="">Support Forums</a></li>
		<li><a href="">Company Information</a></li>
		<li><a href="">Term &amp; Conditions</a></li>
	  </ul>
	</div>
  </div>
  <div class="col-md-4">
	<div class="white-bg NoMargin">
	  <div data-jmodtip="&lt;strong&gt;Edit module&lt;/strong&gt;&lt;br /&gt;Newsletter&lt;br /&gt;Position: footer-b" data-jmodediturl="" class="rt-block box1 nomarginright jmoddiv">
		<div class="module-surround">
		  <div class="module-title">
			<h2 class="title">Newsletter</h2>
		  </div>
		  <div class="acymailing_introtext">Stay tuned with our latest news!</div>
		  <br />
		  <form>
			<input type="text" value="" name="user[name]" class="form-control" placeholder="Name" disabled="disabled" id="user_name_formAcymailing21311">
			<br />
			<input type="text" value=""name="user[email]" class="form-control" placeholder="E-mail" disabled="disabled" id="user_email_formAcymailing21311">
			<br />
			<input type="submit" onclick="try{ return submitacymailingform('optin','formAcymailing21311'); }catch(err){alert('The form could not be submitted '+err);return false;}" name="Submit" value="Subscribe" class="button subbutton btn btn-primary customColor">
		  </form>
		</div>
	  </div>
	</div>
  </div>
  <div class="col-md-4">
	<div class="white-bg NoMargin">
	  <h2>About Us</h2>
	  <div class="custombox1">
		<p>We are Weeloy An awesome Social Distribution Partner.</p>
		<div class="aboutus">
		  <ul>
			<li><span><i class="fa fa-home"></i></span></li>
			<li><span><i class="fa fa-phone"></i></span></li>
			<li><span><i class="fa fa-envelope"></i> support@weeloy.com</span></li>
		  </ul>
		</div>
	  </div>
	</div>
  </div>
</div>