<?php

require_once("conf/conf.init.inc.php");
require_once("lib/wpdo.inc.php");
require_once("lib/class.coding.inc.php");
require_once("lib/class.cluster.inc.php");
require_once("lib/class.cluster.inc.php");

$arg = $_REQUEST["arg"];
$dataR = explode("|", detokenize($arg));
$restaurant = trim($dataR[0]);
$email = trim($dataR[1]);
$second = time() - intval(trim($dataR[2]));
if($second < 0 || $second > 7200) return;

$cluster = new WY_Cluster();
$cluster->read('SLAVE', $email, 'TMS', 'TMS', '');
if($cluster->result < 0) return;
$content = $cluster->clustcontent[0];
if(substr($content, 0, strlen("restaurant")) != "restaurant") return;
$contentAr = explode(";", $content);
$contentAr[0] = "restaurant=$restaurant";
$content = implode(";", $contentAr);

/*
$content = "restaurant=$restaurant;topic=Layout|Preferences";

if($restaurant == "SG_SG_R_TheFunKitchen")
	$content .= "|Waitinglist";
*/

$cluster->update('SLAVE', $email, 'TMS', 'TMS', $content);

//echo "TSM reconfigured to $restaurant for $email -> $content";
header("Location: ../admin_tms/index.php");
?>