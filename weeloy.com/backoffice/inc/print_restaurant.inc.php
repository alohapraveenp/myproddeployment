
<div class="container">
<div class="row">
<div class="col-xl-12 left-sec" ng-controller="printrestaurantController" ng-init="moduleName='printrestaurant'; " ng-cloak>

		<form action="{{ action }}" name='idFormsQuery' id='idFormsQuery' method='POST' enctype='multipart/form-data' >
		<input type='hidden' value="{{ navbar }}" id='navbar' name='navbar'>
		<table width='100%'><tr><td style='font-size:x-large;font-weight: bold;'> {{ dummy }} </td><td>
          </td></tr></table><hr>
		<select ng-model="theRestaurant" id='theRestaurant' name='theRestaurant'  ng-change="triggerSubmit(theRestaurant);">
			<option ng-repeat="x in view = (restaurants | filter:searchText)" value="{{ x.name }}" ng-disabled="x.type=='-'">{{ x.title }}</option>
		</select>  <input ng-model="searchText" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false"  style='margin-left:20px'>
		</td></tr></table>
		<br />	<button type='submit' class='btn btn-info btn-sm customColor'><span class='glyphicon glyphicon-search'></span> &nbsp;Search</button></form><br /><br />
</div>
</div>
</div>

<script>

var token = <?php echo "'".$_SESSION['user_backoffice']['token']."';"; ?>
var email =  <?php echo "'".$_SESSION['user_backoffice']['email']."';";?>

app.controller('printrestaurantController', ['$scope', '$http', '$timeout', '$filter', function($scope, $http, $timeout, $filter) {
	var resto = <?php echo "'". implode("," , $restaurantAr) ."'";?>;
	var title = <?php echo "'". implode("," , $titleAr) ."'";?>;
	var country = <?php echo "'". implode("," , $countryAr) ."'";?>;
	var tmp, i, c, k, restoAr = resto.split(','), titleAr = title.split(','), countryAr = country.split(',');
	var line = '----------------------------';
	var order = ['France', 'Hong Kong', 'India', 'Indonesia', 'Malaysia', 'Singapore', 'South Korea', 'Thailand', 'unknown'];
	
	$scope.action = <?php echo "'". $action ."'";?>;
	$scope.navbar = <?php echo "'". $navbar ."'";?>;
	$scope.navbarTitle = <?php echo "'". $navbarTitle ."'";?>;
	$scope.crestaurant = <?php echo "'". $theRestaurant ."'";?>;
	$scope.searchText = "";
	$scope.theRestaurant = $scope.crestaurant;
	$scope.dummy = "";
	
	k = restoAr.indexOf($scope.crestaurant);

	$scope.restaurants = [];
	if(restoAr.length > titleAr.length || restoAr.length > countryAr.length || restoAr.length < 1)
		return;
	
	for(i = 0; i < restoAr.length; i++) {
		if(titleAr[i] === '') titleAr[i] = restoAr[i].substr(8);
		if(countryAr[i] === '') countryAr[i] = 'unknown';			
		$scope.restaurants.push({ name: restoAr[i], title: restoAr[i], country: countryAr[i], type: '' });
		}
	$scope.restaurants.sort(function(a, b) { var i = order.indexOf(a.country), j = order.indexOf(b.country); if(i !== j) return i-j; return a.title - b.title;  } );		

	tmp = [];
	c = 'starting'; 
	$scope.restaurants.map(function(a) {
		if(c !== a.country) {
			tmp.push({ name: '', title: '', country: '', type: '-', selected: false });
			tmp.push({ name: a.country, title: a.country.toUpperCase(), country: '', type: '-', selected: false });
			tmp.push({ name: line, title: line, country: line, type: '-', selected: false });		
			}
		c = a.country;
		tmp.push(a);
		});
	$scope.restaurants = tmp.slice(0);

	$('#idFormsQuery').attr('action', $scope.action);

	$scope.triggerSubmit = function() {
		$('#idFormsQuery').submit();
		};
	
	

	$scope.triggerSearch = function() {
		if(typeof $scope.searchText !== "string" || $scope.searchText.length < 3)
			return;
		
		$scope.restaurants_view = $filter('filter')($scope.restaurants, $scope.searchText);
		};
		
	$timeout(function() {
		$scope.myColor = 4; // Yellow
	});

}]);	
</script>

  