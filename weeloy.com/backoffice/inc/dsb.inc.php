<style type="text/css">
	.dealdateshow {
	    font-size: 20px;
	    text-align: center;
	    font-weight: bold;
	}
	.dealtext {
		font-size: 11px;
		text-align: center;
	}
</style>

<div class="container">
	<div class="row">
		<div class="col-md-12 left-sec">
			<div ng-controller="MainControllerDSB" ng-init="moduleName='DSB'; listingFlag=true; viewcontentFlag=false; createDSBFlag=false;" >
			<!-- Token -->
				<input type='hidden' id='token' value ="<?php echo $_SESSION['user_backoffice']['token'] ?>" />
				<!-- Listing Section Start -->
				<div id='listing' ng-show='listingFlag'>
				    <div class="form-group"  style='margin-bottom:25px;'>
				        <div class="col-md-4">
							<div class="input-group col-md-3">
								<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>
								<input type="text" class="form-control input-sm" ng-model="searchText" ng-change="paginator.setPage(0);" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" style='width:200px;'> 
							</div>
							<span style='font-size:11px;'> Number of selected DSB: <strong> {{filteredDSB.length}} </strong></span>
				        </div>
						<div class="col-md-3">
							<a href ng-click='create();' class="btn btn-success btn-sm" style='color:white;width:150px;'><span class='glyphicon glyphicon-certificate'></span> &nbsp;Create a New DSB</a>
						</div>
						<div class="col-md-3">
						  <a href ng-click='extractSelection();' class="btn btn-info btn-sm" style='color:white;width:150px;'><span class='glyphicon glyphicon-save'></span> &nbsp;Extract Selection</a>
						</div>
						<div class="col-md-2">
							<div class="btn-group" uib-dropdown >
							<button type="button" class="btn btn-default btn-xs" uib-dropdown-toggle aria-expanded="false" style='font-size:11px;'>Page Size<span class="caret"></span></button>
							<ul class="dropdown-menu" uib-dropdown-menu role="menu">
							<li ng-repeat="x in paginator.pagerange()" class='glyphiconsize'><a href ng-class="{ 'glyphicon glyphicon-ok glyphiconsize': x === paginator.getRowperPage() }" ng-click="paginator.setRowperPage(x)"> {{x}}</a></li>
							</ul>
							</div> 		
						</div>
			    	</div>

			    	<table width='100%' class="table table-condensed table-striped table-hover" style='font-size:12px;'>
						<thead>
						<tr><th ng-repeat="y in tabletitle"><tbtitle var="{{y.a}}" name='tabletitle' module='moduleName'/></th>
						<th width='20'> &nbsp; </th>
						<th width='50'>update</th>
						<th width='20'> &nbsp; </th>
				     	<th width='50'>delete</th>
				     	</tr>
						</thead>
						<tr ng-repeat="x in filteredDSB = (DSB | filter:searchText) | sizefilter:this | orderBy:predicate:reverse | slicefilter:paginator.getPage():paginator.getRowperPage():paginator.getItemCount() | limitTo:paginator.getRowperPage()" style='font-family:helvetica;font-size:11px;'>
							<td ng-repeat="y in tabletitle"><a href ng-click="view(x)" ng-style="{ 'text-align' : getAlignment(x[y.a]) }">{{x[y.a] | adatereverse:y.c }}</a></td>
							<td width='20'>&nbsp;</td>
							<td align='center'><a href ng-click="update(x)"><span class='glyphicon glyphicon-pencil blue'></a></span></td>
							<td width='20'>&nbsp;</td>
							<td align='center'><a href ng-click="delete(x)"><span class='glyphicon glyphicon-trash red'></span></a></td>
						</tr>
						</tr><tr><td colspan='10'></td></tr>
					</table>
					<div ng-show="loadingimg">
						<center>
							<img src="../images/loading2.gif">
						</center>
					</div>
					<div ng-if="filteredDSB.length >= paginator.getRowperPage()" class="col-md-12" align="center" style='font-size:11px' ng-include="'inc/paginator.html'">
						
					</div>
				</div>
				<!-- Listing Section Start -->

				<!-- View Content Flag Start -->
				<div class="col-md-12" ng-show='viewcontentFlag'>
					<br />
					<a href class='btn btn-info btn-sm customColor' ng-click="backlisting()"><span class='glyphicon glyphicon-step-backward'></span> &nbsp;Back to listing</a>
					<br />
					<table class='table-striped' style="margin: 0 0 150px 30px;font-size:13px;font-family: Roboto">		
						<tr ng-repeat="y in tableDSBContent | filter: {b: '!Picture'}"" ng-if="y.t !== 'imagebutton'"><td nowrap><strong>{{ y.b }} </strong></td><td> &nbsp; </td><td>{{ selectedItem[y.a] | adatereverse:y.c }}</td></tr>
					</table>
				</div>
				<!-- View Content Flag End -->

				<!-- createDBS Flag Start -->
				<div class="col-md-12" ng-show='createDSBFlag'>
					<br />
					<a href class='btn btn-info btn-sm customColor' ng-click="backlisting()"><span class='glyphicon glyphicon-step-backward'></span> &nbsp;Back to listing</a>
					<br />
					<div class="col-md-1"></div>

					<div class="col-md-9">
					   
					   <div class="dealdateshow">
					   	 Your post will go live {{mydata_startd}} at 03:00am
					   </div>
					   <div class="dealtext">
					   * Please be adviced that Daily Special will only accept one posting per day thank you.
					   </div>
					   <div class="row" ng-repeat="y in tableDSBContent | filter: {t: '!dontshow' }" style="margin: 0 0 20px 0;font-size:12px;font-familly:Roboto">
			                        
							<div class="input-group" ng-if="y.t === 'input'">
								<span class="input-group-addon input11"><i class="fa fa-{{y.d}}"></i>&nbsp; {{y.b}}</span>
								<input type="text" class="form-control input-sm" ng-model="oo[y.a]" ng-readonly="(y.readonly ===1 && action=='update') || y.alwaysreadonly === 1" >
							</div> 
							<div class="input-group" ng-if="y.t === 'textarea'">
								<span class="input-group-addon input11"><i class="fa fa-{{y.d}}"></i>&nbsp; {{y.b}}</span>
								<textarea class="form-control input-sm" ng-model="oo[y.a]" rows="5" ></textarea>
							</div>
							<!-- <div class="input-group" ng-if="y.t === 'date'">
								<span class="input-group-addon input11"><i class="fa fa-{{y.d}}"></i>&nbsp; {{y.b}}</span>
								<input type="text" class="form-control input-sm" ng-model="[y.u]" readonly >
							</div> -->
							   <div class="input-group"  ng-if="y.t==='checkbox' ">
								   <input   type="checkbox" ng-model="oo[y.a]" ng-checked ="{{oo[y.a]}}"> &nbsp; {{y.b}}
									   
								</div>
				            <div class="input-group" ng-if="y.t === 'array'">
								<div class='input-group-btn' uib-dropdown >
									<button type='button' class='btn btn-default btn-sm input12' uib-dropdown-toggle >
										<i class="fa fa-{{y.d}}"></i>&nbsp; {{y.b}} <span class='caret'></span></button>
									<ul class='dropdown-menu scrollable-menu' uib-dropdown-menu>
									<li ng-repeat="p in y.val"><a href ng-click="selectedItem[y.a]=p;y.func()">{{ p }}</a></li>
									</ul>
								</div>
								<input type='text' ng-model='selectedItem[y.a]' class='form-control input-sm' readonly >
							</div>
				            <div class="input-group" ng-if="y.t === 'imagebutton'">
							    <span class="input-group-addon"><i class="glyphicon glyphicon-file"></i> &nbsp; {{y.b}}</span>
							    <!-- <input type="file" name="{{y.a}}" id="{{y.a}}" class="form-control input-sm" onchange="angular.element(this).scope().uploadImage(this)" ng-model="file" placeholder="Upload Files" accept="image/*">  -->
			                	<input type="file" name="{{y.a}}" id="img" class="form-control input-sm"  ng-model="file" placeholder="Upload Files" accept="image/*">  
			                </div>

					   </div>
					   <div class="row">
					   	 Categories:
					   	 <br>

				          <div class="col-md-4">
				            <center>
				              <select ng-model="oo.selectedOption['cat1']" 
				                      ng-options="cuisines.cuisine as cuisines.cuisine for cuisines in cuisinelist">
				                  <option value="">Select Categories</option>
				              </select>
				            </center>  
				          </div>
				          <div class="col-md-4">
				            <center>
				              <select ng-model="oo.selectedOption['cat2']" 
				                      ng-options="cuisines.cuisine as cuisines.cuisine for cuisines in cuisinelist">
				                  <option value="">Select Categories</option>
				              </select>
				            </center>
				          </div>
				          <div class="col-md-4">
				            <center>
				              <select ng-model="oo.selectedOption['cat3']" 
				                      ng-options="cuisines.cuisine as cuisines.cuisine for cuisines in cuisinelist">
				                  <option value="">Select Categories</option>
				              </select>
				            </center> 
				          </div>
				        </div>

					   <br />
					</div>
					<div class="col-md-2"></div>
					<div class="col-md-7"></div>
					<div class="col-md-5">
						<a href ng-click='savenewdsb();' class="btn btn-success btn-sm" style="color:white;width:150px"><i class='glyphicon glyphicon-save'></i> &nbsp;{{ buttonlabel }} </a><br />
					</div>

				</div>
				<!-- createDBS Flag End -->
			</div>
		</div>
	</div>
</div>
            
<script>

app.controller('MainControllerDSB', ['$scope', '$http', 'bookService', 'extractService', function($scope, $http, bookService, extractService) {

	var forceDate = new Date(new Date().getFullYear()-2, 0, 1);
	var todaydate = new Date();
	$scope.token = $('#token').val();
	$scope.paginator = new Pagination(25);
  	$scope.selectedItem = null;
	$scope.email = <?php echo "'" . $email . "';"; ?>
	$scope.restaurant = <?php echo "'" . $theRestaurant . "';"; ?>
	$scope.restauranttitle = ""
	$scope.restaurantcode = "";
 	$scope.mydata_start = new Date();
 	$scope.mydata_startd = new Date();
 	$scope.names = [];
	$scope.mydata_start.setDate($scope.mydata_start.getDate() + 1);
    $scope.mydata_startd = $scope.mydata_start;
    $scope.mydata_start = $scope.mydata_start.getFullYear() + "-" +   ("0"+($scope.mydata_start.getMonth()+1)).slice(-2) + "-" + ("0" + $scope.mydata_start.getDate()).slice(-2);
    
    $scope.mydata_startd = $scope.mydata_startd.toDateString().substring(3, 15);
	$scope.tabletitle = [ 
							{'a':'ID', 'b':'ID', c:'', q:'down', cc: 'black' }, 
							{'a':'headline', 'b':'Headline', c:'', q:'down', cc: 'black' }, 
							{'a':'deal_date', 'b':'Deal Date', c:'date', q:'down', cc: 'black' }, 
							{'a':'category', 'b':'Category', c:'', q:'down', cc: 'black' }
						];
	$scope.loadingimg = true;

	$scope.bckups = $scope.tabletitle.slice(0);

	$scope.tableDSBContent = [ 
								{ a:'ID', b:'ID' , c:'', t:'input', alwaysreadonly :1}, 
								{ a:'headline', b:'Headline' , c:'', t:'input'},
								{ a:'description', b:'Description' , c:'', t:'textarea'},  
								{ a:'deal_date', b:'Deal Date' , c:'date', t:'date', u:$scope.mydata_start},
								{ a:'cover', b:'Cover', c:'', d:'picture', t:'imagebutton' }, 
								{ a:'code', b:'Code' , c:'', t:'input', alwaysreadonly :1}, 
								{ a:'restaurant', b:'Restaurant' , c:'', t:'input', alwaysreadonly :1}
							 ];
	$scope.DSB = [];

	$scope.getAlignment = function bkgetalignment(a) {
		return (typeof a === 'number' && a < 10) ? 'center':'left';
		};

    $scope.predicate = "datetime";
	$scope.reverse = false;
	
	$scope.gblindex = function(ID) { 
		for(var i = 0; i < $scope.DSB.length; i++) 
			if($scope.DSB[i].ID === ID) 
				return i;
		return -1;
		};
		
	$scope.initorder = function() {
		$scope.tabletitle = $scope.bckups;
		$scope.predicate = "datetime";
		$scope.reverse = true;
		};
		
	$scope.reorder = function(item, alter) {
		alter = alter || "";
		if (alter !== "")  item = alter;
		$scope.reverse = ($scope.predicate === item) ? !$scope.reverse : false;
		$scope.predicate = item;
	};
	
	bookService.readDSB($scope.restaurant).then(function(response) {
		$scope.DSB = [];

		if(response.status !== '1') {
			alert("unable to read data "+response.errors);
			return;
			}
		$scope.loadingimg = false;
		$scope.DSB = response.data; 
		$scope.paginator.setItemCount($scope.DSB.length);
		$scope.initorder();
		});

	
	$scope.allmyviews = ['listingFlag', 'viewcontentFlag', 'createDSBFlag'];	
	$scope.reset = function(item) {
		$scope.allmyviews.forEach(function(f) { $scope[f] = false; });
		$scope[item] = true;
		};
		
  	$scope.view = function(oo) {  
   		$scope.selectedItem = oo;
   		$scope.reset('viewcontentFlag');
  	};

  	$scope.backlisting = function() {
 		$scope.reset('listingFlag');
   	};

   	$scope.initdsb = function() {
            $scope.oo = {
              id: "",
              restaurant: "",
              code: "",
              headline: "",
              description: "",
              status: "pending",
              deal_date: "",
              selectedOption: {
                cat1: "",
                cat2: "",
                cat3: ""
              },
              images: {
                cover: {},
                cover2: {}
              }
            };

            $scope.selectedItemnew = {
            	name: "",
            	object_type: "",
            	path: "",
            	status: "",
            	description: "",
            	morder: "99",
            	index: ""
            };
         };

    $scope.initdsb();
    $scope.file = null;

	$scope.newobj = function() {
		var i, j, id = {};
    	$('#cover').files = [];

		
		for(i = 0; i < 20; i++) {
			id = Math.floor((Math.random() * 10000000) + 1);
			for(j = 0; j < $scope.DSB.length; j++)
				if($scope.DSB[j].ID === id)
					break;
			if(j >= $scope.DSB.length)
				break;
			}
		if(i < 20) {
			return id;
			}
		alert("internal error");
		return null;
	};
		
	$scope.create = function() {
		var newid = $scope.newobj();
		//clear data
		$scope.initdsb();
		
		if(newid === null)
			return;

		$scope.tableDSBContent = [ 
								{ a:'ID', b:'ID' , c:'', t:'hidden', alwaysreadonly :1},
								{ a:'deal_date', b:'Deal Date' , c:'', t:'hidden', u:$scope.mydata_start}, 
								{ a:'headline', b:'Headline' , c:'', t:'input'},
								{ a:'description', b:'Description' , c:'', t:'textarea'},  
								{ a:'cover', b:'Cover', c:'', d:'picture', t:'imagebutton' }, 
								{ a:'code', b:'Code' , c:'', t:'hidden', alwaysreadonly :1}, 
								{ a:'restaurant', b:'Restaurant' , c:'', t:'hidden', alwaysreadonly :1}
							 ];
   		$scope.oo.ID = newid;
   		$scope.oo.restaurant = $scope.restauranttitle;
   		$scope.oo.code = $scope.restaurant;
		$scope.buttonlabel = "Create";
		$scope.action = "create";
		$scope.reset('createDSBFlag');		
		return false;
		};

	$scope.update = function(oo) {
		//convert date format
		var ct = oo.deal_date;
		var ctx = new Date(ct);
		oo.deal_date = ctx.getFullYear() + "-" +   ("0"+(ctx.getMonth()+1)).slice(-2) + "-" + ("0" + ctx.getDate()).slice(-2);
   		$scope.oo = oo;
		// $scope.tableDSBContent = [ 
		// 						{ a:'ID', b:'ID' , c:'', t:'input', alwaysreadonly :1}, 
		// 						{ a:'headline', b:'Headline' , c:'', t:'input'},
		// 						{ a:'description', b:'Description' , c:'', t:'textarea'},  
		// 						{ a:'deal_date', b:'Deal Date' , c:'', t:'date', u:oo.deal_date},
		// 						{ a:'cover', b:'Cover', c:'', d:'picture', t:'imagebutton' }, 
		// 						{ a:'code', b:'Code' , c:'', t:'input', alwaysreadonly :1}, 
		// 						{ a:'restaurant', b:'Restaurant' , c:'', t:'input', alwaysreadonly :1}
		// 					 ];

		$scope.tableDSBContent = [ 
								{ a:'ID', b:'ID' , c:'', t:'hidden', alwaysreadonly :1},
								{ a:'deal_date', b:'Deal Date' , c:'', t:'hidden', u:$scope.mydata_start}, 
								{ a:'headline', b:'Headline' , c:'', t:'input'},
								{ a:'description', b:'Description' , c:'', t:'textarea'},  
								{ a:'cover', b:'Cover', c:'', d:'picture', t:'imagebutton' }, 
								{ a:'code', b:'Code' , c:'', t:'hidden', alwaysreadonly :1}, 
								{ a:'restaurant', b:'Restaurant' , c:'', t:'hidden', alwaysreadonly :1}
							 ];
		$scope.buttonlabel = "Update";
		$scope.action = "update";
		$scope.reset('createDSBFlag');		
		return false;
		};

  $scope.extractSelection = function() {
  
	var exportselect = $scope.filteredDSB;
	var i, j, u, data, maxlimit = 1500, limit, titleAr, contentAr, oo;
	var filename, cdate = new Date();

	titleAr = ["restaurant", "DSB"];
	contentAr = ["resto", "DSB"];

	data = titleAr.join(",");
	limit = exportselect.length;		
	if (limit > maxlimit) limit = maxlimit;
	for (i = 0; i < limit; i++) {
		data += "\n";			
		u = exportselect[i];
		for (j = 0; j < contentAr.length; j++) 
			data += extractService.filter(u[contentAr[j]]) + ",";			
		}	

	filename = "DSB" + cdate.getDate() + cdate.getMonth() + cdate.getFullYear() + ".csv";
	extractService.save(filename, data); 				
  };


  $scope.savenewdsb = function() {
	  	$scope.oo.images = {
		        cover: {},
		        cover2:{}
		      };
	  	var inp = document.getElementById('img');
	  	if($scope.oo == null) {
	  		return alert("Wrong Data");
	  		}

	  	
	  	if($scope.oo.headline == '' || $scope.oo.description == ''){
	  		return alert("Please Fill Up All the fields");	
	  	}
	  	
	  	
	  	if($scope.action === "create"){
	  		for(i=0;i<$scope.DSB.length;i++)
			{
				var checkdate = new Date($scope.DSB[i].deal_date);
				checkdate = checkdate.getFullYear() + "-" +   ("0"+(checkdate.getMonth()+1)).slice(-2) + "-" + ("0" + checkdate.getDate()).slice(-2);
				if(checkdate == $scope.mydata_start){
					return alert("You have already created Daily Special for "+$scope.mydata_startd+", please edit or delete and recreate if you want to change your posting. Thank you.");
				}
			}
			$scope.uploadImage(inp,'create');
	  	}
	  	else
	  	{
	  		$scope.uploadImage(inp);
	  	}

	  	//add some delay for the image content to process
	  	setTimeout(function() {
	  		if($scope.action === "create"){
			 bookService.logevent(171,'DSB_create','DSB_EVENT');
			 bookService.createDSB($scope.oo).then(function(response) { 
				if(response.status > 0) {
					$scope.oo.index = ($scope.DSB.length > 0) ? $scope.DSB[$scope.DSB.length - 1].index + 1 : 1;
					$scope.oo.eventID = response.status;
					alert("DSB has been created with ID "+$scope.oo.headline); 
					$scope.DSB.push($scope.oo);
					setNavBar('DSB');
					}
				});
			}
			else {
				bookService.logevent(171,'DSB_update','DSB_EVENT');
				bookService.updateDSB($scope.oo).then(function(response) { 
					if(response.status > 0) {
						alert("DSB has been updated");
						//setNavBar('DSB');
						}
					else {
						alert("DSB has NOT been updated: "+ response.errors);
						}
					});
				}
			$scope.backlisting();
		}, 2000);

	};


	$scope.delete = function(oo) {
		if(confirm("Are you sure you want to delete " + oo.headline) == false)
			return;
		// console.log('delete');	
		bookService.deleteDSB($scope.restaurant, oo.ID, $scope.email).then(function() { 
 			var value = oo.headline, ind = $scope.gblindex(oo.ID);
 			// console.log('oo.ID ' + oo.ID);
			if(ind >= 0)
				$scope.DSB.splice(ind, 1);
			alert(value + " has been deleted"); 
			});
		
		};

	bookService.getrestauranttitle($scope.restaurant).then(function(response) {
		
		if(response.status== 1)
		{

			$scope.restauranttitle = response.data;
		}
		else
		{
		//$scope.loading = false;
		alert(response.data.errors);
		} 
	});


	bookService.getcuisine().then(function(response) {
		
		if(response.status == 1)
		{
			$scope.cuisinelist = response.data.cuisine;
		}
		else
		{
		//$scope.loading = false;
		alert(response.data.errors);
		} 
	});


	$scope.uploadImage = function(element,indi = "update")  {
	  
	  if(indi == "create")
	  {
	  	if(element.files.length == 0) {
	        alert("Select/Drop one file");
	        return false;
	      }
	  }
      
      if(element.files[0] == null)
      {
      	return false;
      	
      }
      else
      {
      	filename = element.files[0].name;
      
		ext = filename.substring(filename.lastIndexOf(".")+1).toLowerCase();
		if(ext != "jpg" && ext != "jpeg" && ext != "png" && ext != "gif") {
			alert("Invalid file type (jpg, jpeg, png, gif)");
			return false;
		}


		filename = element.files[0].name;    
		ext = filename.substring(filename.lastIndexOf(".")+1).toLowerCase();
		if(ext != "jpg" && ext != "jpeg" && ext != "png" && ext != "gif") {
			alert("Invalid file type (jpg, jpeg, png, gif)");
		return false;
		}

		var reader = new FileReader();
		$scope.oo.images.cover.name = element.files[0].name;
		$scope.oo.images.cover.media_type = 'picture';
		reader.addEventListener("load", function () {
		//$scope.oo.images.cover.content = "";      
		$scope.oo.images.cover.content = reader.result;
		}, false);
		//$scope.oo.images.cover.content = reader.result;
		$scope.oo.images.cover.content = "";
		console.log("Image Loaded");
		reader.readAsDataURL(element.files[0]);

		//resize image
		$scope.oo.images.cover2.name = element.files[0].name;
		$scope.oo.images.cover2.media_type = 'picture';
		var myCanvas = document.createElement("canvas");
	    var ctx = myCanvas.getContext('2d');
	    var img = new Image();
	    img.onload = function(){

	    	var MAX_WIDTH = 182;
	        var MAX_HEIGHT = 125;
	        var width = img.width;
	        var height = img.height;

	        if (width > height) {
	          if (width > MAX_WIDTH) {
	            height *= MAX_WIDTH / width;
	            width = MAX_WIDTH;
	          }
	        } else {
	          if (height > MAX_HEIGHT) {
	            width *= MAX_HEIGHT / height;
	            height = MAX_HEIGHT;
	          }
	        }

	        myCanvas.width = width;
	        myCanvas.height = height;
	        ctx.drawImage(img, 0, 0,img.width,img.height,0,0,myCanvas.width,myCanvas.height);
	    
	        $scope.oo.images.cover2.content = myCanvas.toDataURL();
	       
	    };
	    
	    img.src = URL.createObjectURL(element.files[0]);

      }
    };


    $scope.selectedItemnew = null;

  //   $scope.uploadFiles = function()	{
		// console.log("uploadFiles()");
		// var object_type = 'restaurant';
		// dom = $('#img')[0];
		// files = dom.files;  //get the DOM -> files = document.getElementById('file_upload').files;
		
		// console.log(files.mozFullPath);
		// if(files.length == 0) {
		// 	alert("Select/Drop one file");
		// 	return false;
		// 	}

		// filename = files[0].name;
		// ext = filename.substring(filename.lastIndexOf(".")+1).toLowerCase();
		// if(ext != "jpg" && ext != "jpeg" && ext != "png" && ext != "gif" && ext != "pdf") {
		// 	alert("Invalid file type (jpg, jpeg, png, gif)");
		// 	return false;
		// 	}
		// if(object_type == "") {
		// 	alert("Please Choose a category");
		// 	return false;
		// 	}
		// $scope.selectedItemnew.name = filename;
		// $scope.selectedItemnew.object_type = object_type;    
		// $scope.selectedItemnew.path = '';
		// $scope.selectedItemnew.status = 'inactive';
		// $scope.selectedItemnew.description = '';
		// $scope.selectedItemnew.morder = '99';
		// $scope.selectedItemnew.index = 1;

		// $scope.names.push( $scope.selectedItemnew );
		

  //      // START A LOADING SPINNER HERE

  //       // Create a formdata object and add the files
		// var data = new FormData();
		// $.each(files, function(key, value)
		// {
		// 	data.append(key, value);
		// });

		// data.append('token', token);
		// data.append('restaurant', $scope.restaurant);
		// data.append('category', 'restaurant');
  //       data.append('media_type', 'picture');

  //       $.ajax({
  //           url: 'saveimagesdsb.php?files',
  //           type: 'POST',
  //           data: data,
  //           cache: false,
  //           dataType: 'json',
  //           processData: false, // Don't process the files
  //           contentType: false,
  //           xhr: function() {
  //                   var myXhr = $.ajaxSettings.xhr();
  //                   if(myXhr.upload){
  //                       //myXhr.upload.addEventListener('progress',progressHandlingFunction, false);
  //                   }
  //                    return myXhr;
  //               },// Set content type to false as jQuery will tell the server its a query string request
  //           success: function(data, textStatus, jqXHR)
  //           	{
  //           	if(typeof data.error === 'undefined')
  //           		$scope.submitForm(dom, data);
                       
  //           	else {
  //           		//console.log(data.error);
  //           		$scope.selectedItem.remove();
  //           		$scope.$apply();
  //           		}
                   
  //            },
  //           error: function(jqXHR, textStatus, errorThrown) {
  //          		console.log(textStatus);		
  //           }
  //       });
      
  //   };

}]);

</script>
