<?php $isTheFunKitchen = (preg_match("/TheFunKitchen/", $theRestaurant)); ?>

<div class="container">
    <div class="row">
        <div class="col-md-12 left-sec">
            <div ng-controller="ControllerPromo" ng-init="moduleName = 'promotion'; listingFlagPromo = true" >
                <div ng-show='listingFlagPromo'>
                    <div class="form-group"  style='margin-bottom:25px;'>
                        <div class="col-md-4">
                            <div class="input-group col-md-4">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>
                                <input type="text" class="form-control input-sm" ng-model="searchText" ng-change="paginator.setPage(0);" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" style='width:200px;'> 
                            </div>
                        </div>
                        <div class="col-md-2"></div>
                        <div class="col-md-4">
                            <a href ng-click='create();' class="btn btn-success btn-sm" style='color:white;width:200px;'><span class='glyphicon glyphicon-certificate'></span> &nbsp;Create a New Promotion</a>
                        </div>
                    </div>

                    <div style=" clear: both;"></div>
                    <p ng-show="promos.length == 0">Click on create promotion to add your first promotion  </p> 

                    <table ng-show="promos.length > 0" width='100%' class="table table-condensed table-striped" style='font-size:12px;'>
                        <thead>
                            <tr>
                                <th ng-repeat="y in tabletitle">
                                    <tbtitle var="{{y.a}}" name='tabletitle'  module='moduleName'/>
                                    </th>
                                <th>Default Promo</th>
                                <th>Update</th><th>Delete</th>
                                <th>Move Up</th>
                            </tr>
                        </thead>
                        <tbody style='font-family:helvetica;font-size:12px;'>
                            <tr ng-repeat="x in filteredPeople = (promos| filter:searchText) | orderBy:predicate:reverse | slicefilter:paginator.getPage():paginator.getRowperPage():paginator.getItemCount() | limitTo:paginator.getRowperPage()">
                                <td ng-repeat="y in tabletitle"><a href ng-click="view(x)">{{ x[y.a] | adatereverse:y.c }}</a></td>
                                <td nowrap><a href ng-click="setdefault(x)" style='color:grey;'><span ng-class="{'glyphicon glyphicon-edit purple': x.is_default == '1', 'glyphicon glyphicon-edit': x.is_default == '0'}"></span></a></td>
                                <td nowrap><a href ng-click="update(x)" style='color:blue;'><span class='glyphicon glyphicon-pencil'></span></a></td>
                                <td nowrap><a href ng-click="delete(x)" style='color:red;'><span class='glyphicon glyphicon-trash'></span></a></td>
                                <td nowrap ><a href ng-click="setuppromo(x)" style='color:green;' ng-class="{'hidden': x.value == '1', 'visible': x.value !== '1'}" ><span class ="glyphicon glyphicon-arrow-up "></span></a></td>
                            </tr>
                            <tr><td colspan='10'></td></tr>
                        </tbody>
                    </table>
                    <div ng-if="filteredPeople.length >= paginator.getRowperPage()" class="col-md-12" align="center" style='font-size:11px' ng-include="'inc/paginator.html'"></div>
                </div>

                <div class="col-md-12" ng-show='viewFlagPromo'>
                    <br /><a href class='btn btn-info btn-sm customColor' ng-click="backlisting()"><span class='glyphicon glyphicon-step-backward'></span> &nbsp;Back to listing</a><br />
                    <table class='table-striped' style="margin: 0 0 150px 30px;font-size:13px;font-family: Roboto">		
                        <tr ng-repeat="y in tabletitleContent| filter: {b: '!picture'}"><td nowrap ><strong> {{ y.b}} </strong></td><td> &nbsp; </td><td>{{ selectedItem[y.a] | adatereverse:y.c }} </td></tr>
                    </table>
                </div>

                <div class="col-md-12" ng-show='createFlagPromo'>
                    <br />
                    <a href class='btn btn-info btn-sm customColor' ng-click="backlisting()"><span class='glyphicon glyphicon-step-backward'></span> &nbsp;Back to listing</a>
                    <br />
                    <div class="col-md-2"></div>
                    <div class="col-md-8">

                        <div class="row" ng-repeat="y in tabletitleContent| filter: {a: '!value' }" style="margin: 0 0 20px 0;font-size:12px;font-familly:Roboto">
                            
                            <div class="input-group" ng-if="y.t === 'input'">
                                <span class="input-group-addon input11"><i class="glyphicon glyphicon-{{y.d}} input14"></i>&nbsp; {{y.b}}</span>
                                <input type="text" class="form-control input-sm" ng-model="selectedItem[y.a]" ng-change="cleaninput(y.a)" ng-readonly="y.a === 'name' && action === 'update'" >
                            </div> 
                            
                            
                            
                            <div class="input-group" ng-if="y.t === 'textarea'">
                                <span class="input-group-addon input11"><i class="glyphicon glyphicon-{{y.d}} input14"></i>&nbsp; {{y.b}}</span>
                                <textarea class="form-control input-sm" ng-model="selectedItem[y.a]" rows="5" ng-change="cleaninput(y.a)" ></textarea>
                            </div>
                            
                            
                            <div class="input-group" ng-if="y.t === 'date'">
                                <span class="input-group-btn input11">
				  				<button type="button" class="btn btn-default btn-sm" ng-click="y.u.dateopen($event)"><i class="fa fa-calendar"></i>&nbsp; {{y.b}}</button>
                                </span>
									<input type="text" class="form-control input-sm" uib-datepicker-popup="{{y.u.formats[0]}}" min-date="y.u.minDate" max-date="y.u.maxDate" ng-model="y.u.originaldate"
									is-open="y.u.opened" datepicker-options="y.u.dateOptions" ng-change="y.u.onchange();" close-text="Close" readonly button-render/>
                            </div>
                            
                            <div class="input-group" ng-if="y.t === 'array'">
                                <div class='input-group-btn' uib-dropdown >
                                    <button type='button' class='btn btn-default btn-sm input11' uib-dropdown-toggle >
                                        <i class="glyphicon glyphicon-{{y.d}} input13"></i>&nbsp; {{y.b}}<span class='caret'></span></button>
                                    <ul class='dropdown-menu scrollable-menu' uib-dropdown-menu>
                                        <li ng-repeat="p in y.u"><a href ng-click="selectedItem[y.a] = p; y.func()">{{ p}}</a></li>
                                    </ul>
                                </div>
                                <input type='text' ng-model='selectedItem[y.a]' class='form-control input-sm' readonly >
                            </div>
                            
                            
                            
                        </div>

                        <div class="col-md-4">
                            <a href ng-click='savepromotion();' class="btn btn-success btn-sm" style='color:white;width:200px;'><span class='glyphicon glyphicon-save'></span> &nbsp;{{ buttonlabel}} </a><br /><br /><br /><br />
                        </div>
                    </div>
                    <div class="col-md-2"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>

<?php

printf("var Offers = [");
for ($i = 0, $sep = ""; $i < count($promotions_list); $i++, $sep = ", ") {
    printf("%s '%s'", $sep, $promotions_list[$i]);
}
printf("];"); ?>
    var token = <?php echo "'" . $_SESSION['user_backoffice']['token'] . "';"; ?>

    var restaurant = <?php echo "'" . $theRestaurant . "';"; ?>
    var email = <?php echo "'" . $email . "';"; ?>



app.controller('ControllerPromo', ['$scope', 'bookService', function ($scope, bookService) {
	var forceDate = new Date(new Date().getFullYear()-2, 0, 1);
        var todaydate = new Date();
        $scope.paginator = new Pagination(25);
        $scope.startIndex = 0;
        $scope.restaurant = restaurant;
        $scope.email = email;
        $scope.typeselection = 1;
        $scope.tmpArr = [];
        $scope.predicate = 'value';
        $scope.reverse = false;
        $scope.Offers = Offers.slice(0);
        $scope.PartnerPrograms = [' ','cpp_credit_suisse'];
        $scope.mydata_start = new bookService.ModalDataBooking();
        $scope.mydata_endng = new bookService.ModalDataBooking();
        $scope.startpromo = $scope.endpromo = "";
        $scope.mydata_start.setInit(todaydate, "09:00", function () { $scope.selectedItem.start = $scope.mydata_start.originaldate.getDateFormatReverse('-'); }, 3, todaydate, new Date(todaydate.getFullYear() + 1, 11, 31));
        $scope.mydata_endng.setInit(todaydate, "09:00", function () { $scope.selectedItem.end = $scope.mydata_endng.originaldate.getDateFormatReverse('-');}, 3, todaydate, new Date(todaydate.getFullYear() + 1, 11, 31));
        $scope.tabletitle = [{a: 'value', b: 'Order', c: '', q: 'down', cc: 'fuchsia'}, {a: 'name', b: 'Promo Name', c: '', q: 'down', cc: 'black'}, {a: 'offer', b: 'Promotion', c: '', q: 'down', cc: 'black'}, {alter: 'startv', a: 'start', b: 'StartDate', c: 'date', q: 'down', cc: 'black'}, {alter: 'endv', a: 'end', b: 'EndDate', c: 'date', q: 'down', cc: 'black'}, {a: 'cpp', b: 'Partner Program', c: '', q: 'down', cc: 'black'}];
        $scope.bckups = $scope.tabletitle.slice(0);
        $scope.tabletitleContent = [{a: 'value', b: 'Order', c: '', d: 'sort', t: 'input'}, {a: 'name', b: 'Promo Name', c: '', d: 'tag', t: 'input'}, {a: 'description', b: 'Description', c: '', d: 'list', t: 'textarea'}, {a: 'tnc', b: 'Terms and Conditions', c: '', d: 'list', t: 'textarea'}, {a: 'offer', b: 'Promotion', c: '', d: 'tower', t: 'array', u: $scope.Offers}, {a: 'start', b: 'Start date', c: 'date', d: 'tower', t: 'date', u: $scope.mydata_start }, {a: 'end', b: 'End date', c: 'date', d: 'tower', t: 'date', u: $scope.mydata_endng }, {a: 'cpp', b: 'Partner Program', c: '', d: 'tower', t: 'array', u: $scope.PartnerPrograms}];
        $scope.Objpromo = function () {
            var i, m = 0, isdefault, index;
            for (i = 0; i < $scope.promos.length; i++)
                if ($scope.promos[i].value > m)
                    m = $scope.promos[i].value;
            m += 1;
            isdefault = ($scope.promos.length == 0) ? 1 : 0;
            index = ($scope.promos.length > 0) ? $scope.promos[$scope.promos.length - 1].index + 1 : 1;
            return {
                restaurant: $scope.restaurant,
                ID: 0,
                index: index,
                name: '',
                value: m,
                offer: '',
                description: '',
                tnc: '',
                start: '',
                end: '',
                startv: '',
                endv: '',
                sddate: '',
                eddate: '',
                is_default: isdefault,
                cpp: '',
                remove: function () {
                    for (var i = 0; i < $scope.promos.length; i++)
                        if ($scope.promos[i].name === this.name) {
                            $scope.promos.splice(i, 1);
                            break;
                        }
                },
                replicate: function (obj) {
                    for (var attr in this) {
                        if (this.hasOwnProperty(attr) && typeof this[attr] !== 'function' && attr !== '$$hashKey' && typeof obj[attr] !== 'undefined' && typeof obj[attr] !== null)
                            this[attr] = obj[attr];
                    }
                    return this;
                },
                Retdata: function () {
                    var tmp = {};
                    for (var attr in this)
                        if (this.hasOwnProperty(attr) && typeof this[attr] !== 'function' && attr !== '$$hashKey') {
                            tmp[attr] = this[attr];
                        }
                    return tmp;
                },
                clean: function () {
                    if (this.start === '')
                        this.start = $scope.mydata_start.getDate('-', 'reverse');
                    if (this.end === '')
                        this.end = $scope.mydata_endng.getDate('-', 'reverse');
                    for (var attr in this)
                        if (this.hasOwnProperty(attr) && typeof this[attr] !== 'function' && attr !== '$$hashKey') {
                            if (typeof this[attr] === 'string') {
                                this[attr] = this[attr].replace(/\'|\"/g, '’');
                            }
                        }
                    return this;
                },
                check: function (action) {
                    if (this.name === '' || this.name.length < 6) {
                        alert('Invalid name, empty or too short(5)');
                        return -1;
                    }
                    if (this.description === '' || this.description.length < 3) {
                        alert('Invalid description, empty or too short(2)');
                        return -1;
                    }
                    if (this.offer === '' || this.offer.length < 5) {
                        alert('Invalid offer, empty ?');
                        return -1;
                    }
                    if (action == 'create')
                        for (var i = 0; i < $scope.promos.length; i++)
                            if ($scope.promos[i].name === this.name) {
                                alert("name is not unique");
                                return -1
                            }
                    return 1;
                }
            };
        };
        $scope.initorder = function () {
            $scope.tabletitle = $scope.bckups;
            $scope.predicate = "value";
            $scope.reverse = false;
        };
        $scope.reorder = function (item, alter) {
            alter = alter || "";
            if (alter !== "")
                item = alter;
            $scope.reverse = ($scope.predicate === item) ? !$scope.reverse : false;
            $scope.predicate = item;
        };
        bookService.readPromo($scope.restaurant, $scope.email).then(function (response) {
            var i, data;
            if (response.status !== 1 && response.status !== "1")
                return;
            $scope.promos = [];
            data = response.data.promo;
            for (i = 0; i < data.length; i++) {
                data[i].index = 1 + i;
                data[i].startv = data[i].start.jsdate().getTime();
                data[i].endv = data[i].end.jsdate().getTime();
                data[i].value = parseInt(data[i].value);
                data[i].sddate = data[i].start.jsdate().getDateFormat('-');
                data[i].eddate = data[i].end.jsdate().getDateFormat('-');
                $scope.promos.push(new $scope.Objpromo().replicate(data[i]));
            }
            $scope.initorder();
        });
        $scope.cleaninput = function (ll) {
            if (typeof $scope.selectedItem[ll] === 'string')
                $scope.selectedItem[ll] = $scope.selectedItem[ll].replace(/\'|\"/g, '’');
        };
        $scope.view = function (oo) {
            $scope.selectedItem = oo;
            $scope.reset('viewFlagPromo');
        };
        $scope.reset = function flag(field) {
            $scope.listingFlagPromo = false;
            $scope.createFlagPromo = false;
            $scope.viewFlagPromo = false;
            $scope[field] = true;
        };
        $scope.backlisting = function () {
            $scope.reset('listingFlagPromo');
        };
        $scope.create = function () {
            $scope.selectedItem = new $scope.Objpromo();
            $scope.mydata_start.setDate(todaydate);
            $scope.mydata_endng.setDate(todaydate);
            $scope.reset('createFlagPromo');
            $scope.buttonlabel = "Save promotion";
            $scope.action = "create";
            return false;
        };
        
        $scope.update = function (oo) {
            $scope.selectedItem = new $scope.Objpromo().replicate(oo);
			$scope.mydata_start.originaldate = new Date($scope.selectedItem.start);
			$scope.mydata_endng.originaldate = new Date($scope.selectedItem.end);
			console.log('UPDATE', $scope.mydata_endng, $scope.tabletitleContent);
            $scope.reset('createFlagPromo');
            $scope.buttonlabel = "Update promotion";
            $scope.action = "update";
            return false;
        };
        $scope.setdefault = function (oo) {

            bookService.setdefaultPromo($scope.restaurant, $scope.email, oo.name).then(function (response) {
                if (response.status > 0) {
                    for (var i = 0; i < $scope.promos.length; i++)
                        $scope.promos[i].is_default = '0';
                    oo.is_default = '1';
                }
                else
                    alert('promotion ' + oo.name + ' has NOT been set as default. ' + response.errors);
            });
        };
        $scope.setuppromo = function (oo) {
            var i, value = oo.value;
            if (value <= 1)
                return;
            bookService.setupPromo($scope.restaurant, $scope.email, oo.name, oo.value).then(function (response) {
                for (var i = 0; i < $scope.promos.length; i++)
                    if ($scope.promos[i].value === (value - 1)) {
                        $scope.promos[i].value = value;
                    }
                oo.value = value - 1;
                $scope.promos.sort(function (a, b) {
                    return a.value - b.value;
                });
            });
        }


        $scope.delete = function (oo) {
            if (confirm("Are you sure you want to delete " + oo.name) == false)
                return;
            bookService.deletePromo($scope.restaurant, $scope.email, oo.name).then(function (response) {
                if (response.status > 0) {
                    alert('promotion ' + oo.name + ' has been deleted');
                    oo.remove();
                }
                else
                    alert('promotion ' + oo.name + ' has NOT been deleted. ' + response.errors);
            });
        };
        $scope.savepromotion = function () {
            var apiurl, oo = $scope.selectedItem, msg = ($scope.action == "create") ? "updated" : "created";
 			
			oo = new $scope.Objpromo().replicate($scope.selectedItem);
            oo.clean();
            if (oo.check($scope.action) < 0) {
                return;
            }
            oo.startv = oo.start.jsdate().getTime();
            oo.endv = oo.end.jsdate().getTime();
            oo.sddate = oo.start.jsdate().getDateFormat('-');
            oo.eddate = oo.end.jsdate().getDateFormat('-');
            if ($scope.action == "update") {
                bookService.updatePromo($scope.restaurant, $scope.email, oo).then(function (response) {
                    if (response.status > 0) {
                        alert('promotion ' + oo.name + ' has been updated');
                        oo.remove();
                        $scope.promos.push(oo);
                    }
                    else
                        alert('promotion ' + oo.name + ' has NOT been updated. ' + response.errors);
                });
            }
            else if ($scope.action == "create") {
                bookService.createPromo($scope.restaurant, $scope.email, oo).then(function (response) {
                    if (response.status > 0) {
                        $scope.promos.push(oo);
                        alert('promotion ' + oo.name + ' has been created');
                    }
                    else
                        alert('promotion ' + oo.name + ' has NOT been created. ' + response.errors);
                });
            }

            $scope.backlisting();
        };
    }]);
    
</script>


