
<?php
$baseUrl = __BASE_URL__;
$res = new WY_restaurant;
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<div class="container">
    <div class="row">
        <div class="col-md-12 left-sec">
            <div ng-controller="PABXController"  ng-init="moduleName='payment'; listMemberFlag = true;createView = false" >
                <div class="col-md-2"></div>
                    <div id='listing' ng-show='listMemberFlag'>
                        <div class="form-group"  style='margin-bottom:25px;'>
                                <div class="col-md-4">
                                        <div class="input-group col-md-4">
                                                <span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>
                                                <input type="text" class="form-control input-sm" ng-model="searchText" ng-change="paginator.setPage(0);" style='width:200px;'> 
                                        </div>
                                </div>
                                <div class="col-md-2"></div>
                                <div class="col-md-4">
                                <a href ng-click='create();' class="btn btn-success btn-sm" style='color:white;width:200px;'><span class='glyphicon glyphicon-certificate'></span> &nbsp;Create Member</a>
                                </div>
                        </div>
                        <div style=" clear: both;"></div>
                        <table width='100%' class="table table-condensed table-striped" style='font-size:12px;'>
                            <thead>
							<tr>
							<th ng-repeat="y in tabletitle"><tbtitle var="{{y.a}}" name='tabletitle' module='moduleName'/></th>
			                                <th>update</th><th> &nbsp; </th>
			                                <th>delete</th><th> &nbsp; </th>
							</tr>
							</thead>
			                	<tbody style='font-family:helvetica;font-size:12px;'>
			                    	<tr ng-repeat="x in filteredEvent = (names | filter:searchText) | sizefilter:this | orderBy:predicate:reverse | slicefilter:paginator.getPage():paginator.getRowperPage():paginator.getItemCount() | limitTo:paginator.getRowperPage()" style='font-family:helvetica;font-size:11px;'> 
			                        	<td ng-repeat="y in tabletitle"><a href ng-click="view(x)" ng-style="{ 'text-align' : getAlignment(x[y.a]) }"> {{ x[y.a] | adatereverse:y.c | notzero }}</a></td>
			                            <td><a href ng-click="update(x)" style='color:blue;'><span class='glyphicon glyphicon-pencil'></span></a></td>
			                            <td width='30'>&nbsp;</td>
			                            <td><a href ng-click="deletemember(x)" style='color:red;'><span class='glyphicon glyphicon-trash'></span></a></td>
			                        </tr>
			                        <tr><td colspan='{{tabletitle.length + 5}}'></td></tr>
								</tbody>
						</table>
<!--                        <div ng-if="filteredEvent.length >= paginator.getRowperPage()" class="col-md-12" align="center" style='font-size:11px' ng-include="'inc/paginator.html'"></div>-->
                    </div>
                    <div class="col-md-12" ng-show='viewMemberFlag'>
                        <br /><a href class='btn btn-info btn-sm customColor' ng-click="backlisting()"><span class='glyphicon glyphicon-step-backward'></span> &nbsp;Back to listing</a><br />
                        <table class='table-striped' style="margin: 0 0 150px 30px;font-size:13px;font-family: Roboto">	

                                <tr ng-repeat="y in tabletitleContent | filter: {b: '!picture'}"><td nowrap ><strong> {{ y.b }} </strong></td><td> &nbsp; </td><td>{{ selectedItem[y.a] | adatereverse:y.c }} </td></tr>

                        </table>
                    </div>
                <div iid='create' ng-show='createView' >
           
                    <div id='create' class="col-md-12" >
                        <br />
                        <a href class='btn btn-info btn-sm customColor' ng-click="backlisting()"><span class='glyphicon glyphicon-step-backward'></span> &nbsp;Back to listing</a>
                        <br />
                        
                         <div class="col-md-2"> </div>
              
                        <div class="col-md-6">
                             <h4 style='text-align:center;'>Member Details</h4>
                            <div class="row" ng-repeat="y in tabletitleContent| filter: {a: '!value' }" style="margin: 0 0 20px 0;font-size:12px;font-familly:Roboto">
                                <div class="input-group" ng-if="y.t === 'input'">
                                    <span class="input-group-addon input11"><i class="glyphicon glyphicon-{{y.d}} input14"></i>&nbsp; {{y.b}}</span>
                                    <input type="text" class="form-control input-sm" ng-model="selectedItem[y.a]" ng-change="cleaninput(y.a)" ng-readonly="y.a === 'name' && action === 'update'" >
                                </div>
<!--                                <div class="input-group" ng-if="y.t === 'array'">
                                    <div class='input-group-btn' uib-dropdown >
                                    <button type='button' class='btn btn-default btn-sm input11' uib-dropdown-toggle >
                                        <i class="glyphicon glyphicon-{{y.d}} input13"></i>&nbsp; {{y.b}}<span class='caret'></span>
                                    </button>
                                    <ul class='dropdown-menu scrollable-menu' uib-dropdown-menu style='height: auto;max-height:200px; overflow-x: hidden;' >
                                    <li ng-repeat="p in y.val"><a href ng-click="selectedItem[y.a]=p;y.func()">{{ p }}</a></li>
                                    </ul>
                                    </div>
                                    <input type='text' ng-model='selectedItem[y.a]' class='form-control input-sm' readonly >
                                </div>-->
  
                            </div>
                        </div>
                        
                    </div>
                <div class="col-md-6"></div>
<!--		<div class="col-md-7"></div>-->
		<div class="col-md-6">
                       <a href ng-click='savenewmember();' class="btn btn-success btn-sm" style="color:white;width:150px"><i class='glyphicon glyphicon-save'></i> &nbsp;save member </a><br />
		</div>
            </div>
            </div>

        </div>
    </div>
</div>
<script>
 
    var token = <?php echo "'".$_SESSION['user_backoffice']['token']."';"; ?>
    var email =  <?php echo "'".$_SESSION['user_backoffice']['email']."';";?>

app.controller('PABXController', ['$scope','$http','$timeout','bookService', function($scope,$http,$timeout,bookService) {
	var todaydate = new Date();
	var aorder = (function() { var i, arr=[]; for(i = 0; i < 30; i++) arr.push(i); return arr; })();
	
	$scope.paginator = new Pagination(25);
	$scope.restaurant = <?php echo "'".$theRestaurant."';"; ?>
	$scope.backofficeemail = <?php echo "'" . $email . "';"; ?>
	$scope.predicate = '';
	$scope.reverse = false;
	$scope.nonefunc = function() {};

    var statusarr =['active','inactive'];

	$scope.tabletitle = [ {a:'index', b:'ID', c:'' , l:'', q:'down', cc: 'fuchsia' },
	{a:'email', b:'Email', c:'' , l:'', q:'down', cc: 'fuchsia' }, 
	{a:'firstname', b:'Firstname', c:'' , l:'25', q:'down', cc: 'black' }, 
	{a:'lastname', b:'Lastname', c:'' , l:'', q:'down', cc: 'black' }, 
	{a:'status', b:'Status', c:'' , l:'', q:'down', cc: 'black' }];
           $scope.tabletitleContent = [
            
            {a: 'email', b: 'Email', c: '', d: '', t: 'input'},
            {a: 'firstname', b: 'Firstname', c: '', d: '', t: 'input'},
            {a: 'lastname', b: 'Lastname', c: '', d: '', t: 'input'},
            {a: 'mobile', b: 'Mobile', c: '', d: '', t: 'input'},
            {a:'status', b:'Status', c:'', d:'sort', t:'array',val:statusarr },
          
            //{a: 'notify_sms', b: 'Notify Sms', c: '', d: '', t: 'checkbox'}
        ];

        

       
   $scope.ObjectMember = function() {
		return {
		id: '0',
		email: '',
		firstname: '',
		lastname: '',
                status :''
			};
		};
       		
	$scope.initorder = function() {
		$scope.tabletitle = $scope.bckups;
		$scope.predicate = "vorder";
		$scope.reverse = true;
	};

	$scope.reorder = function(item, alter) {
		alter = alter || "";
		if (alter !== "")  item = alter;
		$scope.reverse = ($scope.predicate === item) ? !$scope.reverse : false;
		$scope.predicate = item;
	};
    
 
	$scope.cleaninput = function(ll) {
		if(typeof $scope.selectedItem[ll] === 'string')
			$scope.selectedItem[ll] =  $scope.selectedItem[ll].replace(/\'|\"/g, '’');
		};
		
	$scope.reset = function(item) {
		$scope.listMemberFlag = false;
		$scope.viewMemberFlag = false
		$scope.createView = false;
		$scope[item] = true;	
		};

	$scope.backlisting = function() {
		$scope.reset('listMemberFlag');
		};

   		
	$scope.view = function(oo) {
		$scope.selectedItem = oo;
		$scope.reset('viewMemberFlag');
		};
	
	$scope.create = function() {
		$scope.selectedItem = new $scope.ObjectMember();
		$scope.reset('createView');
		$scope.buttonlabel = "Save Member";
		$scope.action = "create";
		return false;
		}

	$scope.update = function(oo) {
		$scope.selectedItem = oo;
		$scope.reset('createView');
		$scope.buttonlabel = "Update member";
		$scope.action = "update";
		};
                
         bookService.getCallcentermemberList($scope.restaurant).then(function(response) {
                console.log("RESPONSE" +JSON.stringify(response));
                	$scope.names = [];
                if(response.status === 1){
                var data = response.data;
                for (var i = 0; i < data.length; i++) {
                        data[i].index = i + 1;
                        $scope.names.push(data[i]); 
                } 
                
                }
            });        
          

	$scope.savenewmember = function(action) {
            var u, msg, oo = $scope.selectedItem, valAr = [];
            oo.restaurant = $scope.restaurant;
            oo.action = $scope.action;
            oo.status = 'active';
            
    
            if(typeof oo.email !== "string" || oo.email.length < 1 || oo.email.trim().length < 1)
            return;
            oo.email = oo.email.trim();
            bookService.saveccMember($scope.backofficeemail,$scope.restaurant,oo).then(function(response) {
                console.log("RESPONSE" +JSON.stringify(response));
                if (response.status === 1) {
                    if($scope.action === 'create'){
                        $scope.names.push($scope.selectedItem);
                    }
            $scope.backlisting();
                }
             
             
            });
               
      	
	};
        $scope.removeMemberData = function(email) {
         	var i, data = $scope.names;
         	if(typeof email !== "string" || email.length < 3)
         		return -1;
         		
		for(i = 0; i < data.length; i++)
                    if(data[i].email === email)
                           return data.splice(i, 1);
        };
        $scope.deletemember = function(oo) {
            $scope.selectedItem = oo;
            oo.restaurant = $scope.restaurant;
            bookService.deleteccmember($scope.backofficeemail,oo.email,$scope.restaurant).then(function(response) {

                if(response.status === 1) {
                        $scope.removeMemberData(oo.email);
                        alert(oo.email + " member  has been deleted");
                    }
                else alert(oo.email + " member has NOT been deleted: " + reponse.errors);
            });
 
      	
	};
			
}]);

</script>