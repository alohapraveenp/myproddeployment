<?php

$baseUrl = __BASE_URL__;
$res = new WY_restaurant;


$currency = $res->getCurrency($theRestaurant);
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>

<div class="container">
    <div class="row">
        <div class="col-md-12 left-sec">
            <div ng-controller="RequestformController"  ng-init="moduleName='requestform';" >
                <div class="col-md-2"></div>
                
                    <div id='listing' >
                        <div class="form-group"  style='margin-bottom:25px;'>
                                <div class="col-md-4">
                                        <div class="input-group col-md-4">
                                                <span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>
                                                <input type="text" class="form-control input-sm" ng-model="searchText" ng-change="paginator.setPage(0);" style='width:200px;'> 
                                        </div>
                                </div>
                                <div class="col-md-2"></div>
                               
                        </div>
                        <div style=" clear: both;"></div>
                        <table width='100%' class="table table-condensed table-striped" style='font-size:12px;'>
                            <thead>
				<tr>
				<th ng-repeat="y in tabletitle"><tbtitle var="{{y.a}}" name='tabletitle' module='moduleName'/></th>
                                  <th>Resend Form</th><th> &nbsp; </th>
				</tr>
                            </thead>
                            <tbody style='font-family:helvetica;font-size:12px;'>
                                    <tr ng-repeat="x in filteredEvent = (names | filter:searchText) | sizefilter:this | orderBy:predicate:reverse | slicefilter:paginator.getPage():paginator.getRowperPage():paginator.getItemCount() | limitTo:paginator.getRowperPage()" style='font-family:helvetica;font-size:11px;'> 
                                            <td ng-repeat="y in tabletitle"><a href ng-click="view(x)" ng-style="{ 'text-align' : getAlignment(x[y.a]) }"> {{ x[y.a] | adatereverse:y.c | notzero }}</a></td>
                                            <td><a href ng-click="resendform(x)" style='color:green;'><span class='glyphicon glyphicon-envelope'></span></a></td>
                                    </tr>
                                    <tr><td colspan='{{tabletitle.length + 5}}'></td></tr>
                            </tbody>
                                
                        </table>
<!--                        <div ng-if="filteredEvent.length >= paginator.getRowperPage()" class="col-md-12" align="center" style='font-size:11px' ng-include="'inc/paginator.html'"></div>-->
                    </div>

               
            </div>

        </div>
    </div>
</div>
<script>
 
    app.controller('RequestformController', ['$scope', '$http', 'bookService', function($scope, $http, bookService) {
        $scope.restaurant = <?php echo "'" . $theRestaurant . "';"; ?>
        $scope.email =  <?php echo "'".$_SESSION['user_backoffice']['email']."';";?>
        $scope.baseUrl = <?php echo "'".$baseUrl."';";?>
        $scope.names = [];
        $scope.currency = <?php echo "'" . $currency . "';"; ?>
        var aorder = (function() { var i, arr=[]; for(i = 0; i < 30; i++) arr.push(i); return arr; })();
	
	$scope.paginator = new Pagination(25);
  

        $scope.initorder = function() {
            $scope.predicate = "index";
            $scope.reverse = true;
	};
        
        $scope.reorder = function(item, alter) {
		alter = alter || "";
		if (alter !== "")  item = alter;
		$scope.reverse = ($scope.predicate === item) ? !$scope.reverse : false;
		$scope.predicate = item;
	};

        $scope.tabletitle = [ {a:'index', b:'ID', c:'' , l:'', q:'down', cc: 'fuchsia' },{a:'email', b:'Email', c:'' , l:'', q:'down', cc: 'fuchsia' }, {a:'date', b:'Date', c:'' , l:'25', q:'down', cc: 'black' }];

        bookService.getRequestFormList($scope.restaurant).then(function(response) {
           
           
            if(response.status === 1){
                var result = response.data.details;
                for (var i = 0; i < result.length; i++) {
                        result[i].index = i + 1;
                        $scope.names.push(result[i]); 
                } 
                
            }
        });
        $scope.resendform = function (x) { 
            bookService.sendForm($scope.restaurant, x.email, $scope.email).then(function (response) {
               alert("Request form has been sent to"+ x.email);
            });
            
        };
        
        $scope.cleanamount = function (obj) {  

            obj = obj.replace(/[^0-9\.]/g, '');

            return obj;
        }

    }]);
</script>