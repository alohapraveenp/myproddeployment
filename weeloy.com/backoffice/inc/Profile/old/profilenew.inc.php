<div ng-include="'inc/myModalModif.html'"></div>
<div ng-controller="ProfileControllerNew" ng-init="listProfFlag = true; viewProfFlag=false; editFormatFlag=false; exportFlag = false" >
	<input type='hidden' id='token' value ="<?php echo $_SESSION['user_backoffice']['token'] ?>" />
	<input type='hidden' id='restaurant' value ="<?php echo $theRestaurant ?>" />
	<input type='hidden' id='email' value ="<?php echo $_SESSION['user_backoffice']['email'] ?>" />
	<input type='hidden' id='imglogo' value ="<?php echo $logo ?>"/>
	<input type='hidden' id='editprofiles' value ="<?php echo $editperms ?>"/>
	
	<div id='listing' ng-show='listProfFlag'>
		<div class="form-group">
			<div class='row'>
				<div class="col-md-2">
					<div class="form-group" style='width:150px;'>
						<div class="input-group">
							<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>
							<input type="text" class="form-control input-sm" ng-model="searchText" ng-change="paginator.setPage(0);" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" > 
						</div>
					</div>
				</div>
				<div class="col-md-10">
					<ul class="nav navbar-nav">
						<li>
							<div class="input-group" style='margin-left:5px'>
							<input type="hidden" value="testing" ng-model='contentextractprof' name="contentextractprof" id="contentextractprof">
								<a href ng-click='export1([], 1);' class="btn btn-success btn-sm" style='color:white;font-size:11px;'><span class='glyphicon glyphicon-save'></span> &nbsp;Extract</a>
							</div>
						</li>
						<li>
							<div class="input-group" style='margin-left:5px'>
							<input type="hidden" value="testing" ng-model='contentextractprof' name="contentextractprof" id="contentextractprof">
							<a href ng-click="reset('exportFlag')" class="btn btn-success btn-sm" style='color:white;font-size:11px;'><span class='glyphicon glyphicon-save'></span> &nbsp;Full Extract</a>
							</div>
						</li>
						<li>
							<div class="input-group" style='margin-left:5px'>
							<a href ng-click='Edit();' class="btn btn-info btn-sm" style='color:white;'><span class='glyphicon glyphicon-th'></span> &nbsp;Edit Profil Format</a>
							</div>
						</li>
						<li>
							<div class="input-group" style='margin-left:5px'>
							<a href ng-click='editprofcode();' class="btn btn-primary btn-sm" style='color:white;'><span class='glyphicon glyphicon-pencil'></span> &nbsp;Edit Profile Code</a>
							</div>
						</li>
						<li>
							<div class="input-group" style='margin-left:5px'>
							<a href ng-click='editbkgcode();' class="btn btn-default btn-sm"><span class='glyphicon glyphicon-pencil'></span> &nbsp;Edit Booking Code</a>
							</div>
						</li>
						<li>
							<div class="input-group" style='margin-left:5px'>
							<button  type="button" class="btn btn-default btn-sm" ng-click="printing()"><i class="glyphicon glyphicon-print"></i></button>
							</div>
						</li>
						<li>
							<div class="btn-group" uib-dropdown>
								<button type="button" class="btn btn-default btn-xs" uib-dropdown-toggle aria-expanded="false" style='font-size:11px;margin-left:15px;'>Page Size<span class="caret"></span></button>
								<ul class="dropdown-menu" uib-dropdown-menu role="menu">
									<li ng-repeat="x in paginator.pagerange()" class='glyphiconsize'><a href ng-class="{ 'glyphicon glyphicon-ok glyphiconsize': x === paginator.getRowperPage() }" ng-click="paginator.setRowperPage(x)"> {{x}}</a></li>
								</ul>
							</div> 		
						</li>
					</ul>
				</div>
			</div>
			<div class='row' style="font-size:x-small;margin-bottom:20px">
				<div class="col-md-12">
					<span>number of profile: {{ filteredProf.length }}</span>
				</div>
			</div>
	        <div style=" clear: both;"></div>
			<table width='100%' class="table table-condensed table-striped" style='font-size:13px;'>
				<thead>
					<tr>
					<th ng-repeat="y in tabletitle">
					<tbtitle var="{{y.a}}" name='tabletitle'/>
					</th>
					<th ng-if="editprofiles === 1">edit</th>
					</tr>
				</thead>
				<tbody style='font-family:helvetica;font-size:13px;'>
					<tr ng-repeat="x in filteredProf = (names | filter:searchText) | sizefilter:this | orderBy:predicate:reverse | slicefilter:paginator.getPage():paginator.getRowperPage():paginator.getItemCount() | limitTo:paginator.getRowperPage()" style='font-family:helvetica;font-size:12px;'>
						<td ng-repeat="z in tabletitle">
							<tb-listingbkg><a href ng-click="view(x)">{{ x[z.a] | adatereverse:z.c }}</a></tb-listingbkg>
						</td>
						<td ng-if="editprofiles === 1" align='center'><a href ng-click="updateprofile(x)"><span class='glyphicon glyphicon-pencil blue'></span></a></td>
					</tr><tr><td colspan='{{tabletitlelength}}'></td></tr>
				</tbody>
			</table>
			<div ng-if="filteredProf.length >= paginator.getRowperPage()" class="col-md-12" align="center" style='font-size:11px' ng-include="'inc/paginator.html'"></div>
		</div>
	</div>
	<div class="col-md-12" ng-show='editProfCodeFlag'>
		<br />
		<a href class='btn btn-info btn-sm customColor' ng-click="backlisting()"><span class='glyphicon glyphicon-step-backward'></span> &nbsp;Back to listing</a>
		<br />
		 <div class="col-md-2"></div>
		 <div class="col-md-8">
		   <h4 style='text-align:center;'>Edit Profile Code</h4><br />
		   <table  class='table' style="font-size:12px;font-familly:Roboto">
		   <tr ng-repeat="oo in codeprofdata">
		   <td><input type="text" class="form-control inputsmall" ng-model="oo.label"></td>
		   <td><input type="text" class="form-control inputsmall" ng-model="oo.description"></td>
		   </tr>
		   <tr><td colspan='2'>	<button  type="button" class="btn btn-default btn-xs" ng-click="addcode(codeprofdata, 'profile');"><i class="glyphicon glyphicon-plus"></i>add a new code</button></td></tr>
		   </table>
				<a href ng-click='savecodeprof();' class="btn btn-success btn-sm" style='color:white;'><span class='glyphicon glyphicon-save'></span> &nbsp;Save code</a>
		</div>		
	</div>	
	<div class="col-md-12" ng-show='editBkgCodeFlag'>
		<br />
		<a href class='btn btn-info btn-sm customColor' ng-click="backlisting()"><span class='glyphicon glyphicon-step-backward'></span> &nbsp;Back to listing</a>
		<br />
		 <div class="col-md-2"></div>

		 <div class="col-md-8">
		   <h4 style='text-align:center;'>Edit Booking Code</h4><br />
		   <table  class='table' style="font-size:12px;font-familly:Roboto">
		   <tr ng-repeat="oo in codebkgdata">
		   <td><input type="text" class="form-control inputsmall" ng-model="oo.label"></td>
		   <td><input type="text" class="form-control inputsmall" ng-model="oo.description"></td>
		   <td><select ng-model="oo.value" ng-options="val for val in colornames" ng-change="oo.color = oo.value"></select></td>
		   </tr>
		   <tr><td colspan='3'>	<button  type="button" class="btn btn-default btn-xs" ng-click="addcode(codebkgdata, 'booking');"><i class="glyphicon glyphicon-plus"></i>add a new code</button></td></tr>
		   </table>
				<a href ng-click='savecodebkg();' class="btn btn-success btn-sm" style='color:white;'><span class='glyphicon glyphicon-save'></span> &nbsp;Save code</a>
		</div>		
	</div>
	<div class="col-md-12" ng-show='editFormatFlag'>
		<br />
		<a href class='btn btn-info btn-sm customColor' ng-click="backlisting()"><span class='glyphicon glyphicon-step-backward'></span> &nbsp;Back to listing</a>
		<br />
		 <div class="col-md-2"></div>

		 <div class="col-md-8">
		   <h4 style='text-align:center;'>Edit Format</h4><br />
		   <table  class='table' style="font-size:12px;font-familly:Roboto">
		   <tr ng-repeat="oo in formatobj | filter: {value: '!codekey0000' }">
		   <td><input type="text" class="form-control inputsmall" ng-model="oo.value"></td>
		   </tr>
		   <tr><td>	<button  type="button" class="btn btn-default btn-xs" ng-click="addfield();"><i class="glyphicon glyphicon-plus"></i>add a new field</button></td></tr>
		   </table>
				<a href ng-click='saveformat();' class="btn btn-success btn-sm" style='color:white;'><span class='glyphicon glyphicon-save'></span> &nbsp;Save format</a>
		</div>		
	</div>
	<div class="col-md-12" ng-show='editProfFlag'>
		<br />
		<a href class='btn btn-info btn-sm customColor' ng-click="backlisting()"><span class='glyphicon glyphicon-step-backward'></span> &nbsp;Back to listing</a>
		<br />
		 <div class="col-md-2"></div>
		 <div class="col-md-8">
		   <h4 style='text-align:center;'>Edit Profile</h4><br />
		   <table  class='table' style="font-size:12px;font-familly:Roboto">
		   <tr ng-repeat="y in aprofileInfo | filter: myFilter | filter: {label:'!hide'} | filter: {label:'!Picture'}">
		   	<td nowrap>{{y.label}} :</td><td ng-if="y.type==='static'">{{ y.value }}</td><td ng-if="y.type==='input'"><input type="text" class="form-control inputsmall" ng-model="y.value"></td>
		   	<!-- <td ng-if="y.type==='newdate'"> <md-content><md-datepicker ng-model="y.d" md-placeholder="Enter date"></md-datepicker></md-content></td> -->
		   	<td ng-if="y.type==='date'"><div class="form-control-wrapper"><input type="text" ng-model="y.value" id="materialdate" class="form-control inputsmall" placeholder="Date"></div></td>
		   </tr>
		   <tr ng-if="codeprofile.length > 0"><td>CODE: </td><td><label ng-repeat="oo in codeprofile" style="font-size:12px;margin-left:10px;"><input type="checkbox" ng-model="oo.value"/> {{oo.label}} </label></td></tr>
		   <tr><td colspan='2' nowrap> 
		   </td></tr>
		   </table>
		</div>
		<div class="col-md-2"></div>
		<div class="col-md-4"></div>	
		<div class="col-md-4">
			<a href ng-click='view(current);' class="btn btn-default btn-sm" style='width:150px;'> &nbsp;Cancel</a>
		</div>
		<div class="col-md-4">
		   	<a href ng-click='savesubProfile();' class="btn btn-success btn-sm" style='color:white;width:150px;'><span class='glyphicon glyphicon-save'></span> &nbsp;Save Profile</a>
		</div>
	</div>

	<div class="col-md-12" ng-show='viewProfFlag'>
		<br />
		<a href class='btn btn-info btn-sm customColor' ng-click="backlisting()"><span class='glyphicon glyphicon-step-backward'></span> &nbsp;Back to listing</a>
		<br />
		<br />
		<div class="col-sm-4">
		<a href ng-click='editprof(current);' class="btn btn-info btn-sm" style='color:white;font-size:11px;width:220px;'><span class='glyphicon glyphicon-pencil'></span> &nbsp; {{ showeditproftitle }}</a>
		</div>
		<div class="col-sm-4">
		<a href ng-click='showbkhistory(current);' class="btn btn-primary btn-sm" style='font-size:11px;width:220px;color:white;'><span class='glyphicon glyphicon-th'></span> &nbsp;Show Booking History</a>
		</div>
		<div class="col-sm-4">
		<a ng-if='showprofiletest' href ng-click='showorderfood();' class="btn btn-default btn-sm" style='font-size:11px;width:220px;'><span class='glyphicon glyphicon-eye-open'></span> &nbsp;Show Order History</a>
		</div>
        <div class="col-md-12"><br /></div>
		 <div class="col-md-1"></div>
		 <div class="col-md-10">
		   <table class='table' style="font-size:12px;font-familly:Roboto" ng-if="showinfotype === 'profil'">
		   <tr ng-repeat="y in aprofileInfo | filter: {label:'!hide'}">
				<td ng-if="y.type==='picture' && y.value !==''" style="float: right; position:relative; display: block;" colspan='2'><img ng-src="{{y.value}}" class="img-responsive img-circle"/></td>
				<td ng-if="y.type!=='picture'">{{y.label | apostrophe }} :</td><td ng-if="y.type!=='picture'">{{ y.value | adatereverse:y.c}}</td>
		   </tr>
		   </table>
		   <span style="font-size:x-small" ng-if="showinfotype === 'booking'">number of booking: {{ pfbookings.length }}</span>
		   <table class='table' style="font-size:12px;font-familly:Roboto" ng-if="showinfotype === 'booking'">
		   <thead><tr><th ng-repeat="y in bookingtitle"><tbtitle var="{{y.b}}" name='bookingtitle'  module='moduleName'/></th><th>Orders</th></tr></thead>
		   <tr ng-repeat="x in filteredBkg = pfbookings | sizefilter:this | orderBy:predicate:reverse | slicefilter:paginatorsub.getPage():paginatorsub.getRowperPage():paginatorsub.getItemCount() | limitTo:paginatorsub.getRowperPage()" style='font-family:helvetica;font-size:11px;' > 
		   		<td ng-repeat="z in bookingtitle">{{ x[z.a] | adatereverse:z.c}}</td> 
                <td><a href popover="{{x.order}}" data-popover-trigger="mouseenter" ><span ng-if="x.order !== '' "><i class ='glyphicon glyphicon-asterisk'></i></span></a></td>
		   </tr>
		   <tr><td colspan='{{tabletitle.length+4}}'><div ng-if="pfbookings.length >= paginatorsub.getRowperPage()" class="col-md-12" align="center" style='font-size:11px' ng-include="'inc/paginatorsub.html'"></div></td></tr>
		   </table>
		   <span style="font-size:x-small" ng-if="showinfotype === 'orderfood'">number of items: {{ fooditems.length }}</span>
		   <table class='table' style="font-size:12px;font-familly:Roboto" ng-if="showinfotype === 'orderfood'">
		   <tr ng-repeat="w in fooditems"> <td>{{w}}</td> </tr>
		   </table>				
		 </div>
		 <div class="col-md-1"></div>
	</div>

	<div class="col-md-12" ng-show='updateProfFlag'>
		<br />
		<a href class='btn btn-info btn-sm customColor' ng-click="backlisting()"><span class='glyphicon glyphicon-step-backward'></span> &nbsp;Back to listing</a>
		<br />
		<br />
        <div class="col-md-12"><br /></div>
		 <div class="col-md-1"></div>
		 <div class="col-md-10">
		   <table class='table' style="font-size:12px;font-familly:Roboto" ng-if="showinfotype === 'profil'">
				<tr ng-if="selectedItem.picture !==''">
					<td style="float: right; position:relative; display: block;" colspan='2'><img ng-src="{{selectedItem[y.a]}}" class="img-responsive img-circle"/></td>
				</tr>
				<tr ng-repeat="y in masterprofileInfo">
					<td>{{ y.b | apostrophe }} :</td><td>{{ selectedItem[y.a] | adatereverse:y.c}}</td>
				</tr>
		   </table>
		 </div>
		 <div class="col-md-1"></div>
	</div>

	<div ng-show='exportFlag'><h3>Profile Export Date Range</h3>
		<br />
			<a href class='btn btn-info btn-sm customColor' ng-click="backlisting()"><span class='glyphicon glyphicon-step-backward'></span> &nbsp;Back to listing</a>
		<br />
	  	<table width='100%'>
	  		<tr>
				<td width='75'>
					<div class="input-group">
						<span>
							<button  type="button" class="btn btn-default" ng-click="mydatastart.dateopen($event)"><i class="glyphicon glyphicon-calendar"><span style='font-size:9px;'><br />Start</span></i></button>
						</span>
					    <input type="text" class="form-control" uib-datepicker-popup="{{mydatastart.formats[0]}}"  ng-model="mydatastart.originaldate"  ng-change='mydatastart.onchange();' is-open="mydatastart.opened" min-date="mydatastart.minDate" max-date="mydatastart.maxDate" datepicker-options="mydatastart.dateOptions" ng-required="true" close-text="Close" style="width:0;opacity:0" />
					</div>
				</td>
				<td width='75'>
					<div class="input-group">
						<span><button  type="button" class="btn btn-default" ng-click="mydataend.dateopen($event)"><i class="glyphicon glyphicon-calendar"><span style='font-size:9px;'><br />End</span></i></button></span>
					    <input type="text" class="form-control" uib-datepicker-popup="{{mydataend.formats[0]}}"  ng-model="mydataend.originaldate"  ng-change='mydataend.onchange();' is-open="mydataend.opened" min-date="mydataend.minDate" max-date="mydataend.maxDate" datepicker-options="mydataend.dateOptions" ng-required="true" close-text="Close" style="width:0;opacity:0" />
					</div>
				</td>
				<td width='30'>&nbsp;</td>
			</tr>
		</table>
		<br/>
		<div class="row" ng-if="startProfile !== -1" style="font-size:11px;margin-top:20px;">
		start: {{ mydatastart.originaldate.getDateFormat() }} &nbsp;&nbsp;&nbsp; end: {{ mydataend.originaldate.getDateFormat() }} 
		</div>
		<br />
			<div align="center">
				<a href class='btn btn-info btn-sm' ng-click="export(mydatastart.originaldate, mydataend.originaldate);" style='width:200px;color:white;'>Save&nbsp;<span class='glyphicon glyphicon-save'></span>
				</a>
			</div>
		<br />
		<br />
	</div>
</div>
<script src="inc/Profile/ProfileControllerNew.js?1"></script>