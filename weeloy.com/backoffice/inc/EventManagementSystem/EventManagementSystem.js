app.controller('EventController', ['$scope', 'bookService', '$http', function($scope, bookService, $http) {
	var forceDate = new Date(new Date().getFullYear()-2, 0, 1);
	var todaydate = new Date();
	var aorder = (function() { var i, arr=[]; for(i = 0; i < 30; i++) arr.push(i); return arr; })();
	var paxnumbers = (function() { var i, arr=[]; for(i = 0; i <= 300; i++) arr.push(i); return arr; })();
	$scope.paginator = new Pagination(25);
	$scope.paginatorsub = new Pagination(25); 
	$scope.evbooking = [];
	$scope.names = [];
	$scope.atype = ['public', 'private'];
	$scope.amenus = [];
	$scope.documentdpf = ['cadeau'];
	$scope.path = pathimg;
	$scope.restaurant = $("#restaurant").val();
	$scope.email =  $("#email").val();
    $scope.showprice = $("#price").val();
	$scope.predicate = '';
	$scope.reverse = false;
	$scope.nonefunc = function() {};
	$scope.mydata_start = new bookService.ModalDataBooking();
	$scope.mydata_endng = new bookService.ModalDataBooking();
    $scope.mydata_display = new bookService.ModalDataBooking();
	$scope.mydata_start.setInit(todaydate, "09:00", function() { $scope.selectedItem.start = $scope.mydata_start.originaldate.getDateFormatReverse('-'); }, 3, todaydate, new Date(todaydate.getFullYear()+1, 11, 31));
	$scope.mydata_endng.setInit(todaydate, "09:00", function() { $scope.selectedItem.end = $scope.mydata_endng.originaldate.getDateFormatReverse('-'); }, 3, todaydate, new Date(todaydate.getFullYear()+1, 11, 31));
    $scope.mydata_display.setInit(todaydate, "09:00", function() { $scope.selectedItem.display = $scope.mydata_display.originaldate.getDateFormatReverse('-'); }, 3, todaydate, new Date(todaydate.getFullYear()+1, 11, 31));
	$scope.tableBktitleContent = [
			{ a: 'eventID', b: 'eventID' }, 
			{ a: 'orderID', b: 'orderID' }, 
			{ a: 'pax', b: 'Pax' }, 
			{ a: 'amount', b: 'Amount' }, 
			{ a: 'currency', b: 'Currency' }, 
			{ a: 'firstname', b: 'Firstname' }, 
			{ a: 'lastname', b: 'Lastname' }, 
			{ a: 'email', b: 'Email' }, 
			{ a: 'phone', b: 'Phone' }, 
			{ a: 'status', b: 'Status' }, 
			{ a: 'cdate', b: 'Create Date' }];
	$scope.tabletitlebook = [ 
			{a:'orderID', b:'orderID', c:'' , l:'', q:'down', cc: 'black' }, 
			{a:'pax', b:'Pax', c:'' , l:'', q:'down', cc: 'black' }, 
			{a:'amount', b:'Amount', c:'' , l:'', q:'down', cc: 'black' }, 
			{a:'fullname', b:'Name', c:'' , l:'', q:'down', cc: 'black' }, 
			{a:'email', b:'Email', c:'' , l:'', q:'down', cc: 'black' }, 
			{a:'phone', b:'Phone', c:'' , l:'', q:'down', cc: 'black' }, 
			{a:'status', b:'Status', c:'' , l:'', q:'down', cc: 'black' }];
	$scope.tabletitle = [ 
			{a:'vorder', b:'Order', c:'' , l:'', q:'down', cc: 'fuchsia', h:'left' }, 
			{a:'name', b:'Title', c:'' , l:'25', q:'down', cc: 'black', h:'left' }, 
			{a:'city', b:'Location', c:'' , l:'', q:'down', cc: 'black', h:'left' }, 
			{a:'maxpax', b:'MaxPax', c:'' , l:'', q:'down', cc: 'black', h:'left' }, 
			{a:'price', b:'Price', c:'' , l:'', q:'down', cc: 'black', h:'left' }, 
			{a:'event_times', b:'Times', c:'' , l:'', q:'down', cc: 'black', h:'left' },
			{a:'deposit', b:'Deposit', c:'' , l:'', q:'down', cc: 'black', h:'left' },
			{a:'bookings', b:'Bookings Per Pax', c:'' , l:'', q:'down', cc: 'black', h:'left' },
			{alter:'startv', a:'start', b:'Start', c:'date' , q:'down', cc: 'black', h:'left' }, 
			{alter:'endv',  a:'end', b:'End', c:'date' , q:'down', cc: 'black', h:'left' }, 
			{alter:'displayv',  a:'display', b:'Display', c:'date' , q:'down', cc: 'black', h:'left' } ];
	//console.log("restaurant: "+$scope.restaurant+", email: "+$scope.email+", showprice: "+$scope.showprice);
	if ($scope.showprice === 1 || true)
		$scope.tabletitle.push({a:'bkpax', b:'BkPax', c:'' , l:'', q:'down', cc: 'black' });
	$scope.bckups = $scope.tabletitle.slice(0);
	//$scope.tabletitleContent = [ { a:'name', b:'Name', c:'', d:'anchor', t:'input', i:0 }, { a:'title', b:'Title', c:'', d:'tag', t:'input', i:0 },{ a:'description', b:'Description', c:'', d:'list', t:'textarea' }, { a:'city', b:'Location', c:'', d:'map-marker', t:'input', i:0 }, { a:'price', b:'Price', c:'', d:'money', t:'inputprice', i:0 }, { a:'maxpax', b:'Max Pax', c:'', d:'male', t:'array', val:paxnumbers, i:0 }, {a:'start', b:'Starting Date', c:'date', d:'calendar', t:'date', u:$scope.mydata_start }, {a:'end', b:'Ending Date', c:'date', d:'calendar', t:'date', u:$scope.mydata_endng }, {a:'display', b:'Display Date', c:'date', d:'calendar', t:'date', u:$scope.mydata_display },{ a:'tnc', b:'Terms/Conditions', c:'', d:'legal', t:'textarea' },{ a:'type', b:'Type', c:'', d:'info', t:'array', val: $scope.atype, func: $scope.nonefunc },{ a:'menu', b:'Menus', c:'', d:'cutlery', t:'array', val: $scope.amenus, func: $scope.nonefunc },{ a:'morder', b:'Position', c:'', d:'sort', t:'array', val: aorder, func: $scope.nonefunc },{ a:'picture', b:'Select an image', c:'', d:'photo', t:'picture' } , { a:'picture', b:'Picture', c:'', d:'photo', t:'pictureshow' } ];
    $scope.tabletitleContent = [ 
    		{ a:'name', b:'Name', c:'', d:'anchor', t:'input', i:0, readonly :1 }, 
    		{ a:'title', b:'Title', c:'', d:'tag', t:'input', i:0,readonly :1 },
    		{ a:'description', b:'Description', c:'', d:'list', t:'textarea' }, 
    		{ a:'city', b:'Location', c:'', d:'map-marker', t:'input', i:0 }, 
    		{ a:'price', b:'Price', c:'', d:'money', t:'inputprice', i:0 }, 
    		{ a:'maxpax', b:'Max Pax', c:'', d:'male', t:'array', val:paxnumbers, i:0 }, 
    		{ a:'start', b:'Starting Date', c:'date', d:'calendar', t:'date', u:$scope.mydata_start }, 
    		{ a:'end', b:'Ending Date', c:'date', d:'calendar', t:'date', u:$scope.mydata_endng }, 
    		{ a:'display', b:'Display Date', c:'date', d:'calendar', t:'date', u:$scope.mydata_display },
    		{ a:'tnc', b:'Terms/Conditions', c:'', d:'legal', t:'textarea' }, 
    		{ a:'type', b:'Type', c:'', d:'info', t:'array', val: $scope.atype, func: $scope.nonefunc }, 
    		{ a:'morder', b:'Position', c:'', d:'sort', t:'array', val: aorder, func: $scope.nonefunc }, 
    		{ a:'pdf_link', b:'document-link', c:'', d:'male', t:'array', val:$scope.documentdpf, i:0 },
    		{ a:'event_times', b: 'Times', c: '', d: 'clock-o', t:'input' },
    		{ a:'deposit', b: 'Deposit', c: '', d: 'money', t:'input', i:0 },
    		{ a:'bookings', b: 'Per Bookings(maxpax)', c: '', d: 'book', t:'input', i:0 },
                { a:'perbookingprice', b: 'Deposit Per Booking  ', c: '', d: 'sort', t:'checkbox', i:0 },
    		{ a:'picture', b:'Select an image', c:'', d:'photo', t:'picture' } , 
    		{ a:'picture', b:'Picture', c:'', d:'photo', t:'pictureshow' } ];
	$scope.currency = 'SGD';
	$scope.getAlignment = function(a) {
		return (typeof a === 'number') ? 'center':'left';
	}
	$scope.imgEvent = imgEvent.slice(0);
   	if(typeof email === 'string' && email !== '' && email.search('@weeloy.com') > 0)
		$scope.tabletitleContent.splice(-2, 0, { a:'featured', b:'Featured On Home Page', c:'', d:'sort', t:'checkbox',i:0 });
	$scope.Objevent = function() {
		return {
			restaurant: $scope.restaurant, 
			eventID: 0,
			index: 0, 
			name: '',
			title: '', 
			city: '', 
			country: '', 
			start: '', 
			end: '', 
			startv: '', 
			endv: '',
			displayv: '',
			display:'',
			sddate:'',
			eddate:'', 
            dpdate:'',
            pdf_link: '',
			description: '',
			tnc: '',
			type: $scope.atype[0],
			menu: '',
			maxpax: 0, 
			morder: 0,
			bkpax: 0,
			bkevent: [],
			object_id :'',
			vorder: 0,
           	featured :false,
                perbookingprice :false,
			picture: '',
			price: '',
			event_times: '',
			deposit: 0,
			bookings: 0,
			remove: function() { 
				for(var i = 0; i < $scope.names.length; i++) 
					if($scope.names[i].name === this.name) {
						$scope.names.splice(i, 1);
						break;
					}
			},
			replicate: function(obj) {
				for (var attr in this)
		        	if(this.hasOwnProperty(attr) && typeof this[attr] !== 'function' && attr !== '$$hashKey') 
        				this[attr] = obj[attr];
				return this;
			},						
			clean: function() {
				if(this.start === '') this.start = $scope.mydata_start.getDate('-', 'reverse');
				if(this.end === '') this.end = $scope.mydata_endng.getDate('-', 'reverse');
                		if(this.display === '') this.display = $scope.mydata_display.getDate('-', 'reverse');
				for (var attr in this)
		        	if(this.hasOwnProperty(attr) && typeof this[attr] !== 'function' && attr !== '$$hashKey') {
        				if(typeof this[attr] === 'string') {
        					this[attr] = this[attr].replace(/\'|\"/g, '’');
        					}
        				}
				return this;
			},
			check: function() {
				if(this.name === '' || this.name.length < 6) { alert('Invalid title name, empty or too short(5)'); return -1;}
				if(this.title === '' || this.title.length < 3) { alert('Invalid title name, empty or too short(2)'); return -1;}
				return 1;
			}
		};
	};
	$scope.initorder = function() {
		$scope.tabletitle = $scope.bckups;
		$scope.predicate = "vorder";
		$scope.reverse = true;
	};
	$scope.reorder = function(item, alter) {
		alter = alter || "";
		if (alter !== "")  
			item = alter;
		$scope.reverse = ($scope.predicate === item) ? !$scope.reverse : false;
		$scope.predicate = item;
	};
	bookService.readEvent($scope.restaurant, $scope.email).then(function(response) {
		var i, j, data, bkdata, bkflag;
		//console.log("readEvent(): response: "+JSON.stringify(response));
		if(response.status !== 1 && response.status !== "1")
			return;
		$scope.names = [];
		data = response.data.event;
		bkdata = response.data.evbooking;
		bkflag = response.data.evbookable;
		$scope.currency = response.data.currency;
		if(typeof $scope.currency === "string" && $scope.currency !== "")
			$scope.tabletitleContent.iconcurrency($scope.currency, "price", "a", "d");
		$scope.evbooking = bkdata.slice(0);
		data.map(function(oo) {
			oo.index = i + 1;
			oo.vorder = parseInt(oo.morder);
			if (typeof oo.maxpax !== 'string') 
				oo.maxpax = "0";
			oo.maxpax = parseInt(oo.maxpax);
			oo.startv = oo.start.jsdate().getTime();
			oo.endv = oo.end.jsdate().getTime();
			if(oo.display==='0000-00-00') {
				oo.display = todaydate.getDateFormatReverse('-');
				oo.dpdate = todaydate.getDateFormat('-');
				oo.displayv = todaydate.getTime();		   
			} else {
				oo.displayv = oo.display.jsdate().getTime();
				oo.dpdate = oo.display.jsdate().getDateFormat('-');			  
			}
			oo.sddate = oo.start.jsdate().getDateFormat('-');
			oo.eddate = oo.end.jsdate().getDateFormat('-');
			oo.bkpax = 0;
			oo.bkevent = [];
			oo.featured = (oo.is_homepage==='1') ? true : false;
                        oo.perbookingprice = (oo.is_perbookingprice==='1') ? true : false;
			if(bkflag === 1) {
				bkdata.map(function(vv) {
					if(vv.eventID === oo.eventID) {
						if(vv.status !== 'cancel')
							oo.bkpax += parseInt(vv.pax);
						vv.fullname = vv.firstname + ' ' + vv.lastname;
						oo.bkevent.push(vv);
						}
					});
				}
			$scope.names.push(new $scope.Objevent().replicate(oo));                    
			});
		$scope.getPDF();
		$scope.paginator.setItemCount($scope.names.length);
		$scope.initorder();
		console.log("readEvent() done!");
	});

	bookService.readMenu($scope.restaurant, $scope.email).then(function(response) {
		var i, data, oo;
		$scope.amenus = [];		
		//console.log("readMenu(): response: "+JSON.stringify(response));
		if(response.status !== 1 && response.status !== "1")
			return;
		data = response.data.menus;
		for (i = 0; i < data.length; i++)
			$scope.amenus.push(data[i].value + '-' + data[i].menuID);
		oo = $scope.tabletitleContent.find(function(oo, index, arr) { return (oo.a === "menu"); });
		if(oo && oo.val) oo.val = $scope.amenus;
	});
    $scope.readEvBooking = function(confirmation) {
		return bookService.readEventBkDetails(confirmation).then(function(response) {
        	$scope.selectedBooking.tracking ='';
        	//console.log("readEvBooking(): readEventBkDetails() response: "+JSON.stringify(response));
            if (response.data) {
            	var res = response.data;
                $scope.selectedBooking.tracking = res;
			}
		});
    };
	$scope.cleaninput = function(ll) {
		if(typeof $scope.selectedItem[ll] === 'string' && $scope.selectedItem[ll] !== '')
			$scope.selectedItem[ll] =  $scope.selectedItem[ll].replace(/\'|\"/g, '’');
	};
	$scope.reset = function(item) {
		$scope.listEventFlag = false;
		$scope.viewEventFlag = false
		$scope.createEventFlag = false;
		$scope.viewlistBkEventFlag = false;
		$scope.viewBkEventFlag = false;
		if(item === 'viewlistBkEventFlag') {
			$scope.paginatorsub.setItemCount($scope.selectedItem.bkevent.length);
			$scope.paginatorsub.setPage(0);
		}
		$scope[item] = true;	
	};

	$scope.backlisting = function(page) {
		$scope.reset((page === undefined) ? 'listEventFlag' : page);
	};
		
	$scope.findaccount = function(name) {
		for(var i = 0; i < $scope.names.length; i++)
			if($scope.names[i].name === name)
				return i;
		return -1;
	};	
	$scope.viewevbook = function(oo) {
		console.log(JSON.stringify(oo));
		$scope.selectedBooking = oo;
        $scope.selectedBooking.refund_amount = oo.amount;
        $scope.selectedBooking.payment_status = oo.status;
        $scope.getPaymentDetails(oo);
		$scope.reset('viewBkEventFlag');
        if(oo.object_id !== '')
        	$scope.readEvBooking(oo.object_id);
	};
	$scope.view = function(oo, item) {
		$scope.selectedItem = oo;
		if(item !== 'bkpax')
			$scope.reset('viewEventFlag');
		else $scope.reset('viewlistBkEventFlag');
	};
	$scope.create = function() {
		$scope.selectedItem = new $scope.Objevent();
		$scope.mydata_start.setDate(todaydate);
		$scope.mydata_endng.setDate(todaydate);
		$scope.mydata_display.setDate(todaydate);
		$scope.reset('createEventFlag');
		$scope.buttonlabel = "Save new event";
		$scope.action = "create";
		return false;
	}
	$scope.update = function(oo) { 
		$scope.selectedItem = new $scope.Objevent().replicate(oo);
		$scope.mydata_start.originaldate = new Date($scope.selectedItem.start);
		$scope.mydata_endng.originaldate = new Date($scope.selectedItem.end);
		$scope.mydata_display.originaldate = new Date($scope.selectedItem.display);
		$scope.reset('createEventFlag');
		$scope.buttonlabel = "Update new event";
		$scope.action = "update";
	};
	$scope.savenewevent = function() {
		var u, msg, apiurl, ind, oo = $scope.selectedItem;
		oo.clean();		
		if(oo.check() < 0)
			return;
		oo.startv = oo.start.jsdate().getTime();
		oo.endv = oo.end.jsdate().getTime();
        oo.displayv = oo.end.jsdate().getTime();
		oo.vorder = parseInt(oo.morder);
		if($scope.atype.indexOf(oo.type) < 0)
			oo.type = $scope.atype[0];
		if(typeof oo.morder !== "number" || oo.morder === 0 || oo.morder === "")
			oo.morder = oo.vorder = Math.max.apply(Math,$scope.names.map(function(o){return o.morder;})) + 1;
		if($scope.action === "create"){
			if($scope.findaccount(oo.name) >= 0) {
				alert("name " + oo.name + " already exists. Please choose another name !");
				return;
			}				
			bookService.logevent(170,'BO_EVENT_creation','BO_EVENT');
			bookService.createEvent($scope.restaurant, $scope.email, oo ).then(function(response) {
				//console.log("createEvent(): response: "+JSON.stringify(response));
				if(response.status > 0) {
					oo.index = ($scope.names.length > 0) ? $scope.names[$scope.names.length - 1].index + 1 : 1;
					oo.eventID = response.status;
					alert("Event has been created with ID "+oo.eventID); 
					$scope.names.push(oo);
				}
			});
		} else {
			bookService.logevent(171,'BO_EVENT_update','BO_EVENT');
			bookService.updateEvent($scope.restaurant, $scope.email, oo ).then(function(response) {
				//console.log("updateEvent(): response: "+JSON.stringify(response));
				if(response.status > 0) {
					ind = $scope.findaccount(oo.name);	
					if(ind >= 0) 
						$scope.names.splice(ind, 1);
					$scope.names.push(oo);
					alert("Event has been updated");
				}
			});
		}
 		$scope.backlisting();		
	};
	$scope.delete = function(oo) {
		if(confirm("Are you sure you want to delete " + oo.name) == false)
			return;
		bookService.deleteEvent($scope.restaurant, $scope.email, oo.name).then(function() { 
			bookService.logevent(171,'BO_EVENT_delete','BO_EVENT');
			oo.remove();
			alert(oo.name + " has been deleted"); 
		});
	};
	$scope.extractSelection = function() {
		var maxlimit = 1500, tt, sep, sep1, i, j, k; // ajax call might not support more data
		var model = $scope.tabletitlebook, exportselect = $scope.filteredBook;
		var limit = exportselect.length;

		if (limit > maxlimit) limit = maxlimit;
		tt = '';
		for (i = 0, sep = ''; i < limit; i++, sep = ', ') {
			u = exportselect[i];
			tt += sep + '{ ';
			for(j = 0, sep1 = ''; j < model.length; j++, sep1 = ', ') {
				k = model[j].a;
				tt += sep1 + '"' + k + '":"' + u[k] + '"';
				}
			tt += ' }';
			}
		tt = '{ "booking":[' + tt + '] }';
		$scope.content = tt;
		$('#content').val(tt);
		$('#extractForm').submit();
		};
                
	$scope.getPDF = function() {
		var url = '../api/restaurant/media/picture/' + $scope.restaurant + "/pdf";
		$http.get(url).success(function(response) {
			//console.log("getPDF(): response: "+JSON.stringify(response));
			$scope.documentdpf = [];
			response.data.map(function(oo) { $scope.documentdpf.push(oo.name); });
			var index = $scope.tabletitleContent.inObject("a", "pdf_link");
			if(index >= 0) 
				$scope.tabletitleContent[index].val = $scope.documentdpf;
		});
	};            
	//event booking payment details
    $scope.getPaymentDetails = function(oo){
    	bookService.getEventPayment(oo.paykey).then(function(response) {
        	//console.log("getEventPayment(): response: "+JSON.stringify(response));
            if(response.status === 1 && response.data.payment) {
            	if(response.data.payment.status === 'REFUNDED')
                	$scope.selectedBooking.refund_amount = response.data.payment.refund_amount;
				$scope.selectedBooking.payment_status = response.data.payment.status;
                $scope.selectedBooking.payment_method = response.data.payment.payment_method;
			}
		});
	};
	$scope.depositRefund = function(oo) {
    	if(parseInt(oo.amount) >= parseInt(oo.refund_amount)) { 
        	if (confirm("by cancelling the Event " + oo.orderID + ", you need refund to guest SGD "+ oo.refund_amount ) == false)
            	return;
			bookService.evDepositRefund($scope.restaurant,  oo.orderID,$scope.email,oo.refund_amount,'stripe').then(function(response) {
            	//console.log("evDepositRefund(): response: "+JSON.stringify(response));
                if(response.data.status === 1) {
					$scope.selectedBooking.payment_status = 'REFUNDED';
                    $scope.selectedBooking.refund_amount = oo.refund_amount;
                    alert('The transaction has been completed.' );
				} else
                	alert('refund Failed :'+ response.data.error);
			});
		} else
        	alert("IMPT:Refund amount shall not exceed the initial deposit payment. ");
	};
}]);