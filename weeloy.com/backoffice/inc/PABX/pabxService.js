app.service('CallInfo', ['$http', '$q', function ($http, $q) {

    this.getCalls = function () {
        return $http.post("../api/services.php/pabx/list/",
        {
            'token': token
        }).then(function (response) {
            return response.data;
        });
    }    
    this.getAccountInfo = function () {
        return $http.post("../api/services.php/pabx/getaccount/",
        {
            'token': token
        }).then(function (response) {
            return response.data;
        });        
    };
}]);