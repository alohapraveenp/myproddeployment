<div class="container">
    <div class="row">
        <div class="col-md-12 left-sec">

            <div ng-controller="OrderingController" ng-init="moduleName = 'ordering'; listingOrderingFlag = true; detailsOrderingFlag = false; createOrderingFlag = false;" >

                <div id='listing' ng-show='listingOrderingFlag'>
                    <div class="form-group"  style='margin-bottom:25px;'>
                        <div class="col-md-4">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>
                                <input type="text" class="form-control input-sm" ng-model="searchText" ng-change="paginator.setPage(0);" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" style='width:200px;'> 
                            </div>
                        </div>
                        <div class="col-md-2"></div>
                        <div class="col-md-4">
                            <a href ng-click='create();' class="btn btn-success btn-sm" style='color:white;width:200px;'><span class='glyphicon glyphicon-certificate'></span> &nbsp;Create a New Ordering</a>
                        </div>
                          <div class="col-md-2">
                          <a href ng-click='togglearchive();' class="btn btn-info btn-sm" style='color:white;width:100px;'><span ng-if="archivefilter.status !== 'archive'">Show Archive</span><span ng-if="archivefilter.status === 'archive'">Hide Archive</span></a>
                          </div>
                  </div><br/>

                    <table width='100%' class="table table-condensed table-striped" style='font-family:helvetica;font-size:12px;'>
                        <tr>
                            <th ng-repeat="y in tabletitle"><tbtitle var="{{y.a}}" name='tabletitle'  module='moduleName'/></th>
                        <th><span ng-show="archivefilter.status !== 'archive'">update/archive</span></th><th> &nbsp; </th><th><span ng-show="archivefilter.status !== 'archive'">delete</span></th>
                        </tr>
                        <tr ng-repeat="x in filteredOrdering = (names| filter:searchText | filter: archivefilter) | sizefilter:this | orderBy:predicate:reverse | slicefilter:paginator.getPage():paginator.getRowperPage():paginator.getItemCount() | limitTo:paginator.getRowperPage()" style='font-family:helvetica;font-size:11px;'>
                            <td ng-repeat="z in tabletitle"><a href ng-click="view(x)">{{x[z.a] | adatereverse:z.c}}</a></td>
                            <td style='text-align:center' width='40'><a href ng-if="archivefilter.status !== 'archive'" ng-click="update(x)"><span ng-if="editdate < x.datetime" class='glyphicon glyphicon-pencil blue'></span><span ng-if="editdate >= x.datetime" class='glyphicon glyphicon-folder-open blue'></span></a></td>
                            <td width='30'>&nbsp;</td>
                            <td align='center'><a href ng-if="editdate < x.datetime && archivefilter.status !== 'archive'" ng-click="delete(x)"><span class='glyphicon glyphicon-trash red'></span></a></td>
                        </tr>
                        <tr><td colspan='{{tabletitle.length+3}}'></td></tr>
                    </table>
					<div ng-if="filteredOrdering.length >= paginator.getRowperPage()" class="col-md-12" align="center" style='font-size:11px' ng-include="'inc/paginator.html'"></div>
                </div>

                <div ng-show='detailsOrderingFlag'>
                    <br />
                    <a href class='btn btn-info btn-sm customColor' ng-click="backlisting()"><span class='glyphicon glyphicon-step-backward'></span> &nbsp;Back to listing</a>
                    <br />
                    <table class='table-striped' style="margin: 0 0 10px 30px;font-size:13px;font-family: Roboto">		
                        <tr ng-repeat="x in orderViewContent" ><td nowrap>{{ x.label}}: </td><td> &nbsp; </td><td class="strong" >{{ selectedItem[x.value] | adatereverse:x.c}}</td></tr>
                    </table>
                    <table class='table-striped' ng-repeat="y in itemList" style="margin: 0 0 10px 30px;font-size:13px;font-family: Roboto">		
                        <tr ng-repeat="z in itemListContent" ><td width='50px'>&nbsp;</td><td nowrap>{{ z.label}}: </td><td> &nbsp; </td><td class="strong" >{{ y[z.value]}}</td></tr>
                    </table>
                    <div class="col-md-8"></div>
                    <div class="col-md-4"><button type='button' class='btn btn-primary input13' ng-click="duplicate(x)" ><i class="glyphicon glyphicon-save input13" style='align:left'></i>&nbsp; duplicate this order</button></div>
                    <br /><br />
               </div>

                <div ng-show='createOrderingFlag'>
                    <br />
                    <a href class='btn btn-info btn-sm customColor' ng-click="backlisting()"><span class='glyphicon glyphicon-step-backward'></span> &nbsp;Back to listing</a>
                    <br />
                    <div class="col-md-2"></div>
                    <div class="col-md-8">
                        <div class="row" ng-repeat="y in orderCreaContent | filter: {t: '!dontshow' }" style="margin: 0 0 20px 0;font-size:12px;font-familly:Roboto">

                            <div class="input-group" ng-if="y.t === 'input'">
                                <span class="input-group-addon input11"><i class="fa fa-{{y.d}} fa-lg"></i>&nbsp; {{y.b}}</span>
                                <input type="text" class="form-control input-sm" ng-model="selectedItem[y.a]" ng-change='cleaninput(y.a);' ng-readonly="y.r === true" >
                            </div> 
                            <div class="input-group" ng-if="y.t === 'textarea'">
                                <span class="input-group-addon input11"><i class="fa fa-{{y.d}} fa-lg"></i>&nbsp; {{y.b}}</span>
                                <textarea class="form-control input-sm" ng-model="selectedItem[y.a]" rows="5" ng-change='cleaninput(y.a);' ng-readonly="y.r === true"></textarea>
                            </div>
                            <div class="input-group" ng-if="y.t === 'date'">
                                <span class="input-group-btn">
                                    <button type="button" class="btn btn-default btn-sm" ng-click="y.a.dateopen($event)"><i class="fa fa-calendar fa-lg"></i>&nbsp; {{y.b}}</button>
                                </span>
                                <input type="text" class="form-control input-sm" uib-datepicker-popup="{{y.a.formats[0]}}"  min-date="y.a.minDate" max-date="y.a.maxDate" ng-model="y.a.originaldate"
                                       is-open="y.a.opened" datepicker-options="y.a.dateOptions" ng-change='y.a.onchange();' close-text="Close" readonly />
                            </div>
                            <div class="input-group" ng-if="y.t === 'array'">
                                <div class='input-group-btn' uib-dropdown >
                                    <button type='button' class='btn btn-default btn-sm' uib-dropdown-toggle >
                                        <i class="fa fa-{{y.d}} fa-lg"></i>&nbsp;{{y.b}}&nbsp;<span class='caret'></span></button>
                                    <ul class='dropdown-menu scrollable-menu' uib-dropdown-menu >
                                        <li ng-repeat="p in y.val"><a href ng-click="selectedItem[y.a] = p; y.func()">{{ p}}</a></li>
                                    </ul>
                                </div>
                                <input type='text' ng-model='selectedItem[y.a]' class='form-control input-sm' readonly >
                            </div>
                            <div class="input-group" ng-if="y.t === 'picture'">
                                <div class='input-group-btn' uib-dropdown >
                                    <button type='button' class='btn btn-default btn-sm input11' uib-dropdown-toggle >
                                        &nbsp;<i class="fa fa-{{y.d}} fa-lg"></i>&nbsp;{{y.b}}&nbsp;<span class='caret'></span>
                                    </button>
                                    <ul class='dropdown-menu scrollable-menu' uib-dropdown-menu >
                                        <li ng-repeat="p in imgEvent"><a href ng-click="selectedItem['picture'] = p;">{{ p}}</a></li>
                                    </ul>
                                </div>
                                <input type='text' ng-model="selectedItem['picture']" class='form-control input-sm' readonly >
                            </div>
                            <div class="input-group" ng-if="y.t === 'pictureshow'">
                                <p ng-if="selectedItem.picture != ''"><img ng-src="{{path}}{{selectedItem.picture}}" height='150'/></p>
                            </div>

                        </div><br />

                        <h3><center>Ordering Items Description</center></h3>
                        <div ng-repeat="x in itemList| filter: {status: '!delete' }">

                            <div class="row" ng-repeat="y in itemCreaContent| filter: {t: '!dontshow' }" style="margin: 0 0 20px 0;font-size:12px;font-familly:Roboto">
                                <div class="input-group" ng-if="y.t === 'input'">
                                    <span class="input-group-addon input11"><i class="fa fa-{{y.d}} fa-lg"></i>&nbsp; {{y.b}}</span>
                                    <input type="text" class="form-control input-sm" ng-model="x[y.a]" ng-change="y.func(x)" ng-readonly="y.r === true">
                                </div> 
                                <div class="input-group" ng-if="y.t === 'textarea'">
                                    <span class="input-group-addon input11"><i class="fa fa-{{y.d}} fa-lg"></i>&nbsp; {{y.b}}</span>
                                    <textarea class="form-control input-sm" ng-model="x[y.a]" rows="5" ng-change="y.func(x)" ng-readonly="y.r === true"></textarea>
                                </div>
                                <div class="input-group" ng-if="y.t === 'date'">
                                    <span class="input-group-btn">
                                        <button type="button" class="btn btn-default btn-sm" ng-click="y.a.dateopen($event)"><i class="fa fa-calendar"></i>&nbsp; {{y.b}}</button>
                                    </span>
                                    <input type="text" class="form-control input-sm" uib-datepicker-popup="{{y.a.formats[0]}}" min-date="y.a.minDate" max-date="y.a.maxDate" ng-model="y.a.originaldate"
                                           is-open="y.a.opened" datepicker-options="y.a.dateOptions" ng-change="y.a.onchange();" close-text="Close" readonly />
                                </div>
                                <div class="input-group" ng-if="y.t === 'array'">
                                    <div class='input-group-btn' uib-dropdown >
                                        <button type='button' class='btn btn-default btn-sm' uib-dropdown-toggle >
                                            <i class="fa fa-{{y.d}} fa-lg"></i>&nbsp;{{y.b}}&nbsp;<span class='caret'></span></button>
                                        <ul class='dropdown-menu scrollable-menu' uib-dropdown-menu >
                                            <li ng-repeat="p in y.val"><a href ng-click="x[y.a] = p; y.func()">{{ p}}</a></li>
                                        </ul>
                                    </div>
                                    <input type='text' ng-model='x[y.a]' class='form-control input-sm' readonly >
                                </div>
                            </div>

                            <p align="right"><a href ng-click='deleteitem(x);' class="btn btn-warning btn-sm" style='color:white;width:100px;'><span class='glyphicon glyphicon-minus'></span> &nbsp;delete item </a></p><hr />
                        </div>

                        <div><a href ng-click='onemore();' class="btn btn-info btn-sm" style='color:white;'><span class='glyphicon glyphicon-plus'></span> &nbsp;one more </a><br /></div>
                    </div>
                    <div class="col-md-2"></div>
                    <div  class="col-md-8"></div>
                    <div  class="col-md-4"><a href ng-click='savenewordering();' class="btn btn-success btn-sm" style='color:white;width:200px;'><span class='glyphicon glyphicon-save'></span> &nbsp;{{ buttonlabel}} </a></div>
                </div>

            </div>
        </div>
    </div>
</div>

<script>

var token = <?php echo "'" . $_SESSION['user_backoffice']['token'] . "';"; ?>

<?php
$mediadata = new WY_Media($theRestaurant);
printf("var pathimg = '%s';", $mediadata->getFullPath('small'));
?>


app.controller('OrderingController', ['$scope', '$http', 'bookService', 'cleaningData', function($scope, $http, bookService, cleaningData) {

	var forceDate = new Date(new Date().getFullYear()-2, 0, 1);
	var adelivery = ['pickup', 'delivery' ];
	var atitle = ['Mr.', 'Mrs.', 'Ms' ];
	var aorder = (function() { var i, arr = []; for (i = 0; i < 30; i++) arr.push(i); return arr; })();
	$scope.currency = "";
	$scope.paginator = new Pagination(25);
	$scope.restaurant = <?php echo "'" . $theRestaurant . "';"; ?>
	$scope.typeselection = 1;
	$scope.email = <?php echo "'" . $email . "';"; ?>
	$scope.names = [];
	$scope.subnames = [];
	$scope.predicate = 'index';
	$scope.reverse = false;
	$scope.tabletitle = [ {'a':'orderID', 'b':'index', c:'', q:'down', cc: 'black' }, {'a':'fullname', 'b':'guest', c:'', q:'down', cc: 'black' }, {alter:'datetime', 'a':'delivery_date', 'b':'Date', c:'date', q:'down', cc: 'black' }, {alter:'cdatetime', 'a':'cdate', 'b':'CreateDate', c:'date', q:'up', cc: 'orange' }, {'a':'delivery_mode', 'b':'Mode', c:'', q:'down', cc: 'black' } ];
	$scope.bckups = $scope.tabletitle.slice(0);
	$scope.itemList = [];
	$scope.path = pathimg;
	$scope.current = null;
	$scope.mydata = new bookService.ModalDataBooking();
	$scope.cdate = new Date().today();
	$scope.createdate = new Date().today().getDateFormatReverse('-');
	$scope.editdate = $scope.cdate.getTime() - (2 * 24 * 3600 * 1000);
	$scope.cleananinput = function(x) { x.clean(); }
	$scope.mydata.setInit($scope.cdate, "09:00", function() { $scope.selectedItem.delivery_date = $scope.mydata.originaldate.getDateFormatReverse('-'); }, 3, new Date(), new Date($scope.cdate.getFullYear() + 1, 11, 31));
	$scope.orderViewContent = [{ value: 'orderID', label:'Order number' , c:'' }, { value: 'delivery_date', label:'delivery date' , c:'date' }, { value: 'fullname', label:'Guest' , c:'' }, { value: 'email', label:'Email' , c:'' }, { value: 'mobile', label:'Mobile' , c:'' }, { value: 'cdate', label:'Create Date', c:'date' }, { value: 'total', label:'Total Order' , c:'' }, { value: 'nborder', label:'Number of meals' , c:'' }, { value: 'quantity', label:'Total quantity' , c:'' }, { value: 'tax', label:'Tax' , c:'' }, { value: 'address', label:'Address' , c:'' }, { value: 'zip', label:'Zip Code' , c:'' } ];
	$scope.itemListContent = [{ value: 'item_title', label:'Name' }, { value: 'unit_price', label:'Price' }, { value: 'quantity', label:'Quantity' } ];
	$scope.orderCreaContent = [ { a:'restaurant', b:'Restaurant', c:false, d:'cutlery', t:'input', r:true }, { a:'orderID', b:'Order ID', c:false, d:'tag', t:'input', r:true }, { a:'status', b:'Status', c:false, d:'check-circle', t:'input', r:true }, { a: $scope.mydata, b:'Delivery Date', c:false, d:'calendar', t:'date', r:false }, { a:'total', b:'Total Price', c:false, d:'usd', t:'input', r:false }, { a:'tax', b:'Tax', c:false, d:'plus', t:'input', r:false }, { a:'delivery_mode', b:'Delivery Type', c:false, d:'truck', t:'array', val: adelivery, func: $scope.nonefunc, r:false }, { a:'title', b:'Title', c:false, d:'user', t:'array', val: atitle, func: $scope.nonefunc, r:false }, { a:'firstname', b:'First Name', c:false, d:'user', t:'input', r:false }, { a:'lastname', b:'Last Name', c:false, d:'user', t:'input', r:false }, { a:'email', b:'Email', c:false, d:'envelope', t:'input', r:false }, { a:'mobile', b:'Mobile', c:false, d:'phone', t:'input', r:false }, { a:'address', b:'Address', c:false, d:'home', t:'input', r:false }, { a:'zip', b:'Zip Code', c:false, d:'globe', t:'input', r:false } ];   // { a:'currency', b:'Currency', c:false, d:'usd', t:'array', val: acurrency, func: $scope.nonefunc, r:false }

	$scope.itemCreaContent = [ { a:'orderID', b:'Order number', c:false, d:'tags', t:'input', r:true, func: $scope.cleananinput }, { a:'itemID', b:'Item number', c:false, d:'list', t:'input', r:true, func: $scope.cleananinput }, { a:'status', b:'Status', c:false, d:'check', t:'input', r:true, func: $scope.cleananinput }, { a:'item_title', b:'Name', c:false, d:'tag', t:'input', r:false, func: $scope.cleananinput }, { a:'unit_price', b:'Price', c:false, d:'usd', t:'input', r:false, func: updateprice }, { a:'quantity', b:'Quantity', c:false, d:'align-justify', t:'array', val: aorder, func: updateprice, r:false } ];
	$scope.glbtax = 0.05;
	$scope.minorder = 0;
	$scope.archivefilter = {status: '!archive' };

	$scope.getAlignment = bkgetalignment;

	$scope.togglearchive = function() {
		$scope.archivefilter.status = ($scope.archivefilter.status === "archive") ? "!archive" : "archive";
		}
			
	$scope.orderingItemFactory = function(key, index) {
		return {
			orderID: key,
			itemID: index,
			status: 'new',
			item_title: '',
			unit_price: '',
			quantity: '1',
			gblindex: function() {
				for (var i = 0; i < $scope.subnames.length; i++)
					if ($scope.subnames[i].orderID === this.orderID && $scope.subnames[i].itemID === this.itemID)
						return i;
				return - 1;
			},
			
			clean: function() {
				var tmpAr = Object.keys(this), attr, oo = this;
				tmpAr.forEach (function (attr) { if (typeof oo[attr] === 'string') oo[attr] = oo[attr].replace(/\'|\"/g, '’'); })
				return this;
				},
				
			replicate: function(obj) {
				var tmpAr = Object.keys(this), attr, oo = this;
				tmpAr.forEach (function (attr) { oo[attr] = obj[attr]; })
				return this;
				}
			};
		};
		
	$scope.orderingFactory = function() {
		var cdate = new Date().today().getDateFormatReverse('-');
		
		return {
			restaurant: $scope.restaurant,
			orderID: Math.floor((Math.random() * 10000000) + 1),
			itemindex: 1,
			status: 'new',
			item_title: '',
			cdate: cdate,
			delivery_date: cdate,
			nborder: 0,
			quantity: 0,
			total: 0,
			tax: 0,
			currency: $scope.currency,
			delivery_mode: 'pickup',
			title: '',
			firstname: '',
			lastname: '',
			email: '',
			mobile: '',
			address: '',
			zip: '',
			datetime: 0,
			ddate: '',
			cdatetime: 0,
			cddate: '',	// for search 
			
			gblindex: function() {
				for (var i = 0; i < $scope.names.length; i++)
					if ($scope.names[i].orderID === this.orderID)
						return i;
				return - 1;
				},
				
			clean: function() {
				var tmpAr = Object.keys(this), attr, oo = this;
				tmpAr.forEach (function (attr) { if (typeof oo[attr] === 'string') oo[attr] = oo[attr].replace(/\'|\"/g, '’'); })
				return this;
				},
				
			replicate: function(obj) {
				var tmpAr = Object.keys(this), attr, oo = this;
				tmpAr.forEach (function (attr) { oo[attr] = obj[attr]; })
				return this;
				}
			};
		};
		
	$scope.gblindex = function(orderID) {
		for (var i = 0; i < $scope.names.length; i++)
			if ($scope.names[i].orderID === orderID)
				return i;
		return - 1;
		};
		
	$scope.copyobj = function(o, d) {
		var tmpAr = Object.keys(o), attr;
		tmpAr.forEach (function (attr) { d[attr] = o[attr]; })
		};
		
	$scope.prefillobj = function(o, d) {
		var tmpAr = Object.keys(o), attr;
		tmpAr.forEach (function (attr) {  if(typeof d[attr] !== 'undefined') d[attr] = o[attr]; })
		};
            
    $scope.initorder = function() {
		$scope.tabletitle = $scope.bckups;
		$scope.predicate = "cdate";
		$scope.reverse = true;
		};
		
	$scope.reorder = function(item, alter) {
		alter = alter || "";
		if (alter !== "")  item = alter;
		$scope.reverse = ($scope.predicate === item) ? !$scope.reverse : false;
		$scope.predicate = item;
		};

	bookService.readRestaurant($scope.restaurant).then(function(response) { 
		var data = response.data.data;		
		$scope.glbtax = parseFloat(data.tkouttax);
		$scope.minorder = parseFloat(data.tkoutminorder);
		$scope.currency = data.currency;
		if(typeof $scope.currency === "string" && $scope.currency !== "")
			$scope.orderCreaContent.iconcurrency($scope.currency, "total", "a", "d");

		$scope.modeorder = ((data.tkoutpayment === "1") ? 4 : 0) + ((data.tkoutdeliver === "1") ? 2 : 0) + ((data.tkoutpickup === "1") ? 1 : 0);
		});
		
    bookService.readOrdering($scope.restaurant, $scope.email).then(function(response) {
    	var oo, data, currency = $scope.currency;
		if (response === null) return;
		$scope.data = data = response.orders;
		$scope.subdata = response.orders_items;
		$scope.data.map(function(vv, i) {
			oo = new $scope.orderingFactory().replicate(vv);
			oo.index = i + 1;
			if(oo.firstname === "") oo.firstname = 'John' ;
			if(oo.lastname === "") oo.lastname = 'Smith' ;
			if(oo.delivery_mode === "") oo.delivery_mode = 'pickup' ;
			oo.fullname = oo.title + ' ' + oo.firstname + ' ' + oo.lastname;
			oo.datetime = oo.delivery_date.jsdate().getTime();
			oo.ddate = oo.delivery_date.jsdate().getDateFormat('-');
			oo.cddate = oo.cdate.jsdate().getDateFormat('-');
			oo.cdatetime = oo.cdate.jsdate().getTime();
			$scope.names.push(oo);
			});
		
    	$scope.subdata.map(function(vv) {
    		oo = new $scope.orderingItemFactory($scope.subdata[i].orderID, $scope.subdata[i].itemID).replicate(vv)
    		oo.index = i + 1;
        	$scope.subnames.push(oo);
    		});
    	$scope.paginator.setItemCount($scope.names.length);
    	$scope.initorder();
    	});
    
	$scope.reset = function(item) {
		$scope.listingOrderingFlag = false;
		$scope.detailsOrderingFlag = false
		$scope.createOrderingFlag = false;
		$scope[item] = true;
		};
		
	$scope.onemore = function() {
		var obj = new $scope.orderingItemFactory($scope.selectedItem.orderID, $scope.getnextIndex($scope.selectedItem.orderID));
		$scope.itemList.push(obj);
	};
	
	$scope.getnextIndex = function(orderID) {
		var i, nindex = 1;
		for(i = 0; i < $scope.itemList.length; i++)
			if( $scope.itemList[i].orderID === orderID && parseInt($scope.itemList[i].itemID) >= nindex)
				nindex = parseInt($scope.itemList[i].itemID) + 1;

		return nindex;
		};
		
	$scope.updateprice = function(x) {
		if (typeof x !== 'undefined') x.clean();
		var i, total = 0, nborder = 0, quantity = 0, limit = $scope.itemList.length;
		for (i = 0; i < limit; i++)
			if ($scope.itemList[i].status !== "delete") {
				$scope.itemList[i].unit_price = parseFloat($scope.itemList[i].unit_price);
				$scope.itemList[i].quantity = parseInt($scope.itemList[i].quantity);
				total += ($scope.itemList[i].unit_price * $scope.itemList[i].quantity);
				nborder++;
				quantity += $scope.itemList[i].quantity;
				}
		$scope.selectedItem.nborder = nborder;
		$scope.selectedItem.quantity = quantity;
		$scope.selectedItem.total = total;
		$scope.selectedItem.tax = (total * $scope.glbtax).toPrecision(2);
	};

	$scope.duplicate = function() {
		if(confirm("Please confirm you want to duplicate current order ?") === false)
			return;	
		
		var i, oo = $scope.selectedItem;
		oo.orderID = Math.floor((Math.random() * 10000000) + 1);
		oo.status = "new";
		oo.delivery_date = oo.cdate = $scope.cdate.getDateFormatReverse('-');
		for(i = 0; i < $scope.itemList.length; i++) {
			$scope.itemList[i].orderID = oo.orderID;
			$scope.itemList[i].status = "new";
			}
		$scope.savenewordering();
		}
			
	$scope.updateorderingobj = function(orders, items) {
		var i, ind;
		if (orders.status !== "new") {
			if ((ind = orders.gblindex()) < 0) { alert('Unable to find new record'); return; }
			$scope.copyobj(orders, $scope.names[ind]);
		} else {
			orders.status = "";
			orders.index = $scope.names.length;
			$scope.names.push(orders);
			}
			
		for(i = 0; i < items.length; i++) {
			if (items[i].status === "new") {
				items[i].status = "";
				$scope.subnames.push(items[i]);
				}
			else if ((ind = items[i].gblindex()) >= 0) {
				if (items[i].status === "delete")
					$scope.subnames.splice(ind, 1);
				else $scope.copyobj(items[i], $scope.subnames[ind]);
				}
			}
	};
	
	$scope.setdataOrder = function(orders) {
		var i, limit, orderID, oo;
		$scope.itemList = [];
		oo = new $scope.orderingFactory().replicate(orders);
		orderID = oo.orderID;
		limit = $scope.subnames.length;
		for (i = 0; i < limit; i++) {
			if ($scope.subnames[i].orderID === orderID)
				$scope.itemList.push(new $scope.orderingItemFactory(orderID, $scope.subnames[i].itemID).replicate($scope.subnames[i]));
			}
		$scope.selectedItem = oo;
		$scope.updateprice();
		oo.fullname = oo.title + ' ' + oo.firstname + ' ' + oo.lastname;
		oo.datetime = oo.delivery_date.jsdate().getTime();
		oo.ddate = oo.delivery_date.jsdate().getDateFormat('-');
		$scope.mydata.setDate(oo.delivery_date.jsdate());
		return oo;
	};
	
	$scope.view = function(oo) {
		$scope.selectedItem = $scope.setdataOrder(oo);
		$scope.reset('detailsOrderingFlag');
	};
	
	$scope.create = function() {
		$scope.selectedItem = new $scope.orderingFactory();
		$scope.itemList = [];
		$scope.buttonlabel = "Save new ordering";
		$scope.action = "create";
		$scope.reset('createOrderingFlag');
		return false;
		};
		
	$scope.update = function(oo) {
		if($scope.editdate >= oo.datetime) 
			return $scope.archivemaster(oo);

		$scope.selectedItem = $scope.setdataOrder(oo);
		$scope.buttonlabel = "Update";
		$scope.action = "update";
		$scope.reset('createOrderingFlag');
		return false;
	};
	
	$scope.savenewordering = function() {
		var oo = $scope.selectedItem;
		oo.fullname = oo.title + ' ' + oo.firstname + ' ' + oo.lastname;
		oo.datetime = oo.delivery_date.jsdate().getTime();
		oo.ddate = oo.delivery_date.jsdate().getDateFormat('-');
		oo.cdatetime = oo.cdate.jsdate().getTime('-');
		oo.cddate = oo.cdate.jsdate().getDateFormat('-');

		if (oo.status === "new") {
			bookService.createOrdering($scope.restaurant, $scope.email, { orders: oo, orders_items: $scope.itemList })
			.then(function(response) {
				var i, old = oo.orderID, realID = parseInt(response.errors);
				if (realID !== old) {
					oo.orderID = realID;
					for (i = 0; i < $scope.itemList.length; i++)
						$scope.itemList[i].orderID = realID;
					}
				if ($scope.updateorderingobj(oo, $scope.itemList) === false)
					return;
				alert("Order " + oo.orderID + " has been created");
			});
		}

		else {
			bookService.updateOrdering($scope.restaurant, $scope.email, { orders: oo, orders_items: $scope.itemList })
			.then(function(response) {
			if ($scope.updateorderingobj(oo, $scope.itemList) === false)
					return;
			alert("Order " + oo.orderID + " has been updated");
			});
		}

	$scope.backlisting();
	};

	$scope.archivemaster = function(oo) {
		oo.status = oo.status + "|archive" || "archive";
		bookService.updateOrdering($scope.restaurant, $scope.email, { orders: oo, orders_items: "status" })
			.then(function(response) {
			alert("Order " + oo.orderID + " has been archived");
			});
		}
			
	$scope.delete = function(oo) {
		if (confirm("Are you sure you want to delete " + oo.orderID) == false)
			return;

		bookService.deleteOrdering($scope.restaurant, $scope.email, oo.orderID).then(function() {
			var i, ind = $scope.gblindex(oo.orderID);
			
			$scope.names.splice(ind, 1);
			for (i = $scope.subnames.length - 1; i >= 0; i--)
				if ($scope.subnames[i].orderID === oo.orderID)
					$scope.subnames.splice(i, 1);
			alert(oo.orderID + " has been deleted");
			});
	};
	
	$scope.deleteitem = function(oo) {
		if (confirm("Are you sure you want to delete item " + oo.itemID + " of order " + oo.orderID) == false)
			return;

		oo.status = "delete";
		$scope.updateprice();
	};

	$scope.cleaninput = function(ll) {
		if (typeof $scope.selectedItem[ll] === 'string')
			$scope.selectedItem[ll] = $scope.selectedItem[ll].replace(/\'|\"/g, '’');
		};
	$scope.backlisting = function() {
	$scope.reset('listingOrderingFlag');
	};

	function updateprice() { $scope.updateprice(); };
	function nonefnct() {};
}]);
    
</script>

