<style>
    textarea {
        height: auto;
        max-width: 600px;
        color: #999;
        font-weight: 400;
        font-size: 30px;
        font-family: 'Roboto', Helvetica, Arial, sans-serif;
        width: 100%;
        background: #fff;
        border-radius: 3px;
        line-height: 2em;
        border: none;
        box-shadow: 0px 0px 5px 1px rgba(0, 0, 0, 0.1);
        padding: 30px;
        -webkit-transition: height 2s ease;
        -moz-transition: height 2s ease;
        -ms-transition: height 2s ease;
        -o-transition: height 2s ease;
        transition: height 2s ease;
    }
    * {
        -webkit-font-smoothing: antialiased !important;
    }
</style>
<script>
    <?php
        $mediadata = new WY_Media($theRestaurant);
        printf("var pathimg = '%s';", $mediadata->getFullPath('small'));
        $res = new WY_restaurant;
        $data = $res->getListAccountRestaurant($_SESSION['user_backoffice']['email']);
        $cn = count($data);
          printf("var multirest = '%d';", $cn);
?>
</script>
<div class="container">
    <div class="row">
        <div class="col-md-12 left-sec">
            <div ng-controller="EventOrderingController" md-theme="customTheme" id="evContainer" ng-cloak ng-init="moduleName = 'eventordering'; listingOrderingFlag = true; detailsOrderingFlag = false; createOrderingFlag = false;">
                <input type='hidden' id='token' value ="<?php echo $_SESSION['user_backoffice']['token'] ?>" />
                <input type='hidden' id='email' value ="<?php echo $_SESSION['user_backoffice']['email'] ?>" />
                <input type='hidden' id='restaurant' value ="<?php echo $theRestaurant ?>" />
                <input type='hidden' id='theRestoUrl' value ="<?php echo $theRestoUrl ?>" />
                <input type='hidden' id='multioutlet' value ="<?php echo $cn ?>" />
                <div ng-show='calendarflg'>
                    <br/>
                    <a href class='btn btn-info btn-sm blue' ng-click="backlisting()"><span class='glyphicon glyphicon-step-backward'></span> &nbsp;Back to listing</a>
                    <br/>

                    <md-content class="md-padding" layout-xs="column" layout="row">
                        <div flex-xs flex-gt-xs="50" layout="column">

                            <div class="container">
                                <div style="height: 500px;width: 600px;font-size:10px">
                                    <calendar-md id="calendar-demo"
                                                 flex layout layout-fill
                                                 calendar-direction="direction"
                                                 on-prev-month="prevMonth"
                                                 on-next-month="nextMonth"
                                                 on-day-click="dayClick"
                                                 title-format="'MMMM y'"
                                                 ng-model='originaldate'
                                                 day-format="dayFormat"
                                                 day-label-format="'EEE'"
                                                 day-label-tooltip-format="'EEEE'"
                                                 day-tooltip-format="'fullDate'"
                                                 week-starts-on="weekStartsOn"
                                                 start-Date-of-month="1"
                                                 no-of-days="30"
                                                 tooltips="true"
                                                 day-content="setDayContent"
                                                 disable-future-selection="disableFutureDates"></calendar-md>
                                </div>
                            </div>
                        </div>
                    </md-content>
                </div>
                <div id='listing' ng-show='listingOrderingFlag'>
                <div class="col-md-12">
                    <ul class="nav navbar-nav">
                        <li>
                            <div class="input-group" >
                            <md-input-container class="md-icon-float md-block">
                                <!-- Use floating label instead of placeholder -->
                                <label>Filter</label>
                                <md-icon md-font-set="md" style="color:#00f;" aria-label="Search">search</md-icon>
                                <input type="text" ng-model="searchText" ng-change="paginator.setPage(0);" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" style='width:150px;'>
                            </md-input-container>
                            </div>
                        </li>
<!--                        <li>
                            <div class="input-group" style='margin-left:5px'>
                            <md-button class="md-fab md-mini md-primary" aria-label="Show Calendar" ng-click="reset('calendarflg');">
                                <i class="material-icons" style="font-size:15px;">event</i>
                            </md-button>
                            </div>
                        </li>-->
                        <li>
                            <div class="input-group">
                            <md-button md-theme="dark-pink" class="md-primary md-raised md-small" ng-click='sendform($event);'><i class="material-icons" style="font-size:15px;">send</i>Send Form</md-button>
                            </div>
                        </li>
                        <li>
                            <div class="input-group" >
                            <md-button class="md-primary md-raised md-hue-2 md-small" ng-click='create();'><i class="material-icons" style="font-size:15px;">add_circle_outline</i> Create a New Ordering</md-button>
                            </div>
                        </li>
                        <li>
                            <div class="input-group" >
                            <md-button class="md-accent md-raised md-small" ng-click='togglearchive();'><i class="material-icons" style="font-size:15px;">folder</i> {{showhidearchive}}</md-button>
                            </div>
                        </li>
                        <li>
                            <div class="input-group" >
                            <input type="hidden" value="testing" ng-model='contentextractprof' name="contentextractprof" id="contentextractprof"/>
                             <md-button class="md-primary md-raised md-hue-2 md-small" ng-click='export();' ><i class="material-icons" style="font-size:13px;">save</i> &nbsp;Extract</md-button>
<!--                            <a href ng-click='export();' class="btn btn-success btn-sm" style='color:white;font-size:11px;'><span class='glyphicon glyphicon-save'></span> &nbsp;Extract</a>-->
                            </div>
                        </li>
                        <li ng-if ='isShowMulti'>
                            <div class="input-group" >
                            <md-button md-theme="dark-pink" class="md-primary md-raised  md-small" ng-click='showall();'><i class="material-icons" style="font-size:15px;">dashboard</i>Multi Outlet</md-button>
                            </div>
                        </li>
                    </ul>
                    </div>
                    <br/>
                    <table width='100%' class="table table-condensed table-striped" style='font-family:helvetica;font-size:12px;'>
                        <tr>
                            <th ng-repeat="y in tabletitle">
                                <tbtitle var="{{y.a}}" name='tabletitle' module='moduleName'/>
                            </th>
<!--                            <th><span ng-show="archivefilter.status !== 'archive'">notify</span></th>-->
                            <th> &nbsp; </th>
                            <th>update</th>
                            <th> &nbsp; </th>
                            <th><span ng-show="archivefilter.status !== 'archive'">archive</span></th>
                            <th><span ng-show="archivefilter.status == 'archive'">unarchive</span></th>
                            <th> &nbsp; </th>
                            <th width='30px'><span ng-show="archivefilter.status !== 'archive'">delete</span></th>
                        </tr>
                        <tr ng-repeat="x in filteredOrdering = (names| filter:searchText | filter: archivefilter) | sizefilter:this | orderBy:predicate:reverse | slicefilter:paginator.getPage():paginator.getRowperPage():paginator.getItemCount() | limitTo:paginator.getRowperPage() track by $index"
                            style='font-family:helvetica;font-size:11px;'>
                            <td ng-repeat="z in tabletitle">
                                <a href ng-if="z.a !== 'warningmsg'" ng-click="view(x)" ng-style="{ 'text-align' : getAlignment(x[z.a]) }">{{x[z.a]| adatereverse:z.c}}</a>
                               <a href ng-if="z.a === 'warningmsg' && x.warningmsg !== '' " popover="{{x.warningmsg}}" data-popover-trigger="mouseenter"><span><i class='glyphicon glyphicon-warning-sign' style='color:orange'></i></span></a></td>
<!--                            <td align='center' width='20'><a href ng-if="archivefilter.status !== 'archive' && x['status'] === 'menu_built'" ng-click="notify($event, x)"><i class="material-icons" style="font-size:20px;color:purple">mail_outline</i></a></td>-->

                            <td width='10'>&nbsp;</td>
                            <td align='center' width='20'><a href ng-if="archivefilter.status !== 'archive'" ng-click="displayupdate($event, x)"><span class='glyphicon glyphicon-pencil blue'></span></a></td>
                            <td width='10'>&nbsp;</td>
                            <td align='center' width='30'><a href ng-if="archivefilter.status !== 'archive'" ng-click="archivemaster($event, x)"><i class="material-icons" style="font-size:20px;color:blue">archive</i></a></td>
                            <td align='center' width='30'><a href ng-if="archivefilter.status == 'archive'" ng-click="unarchivemaster($event, x)"><i class="material-icons" style="font-size:20px;color:blue">unarchive</i></a></td>
                            <td width='10'>&nbsp;</td>
                            <td align='center'><a href ng-if="editdate < x.datetime && archivefilter.status !== 'archive'" ng-click="deleteevent($event, x)"><i class="material-icons" style="font-size:20px;color:red">delete</i></span></a></td>
                            <!--                            <td ng-if ="x.chckflgconfirm"><a href popover="Event not finalized" data-popover-trigger="mouseenter" ><span ><i class ='glyphicon glyphicon-warning-sign' style='color:red'></i></span></a></td>-->
                        </tr>
                        <tr>
                            <td colspan='{{tabletitle.length + 7}}'></td>
                        </tr>
                    </table>
                    <div ng-if="filteredOrdering.length >= paginator.getRowperPage()" class="col-md-12" align="center" style='font-size:11px' ng-include="'inc/paginator.html'"></div>
                </div>

                <div ng-show='createOrderingFlag'>
                    <br/>
                    <a href class='btn btn-info btn-sm customColor' ng-click="backlisting()"><span class='glyphicon glyphicon-step-backward'></span> &nbsp;Back to listing</a>
                    <br/>
                    <div class="col-md-2"></div>
                    <div class="col-md-8">
                        <div class="row" ng-repeat="y in orderCreaContent| filter: {t: '!dontshow' }" style="margin: 0 0 20px 0;font-size:12px;font-familly:Roboto">

                            <div class="input-group" ng-if="y.t === 'input'">
                                <span class="input-group-addon input11"><i class="fa fa-{{y.d}}"></i>&nbsp; {{y.b}}</span>
                                <input type="text" class="form-control input-sm" ng-model="selectedItem[y.a]" ng-change="cleandata(y.a)" ng-readonly="y.r">
                            </div>

                            <div class="input-group" ng-if="y.t === 'inputprice' && showprice === 1">
                                <span class="input-group-addon input11"><i class="fa fa-{{y.d}}"></i>&nbsp; {{y.b}}</span>
                                <input type="text" class="form-control input-sm" ng-model="selectedItem[y.a]" ng-change="cleandata(y.a)" ng-readonly="y.r'">
                            </div>

                            <div class="input-group" ng-if="y.t === 'textarea'">
                                <span class="input-group-addon input11"><i class="fa fa-{{y.d}}"></i>&nbsp; {{y.b}}</span>
                                <textarea class="form-control input-sm" ng-model="selectedItem[y.a]" ng-change="cleandata(y.a)" rows="5"></textarea>
                            </div>

                            <div class="input-group" ng-if="y.t === 'date'">
                                <span class="input-group-btn input11">
                                    <button type="button" class="btn btn-default btn-sm" ng-click="y.u.dateopen($event)"><i class="fa fa-calendar"></i>&nbsp; {{y.b}}</button>
                                </span>
                                <input type="text" class="form-control input-sm" uib-datepicker-popup="{{y.u.formats[0]}}" min-date="y.u.minDate" max-date="y.u.maxDate" ng-model="y.u.originaldate"
                                       is-open="y.u.opened" datepicker-options="y.u.dateOptions" ng-change="y.u.onchange();" close-text="Close" readonly/>
                            </div>

                            <div class="input-group" ng-if="y.t === 'checkbox'">
                                <input type="checkbox" ng-model="selectedItem[y.a]" ng-checked="{{selectedItem[y.a]}}"> &nbsp; {{y.b}}
                            </div>

                            <div class="input-group" ng-if="y.t === 'array'">
                                <div class='input-group-btn' uib-dropdown>
                                    <button type='button' class='btn btn-default btn-sm input12' uib-dropdown-toggle>
                                        <i class="fa fa-{{y.d}}"></i>&nbsp; {{y.b}} <span class='caret'></span></button>
                                    <ul class='dropdown-menu scrollable-menu' uib-dropdown-menu>
                                        <li ng-repeat="p in y.val"><a href ng-click="selectedItem[y.a] = p; y.func()">{{ p}}</a></li>
                                    </ul>
                                </div>
                                <input type='text' ng-model='selectedItem[y.a]' class='form-control input-sm' readonly>
                            </div>

                            <div class="input-group" ng-if="y.t === 'picture'">
                                <div class='input-group-btn' uib-dropdown>
                                    <button type='button' class='btn btn-default btn-sm input12' uib-dropdown-toggle>
                                        &nbsp;<i class="fa fa-{{y.d}}"></i>&nbsp; {{y.b}} <span class='caret'></span>
                                    </button>
                                    <ul class='dropdown-menu scrollable-menu' uib-dropdown-menu>
                                        <li ng-repeat="p in imgEvent"><a href ng-click="selectedItem['picture'] = p;">{{ p}}</a></li>
                                    </ul>
                                </div>
                                <input type='text' ng-model="selectedItem['picture']" class='form-control input-sm' readonly>
                            </div>
                            <div class="input-group" ng-if="y.t === 'pictureshow'">
                                <p ng-if="selectedItem.picture != ''"><img ng-src="{{path}}{{selectedItem.picture}}" height='150'/></p>
                            </div>

                        </div>
                        <br/>
                    </div>
                    <div class="col-md-2"></div>
                    <div class="col-md-3"></div>
                    <div class="col-md-4">
                        <a href ng-if="action !== 'create' && selectedItem.status !== 'cancel'" ng-click="write($event, 'cancel');" class="btn btn-danger btn-sm" style="color:white;width:150px"><i class='glyphicon glyphicon-save'></i> &nbsp;Cancel Project </a><br/>
                    </div>
                    <div class="col-md-1"></div>
                    <div class="col-md-4">
                        <a href ng-click="write($event, action);" class="btn btn-success btn-sm" style="color:white;width:150px"><i class='glyphicon glyphicon-save'></i> &nbsp;{{ buttonlabel}} </a><br/>
                    </div>
                    <br/><br/>
                </div>

                <div ng-show='detailsOrderingFlag'>
                    <br/>
                    <a href class='btn btn-info btn-sm customColor' ng-click="backlisting()"><span class='glyphicon glyphicon-step-backward'></span> &nbsp;Back to listing</a>
                    <br/>
                    <div class="col-md-1">
                        <md-button class="md-warn md-raised md-small" ng-click='buildtncs();' style='width:100px;'><i class="material-icons" style="font-size:15px;">description</i> Setup</md-button>
                    </div>

                    <div class="col-md-1"></div>
                    <div class="col-md-1">
                        <md-button class="md-primary md-raised md-hue-2 md-small" ng-click='buildmenus();' style='width:100px;'><i class="material-icons" style="font-size:15px;">restaurant</i> Food</md-button>
                    </div>

                    <div class="col-md-1"></div>
                    <div class="col-md-1">
                        <md-button class="md-primary md-raised md-hue-2 md-small" ng-click='builddrink();' style='width:100px;'><i class="material-icons" style="font-size:15px;">local_bar</i> Drinks</md-button>
                    </div>

                    <div class="col-md-1"></div>
                    <div class="col-md-1">
                        <md-button class="md-primary md-raised md-hue-2 md-small" ng-click='buildoptions();' style='width:100px;'><i class="material-icons" style="font-size:15px;">playlist_add</i> Options</md-button>
                    </div>

                    <div class="col-md-1"></div>
                    <div class="col-md-1">
                        <md-button md-theme="dark-pink" class="md-primary md-raised md-small" ng-click='editmilestones();' style='width:100px;'><i class="material-icons" style="font-size:15px;">gavel</i> Milestones</md-button>
                    </div>

                    <div class="col-md-1"></div>
                    <div class="col-md-1">
                        <md-button md-theme="dark-pink" class="md-accent md-raised md-small" ng-click='showproject();' style='width:100px;'><i class="material-icons" style="font-size:15px;">explore</i> View</md-button>
                    </div>
                    <div class="col-md-1"></div>

                </div>


                <div class="col-md-12" style="margin-top:20px"></div>
                <div class="col-md-12" ng-show="buildmenusflg">
                    <md-content class="md-padding" >
                        <div ng-if="pendingsave">
                           <label flex-offset="20" flex-xs >Save your work when your are done </label>
                            
                            <md-button   flex-offset="10"  flex-xs  md-theme="green" class="md-primary md-raised md-small " ng-click="savepmenu($event);" style='width:150px;margin-top:20px;'>
                                <i class="material-icons" style="font-size:15px;">save</i> 
                                Save
                            </md-button>
                        </div>
                    </md-content>
                    <md-content class="md-padding" layout-xs="column" layout="row">
                        <div flex-xs flex-gt-xs="50" layout="column">
                            <md-card>
                                <table style="width:100%;font-size:13px;font-family: Roboto">
                                    <tr ng-repeat="u in pmenus| drinkfilter:1 | filter: { status:'!deleted'}">
                                        <td> <!-- ng-if="y.item_title != '' && y.type == 'item'"  -->
                                            <table style="width:100%;">
                                                <tr>
                                                    <td colspan='3' style='font-weight: bold;text-align:center'><span style="font-size:13px;color:indigo;text-transform:uppercase">{{ u.value}} </span><span ng-if="u.extented === 1 && u.price > 0"> <i class="fa fa-{{u.cicon}}"
                                                                                                                                                                                                                                                         style="margin-left:20px;"></i>{{ u.price}}</span>
                                                    </td>
                                                    <td>
                                                    <td style="text-align:right">
                                                        <table>
                                                            <tr>
                                                                <td><a href ng-click="tagaction(u, $event, 'rename');"><i class="material-icons" style="font-size:16px;color:blue;">label</i></a></td>
                                                                <td><a href ng-click="tagaction(u, $event, 'moveup');"><i class="material-icons" style="font-size:16px;color:green;">arrow_upward</i></a></td>
                                                                <td><a href ng-click="tagaction(u, $event, 'delete');"><i class="material-icons" style="font-size:16px;color:red;">delete</i></a></td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                    </td></tr>
                                                <tr>
                                                    <td colspan='3'><br/></td>
                                                </tr>
                                                <tr ng-repeat="v in psubmenus| menufilter:u.menuID:2 | filter: { status:'!deleted'} ">
                                                    <td style="padding:0px 5px;"><strong>{{ v.item_title}}</strong><br/><span style='font-size:10px'>{{ v.item_description}}</span><br/><br/></td>
                                                    <td nowrap valign='top' style='text-align:right'><span ng-show="v.price > 0"><span ng-if="v.ciconflg !== ''"><i class="fa fa-{{v.cicon}}" style="margin-right:-5px;"></i>{{ v.price}}</span><span
                                                                ng-if="v.cicon === ''">{{ v.currency}}{{ v.price}}</span></span><a href ng-if="u.extented !== 1" ng-click="tagaction(v, $event, 'delete');" style="margin-left:20px;color:orange">X</a></td>
                                            </table>
                                        </td>
                                    </tr>
                                </table>

                                <md-card-actions layout="row" layout-align="end center">
                                    <md-menu-bar>
                                        <md-button ng-click="promptpcategory($event, 'food');">
                                            Add category
                                        </md-button>
                                    </md-menu-bar>
                                </md-card-actions>
                            </md-card>
                        </div>
                        <div flex-xs flex-gt-xs="50" layout="column">
                            <md-card>
                                <table style="width:100%;font-size:13px;font-family: Roboto">
                                    <tr ng-repeat="u in menus| drinkfilter:1 | filter: { status:'!deleted'}">
                                 
                                        <td colspan='3'> <!--    -->
                                            <table>
                                                <tr>
                                                    <td colspan='3' style='font-weight: bold;text-align:center' width='200px'><span style="font-size:13px;color:indigo;text-transform:uppercase" ng-click="addallitems($event, u, 'food');">{{ u.value}} </span><span
                                                            ng-if="u.extented === 1 && u.price > 0"><i class="fa fa-{{u.cicon}}" style="margin-left:20px;"></i>{{ u.price}}</span></td>
                                                </tr>
                                                <tr>
                                                    <td colspan='3'><br/></td>
                                                </tr>
                                                <tr ng-repeat="v in submenus| menufilter:u.menuID:1"> <!--  | drinkfilter:1  -->
                                                    <td style="padding:0px 5px;"><strong ng-click="additem($event, v, 'food');">{{ v.item_title}}</strong><br/><span style='font-size:10px'>{{ v.item_description}}</span><br/><br/></td>
                                                    <td nowrap valign='top' style='text-align:right' ng-show="v.price > 0"><span ng-if="v.ciconflg !== ''"><i class="fa fa-{{v.cicon}}" style="margin-right:-5px;"></i>{{ v.price}}</span><span ng-if="v.cicon === ''">{{ v.currency}}{{ v.price}}</span>
                                                    </td>
                                            </table>
                                        </td>
                                    </tr>
                                </table>

                            </md-card>
                        </div>
                    </md-content>
                </div>

                <div class="col-md-12" ng-show="builddrinkflg">
                    <md-content class="md-padding" >
                        <div ng-if="pendingsave">
                           <label flex-offset="20" flex-xs >Save your work when your are done </label>
                            
                            <md-button   flex-offset="10"  flex-xs  md-theme="green" class="md-primary md-raised md-small " ng-click="savepmenu($event);" style='width:150px;margin-top:20px;'>
                                <i class="material-icons" style="font-size:15px;">save</i> 
                                Save
                            </md-button>
                        </div>
                    </md-content>
                    <md-content class="md-padding" layout-xs="column" layout="row">
                        <div flex-xs flex-gt-xs="50" layout="column">

                            <md-card>
                                <table style="width:100%;font-size:13px;font-family: Roboto">
                                    <tr ng-repeat="u in pmenus| drinkfilter:2 | filter: { status:'!deleted'}">
                                        <td> <!-- ng-if="y.item_title != '' && y.type == 'item'"  -->
                                            <table style="width:100%;">
                                                <tr>
                                                    <td colspan='3' style='font-weight: bold;text-align:center'><span style="font-size:13px;color:indigo;text-transform:uppercase">{{ u.value}} </span>
                                                    <td>
                                                    <td style="text-align:right">
                                                        <table>
                                                            <tr>
                                                                <td><a href ng-click="tagaction(u, $event, 'rename');"><i class="material-icons" style="font-size:16px;color:blue;">label</i></a></td>
                                                                <td><a href ng-click="tagaction(u, $event, 'moveup');"><i class="material-icons" style="font-size:16px;color:green;">arrow_upward</i></a></td>
                                                                <td><a href ng-click="tagaction(u, $event, 'delete');"><i class="material-icons" style="font-size:16px;color:red;">delete</i></a></td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                    </td></tr>
                                                <tr>
                                                    <td colspan='3'><br/></td>
                                                </tr>
                                                <tr ng-repeat="v in psubmenus| menufilter:u.menuID:2 | filter: { status:'!deleted'}">
                                                    <td style="padding:0px 5px;"><strong>{{ v.item_title}}</strong><br/><span style='font-size:10px'>{{ v.item_description}}</span><br/><br/></td>
                                                    <td nowrap valign='top' style='text-align:right' ng-show="v.price > 0"><span ng-if="v.ciconflg !== ''"><i class="fa fa-{{v.cicon}}" style="margin-right:-5px;"></i>{{ v.price}}</span><span
                                                            ng-if="v.cicon === ''">{{ v.currency}}{{ v.price}}</span></span><a  href ng-click="tagaction(v, $event, 'delete');" style="margin-left:20px;color:orange">X</a></td>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                                <md-card-actions layout="row" layout-align="end center">
                                    <md-menu-bar>
                                        <md-button ng-click="promptpcategory($event, 'drink');">
                                            Add category
                                        </md-button>
                                    </md-menu-bar>
                                </md-card-actions>
                            </md-card>
                        </div>
                        <div flex-xs flex-gt-xs="50" layout="column">
                            <md-card>
                                <table style="width:100%;font-size:13px;font-family: Roboto">
                                    <tr ng-repeat="u in menus| drinkfilter:2">
                                        <td colspan='3'> <!-- ng-if="y.item_title != '' && y.type == 'item'"  -->
                                            <table>
                                                <tr>
                                                    <td colspan='3' style='font-weight: bold;text-align:center' width='200px'><span style="font-size:13px;color:indigo;text-transform:uppercase" ng-click="addallitems($event, u, 'drink');">{{ u.value}} </span><span ng-if="u.extented === 1 && u.price > 0"><i class="fa fa-{{u.cicon}}" style="margin-left:20px;"></i>{{ u.price}}</span></td>
                                                </tr>
                                                <tr>
                                                    <td colspan='3'><br/></td>
                                                </tr>
                                                <tr ng-repeat="v in submenus| menufilter:u.menuID:1">
                                                    <td style="padding:0px 5px;"><strong ng-click="additem($event, v, 'drink');">{{ v.item_title}}</strong><br/><span style='font-size:10px'>{{ v.item_description}}</span><br/><br/></td>
                                                    <td nowrap valign='top' style='text-align:right' ng-show="v.price > 0"><span ng-if="v.ciconflg !== ''"><i class="fa fa-{{v.cicon}}" style="margin-right:-5px;"></i>{{ v.price}}</span><span ng-if="v.cicon === ''">{{ v.currency}}{{ v.price}}</span>
                                                    </td>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </md-card>
                        </div>
                    </md-content>
                </div>


                <div class="col-md-12" ng-show="buildoptionsflg">
                    <md-content class="md-padding" >
                        <div ng-if="pendingsave">
                           <label flex-offset="20" flex-xs >Save your work when your are done </label>
                            
                            <md-button   flex-offset="10"  flex-xs  md-theme="green" class="md-primary md-raised md-small " ng-click="savepmenu($event);" style='width:150px;margin-top:20px;'>
                                <i class="material-icons" style="font-size:15px;">save</i> 
                                Save
                            </md-button>
                        </div>
                    </md-content>

                    <md-content class="md-padding" layout-xs="column" layout="row">

                        <div flex-xs flex-gt-xs="50" layout="column">
                            <md-card>
        

                                <table style="width:100%;font-size:13px;font-family: Roboto">
                                    <tr ng-repeat="u in pmenus| drinkfilter:3| filter: { status:'!deleted'}"> <!-- ng-if="y.item_title != '' && y.type == 'item'"  -->
                                        <td>
                                            <table style="width:100%;">
                                                <tr>
                                                    <td colspan='3' style='font-weight: bold;text-align:center'>
                                                        <span style="font-size:13px;color:indigo;text-transform:uppercase">{{ u.value}} </span>
                                                    <td>
                                                    <td style="text-align:right">
                                                        <table>
                                                            <tr>
                                                                <td>
                                                                    <a href ng-click="tagaction(u, $event, 'rename');"><i class="material-icons" style="font-size:16px;color:blue;">label</i></a>
                                                                </td>
                                                                <td>
                                                                    <a href ng-click="tagaction(u, $event, 'moveup');"><i class="material-icons" style="font-size:16px;color:green;">arrow_upward</i></a>
                                                                </td>
                                                                <td>
                                                                    <a href ng-click="tagaction(u, $event, 'delete');"><i class="material-icons" style="font-size:16px;color:red;">delete</i></a>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan='3'><br/></td>
                                                </tr>
                                                <tr ng-repeat="v in psubmenus| menufilter:u.menuID:2 | filter: { status:'!deleted'}">
                                                    <td style="padding:0px 5px;">
                                                        <strong>{{ v.item_title}}</strong><br/><span style='font-size:10px'>{{ v.item_description}}</span><br/><br/>
                                                    </td>
                                                    <td nowrap valign='top' style='text-align:right' ng-show="v.price > 0">
                                                        <span ng-if="v.ciconflg !== ''"><i class="fa fa-{{v.cicon}}" style="margin-right:-5px;"></i>{{ v.price}}</span><span ng-if="v.cicon === ''">{{ v.currency}}{{ v.price}}</span></span>
                                                        <a href ng-click="tagaction(v, $event, 'delete');" style="margin-left:20px;color:orange">X</a>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                                <md-card-actions layout="row" layout-align="end center">
                                    <md-menu-bar>
                                        <md-button ng-click="promptpcategory($event, 'options');">
                                            Add category
                                        </md-button>
                                    </md-menu-bar>

                                </md-card-actions>
                            </md-card>
                        </div>
                        <div flex-xs flex-gt-xs="50" layout="column">
                            <md-card>
                                <table style="width:100%;font-size:13px;font-family: Roboto">
                                    <tr ng-repeat="u in menus| drinkfilter:3">
                                        <td colspan='3'> <!-- ng-if="y.item_title != '' && y.type == 'item'"  -->
                                            <table>
                                                <tr>
                                                    <td colspan='3' style='font-weight: bold;text-align:center' width='200px'><span style="font-size:13px;color:indigo;text-transform:uppercase" ng-click="addallitems($event, u, 'options');">{{ u.value}} </span><span ng-if="u.extented === 1 && u.price > 0"><i class="fa fa-{{u.cicon}}" style="margin-left:20px;"></i>{{ u.price}}</span></td>
                                                    <!--<td colspan='3' style='font-weight: bold;text-align:center' width='200px'><span style="font-size:13px;color:indigo;text-transform:uppercase">{{ u.value}} </span></td>-->
                                                </tr>
                                                <tr>
                                                    <td colspan='3'><br/></td>
                                                </tr>
                                                <tr ng-repeat="v in submenus| menufilter:u.menuID:1">
                                                    <td style="padding:0px 5px;"><strong ng-click="additem($event, v, 'options');">{{ v.item_title}}</strong><br/><span style='font-size:10px'>{{ v.item_description}}</span><br/><br/></td>
                                                    <td nowrap valign='top' style='text-align:right' ng-show="v.price > 0"><span ng-if="v.ciconflg !== ''"><i class="fa fa-{{v.cicon}}" style="margin-right:-5px;"></i>{{ v.price}}</span><span ng-if="v.cicon === ''">{{ v.currency}}{{ v.price}}</span>
                                                    </td>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </md-card>
                        </div>
                    </md-content>
                </div>


                <div ng-show='milestonesflg'>
                    <md-content class="md-padding" layout-xs="column" layout="row">
                        <div flex-xs flex-gt-xs="50" layout="column">
                            <h3> Milestones </h3>
                            <h5>Lead Time</h5>
                            <md-input-container>
                                <label>Number of days</label>
                                <input name="leadtime" ng-model="selectedItem.more.leadtime" ng-maxlength="10" style="font-size:12px;"></input>
                            </md-input-container>
                            <md-input-container ng-repeat="u in milestonetitle" style="margin: 0 0 0 0">
   
                                <md-switch  class="md-primary" md-no-ink aria-label="Switch No Ink" ng-change="switchStatus(u.c,selectedItem.milestones[u.a]);" ng-model="selectedItem.milestones[u.a]"> {{ u.b }}</md-switch>
                                <span  ng-show="selectedItem.status ==='menu_built' && u.a === 'buildmenu'">
                                      <md-button md-theme="blue" class="md-primary md-raised md-small" ng-click="notify($event, selectedItem)" style='width:150px;margin-top:20px;'> Notify Event Details</md-button>
                                </span>
                            </md-input-container> 
                               <label  ng-show="selectedItem.status ==='confirmed'"  style="margin-left:50px;padding-left :20px !important;">
<!--                                    <input type="button" ng-model="notifydetails" ng-click="evnotify($event)" > Notify event Details-->
                                   <md-button md-theme="blue" class="md-primary md-raised md-small" ng-click="evnotify($event);" style='width:150px;margin-top:20px;'> Notify Event Details</md-button>
                                </label>
                           
                            <md-button md-theme="dark-red" class="md-primary md-raised md-small" ng-click="write($event, 'update');" style='width:150px;margin-top:20px;'><i class="material-icons" style="font-size:15px;">download</i> Save</md-button>
                        </div>
                    </md-content>
                </div>

                <div class="col-md-12" ng-show="buildtncsflg">
                    <md-content class="md-padding" layout-xs="column" layout="row">
                        <div flex-sm="80" layout="column">

                            <h4>Set up</h4>
                            <md-input-container>
                                <textarea name="setup" ng-model="selectedItem.setup" rows="6" required ng-maxlength="550" style="font-size:12px;"></textarea>
                            </md-input-container>

                            <h4>Billing</h4>
                            <md-input-container>
                                <textarea name="billing" ng-model="selectedItem.billing" rows="6" required ng-maxlength="550" style="font-size:12px;"></textarea>
                            </md-input-container>

                            <h4>Cancel Tnc</h4>
                            <md-input-container>
                                <textarea name="tncc" ng-model="selectedItem.tnc_cancel" rows="6" required ng-maxlength="550" style="font-size:12px;"></textarea>
                            </md-input-container>

                            <h4>Pricing</h4>
                            <md-input-container>
                          
                                 <md-checkbox ng-model="selectedItem.minimumprice" aria-labelledby="Minimum price ">Minimum price </md-checkbox>  
                                 <input name="tprice" ng-if='selectedItem.minimumprice === true' ng-model="selectedItem.more.minimumprice" required ng-maxlength="10" style="font-size:12px;"> </input>
                            </md-input-container>
                             <h4>Deposit(%)</h4>
                              <md-input-container>
                                <md-select ng-model="selectedItem.more.deposit" style="font-size:12px;">
                                  <md-option ng-value="opt" ng-repeat="opt in deposit">{{ opt }}</md-option>
                                </md-select>
                              </md-input-container>


                            <h4>Internal Event Id</h4>
                            <md-input-container>
                                <label>Internal Event Id</label>
                                <input name="beo" ng-model="selectedItem.more.beo" ng-maxlength="100" style="font-size:12px;"></input>
                            </md-input-container>

<!--                            <h4>Lead Time</h4>
                            <md-input-container>
                                <label>Number of days</label>
                                <input name="leadtime" ng-model="selectedItem.more.leadtime" ng-maxlength="10" style="font-size:12px;"></input>
                            </md-input-container>-->

                            <h4>Payment</h4>
                              
                            <div layout="row"  ng-if ="selectedItem.more.deposit != 'no deposit' " flex ng-repeat="p in payment_method track by $index" >
                                <md-input-container>
                                <md-checkbox ng-model="selectedItem.payment_method['creditcard']" aria-labelledby="Credit Card">Credit Card</md-checkbox>  
                                </md-input-container>
                                <md-input-container>
                                    <md-checkbox ng-model="selectedItem.payment_method['banktransfer']" aria-labelledby="Bank Transfer">Bank Transfer</md-checkbox>  
                                </md-input-container>
                                <md-input-container>
                                    <md-checkbox ng-model="selectedItem.payment_method['manual']" aria-labelledby="Manual">Manual</md-checkbox> 
                                </md-input-container>
                            </div>


                            <h4>Media</h4>
                            <div layout="row" flex ng-repeat="media in selectedItem.more.media track by $index">

                                <md-input-container flex-sm="80">
                                    <label>Media</label>
                                    <md-select ng-model="media.name" style="font-size:12px;">
                                        <md-option><em></em></md-option>
                                        <md-option ng-repeat="img in eventimages" ng-value="img" ng-disabled="$index === 1"> {{img}}</md-option>
                                    </md-select>
                                </md-input-container>
                                <md-input-container flex-sm="5">
                                    <md-button ng-if="$last" class="md-fab md-mini md-primary" aria-label="add timing" ng-click="addmultiple(selectedItem, 'media');">
                                        <i class="material-icons" style="font-size:15px;">add</i>
                                    </md-button>
                                </md-input-container>
                                <md-input-container flex-sm="5">
                                    <md-button ng-if="$last === false" class="md-fab md-mini md-warning" aria-label="add timing" ng-click="clearmultiple(selectedItem, $index, 'media');">
                                        <i class="material-icons" style="font-size:15px;">remove</i>
                                    </md-button>
                                </md-input-container>
                            </div>

<!--                            <h4>Additional or Restriction</h4>
                            <div layout="row" flex ng-repeat="aa in selectedItem.more.additional track by $index">

                                <md-input-container flex-sm="80">
                                    <label>Addtitional/Restrictions</label>
                                    <textarea ng-model="aa.name" rows="6" required ng-maxlength="550" style="font-size:12px;"></textarea>
                                </md-input-container>
                                <md-input-container flex-sm="5">
                                    <md-button ng-if="$last" class="md-fab md-mini md-primary" aria-label="add timing" ng-click="addmultiple(selectedItem, 'additional');">
                                        <i class="material-icons" style="font-size:15px;">add</i>
                                    </md-button>
                                </md-input-container>
                                <md-input-container flex-sm="5">
                                    <md-button ng-if="$last === false" class="md-fab md-mini md-warning" aria-label="add timing" ng-click="clearmultiple(selectedItem, $index, 'additional');">
                                        <i class="material-icons" style="font-size:15px;">remove</i>
                                    </md-button>
                                </md-input-container>
                            </div>-->

                            <h4>Timing</h4>
                            <div layout="row" flex ng-repeat="time in selectedItem.more.timing track by $index">

                                <md-input-container flex-sm="10">
                                    <label>Time</label>
                                    <input name="name" ng-model="time.name" required ng-maxlength="20" style="font-size:12px;"></input>
                                </md-input-container>

                                <md-input-container flex-sm="50">
                                    <label>Function</label>
                                    <input name="funct" ng-model="time.function" required ng-maxlength="40" style="font-size:12px;"></input>
                                </md-input-container>

                                <md-input-container flex-sm="10">
                                    <label>Venue</label>
                                    <input name="venue" ng-model="time.venue" ng-maxlength="12" style="font-size:12px;"></input>
                                </md-input-container>

                                <md-input-container flex-sm="5">
                                    <label>Guaranteed</label>
                                    <input name="guarented" ng-model="time.guarented" ng-maxlength="5" style="font-size:12px;"></input>
                                </md-input-container>

                                <md-input-container flex-sm="5">
                                    <label>Expected</label>
                                    <input name="expected" ng-model="time.expected" ng-maxlength="5" style="font-size:12px;"></input>
                                </md-input-container>

                                <md-input-container flex-sm="5">
                                    <md-button ng-if="$last" class="md-fab md-mini md-primary" aria-label="add timing" ng-click="addmultiple(selectedItem, 'timing');">
                                        <i class="material-icons" style="font-size:15px;">add</i>
                                    </md-button>
                                </md-input-container>
                                <md-input-container flex-sm="5">
                                    <md-button ng-if="$last === false" class="md-fab md-mini md-warning" aria-label="add timing" ng-click="clearmultiple(selectedItem, $index, 'timing');">
                                        <i class="material-icons" style="font-size:15px;">remove</i>
                                    </md-button>
                                </md-input-container>
                            </div>
                            
                            <h4>Private Note</h4>
                             <md-input-container>
                                <label>private note</label>
                                <textarea name="private-note" ng-model="selectedItem.more.privatenote" rows="6"  ng-maxlength="1000" style="font-size:12px;"></textarea>
                            </md-input-container>
                            <h4>Additional  Note</h4>
                             <md-input-container>
                                <label>additional note</label>
                                <textarea name="additional-note" ng-model="selectedItem.more.additionalnote" rows="6"  ng-maxlength="1000" style="font-size:12px;"></textarea>
                            </md-input-container>
                            <md-button md-theme="dark-red" class="md-primary md-raised md-small" ng-click="write($event, 'update');" style='width:150px;margin-top:20px;'><i class="material-icons" style="font-size:15px;">download</i> Save</md-button>
                        </div>
                    </md-content>
                </div>

                <div class="col-md-8"></div>
                <div class="col-md-4"></div>
                <div class="col-md-12"><br/>
                    <hr/>
                    <br/><br/></div>
            </div>

        </div>
    </div>
</div>
<script src="inc/EventOrderingSystem/EventOrderingSystem.js"></script>