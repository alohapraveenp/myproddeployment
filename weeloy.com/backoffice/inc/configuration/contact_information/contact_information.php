
<?php

    $res = new WY_restaurant;
    $res->getRestaurant($theRestaurant);
    $smsnotify = ($res->smsNotifToRestaurant() > 0) ? 1 : 0;
    $smsheader = ($res->checksmsheader() > 0) ? 1 : 0;

?>
<div class="container">
    <div class="row">
        <div class="col-md-12 left-sec">

            <div ng-controller="ContactInformationCtrl" >
                <input type='hidden' id='restaurant' value ="<?php echo $theRestaurant ?>" />
                <input type='hidden' id='email' value ="<?php echo $_SESSION['user_backoffice']['email'] ?>" />
                <input type='hidden' id='smsnotify' value ="<?php echo $smsnotify ?>" />
                <input type='hidden' id='smsheader' value ="<?php echo $smsheader ?>" />

                <div id='listing' ng-show='!editModeDisplay'>
                    <div class="form-group">
                        <div class="col-md-4">
                            <div class="input-group col-md-4">
                                <span class="input-group-addon">
                                    <i class="glyphicon glyphicon-search"></i>
                                </span>
                                <input type="text" class="form-control input-sm" ng-model="searchText" ng-change="paginator.setPage(0);" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" style='width:200px;'> 
                            </div>
                        </div>
                        <div class="col-md-2"></div>
                        <div class="col-md-4">
                            <a href ng-click="switchview('edit','');" class="btn btn-success btn-sm" style='color:white;width:200px;'><span class='glyphicon glyphicon-certificate'></span> &nbsp;Create a New Contact</a>
                        </div>
                    </div>
                    <table width='100%' class="table table-condensed table-striped" style='font-size:12px;'>
                        <thead>
                            <tr>
                                <th ng-repeat="y in tabletitle"><tbtitle var="{{y.a}}" name='tabletitle' module='moduleName'/></th>
                        <th>update</th><th> &nbsp; </th>
                        <th>delete</th>
                        </tr>
                        </thead>
                        <tbody style='font-family:helvetica;font-size:12px;'>
<!--                         {{tabletitle}}
                        {{contacts | json }} -->
                            <tr ng-repeat="x in contacts = (contacts | filter:searchText | orderBy:predicate:reverse)" style='font-family:helvetica;font-size:11px;'>
                                <!-- {{ x }} -->
                                <td ng-repeat="y in tabletitle">
                                  {{ x[y.a] | adatereverse:y.c }}
                                </td>
                                <td><a href ng-click="switchview('update', x)" style='color:blue;'><span class='glyphicon glyphicon-pencil'></a></span></td>
                                <td width='30'>&nbsp;</td>
                                <td nowrap><a href ng-click="deletecontact(x)" style='color:red;'><span class='glyphicon glyphicon-trash'></a></span></td>
                            </tr><tr><td colspan='{{tabletitle.length + 3}}'></td></tr>
                        </tbody>
                    </table>
                    <div ng-if="filteredBlock.length >= paginator.getRowperPage()" class="col-md-12" align="center" style='font-size:11px' ng-include="'inc/paginator.html'"></div>
                </div>


                <div class="col-md-12" ng-show='editModeDisplay'>

                    <a href class='btn btn-info btn-sm customColor' ng-click="switchview('list','')"><span class='glyphicon glyphicon-step-backward'></span> &nbsp;Back to listing</a>
                    <br />
                    <div class="col-md-2"></div>
                    <div class="col-md-8">
    
                    <!-- hack to close the previous form    -->
                    </form>
                    <!-- hack to close the previous form    -->
                    <form ng-submit="savecontact();" name="MyForm">
                   
                        <div class="row" ng-repeat="y in tabletitleContent| filter: {a: '!value' }" style="margin: 0 0 20px 0;font-size:12px;font-familly:Roboto">
                            <div class="input-group" ng-if="y.t === 'input'">
                                <span class="input-group-addon input11"><i class="glyphicon glyphicon-{{y.d}} input14"></i>&nbsp; {{y.b}}</span>
                                <input type="text" class="form-control input-sm" ng-model="selectedItem[y.a]" ng-change="cleaninput(y.a)" ng-readonly="y.a === 'name' && action === 'update'" >
                            </div> 

                            <div class="input-group" ng-if="y.t === 'textarea'">
                                <span class="input-group-addon input11"><i class="glyphicon glyphicon-{{y.d}} input14"></i>&nbsp; {{y.b}}</span>
                                <textarea class="form-control input-sm" ng-model="selectedItem[y.a]" rows="5" ng-change="cleaninput(y.a)" ></textarea>
                            </div>
                             <div class="input-group" ng-if="y.t === 'smscheck'" >
                                <label style="font-size:12px;margin-left:10px;"><input type="checkbox" ng-true-value="'1'" ng-false-value="'0'"  ng-checked="'{{selectedItem[y.a]}}'"  ng-model="selectedItem[y.a]"/> {{y.b}} </label>
                            </div>

<!--                            <div class="input-group" ng-if="y.t === 'checkbox1'">
                                <span class="">{{y.b}}</span>
                                <input type="checkbox" class="form-control input-sm" ng-model="selectedItem[y.a]" ng-true-value="'1'" ng-false-value="'0'"  ng-checked="'{{selectedItem[y.a]}}'"  ></textarea>
                            </div>-->
                              <div class="input-group" ng-if="y.t === 'checkbox' && smsnotify ==='1'" >
                                  <label style="font-size:12px;margin-left:10px;"><input type="checkbox" ng-true-value="'1'" ng-false-value="'0'"  ng-checked="'{{selectedItem[y.a]}}'"  ng-model="selectedItem[y.a]"/> {{y.b}} </label>


                            </div>
                        </div>
                    
                        <div class="col-md-4">
                            <input type="submit" id="submit" value="Submit" />
<!--                            //<a href ng-click='savecontact();' class="btn btn-success btn-sm" style='color:white;width:200px;'><span class='glyphicon glyphicon-save'></span> Save </a>-->
                        </div>
                </form>
                    </div>
                    <div class="col-md-2"></div>
                </div>

            </div>
        </div>
    </div>
</div>

<script src="inc/configuration/contact_information/contactInformationController.js"></script>

<script src="inc/configuration/contact_information/contactInformationService.js"></script>