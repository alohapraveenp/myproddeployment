<?php
$res = new WY_restaurant;
$res->getRestaurant($theRestaurant);
$multProduct = ($res->mutipleProdAllote()) ? $res->dfproduct : "";
?>
<div class="container">
    <div class="row">
        <div class="col-md-12 left-sec">
<div ng-controller="AvailabilityController" ng-init="listBlockFlag = true; viewBlockFlag=false; createBlockFlag=false;" >
	<input type='hidden' id='token' value ="<?php echo $_SESSION['user_backoffice']['token'] ?>" />
	<input type='hidden' id='email' value ="<?php echo $_SESSION['user_backoffice']['email'] ?>" />
	<input type='hidden' id='multiproduct' value ="<?php echo $multProduct ?>"/>
	<input type='hidden' id='restaurant' value ="<?php echo $theRestaurant ?>" />
	<div id='listing' ng-show='listBlockFlag'>
		<div class="form-group"  style='margin-bottom:25px;'>
			<div class="col-md-4">
				<div class="input-group">
					<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>
					<input type="text" class="form-control input-sm" ng-model="searchText" ng-change="paginator.setPage(0);" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" style='width:200px;'> 
				</div>
			</div>
			<div class="col-md-4">
			<div ng-if="multProd.length > 1">
			<button type="button" class="btn  btn-warning btn-sm" uib-dropdown-toggle aria-expanded="false" style='color:white;'><span class='glyphicon glyphicon-list'></span>&nbsp;&nbsp; Select Product &nbsp;&nbsp; <span class="caret"></span></button><span style="font-size:11px;font-weight:bold;">&nbsp;  [ {{bkproduct}} ]</span>
			<ul class="dropdown-menu" uib-dropdown-menu role="menu">
			<li ng-repeat="x in multProd"><a href ng-click="setalloteproduct(x);">{{x}}  <i ng-show='bkproduct === x' class="glyphicon glyphicon-ok text-primary"></i></a></li>
			</ul>
			</div>
			</div>
			<div class="col-md-4">
			<a href ng-click='create();' class="btn btn-success btn-sm" style='color:white;width:200px;'><span class='glyphicon glyphicon-certificate'></span> &nbsp;Create a New Block</a>
			</div>
		</div>
        <div style=" clear: both;"></div>

		<table width='100%' class="table table-condensed table-striped" style='font-size:12px;'>
			<thead>
				<tr>
				<th ng-repeat="y in tabletitle"><tbtitle var="{{y.a}}" name='tabletitle'/></th>
				<th>update</th><th> &nbsp; </th><th>delete</th>
				</tr>
			 </thead>
			<tbody style='font-family:helvetica;font-size:12px;'>
				<tr ng-repeat="x in filteredBlock = (names | filter:searchText) | sizefilter:this | orderBy:predicate:reverse | slicefilter:paginator.getPage():paginator.getRowperPage():paginator.getItemCount() | limitTo:paginator.getRowperPage()" style='font-family:helvetica;font-size:11px;'>
					<td ng-repeat="y in tabletitle"><a href ng-click="view(x)">{{ x[y.a] | adatereverse:y.c }}</a></td>
					<td align='center'><a href ng-click="update(x)"><span class='glyphicon glyphicon-pencil blue'></span></a></td>
					<td width='30'>&nbsp;</td>
					<td align='center'><a href ng-click="delete(x)"><span class='glyphicon glyphicon-trash red'></span></a></td>
				</tr><tr><td colspan='{{tabletitle.length + 3}}'></td></tr>
			</tbody>
		</table>
		<div ng-if="filteredBlock.length >= paginator.getRowperPage()" class="col-md-12" align="center" style='font-size:11px' ng-include="'inc/paginator.html'"></div>
	</div>
	<div class="col-md-12" ng-show='viewBlockFlag'>
		<br /><a href class='btn btn-info btn-sm customColor' ng-click="backlisting()"><span class='glyphicon glyphicon-step-backward'></span> &nbsp;Back to listing</a><br />
		<table class='table-striped' style="margin: 0 0 150px 30px;font-size:13px;font-family: Roboto">		
			<tr ng-repeat="y in tabletitleContent | filter: {b: '!picture'}"><td nowrap ><strong> {{ y.b }} </strong></td><td width='30'> &nbsp; </td><td>{{ selectedItem[y.a] | adatereverse:y.c }} </td></tr>
		</table>
	</div>
	<div class="col-md-12" ng-show='createBlockFlag'>
		<br />
		<a href class='btn btn-info btn-sm customColor' ng-click="backlisting()"><span class='glyphicon glyphicon-step-backward'></span> &nbsp;Back to listing</a>
		<br />
		 <div class="col-md-2"></div>
		 <div class="col-md-8">
		 	<p ng-if='false'>
		   	<label>
		    <input type="radio" ng-model="type" value="Inventory" ng-change='typeChanged()'>
		    Inventory
		  	</label>
		  	&nbsp;&nbsp;
		   	<label>
		  	<input type="radio" ng-model="type" value="Time" ng-change='typeChanged()'>
		    Booking Hour
		  	</label>
		  	</p>
		   <div class="row" ng-repeat="y in tabletitleContent | filter: {t: '!dontshow' }" style="margin: 10px 0 20px 0;font-size:12px;font-familly:Roboto">
			<div class="input-group" ng-if="y.t === 'input' && (y.a !== 'tablepax' || type !== 'Time')">
				<span class="input-group-addon input11"><i class="glyphicon glyphicon-{{y.d}} input14"></i>&nbsp; {{y.b }}</span>
				<input type="text" class="form-control input-sm" ng-model="selectedItem[y.a]" ng-change="cleaninput(y.a)" ng-readonly="y.a === 'slots' || (y.a === 'tablepax' && type === 'Time')" >
			</div> 
			<div class="input-group" ng-if="y.t === 'textarea'">
				<span class="input-group-addon input11"><i class="glyphicon glyphicon-{{y.d}} input14"></i>&nbsp; {{y.b}}</span>
				<textarea class="form-control input-sm" ng-model="selectedItem[y.a]" ng-change="cleaninput(y.a)" rows="5" ></textarea>
			</div>
			<div class="input-group" ng-if="y.t === 'date'">
				<span class="input-group-btn input11">
				<button type="button" class="btn btn-default btn-sm" ng-click="y.u.dateopen($event)"><i class="glyphicon glyphicon-calendar input13"></i>&nbsp; {{y.b}}</button>
				</span>
				<input type="text" class="form-control input-sm" uib-datepicker-popup="{{y.u.formats[0]}}" min-date="y.u.minDate" max-date="y.u.maxDate" ng-model="y.u.originaldate"
				is-open="y.u.opened" datepicker-options="y.u.dateOptions" ng-change="y.u.onchange();" close-text="Close" readonly />
			</div>
            <div class="input-group" ng-if="y.t === 'array'">
				<div class='input-group-btn' uib-dropdown >
				<button type='button' class='btn btn-default btn-sm input11' uib-dropdown-toggle >
					<i class="glyphicon glyphicon-{{y.d}} input13"></i>&nbsp; {{y.b}}<span class='caret'></span></button>
				<ul class='dropdown-menu scrollable-menu' uib-dropdown-menu >
				<li ng-repeat="p in y.val"><a href ng-click="selectedItem[y.a]=p;y.func()">{{ p }}</a></li>
				</ul>
				</div>
				<input type='text' ng-model='selectedItem[y.a]' class='form-control input-sm' readonly >
			</div>
			<div class="input-group" ng-if="y.t === 'picture'">
				<div class='input-group-btn' uib-dropdown>
					<button type='button' class='btn btn-default btn-sm input11' uib-dropdown-toggle >
						&nbsp;<i class="glyphicon glyphicon-{{y.d}} input13"></i>&nbsp; {{y.b}} <span class='caret'></span>
					</button>
					<ul class='dropdown-menu scrollable-menu' uib-dropdown-menu >
						<li ng-repeat="p in imgBlock"><a href ng-click="selectedItem['picture'] = p;">{{ p }}</a></li>
					</ul>
				</div>
				<input type='text' ng-model="selectedItem['picture']" class='form-control input-sm' readonly >
			</div>
		   </div><br />
		</div>
		<div class="col-md-2"></div>
		<div class="col-md-7"></div>
		<div class="col-md-5">
			<a href ng-click='savenewblock();' class="btn btn-success btn-sm" style="color:white;width:150px"><i class='glyphicon glyphicon-save'></i> &nbsp;{{ buttonlabel }} </a><br />
		</div>
		</div>
</div>
</div>
</div>
</div>
<script src="inc/Availability/AvailabilityController.js"></script>