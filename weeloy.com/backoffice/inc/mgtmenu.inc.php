<div class="container">
    <div class="row">
        <div class="col-md-12 left-sec">

<div id="Menubackoffice" ng-controller="MenuNewController" ng-init="moduleName='menu'; listingMenuFlag = true; detailsMenuFlag=false; createMenuFlag=false;" >

	<div id='listing' ng-show='listingMenuFlag'>
		<div class="form-group"  style='margin-bottom:25px;'>
			<div class="col-md-4">
				<div class="input-group col-md-4">
					<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>
					<input type="text" class="form-control input-sm" ng-model="searchText" ng-change="paginator.setPage(0);" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" style='width:200px;'> 
				</div>
			</div>
			<div class="col-md-2"></div>
			<div class="col-md-4">
			<a href ng-click='create();' class="btn btn-success btn-sm" style='color:white;width:200px;'><span class='glyphicon glyphicon-certificate'></span> &nbsp;Create a New Menu</a>
			</div>
		</div><br/>

		<table width='100%' class="table table-condensed table-striped" style='font-family:helvetica;font-size:12px;'>
			<thead>
				<tr>
				<th ng-repeat="y in tabletitle"><tbtitle var="{{y.a}}" name='tabletitle' module='moduleName'/></th>
				<th  align='center' width='25'>update</th><th width='10'> &nbsp; </th><th  align='center' width='25'>delete</th>
				</tr>
			 </thead>
				<tr ng-repeat="x in filteredMenu = (names | filter:searchText) | sizefilter:this | orderBy:predicate:reverse | slicefilter:paginator.getPage():paginator.getRowperPage():paginator.getItemCount() | limitTo:paginator.getRowperPage()" style='font-family:helvetica;font-size:11px;'>
					<td ng-repeat="z in tabletitle"><a href ng-click="view(x)"><span ng-if="z.a !== 'private'">{{ x[z.a]}}</span><span ng-if="z.a === 'private' && x.private === 1" class='glyphicon glyphicon-ok green'></span></a></td>
					<td align='center'><a href ng-click="update(x)"><span class='glyphicon glyphicon-pencil blue'></span></a></td>
					<td width='10'>&nbsp;</td>
					<td align='center'><a href ng-click="delete(x)"><span class='glyphicon glyphicon-trash red'></span></a></td>
				</tr>
	            <tr><td colspan='{{tabletitle.length + 5}}'></td></tr>
		</table>
		<div ng-if="filteredMenu.length >= paginator.getRowperPage()" class="col-md-12" align="center" style='font-size:11px' ng-include="'inc/paginator.html'"></div>
	</div>

	<div class='col-md-12' ng-show='detailsMenuFlag'>
		<br />
		<a href class='btn btn-info btn-sm customColor' ng-click="backlisting()"><span class='glyphicon glyphicon-step-backward'></span> &nbsp;Back to listing</a>
		<br />
		<table class='table-striped' style="width:100%;font-size:13px;font-family: Roboto">		
			<tr ng-repeat="y in menuContent"><td class="strong truncate" style="width:120px">{{ y.b }}: </td><td width='30'> &nbsp; </td><td class="strong" style="text-align:left;">{{ selectedItem[y.a] }}</td></tr>
			<tr><td class="strong">Menu Items: </td><td> &nbsp; </td><td>  &nbsp;  </td></tr>
            <tr ng-repeat="y in itemList track by $index" ng-if="y.item_title != '' && y.type == 'item'" ><td colspan='3'>
				<table>
					<tr>
					<td width='100' align='right' nowrap><span ng-repeat="z in itemListFlg" ng-if="y[z.label] == '1'"><img ng-src='../images/restaurant_icons/{{z.label}}.png' width='{{z.width}}'> </span></td>
					<td width='10'></td><td class="strong"  width='500'>{{ y.item_title }}</td><td width='10'>&nbsp;</td><td></td>
					</tr>
					<tr><td></td><td></td><td>{{ y.item_description }}</td><td></td><td>
						<span ng-if="y.price > 0" class="strong" style="font-size:12px;">
							<span ng-if="y.ciconflg !==''"><i class="fa fa-{{y.cicon}}"></i></span><span ng-if="y.cicon === ''">{{ y.currency }}</span>{{ y.price }}
						</span>
					</td></tr>
				</table>
                </td></tr>
		</table>
	</div>

	<div ng-show='createMenuFlag'>
		<br />
		<a href class='btn btn-info btn-sm customColor' ng-click="backlisting()"><span class='glyphicon glyphicon-step-backward'></span> &nbsp;Back to listing</a>
		<br />
		 <div class="col-md-2"></div>
		 <div class="col-md-8">			
				<div ng-repeat="y in menuContent | filter:testExtented()" style="margin: 0 0 20px 0;font-size:12px;font-familly:Roboto" class="row">

				<div class="input-group" ng-if="y.t === 'input'">
					<span class="input-group-addon input11"><i class="fa fa-{{y.d}}"></i>&nbsp; {{y.b}}</span>
					<input type="text" class="form-control input-sm" ng-model="selectedItem[y.a]" ng-change="cleaninput(y.a)"> 
				</div>

				<div class="form-group" ng-if="y.t === 'checkbox'" style="margin: 0 0 -15px 0;">
					<label class="checkbox-inline">
					<input type="checkbox" ng-model="selectedItem[y.a]"  ng-true-value="1" ng-false-value="0" ng-if="y.t === 'checkbox'";> {{y.b}}
					</label>
				</div>
				
				<div class="input-group" ng-if="y.t === 'array'">
					<div class='input-group-btn' uib-dropdown >
					<button type='button' class='btn btn-default btn-sm input12' uib-dropdown-toggle >
						<i class="fa fa-{{y.d}}"></i>&nbsp; {{y.b}} <span class='caret'></span></button>
					<ul class='dropdown-menu scrollable-menu' uib-dropdown-menu >
					<li ng-repeat="p in y.val"><a href ng-click="selectedItem[y.a]=p;y.func()">{{ p }}</a></li>
					</ul>
					</div>
					<input type='text' ng-model='selectedItem[y.a]' class='form-control input-sm' readonly >
				</div>
				
				</div><br />
                                
				<h3><center>Menu Items Description</center></h3>
				
				<div ng-repeat="x in itemList track by $index">
				<div ng-show="x.status !== 'delete'">

				<div ng-if="x.item_description ==='section' || x.item_description ==='section_or'">
				<div ng-repeat="y in itemSection" style="margin: 0 0 20px 0;font-size:12px;font-familly:Roboto" class="row">
				<div class="input-group" ng-if="y.t === 'input'">
					<span class="input-group-addon input11"><i class="fa fa-{{y.d}}"></i>&nbsp; {{y.b}}</span>
					<input type="text" class="form-control input-sm" ng-model="x[y.a]" ng-change="x[y.a]=cleansubinput(x[y.a]);"> 
				</div>
				
				<div class="input-group" ng-if="y.t === 'array'">
					<div class='input-group-btn' uib-dropdown >
					<button type='button' class='btn btn-default btn-sm input12' uib-dropdown-toggle >
						<i class="fa fa-{{y.d}}"></i></i>&nbsp; {{y.b}} <span class='caret'></span></button>
					<ul class='dropdown-menu scrollable-menu' uib-dropdown-menu >
					<li ng-repeat="p in y.val"><a href ng-click="x[y.a]=p;y.func()">{{ p }}</a></li>
					</ul>
					</div>
					<input type='text' ng-model='x[y.a]' class='form-control input-sm' readonly >
				</div>
				
				</div>
				<p ng-if="x.item_description ==='section'" style="font-size:10px;margin-top:-15px;">simple section</p><p ng-if="x.item_description !=='section'" style="font-size:10px;margin:-15px;">multiple choice section</p>
				</div>
				
				<div ng-if="x.item_description !=='section' && x.item_description !=='section_or'">
				<div ng-repeat="y in itemContent | filter: {t: '!dontshow' }" style="margin: 0 0 20px 0;font-size:12px;font-familly:Roboto" class="row">
				<div class="input-group" ng-if="y.t === 'input'">
					<span class="input-group-addon input11"><i class="fa fa-{{y.d}}"></i>&nbsp; {{y.b}}</span>
					<input type="text" class="form-control input-sm" ng-model="x[y.a]" ng-change="x[y.a]=cleansubinput(x[y.a]);"> 
				</div>
				
				<div class="input-group" ng-if="y.t === 'array'">
					<div class='input-group-btn' uib-dropdown >
					<button type='button' class='btn btn-default btn-sm input12' uib-dropdown-toggle >
						<i class="fa fa-{{y.d}}"></i></i>&nbsp; {{y.b}} <span class='caret'></span></button>
					<ul class='dropdown-menu scrollable-menu' uib-dropdown-menu >
					<li ng-repeat="p in y.val"><a href ng-click="x[y.a]=p;y.func()">{{ p }}</a></li>
					</ul>
					</div>
					<input type='text' ng-model='x[y.a]' class='form-control input-sm' readonly >
				</div>
				</div><br />

				<div style="margin: 0 0 20px 0;font-size:12px;font-familly:Roboto" class="row" class="row">
				<div class="input-group">
					<table width='100%' style='padding: 18px;margin: 10px;font-size: 12;'>
						<tr><td> &nbsp;</td><td> &nbsp;</td><td rowspan='7'>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</td><td rowspan='9' style="vertical-align:middle">
						<div class='input-group'>
						<div class='input-group-btn' uib-dropdown >
							<button type='button' class='btn btn-default btn-sm' uib-dropdown-toggle >
								&nbsp;<i class="glyphicon glyphicon-picture"></i>&nbsp; Select an image <span class='caret'></span>
							</button>
							<ul class='dropdown-menu scrollable-menu' uib-dropdown-menu>
								<li ng-repeat="p in imageobject"><a href ng-click="x.mimage = p;">{{ p }}</a></li>
							</ul></div>
						<input type='text' ng-model='x.mimage' class='form-control input-sm' readonly >
						</div>
						<br />
						<p ng-if="x.mimage != ''"><img ng-src="{{path}}{{x.mimage}}" height='70'/></p>
						</td></tr>
						<tr ng-repeat="z in itemListFlg">
						<td class="strong"><img ng-src='../images/restaurant_icons/{{z.label}}.png' width='{{z.width}}'>  {{z.label}} </td>
						<td><input type="checkbox" ng-model="x[z.label]" ng-true-value="'1'" ng-false-value="'0'"></td>						
						<tr>
					</table>
				</div></div><br />
				</div>

				<p align="right"><a href ng-click='deleteitem(x);' class="btn btn-warning btn-sm" style='color:white;width:100px;'><span class='glyphicon glyphicon-minus'></span> &nbsp;delete item </a></p><hr />
				</div>
				</div>
				 <div ng-if="selectedItem.extented !== 1"><a href='javascript:;' ng-click='onemore();' class="btn btn-info btn-sm" style='color:white;'><span class='glyphicon glyphicon-plus'></span> &nbsp;one more </a></div>
				 <div ng-if="selectedItem.extented === 1">
				 	<table><tr>
				 		<td><a ng-click='onemore();' class="btn btn-info btn-sm" style='color:white;width:200px;'><span class='glyphicon glyphicon-plus'></span> &nbsp;Add item </a></td><td></td>
				 		<td><a ng-click='onemore(1);' class="btn btn-primary btn-sm" style='color:white;width:200px;margin-left:30px;'><span class='glyphicon glyphicon-plus'></span> &nbsp;Add section </a></td>
				 		<td><a ng-click='onemore(2);' class="btn btn-primary btn-sm" style='color:white;width:200px;margin-left:30px;'><span class='glyphicon glyphicon-plus'></span> &nbsp;Add multiple choice </a></td>
				 		</tr></table>
				 </div>
				 <br /><br /><br /><br />
			
		</div>
		 <div class="col-md-2"></div>
		<div  class="col-md-8"></div>
		<div  class="col-md-4">
		<a href ng-click='savenewmenu();' class="btn btn-success btn-sm" style='color:white;width:200px;'><span class='glyphicon glyphicon-save'></span> &nbsp;{{ buttonlabel }} </a>
		</div>
	</div>

</div>

</div>
</div>
</div>

<script>

var token = <?php echo "'".$_SESSION['user_backoffice']['token']."';"; ?>

<?php
    $mediadata = new WY_Media($theRestaurant);
    printf("var pathimg = '%s';", $mediadata->getFullPath('small'));
?>


app.controller('MenuNewController', ['$scope', '$http', 'bookService', 'cleaningData', function($scope, $http, bookService, cleaningData) {

	var aorder = (function() { var i, arr=[]; for(i = 0; i < 30; i++) arr.push(i); return arr; })();	
	$scope.paginator = new Pagination(25);
	$scope.restaurant = <?php echo "'".$theRestaurant."';"; ?>
	$scope.email = <?php echo "'" . $email . "';"; ?>
	$scope.typeselection = 1;
	$scope.takeoutflg = <?php echo ($takeoutflg != 0) ? 'true' : 'false'; ?>;
	$scope.nonefunc = function() {};					
	$scope.currency	= 'SGD';
	$scope.names = [];
	$scope.subnames = [];
	$scope.selectedItem = null;	
	$scope.glbType = ['private', 'drink', 'extented', 'set meal', 'options','global'];

	$scope.predicate = $scope.oldpredicate = 'index';
	$scope.reverse = false;
	$scope.itemList = [];
	$scope.path = pathimg;
	$scope.current = null;
	$scope.tabletitle = [ {'a':'index', 'b':'index' , q:'down', l:'75', cc: 'black' }, {'a':'value', 'b':'Menu Title' , q:'down', l:'200', cc: 'black' }, {'a':'morder', 'b':'order' , q:'down', l:'35', cc: 'fuchsia' }, {'a':'private', 'b':'’private’' , l:'75', q:'down', cc: 'black' } ];
	$scope.bckups = $scope.tabletitle.slice(0);
	$scope.menuContent = [{ a: 'value', b:'Menu Title', d:'folder-open', t:'input' }, { a: 'description', b:'Description', d:'align-center', t:'input' }, { a: 'price', b:'Price', d:'usd', t:'input' }, { a: 'morder', b:'Position', d:'arrows-v', t:'array', val: aorder, func: $scope.nonefunc } ];
	$scope.itemContent = [{ a: 'item_title', b:'Item Title', d:'folder-open-o', t:'input' }, { a: 'item_description', b:'Description', d:'align-center', t:'input' }, { a: 'price', b:'Price', d:'usd', t:'input' }, { a: 'morder', b:'Position', d:'arrows-v', t:'array', val: aorder, func: $scope.nonefunc } ];
	$scope.itemSection = [{ a: 'item_title', b:'Item Title', d:'folder-open-o', t:'input' }, { a: 'morder', b:'Position', d:'arrows-v', t:'array', val: aorder, func: $scope.nonefunc } ];
	
	$scope.glbType.map(function(item) { $scope.menuContent.push({a:item, b:item, d:'', t: 'checkbox' }); });
	$scope.menuList = [{ a: 'index', b:'Index' }, { a: 'value', b:'Description' }, { a: 'morder', b:'Preference Order' } ];
	$scope.itemListFlg = [ { label: 'vegi', width: '16'}, { label: 'spicy1', width: '20'}, { label: 'spicy2', width: '20'}, { label: 'spicy3', width: '20'}, { label: 'halal', width: '16'}, { label: 'chef_reco', width: '16'} ];	
	if($scope.takeoutflg === true)
		$scope.itemListFlg.push({ label: 'takeout', width: '20'});
		
	$scope.getAlignment = bkgetalignment;

	$scope.menuItemFactory = function(key, index) {
		return {
			restaurant: $scope.restaurant,
			menuID: key,
			itemID: index,
			currency: $scope.currency,
			typeflag: 0,
			extraflag: '',
			item_description: '',
			item_title: '',
			morder: 0,
			price: 0,
			status: 'new',
			type: 'item',
			section: 0,
			menu_categorie_id: 0,
			mimage:'',
            spicy1: 0,
            spicy2: 0,
            spicy3: 0,
            vegi: 0,
            chef_reco: 0,
            halal: 0,
            takeout: 0,
			
			gblindex: function() { 
				for(var i = 0; i < $scope.subnames.length; i++) 
					if($scope.subnames[i].menuID === this.menuID && $scope.subnames[i].itemID === this.itemID) 
						return i;
				return -1;
				},
				
			clean: function() {
				for (var attr in this)
		        	if(this.hasOwnProperty(attr) && typeof this[attr] !== 'function' && attr !== '$$hashKey') {
        				if(typeof this[attr] === 'string') {
        					this[attr] = this[attr].replace(/\'|\"/g, '’');
        					}
        				}
				return this;
				},
					
			replicate: function(obj) {
				var index;
				for (var attr in this)
		        	if(this.hasOwnProperty(attr) && typeof this[attr] !== 'function' && attr !== '$$hashKey') {
        				this[attr] = (attr !== "currency") ? obj[attr] : this.currency;
        				}
        		index = ['USD', 'SGD', 'EURO', 'EUR'].indexOf(this.currency);
        		this.cicon = (index >= 0) ? ['usd', 'usd', 'eur', 'eur'][index] : "";
				return this;
				}	
			};
		};
		
	$scope.menuFactory = function() {
		var self = {
			ID: 0,
			restaurant: $scope.restaurant,
			menuID: Math.floor((Math.random() * 10000000) + 1),
			itemindex: 1,
			status: 'new',
			typeflag: 0,
			extraflag: '',
			morder: 0,
			price: 0,
			currency: $scope.currency,
			type: 'categorie',
			value: '',
			description: '',
			
			gblindex: function() { 
				for(var i = 0; i < $scope.names.length; i++) 
					if($scope.names[i].menuID === this.menuID) 
						return i;
				return -1;
				},

			clean: function() {
				for (var attr in this)
		        	if(this.hasOwnProperty(attr) && typeof this[attr] !== 'function' && attr !== '$$hashKey') {
        				if(typeof this[attr] === 'string') {
        					this[attr] = this[attr].replace(/\'|\"/g, '’');
        					}
        				}
				return this;
				},
					
			replicate: function(obj) {
				for (var attr in this)
		        	if(this.hasOwnProperty(attr) && typeof this[attr] !== 'function' && attr !== '$$hashKey') 
         				this[attr] = (attr !== "currency") ? obj[attr] : this.currency;
        		index = ['USD', 'SGD', 'EURO', 'EUR'].indexOf(this.currency);
        		this.cicon = (index >= 0) ? ['usd', 'usd', 'eur', 'eur'][index] : "";
				return this;
				}						
			};
		$scope.glbType.map(function(item) { self[item] = 0; });
		return self;
		};
	
	$scope.testExtented = function() {
	  return function(item) {
		return (item.a !== 'price' || ($scope.selectedItem && $scope.selectedItem.extented === 1));
	  };
	};

	$scope.setflag = function(flag) {
		if($scope.selectedItem && $scope.selectedItem[flag])
			$scope.selectedItem[flag] ^= 1;
		};
		
	$scope.gblindex = function(menuID) { 
		for(var i = 0; i < $scope.names.length; i++) 
			if($scope.names[i].menuID === menuID) 
				return i;
		return -1;
		};
		
	$scope.copyobj = function(o, d) {
		for (var attr in o) {
        	if(o.hasOwnProperty(attr) && typeof o[attr] !== 'function' && attr !== '$$hashKey') 
        		d[attr] = o[attr];
        	}
		};
		
	$scope.prefillobj = function(o, d) {
		for (var attr in o) {
        	if(o.hasOwnProperty(attr) && typeof o[attr] !== 'function' && typeof d[attr] !== 'undefined' && attr !== '$$hashKey') 
        		d[attr] = o[attr];
        	}
		};
		
	$scope.initorder = function() {
		$scope.tabletitle = $scope.bckups;
		$scope.predicate = $scope.oldpredicate = "morder";
		$scope.reverse = false;
		};
		
	$scope.reorder = function(item, alter) {
		alter = alter || "";
		if (alter !== "")  item = alter;
		$scope.predicate = item;
		if ($scope.oldpredicate === item)
			$scope.reverse = !$scope.reverse;
		else $scope.reverse = false;
		$scope.oldpredicate = item;
		};
				
	bookService.readMenu($scope.restaurant, $scope.email).then(function(response) {
		var i, oo;
		if(response === null) return;
		
		$scope.data = response.data.menus;  
		$scope.currency = response.data.currency;
		if(typeof $scope.currency === "string" && $scope.currency !== "")
			$scope.itemContent.iconcurrency($scope.currency, "price", "a", "d");
			  
		for (i = 0; i < $scope.data.length; i++) {
			oo = new $scope.menuFactory().replicate($scope.data[i]);
			oo.index = i + 1;
			oo.typeflag = parseInt(oo.typeflag);
			oo.drink = oo.private = oo.extented = 0;
			$scope.glbType.map(function(val, index, arr) {
				if(oo.typeflag & (1 << index)) oo[val] = 1;
				else oo[val] = 0;
				});
			oo.morder = parseInt(oo.morder);
			$scope.names.push(oo);
			}

		$scope.subdata = response.data.menus_items;  
		for (i = 0; i < $scope.subdata.length; i++) {
			oo = new $scope.menuItemFactory($scope.subdata[i].menuID, $scope.subdata[i].itemID).replicate($scope.subdata[i]);
			oo.index = i + 1;
			oo.morder = parseInt(oo.morder);
			oo.typeflag = parseInt(oo.typeflag);
			if(oo.item_description === 'section') oo.section = 1;
			if(oo.item_description === 'section_or') oo.section = 2;
			
			$scope.subnames.push(oo);
			}
		$scope.paginator.setItemCount($scope.names.length);
		$scope.initorder();
		});

	bookService.readMenuPicture($scope.restaurant, $scope.email).then(function(response) {		
		var i, data = response.data.data;
		$scope.imageobject = [];
		for (i = 0; i < data.length; i++)
			if(data[i].object_type === 'menu')
				$scope.imageobject.push(data[i].name);

		});

	$scope.reset = function(item) {
		$scope.listingMenuFlag = false;
		$scope.detailsMenuFlag = false
		$scope.createMenuFlag = false;
		$scope[item] = true;	
		};

	$scope.findmax = function(data, field) {
		var i, max = 0;
		for(i = 0; i < data.length; i++) { oo = data[i]; if(oo[field] > max) max = oo[field]; }
		return parseInt(max);
		};
		
	$scope.onemore = function(flg) {
		var obj = new $scope.menuItemFactory($scope.selectedItem.menuID, ++$scope.selectedItem.itemindex);
		obj.morder = $scope.findmax($scope.itemList, 'morder') + 1;
		if(flg && flg === 1) {
			obj.section = 1;
			obj.item_description = 'section';
			oo.typeflag |= 8;
			}
		if(flg && flg === 2) {
			obj.section = 2;
			obj.item_description = 'section_or';
			oo.typeflag |= 16;
			}
		$scope.itemList.push(obj);
		};
				
	$scope.updatemenuobj = function(menus, items) {	
		var i, ind;
		
		if(menus.status !== "new") {
			if((ind = menus.gblindex()) < 0) { alert('Unable to find new record'); return; }
			$scope.copyobj(menus, $scope.names[ind]);
			}
		else {
			menus.status = "";
			menus.index = $scope.names.length;
			$scope.names.push(menus);
			}
	
		for(i = 0; i < items.length; i++) {
			if(items[i].status === "new") {
				items[i].status = "";
				$scope.subnames.push(items[i]);
				}
			else if((ind = items[i].gblindex()) >= 0) { 
				if(items[i].status === "delete")
					$scope.subnames.splice(ind, 1);
				else $scope.copyobj(items[i], $scope.subnames[ind]);
				}
			}
		};
		
	$scope.setdataMenu = function(menus) {
		var i, limit, menuID, oo, maxitem = 0;

		$scope.itemList = [];
		oo = new $scope.menuFactory().replicate(menus);
		menuID = oo.menuID;
		limit = $scope.subnames.length;
		for(i = 0; i < limit; i++)
			if($scope.subnames[i].menuID === menuID) {
				if($scope.subnames[i].itemID > maxitem) maxitem = $scope.subnames[i].itemID;
				$scope.itemList.push(new $scope.menuItemFactory(menuID, $scope.subnames[i].itemID).replicate($scope.subnames[i]));
				}
		$scope.itemList.sort(function(a, b) { return (a.morder - b.morder); });
		oo.itemindex = maxitem;
		return oo;
		};
						
	$scope.view = function(oo) {
		$scope.selectedItem = $scope.setdataMenu(oo);
		$scope.reset('detailsMenuFlag');
		};

	$scope.create = function() {
	
		$scope.selectedItem = new $scope.menuFactory();
		$scope.itemList = [];
		$scope.buttonlabel = "Save new menu";
		$scope.action = "create";
		$scope.reset('createMenuFlag');
		return false;
		};

	$scope.update = function(oo) {
		$scope.selectedItem = $scope.setdataMenu(oo);
		$scope.buttonlabel = "Update";
		$scope.action = "update";
		$scope.reset('createMenuFlag');		
		return false;
		};

	$scope.savenewmenu = function() {
		var oo = $scope.selectedItem;
				
		oo.clean();
		oo.typeflag = 0;
		$scope.glbType.map(function(val, index, arr) {
			if(parseInt(oo[val]) === 1) oo.typeflag += (1 << index);
			});

		for(var i=0; i < $scope.itemList.length; i++) 
			$scope.itemList[i].clean();
		if(oo.status === "new") { 
       
			bookService.createMenu($scope.restaurant, $scope.email, { menus: oo, menus_items: $scope.itemList } )
			.then(function(response) {
                            bookService.logevent(190,'BO_MENU_create','BO_MENU');
				var i, old = oo.menuID, realID = parseInt(response.errors);
				if(realID !== old) {
					oo.menuID = realID;
					for(i = 0; i < $scope.itemList.length; i++) 
						$scope.itemList[i].menuID = realID;
					}
				if($scope.updatemenuobj(oo, $scope.itemList) === false)
					return;
				alert("Menu has been created"); 
				});
			}

		else {
                    oo.status = 'update';
			bookService.updateMenu($scope.restaurant, $scope.email, { menus: oo, menus_items: $scope.itemList } )
			.then(function(response) { 
                            bookService.logevent(190,'BO_MENU_update','BO_MENU');
				if($scope.updatemenuobj(oo, $scope.itemList) === false)
					return;
				alert("Menu has been updated"); 
				});
			}
		
 		$scope.backlisting();		
		};
		
	$scope.delete = function(oo) {
		if(confirm("Are you sure you want to delete " + oo.value) == false)
			return;
			
		bookService.deleteMenu($scope.restaurant, $scope.email, oo.menuID).then(function() { 
                      bookService.logevent(190,'BO_MENU_delete','BO_MENU');
			var i, value = oo.value, menuID = oo.menuID, ind = $scope.gblindex(oo.menuID);

			if(ind >= 0)
				$scope.names.splice(ind, 1);
			for(i = $scope.subnames.length - 1; i >= 0; i--)
				if($scope.subnames[i].menuID === menuID)
					$scope.subnames.splice(i, 1);
			alert(value + " has been deleted"); 
			});
		
		};

	$scope.deleteitem = function(oo) {
		var menuID = oo.menuID, itemID = oo.itemID;
	
		if(confirm("Are you sure you want to delete item " + oo.item_title + " of " + $scope.selectedItem.value) == false)
			return;

		for(var i = 0; i < $scope.itemList.length; i++) {
			if($scope.itemList[i].menuID === menuID && $scope.itemList[i].itemID === itemID) {
				$scope.itemList[i].status = "delete";
				return;
				}
			}
		};

	$scope.cleaninput = function(ll) {
		if(typeof $scope.selectedItem[ll] === 'string')
			$scope.selectedItem[ll] =  $scope.selectedItem[ll].replace(/\'|\"/g, '’');
		};
		
	$scope.cleansubinput = function(str) {
		if(typeof str === 'string')
			return str.replace(/\'|\"/g, '’');
		return str;
		};
		
	$scope.backlisting = function() {
		$scope.reset('listingMenuFlag');
		$scope.selectedItem = null;	
		};

}]);

</script>

