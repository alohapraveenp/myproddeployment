<div class="container">
    <div class="row">
        <div class="col-md-12 left-sec">

<div ng-controller="viewInvoiceController" ng-init="moduleName='viewInvoice'; listingMgFlag = true; listing1MgFlag=false; createMgFlag=false;" >

	<div id='listing' ng-show='listingMgFlag'>
		<div class="form-group"  style='margin-bottom:25px;'>
			<div class="col-md-4">
				<div class="input-group">
					<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>
					<input type="text" class="form-control input-sm" ng-model="searchText" ng-change="paginator.setPage(0);" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" style='width:200px;'> 
				</div>
				<ul class="nav navbar-nav">
				<li class='infobk'>selected invoices: <strong> {{filteredInvoice.length}} </strong></li>
				<li class='infobk' ng-if = 'aSelDay' >selected date: <strong>{{ aSelDate }} </strong>
					<button  type="button" class="btn btn-warning btn-xs" ng-click="cleardate();" style="margin-left:10px"><i class="glyphicon glyphicon-off"></i></button>
				</li>
				</ul>
			</div>

			<div class="col-md-1">
				<div class="input-group" style="width:1px">
					<span><button  type="button" class="btn btn-default" ng-click="mydata.dateopen($event)"><i class="glyphicon glyphicon-calendar"></i></button></span>
					<input type="text" class="form-control" uib-datepicker-popup="{{mydatastart.formats[0]}}" ng-change='mydata.onchange();' ng-model="mydata.originaldate" is-open="mydata.opened" 
					min-date="mydata.minDate" max-date="mydata.maxDate" datepicker-options="mydata.dateOptions" date-disabled="mydata.disabled(date, mode)" ng-required="true" close-text="Close" style="width:0;opacity:0" />
				</div>
			</div>

			<div class="col-md-1"></div>
			<div class="col-md-3">
				</form>
				<form action="echo.php" id="extractInvoice" name="extractInvoice" method="POST" target="_blank">
					<input type="hidden" value="testing" ng-model='contentInvoice' name="contentInvoice" id="contentInvoice">
					<a href ng-click='extractSelection();' class="btn btn-success btn-sm" style='color:white;'><span class='glyphicon glyphicon-save'></span> &nbsp;Extract Selection</a>
				</form>
			</div>

			<div class="col-md-2">
				<div class="btn-group" uib-dropdown >
					<button type="button" class="btn btn-default btn-xs" uib-dropdown-toggle aria-expanded="false" style='font-size:11px;'>Page Size <span class="caret"></span></button>
					<ul class="dropdown-menu" uib-dropdown-menu role="menu">
						<li ng-repeat="x in paginator.pagerange()" class='glyphiconsize'><a href ng-class="{ 'glyphicon glyphicon-ok glyphiconsize': x === paginator.getRowperPage() }" ng-click="paginator.setRowperPage(x)"> {{x}}</a></li>
					</ul>
				</div> 		
			</div>
		</div>
        <div style=" clear: both;"></div>

		<table width='100%' class="table table-condensed table-striped" style='font-family:Roboto;font-size:12px;'>
			<thead><tr><th ng-repeat="y in tabletitle | filter: { a: '!details'}"><tbtitle var="{{y.a}}" name='tabletitle'  module='moduleName'/></th><th>Edit</th></tr></thead>
			<tr ng-repeat="x in filteredInvoice = (invoice | filter:searchText  | filter: filterdate ) | sizefilter:this | orderBy:predicate:reverse | slicefilter:paginator.getPage():paginator.getRowperPage():paginator.getItemCount() | limitTo:paginator.getRowperPage()" style='font-family:helvetica;font-size:11px;'>
				<td ng-repeat="y in tabletitle | filter: { a: '!details'}"><a href ng-click="view(x)">{{ x[y.a] | adatereverse:y.c}}</a></td>
				<td><a href ng-click="update(x)"><span class='glyphicon glyphicon-pencil blue'></span></a></td>
			</tr><tr><td colspan='{{tabletitle.length+2}}'></td></tr>
		</table>
		<div ng-if="filteredInvoice.length >= paginator.getRowperPage()" class="col-md-12" align="center" style='font-size:11px' ng-include="'inc/paginator.html'"></div>
	</div>

	<div class="col-md-12" ng-show='listing1MgFlag'>
		<br /><a href class='btn btn-info btn-sm customColor' ng-click="backlisting()"><span class='glyphicon glyphicon-step-backward'></span> &nbsp;Back to listing</a><br />
		<table class='table-striped' style="margin: 0 0 150px 30px;font-size:13px;font-family: Roboto">		
			<tr ng-repeat="z in tabletitle | filter: { a: '!details'}"><td nowrap><strong>{{ z.b }} :</strong></td><td width='30'> &nbsp; </td><td>{{ selectedItem[z.a] }}</td></tr>
			<tr><td colspan='3'><strong>Details :<br /><br /></strong></td></tr>
			<tr ng-repeat="yy in selectedItem.info track by $index" style='font-family:helvetica;font-size:9px;'><td colspan='3'>{{yy}}</td></tr>
		</table>
	</div>

</div>

</div>
</div>
</div>

<script>

var token = <?php echo "'".$_SESSION['user_backoffice']['token']."';"; ?>
var selectionAr;

app.controller('viewInvoiceController', ['$scope', '$http', 'bookService', function($scope, $http, bookService) {
	
	var forceDate = new Date(new Date().getFullYear()-2, 0, 1);
	$scope.paginator = new Pagination(50);
	$scope.email = <?php echo "'" . $email . "';"; ?>
	$scope.predicate = '';
	$scope.reverse = false;
	$scope.tabletitle = [ {a:'restaurant', b:'Restaurant', c:'', d:'tower', cc:'', q:'down', d:'fuchsia' }, {alter:'vdate', a:'invoicedate', b:'Date', c:'date', d:'ban-circle', cc:'', q:'down', d:'black' }, {alter:'vsubtotal', a:'subtotal', b:'SubTotal', c:'', d:'usd', cc:'', q:'down', d:'black' }, {alter:'vtotal', a:'total', b:'Total', c:'', d:'usd', cc:'', q:'down', d:'black' }, {alter:'vlicense', a:'license', b:'License', c:'', d:'usd', cc:'', q:'down', d:'black' }, {a:'currency', b:'Currency', c:'', d:'usd', cc:'', q:'down', d: 'black' }, {a:'status', b:'status', c:'', d:'flash', cc:'', q:'down', d:'black' }, {a:'details', b:'details', c:'', d:'flash', cc:'', q:'down', d:'black' } ];
	$scope.bckups = $scope.tabletitle.slice(0);
	$scope.cdate = new Date();
	$scope.aSelDay = null;
	$scope.aSelDate = "";
	$scope.selectedvDay = 0;
	$scope.mydata = new bookService.ModalDataBooking();

	$scope.getAlignment = bkgetalignment;

	$scope.Objaccount = function() {
		return {
			restaurant: '',
			invoicedate: '', 
			ddate: '', 
			total: 0, 
			subtotal: 0, 
			currency: 'SGD', 
			status:'', 
			details:'', 
			segments:'', 
			vdate:'', 
			
			replicate: function(obj) {
				for (var attr in this)
		        	if(this.hasOwnProperty(attr) && typeof this[attr] !== 'function' && attr !== '$$hashKey') 
        				this[attr] = obj[attr];
				return this;
				},						
			clean: function() {
				for (var attr in this)
		        	if(this.hasOwnProperty(attr) && typeof this[attr] !== 'function' && attr !== '$$hashKey') {
        				if(typeof this[attr] === 'string')
        					this[attr] = this[attr].replace(/[^a-zA-Z_\-.0-9]/g, '');
        				}
				return this;
				}	
			};
		};

		
	$scope.cleardate = function(item) {
		$scope.aSelDay = null;
		$scope.aSelDate = "";
		$scope.selectedvDay = 0;
		};
		
	$scope.filterdate = function(item) {
		if(!$scope.selectedvDay)
			return true;
		return (item["vdate"] && item["vdate"] === $scope.selectedvDay);
		};

	$scope.setfilterdate = function() {
		$scope.aSelDay = $scope.mydata.getTheDate().beginmonth();
		$scope.selectedvDay = $scope.aSelDay.getTime();
		$scope.aSelDate = $scope.aSelDay.getDateFormat('-');
		$scope.paginator.setPage(0);
		};

	$scope.mydata.setInit(forceDate, "09:00", $scope.setfilterdate, 3, new Date($scope.cdate.getFullYear(), 0, 1), new Date($scope.cdate.getFullYear()+1, 11, 31));

	$scope.initorder = function() {
		$scope.tabletitle = $scope.bckups;
		$scope.predicate = "restaurant";
		$scope.reverse = false;
	};

	$scope.reorder = function(item, alter) {
		alter = alter || "";
		if (alter !== "")  item = alter;
		$scope.reverse = ($scope.predicate === item) ? !$scope.reverse : false;
		$scope.predicate = item;
	};
				
	bookService.readallinvoice('all', $scope.email).then(function(response) { 
		var i, oo;
		$scope.invoice = response.invoice;  
		for(var i = 0; i < $scope.invoice.length; i++) {
			oo = $scope.invoice[i];
			oo.vdate = oo.invoicedate.jsdate().getTime();
			oo.ddate = oo.invoicedate.jsdate().getDateFormat('-');
			oo.total = parseFloat(oo.total);
			oo.subtotal = parseFloat(oo.subtotal);
			oo.vtotal = oo.total;
			oo.vsubtotal = oo.subtotal;
			oo.vlicense = oo.license;
			if(oo.currency === 'HKD') { oo.vtotal /= 5; oo.vsubtotal /= 5; oo.vlicense /= 5; } 
			if(oo.currency === 'BAHT') { oo.vtotal /= 25; oo.vsubtotal /= 25; oo.vlicense /= 25; } 
			}
		$scope.paginator.setItemCount($scope.invoice.length);
		$scope.initorder();
		});

	$scope.reset = function(item) {
		$scope.listingMgFlag = false;
		$scope.listing1MgFlag = false
		$scope[item] = true;	
		};

	$scope.backlisting = function() {
		$scope.reset('listingMgFlag');
		};

	$scope.update = function(oo) {
		var action = prompt("Type 1 to consolidate, 0 otherwise");
		if((oo.status === '' && action !== '1') || (oo.status === 'conso' && action === '1'))
			return;

		oo.status = (parseInt(action) === 1) ? 'conso' : '';
		alert(oo.status);
		bookService.updateinvoicestatus(oo.restaurant, oo.invoicedate, oo.status, $scope.email);			
		};
				
	$scope.view = function(oo) {
		$scope.selectedItem = oo;
		$scope.selectedItem.info = $scope.selectedItem.details.split("|||");
		$scope.reset('listing1MgFlag');
		};
	
	$scope.findaccount = function(restaurant) {
		for(var i = 0; i < $scope.invoice.length; i++)
			if($scope.invoice[i].restaurant === restaurant)
				return i;
		return -1;
		};
		
	$scope.extractSelection = function() {

		var maxlimit = 500; // ajax call will not support more data
		var tt, sep;
		var exportselect = $scope.filteredInvoice;
		var limit = exportselect.length;
		tt = sep = '';
		if (limit > maxlimit) limit = maxlimit;
		for (i = 0; i < limit; i++, sep = ', ') {
			u = exportselect[i];
				tt += sep + '{ ' +
				'"restaurant":"' + u.restaurant + '", ' +
				'"invoicedate":"' + u.invoicedate + '", ' +
				'"subtotal":"' + u.subtotal + '", ' +
				'"total":"' + u.total + '", ' +
				'"currency":"' + u.currency + '", ' +
				'"status":"' + u.status + '" }';
			}
		tt = '{ "booking":[' + tt + '] }';
		$scope.contentInvoice = tt;
		$('#contentInvoice').val(tt);
		$('#extractInvoice').submit();
		};
	
}]);

	
</script>

