(function()  {
if ('getDateFormat' in Date === false) {
	Date.prototype.getDateFormat = function(sep) {
		var d = this.getDate(), m = this.getMonth()+1, y = this.getFullYear();
		if(typeof sep !== 'string' || (sep !== '/' && sep !== '-')) sep = '-';
		return ((d <= 9) ? '0' : '') + d + sep + ((m <= 9) ? '0' : '') + m + sep + y;
		};

	Date.prototype.getDateFormatReverse = function(sep) {
		var d = this.getDate(), m = this.getMonth()+1, y = this.getFullYear();
		if(typeof sep !== 'string' || (sep !== '/' && sep !== '-')) sep = '-';
		return y + sep + ((m <= 9) ? '0' : '') + m + sep + ((d <= 9) ? '0' : '') + d;
		};

	String.prototype.jsdate = function() {
		var n, s, i;
		if(this.length < 10 || this.indexOf('-') === -1) 
			return new Date(2016, 0, 1);
		s = this.substring(0, 10);
		tt = s.split('-');
		for(i = 0; i < tt.length; i++) tt[i] = parseInt(tt[i]);
		tt[1] = tt[1] - 1;
		return new Date(tt[0], tt[1], tt[2], 0, 0, 0, 0);
		};
	}
})();

app.directive('selectCreateBookingDate', [ function () {
	return {
		restrict: 'E',
		scope: {
			theday: '='
			},
		template:
			"<span ng-if='theday.showtitle === true'> selected day: <strong> {{ theday.selectedDay | adatereverse:'date' }} </strong></span>" + 
			"<div class='btn-group' uib-dropdown>" +
				"<button type='button' class='btn btn-warning btn-xs' uib-dropdown-toggle aria-expanded='false' style='font-size:11px;margin-left:15px;'>date mode <span class='caret'></span></button>" +
				"<ul class='dropdown-menu' uib-dropdown-menu role='menu'>" + 
				"<li ng-repeat='x in theday.datemodelist' class='glyphiconsize'><a href ng-class=\"{ 'glyphicon glyphicon-ok glyphiconsize': x === theday.datemode }\" ng-click='setdatemd(x);'> {{x}}</a></li>" +
				"<li class='divider'></li>" +
				"<li><a ng-click='setdatemd(null);'><i class='glyphicon glyphicon-off'></i> reset</li>" +
				"</ul>" +
			"</div>",		
		controller: function ($scope) {
			$scope.setdatemd = function(x) {
				if(!x || x === 'reset') {
					$scope.theday.selectedDay = null;
					if($scope.theday.resetfunc) 
						$scope.theday.resetfunc();
				} else  {
					$scope. theday.datemode = x;
					$scope. theday.field = ($scope. theday.datemode !== $scope. theday.datemodelist[0]) ? "vcdate" : "vdate";
					}
				};
			}
		};
}]);


// Code goes here
// it does not get minified
app.directive('dropdownMultiselect', [ function () {
	return {
		restrict: 'E',
		scope: {
			model: '=',
			list: '=',
			savefunc: '=',
			},
		template:
				"<div class='btn-group btn-sm' data-ng-class='{open: openMS}'>" +
					"<button class='btn btn-default btn-sm' data-ng-click='openDropdownMS()'>{{localtitle}}</button>" +
					"<button class='btn btn-default btn-sm dropdown-toggle' data-ng-click='openDropdownMS()'><span class='caret'></span></button>" +
					"<ul class='dropdown-menu scrollablesmall-menu' aria-labelledby='dropdownMenu'>" +
					// "<li><a data-ng-click='selectAllMS()'><span class='glyphicon glyphicon-ok green' aria-hidden='true'></span> Check All</a></li>" +
					"<li><a data-ng-click='saveMS()'>SAVE</a></li>" +
					"<li><a data-ng-click='deselectAllMS();'>RESET</a></li>" +
					"<li class='divider'></li>" +
					"<li data-ng-repeat='item in list'><a data-ng-click='toggleItemMS(item)'><span data-ng-class='getClassNameMS(item)' aria-hidden='true'></span> {{item}}</a></li>" +
					"</ul>" +
				"</div>",

		controller: function ($scope) {
			var tbname = []; 
			var booking = $scope.model.booking;
			if(typeof $scope.model.tablename === 'string' && $scope.model.tablename.length > 0) 
				tbname = $scope.model.tablename.split(",");
			$scope.localtitle = (tbname.length < 1) ? "add/reset" : tbname.join(",");
			$scope.openDropdownMS = function () { 
				if(booking !== $scope.model.booking) { 
					booking = $scope.model.booking; 
					tbname = $scope.model.tablename.split(","); 
					} 
				$scope.openMS = !$scope.openMS; 
				};
			$scope.saveMS = function () { 
				$scope.openMS = false; 
				$scope.model.tablename = $scope.localtitle = tbname.join(",");  
				if($scope.localtitle === "") $scope.localtitle = "add/reset"; 
				$scope.savefunc($scope.model.booking, $scope.model.tablename, 'set'); 
				};
			$scope.selectAllMS = function () {
				tbname = [];
				angular.forEach($scope.list, function (item, index) { $scope.additemMS(item);  });
				};
			$scope.deselectAllMS = function () { 
				tbname = []; 
				};
			$scope.toggleItemMS = function (item) {
				var ind = $scope.getIndexFound(item);
				if(ind < 0) $scope.additemMS(item);
				else $scope.removeitemMS(item, ind);
				};
			$scope.additemMS = function (item) {
				tbname.push(item); 
				tbname.sort();
				};
				
			$scope.removeitemMS = function (item, ind) {
				tbname.splice(ind, 1);
				};
				
			$scope.getClassNameMS = function (item) {
				if($scope.getIndexFound(item) < 0) 
					return ""; 
				return 'glyphicon glyphicon-ok green';
				};
				
			$scope.getIndexFound = function(item) {
				return tbname.indexOf(item); 
				};
				
			}
		};
}]);
	

app.service('ModalService', ['$uibModal', function ($uibModal) {

        var modalDefaults = {
		size: 'sm',
		backdrop: true,
		keyboard: true,
		modalFade: true,
		animation: true,
		templateUrl: 'modalgen.html'
        };

        var modalOptions = {
		closeButtonText: 'Close',
		actionButtonText: 'OK',
		headerText: 'Proceed?',
		bodyText: 'Perform this action?'
        };

	this.setTemplate = function(template) {
		modalDefaults.templateUrl = template;
		};
		
	this.setSize = function(size) {
		modalDefaults.size = size;
		};
	
        this.showModal = function (customModalDefaults, customModalOptions) {
            if (!customModalDefaults) customModalDefaults = {};
            customModalDefaults.backdrop = 'static';
            return this.show(customModalDefaults, customModalOptions);
        };

        this.show = function (customModalDefaults, customModalOptions) {
            //Create temp objects to work with since we're in a singleton service
            var tempModalDefaults = {};
            var tempModalOptions = {};

            //Map angular-ui modal custom defaults to modal defaults defined in service
            angular.extend(tempModalDefaults, modalDefaults, customModalDefaults);

            //Map modal.html $scope custom properties to defaults defined in service
            angular.extend(tempModalOptions, modalOptions, customModalOptions);

            if (!tempModalDefaults.controller) {
                tempModalDefaults.controller = function ($scope, $modalInstance) {
                    $scope.modalOptions = tempModalOptions;
                    $scope.modalOptions.ok = function (result) {
                        $modalInstance.close(result);
                    };
                    $scope.modalOptions.close = function (result) {
                        $modalInstance.dismiss('cancel');
                    };
                }
            }

            return $uibModal.open(tempModalDefaults);
        };
        
        this.activate = function (title, initval, func, template, mydata, prepostcall, size, labelok, labelclose) {
			if(template === null)
				template = "modalgen1.html";
		
			if(prepostcall !== null)
				prepostcall(0);
		
			if(typeof labelok !== "string" || labelok.length < 2)
				labelok = 'OK';
			if(typeof labelclose !== "string" || labelclose.length < 2)
				labelclose = 'Close';
			
			var modalOptions = {
				againButtonText: 'More',
				closeButtonText: labelclose,
				actionButtonText: labelok,
				headerText: title,
				mydata: mydata,
				size: size,
				submit:function(result){ $modalInstance.close(result); if(prepostcall !== null) prepostcall(1); },
				myclose:function(){ $modalInstance.dismiss('cancel'); if(prepostcall !== null) prepostcall(1); },
				myagain:function() { $modalInstance.close('more'); if(prepostcall !== null) prepostcall(1); }    
			};
							
			this.setTemplate(template);
			this.setSize(size);

			var $modalInstance = this.showModal({}, modalOptions);
			$modalInstance.result.then(function (result) {
				func(result);
			});
		};

	this.ModalDataBooking = function() {

		var i, j, timeslotAr=[], persAr=[], hourslotAr=[], minuteslotAr=[];

		for(i = 1; i < 50; i++) 
			persAr.push(i);
		
		for(i = 9; i < 24; i++)
			for(j = 0; j < 60; j += 15) {
				timeslotAr.push((i < 10 ? '0' : '') + i + ':' + (j === 0 ? '0' : '') + j);
				}
			
		for(i = 9; i < 24; i++) 
			hourslotAr.push((i < 10 ? '0' : '') + i);
		
		for(i = 0; i < 60; i += 5) 
			minuteslotAr.push((i < 10 ? '0' : '') + i);
			
		return  {
				name: "", 
				pers: persAr, 
				timeslot: timeslotAr, 
				hourslot: hourslotAr,
				minuteslot: minuteslotAr,
				ntimeslot: 0,
				event: ['Birthday', 'Wedding', 'Reunion'],
				start_opened: false,
				opened: true,
				selecteddate: null,
				originaldate: null,
				dateFormated: "",
				theDate: null,
				minDate: null,
				maxDate: null,
                deposit:null,
				formats: ['dd-MM-yyyy', 'dd/MMMM/yyyy', 'yyyy/MM/dd', 'dd/MM/yyyy', 'dd-MM-yyyy', 'yyyy-MM-dd', 'dd.MM.yyyy', 'shortDate', 'yyyy-MM-dd', ],
				dateOptions: { formatYear: 'yyyy', startingDay: 1 },
				formatsel: "",
				func: null,
			
				setInit: function(ddate, rtime, func, format, minDate, maxDate) {
					
					this.theDate = null;
					this.func = func;
					this.formatsel = this.formats[format];			
					this.ntimeslot = rtime.substring(0, 5);
				
					if(ddate instanceof Date === false) {
						ddate = ddate.jsdate();
						}
					this.originaldate = ddate;
					this.selecteddate = ddate.getDateFormat('-');		
					this.minDate = minDate;
					this.maxDate = maxDate;
					
					if(navigator.userAgent.indexOf("Chrome") != -1 ) { 
						this.formatsel = this.formats[8]; 
						this.selecteddate = ddate.getDateFormatReverse('-'); 
						}	
					},
			
				setDate: function(ddate) {
					this.theDate = null;
					this.originaldate = ddate; // this is for the case that no data selected
					this.selecteddate = ddate.getDateFormat('-');
					if(navigator.userAgent.indexOf("Chrome") != -1) 
						this.selecteddate = ddate.getDateFormatReverse('-'); 
					},
					
				getDate: function(sep, mode) {
					if(typeof this.theDate !== null && typeof this.theDate !== undefined && this.theDate instanceof Date)
						return (mode !== 'reverse') ? this.theDate.getDateFormat(sep) : this.theDate.getDateFormatReverse(sep);
					return (mode !== 'reverse') ? this.originaldate.getDateFormat(sep) : this.originaldate.getDateFormatReverse(sep);
					},
					
				dateopen: function($event) {
					this.start_opened = true;
					this.opened = true;
					$event.preventDefault();
					$event.stopPropagation();
					},
				
				disabled: function(date, mode) {
					return false;
					},
			
				calendar: function() {
					this.theDate = this.selecteddate;
					if(this.theDate instanceof Date)
						this.dateFormated = this.theDate.getDateFormat('/');
					},
			
				onchange: function() {
					this.theDate = this.selecteddate;
					if(this.theDate instanceof Date)
						this.dateFormated = this.theDate.getDateFormat('/');

					if(this.func !== null)
						this.func();
					}	
				};
		};

    }]);
