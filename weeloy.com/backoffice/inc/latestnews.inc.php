<div ng-controller="LatestnewsController" ng-cloak ng-init="moduleName='latestnews';"  >

<div class='row' style='background: #f6f6f6 !important;'   >
    <div id='carousel-example-generic' class='' data-ride=''>
        <!-- Wrapper for slides -->
        <div class='carousel-inner'>
            <div class=''>
                <img ng-if ='!banner.images' src='../images/admin/bo_slider_928*460.jpg' alt='First slide'>
                <img ng-if ='banner.images' ng-src='{{banner.images}}'> 
                <!-- Static Header -->
            </div>
        </div>
    </div>
</div>
<div class="row" style='background: #f6f6f6 !important;'>
<div class="container"  >
    <div class="row">
        <div class="white-bg1 col-md-12"  >
            <h3 style='padding-left:10px;' >News & Updates</h3>
            <div ng-if ="Categories.length >0" class="col-md-4" ng-repeat="category in Categories" style='margin-top:15px;margin-bottom:10px;'>
                <p>
               <a ng-href="{{category.link}}" class="pinme">
                <img ng-src="{{category.images}}" width="262px">
<!--                    <img src="../images/admin/bo_feature_262*162.jpg" width="262px">-->
                </p><h4>{{category.title}}</h4>
                <p >{{category.description}}</p>

                <p></p></a>
            </div>
            <div ng-if ="Categories.length <= 0 "> 
              <div class="col-md-4"><p>
                    <img src="../images/admin/bo_feature_262*162.jpg" width="262px">
                </p><h4>Check the new Events feature</h4>
                <p style="color:grey;">A new menu EVENTS is now available. You can now feature all your events easily. You just need to upload your Event's image, description, date and it will automatically upload on your restaurant page.</p>

            <p></p></div>

            <div class="col-md-4"><p>
              <img src="../images/admin/bo_weeloy-wheel_262*162.jpg" width="262px">
                </p><h4>Customize your Weeloy Wheel</h4>
                <p style="color:grey;">By default, your Wheel has a standard configuration, with a basic value of 52. <br>Your visibility and conversion rate depend on how you set this value. Customize your WHEEL to increase the WHEEL value and at the end, your market reach. </p>

                <p></p></div>

            <div class="col-md-4"><p>

                    <img src="../images/admin/bo_focus_262*162.jpg" width="262px">
                </p><h4>FOCUS: Reservation system</h4>
                <p style="color:grey;">A dynamic reservation system allows your guest to book instantly to your restaurant and receive an automatic message. Manage your availability using the Table Availability Calendar (TAC) menu.</p>

                <p></p>
            </div>
            <div> 
            <div class="clearfix visible-md-block"></div>
            <div class="clearfix visible-lg-block "></div><!--This clearfix has to be set every 3 div because that's that col-lg break to a new line-->
        </div>
    </div>
</div>

</div>
    </div>
</div>
    
<script>

var token = <?php echo "'".$_SESSION['user_backoffice']['token']."';"; ?>
var email =  <?php echo "'".$_SESSION['user_backoffice']['email']."';";?> 

app.controller('LatestnewsController', ['$scope', '$http', 'bookService', function($scope, $http, bookService) {
       bookService.getBolatestnews().then(function(response) {

            var tempArr = [],banner = null;
            var CateItem = response.data.categories;
            if (response.data.categories.length > 0) {
                response.data.categories.forEach(function(value, key) {
                    var lnewsimage;
                    value.images.forEach(function(v,k){
                        if(v.image !=='' &&  v.image !=='undefined' ){
                            lnewsimage = "https://media.weeloy.com/upload/restaurant/" + v.restaurant + "/" + v.image;
                        }
                    });
                        var obj = {
                            'id': response.data.categories[key].id,
                            'title': response.data.categories[key].title,
                            'tag': response.data.categories[key].tag,
                            'description': response.data.categories[key].description,
                            'link': response.data.categories[key].link,
                            'images': lnewsimage
                        };
                        if(response.data.categories[key].type === 'bo-latestnews'){
                            tempArr.push(obj);
                        }
                        if(response.data.categories[key].type === 'bo-banner'){
                            banner =  obj;
                        }
                        
                });
            }
            $scope.banner = banner; 
            $scope.Categories =  tempArr;


        });
        
        
}]);
</script>
