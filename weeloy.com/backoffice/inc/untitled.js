var token = require("./credential");

function Authentication (functionName, event, callback, next) { // Constructor
	// Private fields
	var functionName = functionName;
	var nextHandler = next;
	var method = event.httpMethod;
	var data = typeof event.body !== 'undefined' ? event.body.data : null;
	var callback = callback;
	autNextInChain = function() {
		return (typeof nextHandler !== 'undefined' && nextHandler) ?
				nextHandler.Handle(function(processed, result) { callback(processed, result); }) : callback(false, -1);		
	}
	autHandlePost = function() {
		if (data === null) {
			console.error("Empty data!");
			return dsbNextInChain();
		}
		console.log("DSB.dsbHandlePost(): operation: "+functionName);
		switch (functionName) {
			case 'Authenticate':
				var checktoken = new token(rtoken);
				checktoken.authorize(function(result) {
					console.log(result);
		            if (result != false) {
		               	callback(true, true);
						}
		            } else {
		                console.error("Not Authorize!");
		                callback(true, null);
		            }
		        });
		    break;
		    
		}
	}
	
	this.Handle = function() {
		console.log("Authetication.Handle(): functionName: "+functionName);
		if (typeof functionName === 'undefined ' || !functionName.length)
			if (typeof nextHandler !== 'undefined' && nextHandler) {
				console.log("Authetication: Not handling "+functionName);
				return nextHandler.Handle(function(processed, result) { callback(processed, result); });
			} else {
				console.log("Authetication: "+functionName+" unhandled!");
				callback(false, -1);
				return;
			}
	    if (typeof method !== 'undefined')
	        switch (method) {
	            case 'DELETE':
	                //dynamo.deleteItem(JSON.parse(event.body), done);
	                break;
	            case 'POST':
	                return dsbHandlePost();
	                break;
	            case 'PUT':
	                //dynamo.updateItem(JSON.parse(event.body), done);
	                break;
	            case 'OPTIONS':
	                //dynamo.updateItem(JSON.parse(event.body), done);
	                break;
	            default:
	                return dsbNextInChain();
	               	break;
	        }
	    // else {
	    //     console.log("DSB GET");
	    //     return dsbHandleGet();
	    // }
	}
};
// export the class
module.exports = Authentication;