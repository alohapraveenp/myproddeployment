<div class="container">
    <div class="row">
        <div class="col-md-12 left-sec">

<div ng-controller="WheelController" ng-init="moduleName='wheel'; listWheelFlag = true; viewWheelFlag=false; updateWheelFlag=false;" >

	<div id='listing' ng-show='listWheelFlag'>
		<div class="form-group"  style='margin-bottom:25px;'>
			<div class="col-sm-2">
				<div class="input-group">
					<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>
					<input type="text" class="form-control input-sm" ng-model="searchText" ng-change="paginator.setPage(0);" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" style='width:150px;'> 
				</div>
			</div>
			<div class="col-sm-1">&nbsp; </div>
			<div class="col-sm-2">
			<a href ng-click='update();' class="btn btn-success btn-sm" style='color:white;font-size:11px;width:130px;'><span class='glyphicon glyphicon-save'></span> &nbsp;Update {{wheeltype}}</a>
			</div>
			<div class="col-sm-2">
			<a href ng-click='view();' class="btn btn-info btn-sm" style='color:white;font-size:11px;width:130px;'><span class='glyphicon glyphicon-picture'></span> &nbsp;View {{wheeltype}}</a>
			</div>
			<div class="col-sm-2">
			<a href ng-click='summary();' class="btn btn-default btn-sm" style='font-size:11px;width:130px;'><span class='glyphicon glyphicon-eye-open'></span> &nbsp;Summary {{wheeltype}}</a>
			</div>
			<div class="col-sm-1">&nbsp; </div>
			<div class="col-md-2" ng-if="extratype.length > 1">
				<div class="btn-group" uib-dropdown>
					<button type="button" class="btn btn-warning btn-sm" uib-dropdown-toggle aria-expanded="false" style='font-size:11px;'>Type Wheel <span class="caret"></span></button>
					<ul class="dropdown-menu" uib-dropdown-menu role="menu">
						<li ng-repeat="x in extratype" class='glyphiconsize'><a href ng-class="{ 'glyphicon glyphicon-ok glyphiconsize': x === wheeltype }" href ng-click="setwheeltype(x)"> {{x}}</a></li>
					</ul>
				</div> 		
			</div>
		</div>
        <div style=" clear: both;"></div>

		<table width='100%' class="table table-condensed table-striped" style='font-size:13px;'>
			<thead><tr><th ng-repeat="y in tabletitle"><tbtitle var="{{y.a}}" name='tabletitle' module='moduleName'/></th></tr></thead>
			<tbody style='font-family:helvetica;font-size:13px;'>
				<tr ng-repeat="x in filteredWheel = (wheel | filter:searchText) | sizefilter:this | orderBy:predicate:reverse | slicefilter:paginator.getPage():paginator.getRowperPage():paginator.getItemCount() | limitTo:paginator.getRowperPage()" style='font-family:helvetica;font-size:12px;'>
					<td ng-repeat="y in tabletitle" width='{{y.l}}'>{{ x[y.a] | adatereverse:y.c }}</td>
				</tr><tr><td colspan='{{tabletitle.length}}'></td></tr>
			</tbody>
		</table>
		<div ng-if="filteredWheel.length >= paginator.getRowperPage()" class="col-md-12" align="center" style='font-size:11px' ng-include="'inc/paginator.html'"></div>
	</div>

	<div class="col-md-12" ng-show='viewWheelFlag'>
		<br /><a href class='btn btn-info btn-sm customColor' ng-click="backlisting()"><span class='glyphicon glyphicon-step-backward'></span> &nbsp;Back to listing</a><br />
		<p><img ng-src="{{picture}}" /></p>
	</div>

	<div class="col-md-12" ng-show='updateWheelFlag'>
		<br />
		<a href class='btn btn-info btn-sm customColor' ng-click="backlisting()"><span class='glyphicon glyphicon-step-backward'></span> &nbsp;Back to listing</a>
		<br />
		 <div class="col-md-2"></div>


		 <div class="col-md-8">
		   <table  class='table' style="font-size:12px;font-familly:Roboto">
		   <tr class="row" ng-repeat="oo in wheel track by $index">
		   <td ng-repeat="y in tablecontent track by $index" width={{y.l}}>

			<span ng-if="y.t === 'readonly'" style='font-size: 18px; color: black; text-shadow: 5px 5px 10px black;'>{{ oo[y.a] }}</span>

            <div class="input-group" ng-if="y.t === 'array'">
				<div class='input-group-btn' uib-dropdown >
				<button type='button' class='btn btn-info btn-sm' uib-dropdown-toggle >Select <span class='caret'></span></button>
				<ul class='dropdown-menu scrollable-menu' uib-dropdown-menu >
				<li ng-repeat="p in y.v track by $index"><a href ng-click="oo[y.a]=p;">{{ p }}</a></li>
				</ul>
				</div>
				<input type='text' ng-model='oo[y.a]' class='form-control input-sm' readonly >
			</div>

		   </td>
		   </tr>
		   </table>
		</div>
		
		
		<div class="col-md-2"></div>
		<div class="col-md-7"></div>
		<div class="col-md-5">
			<a href ng-click='savenewwheel();' class="btn btn-success btn-sm" style="color:white;width:200px"><i class='glyphicon glyphicon-save'></i> &nbsp;{{ buttonlabel }} </a><br />
		</div>
		</div>
		
	<div class="col-md-12" ng-show='summaryWheelFlag'>
		<br />
		<a href class='btn btn-info btn-sm customColor' ng-click="backlisting()"><span class='glyphicon glyphicon-step-backward'></span> &nbsp;Back to listing</a>
		<br />
		 <div class="col-md-2"></div>
		 <div class="col-md-8">
		   <table  class='table' style="font-size:12px;font-familly:Roboto">
		   <tr class="row" ng-repeat="oo in parts track by $index">
		   <td style='font-size: 18px; color: black; text-shadow: 5px 5px 10px black;' width='150'>{{ oo.index }}</td><td style='font-size: 14px;'>{{ oo.text }}</td>
		   </tr>
		   <tr class="row" ><td colspan='2'> </td></tr>
		   </table>
		</div>
				
</div>

</div>
</div>
</div>

<script>

<? 
$mediadata = new WY_Media($theRestaurant); 
printf("var extratype = [");
for($i = 0, $sep = ""; $i < count($extratype); $i++, $sep = ", ") 
printf("%s '%s'", $sep, $extratype[$i]);
printf("];");
?>
 
var pathimg = <?php echo "'". $mediadata->getFullPath('small')."';"; ?>
var token = <?php echo "'".$_SESSION['user_backoffice']['token']."';"; ?>

app.controller('WheelController', ['$scope', 'bookService', function($scope, bookService) {
	
	$scope.paginator = new Pagination(25);
	$scope.restaurant = <?php echo "'".$theRestaurant."';"; ?>
	$scope.email = <?php echo "'" . $email . "';"; ?>
	$scope.predicate = '';
	$scope.parts = [];
	$scope.oo = null;
	$scope.reverse = false;
	$scope.extratype = extratype.slice(0);
	$scope.wheeltype = $scope.extratype[0];
	$scope.picture = pathimg + $scope.wheeltype + '_wheel.png?' + new Date().getTime();
	$scope.tabletitle = [ {a:'morder', b:'Order', c:'' , q:'down', cc:'fuchsia', l:'10%' }, {a:'offer', b:'Offer', c:'' , q:'down', cc: 'black', l:'15%' }, {a:'value', b:'Value', c:'' , q:'down', cc: 'black', l:'10%' }, {a:'description', b:'Description', c:'' , q:'down', cc: 'black', l:'50%' } ];
	$scope.tablecontent = [ {a:'morder', b:'index', c:'' , q:'down', cc: 'black', l:'10%', t:'readonly' }, {a:'offer', b:'Offer', c:'' , v:[], q:'down', cc: 'black', l:'90%', t:'array' } ];
	$scope.bckups = $scope.tabletitle.slice(0);
	$scope.buttonlabel = "Update wheel";

	$scope.getAlignment = bkgetalignment;

	$scope.Objwheel = function() {
		return {
			index: 0, 
			offer: '',
			description: '', 
			value: '', 
			type: '', 
			morder: '', 
			
			remove: function() { 
				for(var i = 0; i < $scope.wheel.length; i++) 
					if($scope.wheel[i].offer === this.offer) {
						$scope.wheel.splice(i, 1);
						break;
						}
				},
				
			replicate: function(obj) {
				for (var attr in this)
		        	if(this.hasOwnProperty(attr) && typeof this[attr] !== 'function' && attr !== '$$hashKey') 
        				this[attr] = obj[attr];
				return this;
				},
										
			clean: function() {

				for (var attr in this)
		        	if(this.hasOwnProperty(attr) && typeof this[attr] !== 'function' && attr !== '$$hashKey') {
        				if(typeof this[attr] === 'string') {
        					this[attr] = this[attr].replace(/\'|\"/g, '’');
        					}
        				}
				return this;
				},
					
			check: function() {
				if(this.offer === '' || this.offer.length < 6) { alert('Invalid title offer, empty or too short(5)'); return -1;}
				return 1;
				}
			};
	};

	$scope.init = function() {
		$scope.wheel = (function() { 
			var i, oo, arr=[]; 
			for(i = 1; i <= 24; i++) {
				oo = new $scope.Objwheel();
				oo.offer = $scope.wheeloffers[3].offer;
				oo.value = $scope.wheeloffers[3].value;
				oo.description = $scope.wheeloffers[3].description;
				oo.index = oo.morder = i;
				arr.push(oo); 
				}
			return arr; })();
		}

	$scope.initorder = function() {
		$scope.tabletitle = $scope.bckups;
		$scope.predicate = "morder";
		$scope.reverse = false;
	};

	$scope.reorder = function(item, alter) {
		alter = alter || "";
		if (alter !== "")  item = alter;
		$scope.reverse = ($scope.predicate === item) ? !$scope.reverse : false;
		$scope.predicate = item;
	};

	$scope.setwheeltype = function(newtype) {
		$scope.wheeltype = newtype;
		$scope.buttonlabel = "Update wheel " + $scope.wheeltype;
		$scope.readWheel();
		}

	$scope.readWheelOffer = function() {
		var i, data, v, limit;
		bookService.readWheelOffers($scope.restaurant, $scope.email, $scope.wheeltype).then(function(response) {
			if(response.status !== 1 && response.status !== "1") {
				$scope.init();
				return;
				}
				
			for(i = 0; i < $scope.tablecontent.length; i++) 
				if($scope.tablecontent[i].a == "offer")
					break;
			if(i >= $scope.tablecontent.length)
				return;
		
			v = $scope.tablecontent[i].v;
			$scope.wheeloffers = [];	 	
			data = response.data;
			for (i = 0; i < data.length; i++) {
				data[i].index = i + 1;
				data[i].morder = parseInt(data[i].morder);
				$scope.wheeloffers[i] = new $scope.Objwheel().replicate(data[i]);
				v.push($scope.wheeloffers[i].offer);
				}
			console.log('OFFER', $scope.wheeloffers);
			});
		}
		
	$scope.readWheel = function() {
		var i, data, v, limit;
		bookService.readWheel($scope.restaurant, $scope.email, $scope.wheeltype).then(function(response) {
			$scope.wheel = [];	 	
			if(response.status !== 1 && response.status !== "1") {
				limit = ($scope.wheeloffers.length >= 24) ? 24 : $scope.wheeloffers.length
				for (i = 0; i < limit; i++) 
					$scope.wheel.push(new $scope.Objwheel().replicate($scope.wheeloffers[i]));
				bookService.updateWheel($scope.restaurant, $scope.email, $scope.wheel, $scope.wheeltype).then(function(response) { alert("wheel " + $scope.wheeltype + " has been created"); });
				return;
				}
			
			data = response.data;
			while(data.length < 24) data.push($scope.wheeloffers[0]);
				
			limit = (data.length >= 24) ? 24 : data.length		
			for (i = 0; i < limit; i++) {
				data[i].index = i+1;
				data[i].morder = parseInt(data[i].morder);
				if(data[i].morder !== data[i].index)
					data[i].morder = data[i].index;
				$scope.wheel.push(new $scope.Objwheel().replicate(data[i]));
				}
					
			console.log('WHEEL', $scope.wheel);
			$scope.paginator.setItemCount($scope.wheel.length);
			$scope.initorder();
			});
		}
		
	$scope.readWheelOffer();
	$scope.setwheeltype('corporate');
		
	$scope.cleaninput = function(ll) {
		if(typeof $scope.selectedItem[ll] === 'string')
			$scope.selectedItem[ll] =  $scope.selectedItem[ll].replace(/\'|\"/g, '’');
		};
		
	$scope.reset = function(item) {
		$scope.listWheelFlag = false;
		$scope.viewWheelFlag = false
		$scope.updateWheelFlag = false;
		$scope.summaryWheelFlag = false;
		$scope[item] = true;	
		};

	$scope.backlisting = function() {
		$scope.reset('listWheelFlag');
		};
		

	$scope.view = function() {
		$scope.picture = pathimg + $scope.wheeltype + '_wheel.png?' + new Date().getTime();
		$scope.reset('viewWheelFlag');
		};

	$scope.update = function() {
		$scope.reset('updateWheelFlag');
		};

	$scope.summary = function() {
		var i, val, data = [], total = 0;
		$scope.parts = [];
		for(i = 0; i < 24; i++) {
			val = $scope.wheel[i].offer;
			total += parseInt($scope.wheel[i].value);
			if(val === "") continue;
			else if(typeof data[val] !== "undefined") 
				data[val]++;
			else data[val] = 1;
			}
		
		for(val in data) {
		    if(data.hasOwnProperty(val) && typeof data[val] === 'number') 
		    	$scope.parts.push({index: 0, text: val, count: data[val] });
			}
		$scope.parts.sort(function(a, b) { return a.count - b.count; });
		for(i = 0; i < $scope.parts.length; i++) {
			$scope.parts[i].index = i+1;
			$scope.parts[i].text = "(" + $scope.parts[i].count + " parts) " + $scope.parts[i].text;
			}
		$scope.parts.push({ index: 'Total Value ' + total, text:' ', count:0 });
		$scope.reset('summaryWheelFlag');
		};

	$scope.savenewwheel = function() {
		var type = $scope.wheeltype;
				
		bookService.updateWheel($scope.restaurant, $scope.email, $scope.wheel, $scope.wheeltype).then(function(response) { alert("wheel " + type + " has been updated"); });
		
 		$scope.backlisting();		
		};
}]);
	
</script>
