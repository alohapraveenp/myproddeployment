
<div class="container">
    <div class="row">
        <div class="col-md-12 left-sec">

<div ng-controller="DocumentController" ng-init="moduleName='document'; listingDocFlag = true; listing1DocFlag=false; updateDocFlag=false; renameImageFlag=false; $scope.createDocFlag = false;" >

	<div id='listing' ng-show='listingDocFlag'>
		<div class="form-group"  style='margin-bottom:25px;'>
			<div class="col-md-4">
				<div class="input-group col-md-4">
					<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>
					<input type="text" class="form-control input-sm" ng-model="searchText" ng-change="paginator.setPage(0);" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" style='width:200px;'> 
				</div>
			</div>
			<div class="col-md-2"></div>

			<div class="col-md-2">
				<div class="btn-group" uib-dropdown>
				<button type="button" class="btn btn-default btn-xs" uib-dropdown-toggle aria-expanded="false" style='font-size:11px;'>Page Size<span class="caret"></span></button>
				<ul class="dropdown-menu" uib-dropdown-menu role="menu">
				<li ng-repeat="x in paginator.pagerange()" class='glyphiconsize'><a href ng-class="{ 'glyphicon glyphicon-ok glyphiconsize': x === paginator.getRowperPage() }" ng-click="paginator.setRowperPage(x)"> {{x}}</a></li>
				</ul>
				</div> 		
			</div>

			<div class="col-md-4">
			<a href ng-click='create();' class="btn btn-success btn-sm" style='color:white;width:200px;'><span class='glyphicon glyphicon-certificate'></span> &nbsp;Create a New Image</a>
			</div>
		</div><br/>

		<table width='100%' class="table table-condensed table-striped" style='font-family:helvetica;font-size:12px;'>
			<thead>
				<tr>
				<th ng-repeat="y in tabletitle"><tbtitle var="{{y.a}}" name='tabletitle'  module='moduleName'/></th>
				<th>rename</th><th> &nbsp; </th><th>update</th><th> &nbsp; </th><th>delete</th>
				</tr>
			 </thead>
			<tr ng-repeat="x in filteredImage = (names | filter:searchText) | sizefilter:this | orderBy:predicate:reverse | slicefilter:paginator.getPage():paginator.getRowperPage():paginator.getItemCount() | limitTo:paginator.getRowperPage()" ng-if="formatDisplay!='today'" style='font-family:helvetica;font-size:11px;'>
				<td ng-repeat="y in tabletitle"><a href ng-click="view(x)">{{ x[y.a] }}</a></td>
				<td align='center'><a href ng-click="rename(x)"><span class='glyphicon glyphicon-tag purple'></span></a></td>
				<td width='30'>&nbsp;</td>
				<td align='center'><a href ng-click="update(x)"><span class='glyphicon glyphicon-pencil blue'></span></a></td>
				<td width='30'>&nbsp;</td>
				<td align='center'><a href ng-click="delete(x)"><span class='glyphicon glyphicon-trash red'></span></a></td>
			</tr>
			<tr><td colspan='{{ tabletitle.length + 5}}'></td></tr>
		</table>
		<div ng-if="filteredImage.length >= paginator.getRowperPage()" class="col-md-12" align="center" style='font-size:11px' ng-include="'inc/paginator.html'"></div>
	</div>

	<div class="col-md-12" ng-show='listing1DocFlag'>
		<br />
		<a href class='btn btn-info btn-sm customColor' ng-click="backlisting()"><span class='glyphicon glyphicon-step-backward'></span> &nbsp;Back to listing</a>
		<br />
		<table class='table-striped' style="margin: 0 0 150px 30px;font-size:13px;font-family: Roboto">		
                    <tr ng-repeat="y in tableContent" ><td><strong> {{ y.a }}: </strong></td><td> &nbsp; </td><td>{{ selectedItem[y.a] }}</td></tr>
                     <tr ng-if="selectedItem.picture != ''"><td colspan='3' align='right'><br/><br/> <a ng-href="{{path}}{{selectedItem.name}}" target="_blank">DOWNLOAD FILE</a></td></tr>
		</table>
	</div>

	<div class="col-md-12" ng-show='renameImageFlag'>
		<br />
		<a href class='btn btn-info btn-sm customColor' ng-click="backlisting()"><span class='glyphicon glyphicon-step-backward'></span> &nbsp;Back to listing</a>
		<br />
		 <div class="col-md-2"></div>
		 <div class="col-md-8">
				<div class="input-group">
					<span class="input-group-addon"><i class="glyphicon glyphicon-tag"></i></span>
					<input type="text" class="form-control input-sm" ng-model="selectedItem.name" placeholder="current name of the media" readonly > 
				</div><br />

				<div class="input-group">
					<span class="input-group-addon"><i class="glyphicon glyphicon-tag"></i></span>
					<input type="text" class="form-control input-sm" ng-model="selectedItem.newname" placeholder="new name of the media"> 
				</div><br />
				<div ng-if="selectedItem.picture != ''" class="row"><img ng-src="{{path}}{{selectedItem.name}}" height='200'/></div><br /><br />							
				<div class="col-md-4">
				<a href ng-click='renamemedia();' class="btn btn-success btn-sm" style='color:white;width:200px;'><span class='glyphicon glyphicon-save'></span> &nbsp;{{ buttonlabel }} </a><br /><br /><br /><br />
				</div>
		</div>
		 <div class="col-md-2"></div>
	</div>

	<div class="col-md-12" ng-show='createImageFlag'>
		<br />
		<a href class='btn btn-info btn-sm customColor' ng-click="backlisting()"><span class='glyphicon glyphicon-step-backward'></span> &nbsp;Back to listing</a>
		<br />
		 <div class="col-md-2"></div>
		 <div class="col-md-8">
			<div class='input-group'>
			<div class='input-group-btn' uib-dropdown >
			<button type='button' class='btn btn-info btn-sm' uib-dropdown-toggle >Category <span class='caret'></span></button>
			<ul class='dropdown-menu' uib-dropdown-menu role="menu" aria-labelledby="single-button">
			<li ng-repeat="p in objectTypelist"><a href ng-click="selectedItem.object_type=p;">{{ p }}</a></li>
			</ul></div><input type='text' class='form-control input-sm' ng-model="selectedItem.object_type" required>
			</div><br />

			<div class="input-group">
				<span class="input-group-addon"><i class="glyphicon glyphicon-file"></i></span>
				<input type="file" name="files_upload" id="files_upload" class="form-control input-sm" ng-model="nfiles" placeholder="ipload files">  
			</div><br />
                        <div id="progressbox" style="display:none;" ><div id="progressbar" ></div><div id="statustxt">0%</div> </div><br/>
			
			<div class="col-md-4">
			<a href  ng-click='uploadFiles();' class="btn btn-success btn-sm" style='color:white;width:200px;'><span class='glyphicon glyphicon-save'></span> &nbsp;{{ buttonlabel }} </a><br /><br /><br /><br />
			</div>
                        

		</div>
		 <div class="col-md-2"></div>
	</div>

	<div class="col-md-12" ng-show='updateDocFlag'>
		<br />
		<a href class='btn btn-info btn-sm customColor' ng-click="backlisting()"><span class='glyphicon glyphicon-step-backward'></span> &nbsp;Back to listing</a>
		<br />
		 <div class="col-md-2"></div>
		 <div class="col-md-8">

				<div class="input-group">
					<span class="input-group-addon"><i class="glyphicon glyphicon-tag"></i></span>
					<input type="text" class="form-control input-sm" ng-model="selectedItem.name" placeholder="name of the media" readonly required> 
				</div><br />

				<div class='input-group'>
				<div class='input-group-btn' uib-dropdown >
				<button type='button' class='btn btn-info btn-sm' uib-dropdown-toggle >Category <span class='caret'></span></button>
				<ul class='dropdown-menu' uib-dropdown-menu role="menu" aria-labelledby="single-button">
				<li ng-repeat="p in objectTypelist"><a href ng-click="selectedItem.object_type=p;">{{p}}</a></li>
				</ul></div><input type='text' class='form-control input-sm' ng-model="selectedItem.object_type" required>
				</div><br />

				<div class='input-group'>
				<div class='input-group-btn' uib-dropdown >
				<button type='button' class='btn btn-info btn-sm' uib-dropdown-toggle >Status <span class='caret'></span></button>
				<ul class='dropdown-menu' uib-dropdown-menu role="menu" aria-labelledby="single-button">
				<li ng-repeat="p in ['active', 'inactive']"><a href ng-click="selectedItem.status=p">{{p}}</a></li>
				</ul></div><input type='text' class='form-control input-sm' ng-model="selectedItem.status" required>
				</div><br />

				<div class="input-group">
					<span class="input-group-addon"><i class="glyphicon glyphicon-sort"></i></span>
					<input type="text" class="form-control input-sm" ng-model="selectedItem.morder" placeholder="order preference"> 
				</div><br />

				<div class="input-group">
					<span class="input-group-addon"><i class="glyphicon glyphicon-list"></i></span>
					<textarea class="form-control input-sm" ng-model="selectedItem.description" rows="5" placeholder="description"></textarea>
				</div><br />

				<div ng-if="selectedItem.picture != ''" class="row"><img ng-src="{{path}}{{selectedItem.name}}" height='200'/></div><br /><br />							
							
				<div class="col-md-4">
				<a href ng-click='updatemedia();' class="btn btn-success btn-sm" style='color:white;width:200px;'><span class='glyphicon glyphicon-save'></span> &nbsp;{{ buttonlabel }} </a><br /><br /><br /><br />
				</div>
		</div>
		 <div class="col-md-2"></div>
	</div>

</div>

</div>
</div>
</div>
<style>
    #progressbox {
    border: 1px solid #0099CC;
    padding: 1px; 
    position:relative;
    width:400px;
    border-radius: 3px;
    margin: 10px;
    display:none;
    text-align:left;
    }
    #progressbar {
    height:20px;
    border-radius: 3px;
    background-color: #49afcd;
    width:1%;
    }
    #statustxt {
    top:3px;
    left:50%;
    position:absolute;
    display:inline-block;
    color: #000000;
    }
</style>

<script>

<?php


global $super_member_type_allowed;

$mediadata = new WY_Media($theRestaurant);
printf("var pathimg = '%s';", $mediadata->getPartialPath());

$object = "";
$supermember = (isset($_SESSION['user_backoffice']['member_type']) && in_array($_SESSION['user_backoffice']['member_type'], $super_member_type_allowed));
$promotionflg = $_SESSION['restaurant']['promotion'];
for($i = 0, $sep = ""; $i < count($picture_typelist); $i++, $sep = ",")
	if($picture_typelist[$i] != "profile" && 
		($picture_typelist[$i] != "promotion" || $promotionflg) &&
		($picture_typelist[$i] != "sponsor" || $supermember) )
		$object .= $sep . "'" . $picture_typelist[$i] . "'";

printf("\nvar type_object = [%s];", $object);

?>

var token = <?php echo "'".$_SESSION['user_backoffice']['token']."';"; ?>
var resto = <?php echo "'".$theRestaurant."';"; ?>
var selectionAr;

app.controller('DocumentController', function($scope, $http) {

	
	$scope.paginator = new Pagination(25);
	$scope.restaurant = <?php echo "'".$theRestaurant."';"; ?>
	$scope.email = <?php echo "'".$email."';"; ?>
	$scope.path = pathimg;
	$scope.typeselection = 1;
	$scope.tmpArr = [];
	$scope.predicate = '';
	$scope.reverse = false;
	$scope.tabletitle = [ {a:'index', b:'index' , q:'down', cc: 'fuchsia' }, {a:'name', b:'Title' , q:'down', cc: 'black' }, {a:'object_type', b:'Type' , q:'down', cc: 'black' }, {a:'status', b:'Status' , q:'down', cc: 'black' }, {a:'morder', b:'List Order' , q:'down', cc: 'black' } ];
	$scope.bckups = $scope.tabletitle.slice(0);
	$scope.tableContent = [ {a:'name', b:'Title' }, {a:'object_type', b:'Object Type' }, {a:'status', b:'Status' }, {a:'media_type', b:'Media Type' }, {a:'morder', b:'List Order' }, {a:'description', b:'Description' } ];
	$scope.objectTypelist = type_object.slice(0);
	
	$scope.getAlignment = bkgetalignment;

	$scope.Objmedia = function() {
		return {
			restaurant: $scope.restaurant,
			index: '',
			name: '',
			newname: '',
			path: '',
			object_type: 'restaurant',
			media_type: '',
			status: '',
			description: '',
			morder: '',
			
			remove: function() { 
				for(var i = 0; i < $scope.names.length; i++) 
					if($scope.names[i].name === this.name) {
						$scope.names.splice(i, 1);
						break;
						}
				},
				
			replicate: function(obj) {
				for (var attr in this)
		        	if(this.hasOwnProperty(attr) && typeof this[attr] !== 'function' && attr !== '$$hashKey') 
        				this[attr] = obj[attr];
				return this;
				},		
								
			clean: function() {
				for (var attr in this)
		        	if(this.hasOwnProperty(attr) && typeof this[attr] !== 'function' && attr !== '$$hashKey') {
        				if(typeof this[attr] === 'string')
        					this[attr] = this[attr].replace(/\'|\"/g, '’');
        				}
				return this;
				}	
			};
		};
		
	$scope.initorder = function() {
		$scope.tabletitle = $scope.bckups;
		$scope.predicate = "index";
		$scope.reverse = false;
	};

	$scope.reorder = function(item, alter) {
		alter = alter || "";
		if (alter !== "")  item = alter;
		$scope.reverse = ($scope.predicate === item) ? !$scope.reverse : false;
		$scope.predicate = item;
	};
				
	var url = '../api/restaurant/media/picture/' + $scope.restaurant + "/pdf";
	$http.get(url).success(function(response) {	
		var i, data;
		
		$scope.names = [];
		data = response.data;
		for (var i = 0; i < data.length; i++)
			if(data[i].name.indexOf('wheel') != 0 || ( data[i].object_type == 'sponsor' && $scope.objectTypelist.indexOf('sponsor') >= 0) ) {
				data[i].index = i + 1;
				$scope.names.push(new $scope.Objmedia().replicate(data[i]));
			}
		$scope.paginator.setItemCount($scope.names.length);
		$scope.initorder();
		});

	$scope.reset = function(item) {
               
		$scope.listingDocFlag = false;
		$scope.listing1DocFlag = false;
		$scope.updateDocFlag = false
		$scope.renameImageFlag = false
		$scope.createDocFlag = false;
		$scope[item] = true;	
		};

	$scope.backlisting = function() {
            console.log("listingDocFlag");
		$scope.reset('listingDocFlag');
		};
		
	$scope.view = function(oo) {
		$scope.selectedItem = oo;
		$scope.reset('listing1DocFlag');
		};
	
	$scope.create = function() {	
		$scope.selectedItem = new $scope.Objmedia();
		$scope.reset('createImageFlag');
		$scope.buttonlabel = "Save new media";
		$scope.action = "create";
		dom = $('#files_upload')[0];
		if(dom.files.length > 0) dom.files[0].name = '';
		return false;
		}
				
	$scope.rename = function(oo) {
		$scope.selectedItem = new $scope.Objmedia().replicate(oo);
		$scope.reset('renameImageFlag');
		$scope.buttonlabel = "Rename new media";
		$scope.action = "rename";
		return false;
		}
				
	$scope.update = function(oo) {
		$scope.selectedItem = new $scope.Objmedia().replicate(oo);
		$scope.reset('updateDocFlag');
		$scope.buttonlabel = "Update new media";
		$scope.action = "update";
		};


	$scope.renamemedia = function() {
		var newext, ext, i;
		$scope.selectedItem.newname = $scope.selectedItem.newname.replace(/[!@#$%^*()=}{\]\[\"\':;><\?/|\\]/g, ' ');
		
		if($scope.selectedItem.newname.length < 6) {
			alert("image name require 6 characters or more..." + $scope.selectedItem.newname.length);
			return;
			}
			
		newext = $scope.selectedItem.newname.substring($scope.selectedItem.newname.lastIndexOf(".")+1)
		ext = $scope.selectedItem.name.substring($scope.selectedItem.name.lastIndexOf(".")+1)
		if(newext != ext) {
			alert("Images can only be renamed with the same extention (" + newext + "/" + ext + ")");
			return false;
			}
		for(i = 0; i < $scope.names.length; i++)
			if($scope.names[i].name == $scope.selectedItem.newname) {
				alert("this name " + $scope.selectedItem.newname + " already exists. Please choose another one.");
				return false;
				}

		apiurl = '../api/renamemedia';
		$.ajax({
			url : apiurl,
			type: "POST",
			data: {
				restaurant: $scope.restaurant,
				name: $scope.selectedItem.name,
				newname: $scope.selectedItem.newname,
				type: $scope.selectedItem.media_type,
				category: $scope.selectedItem.object_type,
				token:token
				},
			success: function(data, textStatus, jqXHR) { 
				alert("'" + $scope.selectedItem.newname + "' has been renamed"); 
				},
			error: function (jqXHR, textStatus, errorThrown) { 
				alert("Unknown Error. Image '" + $scope.selectedItem.newname + "' has NOT been renamed." + textStatus); 
				}
			});
			
		for(i = 0; i < $scope.names.length; i++)
			if($scope.names[i].name == $scope.selectedItem.name) {
				$scope.names[i].name = $scope.selectedItem.newname;
				break;
				}
		 $scope.backlisting();
		};
		
	$scope.delete = function(oo) {
		var i, apiurl = '../api/deletemedia', recordname;

		if(confirm("Are you sure you want to delete " + oo.name) == false)
			return;
		
		recordname = oo.name;
		$.ajax({
			url : apiurl,
			type: "POST",
			data: {
				restaurant: $scope.restaurant,
				name: oo.name,
				category: oo.object_type,
				token:token
				},
			success: function(data, textStatus, jqXHR) { alert(recordname + " has been deleted");  },
				error: function (jqXHR, textStatus, errorThrown) { alert("Unknown Error. " + recordname + " Image has NOT been deleted. " + textStatus); }
			});
		
		oo.remove();		
 		$scope.backlisting();		
		};

	$scope.updatemedia = function() {
		var i, recordname;
		
		$scope.selectedItem.clean();		
		$scope.selectedItem.morder = $scope.selectedItem.morder.replace(/[^0-9]/g, ' ');
		if($scope.selectedItem.morder == '')
			return alert('Invalid number');

		apiurl = '../api/updatemedia';
		for(i = 0; i < $scope.names.length; i++)
			if($scope.names[i].name === $scope.selectedItem.name) {
				$scope.names.splice(i, 1);
				$scope.names.push($scope.selectedItem);
				break;
				}
				
		recordname = $scope.selectedItem.name;		
		$.ajax({
			url : apiurl,
			type: "POST",
			data: {
				restaurant: $scope.restaurant,
				name: $scope.selectedItem.name,
				category: $scope.selectedItem.object_type,
				type: $scope.selectedItem.media_type,
				description: $scope.selectedItem.description,
				status: $scope.selectedItem.status,
				morder: $scope.selectedItem.morder,
				token:token
				},
			success: function(data, textStatus, jqXHR) { alert("'" + recordname + "' has been updated"); },
				error: function (jqXHR, textStatus, errorThrown) { alert("Unknown Error. Image '" + recordname + "'has NOT been updated." + textStatus); }
			});
		
 		$scope.backlisting();		
		};

	// Grab the files and set them to our variable
	//function prepareUpload(event) { files = event.target.files; }

	// Catch the form submit and upload the files
	$scope.uploadFiles = function()	{

		var object_type = $scope.selectedItem.object_type;
		dom = $('#files_upload')[0];

		files = dom.files;  //get the DOM -> files = document.getElementById('files_upload').files;
		
		if(files.length == 0) {
			alert("Select/Drop one file");
			return false;
			}

		filename = files[0].name;
		ext = filename.substring(filename.lastIndexOf(".")+1).toLowerCase();
		if(ext != "jpg" && ext != "jpeg" && ext != "png" && ext != "gif" && ext != "pdf") {
			alert("Invalid file type (jpg, jpeg, png, gif, pdf)");
			return false;
			}
		if(object_type == "") {
			alert("Please Choose a category");
			return false;
			}
			
		$scope.selectedItem.name = files[0].name;
		$scope.selectedItem.object_type = object_type;
		if(ext != 'pdf'){
            $scope.selectedItem.media_type = 'picture';
        }else if(ext == 'pdf'){
            $scope.selectedItem.media_type = 'pdf';
        	}
        
		$scope.selectedItem.path = '';
		$scope.selectedItem.status = 'inactive';
		$scope.selectedItem.description = '';
		$scope.selectedItem.morder = '99';
		$scope.selectedItem.index = ($scope.names.length > 0) ? $scope.names[$scope.names.length - 1].index + 1 : 1;

		$scope.names.push( $scope.selectedItem );
		

       // START A LOADING SPINNER HERE

        // Create a formdata object and add the files
		var data = new FormData();
		$.each(files, function(key, value)
		{
			data.append(key, value);
		});

		data.append('token', token);
		data.append('restaurant', $scope.restaurant);
		data.append('category', $scope.selectedItem.object_type);
        data.append('media_type', $scope.selectedItem.media_type);
        $("#progressbox").css('display','block');

        $.ajax({
            url: 'saveimages.php?files',
            type: 'POST',
            data: data,
            cache: false,
            dataType: 'json',
            processData: false, // Don't process the files
            contentType: false,
            xhr: function() {
                    var myXhr = $.ajaxSettings.xhr();
                    if(myXhr.upload){
                        myXhr.upload.addEventListener('progress',progressHandlingFunction, false);
                    }
                     return myXhr;
                },// Set content type to false as jQuery will tell the server its a query string request
            success: function(data, textStatus, jqXHR)
            	{
            	if(typeof data.error === 'undefined')
            		$scope.submitForm(dom, data);
                       
            	else {
            		alert(data.error);
            		$scope.selectedItem.remove();
            		$scope.$apply();
            		}
                   
             },
            error: function(jqXHR, textStatus, errorThrown) {
           		alert(textStatus);		
            }
        });
      
    };
    
    function progressHandlingFunction(e){
            console.log("in progress");
        var max = e.total;
        var current = e.loaded;
         var percent = (current / max) * 100;

         if(percent>80)
            {
                  console.log(percent);
                $("#statustxt").css('color','#fff'); //change status text to white after 50%
            }
        $('#progressbar').width(percent + '%') //update progressbar percent complete
        $('#statustxt').html(percent + '%');
    }

    $scope.submitForm = function(event, data) {
		// Create a jQuery object from the form
		$form = $(event.target);
		
		// Serialize the form data
		var formData = $form.serialize();
		
		// You should sterilise the file names
		$.each(data.files, function(key, value)
		{
			formData = formData + '&filenames[]=' + value;
		});

		formData = formData + '&token=' + token + '&restaurant=' + $scope.restaurant + '&category=' + $scope.selectedItem.object_type;

		$.ajax({
			url: 'saveimages.php',
            type: 'POST',
            data: formData,
            cache: false,
            dataType: 'json',
            success: function(data, textStatus, jqXHR) {
                
            	if(typeof data.error === 'undefined') {
            		// Success so call function to process the form
            		alert('The file ' + $scope.selectedItem.name + ' has been uploaded');
                        
                       
            		console.log('SUCCESS: ' + data.success);
                        
            		}
            	else { console.log('ERRORS: ' + data.error); /* Handle errors here */ }
            	},
            error: function(jqXHR, textStatus, errorThrown) { console.log('ERRORS: ' + textStatus); /* Handle errors here */ },
            complete: function() {
                 $("#progressbox").css('display','none');
                   window.location.reload();
                 /* STOP LOADING SPINNER  */ }
           
		});
                
	};

		
        
	
	});


</script>
