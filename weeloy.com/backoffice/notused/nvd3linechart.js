app.controller('LineChartCtrl', function($scope, $timeout, bookService) {
	
	$scope.year = new Date().getFullYear();
	$scope.restaurant = chrtresto;
	$scope.restotitle;
	$scope.email = chrtemail;
	$scope.type = "c";	// by create date
	$scope.field = ($scope.type === "c") ? "cweek" : "rweek";	// by create date
	$scope.nodata = false;
	$scope.prebooking = [];
	$scope.booking = [];
	$scope.cmode = 'bookings';
	$scope.ccmode = 'pax';
	$scope.Title = 'Reservation Performance';
	$scope.subTitle = 'this chart shows the different type of bookings and pax received by a period of one week';
	$scope.caption = '<b>Figure 1.</b> <span style="color: darkred;">The x axis </span>represent the week of the year, <span style="text-decoration: underline;">28 being the 28th week of that year.</span> Booking numbers are cummulated. <i>It is all transactions, minus cancellations and noshows.</i>';	
	
	$scope.analyticsObj = function(index, cweek, rweek) {
		return {
			index: index,
			cweek: cweek,
			rweek: rweek,
			lunch: 0,
			dinner: 0,
			transaction: 0,
			booking: 0,
			facebook: 0,
			website: 0,
			weeloy: 0,
			tms: 0,
			callcenter: 0,
			cctms: 0,
			walkin: 0,
			waiting: 0,
			thirdparty: 0,
			wheelwin: 0,
			cancel: 0,
			valid: 0,
			win: 0,
			noshow: 0
			};	 			
		};

	$scope.options = {
		chart: {
			type: 'lineChart',
			height: $("#lineChartDiv").height() - 190,
			width: $("#lineChartDiv").width(),
			margin : {
				top: 20,
				right: 20,
				bottom: 40,
				left: 55
				},
				
			x: function(d){ return d.x; },
			
			y: function(d){ return d.y; },
			
			useInteractiveGuideline: true,
			
			dispatch: {
				stateChange: function(e){ console.log("stateChange"); },
				changeState: function(e){ console.log("changeState"); },
				tooltipShow: function(e){ console.log("tooltipShow"); },
				tooltipHide: function(e){ console.log("tooltipHide"); }
				},
				
			xAxis: {
				axisLabel: 'weeks of the year ' + $scope.year
				},
				
			yAxis: {
				axisLabel: 'Bookings',
				tickFormat: function(d){
					return d3.format('')(d);
					},
				axisLabelDistance: -10
				},
				
			callback: function(chart){
				console.log("!!! lineChart callback !!!");
				}
			},
			
		title: {
			enable: true,
			text: ''
			},
			
		subtitle: {
			enable: true,
			text: '',
			css: {
				'text-align': 'center',
				'margin': '10px 13px 30px 7px'
				}
			},
			
		caption: {
			enable: true,
			html: ' ',
			css: {
				'text-align': 'justify',
				'font-size': '11px',
				'margin': '20px 13px 0px 7px'
				}
			}
		};

	 $scope.compare = function(a, b) {
	 	return (a[$scope.field] - b[$scope.field]);
	 	};

	$scope.togglemode = function() {
		$scope.cmode = ($scope.cmode !== "pax") ? "pax" : "bookings";
		$scope.ccmode = ($scope.cmode !== "pax") ? "pax" : "bookings";
		$scope.chartinit($scope.cmode);
		};
		
	$scope.stats = function(mode) {
		var i, oo, obj, index, first, last, cw, delta;

		$scope.booking = [];

		index = 0;
		oo = $scope.prebooking; 
		x = oo[0];
		cw = x[$scope.field];
		obj = new $scope.analyticsObj(index++, x.cweek, x.rweek);
		for(i = 0; i < oo.length; i++) {
			x = oo[i];
			if(cw != x[$scope.field]) {
				obj.cctms = obj.tms + obj.callcenter;
				obj.booking = obj.transaction - (obj.cancel + obj.noshow);
				$scope.booking.push(obj);
				obj = new $scope.analyticsObj(index++, x.cweek, x.rweek);
				}

			cw = x[$scope.field];
			delta = (mode !== "pax") ? 1 : x.cover;
			obj.transaction += delta;
			if(x.status === "cancel") obj.cancel += delta;
			else if(x.status === "noshow" || x.state.indexOf("no show") >= 0) obj.noshow += delta;
			else if(x.type === "thirdparty") obj.thirdparty += delta;
			else if(x.tracking.indexOf("website") >= 0) obj.website += delta;
			else if(x.tracking.indexOf("facebook") >= 0) obj.facebook += delta;
			else if(x.tracking.indexOf("walkin") >= 0) obj.walkin += delta;
			else if(x.tracking.indexOf("waiting") >= 0) obj.waiting += delta;
			else if(x.tracking.indexOf("tms") >= 0) obj.tms += delta;
			else if(x.tracking.indexOf("callcenter") >= 0) obj.callcenter += delta;
			else { obj.weeloy += delta; }
			if(x.status !== "noshow" && x.state.indexOf("no show") < 0 && x.status !== "cancel") {
				if(x.lunch > 0) obj.lunch += delta;
				if(x.dinner > 0) obj.dinner += delta;
				}

			if(x.wheelwin !== '') obj.win += delta;

			if(x.status !== "cancel" && 
			   x.status !== "noshow" && 
			   x.state !== "no show" && 
			   x.tracking.indexOf("callcenter") < 0)
				obj.valid += delta;
			}

		obj.cctms = obj.tms + obj.callcenter;
		obj.booking = obj.transaction - (obj.cancel + obj.noshow);
		$scope.booking.push(obj);
		wl_stats_data = $scope.booking.slice(0);
		};
			
	$scope.readData = function() { 	
		bookService.readBkgdata($scope.year, $scope.type, $scope.email, $scope.restaurant).success(function(response) { 	 
			var i, oo, obj, index, first, last, cw;

			$scope.prebooking = [];

			if(response.status < 0)
				return response;
				
			wl_resto_title = $scope.restotitle = response.data.title;
			$scope.rdata = response.data.bookings; 
			for(i = 0; i < $scope.rdata.length; i++) {
				obj = $scope.rdata[i];
				if(obj.test || obj.funkitchen)
					continue; 
								
				obj.index = i+1; 
				obj.cover = parseInt(obj.cover);
				obj.cweek = parseInt(obj.cdate.jsdate().format('W')) % 53;
				obj.rweek = parseInt(obj.rdate.jsdate().format('W')) % 53;
				if(obj.cweek === 9)
					console.log(obj, obj.cdate);
				$scope.prebooking.push(obj);
				}
			if($scope.prebooking.length < 1) {
				$scope.nodata = true;
				return;
				}
			
			$scope.prebooking.sort($scope.compare);
			$scope.chartinit($scope.cmode);
			});	
		};

	$scope.buidData = function(data) {
		var i, oo, week, obj = [], enable = {};
		var bkgtype = { 
				booking: { data: [], label:'bookings', color:'#FFE4C4', area:true, strokeWidth:3, classed:'' }, 
				weeloy: { data: [], label:'weeloy', color:'#0000FF', area:false, strokeWidth:'', classed:'' }, 
				website: {data: [], label:'website', color:'#000000', area:false, strokeWidth:'', classed:'' },
				cctms: { data: [], label:'callcenter/tms', color:'#2ca02c', area:false, strokeWidth:'', classed:'' },
				thirdparty: {data: [], label:'thirdparty', color:'#DC143C', area:false, strokeWidth:'', classed:'' },
				facebook: {data: [], label:'facebook', color:'#008B8B', area:false, strokeWidth:'', classed:'' },
				walkin: {data: [], label:'walkin', color:'#DC143C', area:false, strokeWidth:'', classed:'' },
				cancel: {data: [], label:'cancel', color:'#7FFF00', area:false, strokeWidth:'', classed:'' },
				noshow: {data: [], label:'noshow', color:'#FF7F50', area:false, strokeWidth:'', classed:'' },
				lunch: {data: [], label:'lunch', color:'#FFD700', area:false, strokeWidth:'', classed:'dashed' },
				dinner: {data: [], label:'dinner', color:'#BDB76B', area:false, strokeWidth:'', classed:'dashed' }
			  };
						
		//Data is represented as an array of {x,y} pairs.
		names = Object.keys(bkgtype);
		for (i = 0; i < data.length; i++) {
			oo = data[i];
			week = oo[$scope.field];
			for(j = 0; j < names.length; j++) {
				l = names[j];
				x = bkgtype[l];
				x.data.push({x: week, y: oo[l]});
				if(!(l in enable)) enable[l] = 0;
				enable[l] += oo[l];
				}
			}

		for(j = 0; j < names.length; j++) {
			l = names[j];
			x = bkgtype[l];
			if(enable[l] > 5)
				obj.push({ values: x.data, key: x.label, color: x.color, area: x.area, strokeWidth: x.strokeWidth, classed: x.classed });
			}
			
		return obj;
		};

       
 	$scope.chartinit = function(mode) { 
		$scope.options.title.html = $scope.Title + ' in <span style="color:navy">' + $scope.cmode + '</span>';
		if($scope.restotitle !== "")
			$scope.options.title.html += ' for <span style="color: darkred;">' + $scope.restotitle + '</span>';
		$scope.options.subtitle.html = $scope.subTitle;
		$scope.options.caption.html = $scope.caption;
		$scope.options.chart.yAxis.axisLabel = ($scope.cmode !== "pax") ? "Bookings" : "Pax";
		$scope.options.caption.enable = true;

		$scope.stats(mode);			
		$scope.data = $scope.buidData($scope.booking);
		$scope.api.update();
		//console.log('DATA ', $scope.booking);
		//$timeout(function(){ console.log('DONE INIT'); }, 1000);
		};
 
   $scope.readData(); 
	

});
