<?php
$tablename = 7;
$guestcount = 4;
$profitcenter = 10;
$employee = '104';
$checktype = "1";
$clientID = 50;
$sessionID = 0;
$authentification = "MSG";

// you will receive the '<order-number>500084</order-number>' to be used for further query on that table

$post_string = '
<SOAP-ENV:Envelope
        xmlns:xsi = "http://www.w3.org/1999/XMLSchema/instance"
        xmlns:SOAP-ENV= "http://schemas.xmlsoap.org/soap/envelope"
	xsi:schemaLocation= "http://www.infogenesis.com/schemas/ver1.4/POSTransGatewaySchema.xsd">
    <SOAP-ENV:Body xsi:type= "process-order-request-Body">
        <process-order-request-Body>
            <process-order-request>
                <trans-services-header>
                    <client-id>' . $clientID . '</client-id>
                    <session-id>' . $sessionID . '</session-id>
                    <authentication-code>' . $authentification . '</authentication-code>
                </trans-services-header>
                <order-type>open</order-type>
                <order-header>
                    <table-name>' . $tablename . '</table-name>
                    <employee-id>' . $employee . '</employee-id>
                    <guest-count>' . $guestcount . '</guest-count>
                    <profitcenter-id>' . $profitcenter . '</profitcenter-id>
                    <check-type-id>' . $checktype . '</check-type-id>
                    <receipt-required>no</receipt-required>
                </order-header>
            </process-order-request>
        </process-order-request-Body>
    </SOAP-ENV:Body>
</SOAP-ENV:Envelope>';


$address = "agysremote.agilysys.asia";
$url = parse_url($address);
$site = $url["host"];
$path = $url["path"];
$port = 7008;

$header = array();
$header[0] = "Content-Type: text/xml'";
$header[1] = 'Content-length: ' . strlen($post_string);
//$header[2] = 'Authorization : NLAuth nlauth_account="4150703", nlauth_email="alejandro@madisonrooms.asia", nlauth_signature="Madisonrooms023", nlauth_role="1000"'; 



$ch = curl_init();
curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
curl_setopt($ch, CURLOPT_URL, $address);
curl_setopt($ch, CURLOPT_PORT, $port);
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS, $post_string);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

$server_output = curl_exec($ch);
curl_close($ch);

if ($server_output != "") {
    $Envelope = simplexml_load_string($server_output);
// xml name space declaration
    $Envelope->registerXPathNamespace('soap', 'http://schemas.xmlsoap.org/soap/envelope/');
    $Envelope->registerXPathNamespace('auth', 'http://developer.intuit.com/');

// you can use the prefixes that are already defined in the document
    $nodes = $Envelope->xpath('/SOAP-ENV:Envelope/SOAP-ENV:Body/process-order-response-Body/process-order-response/service-completion-status');
    $service_completion = (string) $nodes[0];

    $nodes = $Envelope->xpath('/SOAP-ENV:Envelope/SOAP-ENV:Body/process-order-response-Body/process-order-response/order-number');
    $order_id = (string) $nodes[0];

    $nodes = $Envelope->xpath('/SOAP-ENV:Envelope/SOAP-ENV:Body/process-order-response-Body/process-order-response/order-guid');
    $order_guid = (string) $nodes[0];

    var_dump($service_completion, $order_id, $order_guid);

// further processing ....

    if ($service_completion == 'ok') {
        $sql = "UPDATE booking SET partner_booking_id = '$order_id' WHERE booking.confirmation = '$booking->confirmation';";
        pdo_exec($sql);
        return true;
    }
}





// REPONSE EXAMPLE
//<?xml version="1.0" encoding="utf-8"
?>
<!--<SOAP-ENV:Envelope xmlns:xsi="http://www.w3.org/1999/XMLSchema/instance" xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope" xsi:schemaLocation="http://www.infoGenesis.com/schemas/ver1.4/POSTransGatewaySchema.xsd">
    <SOAP-ENV:Body xsi:type="process-order-response-Body">
        <process-order-response-Body>
            <process-order-response>
                <trans-services-header>
                    <client-id>50</client-id>
                    <session-id>0</session-id>
                    <authentication-code>MSG</authentication-code>
                </trans-services-header>
                <service-completion-status>ok</service-completion-status>
                <order-number>500188</order-number>
                <order-guid>e9fa77fe-74f6-4fd3-8213-f105d609d53f</order-guid>
            </process-order-response>    
        </process-order-response-Body>
  
    </SOAP-ENV:Body>

</SOAP-ENV:Envelope>-->
?>