<?php 
require_once("conf/conf.init.inc.php");
require_once("lib/wpdo.inc.php");
require_once("conf/conf.session.inc.php");
require_once("lib/wglobals.inc.php");
//require_once("lib/class.restaurant.inc.php");
//require_once("lib/class.member.inc.php");
//require_once("printresto.inc.php");
//require_once("search_restaurant.inc.php");



$title_page = 'Weeloy';
$description_page = 'Weeloy';
$keywords_page = 'Weeloy';
$_SESSION['login_type'] = 'member';


            switch ($_REQUEST['page']){
                case 'about':
                        $title_page = 'About Weeloy - Book Best Restaurants in Singapore and Get Rewarded';
                        $description_page = 'Weeloy is Free, Easy, Fun. Book Best Restaurants in Singapore with Weeloy and discover exclusive promotions. Get rewarded with Weeloy at the best restaurants in Singapore.';
                        $keywords_page = '= restaurants in Singapore, where to eat in Singapore, fine dining Singapore, best food in Singapore, places to eat in Singapore';
                    break;
                case 'contact':
                        $title_page = 'Contact Weeloy - Book Best Restaurants in Singapore and Get Rewarded';
                        $description_page = 'Weeloy is Free, Easy, Fun. Book Best Restaurants in Singapore with Weeloy and discover exclusive promotions. Book your table and spin the wheel at your restaurant in Singapore.';
                        $keywords_page = 'restaurants in Singapore, where to eat in Singapore, fine dining Singapore, best food in Singapore, places to eat in Singapore';
                        $login_text = 'Login';
                    break;
                case 'partner':
                        $title_page = 'Weeloy Partner – Market Your Restaurant Online';
                        $description_page = 'Weeloy is Free, Easy, Fun. Let Weeloy be your preferred technology partner. Online restaurant booking system for customers to enjoy promotions.';
                        $keywords_page = 'restaurants in Singapore, where to eat in Singapore, fine dining Singapore, best food in Singapore, places to eat in Singapore';
                        $login_text = 'Login to Weeloy';
                    break;
                case 'faq':
                        $title_page = 'FAQ Weeloy - How to book and review restaurant online';
                        $description_page = 'Wonder where to eat? Discover new restaurants new places Get special promotions in your city Make reservation, win discount, eat best food, review your experience';
                        $keywords_page = 'restaurants in Singapore, where to eat in Singapore, fine dining Singapore, best food in Singapore, places to eat in Singapore';
                    break;
                case 'tnc_service':
                    
                        $title_page = 'Terms and conditions of services Weeloy.com | Dine with discount';
                        $description_page = 'General terms, Prohibited and acceptable uses, Service, Responsibility. Which discounts can you win with Weeloy? Try the best food in Asia with special offer';
                        $keywords_page = 'restaurants in Singapore, where to eat in Singapore, fine dining Singapore, best food in Singapore, places to eat in Singapore';
                    break;
                case 'tnc':
                        $title_page = 'Privacy Policy and terms | Singapore new place | Weeloy';
                        $description_page = 'Data policies, restaurant newsletter, personal account conditions Book a restaurant online and win special discounts, rewards Enjoy your diner and post best review';
                        $keywords_page = 'restaurants in Singapore, where to eat in Singapore, fine dining Singapore, best food in Singapore, places to eat in Singapore';
                    break;                
            }
            
                    //default value for search bar
               if (!empty($_SESSION['user']['forced_city'])){
                   $current_city = $_SESSION['user']['forced_city'];
               }else{
                   $current_city = $_SESSION['user']['search_city'];
               }
               $included_data['active_city'] = $current_city;       
            
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1"/>
        <title><?php echo $title_page;?></title>
        
        
        
        <meta property="og:title" content="{if $htmlTitle}{$htmlTitle}{else}<?php echo $title_page;?>{/if}" />
        <meta property="og:type" content="website" />
        <meta property="og:url" content="https://www.weeloy.com/" />
        <meta property="og:description" content="<?php echo $description_page;?>" />
        <meta property="og:keywords" content="<?php echo $keywords_page;?>" />


        <meta charset='utf-8'/>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
        <meta name="robots" content="index,follow">
        
        <meta name="owner" content="weeloy.com" />
        <meta name="description" content="<?php echo $description_page;?>" />
        <meta name="keywords" content="<?php echo $keywords_page;?>" />
        <!--[if gte IE 8]><meta http-equiv="X-UA-Compatible" content="IE=9" /><![endif]-->
        <!--[if IE 8]><meta http-equiv="X-UA-Compatible" content="IE=8" /><![endif]-->

        <meta name="msvalidate.01" content="90FBBB685EF1AC19990BADECF586BE25" />
        <script src="bower_components/jquery/dist/jquery.min.js"></script>
        <script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
        <script src="bower_components/angular/angular.min.js"></script>
        <script src="inc/weeloy.js"></script>
        <link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css"/>
        <link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap-theme.min.css"/>
        <link rel="stylesheet" href="css/font-awesome.min.css"/>
        <link rel="stylesheet" href="css/new_layout.css"/>
        
        <!--[if lt IE 9]>
        <script src="js/html5.js"></script>
        <script src="js/selectivizr-min.js"></script>
        <script src="js/modernizr.js"></script>
        <link rel="stylesheet" type="text/css" href="css/ie.css" media="screen" />
        <![endif]-->
        
    <style>
    .header_title { font-family:Unkempt;font-size:32px; }
    .modal-title { font-family:Unkempt;font-size:32px; }
    </style>
    </head>
    <body ng-app="weeloyApp">

        <?php include("templates/header_index.inc.php"); ?>

        <section class="content-sec">


            <?php
            if (isset($_REQUEST['page']) && $_REQUEST['page'] != '') {
                
                if($_REQUEST['page'] == 'tnc' || $_REQUEST['page'] == 'tnc_service'){
                    $page = "templates/tnc/" . $_REQUEST['page'] . ".php";
                }else{
                    $page = "templates/info/" . $_REQUEST['page'] . ".php";
                }
                include($page);
            }
            ?>
        </section>

<?php include("templates/footer.inc.php"); ?>


    </body>
</html>
