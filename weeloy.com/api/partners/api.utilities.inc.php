<?php

function api_error($e, $funcname) {

    $debug = new WY_debug;
    $trace = preg_replace("/\'|\"/", "`", $e->getTraceAsString());
    $traceAr = $e->getTrace();
    $file = preg_replace("/^.*\//", "", $traceAr[0]['file']);
    $msg = $e->getMessage() . " - " . $file . " - " . $traceAr[0]['line'];
    $debug->writeDebug("ERROR-API", "API-" . strtoupper($funcname), $msg . " \n " . $trace);
    echo format_api(0, 0, 0, $funcname . "->" . $msg);
}

function getErrorExample() {
    echo format_api(0, null, 0, array('type' => 'error_login', 'message' => 'Login failed please try again later'));
}

function testDecryptExample() {
    $query = $_SERVER['REDIRECT_QUERY_STRING'];

    $coding = new WY_Coding;
    var_dump($coding->mydecode($query));
    //echo format_api(0, null, 0, array('type'=>'error_login','message'=>$_RESQUEST));
}

function testEncryptExample() {
    $query = $_SERVER['REDIRECT_QUERY_STRING'];

    $coding = new WY_Coding;
    var_dump($coding->myencode($query));
    //echo format_api(0, null, 0, array('type'=>'error_login','message'=>$_RESQUEST));
}

function check_required_params($params, $required_parameters) {
    foreach ($params as $k => $v) {
        
        if (($v === NULL || $v === '' ) && in_array($k, $required_parameters)) {
            $errors['error'] = true;
            $errors['code'] = 'MISSING_PARAMETER';
            $errors['message'] = $k;
            return $errors;
        }
    }
    return true;
}

function check_params_type($params, $required_parameters, $required_parameters_type) {
    foreach ($params as $k => $v) {
        if (in_array($k, $required_parameters)) {
        switch ($required_parameters_type[$k]) {
            case 'integer':
                if ($v == (int) $v) {
                    $v = (int) $v;
                }
                break;

            default:
                break;
        }

        if (gettype($v) != $required_parameters_type[$k]) {
            $errors['error'] = true;
            $errors['code'] = 'WRONG_PARAMETER_TYPE';
            $errors['message'] = $required_parameters[$k] . ' should be a ' . $required_parameters_type[$k];
            return $errors;
        }
    }
    }
    return true;
}

function format_api($status, $data, $count, $errors, $header_status = 200, $app = NULL) {

    if($header_status != 200){
        set_header_status($header_status);
        $app->response()->status(422);
        
    }
    
    $resultat['status'] = $status;
    $resultat['data'] = $data;
    $resultat['count'] = $count;
    $resultat['errors'] = $errors;
    return json_encode($resultat);
}

function is_restaurant_valid($restaurant) {
    return !empty($restaurant);
}

function is_mobile_valid($mobile) {
    return preg_match('/^(NA|[0-9+-]+)$/', str_replace(' ', '', $mobile));
}

function is_email_valid($email) {
    return !(filter_var($email, FILTER_VALIDATE_EMAIL) === false);
}

function set_header_status($code) {
    $status = array(
        100 => 'Continue',
        101 => 'Switching Protocols',
        200 => 'OK',
        201 => 'Created',
        202 => 'Accepted',
        203 => 'Non-Authoritative Information',
        204 => 'No Content',
        205 => 'Reset Content',
        206 => 'Partial Content',
        300 => 'Multiple Choices',
        301 => 'Moved Permanently',
        302 => 'Found',
        303 => 'See Other',
        304 => 'Not Modified',
        305 => 'Use Proxy',
        306 => '(Unused)',
        307 => 'Temporary Redirect',
        400 => 'Bad Request',
        401 => 'Unauthorized',
        402 => 'Payment Required',
        403 => 'Forbidden',
        404 => 'Not Found',
        405 => 'Method Not Allowed',
        406 => 'Not Acceptable',
        407 => 'Proxy Authentication Required',
        408 => 'Request Timeout',
        409 => 'Conflict',
        410 => 'Gone',
        411 => 'Length Required',
        412 => 'Precondition Failed',
        413 => 'Request Entity Too Large',
        414 => 'Request-URI Too Long',
        415 => 'Unsupported Media Type',
        416 => 'Requested Range Not Satisfiable',
        417 => 'Expectation Failed',
        422 => 'Missing parameters',
        500 => 'Internal Server Error',
        501 => 'Not Implemented',
        502 => 'Bad Gateway',
        503 => 'Service Unavailable',
        504 => 'Gateway Timeout',
        505 => 'HTTP Version Not Supported');

    header("HTTP/1.1 $code " . $status[$code]);
    return true;
}
