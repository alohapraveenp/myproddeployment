<?php
include('conf/conf.mysql.inc.php');
require 'Slim/Slim.php';
 
\Slim\Slim::registerAutoloader();

$app = new \Slim\Slim();
 
$app->get('/restaurant', 'getAllRestaurants');
$app->post('/restaurant', 'getAllRestaurants');
$app->get('/restaurant/:query',  'getRestaurant');
$app->post('/restaurant/:query',  'getRestaurant');
$app->get('/restaurant/search/:query', 'findByCity');
$app->post('/restaurant/search/:query', 'findByCity');
$app->get('/restaurant/food/:query', 'findByCuisine');
$app->post('/restaurant/food/:query', 'findByCuisine');
$app->get('/logo', 'BuildLogo');

//$app->post('/restaurant', 'addRestaurant');
//$app->put('/restaurant/:id', 'updateRestaurant');
//$app->delete('/restaurant/:id', 'deleteRestaurant');
 
$app->run();
 
function BuildLogo() {

	$db = connect_db();
    $sql = "select restaurant, images, title FROM restaurant ORDER BY restaurant";
	$result = $db->query( $sql );
	while ( $data[] = $result->fetch_array(MYSQLI_ASSOC) );

	/* free result set */
	$result->close();
	
	foreach($data as $resto) {
		$img = preg_replace("/.*(Logo.[a-z]+)/", "$1", $resto['images']);
		$restaurant = $resto['restaurant'];
		$sql = "UPDATE restaurant SET logo='$img' WHERE restaurant='$restaurant' limit 1";
		$result = $db->query( $sql );
	}
	/* close connection */
	echo "Done"; 
	$db->close();
}
 
function getAllRestaurants() {
    $sql = "select restaurant, title, logo, city, cuisine FROM restaurant ORDER BY restaurant limit 30";
    try {
        $db = getConnection();
        $stmt = $db->query($sql);
        $restaurant = $stmt->fetchAll(PDO::FETCH_OBJ);
        $db = null;
        echo '{"restaurant": ' . json_encode($restaurant) . '}';
    } catch(PDOException $e) {
        echo '{"error":{"text":'. __FILE__." ".__FUNCTION__." ".__LINE__." ".$e->getMessage() .'}}';
    }
}
 
function getRestaurant($query) {
    $sql = "SELECT restaurant, title, city, cuisine FROM restaurant WHERE restaurant=:query limit 1";
    try {
        $db = getConnection();
        $stmt = $db->prepare($sql);
        $stmt->bindParam("query", $query);
        $stmt->execute();
        $restaurant = $stmt->fetchObject();
        $db = null;
        echo json_encode($restaurant);
    } catch(PDOException $e) {
        echo '{"error":{"text":'. __FILE__." ".__FUNCTION__." ".__LINE__." ".$e->getMessage() .'}}';
    }
}
 
function findByCity($query) {

	$query = preg_replace("/_/", " ", $query); 	// for Kuala Lumpur
   	$sql = "SELECT restaurant, title, city, cuisine FROM restaurant WHERE city=:query ORDER BY restaurant";
   	$binlog = true;
	if($query == "ALL")  {
		$sql = "SELECT restaurant, title, city, cuisine FROM restaurant ORDER BY city, restaurant";   
   		$binlog = false;
	}
    try {
        $db = getConnection();
        $stmt = $db->prepare($sql);
        if($binlog) $stmt->bindParam("query", $query);
        $stmt->execute();
        $restaurant = $stmt->fetchAll(PDO::FETCH_OBJ);
        $db = null;
        echo '{"restaurant": ' . json_encode($restaurant) . '}';
    } catch(PDOException $e) {
        echo '{"error":{"text":'. __FILE__." ".__FUNCTION__." ".__LINE__." ".$e->getMessage() .'}}';
    }
}

function findByCuisine($query) {

	$request = "";
	$queryAr = explode("|", $query);
	$limit = count($queryAr);
	if($limit < 1) {
	   echo '{"error":{"text":food is empty}}';
	   return;
	   }
	   
	for($i = 0, $sep = ""; $i < $limit; $i++, $sep = " or ")
		$request .= $sep . "cuisine like '%" . $queryAr[$i] . "%'";
		
    $sql = "SELECT restaurant, title, city, cuisine FROM restaurant WHERE $request ORDER BY restaurant";
    try {
        $db = getConnection();
        $stmt = $db->prepare($sql);
        $stmt->execute();
        $restaurant = $stmt->fetchAll(PDO::FETCH_OBJ);
        $db = null;
        echo '{"restaurant": ' . json_encode($restaurant) . '}';
    } catch(PDOException $e) {
        echo '{"error":{"text":'. __FILE__." ".__FUNCTION__." ".__LINE__." ".$e->getMessage() .'}}';
    }
}
 
function addRestaurant() {

	return;

    $request = Slim::getInstance()->request();
    $rq = json_decode($request->getBody());
    $sql = "INSERT INTO restaurant (restaurant, city, cuisine) VALUES (:restaurant, :city, :cuisine)";
    try {
        $db = getConnection();
        $stmt = $db->prepare($sql);
        $stmt->bindParam("restaurant", $rq->restaurant);
        $stmt->bindParam("city", $rq->City);
        $stmt->bindParam("cuisine", $rq->Cuisine);
        $stmt->execute();
        $rq->id = $db->lastInsertId();
        $db = null;
        echo json_encode($rq);
    } catch(PDOException $e) {
        echo '{"error":{"text":'. __FILE__." ".__FUNCTION__." ".__LINE__." ".$e->getMessage() .'}}';
    }
}
 
function updateRestaurant($id) {

	return;

    $request = Slim::getInstance()->request();
    $body = $request->getBody();
    $rq = json_decode($body);
    $sql = "UPDATE restaurant SET restaurant=:restaurant, City=:City, Cuisine=:Cuisine WHERE id=:id limit 1";
    try {
        $db = getConnection();
        $stmt = $db->prepare($sql);
        $stmt->bindParam("restaurant", $rq->restaurant);
        $stmt->bindParam("City", $rq->City);
        $stmt->bindParam("Cuisine", $rq->Cuisine);
        $stmt->bindParam("id", $id);
        $stmt->execute();
        $db = null;
        echo json_encode($rq);
    } catch(PDOException $e) {
        echo '{"error":{"text":'. __FILE__." ".__FUNCTION__." ".__LINE__." ".$e->getMessage() .'}}';
    }
}
 
function deleteRestaurant($id) {

	return;

    $sql = "DELETE FROM restaurant WHERE id=:id limit 1";
    try {
        $db = getConnection();
        $stmt = $db->prepare($sql);
        $stmt->bindParam("id", $id);
        $stmt->execute();
        $db = null;
    } catch(PDOException $e) {
        echo '{"error":{"text":'. __FILE__." ".__FUNCTION__." ".__LINE__." ".$e->getMessage() .'}}';
    }
}

function connect_db() {
        include('conf/conf.mysql.inc.php');
	$mysqli = new mysqli($dbhost, $dbuser, $dbpass, $dbname);

	if ($mysqli->connect_errno)
		die("Connect failed: " . $mysqli->connect_error . "\n");
	return $mysqli;
}

?>