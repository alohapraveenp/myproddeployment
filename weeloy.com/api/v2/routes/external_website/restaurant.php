<?php

require_once 'lib/class.restaurant.inc.php';

$app->get('/{restaurant}', function ($request, $response, $args) {
   return externalWebsiteInfo($request, $response, $args);
});

function externalWebsiteInfo($request, $response, $args) {
    $restaurant_id = $args['restaurant'];
    $restaurant = new WY_restaurant();
    
    $status_response = $restaurant->externalWebsiteInfo($restaurant_id);
    
    if ($status_response) {
        $status = 1;
        $data = array('info' => $status_response);
        $count = count(1);
        $error = NULL;
        $http_status = 200;
        return format_response($response, $status, $data, $count, $error, $http_status);
        } else {
        $status = 0;
        $data = NULL;
        $count = 0;
        $error = array('message'=>'We couldn’t get service categories');
        $http_status = 404;
        return format_response($response, $status, $data, $count, $error, $http_status);
    }
}