<?php

require_once("lib/class.sms.inc.php");
require_once("lib/class.login.inc.php");
require_once("conf/conf.init.inc.php");
require_once 'lib/class.marketing_campaign.inc.php';
require_once("lib/class.analytics.inc.php");
require_once("lib/class.notification.inc.php");
require_once("conf/conf.session.inc.php");


$app->post('/send', function($request, $response, $args) {

        return sendSms($request, $response, $request->getParsedBody());
}); 

$app->post('/template/save', function($request, $response, $args) {
        return savetemplate($request, $response, $request->getParsedBody());
}); 
$app->post('/template/list', function($request, $response, $args) {
    return gettemplate($request, $response, $request->getParsedBody());
});
$app->post('/template/delete', function($request, $response, $args) {
    return deletetemplate($request, $response, $request->getParsedBody());
});

//audit log

function sendSms($request, $response, $args){

    $token = $args->token;
    $sms = new JM_Sms();
    $smsid = '';

    $restaurant = $args->restaurant;
    $recipient_mobile = $args->mobile;
    $sms_message = $args->msg;
    $booking = $args->ref_id;
    $sender = $args->sender;
    $type = $args->type;
    $paid = (isset($args->isPaid)) ? $args->isPaid : '0';
    $provider = (isset($args->provider)) ? $args->provider : 'SMTP';
    
        if(empty($token) ){
            $status = 0;
            $data = false;
            $count = 0;
            $error = 'Empty token';
            $http_status = 200;
            return format_response($response, $status, $data, $count, $error, $http_status); 
        }
            $login = new WY_Login(set_valid_login_type('backoffice'));
        if ($login->checktoken($token) == false) {
            $status = 0;
            $data = false;
            $count = 0;
            $error = 'Wrong token';
            $http_status = 200;
            return format_response($response, $status, $data, $count, $error, $http_status); 

        }
        $bkg = new WY_Booking();
        $res = new WY_restaurant();
        $notification = new WY_Notification();
        

         
        if ($bkg->getBooking($booking) == false) {
               $http_status = 200;
               return format_response(-1, 0, 0, "Confirmation does not exist or wrong restaurant or email",$http_status);
        }
        
        //sms default configuration for restaurant
        
        $notification_type = $bkg->booking_type ; 
        $template = $notification_type . '_member';
        $white_label = $notification->checkisWhitelabel($bkg->tracking);
        $res->getRestaurant($bkg->restaurant);
        if (isset($white_label) && $white_label == true) {
            $template .= '_white_label';
        }
        
        $config = $notification->getConfiguration($bkg->restaurant, $template);

           
        $provider = (isset($notification->pname)) ? $notification->pname : 'SMTP';

        
        $sms_header = $res->getRestaurantSmsNumber($booking->restaurant);
        
        if(!empty($sms_header)){
            $sender = $sms_header;
        }
        

  
        if($type == 'payment_link'){

            $ret = $res->CheckAvailability($restaurant, $bkg->rdate, $bkg->rtime, $bkg->cover, $bkg->confirmation, $bkg->product);
            if($res->result > 0) {  
                $campaign = new WY_MarketingCampaign();
                $link =__BASE_URL__."/modules/booking/deposit/deposit_payment_method.php?action=bktrack_pendingpayment&bkemail=".$args->email."&refid=".$booking;
                $shortUrl = $campaign->bitly_shorten($link);

                //booking life cycle event log 

                $logger = new WY_log("backoffice");
                $loguserid = (isset($_SESSION['user_backoffice']['id'])) ? $_SESSION['user_backoffice']['id'] : "unknown";
                
                $logger->LogEvent($loguserid, 708, $booking, 'backoffice', '', date("Y-m-d H:i:s"));
                
                $expiredDate = $sms->getExpiredTime(date("Y-m-d H:i:s"),$restaurant,$bkg->rdate);
                $sms_message = "Thank you for choosing The Kitchen at Bacchanalia! To confirm your booking, click "."\n".$shortUrl."\n"."Table has been blocked until ".$expiredDate; 
                if($bkg->status == 'expired' || $bkg->status == ''){
                    $bkg->updateBookingStatus('', $bkg->confirmation, 'pending_payment');
                }

            }else{
                 $http_status = 200;
                return format_response(-1, 0, $ret, "No more availability. Restaurant =  " . $restaurant . ", date = " . $bkg->rdate . ", time = " . $bkg->rtime . ", covers = " . $bkg->cover,$http_status);

            }

    }

     
    if(empty($args->mobile) || empty($sms_message)){

        $status = 0;
        $data = false;
        $count = 0;
        $error = 'Empty recipient or message';
        $http_status = 200;
        return format_response($response, $status, $data, $count, $error, $http_status); 
    }
    $res = $sms->sendSmsMessage($recipient_mobile, $sms_message, $sender, $restaurant, $booking,$provider,$paid);
        $status = 1;
        $data = true;
        $count = 1;
        $error = NULL;
        $http_status = 200;
        return format_response($response, $status, $data, $count, $error, $http_status);

    
   
}

function savetemplate($request, $response, $args){
     $token = $args['token'];
     if(empty($token) ){
        $status = 0;
        $data = false;
        $count = 0;
        $error = 'Empty token';
        $http_status = 200;
        return format_response($response, $status, $data, $count, $error, $http_status); 
    }
    $login = new WY_Login(set_valid_login_type('backoffice'));
    if ($login->checktoken($token) == false) {
        $status = 0;
        $data = false;
        $count = 0;
        $error = 'Wrong token';
        $http_status = 200;
        return format_response($response, $status, $data, $count, $error, $http_status); 
    }
     $sms = new JM_Sms();
     $result = $sms->saveSmsTemplate($args);
     $status = 1;
     $data = $result;
     $count = 1;
     $error = NULL;
     $http_status = 200;
     return format_response($response, $status, $data, $count, $error, $http_status);
    
}

function gettemplate($request, $response, $args){
         
      $token = $args['token'];

     if(empty($token) ){
        $status = 0;
        $data = false;
        $count = 0;
        $error = 'Empty token';
        $http_status = 200;
        return format_response($response, $status, $data, $count, $error, $http_status); 
    }
    $login = new WY_Login(set_valid_login_type('backoffice'));
    if ($login->checktoken($token) == false) {
        $status = 0;
        $data = false;
        $count = 0;
        $error = 'Wrong token';
        $http_status = 200;
        return format_response($response, $status, $data, $count, $error, $http_status); 
    }
    
     $sms = new JM_Sms();
     $result = $sms->getSmsTemplate($args['restaurant']);
     $status = 1;
     $data = $result;
     $count = 1;
     $error = NULL;
     $http_status = 200;
     return format_response($response, $status, $data, $count, $error, $http_status);
    
}

function deletetemplate($request, $response, $args){
     $token = $args['token'];

     if(empty($token) ){
        $status = 0;
        $data = false;
        $count = 0;
        $error = 'Empty token';
        $http_status = 200;
        return format_response($response, $status, $data, $count, $error, $http_status); 
    }
    $login = new WY_Login(set_valid_login_type('backoffice'));
    if ($login->checktoken($token) == false) {
        $status = 0;
        $data = false;
        $count = 0;
        $error = 'Wrong token';
        $http_status = 200;
        return format_response($response, $status, $data, $count, $error, $http_status); 
    }
    
    $sms = new JM_Sms();
    $result = $sms->deleteSmsTemplate($args);
    error_log("result =".print_r($result,true));
    $status = 1;
    $data = $result;
    $count = 1;
    $error = NULL;
    $http_status = 200;
    return format_response($response, $status, $data, $count, $error, $http_status);
    
}

