<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

require_once 'lib/class.payment.inc.php';

$app->get('/list', function ($request, $response, $args) {
    return getPaymentMethodList($request, $response, $args);
});
$app->post('/save', function ($request, $response, $args) {

    return savePaymentMethod($request, $response, $args);
});
$app->post('/update', function ($request, $response, $args) {

    return updatePaymentMethod($request, $response, $args);
});
$app->get('/{restaurant}', function ($request, $response, $args) {
    return getResPaymentMethod($request, $response, $args);
});



function getPaymentMethodList($request, $response, $args){
   
    $payment = new WY_Payment();
    try {
        $obj = $payment->getPaymentMethodList();

        $status = 1;        
        $data = array("method" => $obj);
        $count = count($obj);
        $error = NULL;
        $http_status = 200;
        
        return format_response($response, $status, $data, $count, $error, $http_status);
        
    } catch (PDOException $e) {
        
        api_error($e, "getPaymentMethod");
        $status = 0;        
        $data = NULL;
        $count = 0;
        $error = array('message'=>'We couldn’t get Payment Method');
        $http_status = 500;
        
        return format_response($response, $status, $data, $count, $error, $http_status);
    }
}

function savePaymentMethod($request, $response, $args){
    $json = $request->getBody();
    $params = json_decode($json, true); 
    $payment = new WY_Payment();
     
     try {
         $status = 1;  
         $http_status = 200;
         if(empty($params['restaurant']) || empty($params['payment_mode']) ) {
             return format_response($response, 0, 0, 0, 'restuarant or payment mode empty', $http_status);
         }
        $obj = $payment->savePaymentServices($params);
        $data = array("method" => $obj);
        $count = count($obj);
        $error = NULL;
    
        return format_response($response, $status, $data, $count, $error, $http_status);
        
    } catch (PDOException $e) {
        
        api_error($e, "getPaymentMethod");
        $status = 0;        
        $data = NULL;
        $count = 0;
        $error = array('message'=>'We couldn’t get Payment Method');
        $http_status = 500;
        
        return format_response($response, $status, $data, $count, $error, $http_status);
    }
         
         
        
    
}
function updatePaymentMethod($request, $response, $args){
    $json = $request->getBody();
    $params = json_decode($json, true); 
    $payment = new WY_Payment();
     
     try {
         $status = 1;  
         $http_status = 200;
         if(empty($params['restaurant']) || empty($params['payment_mode']) ) {
             return format_response($response, 0, 0, 0, 'restuarant or payment mode empty', $http_status);
         }
        $obj = $payment->updatePaymentServices($params);
        $data = array("method" => $obj);
        $count = count($obj);
        $error = NULL;
    
        return format_response($response, $status, $data, $count, $error, $http_status);
        
    } catch (PDOException $e) {
        
        api_error($e, "getPaymentMethod");
        $status = 0;        
        $data = NULL;
        $count = 0;
        $error = array('message'=>'We couldn’t get Payment Method');
        $http_status = 500;
        
        return format_response($response, $status, $data, $count, $error, $http_status);
    }
         
         
        
    
}
function getResPaymentMethod($request, $response, $args){
   
    $restaurant =  $args['restaurant'];
    $payment = new WY_Payment();
    try {
        $obj = $payment->getPaymentServices($restaurant);

        $status = 1;        
        $data = array("method" => $obj);
        $count = count($obj);
        $error = NULL;
        $http_status = 200;
        
        return format_response($response, $status, $data, $count, $error, $http_status);
        
    } catch (PDOException $e) {
        
        api_error($e, "getPaymentMethod");
        $status = 0;        
        $data = NULL;
        $count = 0;
        $error = array('message'=>'We couldn’t get Payment Method');
        $http_status = 500;
        
        return format_response($response, $status, $data, $count, $error, $http_status);
    }
}



