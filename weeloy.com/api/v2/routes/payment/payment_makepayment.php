<?php


/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


require_once ('lib/class.pay_weeloy_invoice.inc.php');
require_once ('lib/class.payment_new.inc.php');


$app->post('/{restaurant}/', function ($request, $response, $args) {

    return wymakepayment($request, $response, $args);
});
$app->post('/deposit', function ($request, $response, $args) {
    return bkmakepayment($request, $response, $request->getParsedBody());
});



function wymakepayment($request, $response, $args ){
    $json = $request->getBody();
    $params = json_decode($json, true); 

    if(!empty($params['restaurant']) && !empty($params['amount']) ) {
        $payment = new  WY_Pay_invoice($params);
        $result = $payment->doPayment();
        
        if(count($result) > 0){
            $data = array('payment' => $result);
            $count = count(1);
            $error = NULL;
            $http_status = 200;
            $status = 1;
            return format_response($response, $status, $data, $count, $error, $http_status);
        } else {
            $status = 0;
            $data = NULL;
            $count = 0;
            $error = array('message'=>'We couldn’t make the payment');
            $http_status = 200;
            return format_response($response, $status, $data, $count, $error, $http_status);
        }
        
    }else{
        $status = 0;
        $data = NULL;
        $count = 0;
        $error = array('message'=>'Missing params => We couldn’t make the payment');
        $http_status = 200;
        return format_response($response, $status, $data, $count, $error, $http_status);
    }
 
}
function bkmakepayment($request, $response, $args ){
    $params = $args; 

    if(!empty($params['restaurant']) && !empty($params['amount']) ) {
        $payment = new WY_Payment_new($params);
        $result = $payment->doDeposit();
        
        if(count($result) > 0){
            $data = array('payment' => $result);
            $count = count(1);
            $error = NULL;
            $http_status = 200;
            $status = 1;
            return format_response($response, $status, $data, $count, $error, $http_status);
        } else {
            $status = 0;
            $data = NULL;
            $count = 0;
            $error = array('message'=>'We couldn’t make the payment');
            $http_status = 200;
            return format_response($response, $status, $data, $count, $error, $http_status);
        }
        
    }else{
        $status = 0;
        $data = NULL;
        $count = 0;
        $error = array('message'=>'Missing params => We couldn’t make the payment');
        $http_status = 200;
        return format_response($response, $status, $data, $count, $error, $http_status);
    }
 
}