<?php

require_once 'lib/class.restaurant.inc.php';
require_once 'lib/class.restaurant_section.inc.php';


$app->get('/product/{restaurant}/{product}', function ($request, $response, $args) {
   return getConfigByProduct($request, $response, $args );
});
$app->post('/save', function ($request, $response, $args) {

    return saveRestaurantSection($request, $response, $request->getParsedBody());
});








function getConfigByProduct($request, $response, $args) {
    $restaurant = $args['restaurant'];
    $product= $args['product'];
    $section = new WY_Restaurant_Section();
    $productDetails = $section->getSectionByProduct($product,$restaurant);
    
    if (count($productDetails)>0) {
        $status = 1;        
        $data = array('productdetails' => $productDetails);
        $count = count($productDetails);
        $error = NULL;
        $http_status = 200;
        return format_response($response, $status, $data, $count, $error, $http_status);
                
    } else {
        $status = 0;        
        $data = NULL;
        $count = 0;
        $error = array('message'=>'We couldn’t get sections');
        $http_status = 200;
        return format_response($response, $status, $data, $count, $error, $http_status);
    }
    
}
function saveRestaurantSection($request, $response, $args){
    $section = new WY_Restaurant_Section();
    $saveDetails = $section->saveSection($args);
    $status = 1;        
    $count = count($productDetails);
    $error = NULL;
    $http_status = 200;
    return format_response($response, $status, $saveDetails, $count, $error, $http_status);
    
}