<?php

require_once 'lib/class.cluster.inc.php';

//// RESTAURANT GROUPS
$app->get('/{group}/details', function ($request, $response, $args) {
   return getRestaurantGroupDetails($request, $response, $args);
});
   

function getRestaurantGroupDetails($request, $response, $args) {
    $group = clean_text($args['group']);
    
    try {
        $res = new WY_restaurant();
        $cluster = new WY_Cluster();
        $status = 1;
        
        $cluster_info = $cluster->getParentClusterAdditionalInformation($group,'RESTAURANTGROUP');
        $data['info'] = $cluster_info['info'];  
        
        $master_cluster = $cluster->getCluster('MASTER',$group,'RESTAURANTGROUP','');
        
        if(count($master_cluster)>0){
            if($master_cluster['clustcontent'] == 'restaurant-tag'){
                $data['restaurant'] = $res->getRestaurantByTag($group);
            }else{
                $data['restaurant'] = $res->getRestaurantByCluster($group,'RESTAURANTGROUP');      
            }
        }
        $count = count($data);
        $error = NULL;
        $http_status = 200;
        
        return format_response($response, $status, $data, $count, $error, $http_status);
        
    } catch (PDOException $e) {
        
        api_error($e, "getEventsRestaurant");
        $status = 0;        
        $data = NULL;
        $count = 0;
        $error = array('message'=>'We couldn’t get teh cluster details');
        $http_status = 500;
        
        return format_response($response, $status, $data, $count, $error, $http_status);
    }
}
