<?php

require_once("lib/class.analytics.inc.php");
require_once("lib/class.member.inc.php");
require_once("conf/conf.session.inc.php");



$app->post('/log', function($request, $response, $args) {
        return logEvent($request, $response, $request->getParsedBody());
}); 
$app->post('/log/getbooking', function($request, $response, $args) {
        return getBookinglogEvent($request, $response, $request->getParsedBody());
}); 

//audit log

function logEvent($request, $response, $args,$type='website'){

    $action = $args['action'];
    $event = $args['event'];
    $page = $args['page'];
    $others =$args['others'];
    
    $logger = new WY_log($type);
    //$userAgent = $app->request->getUserAgent();
    $member = new WY_Member();
 
    if($type==='backoffice'){
        $user_id = (isset($_SESSION['user_backoffice']['id'])) ? $_SESSION['user_backoffice']['id'] : "";  
     }else{
         $user_id = (isset($_SESSION['user']['user_id'])) ? $_SESSION['user']['user_id'] : "";
     }

   
    $logger->LogEvent($user_id, $action, $event, $page, $others, date("Y-m-d H:i:s"));
        $status = 1;
        $data = true;
        $count = 1;
        $error = NULL;
        $http_status = 200;
        return format_response($response, $status, $data, $count, $error, $http_status);
}


function getBookinglogEvent($request, $response, $args){
    
    
    $token = $args->token;
      
    if(empty($token) ){
        $status = 0;
        $data = false;
        $count = 0;
        $error = 'Empty token';
        $http_status = 200;
        return format_response($response, $status, $data, $count, $error, $http_status); 
    }
    $login = new WY_Login(set_valid_login_type('backoffice'));
    if ($login->checktoken($token) == false) {
        $status = 0;
        $data = false;
        $count = 0;
        $error = 'Wrong token';
        $http_status = 200;
        return format_response($response, $status, $data, $count, $error, $http_status); 

    }
     $logger = new WY_log('backoffice');
     $result = $logger->getBookingLog($args->bookid);
     $status = 1;
     $data = $result;
     $count = 1;
     $error = NULL;
     $http_status = 200;
     return format_response($response, $status, $data, $count, $error, $http_status);
    
}