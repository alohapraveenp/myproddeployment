<?php

$app->get('/check', function($request, $response, $args) {
    return checkVersion($request, $response, $args);
});

//audit log

function checkVersion($request, $response, $args) {

    // header information on app
    $app_platform = $request->getHeader('Platform')[0];
    $app_name = $request->getHeader('App')[0];
    $app_version = $request->getHeader('Version')[0];
    $app_device = $request->getHeader('Device')[0];
    $app_sdk = $request->getHeader('Sdk')[0];
    $app_user_agent = $request->getHeader('User_agent')[0];

    // dead version = last version accep
    $obselete_version_ios = '2.2.24';   
    $obselete_version_android = '2.2.8.0';
    $data = array('result' => 'failure');
    
    if (isset($app_name) && isset($app_version) && $app_name == 'Weeloy'){
       
        $obselete_version = ($app_platform == 'ios') ? $obselete_version_ios : $obselete_version_android;

        if(version_greater_than($app_version, $obselete_version))
            $data = array('result' => 'success_update');
        else $data =  array('result' => 'success');
    }
  
    $status = 1;
    $count = 1;
    $error = NULL;
    $http_status = 200;
    return format_response($response, $status, $data, $count, $error, $http_status);
}

function version_greater_than($v1, $v2) {

	$ret = true;	
    $v1_exp = explode('.', $v1);
    $v2_exp = explode('.', $v2);
    for($i = 0; $i < count($v1_exp); $i++){
        if(intval($v1_exp[$i]) > intval($v2_exp[$i])){
            $ret = false;
            break;
        }
    }
    
    if($ret)
		WY_debug::recordDebug("DEBUG", "AP UPDATE", $v1 . ' => ' . $v2);

    return $ret;

}