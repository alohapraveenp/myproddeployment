<?

$app->post('/read/one/booking/', function () use ($app) {
	$data = json_decode($app->request()->getBody(), true);
	getOneProfileBooking($data['restaurant'], $data['systemid'], $data['email'], $data['token']);
	});

$app->post('/read/one/', function () use ($app) {
	$data = json_decode($app->request()->getBody(), true);
	if(!isset($data['account'])) $data['account'] = "";
	getOneProfile($data['restaurant'], $data['email'], $data['phone'], $data['token'], $data['account']);
	});

$app->post('/read/shortone/', function () use ($app) {
	$data = json_decode($app->request()->getBody(), true);
	getShortOneProfile($data['restaurant'], $data['email'], $data['phone'], $data['token']);
	});


$app->post('/read/', function () use ($app) {
	$data = json_decode($app->request()->getBody(), true);
	getProfile($data['restaurant'], $data['email'], $data['token']);
	});

$app->post('/readcodeprof/', function () use ($app) {
	$data = json_decode($app->request()->getBody(), true);
	getCodeProfile($data['restaurant'], $data['email'], $data['token']);
	});

$app->post('/updatecodeprof/', function () use ($app) {
	$data = json_decode($app->request()->getBody(), true);
	updateCodeProfile($data['restaurant'], $data['content'], $data['email'], $data['token']);
	});

$app->post('/readcodebkg/', function () use ($app) {
	$data = json_decode($app->request()->getBody(), true);
	getCodeBooking($data['restaurant'], $data['email'], $data['token']);
	});

$app->post('/updatecodebkg/', function () use ($app) {
	$data = json_decode($app->request()->getBody(), true);
	updateCodeBooking($data['restaurant'], $data['content'], $data['email'], $data['token']);
	});

$app->post('/updateMasterprofile/', function () use ($app) {
	$data = json_decode($app->request()->getBody(), true);
	updateMasterprofile($data['restaurant'], $data['content'], $data['email'], $data['token']);
	});

$app->post('/getprofiles/', function () use ($app) {
	$data = json_decode($app->request()->getBody(), true);
	error_log(__FILE__." ".__FUNCTION__." ".__LINE__.": data: ".print_r($data, true));
	getProfiles($data['token'], $data['restaurant'], $data['start'], $data['end']);
});
function getProfiles($token, $restaurant, $start, $end) {
	try {
		$login_type = 'translation';
		$login = new WY_Login(set_valid_login_type($login_type));
		if ($token != "callcenter" && $login->checktoken($token) == false) {	// temporary hack as callcenter has no login !!
			echo format_api(-1, '', 0, 'bad token');
			return;
		}
		$prof = new WY_Profile;
		$data = $prof->getProfiles($restaurant, $start, $end);
		$msg = $prof->msg;
		$status = $prof->result;
		if ($prof->result > 0) 
			$zdata = count($data);
		echo format_api($status, $data, $zdata, $msg);
	} catch (PDOException $e) {
		error_log(__FILE__." ".__FUNCTION__." ".__LINE__." Exception: ".$e);
		api_error($e, __FUNCTION__); 
	}
}
function getOneProfileBooking($restaurant, $systemid, $email, $token) {    
	$data = null;
	$status = $zdata = 0;
	$msg = "";
	$login_type = 'translation';
	$mode = 'api-ajax';
	try {
		$login = new WY_Login(set_valid_login_type($login_type));
		if ($token != "callcenter" && $login->checktoken($token) == false) {	// temporary hack as callcenter has no login !!
			echo format_api(-1, '', 0, 'bad token');
			return;
			}
		$prof = new WY_Profile;
		$data = $prof->read1ProfBooking($restaurant, $systemid);
		$msg = $prof->msg;
		$status = $prof->result;
		if ($prof->result > 0) 
			$zdata = count($data);
		
		echo format_api($status, $data, $zdata, $msg);
	} catch (PDOException $e) { api_error($e, __FUNCTION__); }
}
    

function getOneProfile($restaurant, $email, $phone, $token, $account) {
        
	$data = null;
	$status = $zdata = 0;
	$msg = "";
	$login_type = 'translation';
	$mode = 'api-ajax';
	
	try {
		$login = new WY_Login(set_valid_login_type($login_type));
		if ($token != "callcenter" && $login->checktoken($token) == false) {	// temporary hack as callcenter has no login !!
			echo format_api(-1, '', 0, 'bad token');
			return;
			}
		$prof = new WY_Profile;
		$data = $prof->read1Prof($restaurant, $email, $phone, $account);
		$msg = $prof->msg;
		$status = $prof->result;
		if ($prof->result > 0) 
			$zdata = count($data);
		
		echo format_api($status, $data, $zdata, $msg);
	} catch (PDOException $e) { api_error($e, __FUNCTION__); }
}
    
function getShortOneProfile($restaurant, $email, $phone, $token) {
        
	$data = null;
	$status = $zdata = 0;
	$msg = "";
	$login_type = 'translation';
	$mode = 'api-ajax';
	
	try {
		$login = new WY_Login(set_valid_login_type($login_type));
		if ($token != "callcenter" && $login->checktoken($token) == false) {	// temporary hack as callcenter has no login !!
			echo format_api(-1, '', 0, 'bad token');
			return;
			}
		$prof = new WY_Profile;
		$data = $prof->readShortProf($restaurant, $email, $phone);
		$msg = $prof->msg;
		$status = $prof->result;
		if ($prof->result > 0) 
			$zdata = count($data);
		
		echo format_api($status, $data, $zdata, $msg);
	} catch (PDOException $e) { api_error($e, __FUNCTION__); }
}
    

function getCodeBooking($restaurant, $email, $token) {
        
	$data = null;
	$status = $zdata = 0;
	$msg = "";
	$login_type = 'translation';
	$mode = 'api-ajax';
	
	// can be called from the call center. no login yet. Should do a login soon.
	if($email != "call@center.com") {
		$log = new WY_Login(set_valid_login_type($login_type));
		$log->check_login_remote($email, $token, $mode);
		$msg = $log->msg;
		$result = $log->result;
		}
	else $result = 1;
	
	if ($result > 0) {
		$prof = new WY_Profile;
		$data = $prof->readCodeBooking($restaurant);
		$msg = $prof->msg;
		$status = $prof->result;
		if ($prof->result > 0) {
			$zdata = count($data);
		}
	}
	echo format_api($status, $data, $zdata, $msg);
}
    
function updateCodeBooking($restaurant, $content, $email, $token) {

	$status = 0;
	$msg = "";
	$login_type = 'translation';
	$mode = 'api-ajax';
	
	$log = new WY_Login(set_valid_login_type($login_type));
	$log->check_login_remote($email, $token, $mode);
	$msg = $log->msg;
	if ($log->result > 0) {
		$prof = new WY_Profile;
		$prof->updateCodeBooking($restaurant, $content);
		$msg = $prof->msg;
		$status = $prof->result;
		}
	echo format_api($status, "", 1, $msg);
}

function updateMasterprofile($restaurant, $content, $email, $token) {

	$status = 0;
	$msg = "";
	$login_type = 'translation';
	$mode = 'api-ajax';
	
	$log = new WY_Login(set_valid_login_type($login_type));
	$log->check_login_remote($email, $token, $mode);
	$msg = $log->msg;
	if ($log->result > 0) {
		$prof = new WY_Profile;
		$prof->updateMasterprofile($restaurant, $content);
		$msg = $prof->msg;
		$status = $prof->result;
		}
	echo format_api($status, "", 1, $msg);
}

function getCodeProfile($restaurant, $email, $token) {
        
	$data = null;
	$status = $zdata = 0;
	$msg = "";
	$login_type = 'translation';
	$mode = 'api-ajax';
	
	$log = new WY_Login(set_valid_login_type($login_type));
	$log->check_login_remote($email, $token, $mode);
	$msg = $log->msg;
	if ($log->result > 0) {
		$prof = new WY_Profile;
		$data = $prof->readCodeProf($restaurant);
		$msg = $prof->msg;
		$status = $prof->result;
		if ($prof->result > 0) {
			$zdata = count($data);
		}
	}
	echo format_api($status, $data, $zdata, $msg);
}
    
function updateCodeProfile($restaurant, $content, $email, $token) {

	$status = 0;
	$msg = "";
	$login_type = 'translation';
	$mode = 'api-ajax';
	
	$log = new WY_Login(set_valid_login_type($login_type));
	$log->check_login_remote($email, $token, $mode);
	$msg = $log->msg;
	if ($log->result > 0) {
		$prof = new WY_Profile;
		$prof->updateCodeProf($restaurant, $content);
		$msg = $prof->msg;
		$status = $prof->result;
		}
	echo format_api($status, "", 1, $msg);
}

function getProfile($restaurant, $email, $token) {
        
	$data = null;
	$status = $zdata = 0;
	$msg = "";
	$login_type = 'translation';
	$mode = 'api-ajax';
	
	$log = new WY_Login(set_valid_login_type($login_type));
	$log->check_login_remote($email, $token, $mode);
	$msg = $log->msg;
	if ($log->result > 0) {
		$prof = new WY_Profile;
		$data = $prof->readProf($restaurant);
		$msg = $prof->msg;
		$status = $prof->result;
		if ($prof->result > 0) {
			$zdata = count($data);
		}
	}
	echo format_api($status, $data, $zdata, $msg);
}
?>