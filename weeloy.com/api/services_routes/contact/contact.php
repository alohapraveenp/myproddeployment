<?

$app->post('/list/', function () use ($app) {
  $data = json_decode($app->request()->getBody(),true);
  getRestaurantContactList($data['restaurant'], $data['email'], $data['token']);
});

$app->post('/save/', function () use ($app) {
  $data = json_decode($app->request()->getBody(),true);
  saveRestaurantContact($data['restaurant'], $data['email'], $data['token'], $data['contact']);
});


$app->post('/delete/', function () use ($app) {
  $data = json_decode($app->request()->getBody(),true);
  deleteRestaurantContact($data['restaurant'], $data['email'], $data['token'], $data['contact']);
});


function getRestaurantContactList($restaurant, $email, $token) {
	$data = null;
	$status = $zdata = 0;
	$login_type = 'backoffice';
	$mode = $msg = "";
		
    try {
	    $log = new WY_Login(set_valid_login_type($login_type));
		$log->check_login_remote($email, $token, $mode);
		$msg = $log->msg;
        if ($log->result > 0) {
			$res = new WY_restaurant();
			$data = $res->getContactList($restaurant);
			$status = 1;
			$msg = '';
			}
	} catch (PDOException $e) { api_error($e, "getRestaurantContact"); }
    echo format_api($status, $data, $zdata = 1, $msg);
}

function saveRestaurantContact($restaurant, $email, $token, $contact) {
	$data = null;
	$status = $zdata = 0;
	$login_type = 'backoffice';
	$mode = $msg = "";
		
    try {
	    $log = new WY_Login(set_valid_login_type($login_type));
		$log->check_login_remote($email, $token, $mode);
		$msg = $log->msg;
        if ($log->result > 0) {
			$res = new WY_restaurant();
			$data = $res->saveRestaurantContact($restaurant, $contact);
			$status = 1;
			$msg = '';
			}
	} catch (PDOException $e) { api_error($e, "saveRestaurantContact"); }
    echo format_api($status, $data, $zdata = 1, $msg);
}


function deleteRestaurantContact($restaurant, $email, $token, $contact) {
	$data = null;
	$status = $zdata = 0;
	$login_type = 'backoffice';
	$mode = $msg = "";

    try {
	    $log = new WY_Login(set_valid_login_type($login_type));
		$log->check_login_remote($email, $token, $mode);
		$msg = $log->msg;
        if ($log->result > 0) {
			$res = new WY_restaurant();
			$data = $res->deleteRestaurantContact($restaurant, $contact);
			$status = 1;
			$msg = '';
			}
	} catch (PDOException $e) { api_error($e, "deleteRestaurantContact"); }
    echo format_api($status, $data, $zdata = 1, $msg);
}



?>