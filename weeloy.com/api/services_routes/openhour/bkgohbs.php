<?

$app->post('/read/', function () use ($app) {
  $data = json_decode($app->request()->getBody(),true);
  readBkgohbs($data['restaurant'], $data['email'], $data['token']);
});

$app->post('/update/', function () use ($app) {
  $data = json_decode($app->request()->getBody(),true);
  updateBkgohbs($data['restaurant'], $data['ohbs'], $data['email'], $data['token']);
});


function readBkgohbs($restaurant, $email, $token) {
	$data = null;
	$status = $zdata = 0;
	$login_type = 'backoffice';
	$mode = $msg = "";
		
    try {
	    $log = new WY_Login(set_valid_login_type($login_type));
		$log->check_login_remote($email, $token, $mode);
		$msg = $log->msg;
		if ($log->result > 0) {
			$resto = new WY_Restaurant();
			$data = $resto->getBkgohbs($restaurant);
			$msg = $resto->msg;
			$status = $resto->result;
			if ($resto->result > 0) {
				$data = array('restaurant' => $restaurant, 'data' => $data);
				$zdata = count($data);
				}
			}
	} catch (PDOException $e) { api_error($e, "readBkgohbs"); }
	echo format_api($status, $data, $zdata, $msg);
}

function updateBkgohbs($restaurant, $ohbs, $email, $token) {
	$data = null;
	$status = $zdata = 0;
	$login_type = 'backoffice';
	$mode = $msg = "";
		
    try {
	    $log = new WY_Login(set_valid_login_type($login_type));
		$log->check_login_remote($email, $token, $mode);
		$msg = $log->msg;
		if ($log->result > 0) {
			$resto = new WY_Restaurant();
			$resto->updateBkgohbs($restaurant, $ohbs);
			$status = $resto->result;
			$msg = $resto->msg;
			}
	} catch (PDOException $e) { api_error($e, "updateBkgohbs"); }
    echo format_api($status, $data = "ok", $zdata = 1, $msg);
}




?>