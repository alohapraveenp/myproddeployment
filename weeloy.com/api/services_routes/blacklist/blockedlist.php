<?

$app->post('/get/', function () use ($app) {
    
  $data = json_decode($app->request()->getBody(),true);
  getBlockedlist($data['restaurant'], $data['email'], $data['token']);
});

$app->post('/save/', function () use ($app) {
    
  $data = json_decode($app->request()->getBody(),true);
  saveblocklist($data);
});


function getBlockedlist($restaurant, $email, $token) {
	$data = null;
	$status = $zdata = 0;
	$login_type = 'backoffice';
	$mode = $msg = "";
		
    try {
            $log = new WY_Login(set_valid_login_type($login_type));
            $log->check_login_remote($email, $token, $mode);
            $msg = $log->msg;
            if ($log->result > 0) {
                    $eobj = new WY_Blokedlist($restaurant);
                    $obj = $eobj->getBlockedUser();
                    $status = $eobj->result;
                    
                    //$msg = $eobj->msg;
                    if ($eobj->result > 0) {
                        $data = $obj;
                    }
            }
	} catch (PDOException $e) { api_error($e, "readEvent"); }
	echo format_api($status, $data, $zdata, $msg);
}

function saveblocklist($params) {
	$data = null;
	$status = $zdata = 0;
	$login_type = 'backoffice';
	$mode = $msg = "";
        $token =$params['token'];
        $email =$params['backofficeemail'];
	
    try {
            $log = new WY_Login(set_valid_login_type($login_type));
            $log->check_login_remote($email, $token, $mode);
            $msg = $log->msg;
            if ($log->result > 0) {
                    $eobj = new WY_Blokedlist($params['restaurant']); 
                    $obj = $eobj->saveBlockedUser($params['email'],$params['mobile'],$params['status'],$params['firstname'],$params['lastname'],$params['reason']);
                    $status = $eobj->result;
                    //$msg = $eobj->msg;
                    if ($eobj->result > 0) {
                            $data = $obj;
                    }
            }
	} catch (PDOException $e) { api_error($e, "readEvent"); }
	echo format_api($status, $data, $zdata, $msg);
}


?>