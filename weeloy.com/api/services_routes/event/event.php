<?php

$app->post('/read/', function () use ($app) {
  $data = json_decode($app->request()->getBody(),true);
  readEvent($data['restaurant'], $data['email'], $data['token']);
});

$app->post('/create/', function () use ($app) {
  $data = json_decode($app->request()->getBody(),true);
  createEvent($data['restaurant'], $data['email'], $data['token'], $data['events']);
});
$app->post('/create/private/', function () use ($app) {
  $data = json_decode($app->request()->getBody(),true);
  createPrivateEvent($data);
});

$app->post('/update/', function () use ($app) {
  $data = json_decode($app->request()->getBody(),true);
  updateEvent($data['restaurant'], $data['email'], $data['token'], $data['events']);
});

$app->post('/delete/', function () use ($app) {
  $data = json_decode($app->request()->getBody(),true);
  deleteEvent($data['restaurant'], $data['email'], $data['token'], $data['eventID']);
});

$app->get('/getDetailedActiveEvents', function() use ($app) {
	GetDetailedActiveEvents();
});

$app->get('/bookingDetails/:confirmation', function ($confirmation) use ($app) {

    getEvBookingDetails($confirmation);
});
function GetDetailedActiveEvents() {
	$data = null;
	$status = $zdata = 0;
	$login_type = 'backoffice';
	$mode = $msg = "";
    try {
    	$event = new WY_Event();
    	$data = $event->getDetailedActiveEvents();
		$status = $event->result;
		$msg = $event->msg;
    	error_log(__FILE__." ".__FUNCTION__." ".__LINE__." ".count($data)." active events status: ".$status);
    	echo format_api($status, $data, count($data), $msg);
    	//return $result;
    } catch (Exception $e) {
    	error_log(__FILE__." ".__FUNCTION__." ".__LINE__." Exception: ".$e);
    	api_error($e, "GetDetailedActiveEvents");
    }
}
function readEvent($restaurant, $email, $token) {
	$data = null;
	$status = $zdata = 0;
	$login_type = 'backoffice';
	$mode = $msg = "";
    try {
	    $log = new WY_Login(set_valid_login_type($login_type));
		$log->check_login_remote($email, $token, $mode);
		$msg = $log->msg;
		if ($log->result > 0) {
			$eobj = new WY_Event($restaurant);
			$obj = $eobj->getEvents();
			$status = $eobj->result;
			$msg = $eobj->msg;
			if ($eobj->result > 0) {
				$data = array('restaurant' => $restaurant, 'currency' => $obj['currency'], 'event' => $obj['event'], 'evbooking' => $obj['evbooking'], 'evbookable' => $obj['evbookable']);
				$zdata = count($obj);
				}
			}
	} catch (PDOException $e) { api_error($e, "readEvent"); }
	echo format_api($status, $data, $zdata, $msg);
}
function getEvBookingDetails($confirmation) {
	$data = null;
	$status = $zdata = 0;
	$login_type = 'backoffice';
	$mode = $msg = "";
    try {
	    //$log = new WY_Login(set_valid_login_type($login_type));
		//$log->check_login_remote($email, $token, $mode);
		//$msg = $log->msg;
		//if ($log->result > 0) {
			$eobj = new WY_Event();
			$obj = $eobj->getEvBookingDetails($confirmation);
			$status = $eobj->result;
			$msg = $eobj->msg;
			if ($eobj->result > 0) {
				$data = $obj;
				$zdata = count($obj);
				}
			//}
	} catch (PDOException $e) { api_error($e, "readEvent"); }
	echo format_api($status, $data, $zdata, $msg);
}

function createEvent($restaurant, $email, $token,  $obj) {
	$data = null;
	$status = $zdata = 0;
	$login_type = 'backoffice';
	$mode = $msg = "";
	$obj["eventname"] = $obj["name"];
    try {
	    $log = new WY_Login(set_valid_login_type($login_type));
		$log->check_login_remote($email, $token, $mode);
		$msg = $log->msg;
		if ($log->result > 0) {
			$event = new WY_Event($restaurant);
			$event->insertEvent($obj);
			$status = $event->result;
			$msg = $event->msg;
		}
	} catch (PDOException $e) { api_error($e, "createEvent"); }
    echo format_api($status, $data = "ok", $zdata = 1, $msg);
}
function createPrivateEvent($obj) {
	$data = null;
	$status = $zdata = 0;
	$login_type = 'backoffice';
	$mode = $msg = "";
        $restaurant = $obj["restaurant"] ;
        $obj["eventname"] = $obj["name"];
		
    try {
        $event = new WY_Event($restaurant);
        $event->insertEvent($obj);
        $eventID = $event->result;
        $msg = $event->msg;
	} catch (PDOException $e) { api_error($e, "createPrivateEvent"); }
    echo format_api(1, $eventID, $zdata = 1, $msg);
}

function updateEvent($restaurant, $email, $token,  $obj) {
	$data = null;
	$status = $zdata = 0;
	$login_type = 'backoffice';
	$mode = $msg = "";

	$obj["eventname"] = $obj["name"];
		
    try {
	    $log = new WY_Login(set_valid_login_type($login_type));
		$log->check_login_remote($email, $token, $mode);
		$msg = $log->msg;
		if ($log->result > 0) {
			$event = new WY_Event($restaurant);
			$event->updateEvent($obj);
			$status = $event->result;
			$msg = $event->msg;
			}
	} catch (PDOException $e) { api_error($e, "updateEvent"); }
    echo format_api($status, $data = "ok", $zdata = 1, $msg);
}

function deleteEvent($restaurant, $email, $token,  $eventID) {
	$data = null;
	$status = $zdata = 0;
	$login_type = 'backoffice';
	$mode = $msg = "";
		
    try {
	    $log = new WY_Login(set_valid_login_type($login_type));
		$log->check_login_remote($email, $token, $mode);
		$msg = $log->msg;
		if ($log->result > 0) {
			$event = new WY_Event($restaurant);
			$event->delete($eventID);
			$status = $event->result;
			$msg = $event->msg;
			}
	} catch (PDOException $e) { api_error($e, "deleteEvent"); }
    echo format_api($status, $data = "ok", $zdata = 1, $msg);
}
?>