<?

$app->post('/read/', function () use ($app) {
	$data = json_decode($app->request()->getBody(), true);
	readcrawler($data['email'], $data['token']);
	});

$app->post('/create/', function () use ($app) {
	$data = json_decode($app->request()->getBody(), true);
	createcrawler($data['content'], $data['email'], $data['token']);
	});

$app->post('/update/', function () use ($app) {
	$data = json_decode($app->request()->getBody(), true);
	updatecrawler($data['content'], $data['email'], $data['token']);
	});

$app->post('/toggleactivation/', function () use ($app) {
	$data = json_decode($app->request()->getBody(), true);
	toggleactivatecrawler($data['content'], $data['email'], $data['token']);
	});

$app->post('/delete/', function () use ($app) {
	$data = json_decode($app->request()->getBody(), true);
	deletecrawler($data['content'], $data['email'], $data['token']);
	});


function readcrawler($email, $token) {
        
	$data = null;
	$status = $sizedata = 0;
	$mode = 'api';
	$msg = "";
	$login_type = 'translation';
	$mode = 'api-ajax';
	
	$log = new WY_Login(set_valid_login_type($login_type));
	$log->check_login_remote($email, $token, $mode);
	$msg = $log->msg;
	if ($log->result > 0) {
		$crawler = new WY_Crawler;
		$data = $crawler->crwlr_read();
		$status = $crawler->result;
		$msg = $crawler->msg;
		if ($crawler->result > 0) {
			$sizedata = count($data);
		}
	}
	echo format_api($status, $data, $sizedata, $msg);
}
    
function createcrawler($content, $email, $token) {
        
	$data = null;
	$status = $sizedata = 0;
	$mode = 'api';
	$msg = "";
	$login_type = 'translation';
	$mode = 'api-ajax';
	
	$log = new WY_Login(set_valid_login_type($login_type));
	$log->check_login_remote($email, $token, $mode);
	$msg = $log->msg;
	if ($log->result > 0) {
		$crawler = new WY_Crawler;
		$crawler->crwlr_create($content);
		$status = $crawler->result;
		$msg = $crawler->msg;
		}
	echo format_api($status, $data, $sizedata, $msg);
}
    
function updatecrawler($content, $email, $token) {
        
	$data = null;
	$status = $sizedata = 0;
	$mode = 'api';
	$msg = "";
	$login_type = 'translation';
	$mode = 'api-ajax';
	
	$log = new WY_Login(set_valid_login_type($login_type));
	$log->check_login_remote($email, $token, $mode);
	$msg = $log->msg;
	if ($log->result > 0) {
		$crawler = new WY_Crawler;
		$crawler->crwlr_update($content);
		$status = $crawler->result;
		$msg = $crawler->msg;
		}
	echo format_api($status, $data, $sizedata, $msg);
}
    
function toggleactivatecrawler($content, $email, $token) {
        
	$data = null;
	$status = $sizedata = 0;
	$mode = 'api';
	$msg = "";
	$login_type = 'translation';
	$mode = 'api-ajax';
	
	$log = new WY_Login(set_valid_login_type($login_type));
	$log->check_login_remote($email, $token, $mode);
	$msg = $log->msg;
	if ($log->result > 0) {
		$crawler = new WY_Crawler;
		$crawler->crwlr_toggleactivate($content);
		$status = $crawler->result;
		$msg = $crawler->msg;
		}
	echo format_api($status, $data, $sizedata, $msg);
}
    
function deletecrawler($content, $email, $token) {
        
	$data = null;
	$status = $sizedata = 0;
	$mode = 'api';
	$msg = "";
	$login_type = 'translation';
	$mode = 'api-ajax';
	
	$log = new WY_Login(set_valid_login_type($login_type));
	$log->check_login_remote($email, $token, $mode);
	$msg = $log->msg;
	if ($log->result > 0) {
		$crawler = new WY_Crawler;
		$crawler->crwlr_delete($content);
		$status = $crawler->result;
		$msg = $crawler->msg;
		}
	echo format_api($status, $data, $sizedata, $msg);
}

?>