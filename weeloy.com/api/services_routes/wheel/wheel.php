<?

$app->post('/read/', function () use ($app) {
  $data = json_decode($app->request()->getBody(),true);
  readWheel($data['restaurant'], $data['type'], $data['email'], $data['token']);
});

$app->post('/readoffers/', function () use ($app) {
  $data = json_decode($app->request()->getBody(),true);
  readofferWheel($data['restaurant'], $data['email'], $data['token']);
});

$app->post('/create/', function () use ($app) {
  $data = json_decode($app->request()->getBody(),true);
  createWheel($data['restaurant'], $data['wheels'], $data['type'], $data['email'], $data['token']);
});

$app->post('/update/', function () use ($app) {
  $data = json_decode($app->request()->getBody(),true);
  updateWheel($data['restaurant'], $data['wheels'], $data['type'], $data['email'], $data['token']);
});

$app->post('/delete/', function () use ($app) {
  $data = json_decode($app->request()->getBody(),true);
  deleteWheel($data['restaurant'], $data['type'], $data['email'], $data['token']);
});


function readWheel($restaurant, $type, $email, $token) {
	$data = null;
	$status = $zdata = 0;
	$login_type = 'backoffice';
	$mode = $msg = "";
		
    try {
	    $log = new WY_Login(set_valid_login_type($login_type));
		$log->check_login_remote($email, $token, $mode);
		$msg = $log->msg;
		if ($log->result > 0) {
			$wheel = new WY_wheel($restaurant, 'api', $type);
			$data = $wheel->readtypeWheel();
			$status = $wheel->result;
			$msg = $wheel->msg;
			$zdata = count($data);
			}
	} catch (PDOException $e) { api_error($e, "readWheel"); }
	echo format_api($status, $data, $zdata, $msg);
}

function readofferWheel($restaurant, $email, $token) {
	$data = null;
	$status = $zdata = 0;
	$login_type = 'backoffice';
	$mode = $msg = "";
		
    try {
	    $log = new WY_Login(set_valid_login_type($login_type));
		$log->check_login_remote($email, $token, $mode);
		$msg = $log->msg;
		if ($log->result > 0) {
			$wheel = new WY_wheel($restaurant, 'api', '');
			$data = $wheel->readofferWheel();
			$status = $wheel->result;
			$msg = $wheel->msg;
			$zdata = count($data);
			}
	} catch (PDOException $e) { api_error($e, "readWheel"); }
	echo format_api($status, $data, $zdata, $msg);
}

function createWheel($restaurant, $obj, $type, $email, $token) {
	$data = null;
	$status = $zdata = 0;
	$login_type = 'backoffice';
	$mode = $msg = "";
		
    try {
	    $log = new WY_Login(set_valid_login_type($login_type));
		$log->check_login_remote($email, $token, $mode);
		$msg = $log->msg;
		if ($log->result > 0) {
			$wheel = new WY_wheel($restaurant, 'api', $type);
			$wheel->createtypeWheel($obj);
			$status = $wheel->result;
			$msg = $wheel->msg;
			}
	} catch (PDOException $e) { api_error($e, "createWheel"); }
    echo format_api($status, $data = "ok", $zdata = 1, $msg);
}

function updateWheel($restaurant, $obj, $type, $email, $token) {
	$data = null;
	$status = $zdata = 0;
	$login_type = 'backoffice';
	$mode = $msg = "";
		
    try {
	    $log = new WY_Login(set_valid_login_type($login_type));
		$log->check_login_remote($email, $token, $mode);
		$msg = $log->msg;
		if ($log->result > 0) {
			$wheel = new WY_wheel($restaurant, 'api', $type);
			$wheel->updatetypeWheel($obj);
			$status = $wheel->result;
			$msg = $wheel->msg;
			}
	} catch (PDOException $e) { api_error($e, "updateWheel"); }
    echo format_api($status, $data = "ok", $zdata = 1, $msg);
}

function deleteWheel($restaurant, $type, $email, $token) {
	$data = null;
	$status = $zdata = 0;
	$login_type = 'backoffice';
	$mode = $msg = "";
		
    try {
	    $log = new WY_Login(set_valid_login_type($login_type));
		$log->check_login_remote($email, $token, $mode);
		$msg = $log->msg;
		if ($log->result > 0) {
			$wheel = new WY_wheel($restaurant, 'api', $type);
			$wheel->deletetypeWheel();
			$status = $wheel->result;
			$msg = $wheel->msg;
			$data = ($wheel->result > 0) ? "ok" : "not ok";
			}
	} catch (PDOException $e) { api_error($e, "deleteWheel"); }
    echo format_api($status, $data = "ok", $zdata = 1, $msg);
}





?>