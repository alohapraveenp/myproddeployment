<?

$app->post('/module/login/', function () use ($app) {
	$data = json_decode($app->request()->getBody(), true);
	if(!isset($data['platform'])) $data['platform'] = 'translation';
	if(!isset($data['application'])) $data['application'] = '';
	rmloginc($data['email'], $data['passw'], $data['platform'], $data['application']);
	});

$app->post('/module/loginsetapp/', function () use ($app) {
	$data = json_decode($app->request()->getBody(), true);
	rmloginsetapp($data['application'], $data['email'], $data['token']);
	});

$app->post('/module/loginfacebook/', function () use ($app) {
	$data = json_decode($app->request()->getBody(), true);
	if(!isset($data['platform'])) $data['platform'] = 'translation';
	rmloginfacebookc($data['email'], $data['facebookid'], $data['facebooktoken'], $data['platform']);
	});

$app->post('/module/logout/', function () use ($app) {
	$data = json_decode($app->request()->getBody(), true);
	rmlogoutc($data['email'], $data['token']);
	});

$app->post('/module/checkstatus/', function () use ($app) {
	$data = json_decode($app->request()->getBody(), true);
	rmcheckstatusc($data['email'], $data['token']);
	});

$app->post('/module/change/', function () use ($app) {
	$data = json_decode($app->request()->getBody(), true);
	rmchangepassc($data['email'], $data['password'], $data['npassword'], $data['token']);
	});

$app->post('/module/forgot/', function () use ($app) {
	$data = json_decode($app->request()->getBody(), true);
	if(!isset($data['redirect_to'])) $data['redirect_to'] = "backoffice";
    rmforgotc($data['email'], $data['redirect_to']);
	});

$app->post('/module/register/', function () use ($app) {
	$data = json_decode($app->request()->getBody(), true);
	rmregisterc($data['first'], $data['last'], $data['mobile'], $data['email'], $data['password']);
	});


function rmloginsetapp($app, $email, $token) {
    $login = new WY_Login(set_valid_login_type('backoffice'));
	$login->process_login_remote_setapp($app, $email, $token);
	echo ($login->result > 0) ? format_api(1, "ok", 1, $login->msg) : format_api(0, null, 0, $login->msg);
}

function rmloginc($email, $pass, $platform, $extrafield="") {
        //return state
        // 0  not login
        // 1 login
        // 2 login but need to update api
	$mode = 'api';
	$logintype = 'backoffice';
	if ($platform == 'translation') {
		$logintype = 'translation';
		$mode = 'api-ajax';
	}
	$log = new WY_Login(set_valid_login_type($logintype));
    try {
        $log->process_login_remote($email, $pass, $mode, $extrafield);
        $user_info = $log->signIn;
		$mobile_array['prefix'] = '';
		$mobile_array['mobile'] = $user_info['mobile'];
		$tmp = explode(' ', $user_info['mobile']);
		if (count($tmp) == 2 && substr($tmp[0], 0, 1) == '+') {
        	$mobile_array['prefix'] = trim($tmp[0]);
        	$mobile_array['mobile'] = trim($tmp[1]);
		} else {
	        if (substr($user_info['mobile'], 0, 1) == '+') {

	            if (substr($user_info['mobile'], 1, 1) == '1') {
	                $mobile_array['prefix'] = trim(substr($user_info['mobile'], 0, 4));
	                $mobile_array['mobile'] = trim(substr($user_info['mobile'], 4));
	            } else {
	                $mobile_array['prefix'] = trim(substr($user_info['mobile'], 0, 3));
	                $mobile_array['mobile'] = trim(substr($user_info['mobile'], 3));
	            }
	        }
	    }        
	    $res = array(
	        'token' => $log->token,
	        'member_type' => 'member',
	        'email' => $user_info['email'],
	        'id' => $user_info['user_id'],
	        'lang' => 'en',
	        'firstname' => $user_info['firstname'],
	        'lastname' => $user_info['name'],
	        'gender' => $user_info['gender'], 
	        'member_permission' => $user_info['member_permission'],
	        'platform' => $user_info['platform'],
	        'cookie' => $log->cookie,
	        'duration' => $log->duration,
	        'prefix' => $mobile_array['prefix'],
	        'mobile' => $mobile_array['mobile']
	        );
		if($log->result > 0)
      		$_SESSION['user'] = $res;
    } catch (PDOException $e) {
    	error_log(__FILE__." ".__FUNCTION__." ".__LINE__.": PDOException: ".$e);
        api_error($e, "rmloginc");
    }
	echo ($log->result > 0) ? format_api(1, $res, 1, $log->msg) : format_api(0, null, 0, $log->msg);
	}
    
function rmloginfacebookc($email, $facebookid, $facebooktoken, $platform) {
        
	$mode = 'api';
	$login_type = 'backoffice';
	if ($platform == 'translation') {
		$login_type = 'translation';
		$mode = 'api-ajax';
	}
	
	$log = new WY_Login(set_valid_login_type($login_type));
	$res_login = $log->process_remote_facebook($email, $facebookid, $facebooktoken, $mode);
	
	$user_info = $log->signIn;
	
	$res = array(
				 'result' => $log->msg,
				 'token' => $log->token,
				 'firstname' => $user_info['firstname'],
				 'lastname' => $user_info['name'],
				 'gender' => $user_info['gender'],
				 'duration' => $log->duration,
				 'cookie' => $log->cookie);
	$_SESSION['email'] = [
	"email" => $email,
	"data" => $res,
	];
	echo ($log->result > 0) ? format_api(1, $res, 1, $log->msg) : format_api(0, null, 0, $log->msg);
    }
    
function rmlogoutc($email, $token) {
	
    $remote_type = 91;
	$platform = 'translation';
	$mode = "api-ajax";
	$log = new WY_Login(set_valid_login_type($platform));
	$log->check_login_remote($email, $token, $mode);
	if ($log->result > 0) {
		$log->logout_remote($email, $mode, $remote_type, $token);
		}
	
	echo ($log->result > 0) ? format_api(1, "1", "1", $log->msg) : format_api(0, "0", "1", $log->msg);
    }
    
function rmforgotc($email, $redirect_to) {

	$platform = 'translation';
	$mode = "api-ajax";
	$log = new WY_Login(set_valid_login_type($platform));
	$log->forgotPass($email, $redirect_to);
	echo ($log->result > 0) ? format_api(1, "1", "1", $log->msg) : format_api(1, "0", "1", $log->msg);
    }
    
function rmchangepassc($email, $password, $npassword, $token) {
	
	$platform = 'translation';
	$mode = "api-ajax";
	$log = new WY_Login(set_valid_login_type($platform));
	$log->check_login_remote($email, $token, $mode);
	if ($log->result < 0) {
		echo format_api(1, "0", "1", "");
		return;
	}
	
	$arg = new WY_Arg;
	$arg->email = $email;
	$arg->password = $password;
	$arg->npassword = $arg->rpassword = $npassword;
	$log->UpdatePassword($arg);
	echo ($log->result > 0) ? format_api(1, "1", "1", $log->msg) : format_api(1, "0", "1", $log->msg);
    }
    
function rmcheckstatusc($email, $token) {
	
	$platform = 'translation';
	$mode = "api-ajax";
	$log = new WY_Login(set_valid_login_type($platform));
	$log->check_login_remote($email, $token, $mode);
	echo ($log->result > 0) ? format_api(1, "1", "1", $log->msg) : format_api(1, "0", "1", $log->msg);
    }
    
function rmregisterc($email, $token) {
	echo format_api(-1, 0, 0, "Not implemented for backoffice/tms/callcenter");
	}

?>