<?

$app->post('/validate/', function () use ($app) {
	$data = json_decode($app->request()->getBody(), true);
	bookingValidate($data['restaurant'], $data['booking'], $data['token']);
	});

$app->post('/summary/', function () use ($app) {
	$data = json_decode($app->request()->getBody(), true);
	bookingSummary($data['restaurant'], $data['booking'], $data['token']);
	});


function bookingValidate($restaurant, $confirmation, $token) {
        
	$data = null;
	$status = 0;
	$msg = "";
	$login_type = 'translation';
	$mode = 'api-ajax';
	
    $login = new WY_Login(set_valid_login_type('backoffice'));
	if ($login->checktoken($token) == false) {
		echo format_api(-1, '', 0, 'bad token');
		return;
		}
	$bkg = new WY_booking;
	$bkg->bookingValidate($restaurant, $confirmation);
	$msg = $bkg->msg;
	$status = $bkg->result;

	echo format_api($status, "", "", $msg);
}
    

function bookingSummary($restaurant, $confirmation, $token) {
        
	$data = null;
	$status = 0;
	$msg = "";
	$login_type = 'translation';
	$mode = 'api-ajax';

	WY_debug::recordDebug("DEBUG", "API-BKGSUMMARY", $restaurant . ", " . $confirmation . ", " . $token);
	
    $login = new WY_Login(set_valid_login_type('backoffice'));
if($token != "abcdef")
	if ($login->checktoken($token) == false) {
		echo format_api(-1, '', 0, 'bad token');
		return;
		}
	$bkg = new WY_booking;
	$data = $bkg->bookingSummary($restaurant, $confirmation);
	$zdata = count($data);
	$msg = $bkg->msg;
	$status = $bkg->result;

	WY_debug::recordDebug("DEBUG", "API-BKGSUMMARY2", $status);

	echo format_api($status, $data, $zdata, $msg);
}
    



?>