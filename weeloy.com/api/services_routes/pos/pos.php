<?

$app->post('/open/', function () use ($app) {
	$data = json_decode($app->request()->getBody(), true);
	return openPos($data['restaurant'], $data['booking'], $data['token']);
	});

$app->post('/status/',function () use ($app) {
	$data = json_decode($app->request()->getBody(), true);
    return getstatusPos($data['restaurant'], $data['booking'], $data['token']);
});


function openPos($restaurant, $booking, $token) {
	$login_type = 'backoffice';
	$mode = 'api-ajax';
	
	try {
		$login = new WY_Login(set_valid_login_type($login_type));
		if ($login->checktoken($token) == false) {	// temporary hack as callcenter has no login !!
			echo format_api(-1, null, 0, 'bad token');
			return;
			}
			
		$bkg = new WY_Booking();
		if ($bkg->getBooking($booking) == false || $bkg->restaurant != $restaurant || $bkg->restable != '') {
			echo format_api(-1, null, 0, 'invalid confirmation or reattribution');
			return;
			}
			
        $pos = new WY_POS_integration($bkg);
		if ($pos->result < 0) {
			echo format_api(-1, null, 0, 'Unable to create pos object');
			return;
			}
	
		$pos->result = $pos->openOrder();		
		if ($pos->result) {
			$bkg->addtoGeneric('{"more":"{’statepos’: ’open’}"}');
			echo format_api(1, null, 1, "open");
			}
		else echo format_api(-1, null, 0, "unable to open");
	} catch (PDOException $e) { api_error($e, "openPos"); }
}

function getstatusPos($restaurant, $booking, $token) {
	$login_type = 'backoffice';
	$mode = 'api-ajax';
	
	try {
		$login = new WY_Login(set_valid_login_type($login_type));
		if ($login->checktoken($token) == false) {	// temporary hack as callcenter has no login !!
			echo format_api(-1, null, 0, 'bad token');
			return;
			}
		$bkg = new WY_Booking();
		if ($bkg->getBooking($booking) == false || $bkg->restaurant != $restaurant || $bkg->restable != '') {
			echo format_api(-1, null, 0, 'invalid confirmation');
			return;
			}
			
        $pos = new WY_POS_integration($bkg);
		if ($pos->result < 0) {
			$bkg->addtoGeneric('{"more":"{’statepos’: ’error’}"}');
			echo format_api(-1, null, 0, 'Unable to create pos object');
			return;
			}
	
		$res = $pos->updateTableListOpenorder();
		if(!isset($res['status']) || $res['status'] != "open") {
			$data['info'] = "close";
			$data['state'] = "close";
			$bkg->addtoGeneric('{"more":"{’statepos’: ’done’}"}');
			echo format_api(1, $data, 1, "");
			return;
			}
			
		if(!isset($res['table']) || !isset($res['new_table']) ) {
			$data['info'] = "unable to read table";
			$data['state'] = "error";
			$bkg->addtoGeneric('{"more":"{’statepos’: ’error’}"}');
			echo format_api(1, $data, 1, "unknown");
			return;
			}
			
		if($res['table'] != $res['new_table'] && $res['new_table'] != "" ) {
			$data['info'] = $res;
			$data['state'] = "newtable";
			$data['new_table'] = $res['new_table'];
			$bkg->addtoGeneric('{"more":"{’statepos’: ’done’}"}');
			echo format_api(1, $data, 1, "");
			return;
			}

		$res = $pos->getRemoteOrderDetails();
		if($res != null && isset($res['summary']['total-amount']) && $res['summary']['total-amount'] != '0') {	
			$data['info'] = $res['summary']['total-amount'];
			$data['state'] = "gotorder";
			$bkg->addtoGeneric('{"more":"{’statepos’: ’done’}"}');
			echo format_api(1, $data, 1, "");
			}
		else {
			$data['info'] = "";
			$data['state'] = "nochange";
			echo format_api(1, $data, 1, "");
			}
	} catch (PDOException $e) { api_error($e, "getstatusPos"); }
}

?>