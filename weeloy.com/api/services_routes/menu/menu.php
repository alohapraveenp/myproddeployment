<?

$app->post('/read/', function () use ($app) {
  $data = json_decode($app->request()->getBody(),true);
  readMenu($data['restaurant'], $data['email'], $data['token']);
});

$app->post('/create/', function () use ($app) {
  $data = json_decode($app->request()->getBody(),true);
  createMenu($data['restaurant'], $data['email'], $data['token'], $data['menus']);
});

$app->post('/update/', function () use ($app) {
  $data = json_decode($app->request()->getBody(),true);
  updateMenu($data['restaurant'], $data['email'], $data['token'], $data['menus']);
});

$app->post('/delete/', function () use ($app) {
  $data = json_decode($app->request()->getBody(),true);
  deleteMenu($data['restaurant'], $data['email'], $data['token'], $data['menuID']);
});

function readMenu($restaurant, $email, $token) {
	$data = null;
	$status = $zdata = 0;
	$login_type = 'backoffice';
	$mode = $msg = "";
		
    try {
	    $log = new WY_Login(set_valid_login_type($login_type));
		$log->check_login_remote($email, $token, $mode);
		$msg = $log->msg;
		if ($log->result > 0) {
			$menu = new WY_Menu($restaurant);
			$obj = $menu->getfullMenus();
			$status = $menu->result;
			$msg = $menu->msg;
			if ($menu->result > 0) {
				$data = array('restaurant' => $restaurant, 'currency' => $obj['currency'], 'menus' => $obj['menus'], 'menus_items' => $obj['menus_items']);
				$zdata = count($obj);
				}
			}
	} catch (PDOException $e) { api_error($e, "readMenu"); }
	echo format_api($status, $data, $zdata, $msg);
}

function createMenu($restaurant, $email, $token,  $obj) {
	$data = null;
	$status = $zdata = 0;
	$login_type = 'backoffice';
	$mode = $msg = "";
	
    try {
	    $log = new WY_Login(set_valid_login_type($login_type));
		$log->check_login_remote($email, $token, $mode);
		$msg = $log->msg;
		if ($log->result > 0) {
                        $obj['email'] = $email;
			$menu = new WY_Menu($restaurant);
                        $menus = $obj["menus"];
                        if(isset($menus) && !$menus['global'] ){
                          $menu->createfullMenus($obj);
                        }else{
			 $menu->createglobalmenu($email,$obj);
                        }
			$status = $menu->result;
			$msg = $menu->msg;
			}
	} catch (PDOException $e) { api_error($e, "createMenu"); }
    echo format_api($status, $data = "ok", $zdata = 1, $msg);
}

function updateMenu($restaurant, $email, $token,  $obj) {
	$data = null;
	$status = $zdata = 0;
	$login_type = 'backoffice';
	$mode = $msg = "";
		
    try {
	    $log = new WY_Login(set_valid_login_type($login_type));
		$log->check_login_remote($email, $token, $mode);
		$msg = $log->msg;
		if ($log->result > 0) {
			$menu = new WY_Menu($restaurant);
                        $menus = $obj["menus"];
           
                        if($menus["global"]){
              
                            $menu->updateGlobalmenu($email,$obj);
                        }else{
			 $menu->updatefullMenus($obj);
                        }
			$status = $menu->result;
			$msg = $menu->msg;
			}
	} catch (PDOException $e) { api_error($e, "updateMenu"); }
    echo format_api($status, $data = "ok", $zdata = 1, $msg);
}

function deleteMenu($restaurant, $email, $token,  $menuID) {
	$data = null;
	$status = $zdata = 0;
	$login_type = 'backoffice';
	$mode = $msg = "";
		
    try {
	    $log = new WY_Login(set_valid_login_type($login_type));
		$log->check_login_remote($email, $token, $mode);
		$msg = $log->msg;
		if ($log->result > 0) {
			$menu = new WY_Menu($restaurant);
			$menu->deletefullMenus($menuID);
			$status = $menu->result;
			$msg = $menu->msg;
			}
	} catch (PDOException $e) { api_error($e, "deleteMenu"); }
    echo format_api($status, $data = "ok", $zdata = 1, $msg);
}



?>