<?

$app->post('/readall/', function () use ($app) {
  $data = json_decode($app->request()->getBody(),true);
  readAllInvoice($data['date'], $data['email'], $data['token']);
});

$app->post('/read/', function () use ($app) {
  $data = json_decode($app->request()->getBody(),true);
  readInvoice($data['restaurant'], $data['date'], $data['email'], $data['token']);
});

$app->post('/create/', function () use ($app) {
  $data = json_decode($app->request()->getBody(),true);
  createInvoice($data['restaurant'], $data['date'], $data['subtotal'], $data['license'], $data['currency'], $data['segments'], $data['details'], $data['email'], $data['token']);
});

$app->post('/total/', function () use ($app) {
  $data = json_decode($app->request()->getBody(),true);
  totalInvoice($data['restaurant'], $data['date'], $data['total'], $data['email'], $data['token']);
});

$app->post('/readallconf/', function () use ($app) {
  $data = json_decode($app->request()->getBody(),true);
  readAllInvConf($data['email'], $data['token']);
});

$app->post('/readconf/', function () use ($app) {
  $data = json_decode($app->request()->getBody(),true);
  readInvConf($data['restaurant'], $data['email'], $data['token']);
});

$app->post('/updatestatus/', function () use ($app) {
  $data = json_decode($app->request()->getBody(),true);
  updateInvStatus($data['restaurant'], $data['invoicedate'], $data['status'], $data['email'], $data['token']);
});

$app->post('/createconf/', function () use ($app) {
  $data = json_decode($app->request()->getBody(),true);
  createInvConf($data['email'], $data['token'], $data['invoice']);
});

$app->post('/updateconf/', function () use ($app) {
  $data = json_decode($app->request()->getBody(),true);
  updateInvConf($data['email'], $data['token'], $data['invoice']);
});

$app->post('/deleteconf/', function () use ($app) {
  $data = json_decode($app->request()->getBody(),true);
  deleteInvConf($data['restaurant'], $data['email'], $data['token']);
});



function readInvoice($restaurant, $date, $email, $token) {
	$data = null;
	$status = $zdata = 0;
	$login_type = 'backoffice';
	$mode = $msg = "";
		
    try {
	    $log = new WY_Login(set_valid_login_type($login_type));
		$log->check_login_remote($email, $token, $mode);
		$msg = $log->msg;
		error_log('LOGIN readInvoice '. $log->result . " $restaurant, $email, $token");
		if ($log->result > 0) {
			$invoice = new WY_Invoice();
			$data = $invoice->readInvoice($restaurant, $date);
			$status = $invoice->result;
			$msg = $invoice->msg;
			if ($invoice->result > 0) {
				$data = array('invoice' => $data);
				$zdata = count($data);
				}
			}
	} catch (PDOException $e) { api_error($e, "readInvoivce"); }
	echo format_api($status, $data, $zdata, $msg);
}

function readAllInvoice($date, $email, $token) {
	$data = null;
	$status = $zdata = 0;
	$login_type = 'backoffice';
	$mode = $msg = "";
		
    try {
	    $log = new WY_Login(set_valid_login_type($login_type));
		$log->check_login_remote($email, $token, $mode);
		$msg = $log->msg;
		error_log('LOGIN readAllInvoice '. $log->result . "$email, $token");
		if ($log->result > 0) {
			$invoice = new WY_Invoice();
			$data = $invoice->readAllInvoice($date);
			$status = $invoice->result;
			$msg = $invoice->msg;
			if ($invoice->result > 0) {
				$data = array('invoice' => $data);
				$zdata = count($data);
				}
			}
	} catch (PDOException $e) { api_error($e, "readInvoivce"); }
	echo format_api($status, $data, $zdata, $msg);
}

function createInvoice($restaurant, $date, $subtotal, $license, $currency, $segments, $details, $email, $token) {
	$data = null;
	$status = $zdata = 0;
	$login_type = 'backoffice';
	$mode = $msg = "";
		
    try {
	    $log = new WY_Login(set_valid_login_type($login_type));
		$log->check_login_remote($email, $token, $mode);
		$msg = $log->msg;
		if ($log->result > 0) {
			$invoice = new WY_Invoice();
			$invoice->createInvoice($restaurant, $date, $subtotal, $license, $currency, $segments, $details);
			$status = $invoice->result;
			$msg = $invoice->msg;
			}
	} catch (PDOException $e) { api_error($e, "createInvoice"); }
    echo format_api($status, "ok", $zdata = 1, $msg);
}

function totalInvoice($restaurant, $date, $total, $email, $token) {
	$data = null;
	$status = $zdata = 0;
	$login_type = 'backoffice';
	$mode = $msg = "";
		
    try {
	    $log = new WY_Login(set_valid_login_type($login_type));
		$log->check_login_remote($email, $token, $mode);
		$msg = $log->msg;
		if ($log->result > 0) {
			$invoice = new WY_Invoice();
			$invoice->totalInvoice($restaurant, $date, $total);
			$status = $invoice->result;
			$msg = $invoice->msg;
			}
	} catch (PDOException $e) { api_error($e, "createInvoice"); }
    echo format_api($status, "ok", $zdata = 1, $msg);
}

function readInvConf($restaurant, $email, $token) {
	$data = null;
	$status = $zdata = 0;
	$login_type = 'backoffice';
	$mode = $msg = "";
		
    try {
	    $log = new WY_Login(set_valid_login_type($login_type));
		$log->check_login_remote($email, $token, $mode);
		$msg = $log->msg;
		error_log('LOGIN readInvCon '. $log->result . " $restaurant, $email, $token");
		if ($log->result > 0) {
			$invoice = new WY_Invoice();
			$obj = $invoice->readInvConfig($restaurant);
			$status = $invoice->result;
			$msg = $invoice->msg;
			if ($invoice->result > 0) {
				$data = array('config' => $obj["config"]);
				$zdata = count($obj);
				}
			}
	} catch (PDOException $e) { api_error($e, "readInvConf"); }
	echo format_api($status, $data, $zdata, $msg);
}

function readAllInvConf($email, $token) {
	$data = null;
	$status = $zdata = 0;
	$login_type = 'backoffice';
	$mode = $msg = "";
		
    try {
	    $log = new WY_Login(set_valid_login_type($login_type));
		$log->check_login_remote($email, $token, $mode);
		$msg = $log->msg;
		error_log('LOGIN readInvCon '. $log->result . " $email, $token");
		if ($log->result > 0) {
			$invoice = new WY_Invoice();
			$obj = $invoice->readAllInvConfig();
			$status = $invoice->result;
			$msg = $invoice->msg;
			if ($invoice->result > 0) {
				$data = array('config' => $obj["config"]);
				$zdata = count($obj);
				}
			}
	} catch (PDOException $e) { api_error($e, "readAllInvConf"); }
	echo format_api($status, $data, $zdata, $msg);
}

function updateInvStatus($restaurant, $invoicedate, $invstatus, $email, $token) {
	$data = null;
	$status = $zdata = 0;
	$login_type = 'backoffice';
	$mode = $msg = "";
		
    try {
	    $log = new WY_Login(set_valid_login_type($login_type));
		$log->check_login_remote($email, $token, $mode);
		$msg = $log->msg;
		if ($log->result > 0) {
			$invoice = new WY_Invoice();
			$invoice->updateStatus($restaurant, $invoicedate, $invstatus);
			$status = $invoice->result;
			$msg = $invoice->msg;
			}
	} catch (PDOException $e) { api_error($e, "updateInvConf"); }
    echo format_api($status, $data = "ok", $zdata = 1, $msg);
}

function createInvConf($email, $token,  $obj) {
	$data = null;
	$status = $zdata = 0;
	$login_type = 'backoffice';
	$mode = $msg = "";
		
    try {
	    $log = new WY_Login(set_valid_login_type($login_type));
		$log->check_login_remote($email, $token, $mode);
		$msg = $log->msg;
		if ($log->result > 0) {
			$invoice = new WY_Invoice();
			$invoice->createInvConfig($obj);
			$status = $invoice->result;
			$msg = $invoice->msg;
			}
	} catch (PDOException $e) { api_error($e, "createInvConf"); }
    echo format_api($status, $data = "ok", $zdata = 1, $msg);
}

function updateInvConf($email, $token,  $obj) {
	$data = null;
	$status = $zdata = 0;
	$login_type = 'backoffice';
	$mode = $msg = "";
		
    try {
	    $log = new WY_Login(set_valid_login_type($login_type));
		$log->check_login_remote($email, $token, $mode);
		$msg = $log->msg;
		if ($log->result > 0) {
			$invoice = new WY_Invoice();
			$invoice->updateInvConfig($obj);
			$status = $invoice->result;
			$msg = $invoice->msg;
			}
	} catch (PDOException $e) { api_error($e, "updateInvConf"); }
    echo format_api($status, $data = "ok", $zdata = 1, $msg);
}

function deleteInvConf($restaurant, $email, $token) {
	$data = null;
	$status = $zdata = 0;
	$login_type = 'backoffice';
	$mode = $msg = "";
		
    try {
	    $log = new WY_Login(set_valid_login_type($login_type));
		$log->check_login_remote($email, $token, $mode);
		$msg = $log->msg;
		if ($log->result > 0) {
			$invoice = new WY_Invoice();
			$invoice->deleteInvConfig($restaurant);
			$status = $invoice->result;
			$msg = $invoice->msg;
			}
	} catch (PDOException $e) { api_error($e, "deleteInvConf"); }
    echo format_api($status, $data = "ok", $zdata = 1, $msg);
}




?>