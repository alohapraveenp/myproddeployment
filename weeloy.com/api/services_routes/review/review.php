<?

$app->post('/response/', function () use ($app) {
  $data = json_decode($app->request()->getBody(),true);
  setReviewResponse($data['restaurant'], $data['confirmation'], $data['response'], $data['email'], $data['token']);
});


function setReviewResponse($restaurant, $confirmation, $response, $email, $token) {
	$data = null;
	$status = $zdata = 0;
	$login_type = 'backoffice';
	$mode = $msg = "";

    try {
	    $log = new WY_Login(set_valid_login_type($login_type));
		$log->check_login_remote($email, $token, $mode);
		$msg = $log->msg;
		if ($log->result > 0) {
			$review = new WY_Review($restaurant);
			$review->saveResponse($confirmation, $response);
			$status = $review->result;
			$msg = $review->msg;
			}
	} catch (PDOException $e) { api_error($e, "reviewResponse"); }
    echo format_api($status, $data = "ok", $zdata = 1, $msg);
}



?>