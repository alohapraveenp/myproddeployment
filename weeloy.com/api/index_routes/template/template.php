<?

$app->post('/login/:restaurant_id', function ($restaurant_id) use ($app) {
    $data = json_decode($app->request()->getBody(), true);
    templateLogin($restaurant_id, $data['email'], $data['password']);
});

$app->post('/users/:restaurant_id', function ($restaurant_id) use ($app) {
    $data = json_decode($app->request()->getBody(), true);
    templateAddUser($restaurant_id, $data['email'], $data['password']);
});
$app->post('/users/update/:user_id', function ($user_id) use ($app) {
    $data = json_decode($app->request()->getBody(), true);
    templateUpdateUser($user_id, $data['restaurant_id'], $data['old_password'], $data['new_password']);
});

/**
 * check login
 *
 * @param string $email, string $password
 * @return mixed
 */
function templateLogin($restaurant_id, $email, $password) {
    $sql = "SELECT * FROM template_users WHERE email = '{$email}' AND restaurant_id = '{$restaurant_id}'";
    $db = getConnection();
    $stmt = $db->prepare($sql);
    $stmt->execute();
    $user = $stmt->fetch(PDO::FETCH_OBJ);
    if($user == false) {
        echo format_api(0, false, 0, "User not found");
    } else {

        if (password_verify($password, $user->password)) {
            unset($user->password);
            echo format_api(1, $user, 1, null);
    } else {
            echo format_api(0, false, 0, "password does not match");
        }
    }
}

/**
 * create new user for template website
 *
 * @param string $email, string $password
 * @return mixed
 */
function templateAddUser($restaurant_id, $email, $password) {
    $sql = "SELECT id, email FROM template_users WHERE email = '{$email}' AND restaurant_id = '{$restaurant_id}'";
        $db = getConnection();
        $stmt = $db->prepare($sql);
        $stmt->execute();
    $user = $stmt->fetch(PDO::FETCH_OBJ);
    if($user) {
        echo format_api(0, false, 0, "Email already exist");
        } else {
        $hash = password_hash($password, PASSWORD_DEFAULT);
        $sql = "INSERT INTO template_users(email, password, restaurant_id) VALUES ('{$email}', '{$hash}', '$restaurant_id')";
        $stmt = $db->prepare($sql);
        $stmt->execute();
        echo format_api(1, true, 1, null);
    }
}

/**
 * update user password
 *
 * @param int $user_id, string $old_password, string $new_password
 * @return mixed
 */
function templateUpdateUser($user_id, $restaurant_id, $old_password, $new_password) {
    $sql = "SELECT id, email, password FROM template_users WHERE id = '{$user_id}' AND restaurant_id = '{$restaurant_id}'";
    $db = getConnection();
    $stmt = $db->prepare($sql);
    $stmt->execute();
    $user = $stmt->fetch(PDO::FETCH_OBJ);
    if($user == false) {
        echo format_api(0, false, 0, "User not found");
    } else {
        if(!password_verify($old_password, $user->password)) {
            echo format_api(0, false, 0, "password does not match");
        } else {
            $hash = password_hash($new_password, PASSWORD_DEFAULT);
            $sql = "UPDATE template_users SET password='{$hash}'";
    $stmt = $db->prepare($sql);
    $stmt->execute();
            echo format_api(1, true, 1, null);
}
    }
}



?>