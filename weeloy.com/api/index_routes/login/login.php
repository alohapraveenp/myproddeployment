<?

function GetFacebookUser($email, $facebookid, $facebooktoken) {

    $log = new WY_Login(set_valid_login_type('member'));
    $data = $log->get_facebook_member($email, $facebookid, $facebooktoken);
    echo ($log->result > 0) ? format_api(1, $data, 1, $log->msg) : format_api(0, NULL, 0, $log->msg);
}

function rmloginc_facebook($app) {
//return state 
//
//
//$email, $facebookid, $facebooktoken
    // 0  not login
    // 1 login 
    // 2 login but need to update api 

    $email = $app->request->post("email");
    $facebookid = $app->request->post("facebookid");
    $facebooktoken = $app->request->post("facebooktoken");
    $appname = $app->request->post("name");
    $appversion = $app->request->post("version");

    rmloginfacebookc($email, $facebookid, $facebooktoken, $appname, $appversion);
}

function rmloginfacebookc($email, $facebookid, $facebooktoken, $appname, $appversion) {

    $restau = '';
    $login_message = '';
    $err = '';
    //check app version and return error if too old
    if ($appversion == '0') {
        $login_message = 'login_failed_update';
        $res = array('result' => $login_message);
        echo format_api(0, $res, 0, "This version is too old. Please update your App");
        return;
    }
    if ($appversion == '0.1') {
        $login_message = 'login_succeed_update';
        $err = "This version is too old. Please update your App";
    }
    // if restaurant manager -> get restaurant name
    $mode = 'api';
    $login_type = 'backoffice';
    if ($appversion == 'translation') {
        $login_type = 'translation';
        $mode = 'api-ajax';
    }
    if ($appname == 'weeloy') {
        $login_type = 'member';
    }

    $log = new WY_Login(set_valid_login_type($login_type));
    $res_login = $log->process_remote_facebook($email, $facebookid, $facebooktoken, $mode);
    if ($login_message == '' && $res_login != '') {
        $login_message = 'login_succeed';
    }
    //$res = $_SESSION['user'];
    //$res['result'] = $login_message;
    
    $session = $_SESSION['user'];

    $res = array(
      'result' => $login_message,
      'token' => $session['token'],
      'member_type' => $session['member_type'],
      'firstname' => $session['firstname'],
      'lastname' => $session['name'],
      'salutation'=>$session['salutation'],
      'gender' => $session['gender'], 
      'cookie' => $session['cookie'],
      'duration' => $session['duration'],
      'prefix' => $session['prefix'],
      'mobile' => $session['mobile'], 
      'affiliate_program' => $session['affiliate_program']);

    
    echo ($log->result > 0) ? format_api(1, $res, 1, $err) : format_api(0, NULL, 0, $log->msg);
}

function rmlogout($email, $platform = "api") {
    $mode = $platform;
    $remote_type = 91;
    $token = "";
    
    if($platform == 'weeloy.com'){
        $mode = $platform = 'member';
        $remote_type = 90;
    }

    $log = new WY_Login(set_valid_login_type($platform));
    $log->logout_remote($email, $mode, $remote_type, $token);
    unset($_SESSION['user']);
    echo ($log->result > 0) ? format_api(1, NULL, 1, "loggedout") : format_api(0, NULL, 0, "loggedout");
}

function rmlogoutc($email, $token, $platform) {

    $remote_type = 91;
    $mode = ($platform != 'translation') ? "api" : "api-ajax";
    $log = new WY_Login(set_valid_login_type($platform));
    $log->check_login_remote($email, $token, $mode);
    if ($log->result > 0)
        $log->logout_remote($email, $mode, $remote_type, $token);
    echo ($log->result > 0) ? format_api(1, "1", "1", "") : format_api(0, "0", "1", "");
}

function rmforgotc($email, $platform) {
    $log = new WY_Login(set_valid_login_type($platform));
    $log->forgotPass($email, 'weeloy.com');
    echo ($log->result > 0) ? format_api(1, "1", "1", "") : format_api(1, "0", "1", "");
}

function rmchangepassc($email, $password, $npassword, $token, $platform) {

    $mode = ($platform != 'translation') ? "api" : "api-ajax";
    $log = new WY_Login(set_valid_login_type($platform));
    $log->check_login_remote($email, $token, $mode);
    if ($log->result < 0) {
        echo format_api(1, "0", "1", "");
        return;
    }

    $arg = new WY_Arg;
    $arg->email = $email;
    $arg->password = $password;
    $arg->npassword = $arg->rpassword = $npassword;
    $log->UpdatePassword($arg);
    echo ($log->result > 0) ? format_api(1, "1", "1", "") : format_api(1, "0", "1", "");
}

function rmcheckstatusc($email, $token, $platform) {

    $mode = ($platform != 'translation') ? "api" : "api-ajax";
    $log = new WY_Login(set_valid_login_type($platform));
    $log->check_login_remote($email, $token, $mode);
    echo ($log->result > 0) ? format_api(1, "1", "1", "") : format_api(1, "0", "1", "");
}

/* add platform */

function rmreadlogin($email, $token) {

    $log = new WY_Login(set_valid_login_type('backoffice'));
    $log->check_login_remote($email, $token, 'api');
    echo ($log->result > 0) ? format_api(1, NULL, 1, "loggedin") : format_api(0, NULL, 0, "loggedout");
}

function getLoginMemberDetails($email,$password) {
   $log = new WY_Login(set_valid_login_type('backoffice'));
   $member = new WY_Member();
   $data = $member->getMemberDetails($email,$password);
   echo format_api(1, $data, 0, 0);
}

function sendAuthResetLink($email){
    try {
     if( !isset($email)) {
            throw new Exception("input not valid", 1);
      }
      $log = new WY_Login(set_valid_login_type('backoffice'));
      $link = $log->generateResetAuthlink($email);
      format_api(1, "1", "1", "");
    } catch (Exception $e) {
        echo format_api(0, $e->getMessage(), 0, null);
    }
}
function resetGooleAuth($email){
    try {
     if( !isset($email)) {
            throw new Exception("input not valid", 1);
      }
      $log = new WY_Login(set_valid_login_type('backoffice'));
      $data = $log->reset_google_auth($email);
      echo format_api(1, $data, 0, 0);
    } catch (Exception $e) {
        echo format_api(0, $e->getMessage(), 0, null);
    }
}


function ResetPassword($email, $token, $password) {
    try {
        if(!isset($password) || !isset($email) || !isset($token) || $password == '') {
            throw new Exception("input not valid", 1);
        }
        $sql  = "SELECT ID, email, reset_token FROM `login` WHERE email = '{$email}' AND reset_token = '{$token}' AND tmpTimeStamp BETWEEN DATE_SUB(NOW(), INTERVAL 2 DAY) AND NOW() LIMIT 1";
        $db   = getConnection();
        $stmt = $db->prepare($sql);
        $stmt->execute();
        $user = $stmt->fetchAll(PDO::FETCH_OBJ);
        if(count($user) == 0) {
            throw new Exception("email not found or token invalid", 1);
        } else {
            $log = new WY_Login(set_valid_login_type('backoffice'));
            $password = $log->set_password($password);
            $user = $user[0];
            $sql = "update login set Password = '{$password}', reset_token = '' where email = '{$email}' limit 1";
            $stmt = $db->prepare($sql);
            $stmt->execute();
        }
        echo format_api(1, 'password has been reset', 1, NULL);
    } catch (Exception $e) {
        echo format_api(0, $e->getMessage(), 0, null);
    }
}

/* add platform */
function rmlogin($email, $pass, $appname = NULL, $appversion = NULL) {
//return state 
    // 0  not login
    // 1 login 
    // 2 login but need to update api 
    $sql_store_login = "INSERT INTO app_login_history (email, pass, appname, appversion) VALUES ('$email','$pass','$appname','$appversion')";
    try {
        $db = getConnection('dwh');
        $stmt = $db->prepare($sql_store_login);
        $stmt->execute();
    } catch (PDOException $e) {
        error_log(__FILE__." ".__FUNCTION__." ".__LINE__.": PDOException: ".$e);
        api_error($e, "app tracking rmlogin");
    }
    $restau = '';
    $login_message = '';
    $err = '';
    //check app version and return error if too old
    if ($appversion == '0') {
        $login_message = 'login_failed_update';
        $res = array('result' => $login_message);
        error_log(__FILE__." ".__FUNCTION__." ".__LINE__." This version is outdated. Please update using the Application Store.");
        echo format_api(0, $res, 0, "This version is outdated. Please update using the Application Store.");
        return;
    }


    if ($appname == NULL && $appversion == NULL) {
        $login_message = 'login_succeed_update';
        $err = "This version is outdated. Please update using the Application Store.";
    }

    if ($appversion == '0.05') {
        $login_message = 'login_succeed_update';
        $err = "This version is outdated. Please update using the Application Store.";
    }
    // if restaurant manager -> get restaurant name

    // $sql = "SELECT restaurant_id FROM restaurant_app_managers WHERE user_id=:query LIMIT 1";
    $sql = "SELECT restaurant_id FROM restaurants_managers WHERE member_id=:query LIMIT 1";
    try {
        $db = getConnection();
        $stmt = $db->prepare($sql);
        $stmt->bindParam("query", $email);
        $stmt->execute();
        $data = $stmt->fetch(PDO::FETCH_OBJ);
    } catch (PDOException $e) {
        error_log(__FILE__." ".__FUNCTION__." ".__LINE__.": PDOException: ".$e);
        api_error($e, "rmlogin");
    }
    if (isset($data->restaurant_id))
        $restau = $data->restaurant_id;
    $log = new WY_Login(set_valid_login_type('backoffice'));
    $log->process_login_remote($email, $pass, 'api');
    if ($login_message == '')
        $login_message = 'login_succeed';
    $res = array('result' => $login_message, 'token' => $log->token, 'member_type' => 'restaurant', 'restaurant_id' => $restau);
    echo ($log->result > 0) ? format_api(1, $res, 1, $err) : format_api(0, NULL, 0, $log->msg);
}

/* add platform */

function rmloginc($email, $pass, $appname = NULL, $appversion = NULL) {
//return state 
    // 0  not login
    // 1 login 
    // 2 login but need to update api 


    $restau = '';
    $login_message = '';
    $err = '';
    //check app version and return error if too old
    if ($appversion < '0') {
        $login_message = 'login_failed_update';
        $res = array('result' => $login_message);
        echo format_api(0, $res, 0, "This version is too old. Please update your App");
        return;
    }
    
    if ($appversion < '0') {
        $login_message = 'login_succeed_update';
        $err = "This version is too old. Please update your App";
    }
    // if restaurant manager -> get restaurant name

    $mode = 'api';
    $logintype = 'backoffice';
    if ($appversion == 'translation') {
        $logintype = 'translation';
        $mode = 'api-ajax';
    }
    $log = new WY_Login(set_valid_login_type($logintype));
    $log->process_login_remote($email, $pass, $mode);
    
    if($log->result < 1){
       echo format_api(0, NULL, 0, "Invalid Email & Password ");
       return;
    }

    if ($login_message == '') {
        $login_message = 'login_succeed';
    }
    
    
    $session = $_SESSION['user'];

    $res = array(
      'result' => $login_message,
      'token' => $session['token'],
      'member_type' => $session['member_type'],
      'firstname' => $session['firstname'],
      'lastname' => $session['name'],
      'salutation'=>$session['salutation'],
      'gender' => $session['gender'], 
      'cookie' => $session['cookie'],
      'duration' => $session['duration'],
      'prefix' => $session['prefix'],
      'mobile' => $session['mobile'], 
      'affiliate_program' => $session['affiliate_program']);


    echo ($log->result > 0) ? format_api(1, $res, 1, $err) : format_api(0, NULL, 0, $log->msg);
}

function updatePassword($email, $oldpwd, $newpwd) {

    $arg = new WY_Arg();
    $arg->email = $email;
    $arg->password = $oldpwd;
    $arg->npassword = $arg->rpassword = $newpwd;
    $arg->application = '';
    $log = new WY_Login(set_valid_login_type('backoffice'));
    $log->updatePassword($arg);

    echo ($log->result > 0) ? format_api(1, "success", 1, NULL) : format_api(0, NULL, 0, $log->msg);
}

/* add platform */

function forgotPassword($email) {

    $arg = new WY_Arg();
    $arg->email = $email;
    $arg->application = '';
    $log = new WY_Login(set_valid_login_type('backoffice'));
    $log->process_login('LostPassword', $arg);

    echo ($log->result > 0) ? format_api(1, "success", 1, NULL) : format_api(0, NULL, 0, "Update Password Failed");
}

function ChkLogin($query) {

    $queryArg = explode("_:::_", $query);
    $sql = "SELECT Email FROM login WHERE Email=:query";
    try {
        $db = getConnection();
        $stmt = $db->prepare($sql);
        $stmt->bindParam("query", $query);
        $stmt->execute();
        $login = $stmt->fetchAll(PDO::FETCH_OBJ);
        $db = null;

        $errors = null;
        $data = array("login" => $login);
        echo format_api(1, $data, count($login), $errors);

        //echo '{"login": ' . json_encode($login) . '}';
    } catch (PDOException $e) {
        api_error($e, "ChkLogin");
    }
}



?>