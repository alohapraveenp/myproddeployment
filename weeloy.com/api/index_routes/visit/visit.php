<?

$app->post('/booking/', function () use ($app) {
    $data = json_decode($app->request()->getBody(), true);
    get1booking($data['confirmation'], $data['token']);
});

$app->post('/read/', function () use ($app) {
    $data = json_decode($app->request()->getBody(), true);
    if(!isset($data['options'])) $data['options'] = "";
    getVisit($data['restaurant'], $data['mode'], $data['options'], $data['token']);
});

$app->get('/modif/:restaurant/:mode/:token', 'getmodifVisit');

$app->post('/modif/', function () use ($app) {
    $data = json_decode($app->request()->getBody(), true);
    getmodifVisit($data['restaurant'], $data['mode'], $data['token']);
});

$app->get('/cancelconfirm/:confirmation/:email', 'getConfirmation');

$app->get('/cancel/:restaurant/:confirmation/:email', 'cancelVisit');
$app->post('/cancel/:restaurant/:confirmation/:email', function() use ($app) {
    $data = $app->request->post();
    cancelVisit($data['restaurant'], $data['confirmation'], $data['email'], $data['reason']);
});

$app->post('/payment/cancel/', function() use ($app) {
    $data = $app->request->post();
    cancelrefundBooking($data['deposit'],$data['restaurant'], $data['confirmation'], $data['email'], '',$data['reason'],$data['amount'],'member',$data['payment_method']);
});

$app->post('/booking/payment/cancel/', function () use ($app) {
    $data = json_decode($app->request()->getBody(), true);
    cancelrefundBooking($data['deposit'],$data['restaurant'], $data['booking'], $data['email'], $data['token'],'',$data['amount'],'backoffice',$data['payment_method']);
});


function get1booking($confirmation, $token) {
	$data = null;
	$zdata = 0;
	$login_type = $platform = 'translation';
	$mode = 'api-ajax';
	
	try {
		$login = new WY_Login(set_valid_login_type('backoffice'));
		if ($login->checktoken($token) == false) {
			echo format_api(-1, '', 0, 'bad token');
			return;
			}
	
		$bkg = new WY_Booking();
		$data = $bkg->get1booking($confirmation);
		$zdata = ($bkg->result > 0) ? count($data) : 0;
		echo format_api($bkg->result, $data, $zdata, $bkg->msg);
	} catch (PDOException $e) { api_error($e, "get1booking"); }

}

// get the booking of the restaurant or mode -> email and get the booking of all restaurant in the account
function getVisit($restaurant, $typemode, $options, $token) {
	$data = null;
	$zdata = 0;
	$login_type = $platform = 'translation';
	$mode = 'api-ajax';
	
	try {
		$login = new WY_Login(set_valid_login_type('backoffice'));
		if ($login->checktoken($token) == false || ($typemode == "email" && $login->email != $restaurant)) {
			echo format_api(-1, '', 0, 'bad token');
			return;
			}
	
		$bkg = new WY_Booking();
		$data = $bkg->getVisit($restaurant, $typemode, $options);
		$zdata = ($bkg->result > 0) ? count($data) : 0;
		echo format_api($bkg->result, $data, $zdata, $bkg->msg);
	} catch (PDOException $e) { api_error($e, "getVisit"); }

}

function getmodifVisit($restaurant, $typemode, $token) {
	$data = null;
	$zdata = 0;
	$login_type = $platform = 'translation';
	$mode = 'api-ajax';
	
	try {
		$login = new WY_Login(set_valid_login_type('backoffice'));
		if ($login->checktoken($token) == false || ($typemode == "email" && $login->email != $restaurant)) {
			echo format_api(-1, '', 0, 'bad token');
			return;
			}
	
		$bkg = new WY_Booking();
		$data = $bkg->getmodifVisit($restaurant, $typemode);
		$zdata = ($bkg->result > 0) ? count($data) : 0;
		echo format_api($bkg->result, $data, $zdata, $bkg->msg);
	} catch (PDOException $e) { api_error($e, "getmodifVisit"); }

}

function cancelVisit($restaurant, $confirmation, $email, $reason = '', $type="", $bookeremail = "") {

    $restaurant = clean_text($restaurant);
    $bkg = new WY_Booking();
    $notification = new WY_Notification();

    if ($bkg->getBooking($confirmation) == false) {
        echo format_api(0, "FAIL", 0, "Confirmation does not exist or wrong restaurant or email");
        return;
    }

    if ($bkg->restaurant != $restaurant || $bkg->email != $email) {
        echo format_api(0, "FAIL", 0, "Confirmation does exist for a different restaurant or email ");
        return;
    }

    if ($bkg->status == 'cancel') {
        echo format_api(0, "FAIL", 0, "Confirmation has already been canceled ");
        return;
    }


    if ($bkg->cancelBooking($confirmation, $reason, $type, $bookeremail) < 0) {
        echo format_api(0, "FAIL", 0, $bkg->msg);
        return;
    }
    if(!preg_match("/tms/i", $bkg->tracking) && $type != "tms"){
        $notification->notify($bkg,'cancel');
    }
    echo format_api(1, "OK", 0, "Confirmation has been canceled");
}

//cancel the deposit booking
function cancelrefundBooking($deposit, $restaurant, $confirmation, $email, $token, $reason, $amount, $type, $payment_method){
     
        $login = new WY_Login(set_valid_login_type('backoffice'));
        $notification = new WY_Notification();
        
       if($type ==='backoffice'){
           if ($login->checktoken($token)== false){
               echo format_api(-1, '', 0, 'wrong token'); 
                return;
           }
       }
     
    //if ($login->checktoken($token)){
        $restaurant = clean_text($restaurant);
        $res = new WY_Booking();
        $pay = new WY_Payment();
        $stripe = new WY_Payment_Stripe();
        
        if ($res->getBooking($confirmation) == false) {
            echo format_api(0, "FAIL", 0, "Confirmation does not exist or wrong restaurant or email");
            return;
        }

        if ($res->restaurant != $restaurant || $res->email != $email) {
            echo format_api(0, "FAIL", 0, "Confirmation does exist for a different restaurant or email ");
            return;
        }

        if (trim($res->status) == 'cancel') {
            echo format_api(0, "FAIL", 0, "Confirmation has already been canceled ");
            return;
        }

       if($amount >0 && $res->status != 'cancel'  ){
            $payment = array(
                    'amount' => $amount,
                    'payment_type' => $payment_method,
                    'reference_id' => $confirmation,
                    'restaurant' => $restaurant,
                    'mode' =>'booking'
            );
            if(trim($payment_method) === "stripe" || trim($payment_method) === "deposit" ){
                  
                $resData = $stripe->stripeDepositRefund($deposit, $restaurant, $confirmation, $email, $token, $amount);
                $status = $resData['status'];

            }
            else if(trim($payment_method) === "carddetails"){
                 $resData = $stripe->createStripePayment($deposit, $restaurant, $confirmation, $email,$amount, $type);
                 $status =$resData['status'];

            } else if (trim($payment_method) === "reddot"){

            $payment['payment_type']= trim($payment_method);
                $payment_new = new WY_Payment_new($payment);
                $resData = $payment_new->depositRefund();
                 if(isset($resData)){
                    if($resData['result_status'] == 'accepted'){
                       $resData['status'] = $resData['result_status'];
                       $resData['message'] ='success';
                       $status ='succeeded';
                    }else{
                     $status = $resData['result_status'];
                    }
                 }
                 
            }else{ 
                 $payment_new = new WY_Payment_new($payment);
                 $resData = $payment_new->depositRefund();
                 $resData['status'] = $resData['status'];
                 $resData['message'] ='success';
            }
        }
        else{
           $status ='succeeded' ;
        }

        if($status==='REFUNDED' || $status ==='succeeded'){

            if ($res->cancelBooking($confirmation, $reason, $type, $login->email) < 0) {
                echo format_api(0, "FAIL", 0, $res->msg);
                return;
            }
            //updated booking details
            $res->getBooking($confirmation);
            $notification->notify($res, 'cancel');
           echo format_api(1, "OK", 0, "Confirmation has been canceled");
           return;
        }else{
            echo format_api(0, $resData, 0, "REFUND FAILED");
             return;
            
        }      
    //} else{
       // echo format_api(-1, '', 0, 'wrong token');
    //}
}


?>