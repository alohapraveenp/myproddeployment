<?php

$app->post('/read/twositting/', function () use ($app) {
    $data = json_decode($app->request()->getBody(), true);
    readAllotetwoSitting($data['restaurant'], $data['year'], $data['token']);
});

$app->post('/write/twositting/', function () use ($app) {
    $data = json_decode($app->request()->getBody(), true);
    writeAllotetwoSitting($data['restaurant'], $data['value'], $data['year'], $data['token']);
});

function readAllotetwoSitting($restaurant, $year, $token) {

    try {
        $login = new WY_Login(set_valid_login_type('backoffice'));
        $result = $login->checktoken($token);
		if($result < 0) {
			echo format_api(-1, '', 0, $login->msg);
			return;
			}
        $restaurant = clean_text($restaurant);
        $allote = new WY_Allote($restaurant);
        $data = $allote->readtwoSitting($restaurant, $year);
		if($allote->result > 0)
			echo format_api(1, $data, count($data), '');
		else echo format_api(-1, -1, 0, $allote->msg);

    } catch (Exception $e) {
        api_error($e, "readAllotetwoSetting");
    }
}

function writeAllotetwoSitting($restaurant, $value, $year, $token) {

    try {
        $login = new WY_Login(set_valid_login_type('backoffice'));
        $result = $login->checktoken($token);
		if($result < 0) {
			echo format_api(-1, '', 0, $login->msg);
			return;
			}
        $restaurant = clean_text($restaurant);
        $allote = new WY_Allote($restaurant);
        $data = $allote->writetwoSitting($restaurant, $value, $year);
		if($allote->result > 0)
			echo format_api(1, $data, count($data), '');
		else echo format_api(-1, -1, 0, $allote->msg);
		
    } catch (Exception $e) {
        api_error($e, "writeAllotetwoSetting");
    }
}



?>