<?

$app->post('/depositpay', function () use ($app) {
    return depositpayment($app);
});

$app->post('/paypal', function () use ($app) {
    return savePaypalPayment($app);
});

$app->get('/stripe/credentials/:restaurant', function ($restaurant) {
    return stripeCredentials($restaurant);
});

$app->get('/stripe/card/:email', function ($email) {
    return stripeCardInfo($email);
});

$app->post('/stripe/createcustomer/', function () use ($app) {
    $data = json_decode($app->request()->getBody(), true);
    return stripeCreateCustomerMobile($data);
});

$app->post('/stripe/pay/', function () use ($app) {
    $data = json_decode($app->request()->getBody(), true);
    return stripeMobilePaymentWithCustomerCreation($data);
});

$app->get('/chargedetails/:confirmation', function ($confirmation) use ($app) {
    return changeDetails($confirmation);
});


function depositpayment($app){
    $status='pending';
    $params = $app->request()->post();
   
    $action = $params['action'];
    $paymentKey = $params['paykey'];
    $amount = $params['amount'];

    
    $object_type = $params['object_type'];
    $object_id = $params['object_id'];
    
    
    $payment = new WY_Paypal();
    $status =$params['status'];
    if($action==='payment_ipn'){
         $result = $payment->updatePayresponse($paykey, $status);
    }else{
        $result = $payment->addPaykey($amount, $paymentKey, $orderId, $status, $object_type, $object_id);
        if($object_type == 'booking_deposit'){
            $booking = new WY_Booking();
            $booking->update_status($paymentKey, $object_id, $status);
        }
    }
     $errors = null;
        echo format_api(1, 1, $result, $errors);
}


function savePaypalPayment($app){
    $params = $app->request()->post();
    $payment = new WY_Paypal();

    if(!empty($params['action']) && $params['action'] === 'payment_ipn'){
         $result = $payment->updatePaymentPaypal($params['paykey'], $params['status']);
    }else{
        $result = $payment->addPaymentPaypal($params['payment_id'], $params['payment_method'], $params['receiver'], $params['amount'], $params['currency'], $params['object_type'], $params['object_id'], $params['status']);
        
        if($result && $params['object_type'] == 'booking_deposit'){
            $booking = new WY_Booking();
            $booking->updateBookingStatus($params['payment_id'], $params['object_id'], $params['status']);
            
            //$booking->notifyBooking($params['object_id']);
        }
    }
    $errors = null;
    echo format_api(1, 1, $result, $errors);
}

function stripeCredentials($restaurant){
    $stripe = new WY_Payment_Stripe();
    if(empty($restaurant)){
        echo format_api(-1, -1, 0, 'FAIL');   
    }
   $credentials = $stripe->stripeCredentials($restaurant);
    if(isset($credentials)){
     echo format_api(1, $credentials, 0, 'SUCCESS');   
  
   }else{
         echo format_api(-1, -1, 0, 'FAIL');  
     }     
}

function changeDetails($confirmation){
    $booking = new WY_Booking();
    if(empty($confirmation)){
        echo format_api(-1, -1, 0, 'FAIL');   
    }
    $changedetails = $booking->getChargeDetails($confirmation);

     echo format_api(1, $changedetails, 0, 'SUCCESS');   
}

function stripeCardInfo($email){
    
    $payment = new WY_Payment();
    if(empty($email)){
        echo format_api(-1, -1, 0, 'FAIL');   
    }
   $card = $payment->getStripeCardInfoByEmail($email);
    if(isset($card)){
     echo format_api(1, $card, 0, 'SUCCESS');   
  
   }else{
         echo format_api(-1, -1, 0, 'FAIL');  
     } 
    
    
}

function stripeCreateCustomerMobile($data){
    $stripe = new WY_Payment_Stripe();
    $res = new WY_restaurant();
     
    $missing_params  = array();
 
    $confirmation =(isset($data['confirmation'])) ? $data['confirmation'] :'' ;
    $token = (isset($data['token'])) ? $data['token'] :'';
    $restaurant =(isset($data['restaurant'])) ? $data['restaurant'] :'';
    $email = (isset($data['email'])) ? $data['email'] :'';
    $amount = (isset($data['amount'])) ? $data['amount'] :'0';
  
    if(empty($confirmation)){
       $missing_params[] = 'confirmation';
    }
    if(empty($token)){
   
        $missing_params[] = 'token';
    }
    if(empty($restaurant)){
       
        $missing_params[] = 'restaurant';
    }
    if($amount==="0" || $amount===0 || $amount=null){
        
        $missing_params[] = 'amount';
    }
    if(empty($email) ){
        $missing_params[] = 'email';
    }
    //deposit flag
    if(isset($restaurant)){
        $res->getRestaurant($restaurant);
        if($res->result < 0) {
        	WY_debug::recordDebug("ERROR", "STRIPE", "stripeCreateCustomerMobile, Invalid Restaurant =  " . $restaurant);
	    	//echo format_api(-1, '', 0, $res->msg);
	    	//return;
       		}
    }

    if(count($missing_params)>0){
        $missing_params =json_encode($missing_params);
        echo format_api(0, $missing_params, 0, 'missing params');  
        exit;
    }
  
    $customer = $stripe->stripeCreateCustomerMobile($restaurant, $confirmation, $email, $token, $data['amount'], 'mobile');

    if(!$customer['result']){
        echo format_api(0, null, 0, $customer['message']); 
        exit;
    }
    echo format_api(1, $customer, 0, 0);   
}


function stripeMobilePaymentWithCustomerCreation($data){
   
    $stripe = new WY_Payment_Stripe();
    $res = new WY_restaurant();
     
    $missing_params  = array();
    
    $confirmation =(isset($data['confirmation'])) ? $data['confirmation'] :'' ;
    $token = (isset($data['token'])) ? $data['token'] :'';
    $restaurant =(isset($data['restaurant'])) ? $data['restaurant'] :'';
    $email = (isset($data['email'])) ? $data['email'] :'';
    $amount = (isset($data['amount'])) ? $data['amount'] :'0';
  
    if(empty($confirmation)){
       $missing_params[] = 'confirmation';
    }
    
    if(empty($token)){
   
        $missing_params[] = 'token';
    }
    if(empty($restaurant)){
       
        $missing_params[] = 'restaurant';
    }
    if($amount==="0" || $amount===0 || $amount=null){
        
        $missing_params[] = 'amount';
    }
    if(empty($email) ){
        $missing_params[] = 'email';
    }
    
    if(count($missing_params)>0){
        $missing_params =json_encode($missing_params);
        echo format_api(0, $missing_params, 0, 'missing params');  
        exit;
    }
   
    //deposit flag
    if(isset($restaurant)){
        $res->getRestaurant($restaurant);
        if($res->result < 0) {
        	WY_debug::recordDebug("ERROR", "STRIPE", "stripeMobilePaymentWithCustomerCreation, Invalid Restaurant =  " . $restaurant);
	    	//echo format_api(-1, '', 0, $res->msg);
	    	//return;
       		}
    }
  
    $customer = $stripe->stripeMobilePaymentWithCustomerCreation($restaurant, $confirmation, $email, $token, $data['amount'], 'mobile');

    if(!$customer['result']){
        echo format_api(0, null, 0, $customer['message']); 
        exit;
    }
    echo format_api(1, $customer, 0, 0);   
}

?>