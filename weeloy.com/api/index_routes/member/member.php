<?

function getVisitMember($email) {

	try {
/*
		$login = new WY_Login(set_valid_login_type('backoffice'));
		if ($login->checktoken($token) == false) {
			echo format_api(-1, '', 0, 'bad token');
			return;
			}
*/	
		$bkg = new WY_Booking();
		$data = $bkg->get1booking($confirmation);
		$zdata = ($bkg->result > 0) ? count($data) : 0;
		echo format_api($bkg->result, $data, $zdata, $bkg->msg);
	} catch (PDOException $e) { api_error($e, "getVisitMember"); }
}

function madisonMemberRead($restaurant, $token) {
        
	$data = null;
	$status = $zdata = 0;
	$msg = "";
	$login_type = $platform = 'translation';
	$mode = 'api-ajax';

	try {
		$login = new WY_Login(set_valid_login_type($login_type));
		if ($token != "callcenter" && $login->checktoken($token) == false) {	// temporary hack as callcenter has no login !!
			echo format_api(-1, '', 0, 'bad token');
			return;
		}
		$prof = new WY_Booking;
		$data = $prof->readmadisonmember();
		$msg = $prof->msg;
		$status = $prof->result;
		if ($prof->result > 0) 
			$zdata = count($data);
		
		echo format_api($status, $data, $zdata, $msg);
	} catch (PDOException $e) { api_error($e, "getOneProfile"); }
}

function addMember($email, $mobile, $password, $lastname, $firstname, $platfrom, $member_type = "", $gender="", $salutation="", $country="") {

    try {
        $login = new WY_Login(set_valid_login_type('member'));

		$arg = new WY_Arg;
		$arg->email = $email;
		$arg->name = $lastname;
		$arg->firstname = $firstname;
		$arg->gender = $gender;
		$arg->salutation = $salutation;
		$arg->country = $country;
		$arg->member_type = $member_type;

        $arg->password = $password;
        $arg->rpassword = $password;

        $arg->mobile = trim($mobile);
        if(substr($mobile, 0, 1) != '+'){
            $arg->mobile = '+'.trim($mobile);
        }

        $arg->application = '';
        $arg->platform = 'member';

        $res = $login->register_remote($arg);

        if ($res > 0) {
            echo format_api(1, "success", 0, NULL);
//            /return rmlogin($email, $password);
        } else {
            if ($res == -3) {
                echo format_api(0, NULL, 0, 'This account already exists. Please verify email and mobile.');
            } else {
                echo format_api(0, NULL, 0, 'Unable to create this account. Please verify email and mobile.');
            }
        }
    } catch (PDOException $e) {
        api_error($e, "addMember");
    }
}
  
function inviteOther($emails, $bookid) {
    $invite = New IN_Friends();
    $res = $invite->inviteother($emails, $bookid);
    if ($res == 'error') {
        echo format_api(0, "FAIL", 0, "Invalid Email or An error occurred. Please try again later. ");
        return;
    } else {
        echo format_api(1, "OK", 0, "Your Invitation has been successfully sent.!!");
    }
}

function SendContactEmail($app) {
  try {
    $request = $app->request->post();
    
    try {
        $email = $request['email'];
        $message = $request['message'];
        $firstname = $request['firstname'];
        $lastname = $request['lastname'];
    } catch (Exception $e) {
        $data = json_decode($app->request()->getBody(), true);
        $email = $data['email'];
        $message = $data['message'];
        $firstname = $data['firstname'];
        $lastname = $data['lastname'];
    }

    $member = new WY_Member();
    
    $result = $member->sendContactEmail($email, $message, $firstname, $lastname);
    if($result){
        echo format_api(1, 'OK', 1, NULL);
    }
  } catch (PDOException $e) {
    api_error($e, "SendContactEmail"); 
    //var_dump($e);
  }
}

function SendContactEmail2($app) {
    try {
        $request = $app->request->post();

        $email = $request['email'];
        $message = $request['message'];
        $firstname = $request['firstname'];
        $lastname = $request['lastname'];

        $member = new WY_Member();

        $result = $member->sendContactEmail($email, $message, $firstname, $lastname);
        if ($result) {
            echo format_api(1, 'OK', 1, NULL);
        }
    } catch (PDOException $e) {
        api_error($e, "SendContactEmail2");
    }
}

function SendWebsiteContactEmail($app) {
  try {
    $request = $app->request->post();
    
    try {
        $email_client = $request['email'];
        $email_restaurant = $request['restaurant'];
        $message = $request['message'];
        $firstname = $request['firstname'];
        $lastname = $request['lastname'];
    } catch (Exception $e) {
        $data = json_decode($app->request()->getBody(), true);
        $email = $data['email'];
        $email_restaurant = $data['restaurant'];
        $message = $data['message'];
        $firstname = $data['firstname'];
        $lastname = $data['lastname'];
    }

    $member = new WY_Member();
    
    $result = $member->sendWebsiteContactEmail($email, $email_restaurant,$message, $firstname, $lastname);
    if($result){
        echo format_api(1, 'Message sent', 1, NULL);
    }
  } catch (PDOException $e) {
    api_error($e, "Contact message on website"); 
    var_dump($e);
  }
}

function NewsletterAddEmail($email) {
    try {
        $member = new WY_Member();
        if(email_validation($email) < 0) {
            echo format_api(0, "FAIL", 0, "invalid email");
            return;
        }

        $result = $member->addEmailNewsletter($email);
        if ($result > -1) {
            $data['succes'] = true;
            $data['message'] = "Thank you! ";
            echo format_api(1, $data, 1, NULL);
        } else {
            $error["error"] = true;
            $error["message"] = "wrong login and password";
            echo format_api(1, NULL, 1, $error);
        }
    } catch (PDOException $e) {
        api_error($e, "showCaseRestaurants");
    }
}

function NewsletterExternalRestaurantAddEmail($restaurant_id, $email) {
    try {
        
        $res = new WY_restaurant();
        $res->getRestaurant($restaurant_id);
        if($res->result < 0) {
	    	echo format_api(-1, '', 0, $res->msg);
	    	return;
       		}
      
        if(email_validation($email) < 0) {
            echo format_api(0, "FAIL", 0, "invalid email");
            return;
        }
        
        $notification = new WY_Notification();
        $resultat = $notification->notifyExternalWebsiteNewsletterRegistration($restaurant_id, $email);
        if ($resultat) {
            $data['succes'] = true;
            $data['message'] = "Thank you! ";
            echo format_api(1, $data, 1, NULL);
        } else {
            $error["error"] = true;
            $error["message"] = "wrong login and password";
            echo format_api(1, NULL, 1, $error);
        }
    } catch (PDOException $e) {
        api_error($e, "showCaseRestaurants");
    }
}

function listMemberRes($token){

	try {
		if($token != "dontcheck") {
			$login = new WY_Login(set_valid_login_type('backoffice'));
			if ($login->checktoken($token) == false) {
				echo format_api(-1, '', 0, 'bad token');  
				return;         
			}
		}

        $member = new WY_Member();     
        $result = $member->getListMemberRestaurantNew();
        if($member->result == 1) {
            $data['succes'] = true;
            $data['message'] = "success";
            echo format_api(1, $result, 1, NULL);
        } else {
            $error["error"] = true;
            $error["message"] = "No Record found";
            echo format_api(1, NULL, 1, $error);
        }
    } catch (PDOException $e) {
        api_error($e, "getListMemberRestaurantNew");
    }

}

function deleteMember($mem_email, $token){

	try {
		$login = new WY_Login(set_valid_login_type('backoffice'));
		if ($login->checktoken($token) == false) {
			echo format_api(-1, '', 0, 'bad token');  
			return;         
		}

        $member = new WY_Member();     
        $result = $member->deleteMember($mem_email);
        if($member->result == 1) {
            $data['succes'] = true;
            $data['message'] = "success";
            echo format_api(1, $result, 1, NULL);
        } else {
            $error["error"] = true;
            $error["message"] = "No Record found";
            echo format_api(0, NULL, 1, $error);
        }
    } catch (PDOException $e) {
        api_error($e, "getListMemberRestaurantNew");
    }

}

function readMemberResto($mem_email, $token){

	try {
		$login = new WY_Login(set_valid_login_type('backoffice'));
		if ($login->checktoken($token) == false) {
			echo format_api(-1, '', 0, 'bad token');  
			return;         
		}

        $member = new WY_Member();     
        $result = $member->readMemberResto($mem_email);
        if($member->result == 1) {
            $data['succes'] = true;
            $data['message'] = "success";
            echo format_api(1, $result, 1, NULL);
        } else {
            $error["error"] = true;
            $error["message"] = "No Record found";
            echo format_api(0, NULL, 1, $error);
        }
    } catch (PDOException $e) {
        api_error($e, "getListMemberRestaurantNew");
    }

}



function updateMemberPermission($email, $content, $token){

	try {
		$login = new WY_Login(set_valid_login_type('backoffice'));
		if ($login->checktoken($token) == false) {
			echo format_api(-1, '', 0, 'bad token');  
			return;         
		}

        $member = new WY_Member();     
        $result = $member->updateMemberPermission($email, $content);
        if($member->result == 1) {
            $data['succes'] = true;
            $data['message'] = "success";
            echo format_api(1, $result, 1, NULL);
        } else {
            $error["error"] = true;
            $error["message"] = "No Record found";
            echo format_api(0, NULL, 1, $error);
        }
    } catch (PDOException $e) {
        api_error($e, "getListMemberRestaurantNew");
    }

}

function readPermissionValue($token){

	try {
		$login = new WY_Login(set_valid_login_type('backoffice'));
		if ($login->checktoken($token) == false) {
			echo format_api(-1, '', 0, 'bad token');  
			return;         
		}

        $member = new WY_Member();     
        $result = $member->readPermissionValue();
        if ($result > -1) {
            $data['succes'] = true;
            $data['message'] = "success";
            echo format_api(1, $result, 1, NULL);
        } else {
            $error["error"] = true;
            $error["message"] = "No Record found";
            echo format_api(0, NULL, 1, $error);
        }
    } catch (PDOException $e) {
        api_error($e, "getListMemberRestaurantNew");
    }

}


function callcentermember ($email,$restaurant,$item,$token){
    try {

            $login = new WY_Login(set_valid_login_type('backoffice'));
            if ($login->checktoken($token) == false) {
                echo format_api(-1, '', 0, 'bad token'); 
                return;          
            }

            $member = new WY_Member();
            $result = $member->savecallcentermember($restaurant,$item);
            if ($result > -1) {
                $data['succes'] = true;
                $data['message'] = "success";
                echo format_api(1, $result, 1, NULL);
            }else {
                $error["error"] = true;
                $error["message"] = "Member creation error";
                echo format_api(1, NULL, 1, $error);
            } 

    } catch (PDOException $e) { api_error($e, "saveCallcentermember"); }

}

function getCallcenterMember($restaurant){

    try {

            $member = new WY_Member();
            $result = $member->getCallMemberDetails($restaurant);
            if ($result > -1) {
                $data['succes'] = true;
                $data['message'] = "success";
                echo format_api(1, $result, 1, NULL);
            } else {
                $error["error"] = true;
                $error["message"] = "No Record found";
                echo format_api(1, NULL, 1, $error);
            }
        } catch (PDOException $e) {
            api_error($e, "getCallcenterMember");
        }

    }
    function deleteccMember($data){
        try {
         $login = new WY_Login(set_valid_login_type('backoffice'));
            if ($login->checktoken($data['token']) == false) {
                echo format_api(-1, '', 0, 'bad token');
            }
            $member = new WY_Member();
            $result = $member->deleteccMember($data['email'],$data['restaurant']);
            if ($result > -1) {
                $data['succes'] = true;
                $data['message'] = "success";
                echo format_api(1, $result, 1, NULL);
            } else {
                $error["error"] = true;
                $error["message"] = "Member creation error";
                echo format_api(1, NULL, 1, $error);
            }
        } catch (PDOException $e) {
            api_error($e, "deleteccMember");
        }

    }
    
    



?>