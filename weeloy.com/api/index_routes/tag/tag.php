<?

function updaterestaurantTag($data){
	$res = new WY_restaurant();
	$result = $res->updateTag($data['tag'],$data['restaurant']);
	$errors=null;
 	echo format_api(1, $result, 0, $errors);
}

function removerestaurantTag($data){
 
	$res = new WY_restaurant();
	$result = $res->removeTag($data['tag'],$data['restaurant'],$data['type']);
	$errors=null;
	echo format_api(1, $result, 0, $errors); 
}

function createTag($data){
	require_once 'lib/class.service.inc.php';
	$service = new WY_Service();
	$tag = $service->createtag($data);
	$errors=null;
	echo format_api(1, $tag, 0, $errors);
}
   
?>