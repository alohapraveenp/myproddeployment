<?

function getAreaRestaurant($app){
    $citycode =$app->request->get('citycode');
    $area =$app->request->get('area');

     $res = new WY_restaurant();
    $restaurant = $res->getAreaRes($citycode,$area);
    $restaurant = implode(",",$restaurant);
    $errors = null;
       echo format_api(1, $restaurant, count($restaurant), $errors);
    
}


function insertSitemap($url, $nbresult) {
    try {
        $sql = "INSERT INTO sitemap(url, nb_res) VALUES ('{$url}', {$nbresult}) ON DUPLICATE KEY UPDATE nb_res = {$nbresult}, occur = occur + 1";
        $db = getConnection();
        $stmt = $db->prepare($sql);
        $stmt->execute();
        echo format_api(1, 1, 1, NULL);
    } catch (Exception $ex) {
        echo format_api(1, NULL, 1, array('error' => 'error'));
    }
}

function addIndex(){
    try {
        $sql = "SELECT * FROM restaurant";
        $db = getConnection();
        $stmt = $db->prepare($sql);
        $stmt->execute();
        $restaurants = $stmt->fetchAll(PDO::FETCH_OBJ);
        foreach ($restaurants as $restaurant) {
            $words = array();
            $words[] = soundex($restaurant->title);
            $explode_str = explode(" ", $restaurant->title);
            foreach ($explode_str as $word) {
                $words[] = soundex($word);
            }
            $explode_str = explode("|", $restaurant->cuisine);
            foreach ($explode_str as $word) {
                $words[] = soundex($word);
            }
            $words = implode(" ", $words);
            $update_sql = "UPDATE restaurant set soundex_index='{$words}' WHERE ID = '{$restaurant->ID}'";
            $stmt = $db->prepare($update_sql);
            $stmt->execute();
            echo $update_sql . '</br>';
        }
    } catch (PDOException $e) { api_error($e, "add_index"); }
}

function getResTop($app){
    $res = new WY_restaurant();
    $res_toprestaurant= $res->getTopRestaurant();
   echo format_api(1, $res_toprestaurant, count($res_toprestaurant), NULL);
}

function getBlogTopArticles($app){
    $errors=NULL;
    $res = new WY_restaurant();
    $articles= $res->getBlogArticles();
     
    echo format_api(1, $articles, count($articles), NULL);

}

function saverestcategory($data){
   $cat = new WY_category(); 

   $result =$cat->saverestcategory($data);
   $errors=null;
   if($result===0){
	   $errors='This restuarant and type already exist.Please create new restuarant category';
   }
   echo format_api(1, $result, 0, $errors);
}

function deleterestcategory($data){
   $cat = new WY_category(); 

   $result =$cat->deletecategory($data);
   $errors=null;
   
   echo format_api(1, $result, 0, $errors);
}
  function getrestManagelevel(){
	$cat = new WY_category(); 
	$result = $cat->getRestaurantcategory();
	$errors=null;
  echo format_api(0, $result, count($result), $errors);
}
   
   

?>