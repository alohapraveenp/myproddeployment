<?

$app->get('/banner/:query', 'getBanner');
$app->get('/banner/:query/:tag', 'getcobrandingBanner');

/////  easter EVENTS   /////


$app->get('/easter', function () {
   //     try {
           
   //      $event_list=array(321, 317, 316, 307, 305, 310, 315, 322);
   //      $ids = join(',',$event_list);  
   //      $sql = "SELECT e.*, r.title, r.region as area FROM event e, restaurant r WHERE e.restaurant = r.restaurant AND e.ID IN ($ids) ORDER BY e.restaurant";
        
   //      $db = getConnection();
   //      $stmt = $db->prepare($sql);
   //      $stmt->execute();
   //      $events = $stmt->fetchAll(PDO::FETCH_OBJ);
   //      if(count($events) < 0) {
   //          throw new Exception("event not found", 1);
   //      } else {
   //          echo format_api(1, $events, count($events), NULL);
   //      }
   // } catch (Exception $e) {
   //      echo format_api(0, $e->getMessage(), 1, NULL);
   // } 
      try { 
      $data = array();       
      $cluster = new WY_Cluster();
      $cluster->read('SLAVE', 'EASTER', 'EVENT', 'EVENT', '');
      if($cluster->result < 0) {
            echo format_api(-1, array(), 0, "cluster for easter day not found");
        return;
        }
       $content = $cluster->clustcontent[0];

        $eobj = new WY_Event();
        $events = $eobj->getEventSearch($content);
        $landing = $eobj->getEventLandingPageInformation("event_easter");
          
          if(count($landing) > 0) {
              $data['landing'] = $landing;
          }
          if($eobj->result > 0) 
            {
              $data['events'] = $events;
              echo format_api(1, $data, count($events), NULL);
          }else {echo format_api(-1,  $data, 0, "event for easter day not found");}
       } catch (Exception $e) {
            echo format_api(0, $e->getMessage(), 1, NULL);
          }   
});



///// mother day EVENTS /////
//  for testing. just take the following line and add it to eventmgt in backoffice -> testing cluster, event...
// 	$http.get('../api/event/mother_day').then(function(response) { return response.data; });
// previous ID = 337, 347, 359, 360, 362, 363, 366, 367, 368, 369, 371
	

$app->get('/mother_day', function () {
       try {        
		$cluster = new WY_Cluster();
		$cluster->read('SLAVE', 'MOTHERDAY', 'EVENT', 'EVENT', '');
		if($cluster->result < 0) {
        	echo format_api(-1, array(), 0, "cluster for mother day not found");
			return;
			}
            $content = $cluster->clustcontent[0];
            $eobj = new WY_Event();
            $events = $eobj->getEventSearch($content);
            $data['events'] = $events;
            $landing = $eobj->getEventLandingPageInformation("event_mother");
            if(count($landing) > 0) {
                $data['landing'] = $landing;
            }
        if($eobj->result > 0) 
            echo format_api(1, $data, count($events), NULL);
        else echo format_api(-1, array(), 0, "event for mother day not found");
 	   } catch (Exception $e) {
        	echo format_api(0, $e->getMessage(), 1, NULL);
   			}   
});
$app->get('/year_end_festive', function () {
       try {        
		$cluster = new WY_Cluster();
		$cluster->read('SLAVE', 'FESTIVE', 'EVENT', 'EVENT', '');
		if($cluster->result < 0) {
        	echo format_api(-1, array(), 0, "cluster for Festive day not found");
			return;
			}
		$content = $cluster->clustcontent[0];
	        $eobj = new WY_Event();
		$events = $eobj->getEventSearch($content);
        if($eobj->result > 0) 
            echo format_api(1, $events, count($events), NULL);
        else echo format_api(-1, array(), 0, "event for mother day not found");
 	   } catch (Exception $e) {
        	echo format_api(0, $e->getMessage(), 1, NULL);
   			}   
});




///// father day EVENTS /////

$app->get('/father_day', function () {
       try {        
		$cluster = new WY_Cluster();
                $data = array();
		$cluster->read('SLAVE', 'FATHERDAY', 'EVENT', 'EVENT', '');
		if($cluster->result < 0) {
        	echo format_api(-1, array(), 0, "cluster for father day not found");
			return;
			}
		$content = $cluster->clustcontent[0];

	    $eobj = new WY_Event();
		$events = $eobj->getEventSearch($content);
                $data['events'] = $events;
            $landing = $eobj->getEventLandingPageInformation("event_father");
            if(count($landing) > 0) {
                $data['landing'] = $landing;
            }
        if(count($data) > 0) 
            echo format_api(1, $data, count($data), NULL);
        else echo format_api(-1, array(), 0, "event for father day not found");
 	   } catch (Exception $e) {
        	echo format_api(0, $e->getMessage(), 1, NULL);
   			}   
});

///// ALL EVENTS /////

$app->get('/all', function () {
       try {
           
        $sql = "SELECT e.*, r.title, r.region as area FROM event e, restaurant r WHERE e.restaurant = r.restaurant AND end > NOW() AND r.is_displayed = 1 AND e.type = 'public' AND r.status = 'active' ORDER BY rand() ";
        
        $db = getConnection();
        $stmt = $db->prepare($sql);
        $stmt->execute();
        $events = $stmt->fetchAll(PDO::FETCH_OBJ);
        if(count($events) < 0) {
            throw new Exception("event not found", 1);
        } else {
            echo format_api(1, $events, count($events), NULL);
        }
   } catch (Exception $e) {
        echo format_api(0, $e->getMessage(), 1, NULL);
   }   
});

///// CNY EVENTS /////

$app->get('/cny', function () {
  try {
      $cluster = new WY_Cluster();
      $cluster->read('SLAVE', 'CNY', 'EVENT', 'EVENT', '');
      if($cluster->result < 0) {
        echo format_api(-1, array(), 0, "cluster for event_lny not found");
        error_log(__FILE__." ".__FUNCTION__." ".__LINE__." cluster for event_lny not found");
        return;
      }
      //error_log(__FILE__." ".__FUNCTION__." ".__LINE__." clustconent: ".print_r($cluster->clustcontent, true));
      $content = $cluster->clustcontent[0];
      $eobj = new WY_Event();
      $events = $eobj->getEventSearch($content);
      //error_log(__FILE__." ".__FUNCTION__." ".__LINE__." result: ".$eobj->result." ".count($events)." results");
      if($eobj->result > 0) {
        $landing = $eobj->getEventLandingPageInformation("event_lny");
        $data['events'] = $events;
        $data['landing'] = $landing;
        //error_log(__FILE__." ".__FUNCTION__." ".__LINE__.": data: ".print_r($data, true));
        echo format_api(1, $data, count($events), NULL);
      } else {
        error_log(__FILE__." ".__FUNCTION__." ".__LINE__." event_lny not found");
        echo format_api(-1, array(), 0, "event_lny not found");
      }
   } catch (Exception $e) {
      error_log(__FILE__." ".__FUNCTION__." ".__LINE__." Exception: ".$e);
      echo format_api(0, $e->getMessage(), 1, NULL);
   }   
});
///// VDAY EVENTS /////

$app->get('/vday', function () {
  try {
    $cluster = new WY_Cluster();
    $cluster->read('SLAVE', 'VDAY', 'EVENT', 'EVENT', '');
    if($cluster->result < 0) {
      echo format_api(-1, array(), 0, "cluster for event_valentines not found");
      error_log(__FILE__." ".__FUNCTION__." ".__LINE__." cluster for event_valentine not found");;
      return;
    }
    $content = $cluster->clustcontent[0];
    $eobj = new WY_Event();
		$events = $eobj->getEventSearch($content);
    if($eobj->result > 0) {
      $landing = $eobj->getEventLandingPageInformation("event_valentines");
      $data['events'] = $events;
      $data['landing'] = $landing;
      //error_log(__FILE__." ".__FUNCTION__." ".__LINE__.": data: ".print_r($data, true));
      echo format_api(1, $data, count($events), NULL);
    } else {
      error_log(__FILE__." ".__FUNCTION__." ".__LINE__." event_valentines not found");
      echo format_api(-1, array(), 0, "event_valentines not found");
    }
   } catch (Exception $e) {
      error_log(__FILE__." ".__FUNCTION__." ".__LINE__." Exception: ".$e);
      echo format_api(0, $e->getMessage(), 1, NULL);
   }   
});

function getBanner($type){
     try {        
        $eobj = new WY_Event();
        $landing = $eobj->getEventLandingPageInformation($type);
        if($eobj->result > 0) 
             echo format_api(1, $landing, count($landing), NULL);
             else echo format_api(-1, array(), 0, "Banner image and content not found");
 	} catch (Exception $e) {
        	echo format_api(0, $e->getMessage(), 1, NULL);
       } 

}

function getcobrandingBanner($type,$tag){
try {        
        $eobj = new WY_Event();
        $landing = $eobj->getEventLandingPageInformation($type,$tag);
        if($eobj->result > 0) 
             echo format_api(1, $landing, count($landing), NULL);
             else echo format_api(-1, array(), 0, "Banner image and content not found");
 	} catch (Exception $e) {
        	echo format_api(0, $e->getMessage(), 1, NULL);
       }

}

?>