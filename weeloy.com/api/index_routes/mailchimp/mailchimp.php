<?php
require_once("lib/class.marketing_campaign.inc.php");
$app->get('/get', function() {
	$mailchimp = new WY_MarketingCampaign();
	$result = $mailchimp->getMailChimps();
	//error_log(__FILE__." ".__FUNCTION__." ".__LINE__.": result: ".print_r($result, true));
    echo format_api(1, $result, count($result), null);	
});
$app->post('/new', function () use ($app) {
    $mailchimp = new WY_MarketingCampaign();
    echo $mailchimp->processMailChimp($_REQUEST['data'], $_FILES['file']) ? format_api(1, "Success!", 1, NULL) : format_api(0, "Invalid input parameters!", 0, NULL);
});
$app->post('/delete', function () use ($app) {
    $mailchimp = new WY_MarketingCampaign();
    echo $mailchimp->deleteMailChimp($_REQUEST['data']) ? format_api(1, "Success!", 1, NULL) : format_api(0, "Invalid input parameters!", 0, NULL);
});
$app->post('/download', function () use ($app) {
	//error_log(__FILE__." ".__FUNCTION__." ".__LINE__." data: ".print_r($_REQUEST['data'], true));
	$mailchimp = new WY_MarketingCampaign();
	echo $mailchimp->downloadMailChimp($_REQUEST['data']) ? format_api(1, "Success!", 1, NULL) : format_api(0, "Invalid input parameters!", 0, NULL);
});
?>