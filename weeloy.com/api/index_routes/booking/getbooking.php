<?php

function getBooking($confirmation){
    
    $res = new WY_Booking();//
    //decode  booking confirmation
    $deconfirmation = $res->base64url_decode($confirmation);
    if ($res->getBooking($deconfirmation) == false) {
        echo format_api(0, "FAIL", 0, "Confirmation does not exist or wrong restaurant or email");
        return;
    }
 
    $data['resto'] = $res->restaurant;
    $data['title'] = $res->restaurantinfo->title;
    $data['logo'] = $res->restaurantinfo->logo;
    $data['booking'] = $res->confirmation;
    $data['date'] = $res->rdate;
    $data['time'] = $res->rtime;
    $data['bkstatus'] = $res->status;
    $data['email'] = $res->email;
   
    $review = new WY_Review();
    $res_review = $review->getReview($deconfirmation, '', 'myreview');
    $data['reviewcount'] = count($res_review);
    $res_review['review_status'] = $review->review_status;

    $data['review'] = $res_review;
     
    $images = new WY_Media($res->restaurant);
    $data['image'] = $images->getDefaultPicture($res->restaurant);
    $errors = null;

    //$data = array("visit" => $data);
    echo format_api(1, $data, count($data), $errors);
  
}

function getConfirmation($confirmation, $email) {

    $confirmation = clean_text($confirmation);
    $email = clean_text($email);
	if(email_validation($email) < 0) {
        echo format_api(0, "FAIL", 0, "invalid email");
        return;
		}
		
    $res = new WY_Booking();
    if ($res->getBooking($confirmation) == false || $res->email != $email) {
        echo format_api(0, "FAIL", 0, "Confirmation does not exist or wrong restaurant or email");
        return;
    }

    $data['resto'] = $res->restaurant;
    $data['title'] = $res->restaurantinfo->title;
    $data['logo'] = $res->restaurantinfo->logo;
    $data['booking'] = $res->confirmation;
    $data['bkstatus'] = $res->status;
    $data['salutation'] = $res->salutation;
    $data['first'] = $res->firstname;
    $data['date'] = $res->rdate;
    $data['time'] = $res->rtime;
    $data['pers'] = $res->cover;
    $data['state'] = $res->state;
    $data['phone'] = $res->mobile;
    $data['email'] = $res->email;
    $data['wheelwin'] = $res->wheelwin;
    $data['restCode'] = $res->restCode;
    $data['membCode'] = $res->membCode;
    $data['canceldate'] = $res->canceldate;
    $data['specialrequest'] = $res->specialrequest;

    $errors = null;

    //$data = array("visit" => $data);
    echo format_api(1, $data, count($data), $errors);
}

function getWalkinsUserFilter($user, $filter, $date1, $date2) {
    $where = '';
    if ($date1 == '2013-12-01') {
        $date1 = date('Y-m-d', time() - 3600 * 24);
    }
    if ($date2 == '2015-12-01') {
        $date2 = date('Y-m-d', time() + 3600 * 24 * 1000);
    }

    if ($date1 == 'START_DATE') {
        $date1 = date('Y-m-d', time());
        $date2 = date('Y-m-d', time() + 3600 * 24 * 7);
    }

    if ($filter == 'all') {
        $where = '';
    } else {
        if ($filter == 'not_spun') {
            $where = ' AND (wheelwin IS NULL OR wheelwin LIKE "") ';
        } else {
            $where = ' AND (wheelwin IS NOT NULL && wheelwin NOT LIKE "") ';
        }
    }
    $selectresto = "b.restaurant='" . $user . "'";

    $sql = "SELECT b.ID, b.confirmation, b.type, b.restaurant, rest.title as title, rest.cuisine, b.restable, b.status, b.restCode, b.membCode, b.email, b.mobile, b.salutation, b.lastname, b.firstname, b.cdate, b.rdate, b.rtime, "
            . "b.cover, b.revenue, b.specialrequest,rest.mealtype, b.wheelsegment,wheelwin, b.wheeldesc, b.spinsource, b.browser, b.language, b.country, b.ip,"
            . "r.reviewgrade, r.foodgrade, r.ambiancegrade, r.servicegrade, r.pricegrade, r.comment, r.response_to, r.reviewdtcreate "
            . " FROM booking b "
            . "LEFT JOIN restaurant rest ON b.restaurant = rest.restaurant "
            . "LEFT JOIN review r ON b.confirmation = r.confirmation "
            . "WHERE type = 'walkin' AND " . $selectresto . " AND rdate > :date1 AND rdate < :date2 $where ORDER BY rdate ASC ";

    try {
        $db = getConnection();
        $stmt = $db->prepare($sql);
        //$stmt->bindParam("query", $user);
        $stmt->bindParam("date1", $date1);
        $stmt->bindParam("date2", $date2);
        $stmt->execute();
        $data_sql = $stmt->fetchAll(PDO::FETCH_BOTH);
        $db = null;

        $errors = null;
        $bookingsAr = array();
        foreach ($data_sql as $data) {
            $bookings['ID'] = $data['ID'];
            $bookings['confirmation'] = $data['confirmation'];
            $bookings['restaurant'] = $data['restaurant'];
            $bookings['title'] = $data['title'];
            $bookings['cuisine'] = $data['cuisine'];
            $bookings['status'] = $data['status'];
            $bookings['restCode'] = $data['restCode'];
            $bookings['membCode'] = $data['membCode'];
            $bookings['table'] = $data['restable'];
            $bookings['email'] = $data['email'];
            $bookings['mobile'] = $data['mobile'];
            $bookings['salutation'] = $data['salutation'];
            $bookings['lastname'] = $data['lastname'];
            $bookings['firstname'] = $data['firstname'];
            $bookings['cdate'] = $data['cdate'];
            $bookings['rdate'] = $data['rdate'];
            $bookings['rtime'] = $data['rtime'];
            $bookings['cover'] = $data['cover'];
            $bookings['revenue'] = $data['revenue'];
            $bookings['specialrequest'] = $data['specialrequest'];
            $bookings['mealtype'] = $data['mealtype'];
            $bookings['wheelsegment'] = $data['wheelsegment'];
            $bookings['wheelwin'] = $data['wheelwin'];
            $bookings['wheeldesc'] = $data['wheeldesc'];
            $bookings['spinsource'] = $data['spinsource'];
            $bookings['browser'] = $data['browser'];
            $bookings['language'] = $data['language'];
            $bookings['country'] = $data['country'];
            $bookings['ip'] = $data['ip'];
            if ($data['reviewgrade'] != NULL && !$data['response_to']) {
                $bookings['review']['reviewgrade'] = $data['reviewgrade'];
                $bookings['review']['foodgrade'] = $data['foodgrade'];
                $bookings['review']['ambiancegrade'] = $data['ambiancegrade'];
                $bookings['review']['servicegrade'] = $data['servicegrade'];
                $bookings['review']['pricegrade'] = $data['pricegrade'];
                $bookings['review']['comment'] = $data['comment'];
                $bookings['review']['reviewdtcreate'] = $data['reviewdtcreate'];
            } else {
                $bookings['review'] = NULL;
            }

            if ($data['reviewgrade'] != NULL && $data['response_to']) {
                $bookings['review_response']['reviewgrade'] = $data['reviewgrade'];
                $bookings['review_response']['foodgrade'] = $data['foodgrade'];
                $bookings['review_response']['ambiancegrade'] = $data['ambiancegrade'];
                $bookings['review_response']['servicegrade'] = $data['servicegrade'];
                $bookings['review_response']['pricegrade'] = $data['pricegrade'];
                $bookings['review_response']['comment'] = $data['comment'];
                $bookings['review_response']['reviewdtcreate'] = $data['reviewdtcreate'];
            } else {
                $bookings['review_response'] = NULL;
            }

            $bookingsAr[] = $bookings;
            $bookings = array();
        }

        $data = array("bookings" => $bookingsAr);
        echo format_api(1, $data, count($data), $errors);
    } catch (PDOException $e) {
        api_error($e, "getWalkinsUserFilter");
    }
}


?>