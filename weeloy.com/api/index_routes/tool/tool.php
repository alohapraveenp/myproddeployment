<?

	$app->post('/datacheck', function () use ($app) {
	    return getDataCheck();
	});

	$app->post('/columncheck', function () use ($app) {
	    return getColumnCheck($app);
	});

	$app->post('/updatecolumn', function () use ($app) {
	    return updColumn($app);
	});

	////////


	function getDataCheck () {

		$datacheck = new WY_Tool();

		$data = $datacheck->getDataCheck(); 
    
    	echo format_api(1, $data, 0, 0);

	}

	function getColumnCheck ($app) {

		$datacheck = new WY_Tool();

		$data = json_decode($app->request()->getBody(),true);

		$result = $datacheck->getColumnCheck($data['record']); 
    
    	echo format_api(1, $result, 0, 0);

	}

	function updColumn ($app) {

		$datacheck = new WY_Tool();

		$data = json_decode($app->request()->getBody(),true);

		$result = $datacheck->updColumn($data['record'], $data['id'], $data['value']); 
    
    	echo format_api(1, $result, 0, 0);

	}

?>
