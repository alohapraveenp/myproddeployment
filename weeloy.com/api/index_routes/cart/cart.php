<?

$app->get('/items', function() {
    if(isset($_SESSION['cart']) && isset($_SESSION['cart']['items'])) {
        echo format_api(1, $_SESSION['cart']['items'], count($_SESSION['cart']['items']), NULL);
    } else {
        echo format_api(0, "No items", 0, NULL);
    }
});

$app->post('/items', function () use ($app) {
    try {
        $data = json_decode($app->request()->getBody(), true);
        $result = WY_OrderOnline::getItemDetails($data);
        echo format_api(1, $result, 1, NULL);
        
    } catch (Exception $e) {
        echo format_api(0, $e->getMessage(), 0, null);
    }
});

$app->put('/items/:item_id', function($item_id) use ($app) {
    $data = json_decode($app->request()->getBody(), true);
    try {
        $issetInCart = WY_OrderOnline::putItemInCart($item_id, $data['quantity']);
        if($issetInCart) {
            echo format_api(1, "update cart successfully", 1, NULL);
        } else {
            throw new Exception('item does not exist in cart', 1);
        }
    } catch (Exception $e) {
        echo format_api(0, $e->getMessage(), 0, null);
    }
});

$app->delete('/items/:item_id', function($item_id) {
    try {
        $issetInCart = WY_OrderOnline::deleteItemInCart($item_id);
        if($issetInCart) {
            echo format_api(1, "remove item in cart successfully", 1, NULL);
        } else {
            throw new Exception('item does not exist in cart', 1);
        }
    } catch (Exception $e) {
        echo format_api(0, $e->getMessage(), 0, NULL);
    }
});

$app->get('', function() {
    try {
        $cart = WY_OrderOnline::getCart();
        echo format_api(1, $cart, 1, NULL);
    } catch (Exception $e) {
        echo format_api(0, $e->getMessage(), 1, NULL);
    }
});

$app->post('/delivery-time', function() use ($app) {
    $data = json_decode($app->request()->getBody(), true);
    try {
        $result =  WY_OrderOnline::setDeliveryTime($data);
        if($result){
            echo format_api(1, "update time successfully", 1, NULL);
        }
    } catch (Exception $e) {
        echo format_api(0, $e->getMessage(), 1, NULL);
    }
});

$app->post('', function() use ($app) {
    try {
        $data = json_decode($app->request()->getBody(), true);
        $order = new WY_OrderOnline();
        $data = $order->createOrderPhil($data);
        // WY_Paypal
        //unset($_SESSION['cart']);
        echo format_api(1, $data, 1, NULL);
    } catch (Exception $e) {
        echo format_api(0, $e->getMessage(), 1, NULL);
    }
});



$app->delete('', function() {
    WY_OrderOnline::deleteCart();
    echo format_api(1, 'remove all items in cart successfully', 1, NULL);
});



?>