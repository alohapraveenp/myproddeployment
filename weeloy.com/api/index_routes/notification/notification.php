<?
//notiifcation actions

$app->post('/create', function () use ($app) {
     $data = json_decode($app->request()->getBody(),true);
    return notifycreate($data);
});
$app->post('/update', function () use ($app) {
     $data = json_decode($app->request()->getBody(),true);
    return notifycreate($data);
});
$app->get('/getType',function() use ($app){
    return getNotifyType();
});
$app->post('/updateactions', function () use ($app) {
     $data = json_decode($app->request()->getBody(),true);
    return notifyupdateaction($data);
});

 $app->get('/getnotiifcation/:query','getnotifyaction');
 
 $app->get('/smstracking/:restaurant/:query/:page', 'getSmsTracking' );


function notifycreate($data){
	 
   $notify = new WY_Notification;
   $result= $notify->createNotifyConfiAction($data);

   $errors = null;
   $data = array("notify" => $result);
	 echo format_api(1, $data, count($result), $errors);
}
function getNotifyType(){
  
   $notify = new WY_Notification;
   $result= $notify->getNotificationAdminType();
   $errors = null;
   $data = array("notify" => $result);
	 echo format_api(1, $data, count($result), $errors);
}

function notifyupdateaction($data){
	$notify = new WY_Notification;
	$result= $notify->updateAction($data['data']);
	$errors = null;
	 echo format_api(1, 1, $result, $errors);
  
}
function getnotifyaction($restaurant){
  $notify = new WY_Notification;
  $result= $notify->getNotificationConfigAction($restaurant);
  $errors = null;
  $data = array("notify" => $result);
  echo format_api(1, $data, count($result), $errors);

}

function getSmsTracking($restaurant,$query,$page){

        $sms = new JM_Sms();
        $result = $sms->getSmsTracking($query,$page,$restaurant);
        $data = array("history" => $result);
        $errors=null;
        echo format_api(1, $data, count($result), $errors);
}

/************end***********************/


?>