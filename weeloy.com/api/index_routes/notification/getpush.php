<?

function pushNotify($data){
   $message = $data['message']; 
   $type =$data['type']; 
   $deviceId="";
   $title = $data['title']; 
	$apn = new JM_Pushnotif();
	if(isset($data['deviceId'])){$deviceId=$data['deviceId'];}
	$result = $apn->notifyConsumer($type,$message,$data['env'],$deviceId,$title);
	$errors = null;
	echo format_api(1, $result, count($result), $errors);

}

function getNotificationall(){
	 $apn = new JM_Pushnotif();
	 $notify = $apn->getNotificationAll();
	 $errors = null;
	 $data = array("notify" => $notify);
	 echo format_api(1, $data, count($notify), $errors);
   
}
   
// SEND GENERIC SMS is sms sent from the TMS
function sendgenericsms($restaurant, $booking, $mobile, $sms_message, $email, $token) {
    try {
            $sms = new JM_Sms();
		$data = null;
		$status = $zdata = 0;
		$msg = "";
		$login_type = $platform = 'translation';
		$mode = 'api-ajax';
		
		$log = new WY_Login(set_valid_login_type($login_type));
		$log->check_login_remote($email, $token, $mode);
		$msg = $log->msg;
		if($log->result > 0) {
			$mobj = new WY_Booking();
			$mobj->getBooking($booking);
			$status = $mobj->result;
			$msg = $mobj->msg;
			if($status < 0) {
				echo format_api($status, $data="", $zdata=1, $msg);
				return;
				}
				
			if($mobj->mobile != $mobile) {
				echo format_api(-1, $data="", $zdata=1, "invalid mobile number");
				return;
				}
  
  			if(isset($mobj->restaurantinfo->title)) 
  				 $sms_message = $mobj->restaurantinfo->title . "\n" . $sms_message;
			// notify => send sms
			$recipient_mobile = $mobile;
			$smsid = '';
                        
                        // get notification restaurant configuration
                        
                        $notification = new WY_Notification();
                        

                        $notification_type = $mobj->booking_type ; 
                        $template = $notification_type . '_member_white_label';
                        
                        $config = $notification->getConfiguration($restaurant, $template);


                        $provider = (isset($notification->pname)) ? $notification->pname : 'SMTP';

                        if(!empty($mobj->restaurantinfo->smsid)){
                            $smsid = $mobj->restaurantinfo->smsid;
                        }

			$sms->sendSmsMessage($recipient_mobile, $sms_message, $smsid, $restaurant, $mobj->confirmation,$provider);

			$mobj->incsmsBooking($booking);
			}
		echo format_api($status, $data="", $zdata=1, $msg);
    } catch (Exception $e) {
        api_error($e, "sendgenericsms");
    }
}


?>