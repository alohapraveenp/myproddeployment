<?php // You need to add server side validation and better error handling here


function error_msg($msg) {
	header('Content-type: application/json');
	$data = array('error' => $msg);
	echo json_encode($data);
	return;
}

	if(!isset($_GET['files'])) {
	    header('Content-type: application/json');
		$data = array('success' => 'Form was submitted', 'formData' => $_POST);
		echo json_encode($data);
		return;
	}

	require_once("conf/conf.init.inc.php");
	require_once("lib/wpdo.inc.php");
	require_once("lib/class.login.inc.php");
	require_once("lib/class.media.inc.php");
	require_once("lib/class.images.inc.php");

	$token = $_REQUEST['token'];
	$theRestaurant = $_REQUEST['restaurant'];
	$category = $_REQUEST['category'];
	if (empty($token))
		return error_msg("Missing token!");
	else if (empty($theRestaurant))
		return error_msg("Missing restaurant!");
	else if (empty($category))
		return error_msg("Missing category!");
	error_log("theRestaurant: ".$theRestaurant);
	if(!in_array($category, $picture_typelist))
		$category = $picture_typelist[0];

	$uploaddir = './uploads/';
    //error_log(__FILE__." ".__FUNCTION__." ".__LINE__.": _FILES: ".print_r($_FILES, true));
	foreach($_FILES as $file) {
		$_FILES['file']['name'] = $files[] = $file['name'];
		$_FILES['file']['type'] = $file['type'];
		$_FILES['file']['size'] = $file['size'];
		$_FILES['file']['tmp_name'] = $file['tmp_name'];
		$_FILES['file']['error'] = $file['error'];
		break;
	}
	
	$login = new WY_Login(LOGIN_BACKOFFICE);
	if($login->checktoken($token) == false)
		return error_msg("wrong token");
//	
	$_POST["Stage"] = "Load";
	$mediadata = new WY_Media($theRestaurant);
	list($log, $msg) = $mediadata->uploadImage($theRestaurant, $category,'category');

	if($log > 0) {
	    header('Content-type: application/json');
		echo json_encode(array('files' => $files));
	} else 
		error_msg('There was an error uploading your file ' . $msg);
	//error_log("SAVEIMAGE $log " . $theRestaurant . ' ' . $category . ' ' . $file['name'] );
	foreach($_FILES as $file) {
		unlink($file['tmp_name']);
		break;
	}
?>