
<div class="container">
    <div class="row">
        <div class="col-xl-12 left-sec">
<div ng-controller="CrawlerController" ng-init="moduleName='Crawler'; listCrawlerFlag = true; viewCrawlerFlag=false; createCrawlerFlag=false;" >
	<div id='listing' ng-show='listCrawlerFlag'>
		<div class="form-group"  style='margin-bottom:25px;'>
			<div class="col-md-4">
				<div class="input-group col-md-4">
					<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>
					<input type="text" class="form-control input-sm" ng-model="searchText" ng-change="paginator.setPage(0);" style='width:200px;' autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" > 
				</div>
			</div>
			<div class="col-md-2"></div>
			<div class="col-md-4">
			<a href ng-click='create();' class="btn btn-success btn-sm" style='color:white;width:200px;'><span class='glyphicon glyphicon-certificate'></span> &nbsp;Create a new Crawl</a>
			</div>
		</div>
		<span style='font-size:12px;'><i>* make sure that the email has been "IMAP enabled" (mailbox config), and set "Allow less secure apps: ON" in the sign-in & security (my account)<br /> Full name is the key word found in the 'parsed' restaurant name (Tiger is in Tiger’s Milk)</i><br /><br /></span>
        <div style=" clear: both;"></div>
		<table width='100%' class="table table-condensed table-striped" style='font-size:12px;'>
			<thead>
				<tr>
				<th ng-repeat="y in tabletitle"><tbtitle var="{{y.a}}" name='tabletitle' module='moduleName'/></th>
				<th>update</th><th> &nbsp; </th>
				<th>delete</th><th> &nbsp; </th>
                             
				</tr>
			 </thead>
			<tbody style='font-family:helvetica;font-size:12px;'>
				<tr ng-repeat="x in filteredCrawler = (names | filter:searchText) | sizefilter:this | orderBy:predicate:reverse | slicefilter:paginator.getPage():paginator.getRowperPage():paginator.getItemCount() | limitTo:paginator.getRowperPage()" style='font-family:helvetica;font-size:11px;'>
				<td ng-repeat="y in tabletitle">
					<span ng-if="y.a !=='status' && y.a !=='action'"><a href ng-click="view(x)">{{ x[y.a] | adatereverse:y.c }}</a></span>
					<span ng-if="y.a==='status' && x.status === 'active'"> {{ x[y.a]}} </span>
					<span ng-if="y.a==='status' && x.status !== 'active'"> </span>
					<span ng-if="y.a==='action'"><a href ng-click="toggleactivation(x)"><span ng-if="x[y.a] === true"  style='color:green;' class='glyphicon glyphicon-flag'></span><span ng-if="x[y.a] === false"  style='color:red;' class='glyphicon glyphicon-ban-circle'></span></a>
					</td>
					<td><a href ng-click="update(x)" style='color:blue;'><span class='glyphicon glyphicon-pencil'></span></a></td>
                    <td width='30'>&nbsp;</td>
					<td nowrap><a href ng-click="delete(x)" style='color:red;'><span class='glyphicon glyphicon-trash'></span></a></td>
					
				</tr><tr><td colspan='{{tabletitle.length + 3}}'></td></tr>
			</tbody>
		</table>
		<div ng-if="filteredCrawler.length >= paginator.getRowperPage()" class="col-md-12" align="center" style='font-size:11px' ng-include="'app/components/paginator.html'"></div>
	</div>

	<div class="col-md-12" ng-show='viewCrawlerFlag'>
		<br /><a href class='btn btn-info btn-sm customColor' ng-click="backlisting()"><span class='glyphicon glyphicon-step-backward'></span> &nbsp;Back to listing</a><br />
		<br />
		<table class='table-striped' style="margin: 0 0 150px 30px;font-size:13px;font-family: Roboto">		
			<tr ng-repeat="y in tabletitleContent | filter: {b: '!picture'} track by $index"><td nowrap ><strong> {{ y.b }} </strong></td><td> &nbsp; </td><td>{{ selectedItem[y.a] | adatereverse:y.c }} </td></tr>
		</table>
	</div>

	<div class="col-md-12" ng-show='createCrawlerFlag'>
		<br />
		<a href class='btn btn-info btn-sm customColor' ng-click="backlisting()"><span class='glyphicon glyphicon-step-backward'></span> &nbsp;Back to listing</a>
		<br />
		 <div class="col-md-2"></div>
		 <div class="col-md-8">
		   <div class="row" ng-repeat="y in tabletitleContent | filter: {t: '!dontshow' }"  style="margin: 0 0 20px 0;font-size:12px;font-familly:Roboto">

			<div class="input-group"  ng-if="y.t === 'input'">
					<span class="input-group-addon input11"><i class="glyphicon glyphicon-{{y.d}} input14"></i>&nbsp;{{y.b}}</span>
					<input type="text" class="form-control input-sm" ng-model="selectedItem[y.a]" ng-change="cleaninput(y.a)"  ng-readonly="action==='update' && (y.a==='name' || y.a === 'provider') ">
			</div>
				<div class="input-group" ng-if="y.t === 'array'"> <!--  && (y.a !=='provider' || action !== 'update') -->
						<div class='input-group-btn' uib-dropdown  >
						<button type='button' class='btn btn-default btn-sm' uib-dropdown-toggle >
						<i class="glyphicon glyphicon-{{y.d}}"></i>&nbsp; {{y.b}}<span class='caret'></span></button>
						<ul class='dropdown-menu scrollable-menu' uib-dropdown-menu style='height: auto;max-height:200px; overflow-x: hidden;' >
						<li ng-repeat="p in y.val"><a href ng-click="selectedItem[y.a]=p;y.func()">{{ p }}</a></li>
						</ul>
						</div>
						<input type='text' ng-model='selectedItem[y.a]' class='form-control input-sm' readonly >
				</div>
                              
		   </div><br />
		</div>
		<div class="col-md-2"></div>
		<div class="col-md-7"></div>
		<div class="col-md-5">
			<a href ng-click='savenewcrawler(action);' class="btn btn-success btn-sm" style="color:white;width:250px"><i class='glyphicon glyphicon-save'></i> &nbsp;{{ buttonlabel }} </a><br />
		</div>
		</div>
</div>

</div>
</div>
</div>

<script>

var token = <?php echo "'".$_SESSION['user_backoffice']['token']."';"; ?>
var email =  <?php echo "'".$_SESSION['user_backoffice']['email']."';";?>

app.controller('CrawlerController', ['$scope', '$http', '$timeout', 'adminServiceApi', function($scope, $http, $timeout, adminServiceApi) {
	var todaydate = new Date();
	var aorder = (function() { var i, arr=[]; for(i = 0; i < 30; i++) arr.push(i); return arr; })();
	
	$scope.paginator = new Pagination(25);
	$scope.email = email;
	$scope.predicate = '';
	$scope.reverse = false;
	$scope.nonefunc = function() {};
 	$scope.listprovider = [ "CHOPE", "HGW", "QUANDO"];
 	$scope.liststatus = ["active", "inactive"];
 	
 	$scope.tabletitle = [ {a:'name', b:'Name', c:'' , q:'down', cc: 'black' }, {a:'restaurant', b:'Restaurant', c:'' , q:'down', cc: 'black' }, {a:'provider', b:'Provider', c:'' , q:'down', cc: 'black' }, {a:'action', b:'Active/Stop', c:'' , q:'down', cc: 'black' }, {a:'status', b:'Status', c:'' , q:'down', cc: 'black' } ];	
	$scope.tabletitleContent = [{a:'name', b:'Name', c:'' , q:'down', cc: 'black', t:'input' }, {a:'restaurant', b:'Restaurant', c:'' , q:'down', cc: 'black', t:'input' }, {a:'restaurant_name', b:'Restaurant Full Name', c:'' , q:'down', cc: 'black', t:'input' }, {a:'hostname', b:'Host Name', c:'' , q:'down', cc: 'black', t:'input' }, {a:'email_sender', b:'Email Sender', c:'' , q:'down', cc: 'black', t:'input' }, {a:'mailbox', b:'Mailbox', c:'' , q:'down', cc: 'black', t:'input' }, {a:'credentials', b:'Credentials', c:'' , q:'down', cc: 'black', t:'input' }, {a:'provider', b:'Provider', c:'' , q:'down', cc: 'black', t:'array', val: $scope.listprovider }, {a:'require_payment', b:'Require Payment', c:'' , q:'down', cc: 'black', t:'input' }, {a:'astatus', b:'Status', c:'' , q:'down', cc: 'black', t:'array', val: $scope.liststatus }];
        
	$scope.ObjectCrawler = function() {
		var oo, obj = {};
	
		if($scope.names.length < 0)
			return obj;
		
		oo = $scope.names[0];
		Object.keys(oo).map(function(ll) { obj[ll] = oo[ll]; });
		obj['name'] = obj['restaurant_name'] = "";
		obj['credentials'] = "mailboxNEW@gmail.com:Mbpfm007";
		
		return obj;
		};
       		
	$scope.initorder = function() {
		$scope.tabletitle = $scope.bckups;
		$scope.predicate = "vorder";
		$scope.reverse = true;
	};

	$scope.reorder = function(item, alter) {
		alter = alter || "";
		if (alter !== "")  item = alter;
		$scope.reverse = ($scope.predicate === item) ? !$scope.reverse : false;
		$scope.predicate = item;
	};
    
	adminServiceApi.readcrawler($scope.email, token).then(function(response) {              
		var data = response.data;
		$scope.selectedItem= null;
		$scope.names = [];	
		console.log('RESPONSE', $scope.email, token, data);
		if(response.status !== 1)
			return;

		data.map(function(oo, index, arr) {
			if(oo.status === 'active') { oo.action = false; }
			else { oo.action = true; }
			$scope.names.push(oo);                         
			});   
		console.log('DATA',   $scope.names);       
        });
 
	$scope.reset = function(item) {
		$scope.listCrawlerFlag = false;
		$scope.viewCrawlerFlag = false
		$scope.createCrawlerFlag = false;
		$scope[item] = true;	
		};

	$scope.backlisting = function() {
		$scope.reset('listCrawlerFlag');
		};
		
	$scope.removeCrawlerData = function(oo) {
		for(i = 0; i < data.length; i++)
        	if(data[i].name === oo.name)
                return data.splice(i, 1);
		};
   		
	$scope.view = function(oo) {
		oo.astatus = (oo.status === 'active') ? 'active' : 'inactive';
		$scope.selectedItem = oo;
		$scope.reset('viewCrawlerFlag');
		};

	$scope.fdigest = function() {
		$timeout( function() { /* forcing apply */ }, 500);
		};

	$scope.create = function() {
		$scope.selectedItem = new $scope.ObjectCrawler();
		$scope.reset('createCrawlerFlag');
		$scope.buttonlabel = "Save new crawled mailbox";
		$scope.action = "create";
		return false;
		}

	$scope.update = function(oo) {
		oo.astatus = (oo.status === 'active') ? 'active' : 'inactive';
		$scope.selectedItem = oo;
		$scope.reset('createCrawlerFlag');
		$scope.buttonlabel = "Update categories";
		$scope.action = "update";
		};

	$scope.toggleactivation = function(oo) {
		var action;
		if(oo.status === 'active') 
			{ oo.status = 'nactive'; oo.action = true; action = 'desactivated';}
		else { oo.status = 'active'; oo.action = false; action = 'activated';}		
		adminServiceApi.toggleactivationcrawler(oo, $scope.email, token).then(function(response) {
			if(response.status === 1)
				alert("crawler  has been " + action);
			else alert("crawler has NOT been " + action + " :" + reponse.errors);
			});
		}
			
	$scope.savenewcrawler = function(action) {
		var u, msg, oo = $scope.selectedItem, valAr = [], mandatory = ['name', 'restaurant', 'credentials'], err = 0;
      
		if(action === "create") {
			mandatory.map(function(ll) { if(!oo[ll] || oo[ll] === "") err++; });
			if(err > 0)
            	return alert( " Please fill mandatory fields (" + mandatory.join(", ") + ")");
 
 			adminServiceApi.createcrawler(oo, $scope.email, token).then(function(response) {
				if(response.status === 1) {
					$scope.names.push($scope.selectedItem);
					alert("crawled mailbox has been created");
					$scope.fdigest();
					}
				else alert("crawled mailbox has NOT been created: " + response.errors);
				});
				//window.location.reload();
                    }
		else if(action == "update") {
			oo.status = (oo.astatus === 'active') ? 'active' : 'inactive';
			oo.action = (oo.astatus !== 'active');
			adminServiceApi.updatecrawler(oo, $scope.email, token).then(function(response) {
				if(response.status === 1) alert("crawler  has been updated");
				else alert("crawler has NOT been updated: " + reponse.errors);
				});
			}
		$scope.backlisting();	
		};
		
	$scope.delete = function(oo) {	
		return alert("'remove' function not activated");
		adminServiceApi.deletecrawler(oo, $scope.email, token).then(function(response) {
			if(response.status === 1) {
				$scope.removecrawlerData(oo);
				alert(email + " crawler  has been deleted");
				}
			else alert(email + " crawler has NOT been deleted: " + reponse.errors);
			});

		$scope.backlisting();	
			
		};

	function validateEmail(email) {
		if(typeof email !== "string" || email.length < 4)
			return false;
			
    		var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    		return re.test(email);
		}			
}]);

	
</script>

