
<?php
$res = new WY_restaurant;
 $res->getRestaurant($theRestaurant);

$multProduct = ($res->mutipleProdAllote()) ? $res->dfproduct : "";

?>

<div class="container">
    <div class="row">
        <div class="col-md-12 left-sec">
            <div  >
                <div class="col-md-12" ng-controller="sectionConfigController" >

                    <form   name="MyForm">
                        <input type='hidden' id='restaurant' value ="<?php echo $theRestaurant ?>" />
                        <input type='hidden' id='email' value ="<?php echo $_SESSION['user_backoffice']['email'] ?>" />
                         <input type='hidden' id='multiproduct' value ="<?php echo $multProduct ?>" />
                        <div class="row"  >
                            <div class="panel-body">
                               
                                    <div class="product_Section" >
                                        <div class='form-group row' ng-if="multProd.length > 1" >
                                            <div class="col-sm-2">
                                            </div>
                                            <div class="col-sm-5" >
                                                <button type="button" class="btn  btn-success btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false" style='color:white;'><span class='glyphicon glyphicon-list'></span>&nbsp;&nbsp; Select Product &nbsp;&nbsp; <span class="caret"></span></button><span style="font-size:11px;font-weight:bold;">&nbsp;  [ {{bkproduct}} ]</span>
                                                <ul class="dropdown-menu" role="menu">
                                                <li ng-repeat="x in multProd"><a href  ng-click="setproduct(x);">{{x}}  <i ng-show='bkproduct === x' class="glyphicon glyphicon-ok text-primary"></i></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                        </br> </br>
                                       
                                  
                                        <div class='form-group row' >
                                           <div class="col-md-2">  <label >Description </label> </div>
                                           <div class="col-md-6"> <input type="text" class="form-control input-sm" ng-model="description"   ></div>
                                        </div>
                                        <div class='form-group row' >
                                              <div class="col-md-2"><label >Minpax </label></div>
                                              <div class="col-md-6"><input type="text" class="form-control input-sm" ng-model="min_pax"   ></div>
                                         
                                        </div>
                                        <div class='form-group row' >
                                            <div class="col-md-2"> <label >Maxpax </label></div>
                                            <div class="col-md-6"><input type="text" class="form-control input-sm" ng-model="max_pax"   ></div>
                                     
                                        </div>
                                        <div class='form-group row' >
                                            <div class="col-md-2"> <label >Open Time Lunch </label></div>
                                            <div class="col-md-6"><input type="text" class="form-control input-sm" ng-model="lunch"   ></div>
                                        </div>
                                        <div class='form-group row' >
                                            <div class="col-md-2"> <label >Open Time Dinner </label></div>
                                            <div class="col-md-6"><input type="text" class="form-control input-sm" ng-model="dinner"   ></div>
                                        </div>
                                        <div class='form-group row' >
                                            <div class="col-md-2"> <label >additional info </label></div>
                                            <div class="col-md-6"><input type="text" class="form-control input-sm" ng-model="additional_info"   ></div>
                                        </div>
                                        <div class='form-group row' style='margin-left:20px!important'>
                                            <input type="checkbox" ng-model="require_config"  ng-true-value="'1'" ng-false-value="'0'" ng-click="showmessage();"  />&nbsp;required confirmation
                                        </div>
                                        <div class='form-group row' ng-if="require_config==='1' " >
                                            <div class="col-md-2"> <label >Required Message </label></div>
                                            <div class="col-md-6"><input type="text" class="form-control input-sm" ng-model="require_text"   ></div>
                                        </div>
                                        
                                        <!--
                                        <div class='form-group row'>
                                            <div class='col-sm-1'><label for='range1'>below  </label>    </div>
                                            <div class='col-sm-2'><input type='text' ng-model="range1" class='form-control input-sm' id='range1' name='$reference' placeholder='range1'  > </div>
                                            <div class='col-sm-1'> <span class=''><label for='range1'> hours =>  </label></span></div>
                                            <div class='col-sm-2'> <input  type='text' ng-model="policy1" class='form-control input-sm' id='range1' name='$reference' placeholder='' > </div>
                                            <div class='col-sm-1'> % </div>
                                        </div>
                                        <div   class='form-group row'>
                                            <div class='col-sm-1'> <label for='range1'>above </label></div>
                                              <div  class='col-sm-3'><input type='text' ng-model="range1"  class='form-control input-sm' id='range1' name='$reference' placeholder='' readonly  > </div>
                                            <div class='col-sm-1'> <label for='range1'> hours =>  </label> </div>
                                            <div class='col-sm-3'><input type='text' ng-model="policy3" class='form-control input-sm' id='range1' name='$reference' placeholder=''  > </div>
                                            <div class='col-sm-1'> <label for='range1'>% </label></div>

                                        </div>-->
                                      
                                         

                                        </div>

                                    </div>
<!--
                                <div class='form-group row' style='margin-left:10px!important'>
                                    <input type="checkbox" ng-model="is_range2"  ng-true-value="'1'" ng-false-value="'0'" ng-click="showrange();"  />&nbsp;use section
                                </div>-->
                                <div class="col-md-2">

                                    <a href ng-click='saveSection();' class="btn btn-primary btn-sm" style='color:white;'> Save changes </a>
                                </div>

                            </div>
                            <div class="col-md-2"></div>
                    </form>
                </div>

            </div>

        </div>
    </div>
</div>

<script type="text/javascript" src="app/components/restaurant/section_config/sectionConfigController.js?22"></script>