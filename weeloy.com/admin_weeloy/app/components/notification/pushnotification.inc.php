<div class="container">
    <div class="row">
        <div class="col-md-12 left-sec">
            <div ng-controller="NotifyController" ng-init="moduleName = 'notify'; listEventFlag = true; viewEventFlag = false; createEventFlag = false;" >

                <div id='listing' ng-show='listEventFlag'>
                    <div class="form-group"  style='margin-bottom:25px;'>
                        <div class="col-md-4">
                            <div class="input-group col-md-4">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>
                                <input type="text" class="form-control input-sm" ng-model="searchText" ng-change="paginator.setPage(0);" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" style='width:200px;'> 
                            </div>
                        </div>
                         <div class="col-md-2" style='font-size:12px;'>
                            selected Items: <strong> {{names.length}} </strong>
                        </div>

                        <div class="col-md-2">
                                <div class="btn-group">
                                        <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown" aria-expanded="false" style='font-size:11px;'>Page Size <span class="caret"></span></button>
                                        <ul class="dropdown-menu" role="menu">
                                            <li ng-repeat="x in paginator.pagerange()"><a href ng-click="paginator.setRowperPage(x)">{{x}}</a></li>
                                        </ul>
                                </div> 		
                        </div>
                        <div class="col-md-2">
                            <a href ng-click='create();' class="btn btn-success btn-sm" style='color:white;width:200px;'><span class='glyphicon glyphicon-certificate'></span> &nbsp;Create a New Notification</a>
                        </div>
                    </div>
                    <div style=" clear: both;"></div>
                    <table width='100%' class="table table-condensed table-striped" style='font-size:12px;'>
                        <thead>
                            <tr>
                                <th ng-repeat="y in tabletitle"><tbtitle var="{{y.a}}" name='tabletitle' module='moduleName'/></th>                        </tr>
                        </thead>
                        <tbody style='font-family:helvetica;font-size:12px;'>
                            <tr ng-repeat="x in filteredEvent = (names| filter:searchText) | sizefilter:this | orderBy:predicate:reverse | slicefilter:paginator.getPage():paginator.getRowperPage():paginator.getItemCount() | limitTo:paginator.getRowperPage()" style='font-family:helvetica;font-size:11px;'>
                               <td ng-repeat="y in tabletitle">
                                  {{ x[y.a] | adatereverse:y.c }}
                                </td>
                           </tr><tr><td colspan='{{tabletitle.length + 3}}'></td></tr>
                        </tbody>
                    </table>
                    <div ng-if="filteredEvent.length >= paginator.getRowperPage()" class="col-md-12" align="center" style='font-size:11px' ng-include="'app/components/paginator.html'"></div>
                </div>

                <div class="col-md-12" ng-show='viewEventFlag'>
                    <br /><a href class='btn btn-info btn-sm customColor' ng-click="backlisting()"><span class='glyphicon glyphicon-step-backward'></span> &nbsp;Back to listing</a><br />
                    <table class='table-striped' style="margin: 0 0 150px 30px;font-size:13px;font-family: Roboto">		
                        <tr ng-repeat="y in tabletitleContent| filter: {b: '!picture'}"><td nowrap ><strong> {{ y.b}} </strong></td><td> &nbsp; </td><td>{{ selectedItem[y.a] | adatereverse:y.c }} </td></tr>

                    </table>
                </div>
                <div class="col-md-12" ng-show='createEventFlag'>
                    <br />
                    <a href class='btn btn-info btn-sm customColor' ng-click="backlisting()"><span class='glyphicon glyphicon-step-backward'></span> &nbsp;Back to listing</a>
                    <br /> <br />
                                     
                    <div class="col-md-8" style="margin-bottom:20px;">
                        <div class="row" ng-repeat="y in tabletitleContent| filter: {t: '!dontshow' }"  style="margin: 0 0 20px 0;font-size:12px;font-familly:Roboto">
                         
                            <div class="input-group"  ng-if="y.t === 'input'">
                                <span class="input-group-addon input11"><i class="glyphicon glyphicon-{{y.d}} input14"></i>&nbsp;{{y.b}}</span>
                                <input type="text" class="form-control input-sm" ng-model="selectedItem[y.a]" ng-change="cleaninput(y.a)" maxlength="120" >
                            </div>
                
                               <div class="input-group"  ng-if="(y.t === 'inputtitle' && (selectedItem.type==='android_weeloy' || selectedItem.type==='android_weeloypro')) ">
                                <span class="input-group-addon input11"><i class="glyphicon glyphicon-{{y.d}} input14"></i>&nbsp;{{y.b}}</span>
                                <input type="text" class="form-control input-sm" ng-model="selectedItem[y.a]" ng-change="cleaninput(y.a)" maxlength="120" >
                            </div>
                            <div class="input-group" ng-if="y.t === 'inputtype'">
                                <div class='input-group-btn' dropdown >
                                    <button type='button' class='btn btn-default dropdown-toggle btn-sm input11' data-toggle='dropdown'>
                                        <i class="glyphicon glyphicon-{{y.d}} input13"></i>&nbsp; {{y.b}}<span class='caret'></span></button>
                                    <ul class='dropdown-menu scrollable-menu'style='height: auto;max-height:120px; overflow-x: hidden;' >
                                        <li ng-repeat="p in deviceList"><a href ng-click="deviceIndex(p.a);">{{ p.a}}</a></li>
                                    </ul>
                                </div>
                                <input type='text' ng-model='selectedItem[y.a]' class='form-control input-sm' readonly >
                            </div>
                        </div>
                        <div class="live-btn" style="float:right;" >
                            <a href ng-click='sendnotification();' class="btn btn-success btn-sm" style="color:white;width:150px"><i class='glyphicon glyphicon-save'></i> &nbsp;{{ buttonlabel}} </a>
                        </div>
                    </div>

                    <div class="col-md-8" style="padding-left:29px;">
                        <div class="row" style="margin: 0 0 20px 10;font-size:12px;font-familly:Roboto">
                            <h5>Please Select testing device Id </h5>
                        <div class="input-group" >
                            <div class='input-group-btn' dropdown >
                                <button type='button' class='btn btn-default dropdown-toggle btn-sm input11' data-toggle='dropdown'>
                                    <i class="glyphicon glyphicon-envelope input13"></i>&nbsp;DeviceId<span class='caret'></span></button>
                                <ul class='dropdown-menu scrollable-menu'style='height: auto;max-height:120px; overflow-x: hidden;' >
                                    <li ng-repeat="p in deviceIds"><a href ng-click="deviceIdList(p);">{{ p.a}}</a></li>
                                </ul>
                            </div>
                            <input type='text' ng-model='selectedItem["devicename"]' class='form-control input-sm' readonly >
                        </div>
                        </div>
                    </div>
                    <div class="col-md-2"></div>
                    <div class="col-md-7"></div>
                    <div class="col-md-5" style="margin-top:10px;">
                      
                        <a href ng-click='testnotify();' class="btn btn-info btn-sm" style="color:white;"><i class='glyphicon glyphicon-save'></i> &nbsp;TEST </a><br />
                    </div>
                </div>
               
            </div>

        </div>
    </div>
</div>

<script>

<?php
$mediadata = new WY_Media();
$resdata = new WY_restaurant();
$imgAr = $mediadata->getEventPictureNames($theRestaurant);
$resAr = $resdata->getSimpleListRestaurant();

printf("var imgEvent = [");
for ($i = 0, $sep = ""; $i < count($imgAr); $i++, $sep = ", ") {
    printf("%s '%s'", $sep, $imgAr[$i]);
}
printf("];");

printf("var pathimg = '%s';", $mediadata->getFullPath('small'));


printf("var resArr = [");
for ($i = 0, $sep = ""; $i < count($resAr); $i++, $sep = ", ") {
    printf("%s '%s'", $sep, $resAr[$i]);
}
printf("];");
?>

    var token = <?php echo "'" . $_SESSION['user_backoffice']['token'] . "';"; ?>
    var email = <?php echo "'" . $_SESSION['user_backoffice']['email'] . "';"; ?>

    app.controller('NotifyController', ['$scope', '$http', '$timeout', 'bookService', function($scope, $http, $timeout, bookService) {

    var todaydate = new Date();
            var aorder = (function() { var i, arr = []; for (i = 0; i < 30; i++) arr.push(i); return arr; })();
            $scope.paginator = new Pagination(25);
            $scope.path = pathimg;
            $scope.restaurant = <?php echo "'" . $theRestaurant . "';"; ?>
            $scope.email = <?php echo "'" . $email . "';"; ?>
            $scope.predicate = '';
            $scope.reverse = false;
            $scope.nonefunc = function() {};
            $scope.resArr = resArr;
            
            $scope.tabletitle = [ {a:'index', b:'ID', c:'', q:'down', cc: 'black' }, {a:'app_name', b:'Appname', c:'', q:'down', cc: 'black' }, {a:'device', b:'Device', c:'', q:'down', cc: 'black' }, {a:'date', b:'Date', c:'', q:'down', cc: 'black' }, { a:'errorcount', b:'Error', c:'', q:'down', cc: 'black' }, , {a:'totalcount', b:'Total', c:'', q:'down', cc: 'black' } ];
            $scope.bckups = $scope.tabletitle.slice(0);
            $scope.tabletitleContent = [ { a:'type', b:'Type', c:'', d:'flag', t:'inputtype', i:0 }, { a:'title', b:'Title', c:'', d:'flag', t:'inputtitle', i:0 } ,{ a:'message', b:'Message', c:'', d:'envelope', t:'input', i:0 }];
            $scope.deviceList = [{a:'android_weeloy', b:'Android Weeloy', c:'', q:'down', cc: 'black'}, {a:'ios_weeloy', b:'Ios Weeloy', c:'', q:'down', cc: 'black'}, {a:'android_weeloypro', b:'Android Weeloypro', c:'', q:'down', cc: 'black'}, {a:'ios_weeloypro', b:'Ios_Weeloypro', c:'', q:'down', cc: 'black'}];
            $scope.deviceIds = [{a:'ios_kala_weeloypro', b:'Ios Kala weeloypro', c:'f03e7f9848d21b194336fe03d5501b07c9bd23cb4739f4ccc84d8d788cb4d73e', q:'down', cc: 'black'}, {a:'ios_kala_weeloy', b:'Ios Kala weeloy', c:'9bd32fbd05842191113e4e4eb182fdb0f483478cd46c11ccdd2107d916969fa2', q:'down', cc: 'black'}, {a:'ios_phil_weeloy', b:'Ios Phil weeloy', c:'d325434b945552c15bbdffdb00040d017424a35912b648bd543a3ead8630fe62', q:'down', cc: 'black'}, {a:'android_edwin_weeloy', b:'Android Edwin weeloy', c:'fR5hjGVfT3U:APA91bHP59EOhSlIIoVibv25K3RPLu48_GwWtSoIdGRL1Q5k4zJttuIcGl4WkiM3S0UK0ayVEqIywsh1rocJw6yYnyF4zZou9C4r5KMVyoVxn7gxnjQRodROhyCii_xJt3-05NC3w0qF', q:'down', cc: 'black'}, {a:'android_edwin_weeloypro', b:'Android Edwin WeeloyPro', c:'eE2rFuIiM9s:APA91bFz_2M7yN7v0W3Zd4KCsNH95urR3_iH4LYbsEAzHgJkbiFQ4kB7f4kQukaaY90sWpAsEd420oHkJa8J4AOvZVUUdXM9obmVCXGR0HqQMqG-uGcpA6B4TlR7d7CvMwo0KxTO_aYQ', q:'down', cc: 'black'}, {a:'android_matthew_weeloy', b:'Android Matthew Weeloy', c:'ebpmuiegogU:APA91bFRzHkDNeFu6_G2dnd6tuGEXqWCtvxBHi02Q3eNCT7uaUMg7l_S3yT7IyGZczYXGK16EK9bdVinH1goi10e-OLslUInejCdqi3DyhtKYofdoUgD5rAjRpYtWxXufpd6eDRDWUjH', q:'down', cc: 'black'},{a:'android_matthew_weeloypro', b:'Android Matthew Weeloypro', c:'APA91bEePBXK21WUwMo-tTPelPJHuARfQqUYHEqxbpfemboZ8jslNuu9dTKIfcZINujVJsUohKuSyM_JQZM5g5Ewbhuh0pbUE5eNSamNaiAaBQbLfjSmrCGZzyGobPqWvqf7CCiV2I-r', q:'down', cc: 'black'}];
            var mailArr = email.split('@');
            $scope.is_email = mailArr[1];
            //$scope.is_email='gmail.com';
            $scope.path = "https://media.weeloy.com/upload/restaurant/";
            $scope.Objevent = function() {
            return {
      
                    index: 0,
                    app_name: '',
                    device: '',
                    date: '',
                    errorcount:'',
                    totalcount:'',
                    title:'',
                    message:'',
                    
//			description: '', 
//			restaurant1: '', 
//			image1: '', 
//			restaurant2: '', 
//			image2: '',
//                        restaurant3: '',
//                        image3:'',
//                        morder:'',
//                        city:'',
//                        is_tag:'',

                    remove: function() {
                    for (var i = 0; i < $scope.names.length; i++)
                            if ($scope.names[i].name === this.name) {
                    $scope.names.splice(i, 1);
                            break;
                    }
                    },
                    replicate: function(obj) {
                    for (var attr in this)
                            if (this.hasOwnProperty(attr) && typeof this[attr] !== 'function' && attr !== '$$hashKey')
                            this[attr] = obj[attr];
                            return this;
                    },
                    clean: function() {


                    for (var attr in this)
                            if (this.hasOwnProperty(attr) && typeof this[attr] !== 'function' && attr !== '$$hashKey') {
                    if (typeof this[attr] === 'string') {
                    this[attr] = this[attr].replace(/\'|\"/g, '’');
                    }
                    }
                    return this;
                    },
                    check: function() {
                    if (this.name === '' || this.name.length < 6) { alert('Invalid title name, empty or too short(5)'); return - 1; }
                    if (this.title === '' || this.title.length < 3) { alert('Invalid title name, empty or too short(2)'); return - 1; }
                    return 1;
                    }
            };
            };
            $scope.initorder = function() {
            $scope.tabletitle = $scope.bckups;
                    $scope.predicate = "vorder";
                    $scope.reverse = true;
            };
            $scope.reorder = function(item, alter) {
            alter = alter || "";
                    if (alter !== "")  item = alter;
                    $scope.reverse = ($scope.predicate === item) ? !$scope.reverse : false;
                    $scope.predicate = item;
            };
            $scope.deviceIndex = function(p){
            $scope.selectedItem.type = p;
            };
            $scope.deviceIdList = function(p){
            $scope.selectedItem.devicename = p.a;
                    $scope.selectedItem.deviceid = p.c;
            };
 
            var type = 'admin';
            var url = "../api/getpushnotification";
            $http.get(url).then(function(response) {
                $scope.selectedItem = [];
                $scope.names = [];
                data = response.data.data.notify;
                for (i = 0; i < data.length; i++) {
                        data[i].index = data[i].id;
                        var value = data[i];

                $scope.names.push(new $scope.Objevent().replicate(value));
                }

            });

            $scope.cleaninput = function(ll) {
            if (typeof $scope.selectedItem[ll] === 'string')
                    $scope.selectedItem[ll] = $scope.selectedItem[ll].replace(/\'|\"/g, '’');
            };
            $scope.reset = function(item) {
            $scope.listEventFlag = false;
                    $scope.viewEventFlag = false
                    $scope.createEventFlag = false;
                    $scope[item] = true;
            };
            
            $scope.backlisting = function() {
                $scope.reset('listEventFlag');
            };
            $scope.findaccount = function(name) {
            for (var i = 0; i < $scope.names.length; i++)
                    if ($scope.names[i].name === name)
                    return i;
                    return - 1;
            };
            $scope.view = function(oo) {
            $scope.selectedItem = oo;
                    $scope.reset('viewEventFlag');
            };
            $scope.create = function() {

            $scope.selectedItem = new $scope.Objevent();
                    $scope.selectedItem.is_tag = true;
                    $scope.reset('createEventFlag');
                    $scope.buttonlabel = "Send new notiifcation";
                    $scope.action = "create";
                    return false;
            }


        $scope.update = function(oo) {

                if (oo['tag'] === "" || oo['tag'] === 'undefiend'){
                oo['is_tag'] = false;
                } else{oo['is_tag'] = true; }
                $scope.listindex($scope.selectedItem.restaurant1);
                $scope.listindex1($scope.selectedItem.restaurant2);
                $scope.listindex2($scope.selectedItem.restaurant3);
                $scope.reset('createEventFlag');
                $scope.buttonlabel = "Update categories";
                $scope.action = "update";
        };
    
    
            $scope.testnotify = function() {

                    if (!angular.isDefined($scope.selectedItem.message)){
                    alert("Please key in notification message");
                            return;
                    }
                    if (!angular.isDefined($scope.selectedItem.devicename)){
                    alert("Please Choose the device Id");
                            return;
                    }

                    $http.post("../api/pushnotification",
                    {
                    'message' : $scope.selectedItem.message,
                            'type'  : $scope.selectedItem.devicename,
                            'env'  :'test',
                            'deviceId' :$scope.selectedItem.deviceid,
                            'title'    :$scope.selectedItem.title,
                    }).then(function(response) { console.log(JSON.stringify(response)); alert("SuccessFully sent notification"); });
                     $scope.backlisting();
            };
            $scope.sendnotification = function() {

                    var u, msg, apiurl, ind;
                   if (!angular.isDefined($scope.selectedItem.message)){
                            alert("Please key in notification message");
                                    return;
                    }
                    if (!angular.isDefined($scope.selectedItem.type)){
                    alert("Please Choose the device Id");
                            return;
                    }
                    if ($scope.action === "create"){
                        if($scope.selectedItem.type === 'ios_weeloy' || $scope.selectedItem.type==='ios_weeloypro'){
                            $scope.selectedItem.title = "New message";
                        }

                        $http.post("../api/pushnotification",
                        {
                                'message' : $scope.selectedItem.message,
                                'title'    :$scope.selectedItem.title,
                                'type'  : $scope.selectedItem.type,
                                'env'  :'live'
                        }).then(function(response) {alert("SuccessFully sent notification");  });
                    }
                        //alert("SuccessCount:"+response.data.data.success+ " " +"ErrorCount:"+response.data.data.fails);

            //$scope.backlisting();		
            };


    }]);</script>





