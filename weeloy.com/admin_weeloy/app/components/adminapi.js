app.service('adminServiceApi', ['$http', '$q', function($http, $q){  

  	var self = this;
        this.ModalDataBooking = function() {

		var i, j, timeslotAr=[], persAr=[], hourslotAr=[], minuteslotAr=[];

		for(i = 1; i < 50; i++) 
			persAr.push(i);
		
		for(i = 9; i < 24; i++)
			for(j = 0; j < 60; j += 15) {
				timeslotAr.push((i < 10 ? '0' : '') + i + ':' + (j === 0 ? '0' : '') + j);
				}
			
		for(i = 9; i < 24; i++) 
			hourslotAr.push((i < 10 ? '0' : '') + i);
		
		for(i = 0; i < 60; i += 5) 
			minuteslotAr.push((i < 10 ? '0' : '') + i);
			
		return  {
				name: "", 
				pers: persAr, 
				timeslot: timeslotAr, 
				hourslot: hourslotAr,
				minuteslot: minuteslotAr,
				ntimeslot: 0,
				event: ['Birthday', 'Wedding', 'Reunion'],
				start_opened: false,
				opened: true,
				selecteddate: null,
				originaldate: null,
				dateFormated: "",
				theDate: null,
				minDate: null,
				maxDate: null,
				dateOptions: { formatYear: 'yyyy', startingDay: 1 },
				formats: ['dd-MM-yyyy', 'dd/MMMM/yyyy', 'yyyy/MM/dd', 'dd/MM/yyyy', 'dd-MM-yyyy', 'yyyy-MM-dd', 'dd.MM.yyyy', 'shortDate', 'yyyy-MM-dd', ],
				formatsel: "",
				func: null,
			
				setInit: function(ddate, rtime, func, format, minDate, maxDate) {
					
					this.theDate = null;
					this.func = func;
					this.formatsel = this.formats[format];			
					this.ntimeslot = rtime.substring(0, 5);
				
					if(ddate instanceof Date === false) {
						ddate = ddate.jsdate();
						}
					this.originaldate = ddate;
					this.selecteddate = ddate.getDateFormat('-');		
					this.minDate = minDate;
					this.maxDate = maxDate;
					
					if(navigator.userAgent.indexOf("Chrome") != -1 ) { 
						this.formatsel = this.formats[8]; 
						this.selecteddate = ddate.getDateFormatReverse('-'); 
						}	
					},
			
				setDate: function(ddate) {
					this.theDate = null;
					this.originaldate = ddate; // this is for the case that no data selected
					this.selecteddate = ddate.getDateFormat('-');
					if(navigator.userAgent.indexOf("Chrome") != -1) 
						this.selecteddate = ddate.getDateFormatReverse('-'); 
					},
					
				getDate: function(sep, mode) {
					if(typeof this.theDate !== null && typeof this.theDate !== undefined && this.theDate instanceof Date)
						return (mode !== 'reverse') ? this.theDate.getDateFormat(sep) : this.theDate.getDateFormatReverse(sep);
					return (mode !== 'reverse') ? this.originaldate.getDateFormat(sep) : this.originaldate.getDateFormatReverse(sep);
					},
					
				dateopen: function($event) {
					this.start_opened = true;
					this.opened = true;
					$event.preventDefault();
					$event.stopPropagation();
					},
				
				disabled: function(date, mode) {
					return false;
					},
			
				calendar: function() {
					this.theDate = this.selecteddate;
					this.dateFormated = this.theDate.getDateFormat('/');
					},
			
				onchange: function() {
					this.theDate = this.selecteddate;
					this.dateFormated = this.theDate.getDateFormat('/');

					if(this.func !== null)
						this.func();
					}	
				};
		};
	var weeloy_demo_restaurant = ["SG_SG_R_TheFunKitchen", "SG_SG_R_TheOneKitchen", "FR_PR_R_LaTableDeLydia", "KR_SE_R_TheKoreanTable"];

	this.readcrawler = function(email, token) {
		return $http.post("../api/services.php/crawler/read/",
			{ 'email' : email,
            'token'  : token
 			}).then(function(response) { return response.data;});
		};

	this.updatecrawler = function(oo, email, token) {
		return $http.post("../api/services.php/crawler/update/",
			{ 'email' : email,
		   'content'  : oo,
           'token'  : token
		   }).then(function(response) { return response.data;});
		};
		
	this.createcrawler = function(oo, email, token) {
		return $http.post("../api/services.php/crawler/create/",
			{ 'email' : email,
		   'content'  : oo,
           'token'  : token
		   }).then(function(response) { return response.data;});
		};
       
	this.toggleactivationcrawler = function(oo, email, token) {
		return $http.post("../api/services.php/crawler/toggleactivation/",
			{ 'email' : email,
		   'content'  : oo,
           'token'  : token
		   }).then(function(response) { return response.data;});
		};
       
	this.deletecrawler = function(oo, email, token) {
		return $http.post("../api/services.php/crawler/delete/",
			{ 'email' : email,
		   'content'  : oo,
           'token'  : token
		   }).then(function(response) { return response.data;});
		};

	this.readCluserEmail = function(email, token) {
		var oo = { 'email' : email, 'token'  : token };
		return $http.post("../api/services.php/cluster/reademail/", oo).then(function(response) { 
			return response.data;
			});
		};


	this.readusracl = function() {
		return $http.get("../api/user/getuseracl").then(function(response) { return response; });
		};

	this.updateusracl = function(email, id, role, value) {
		return $http.post("../api/user/updateacl",
			{ 'email' : email,
		   'id'  : id,
		   'role'  : role,
		   'acl'  : value
		   }).then(function(response) { return response.data;});
		};
		
	this.createusracl = function(email, role, value) {
		return $http.post("../api/user/acl",
			{
		   'email' : email,
		   'role'  : role,
		   'acl'  : value
			}).then(function(response) { return response.data;});
		};
       
	this.deleteusracl = function(email) {
		return $http.post("../api/user/deleteacl",
			{
            'email' : email,
			}).then(function(response) { return response.data;});
		};

	this.readMember = function(email) {
		return $http.post("../api/user/member/list",
			{
            'email' : email,
			}).then(function(response) { return response.data;});
		};
        this.readPaymentMethod = function(){
            var url = "../api/v2/payment/method/list";
            var deferred = $q.defer();
		   $http.get(url, {
				cache: true
			}).success(function(response) {
				deferred.resolve(response);
			});
			return deferred.promise;

           

         }
         this.createpaymentmode = function(restaurant, payment_type, payment_mode,product,status) {
		return $http.post("../api/v2/payment/method/save",
			{
		    'restaurant' : restaurant,
		    'payment_type'  : payment_type,
		    'payment_mode'  : payment_mode,
                    'product'       : product,
                    'status'        : status
			}).then(function(response) { return response.data;});
        };
         this.updatepaymentmode = function(restaurant, payment_type, payment_mode,product,status) {
		return $http.post("../api/v2/payment/method/update",
			{
		    'restaurant' : restaurant,
		    'payment_type'  : payment_type,
		    'payment_mode'  : payment_mode,
                    'product'       : product,
                    'status'        : status
			}).then(function(response) { return response.data;});
        };
        this.readPaymentList = function(restaurant){
            var url = "../api/v2/payment/method/"+restaurant;
            var deferred = $q.defer();
		   $http.get(url, {
				cache: true
			}).success(function(response) {
				deferred.resolve(response);
			});
			return deferred.promise;
         };
         
    	this.readMemberListRes = function() {
		return $http.post("../api/listmember/",
		{
                'token'        : token
		}).then(function(response) { return response; });
        };

        this.readDSBRestaurant = function() {
		return $http.post("../api/dailyboard.php/md_restaurant/getdsbrestlist",
		{
                'token'        : token
		}).then(function(response) { return response.data; });
        };

        this.deleteDSBRestaurant = function(code) {
		return $http.post("../api/dailyboard.php/md_restaurant/deletedsbresto",{
				"httpMethod": "DELETE",
				"body":{
					"data":{
						'code'        : code,
                		'token'        : token		
					}	
			}
                
		}).then(function(response) { return response.data; });
        };

        this.createDSBRestaurant = function(params) {
		return $http.post("../api/dailyboard.php/md_restaurant/newdsbresto",{
				"httpMethod": "POST",
				"body":{
					"data": params
				}               
		}).then(function(response) { return response.data; });
        };

        this.getallrestaurant = function() {  
	        return $http.post('../api/dailyboard.php/md_dailyspecial/getallrestaurant').then(function(response) {return response.data; })

		};

		this.getallrestaurantdetails = function(params) {  
	        return $http.post('../api/dailyboard.php/md_dailyspecial/getallrestaurantdetails', params).then(function(response) {return response.data; })

		};

    	this.updateMemberPermission = function(email, content) {
		return $http.post("../api/updatememberpermission",
		{
		'email'        : email,
		'content'        : content,
        'token'        : token
		}).then(function(response) { return response; });
        };

    	this.addMember = function(email, mobile, password, lastname, firstname, platform, member_type, gender, salutation, country) {
    		var url = "../api/addmember/" ;
			return $http.post(url,
			{
			'email'        : email,
			'mobile'        : mobile,
			'password'        : password,
			'lastname'        : lastname,
			'firstname'        : firstname,
			'member_type'        : member_type,
			'gender'        : gender,
			'salutation'        : salutation,
			'country'        : country,
			'platform'        : platform,
			'token'        : token
			}).then(function(response) { return response; });
        };
         
    	this.deleteMember = function(email) {
		return $http.post("../api/deletemember",
		{
                'email'        : email,
                'token'        : token
		}).then(function(response) { return response; });
        };
         
     	this.readMemberResto = function(email) {
		return $http.post("../api/readmemberresto",
		{
                'email'        : email,
                'token'        : token
		}).then(function(response) { return response; });
        };
         
        
    	this.readPermissionValue = function() {
		return $http.post("../api/readpermissionvalue",
		{
                 'token'        : token
		}).then(function(response) { return response; });
        };
         
         
	this.readallrestonames = function(email) {
		return $http.post("../api/services.php/restaurant/allnames/", 
		{
		'email' : email,
		'token'  : token
		}).then(function(response) { return response.data.data; });
	};
        this.savePolicy = function (restaurant,data) {
                var defferred = $q.defer();
       
          $http.post("../api/services.php/cancelpolicy/save/",
                    {
                        'restaurant': restaurant,
                        'data':data
                    }).success(function(response) {
                defferred.resolve(response);
            });
             return defferred.promise;
        };
        this.updatePolicy = function (restaurant,data) {
                var defferred = $q.defer();
       
          $http.post("../api/services.php/cancelpolicy/update/",
                    {
                        'restaurant': restaurant,
                        'data':data
                    }).success(function(response) {
                defferred.resolve(response);
            });
             return defferred.promise;
        };
        this.deletePolicy = function (restaurant,data) {
                var defferred = $q.defer();
       
          $http.post("../api/services.php/cancelpolicy/delete/",
                    {
                        'restaurant': restaurant,
                        'data':data
                    }).success(function(response) {
                defferred.resolve(response);
            });
             return defferred.promise;
        };
        this.getCancelPolicy = function (restaurant,product) {
            var defferred = $q.defer();
            return $http.post("../api/services.php/cancelpolicy/list",
                    {
                        'restaurant': restaurant,
                        'type'       :'admin',
                        'product'    :product
                     }).success(function(response) {
                defferred.resolve(response);
            });
             return defferred.promise;
        };
        this.savePaymentDetails = function (restaurant,data) {
                var defferred = $q.defer();
       
          $http.post("../api/services.php/payment/createcredentials/",
                    {
                        'restaurant': restaurant,
                        'data':data
                    }).success(function(response) {
                defferred.resolve(response);
            });
             return defferred.promise;
        };
        this.updatePaymentDetails = function (restaurant,data) {
                var defferred = $q.defer();
       
          $http.post("../api/services.php/payment/updatecredentials/",
                    {
                        'restaurant': restaurant,
                        'data':data
                    }).success(function(response) {
                defferred.resolve(response);
            });
             return defferred.promise;
        };
        this.getPaymentDetails = function (restaurant) {
                var defferred = $q.defer();
       
          $http.post("../api/services.php/payment/getcredentials/",
                    {
                        'restaurant': restaurant
                    }).success(function(response) {
                defferred.resolve(response);
            });
             return defferred.promise;
        };
        this.deletePaymentDetails = function (restaurant) {
                var defferred = $q.defer();

          $http.post("../api/services.php/payment/deletePaymentDetails/",
                    {
                        'restaurant': restaurant
                    }).success(function(response) {
                defferred.resolve(response);
            });
             return defferred.promise;
        };
        this.saveVideolinkDetails = function (restaurant,link, method) {
                var defferred = $q.defer();
       
          $http.post("../api/update/videolink",
                    {
                        'restaurant': restaurant,
                        'data':link,
                        'method':method
                    }).success(function(response) {
                defferred.resolve(response);
            });
             return defferred.promise;
        };
        this.getVideo = function (restaurant,link, method) {
          var defferred = $q.defer();
          $http.post("../api/videolink/getlist",
                    {
                        'restaurant': restaurant,
                    }).success(function(response) {
                defferred.resolve(response);
            });
             return defferred.promise;
        };
        
        
        
        this.getDataTables = function(){
            var API_URL = '../api/tool/datacheck';
            return $http.post(API_URL,
					{
					}).then(function(response) { return response; });
            
        };
		this.getColumnValue = function(record){
            var API_URL = '../api/tool/columncheck';
            // alert("in getColumnValue");
            return $http.post(API_URL,
					{
						'record': record
					}).then(function(response) { return response; });
            
        };		
        this.postUpdColumn = function (record,id,value) {
        	var API_URL = '../api/tool/updatecolumn';
        	return $http.post(API_URL,
        			{
        				'record': record,
        				'id': id,
        				'value':value
        			}).then(function(response) { return response; });
        };    
 }]);
