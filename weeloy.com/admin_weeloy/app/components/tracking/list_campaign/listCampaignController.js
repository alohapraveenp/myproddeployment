app.controller('CampaignController', ['$scope', 'CampaignTracking', '$http', function ($scope, CampaignTracking, $http) {
        $scope.paginator = new Pagination(100);
        $scope.predicate = 'datetime';
        $scope.reverse = false;
        $scope.cpList = [];
       
        $scope.nonefunc = function() {};
        
        $scope.Objcampaign= function () {
            return {
                index: '',
                campaign: '',
                group:'',
                type: '',
                campaign_token: '',
                restaurant:'',
                name:'',
                link:'',
                click:'',
                conversion:''
                
            };
        };
        
        $scope.tabletitle = [
            {a: 'cpg', b: 'Campaign', c: '', q: 'down', cc: 'black', l: ''}, 
            {a: 'name', b: 'Name', c: '', q: 'down', cc: 'black', l: ''}, 
            { a:'group', b:'Group', c:'', q:'down', cc:'black', l:'' },
            {a: 'type', b: 'Type', c: '', q: 'down', cc: 'black', l: ''}, 
            {a: 'restaurant_id', b: 'Restaurant', c: '', q: 'down', cc: 'black', l: '150'}, 
            {alter: 'datetime', a: 'creation_date', b: 'Date', c: 'date', q: 'up', cc: 'orange', l: ''}, 
            { a:'link', b:'Link', c:'', q:'down', cc:'black', l:''},
            { a:'click', b:'Click', c:'',  q:'down', cc:'black', l:'' },
            { a:'conversion', b:'Conversion', c:'',  q:'down', cc:'black', l:'' }];
                  
              
              
        
        $scope.tabletitleContent =[
            { a:'link', b:'Link', c:'', d:'pencil', t:'input', i:0 },
            { a:'name', b:'Name', c:'', d:'tower', t:'input', i:0 },
            { a:'group', b:'Group', c:'', d:'tower', t:'input', i:0 },
            { a:'type', b:'Type', c:'', d:'tower', t:'inputtype', i:0 },
             { a:'campaign', b:'CampaignId', c:'', d:'tag', t:'inputtag', i:0 },
            { a:'restaurant', b:'Restaurant', c:'', d:'tower', t:'dropdown', val:$scope.resArr, func: $scope.nonefunc }];
       

        CampaignTracking.getTracking();
        


//TO BE DONE
        $scope.reset = function(item) {
     
            $scope.listTrackingFlag = false;
            $scope.createTrackingFlag = false;
            $scope[item] = true;
        
        };
        $scope.backlisting = function() {
            $scope.reset('listTrackingFlag');
        };

        $scope.create = function() {
     
            $scope.selectedItem = new $scope.Objcampaign();

            $scope.reset('createTrackingFlag');
            $scope.action = "create";
            return false;
        };

        $scope.savecampaign = function () {
            if($scope.selectedItem.link===''){
                alert("Please Complete Link.");
                return;
            }
            if($scope.selectedItem.type==='external'){
                if($scope.selectedItem.restaurant===''){
                     alert("Please Select the restaurant.");
                    return;
                }
               
            }
            CampaignTracking.saveCampaign($scope.selectedItem)
                    .then(function (response) {
                        if (response.status == 1) {
                            $scope.backlisting();
                        window.location.reload();
                        }
                    });
        };
        
        $scope.deletecampaign = function (object) {
            if (confirm("Are you sure you want to delete " + object.email) == false) {
                return;
            }
            CampaignTracking.deleteCampaign(RestaurantID, email, object).then(function (response) {
                if (response.status == 1) {
                    var index = $scope.campaigns.indexOf(object);
                    $scope.campaigns.splice(index, 1);    
                }
                else{
                    alert('campaign ' + object.email + ' has NOT been deleted. ' + response.errors);
                }
            });
        };

//TO BE DONE



        $scope.initorder = function () {
            $scope.predicate = "datetime";
            $scope.reverse = true;
        };

        $scope.reorder = function (item, alter) {
            alter = alter || "";
            if (alter !== "")
                item = alter;
            $scope.reverse = ($scope.predicate == item) ? !$scope.reverse : false;
            $scope.predicate = item;
        };
                

        $http.get("../api/marketing/campaignlist").success(function (response) {
            var data = response.data.data;
            for (var i = 0; i < data.length; i++) {
                data[i].index = i + 1;
                data[i].cpg = data[i].campaign + data[i].sub_id;
                data[i].creation_date = data[i].creation_date.substring(0, 10);
                data[i].datetime = data[i].creation_date.jsdate().getTime();
                 data[i].group = data[i].campaign_group;
                data[i].link = data[i].short_link;
                data[i].click =data[i].lcount;
                data[i].conversion = data[i].conversion;
            }
            $scope.cpList = data;
            $scope.currentSelection = "all";
            $scope.paginator.setItemCount($scope.cpList.length);
            $scope.resArr=response.data.restuarant;
            $scope.initorder();
             console.log(JSON.stringify($scope.resArr));
        });
    }]);