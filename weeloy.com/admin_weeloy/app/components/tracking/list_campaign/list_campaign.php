<?php
//    $resdata = new WY_restaurant();
//    //$imgAr = $mediadata->getEventPictureNames($theRestaurant);
//        $resAr= $resdata->getSimpleListRestaurant();
//      printf("var resArr = [");
//            for($i = 0, $sep = ""; $i < count($resAr); $i++, $sep = ", "){
//                printf("%s '%s'", $sep, $resAr[$i]);
//            }
//    printf("];");
            
    
?>

<div class="container">
    <div class="row">
        <div class="col-md-12 left-sec">

            <div ng-controller="CampaignController" ng-init="moduleName = 'campaign'; listTrackingFlag = true; createTrackingFlag=false;" >
             
                <div id='listing' ng-show='listTrackingFlag'>
<!--                                    <h2 style="text-align:center;">Marketing Campaign List</h2> -->
                <div class="form-group"  style='margin-bottom:25px;'>
                    <div class="col-md-4">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>
                            <input type="text" class="form-control input-sm" ng-model="searchText" ng-change="paginator.setPage(0);" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" style='width:200px;'> 
                        </div>
                    </div>
                    <div class="col-md-2 infobk" style='font-size:12px;'>
                            selected records: <strong> {{cpList.length}} </strong>
                    </div>
                    <div class="col-md-2">
                        <div class="btn-group">
                            <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown" aria-expanded="false" style='font-size:11px;'>Page Size <span class="caret"></span></button>
                            <ul class="dropdown-menu" role="menu">
                                <li ng-repeat="x in paginator.pagerange()"><a href ng-click="paginator.setRowperPage(x)">{{x}}</a></li>
                            </ul>
                        </div> 		
                    </div>
                   <div class="col-md-4">
                            <a href ng-click='create();' class="btn btn-success btn-sm" style='color:white;width:200px;'><span class='glyphicon glyphicon-certificate'></span> &nbsp;Create Tracking Link</a>
                    </div>
                  
               </div>
               
                <div class="row">
                    <br /><br /><br />
                    <table width='100%' class="table table-condensed table-striped" style='font-family:helvetica;font-size:11px;'>
                        <thead><tr><th ng-repeat="y in tabletitle"><tbtitle var="{{y.a}}" name='tabletitle' module='moduleName'/></th></tr></thead>						
                        <tr ng-repeat ="x in filteredCampaign = (cpList| filter:searchText) | sizefilter:this | orderBy:predicate:reverse | slicefilter:paginator.getPage():paginator.getRowperPage():paginator.getItemCount() | limitTo:paginator.getRowperPage() " style='font-family:helvetica;font-size:11px;'>
                            <td ng-repeat ="z in tabletitle" nowrap>
                                <span ng-if="z.a !== 'link'"><tb-listingbkg>{{ x[z.a] | adatereverse:z.c }}</tb-listingbkg></span>
                                <span ng-if="z.a === 'link'"><tb-listingbkg><a href="{{ x[z.a]}}" target="_blank" >{{ x[z.a] | adatereverse:z.c}}</a></tb-listingbkg></span>
                            </td>
                        </tr>
                        <tr><td colspan='{{ tabletitle.length}}'></td></tr>
                    </table>
                </div>
                <div ng-if="filteredCampaign.length >= paginator.getRowperPage()" class="col-md-12" align="center" style='font-size:11px' ng-include="'app/components/paginator.html'"></div>
            </div>
            <div class="col-md-12" ng-show='createTrackingFlag'>

                    <br />
                    <a href class='btn btn-info btn-sm customColor' ng-click="backlisting()"><span class='glyphicon glyphicon-step-backward'></span> &nbsp;Back to listing</a>
                    <br />
                <div class="col-md-2"></div>
		 <div class="col-md-8">
		   <div class="row" ng-repeat="y in tabletitleContent | filter: {t: '!dontshow' }"  style="margin: 0 0 20px 0;font-size:12px;font-familly:Roboto">
                      
                        <div class="input-group"  ng-if="y.t === 'input'">
                            <span class="input-group-addon input11"><i class="glyphicon glyphicon-{{y.d}} input14"></i>&nbsp;{{y.b}}</span>
                            <input type="text" class="form-control input-sm" ng-model="selectedItem[y.a]" ng-change="cleaninput(y.a)"  >
                        </div>
                        <div class="input-group"  ng-if="y.t === 'inputtype'">
                            <span class="input-group-addon input11"><i class="glyphicon glyphicon-{{y.d}} input14"></i>&nbsp;{{y.b}}</span>
                            <input type="text" class="form-control input-sm" ng-model="selectedItem[y.a]" ng-change="cleaninput(y.a)"  >
                        </div>
                        <div class="input-group"  ng-if="y.t === 'inputtag' && selectedItem.type!=='external'">
                            <span class="input-group-addon input11"><i class="glyphicon glyphicon-{{y.d}} input14"></i>&nbsp;{{y.b}}</span>
                            <input type="text" class="form-control input-sm" ng-model="selectedItem[y.a]" ng-change="cleaninput(y.a)"  >
                        </div>
                       <div class="input-group"  ng-if="y.t === 'inputtag' && selectedItem.type ==='external' ">
                            <span class="input-group-addon input11"><i class="glyphicon glyphicon-{{y.d}} input14"></i>&nbsp;{{y.b}}</span>
                            <input type="text" class="form-control input-sm" ng-model="selectedItem[y.a]='0022' " ng-change="cleaninput(y.a)"  readonly >
                        </div>
                    
                       <div class="input-group" ng-if="y.t === 'dropdown' " >
                            <div class='input-group-btn' dropdown >
                            <button type='button' class='btn btn-default dropdown-toggle btn-sm input11' data-toggle='dropdown'>
                                    <i class="glyphicon glyphicon-{{y.d}} input13"></i>&nbsp; {{y.b}}<span class='caret'></span></button>
                            <ul class='dropdown-menu multi-level scrollable-menu' style='height: auto;max-height: 250px; overflow-x: hidden;'>
                                 <li ng-repeat="p in resArr"><a href ng-click="selectedItem[y.a]=p;y.func()">{{ p }}</a></li>
                            </ul>

                            </div>
                            <input type='text' ng-model='selectedItem[y.a]' class='form-control input-sm' readonly >
                        </div>
                    </div>
                      
                 </div>
                <div class="col-md-2"></div>
		<div class="col-md-7"></div>
		<div class="col-md-5">
			<a href ng-click='savecampaign();' class="btn btn-success btn-sm" style="color:white;width:150px"><i class='glyphicon glyphicon-save'></i> &nbsp;Create </a><br />
		</div>
            </div>
            </div>
            
           
        </div>
    </div>
</div>

<script src="app/components/tracking/list_campaign/listCampaignController.js"></script>
<script src="app/components/tracking/list_campaign/listCampaignService.js"></script>