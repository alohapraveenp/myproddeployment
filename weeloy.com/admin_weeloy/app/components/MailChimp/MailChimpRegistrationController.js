app.controller('MailChimpRegistrationController', ['$scope','$http','$timeout', '$window', 'adminServiceApi', 'Upload', function($scope,$http,$timeout, $window, adminServiceApi, Upload) {
  var todaydate = new Date();
  var aorder = (function() { var i, arr=[]; for(i = 0; i < 30; i++) 
    arr.push(i); return arr; })();
  $scope.paginator = new Pagination(25);
  $scope.token = $("#token").val();
  $scope.email = $("#email").val();
  $scope.restaurant = $("#restaurant").val();
  $scope.mydata = new adminServiceApi.ModalDataBooking();
  $scope.predicate = '';
  $scope.reverse = false;
  $scope.names = [];
  $scope.nonefunc = function() {};
  $scope.tabletitle = [
    {a:'ID', b:'ID', c:'' , q:'down', cc: 'black' },
    {a:'url', b:'URL', c:'', q:'up', cc: 'black'},
    {a:'title', b:'Title', c:'' , q:'down', cc: 'black' }, 
    {a:'description', b:'Description', c:'' , q:'down', cc: 'black' }, 
    {a:'tag', b:'Tag', c:'' , q:'down', cc: 'black' }, 
    {a:'image', b:'Image', c:'' , q:'down', cc: 'black' },
    {a:'date', b:'Date', c:'' , q:'down', cc: 'black' },
    ];
  $scope.bckups = $scope.tabletitle.slice(0);
  $scope.tabletitleContent = [
    { a:'url', b:'URL', c:'', d:'home', t:'input', type:'url', i:0, r:true },
    { a:'title', b:'Title', c:'', d:'tower', t:'input', type:'text', i:0, r:true }, 
    { a:'description', b:'Description', c:'', d:'flag', t:'input', type:'text', i:0 },
    { a:'tag', b:'Tag', c:'', d:'tag', t:'inputtag', i:0 },
    { a:'image', b:'Image', c:'', d:'picture', t:'imagebutton' },
    { a:'date', b:'Date', c:'', d:'calendar', t:'datepicker' },
    ];
  $scope.path ="https://static.weeloy.com/mailchimp/";
  $scope.Objevent = function() {
    console.log("Objevent()");
    return {
      restaurant: $scope.restaurant, 
      id: 0, 
      url: "",
      title: '', 
      tag: '', 
      description: '', 
      image: {},
      date: angular.isDefined($scope.mydata.theDate) && $scope.mydata.theDate !== null ? moment($scope.mydata.theDate).format("YYYY-MM-DD") : moment($scope.date).format("YYYY-MM-DD"),
      remove: function() { 
        for(var i = 0; i < $scope.names.length; i++) 
          if($scope.names[i].name === this.name) {
            $scope.names.splice(i, 1);
            break;
            }
        },
      replicate: function(obj) {
        for (var attr in this)
              if(this.hasOwnProperty(attr) && typeof this[attr] !== 'function' && attr !== '$$hashKey') 
                this[attr] = obj[attr];
        return this;
      },
      clean: function() {
        for (var attr in this)
              if(this.hasOwnProperty(attr) && typeof this[attr] !== 'function' && attr !== '$$hashKey') {
                if(typeof this[attr] === 'string') {
                  this[attr] = this[attr].replace(/\'|\"/g, '’');
                  }
                }
        return this;
      }
    };
  };
  var url ="../api/mailchimp/get";
  $scope.dateChange = function() {
    //consoole.log("dateChange(): theDate: "+$scope.mydata.theDate);
    $scope.selectedItem.date = moment($scope.mydata.theDate).format("YYYY-MM-DD");
  }
  $scope.mydata.setInit(new Date(), "00:00", $scope.dateChange, 5, null, null);
  $http.get(url).then(function(response) {
    $scope.names = [];
    //console.log("pi/home/getmailchimps/ returns "+JSON.stringify(response));
    if (typeof response !== "undefined" && response.status == 200 && typeof response.data !== 'undefined' && typeof response.data.data !== 'undefined' && response.data.data.length > 0) {
      //console.log("get(): "+JSON.stringify(response.data.data));
      $scope.names.push.apply($scope.names, response.data.data);
    }
  });
  $scope.cleaninput = function(ll) {
    if(typeof $scope.selectedItem[ll] === 'string')
      $scope.selectedItem[ll] =  $scope.selectedItem[ll].replace(/\'|\"/g, '’');
  };
  $scope.reset = function(item) {
    $scope.listMailChimpFlag = false;
    $scope.viewMailChimpFlag = false
    $scope.createMailChimpFlag = false;
    $scope[item] = true;  
  };
  $scope.backlisting = function() {
    $scope.reset('listMailChimpFlag');
  };
  $scope.findaccount = function(name) {
    var data =$scope.names;
    for(var i = 0; i < data.length; i++) {
      if(data[i].index.trim()===name.trim()) {
        return i;
      }
    }
    return -1;
  };
  $scope.view = function(oo) {
    $scope.selectedItem = oo;
    console.log("view. selectedItem: "+JSON.stringify($scope.selectedItem));
    $scope.reset('viewMailChimpFlag');
  };
   $scope.create = function() {
    $scope.selectedItem = new $scope.Objevent();
    $scope.reset('createMailChimpFlag');
    $scope.buttonlabel = "Save new mailchimps";
    $scope.action = "create";
    console.log("create(): selectedItem: "+JSON.stringify($scope.selectedItem));
    return false;
  }
  $scope.update = function(oo) {
    $scope.selectedItem = new $scope.Objevent().replicate(oo);
    $scope.reset('createMailChimpFlag');
    $scope.buttonlabel = "Update mailchimps";
    $scope.action = "update";
    console.log("update(): selectedItem: "+JSON.stringify($scope.selectedItem));
  };
  $scope.downloadAndBrowse = function(oo, url) {
      Upload.upload({
            url: "../api/mailchimp/download",
            data: {'data': oo}
          }).then(function(response) {
              if (typeof response !== "undefined" && response.status > 0) {
                  console.log('MailChimp successfully downloaded!');
                  console.log("strURL: "+url);
                  $window.open(url, '_blank');
              } else {
                 console.error("Failed to register MailChimp!");
                 //Notification.show('error', 'Oops!! Sorry Unexpected Error');
              }
      });
  }
  $scope.browse = function(oo) {
    console.log("browse(): selectedItem: "+JSON.stringify(oo));
    var url = window.location.toString();
    var index = url.indexOf("weeloy.com");
    var len = "weeloy.com/".length
    if (index < 0) {
      index = url.indexOf("weeloy.asia");
      len = "weeloy.asia/".length;
    }
    var filename = oo['title'].replace(/ /g, "_") + ".html";
    url = url.slice(0, index+len)+"magazines/"+oo['date']+"/"+filename;
    $.ajax({
        url:url,
        error: function()
        {
           console.log(url+' does not exist! Downloading...');
           $scope.downloadAndBrowse(oo, url);
        },
        success: function()
        {
            console.log(url+' exists! Opening...');
            $window.open(url, '_blank');
        }
    });        
  };
  $scope.delete = function(oo) {
    if(confirm("Are you sure you want to delete " + oo.title) == false)
      return;    
    console.log("Delete(): selectedItem: "+JSON.stringify(oo));
      Upload.upload({
            url: "../api/mailchimp/delete",
            data: {'data': oo}
          }).then(function(response) {
              if (typeof response !== "undefined" && response.status > 0) {
                  console.log('MailChimp successfully deleted!');
                  var index = $scope.names.indexOf(oo);
                  $scope.names.splice(index, 1);
              } else
                 console.error("Failed to delete MailChimp!");
                 //Notification.show('error', 'Oops!! Sorry Unexpected Error');
      });    
  }
  $scope.saveitem = function(MailChimpForm) {
    var u, msg, apiurl, ind;
    if (MailChimpForm.$invalid) {
      var top;
      if (MailChimpForm.url.$invalid || MailChimpForm.title.$invalid) {
        top = $('input[name="url"]').offset().top;
        $(window).scrollTop(top - 200);
      }
      return false;
    }
    $scope.selectedItem['title'] = $scope.selectedItem['title'].replace(/[\W_]+/g, "_");
    console.log("saveitem(): "+JSON.stringify($scope.selectedItem));
    var files = [];
    if (!angular.isUndefined($scope.selectedItem.image.toUpload) && $scope.selectedItem.image.toUpload !== null)
      files.push($scope.selectedItem.image.toUpload);
    //console.log(files.length+" files: "+JSON.stringify(files));
    Upload.upload({
          url: "../api/mailchimp/new",
          data: {file: files, 'data': $scope.selectedItem}
        }).then(function(response) {
            if (typeof response !== "undefined" && response.status > 0) {
                console.log('New MailChimp successfully created!');
//                if(response.data !== null && response.data.data !== null)
//                  console.log("response.data.data: " + JSON.stringify(response.data.data));
                $scope.selectedItem.clean();
                if($scope.action == "update") {
                  ind = $scope.findaccount($scope.selectedItem.id);
                  if(ind >= 0) 
                    $scope.names.splice(ind, 1);
                }
                $item = new $scope.Objevent().replicate($scope.selectedItem);
                $item.image = $scope.selectedItem.image.name;
                $scope.names.push($item);
            } else {
               console.error("Failed to register MailChimp!");
               //Notification.show('error', 'Oops!! Sorry Unexpected Error');
            }
    });
    $scope.backlisting();
  };
  $scope.uploadImage = function(element)  {
    //console.log("uploadImage(): id: "+element.id);
    if(element.files.length == 0) {
      alert("Select/Drop one file");
      return false;
    }
    filename = element.files[0].name;
    ext = filename.substring(filename.lastIndexOf(".")+1).toLowerCase();
    if(ext != "jpg" && ext != "jpeg" && ext != "png" && ext != "gif") {
      alert("Invalid file type (jpg, jpeg, png, gif)");
      return false;
    }
    console.log("uploadImage(): selectedItem: "+JSON.stringify($scope.selectedItem)+" element.files: "+JSON.stringify(element.files));
    $scope.selectedItem.image.name = element.files[0].name;
    $scope.selectedItem.image.media_type = 'picture';
    $scope.selectedItem.image.toUpload = element.files[0];
    console.log("uploadImage(): $scope.restaurant.images: "+JSON.stringify($scope.selectedItem.image));
  };
  $scope.reorder = function (item, alter) {
    alter = alter || "";
    if (alter !== "")
      item = alter;
    $scope.reverse = ($scope.predicate == item) ? !$scope.reverse : false;
    $scope.predicate = item;
  };
  function progressHandlingFunction(e) {
    console.log("even="+JSON.stringify(e));
    var max = e.total;
    var current = e.loaded;
    var Percentage = (current * 100)/max;
    var percent = (current / max) * 100;
    if (percent > 80) {
      console.log(percent);
      $("#statustxt").css('color','#fff'); //change status text to white after 50%
    }
    $('#progressbar').width(percent + '%') //update progressbar percent complete
    $('#statustxt').html(percent + '%');
  }
}]);