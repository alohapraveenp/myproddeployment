
<div class="container">
    <div class="row">
        <div class="col-xl-12 left-sec">
<div ng-controller="MemberController" ng-init="moduleName='member'; listMemberFlag = true; viewMemberFlag=false; createMemberFlag=false;" >
	<div id='listing' ng-show='listMemberFlag'>
		<div class="form-group"  style='margin-bottom:25px;'>
			<div class="col-md-3">
				<div class="input-group">
					<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>
					<input type="text" class="form-control input-sm" ng-model="searchText" ng-change="paginator.setPage(0);" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" style='width:200px;'> 
				</div>
			</div>
			<div class="col-md-9">
			<div class="container" style="font-size:11px">
			      <label ng-repeat="p in memberfilter" class="checkbox-inline" style="margin-left:20px">
					<input  type="checkbox" ng-model="p.a" ng-true-value="true" ng-false-value="false" ng-checked ="p.a" ng-click="filterdata();">{{p.b}}
 				   </label>
			  </div>
			</div>
		</div>
			<div class="col-md-5">
			<a href ng-click='setfilter();' class="btn btn-info btn-xs" style="color:white;width:100px"><i class='glyphicon glyphicon-save'></i> &nbsp;set all</a>
			<a href ng-click='clearfilter();' class="btn btn-warning btn-xs" style="color:white;width:100px;margin-left:50px;"><i class='glyphicon glyphicon-save'></i> &nbsp;clear all</a>
			</div>
			<div class="col-md-3"></div>
			<div class="col-md-4">
			<a href ng-click='create();' class="btn btn-success btn-sm" style='color:white;width:200px;'><span class='glyphicon glyphicon-certificate'></span> &nbsp;Create a New Member</a>
			</div>

        <div style=" clear: both;"></div>
		<table width='100%' class="table table-condensed table-striped" style='font-size:12px;margin-top:20px;'>
			<thead>
				<tr>
				<th ng-repeat="y in tabletitle"><tbtitle var="{{y.a}}" name='tabletitle' module='moduleName'/></th>
				<th>update</th><th> &nbsp; </th><th>delete</th>
                             
				</tr>
			 </thead>
			<tbody style='font-family:helvetica;font-size:12px;'>
				<tr ng-repeat="x in filteredAcl = (names | filter:searchText | filter: filterByType) | sizefilter:this | orderBy:predicate:reverse | slicefilter:paginator.getPage():paginator.getRowperPage():paginator.getItemCount() | limitTo:paginator.getRowperPage()" style='font-family:helvetica;font-size:11px;'>
					<td ng-repeat="y in tabletitle"> {{ x[y.a] | adatereverse:y.c }} </td>
					<td><a href ng-click="update(x)" style='color:blue;'><span class='glyphicon glyphicon-pencil'></span></a></td>
                    <td width='30'>&nbsp;</td>
					<td align='center'><a href ng-click="mdelete(x)" style='color:red;'><span class='glyphicon glyphicon-trash red'></span></a></td>
				</tr><tr><td colspan='{{tabletitle.length + 3}}'></td></tr>
			</tbody>
		</table>
		<div ng-if="filteredAcl.length >= paginator.getRowperPage()" class="col-md-12" align="center" style='font-size:11px' ng-include="'app/components/paginator.html'"></div>
	</div>

	<div class="col-md-12" ng-show='viewMemberFlag'>
		<br /><a href class='btn btn-info btn-sm customColor' ng-click="backlisting()"><span class='glyphicon glyphicon-step-backward'></span> &nbsp;Back to listing</a><br />
		<table class='table-striped' style="margin: 0 0 150px 30px;font-size:13px;font-family: Roboto">		
			<tr ng-repeat="y in tabletitleContent | filter: {a: '!picture'} | filter: {a: '!password'}"><td nowrap ><strong> {{ y.b }} </strong></td><td> &nbsp; </td><td>{{ selectedItem[y.a] | adatereverse:y.c }} </td></tr>
			<tr ng-if="selectedItem['picture'] != ''"><td nowrap><strong>Picture: </strong></td><td> &nbsp; </td><td class="showpict"><img ng-src="{{path}}{{selectedItem.picture}}" height='150'/></td></tr>
		</table>
	</div>

	<div class="col-md-12" ng-show='createMemberFlag'>
		<br />
		<a href class='btn btn-info btn-sm customColor' ng-click="backlisting(1, action)"><span class='glyphicon glyphicon-step-backward'></span> &nbsp;Back to listing</a>
		<br />
		 <div class="col-md-2"></div>
		 <div class="col-md-8">
		   <div class="row" ng-repeat="y in tabletitleContent | filter: {t: '!dontshow' }"  style="margin: 0 0 20px 0;font-size:12px;font-familly:Roboto">

				<div class="input-group"  ng-if="y.t === 'input'">
						<span class="input-group-addon input11"><i class="glyphicon glyphicon-{{y.d}} input14"></i>&nbsp;{{y.b}}</span>
						<input type="text" class="form-control input-sm" ng-model="selectedItem[y.a]" ng-change="cleaninput(y.a)"  ng-readonly="action==='update' && y.a==='email' ">
				</div>
					<div class="input-group" ng-if="y.t === 'array'">
					<div class='input-group-btn' uib-dropdown >
					<button type='button' class='btn btn-default btn-sm input11' uib-dropdown-toggle >
							<i class="glyphicon glyphicon-{{y.d}} input13"></i>&nbsp; {{y.b}}<span class='caret'></span></button>
					<ul class='dropdown-menu scrollable-menu' uib-dropdown-menu style='height: auto;max-height:200px; overflow-x: hidden;' >
					<li ng-repeat="p in y.val"><a href ng-click="selectedItem[y.a]=p;y.func()">{{ p }}</a></li>
					</ul>
					</div>
					<input type='text' ng-model='selectedItem[y.a]' class='form-control input-sm' readonly >
				</div>
				
				<div class="input-group" ng-if="y.t === 'checkarr'">
					<h4>{{y.h }} </h4>
					  <div ng-repeat="p in y.a">
                        	<input  type="checkbox" ng-model="p.a" ng-true-value="true" ng-false-value="false" ng-checked ="p.a" > &nbsp; {{p.b}}
					  </div>
				</div>

				<div class="input-group" ng-if="y.t === 'mselect' && selectedItem.member_type !== 'visitor'">
					<table><tr><td>
				   <label for="multipleSelect"> {{y.h}} </label><br>
					<select name="multipleSelect" ng-model="selectedItem[y.a]" multiple  size="10" style="margin-left:50px;font-size:12px">
					  <option ng-repeat="p in y.val">{{ p }}</option>
					</select><br> 
					</td><td width='50px'>&nbsp;</td><td align-vertical="middle"><p ng-repeat="z in selectedItem[y.a]">{{ z }}</p></td></tr></table>
				</div>

				<div class="input-group" ng-if="y.t === 'resto' && selectedItem.member_type !== 'visitor'">
					<table><tr><td>
				   <label for="multipleSelect"> {{y.h}} </label><br>
					<select name="multipleSelect" ng-model="selectedItem[y.a]" multiple  size="10" style="margin-left:50px;font-size:12px">
					  <option ng-repeat="p in y.val">{{ p }}</option>
					</select><br> 
					</td><td width='50px'>&nbsp;</td><td align-vertical="middle"><p ng-repeat="z in selectedItem[y.a]"><a ng-click="removeMbResto(selectedItem, z)" style="cursor:pointer" >{{ z }}</p></td></tr></table>
				</div>
                              
		   </div><br />
		</div>
		<div class="col-md-2"></div>
		<div class="col-md-7"></div>
		<div class="col-md-5">
			<a href ng-click='processaction();' class="btn btn-success btn-sm" style="color:white;width:150px"><i class='glyphicon glyphicon-save'></i> &nbsp;{{ buttonlabel }} </a><br />
		</div>
		</div>
</div>

</div>
</div>
</div>

<script>

var token = <?php echo "'".$_SESSION['user_backoffice']['token']."';"; ?>
var email =  <?php echo "'".$_SESSION['user_backoffice']['email']."';";?>

app.controller('MemberController', ['$scope','$http','$timeout','adminServiceApi', function($scope,$http,$timeout,adminServiceApi) {
	var todaydate = new Date();
	var aorder = (function() { var i, arr=[]; for(i = 0; i < 30; i++) arr.push(i); return arr; })();
	
	$scope.paginator = new Pagination(25);
	$scope.predicate = '';
	$scope.reverse = false;
	$scope.nonefunc = function() {};
    $scope.useremail = email;
    $scope.priviledged = false;
    $scope.prevtry = {};
 
    var statusarr =['active','inactive', 'deleted'];
    var genderar =['male','female'];
    var titlear =['Mr.','Mrs.', 'Ms.'];
    var countryar = ["Singapore", "Thailand", "HongKong", "Malaysia", "Indonesia", "South Korea"];
    
    $scope.typearr = ['visitor','restaurant', 'deleted', 'weeloy_sales', 'super_weeloy']; // add admin if your priviledge
    $scope.permarr = []; // , "Allow create/Restaurant"
	
	$scope.tabletitle = [ {a:'index', b:'index', c:'' , q:'down', cc: 'black' },{a:'email', b:'Email', c:'' , q:'down', cc: 'black' },{a:'firstname', b:'FirstName', c:'' , q:'down', cc: 'black' },{a:'lastname', b:'LastName', c:'' , q:'down', cc: 'black' }, {a:'member_permission', b:'Permission', c:'' , q:'down', cc: 'black' }, {a:'member_type', b:'Type', c:'' , q:'down', cc: 'black' }, {a:'status', b:'Status', c:'' , q:'down', cc: 'black' }];
	$scope.tabletitleContent = [ {a:'email', b:'Email', c:'', d:'flag', t:'input' }, {a:'mobile', b:'Mobile', c:'', t:'input' }, {a:'gender', b:'Gender', c:'', t:'array', val:genderar }, {a:'salutation', b:'title', c:'', t:'array', val:titlear }, {a:'firstname', b:'FirstName', c:'', d:'flag', t:'input' }, {a:'lastname', b:'LastName', c:'', d:'flag', t:'input' }, {a:'country', b:'Country', c:'', d:'flag', t:'array', val:countryar }, {a:'password', b:'Initial Password', c:'', d:'flag', t:'input' }, {a:'member_type', b:'Type', c:'', d:'sort', t:'array', val:$scope.typearr }, {a:'status', b:'Status', c:'', d:'sort', t:'array', val:statusarr }, {a:'permcp', b:'Permission', c:'', h:'choose permissions', d:'sort', t:'mselect', val:$scope.permarr } ];
	$scope.memberfilter = [{a:true, b:'visitor'}, {a:true, b:'restaurant'}, {a:true, b:'weeloy'}, {a:true, b:'deleted'}, {a:true, b:'weeloy_sales'}, {a:true, b:'super_weeloy'}, {a:true, b:'admin'}];

	$scope.setfilter = function() {
		$scope.memberfilter.map(function(oo) { oo.a = true; });
		};
		
	$scope.clearfilter = function() {
		$scope.memberfilter.map(function(oo) { oo.a = false; });
		};
		
	$scope.filterByType = function(item) {
		var i, limit = $scope.memberfilter.length, type = item.member_type;
		for(i = 0; i < limit; i++)
			if($scope.memberfilter[i].b === type)
				break;
		if(i < limit && !$scope.memberfilter[i].a)
			return false;
		return true;
		};
		
    $scope.ObjectMember = function() {
		return {
		id: '0',
		email: '',
		firstname: '',
		lastname: '',
        member_type: '',
        member_permission: '',
        status :''
			};
		};
       		
	$scope.initorder = function() {
		$scope.tabletitle = $scope.bckups;
		$scope.predicate = "vorder";
		$scope.reverse = true;
	};

	$scope.reorder = function(item, alter) {
		alter = alter || "";
		if (alter !== "")  item = alter;
		$scope.reverse = ($scope.predicate === item) ? !$scope.reverse : false;
		$scope.predicate = item;
	};
      
	$scope.gblindex = function(email) {
		for (var i = 0; i < $scope.names.length; i++)
			if ($scope.names[i].email === email)
				return i;
		return - 1;
		};


 	$scope.readMember = function() { 
		adminServiceApi.readMemberListRes().then(function(response) {              
			var index = 1;
			$scope.selectedItem= null;
			$scope.names = [];	
		
			if(response.data.status === 1) {
				var ind, data = response.data.data;
				$scope.names = data;
				$scope.names.map(function(oo) { 
					oo.index = index++;
					oo.perm = oo.permcp = [];
					oo.resto = oo.restocp = [];
					oo.member_permission = parseInt(oo.member_permission);
					if(oo.member_permission === 0)
						oo.member_permission = "";
					else {
						$scope.permissionsAr.map(function(p) {
						if((oo.member_permission & p.b) !== 0)
							oo.perm.push(p.a);
							});
						oo.permcp = oo.perm.slice(0);
						}
					if(oo.email === $scope.useremail) {
						$scope.priviledged = (oo.perm && oo.perm.length > 0 && oo.member_type === "admin" && oo.perm.indexOf("Allow create/Restaurant") > -1);
						console.log('CURRENT USER', oo, $scope.priviledged);
						}
					ind = $scope.tabletitleContent.inObject("a", "member_type");
					if(ind > -1)
						$scope.tabletitleContent[ind].val = ($scope.priviledged) ? ['visitor','restaurant', 'deleted', 'weeloy_sales', 'super_weeloy', 'admin'] : ['visitor','restaurant', 'deleted', 'weeloy_sales', 'super_weeloy'];
					});
				
				}
			});
        };
 
	$scope.readResto = function() {
		adminServiceApi.readallrestonames($scope.useremail).then(function(response) {  
			$scope.restaurantAr = [];	
			if(response && response.names && response.names instanceof Array) {
				response.names.map(function(oo) { $scope.restaurantAr.push(oo.restaurant); });
				$scope.tabletitleContent.push({a:'restocp', b:'Restaurant', c:'', h:'choose the restaurants', d:'sort', t:'resto', val: $scope.restaurantAr });
				}
			$scope.readMember();
			});
		}; 
		
	$scope.readPermissions = function() {
		adminServiceApi.readPermissionValue().then(function(response) {  
			$scope.permissionsAr = [];	
			$scope.permissions = {};
			if(response.data.status === 1) {
				var data = response.data.data;
				$scope.permissions = response.data.data;
				Object.keys(data).map(function(ll) { 
					$scope.permissionsAr.push({a: ll, b: data[ll]}); 
					});
				}
			$scope.readResto();
			}); 
		};           

	$scope.readMemberResto = function(email) {
		adminServiceApi.readMemberResto(email).then(function(response) {  
			if(response.data.status === 1) {
				var data = response.data.data;
				var ind = $scope.gblindex(email), oo;
				if(ind < 0)
					return;
				oo = $scope.names[ind];
				oo.resto = [];
				data.map(function(p) { oo.resto.push(p.restaurant_id); });
				oo.restocp = oo.resto.slice(0);
				}
			}); 
		};           

	
	$scope.readPermissions(); // this call readresto -> readmember
	
	
	$scope.cleaninput = function(ll) {
		if(typeof $scope.selectedItem[ll] === 'string')
			$scope.selectedItem[ll] =  $scope.selectedItem[ll].replace(/\'|\"/g, '’');
		};
		
	$scope.reset = function(item) {
		$scope.listMemberFlag = false;
		$scope.viewMemberFlag = false
		$scope.createMemberFlag = false;
		$scope[item] = true;	
		};

	$scope.backlisting = function(option, action) {
		if($scope.selectedItem && option && option === 1 && action !== "create") {
			var oo = $scope.selectedItem;
			oo.permcp = oo.perm.slice(0);				
			oo.restocp = oo.resto.slice(0);				
			}
		$scope.reset('listMemberFlag');
		};
		
	$scope.removeAclData = function(email) {
         	var i, data = $scope.names;
         	if(typeof email !== "string" || email.length < 3)
         		return -1;
         		
		for(i = 0; i < data.length; i++)
            if(data[i].email === email)
                return data.splice(i, 1);
		};
   		
	$scope.view = function(oo) {
		$scope.selectedItem = oo;
		$scope.reset('viewMemberFlag');
		};

	$scope.removeMbResto = function(oo, restname) {
		var ind = oo.restocp.indexOf(restname);
		if(ind > -1) {
			oo.restocp.splice(ind, 1);
			}
		//$timeout( function(){ }, 100);	
		};


	$scope.create = function() {
		var ind;
		if((ind = $scope.tabletitleContent.inObject("a", "password")) >= 0) 
			$scope.tabletitleContent[ind].t = "input";
			
		if(typeof $scope.prevtry["email"] === "string")
			$scope.selectedItem = Object.assign({}, $scope.prevtry);
		else $scope.selectedItem = {email:'', gender:'', salutation:'', firstname:'', lastname:'', mobile:'+65 ', password:'', platform: '', country:'Singapore', member_type: 'visitor', resto: [], restocp: [] };
		$scope.reset('createMemberFlag');
		$scope.buttonlabel = "Create member";
		$scope.action = "create";
		$scope.processaction = $scope.createmember;
		}
			
	$scope.update = function(oo) {
		var ind = $scope.tabletitleContent.inObject("a", "permcp");
		
		$scope.selectedItem = oo;
		if(oo.member_type !== "visitor")
			$scope.readMemberResto(oo.email);
		
		$scope.permarr = [];
		$scope.permissionsAr.map(function(p) {
			if(p.a !== "Allow create/Restaurant" || $scope.priviledged)
				$scope.permarr.push(p.a);
				});

		if(ind > -1) 
			$scope.tabletitleContent[ind].val = $scope.permarr;			

		if((ind = $scope.tabletitleContent.inObject("a", "password")) >= 0) 
			$scope.tabletitleContent[ind].t = "dontshow";

		$scope.prev = Object.assign({}, oo);
		$scope.reset('createMemberFlag');
		$scope.buttonlabel = "Update member";
		$scope.processaction = $scope.updatemember;
		$scope.action = "update";
		};

	$scope.mdelete = function(oo) {

		if(['admin', 'super_weeloy'].indexOf(oo.member_type) > -1) {
			alert("cannot delete priviledge (admin..) user");
			return;
			}

		if (confirm("Are you sure you want to delete " + oo.email) == false)
			return;
			
		adminServiceApi.deleteMember(oo.email).then(function() {
			var ind = $scope.gblindex(oo.email);
			
			$scope.names.splice(ind, 1);
			alert(oo.email + " has been deleted");
			});
	};
	
	$scope.IsEmail = function(email) { var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/; return regex.test(email); }
	$scope.cleanemail = function(obj) { obj = obj.replace(/[!#$%^&*()=}{\]\[\"\':;><\?/|\\]/g, '');  return obj; }

	$scope.createmember = function() {
		var oo = $scope.selectedItem, field, errstr = "";

		field = ["email", "mobile", "password", "lastname", "firstname", "gender", "salutation", "country", "member_type"];
		field.forEach(function(ll, index) {
			var val = oo[ll];
			if(typeof val !== "string" || val.length < 1) {
				errstr += " '" + ll + "'";
				return;
				}
			val = val.replace(/`|’|\'|\"/g, /_/).trim();
			if(ll === "email" && $scope.IsEmail(oo.email) === false)
				errstr += " '" + ll + "'";
			oo[ll] = val;
			});
		
		$scope.prevtry = Object.assign({}, $scope.selectedItem); // can easily fix
		if(errstr !== "") {
			alert("error on the following field: " + errstr + ". Member not created");
			return;
			}
			
		adminServiceApi.addMember(oo.email, oo.mobile, oo.password, oo.lastname, oo.firstname, oo.platform, oo.member_type, oo.gender, oo.salutation, oo.country).then(function(response) {
			if(response.data.status === 1) {
				oo.index = $scope.names.length;
				$scope.names.push(oo);
				$scope.prevtry = {};		
				alert("member " + oo.email + " has been created");
				}
			else alert("member " + oo.email + " has NOT been created." + response.errors);
		});
		$scope.backlisting();
	};

			
	$scope.updatemember = function() {
		var oo = $scope.selectedItem, val = 0, restodiff = 0; permdata = $scope.permissions;

		if(!$scope.prev) {
			alert("internal error");
			return;
			}
	
		var prev = $scope.prev;
		if(prev.email !== oo.email) {
			alert("Changing email is not allowed");
			return;
			}
		
		oo.permcp.map(function(ll) { if(permdata[ll]) val += permdata[ll]; });
		oo.member_permission = val;
		if(oo.restocp.length !== oo.resto.length)
			restodiff++;
		else oo.restocp.map(function(ll) { if(typeof oo.resto.indexOf(ll) < 0) restodiff++; });
		
		if(oo.member_permission === prev.member_permission && oo.firstname === prev.firstname && oo.lastname === prev.lastname && oo.member_type === prev.member_type && oo.status === prev.status && restodiff === 0) {
			alert("nothing to change");
			return;
			}
		
		oo.perm = oo.permcp.slice(0);				
		oo.resto = oo.restocp.slice(0);				
		adminServiceApi.updateMemberPermission(oo.email, oo).then(function(response) {
			if(response.data.status === 1) {
				var ind = $scope.gblindex(oo.email);
				if(ind > -1) {
					$scope.names.splice(ind, 1);
					$scope.names.push(oo);
					alert("member " + oo.email + " has been updated");
					}
				}
			else alert("member " + oo.email + " has NOT been updated");
		});
	$scope.prev = null;
	$scope.backlisting();
	};

			
}]);

	
</script>

