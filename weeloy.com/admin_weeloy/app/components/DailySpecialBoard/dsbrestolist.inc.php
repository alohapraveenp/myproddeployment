
<div class="container">
    <div class="row">
        <div class="col-xl-12 left-sec">
<div ng-controller="DSBRestaurantController" ng-init="moduleName='DSB'; listRestaurantFlag = true; viewRestaurantFlag=false; NewRestaurantFlag=false;" >
	
	<div id='listing' ng-show='listRestaurantFlag'>
		<div class="form-group"  style='margin-bottom:25px;'>
			<div class="col-md-3">
				<div class="input-group">
					<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>
					<input type="text" class="form-control input-sm" ng-model="searchText" ng-change="paginator.setPage(0);" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" style='width:200px;'> 
				</div>
			</div>
			<div class="col-md-9">
			<div class="container" style="font-size:11px">
			      <label ng-repeat="p in restaurantfilter" class="checkbox-inline" style="margin-left:20px">
					<input  type="checkbox" ng-model="p.a" ng-true-value="true" ng-false-value="false" ng-checked ="p.a" ng-click="filterdata();">{{p.b}}
 				   </label>
			  </div>
			</div>
		</div>
			<div class="col-md-5">
			<a href ng-click='setfilter();' class="btn btn-info btn-xs" style="color:white;width:100px"><i class='glyphicon glyphicon-save'></i> &nbsp;set all</a>
			<a href ng-click='clearfilter();' class="btn btn-warning btn-xs" style="color:white;width:100px;margin-left:50px;"><i class='glyphicon glyphicon-save'></i> &nbsp;clear all</a>
			</div>
			<div class="col-md-3"></div>
			<div class="col-md-4">
			<a href ng-click='create();' class="btn btn-success btn-sm" style='color:white;width:200px;'><span class='glyphicon glyphicon-certificate'></span> &nbsp;Create a New Restaurant</a>
			</div>

        <div style=" clear: both;"></div>
		<table width='100%' class="table table-condensed table-striped" style='font-size:12px;margin-top:20px;'>
			<thead>
				<tr>
				<th ng-repeat="y in tabletitle"><tbtitle var="{{y.a}}" name='tabletitle' module='moduleName'/></th>
				<th>Update</th><th>Approve</th><th>Delete</th>
                             
				</tr>
			 </thead>
			<tbody style='font-family:helvetica;font-size:12px;'>
				<tr ng-repeat="x in filteredAcl = (names | filter:searchText | filter: filterByType) | sizefilter:this | orderBy:predicate:reverse | slicefilter:paginator.getPage():paginator.getRowperPage():paginator.getItemCount() | limitTo:paginator.getRowperPage()" style='font-family:helvetica;font-size:11px;'>
					<td ng-repeat="y in tabletitle"><a href ng-click="view(x)"> {{ x[y.a] | adatereverse:y.c }} </a></td>
					<td align='center'><a href ng-click="update(x)" style='color:blue;'><span class='glyphicon glyphicon-pencil'></span></a></td>
                    <td align='center'><a href ng-click="approve(x)" style='color:green;'><span class='glyphicon glyphicon-ok'></span></a></td>
					<td align='center'><a href ng-click="mdelete(x)" style='color:red;'><span class='glyphicon glyphicon-trash red'></span></a></td>
				</tr><tr><td colspan='{{tabletitle.length + 3}}'></td></tr>
			</tbody>
		</table>
		<div ng-if="filteredAcl.length >= paginator.getRowperPage()" class="col-md-12" align="center" style='font-size:11px' ng-include="'app/components/paginator.html'"></div>
	</div>



	<div class="col-md-12" ng-show='viewRestaurantFlag'>
		<br /><a href class='btn btn-info btn-sm customColor' ng-click="backlisting()"><span class='glyphicon glyphicon-step-backward'></span> &nbsp;Back to listing</a><br />
		<br>
		<table class='table-striped' style="margin: 0 0 150px 30px;font-size:13px;font-family: Roboto">		
			<tr ng-repeat="y in tableNewRestaurant | filter: {b: '!I am the authorized representative of this business'}">
			<td nowrap ><strong> {{ y.b }} </strong></td><td> &nbsp; </td><td>{{ selectedItem[y.a] | adatereverse:y.c }} </td></tr>
			<tr ng-if="selectedItem['owner'] == 1">
				<td>
					<strong>Owner: </strong>
				</td>
			<td> &nbsp; </td><td>Yes</td>
			</tr>
			<tr ng-if="selectedItem['owner'] == 0">
				<td>
					<strong>Owner: </strong>
				</td>
			<td> &nbsp; </td><td>No</td>
			</tr>
			<!-- 
			<tr ng-if="selectedItem['picture'] != ''"><td nowrap><strong>Picture: </strong></td><td> &nbsp; </td><td class="showpict"><img ng-src="{{path}}{{selectedItem.picture}}" height='150'/></td></tr> -->
		</table>
	</div>


	<div class="col-md-12" ng-show='NewRestaurantFlag'>
		<br />
		<a href class='btn btn-info btn-sm customColor' ng-click="backlisting(1, action)"><span class='glyphicon glyphicon-step-backward'></span> &nbsp;Back to listing</a>
		<br />
		 <div class="col-md-2"></div>
		 <div class="col-md-8">
		 
		 <div id="parnercodecont" ng-show="partnershow">
            <select ng-model="restaurantc" 
	               ng-options="restcodex.restaurant as restcodex.restaurant for restcodex in restcode" ng-change="getdetails(this)">
	              <option value="">Select Restaurant Code</option>
	          </select>
		 </div>
		 <br>
		 
		 	<div class="row" ng-repeat="y in tableNewRestaurant | filter: {t: '!dontshow' }"  style="margin: 0 0 20px 0;font-size:12px;font-familly:Roboto">
                 <div class="input-group" ng-if="y.t === 'input'">
                    <span class="input-group-addon input11"><i class="glyphicon glyphicon-{{y.d}} input14"></i>&nbsp;{{y.b}}</span>
                    <input type="{{y.type}}" class="form-control input-sm" ng-model="restaurant[y.a]" ng-change="cleaninput(y.a)" name={{y.a}} ng-required="{{y.r}}"/>
                </div>
                <div class="error error-msg" style='color:red;'>
                    <span ng-show="RegistrationForm.$submitted && RegistrationForm.{{y.a}}.$error.required">Please enter '{{y.b}}'</span>
                    <span ng-show="RegistrationForm.$submitted && RegistrationForm.{{y.a}}.$error.url">Invalid URL!</span>
                    <span ng-show="RegistrationForm.$submitted && RegistrationForm.{{y.a}}.$error.email">Invalid Email!</span>
                </div>
                
                <div class="input-group" ng-if="y.t === 'dropdown'">
                   
                    <div class='input-group-btn' dropdown >
                        <button type='button' class='btn btn-default dropdown-toggle btn-sm input1' data-toggle='dropdown'>
                            <i class="glyphicon glyphicon-{{y.d}} input13"></i>&nbsp; {{y.b}}<span class='caret'></span>
                        </button>
                        <ul class='dropdown-menu scrollable-menu'style='height: auto;max-height:120px; overflow-x: hidden;'>
                            <li ng-repeat="p in y.val"><a href ng-click="restaurant[y.a]=p;checkpartner(p);y.func()">{{ p }}</a></li>
                        </ul>
                    </div>
                    <input type='text' ng-model='restaurant[y.a]' class='form-control input-sm' readonly >
                </div>

                <div class="input-group" ng-if="y.t === 'array'">
                    <div class='input-group-btn' dropdown >
                        <button type='button' class='btn btn-default dropdown-toggle btn-sm input11' data-toggle='dropdown'>
                            <i class="glyphicon glyphicon-{{y.d}} input13"></i>&nbsp; {{y.b}}<span class='caret'></span>
                        </button>
                        <ul class='dropdown-menu scrollable-menu'style='height: auto;max-height:120px; overflow-x: hidden;' >
                            <li ng-repeat="p in y.val"><a href ng-click="restaurant[y.a]=p;y.func()">{{ p }}</a></li>
                        </ul>
                    </div>
                    <input type='text' ng-model='restaurant[y.a]' class='form-control input-sm' readonly >
                </div>
                <div class="input-group"  ng-if="y.t==='checkbox'">
                    <input type="checkbox" ng-model="restaurant[y.a]" ng-checked ="{{restaurant[y.a]}}"> &nbsp; {{y.b}}
                </div>
                <div class="input-group"  ng-if="y.t==='textarea'">
                 <span class="input-group-addon input11"><i class="glyphicon glyphicon-{{y.d}} input14"></i>&nbsp;{{y.b}}</span>
                    <textarea class="form-control input-sm" ng-model="restaurant[y.a]" ng-change="cleaninput(y.a)"></textarea>
                </div> 
                <div class="input-group" ng-if="y.t === 'imagebutton'">
				    <span class="input-group-addon"><i class="glyphicon glyphicon-file"></i> &nbsp; {{y.b}}</span>
				    <input type="file" name="{{y.a}}" id="{{y.a}}" class="form-control input-sm" onchange="angular.element(this).scope().uploadImage(this)" ng-model="file" placeholder="Upload Files" accept="image/*">  
                </div>
		   </div>
            
            <!-- <div class="g-recaptcha col-md-6" data-sitekey="6LdmpA4UAAAAAETsRaMqz8cjrUoyFCNwuuvJk5DS" data-callback="correctCaptcha" style='margin-left:0px;padding-left:0px;' ng-show="showCaptcha"></div> -->
		   
		 </div>
		<div class="col-md-2"></div>
		<div class="col-md-7"></div>
		
		<div class="col-md-5">
			<a href ng-click='processaction();' class="btn btn-success btn-sm" style="color:white;width:150px"><i class='glyphicon glyphicon-save'></i> &nbsp;{{ buttonlabel }} </a><br />
		</div>
	</div>

			</div>
		</div>
</div>

</div>
</div>
</div>

<script>

var token = <?php echo "'".$_SESSION['user_backoffice']['token']."';"; ?>
var email =  <?php echo "'".$_SESSION['user_backoffice']['email']."';";?>

app.controller('DSBRestaurantController', ['$scope','$http','$timeout','adminServiceApi', function($scope,$http,$timeout,adminServiceApi) {
	var todaydate = new Date();
	var aorder = (function() { var i, arr=[]; for(i = 0; i < 30; i++) arr.push(i); return arr; })();
	
	$scope.paginator = new Pagination(25);
	$scope.predicate = '';
	$scope.reverse = false;
	$scope.nonefunc = function() {};
    $scope.useremail = email;
    $scope.priviledged = false;
    $scope.prevtry = {};
 	$scope.DSB = [];
 	$scope.restcode = [];
    var statusarr =['pending','emailverified','dsb', 'delete'];
    var typedsbarr =['Basic - Free','Enhanced', 'Partner'];
    var genderar =['male','female'];
    var titlear =['Mr.','Mrs.', 'Ms.'];
    var countryar = ["Singapore", "Thailand", "HongKong", "Malaysia", "Indonesia", "South Korea"];
    var cityar = ["Singapore", "Bangkok", "Phuket","HongKong", "Kuala Lumpur", "Jakarta", "Seoul"];
    
    $scope.typearr = ['pending','emailverified','dsb', 'delete']; // add admin if your priviledge
    $scope.permarr = []; // , "Allow create/Restaurant"
	
	$scope.tabletitle = [
						{a:'code', b:'Code', c:'' , q:'down', cc: 'black'},
						{a:'name', b:'Name', c:'' , q:'down', cc: 'black' },
						{a:'country', b:'Country', c:'' , q:'down', cc: 'black' },  
						{a:'email', b:'Email', c:'' , q:'down', cc: 'black' },
						{a:'owner_phone', b:'Phone', c:'' , q:'down', cc: 'black' },
						{a:'status', b:'Status', c:'' , q:'down', c: 'black' },
						{a:'firstname', b:'Firstname', c:'' , q:'down', cc: 'black' }
						];
	
	$scope.tableNewRestaurant = [
		  { a:'type', b:'Select Listing Type', c:'', d:'list', t:'dropdown', val: ["Basic - Free", "Enhanced", "Partner"] },
          { a:'title', b:'Title', c:'', d:'tower', t:'input', i:0, r:false, type:'text' },
          { a:'firstname', b:'First Name', c:'', d:'pencil', t:'input', i:0, r:true, type:'text' },
          { a:'lastname', b:'Last Name', c:'', d:'pencil', t:'input', i:0, r:true, type:'text' },
          { a:'email', b:'Restaurant Email', c:'', d:'inbox', t:'input', i:0, r:true, type:'email' },
          { a:'restaurantphone', b:'Phone', c:'', d:'phone', t:'input', i:0, r:false, type:'text' },
          { a:'owner', b:'I am the authorized representative of this business', c:'', d:'user', t:'checkbox', i:0 },
          { a:'name', b:'Restaurant Name', c:'', d:'pencil', t:'input', i:0, r:true, type:'text' },
          { a:'address', b:'Street Address', c:'', d:'map-marker', t:'input', i:0, r:false, type:'text' },
          { a:'city', b:'City', c:'', d:'pencil', t:'input', i:0, r:true, type:'text' },
          { a:'country', b:'Country', c:'', d:'pencil', t:'input', i:0, r:true, type:'text' },
          { a:'postalcode', b:'Postal Code', c:'', d:'pencil', t:'input', i:0, r:false, type:'text' },
          { a:'restaurantemail', b:'Email', c:'', d:'inbox', t:'input', i:0, r:true, type:'email' },
          { a:'owner_phone', b:'Restaurant Phone', c:'', d:'phone', t:'input', i:0, r:false, type:'text' },
          { a:'cuisines', b:'Cuisines', c:'', d:'cutlery', t:'input', i:0, r:false, type:'text' },
          { a:'website', b:'Website', c:'', d:'home', t:'input', i:0, r:true, type:'url' },
          { a:'pricepoint', b:'Price Point', c:'', d:'usd', t:'input', i:0, r:false, type:'text' },
          { a:'description', b:'Description', c:'', d:'comment', t:'textarea', i:0 },
          { a:'logo', b:'Logo', c:'', d:'picture', t:'imagebutton' },
          { a:'cover', b:'Cover', c:'', d:'picture', t:'imagebutton' },
        ];
	
	$scope.restaurantfilter = [{a:true, b:'pending'}, {a:true, b:'emailverified'}, {a:true, b:'dsb'}, {a:true, b:'delete'}];

	$scope.init = function() {

	$scope.restaurantc = null;

    $scope.restaurant = {
	      id: "",
	      code: "",
	      title: "",
	      firstname: "",
	      lastname: "",
	      email: "",
	      phone: "",
	      owner: false,
	      name: "",
	      address: "",
	      city: "",
	      country: "",
	      postalcode: "",
	      restaurantemail: "",
	      restaurantphone: "",
	      cuisines: "",
	      website: "",
	      pricepoint: "",
	      statusupdate: "0",
	      images: {
	        logo: {},
	        cover: {}
	      },
	      type: "Basic - Free",
	      status: "pending"
	    };
    	$('#logo').files = [];
    	$('#cover').files = [];
  	}
  	$scope.file = null;
  	$scope.init();

  	$scope.savenewresto = function(RegistrationForm) {
	    $scope.restaurant.id = $scope.newobj();
	    var params = $scope.restaurant;
	    

	    adminServiceApi.createDSBRestaurant(params).then(function(response) {
			
			if(response.status === '1') {
				alert("New Restaurant Successfully Created");
				javascript:setNavBar('DSB_MEMBERLIST');			
			}
			else
			{
				alert("New Restaurant Fail");
			}
		});
	  };

	$scope.newobj = function() {
		var i, j, id, oo = {};
		$scope.tableNewRestaurant.forEach(function(f) { oo[f.a] = ""; });
		for(i = 0; i < 20; i++) {
			id = Math.floor((Math.random() * 10000000) + 1);
			for(j = 0; j < $scope.DSB.length; j++)
				if($scope.DSB[j].ID === id)
					break;
			if(j >= $scope.DSB.length)
				break;
			}
		if(i < 20) {
			oo.ID = id;
			return id;
			}
		alert("internal error");
		return null;
		};

	$scope.partnershow = false;
	$scope.checkpartner = function(params) {
		if(params == 'Partner')
		{
			$scope.partnershow = true;
		}
		else
		{
			$scope.partnershow = false;
		}
	};

	$scope.getdetails = function(params)
	{

		adminServiceApi.getallrestaurantdetails(params.restaurantc).then(function(response) {
		
			console.log(response.data);
			if(response.status== 1)
			{
				$scope.restaurant = response.data
				$scope.restaurant.images = {
				        logo: {},
				        cover: {}
				      };
				$scope.restaurant.statusupdate = "0";
				$scope.restaurant.status = "verifyemail";
			}
			else
			{
				alert(response.data.errors);
			} 
		});
	}

  	$scope.uploadImage = function(element)	{

	    if(element.files.length == 0) {
	      alert("Select/Drop one file");
	      return false;
	    }
	    filename = element.files[0].name;    
	    ext = filename.substring(filename.lastIndexOf(".")+1).toLowerCase();
	    if(ext != "jpg" && ext != "jpeg" && ext != "png" && ext != "gif") {
	      alert("Invalid file type (jpg, jpeg, png, gif)");
	      return false;
	    }
	    var reader = new FileReader();
	    $scope.restaurant.images[element.id].name = element.files[0].name;
	    $scope.restaurant.images[element.id].media_type = 'picture';

	    reader.addEventListener("load", function () {
	      $scope.restaurant.images[element.id].content = reader.result;      
	    }, false);
	    reader.readAsDataURL(element.files[0]);
	  };


	$scope.setfilter = function() {
		$scope.restaurantfilter.map(function(oo) { oo.a = true; });
		};
		
	$scope.clearfilter = function() {
		$scope.restaurantfilter.map(function(oo) { oo.a = false; });
		};
		
	$scope.filterByType = function(item) {
		var i, limit = $scope.restaurantfilter.length, type = item.status;
		for(i = 0; i < limit; i++)
			if($scope.restaurantfilter[i].b === type)
				break;
		if(i < limit && !$scope.restaurantfilter[i].a)
			return false;
		return true;
		};
		
       		
	$scope.initorder = function() {
		$scope.tabletitle = $scope.bckups;
		$scope.predicate = "vorder";
		$scope.reverse = true;
	};

	$scope.reorder = function(item, alter) {
		alter = alter || "";
		if (alter !== "")  item = alter;
		$scope.reverse = ($scope.predicate === item) ? !$scope.reverse : false;
		$scope.predicate = item;
	};
      
	$scope.gblindex = function(email) {
		for (var i = 0; i < $scope.names.length; i++)
			if ($scope.names[i].email === email)
				return i;
		return - 1;
	};


 	$scope.readDSBRestaurant = function() { 
		adminServiceApi.readDSBRestaurant().then(function(response) {
			             
			var index = 1;
			$scope.selectedItem= null;
			$scope.names = [];	
			
			
			if(response.status == 1) {
				var ind, data = response.data;
				$scope.names = data;
				$scope.names.map(function(oo) { 
					
					$scope.DSB = response.data;
					oo.index = index++;
					
					ind = $scope.tabletitle.inObject("a", "dsbrestaurant");
					});
				}
			});
        };

        $scope.readDSBRestaurant();
	
	$scope.cleaninput = function(ll) {
		if(typeof $scope.selectedItem[ll] === 'string')
			$scope.selectedItem[ll] =  $scope.selectedItem[ll].replace(/\'|\"/g, '’');
		};
		
	$scope.reset = function(item) {
		$scope.listRestaurantFlag = false;
		$scope.viewRestaurantFlag = false
		$scope.NewRestaurantFlag = false;
		$scope[item] = true;	
		};

	$scope.backlisting = function(option, action) {
		if($scope.selectedItem && option && option === 1 && action !== "create") {
			var oo = $scope.selectedItem;
							
			}
		$scope.reset('listRestaurantFlag');
		};
		
	$scope.removeAclData = function(email) {
         	var i, data = $scope.names;
         	if(typeof email !== "string" || email.length < 3)
         		return -1;
         		
		for(i = 0; i < data.length; i++)
            if(data[i].email === email)
                return data.splice(i, 1);
		};
   		
	$scope.view = function(oo) {
		$scope.selectedItem = oo;
		$scope.reset('viewRestaurantFlag');
		};

	$scope.create = function() {
		
		$scope.init();
		$scope.reset('NewRestaurantFlag');
		$scope.buttonlabel = "New Restaurant";
		$scope.action = "create";
		$scope.restaurant.status = "pending";
		$scope.restaurant.statusupdate = "0";
		$scope.processaction = $scope.savenewresto;
		}
			
	$scope.update = function(oo) {
		oo.images = {
	        logo: {},
	        cover: {}
	      };
		$scope.restaurant = oo;
		$scope.restaurant.images.logo.name = oo.logo;
		$scope.restaurant.images.logo.media_type = 'picture';
		$scope.restaurant.images.logo.content = "";
		$scope.restaurant.images.cover.name = "";
		$scope.restaurant.images.cover.media_type = 'picture';
		$scope.restaurant.images.cover.content = "";
		
		$scope.reset('NewRestaurantFlag');
		$scope.buttonlabel = "Update Restaurant";
		$scope.processaction = $scope.savenewresto;
		$scope.action = "update";
		};

	$scope.approve = function(oo) {

		if (confirm("Are you sure you want to Approve "+oo.code+" to DSB status?" ) == false)
			return;
		oo.images = {
	        logo: {},
	        cover: {}
	      };
	    console.log(oo.logo);
		$scope.restaurant = oo;
		$scope.restaurant.images.logo.name = oo.logo;
		$scope.restaurant.images.logo.media_type = 'picture';
		$scope.restaurant.images.logo.content = "";
		$scope.restaurant.images.cover.name = oo.cover;
		$scope.restaurant.images.cover.media_type = 'picture';
		$scope.restaurant.images.cover.content = "";
		$scope.restaurant.status = "dsb";
		$scope.restaurant.statusupdate = "0";
		$scope.restaurant.id = $scope.newobj();
	    var params = $scope.restaurant;
	    console.log(params);
	    adminServiceApi.createDSBRestaurant(params).then(function(response) {
			
			if(response.status === '1') {
				alert("Restaurant Successfully Approved");
				javascript:setNavBar('DSB_MEMBERLIST');		
			}
			else
			{
				alert("Restaurant Fail");
			}
		});	
		
	};

	$scope.mdelete = function(oo) {

		if (confirm("Are you sure you want to delete " + oo.code) == false)
			return;
			
		adminServiceApi.deleteDSBRestaurant(oo.code).then(function() {
			var ind = $scope.gblindex(oo.restaurantemail);
			
			$scope.names.splice(ind, 1);
			alert(oo.code + " has been deleted");
			javascript:setNavBar('DSB_MEMBERLIST');	
			});
	};

	adminServiceApi.getallrestaurant().then(function(response) {
		
		if(response.status== 1)
		{
			$scope.restcode = response.data;
		}
		else
		{
			alert(response.data.errors);
		} 
	});


	$scope.IsEmail = function(email) { var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/; return regex.test(email); }
	$scope.cleanemail = function(obj) { obj = obj.replace(/[!#$%^&*()=}{\]\[\"\':;><\?/|\\]/g, '');  return obj; }
			
}]);

	
</script>

