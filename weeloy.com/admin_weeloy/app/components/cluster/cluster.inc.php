
<div class="container">
    <div class="row">
        <div class="col-xl-12 left-sec">
<div ng-controller="CluserEmailController" ng-init="moduleName='CluserEmail'; listCluserEmailFlag = true; viewCluserEmailFlag=false; createCluserEmailFlag=false;" >
	<div id='listing' ng-show='listCluserEmailFlag'>
		<div class="form-group"  style='margin-bottom:25px;'>
			<div class="col-md-4">
				<div class="input-group col-md-4">
					<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>
					<input type="text" class="form-control input-sm" ng-model="searchText" ng-change="paginator.setPage(0);" style='width:200px;' autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" > 
				</div>
			</div>
			<div class="col-md-2"></div>
			<div class="col-md-4">
<!--			<a href ng-click='create();' class="btn btn-success btn-sm" style='color:white;width:200px;'><span class='glyphicon glyphicon-certificate'></span> &nbsp;Create a new Slave Cluster</a> -->
			</div>
		</div>
            <div style=" clear: both;"></div>
		<table width='100%' class="table table-condensed table-striped" style='font-size:12px;'>
			<thead>
				<tr>
				<th ng-repeat="y in tabletitle"><tbtitle var="{{y.a}}" name='tabletitle' module='moduleName'/></th>
				<th>update</th><th> &nbsp; </th>
				<th>delete</th><th> &nbsp; </th>
                             
				</tr>
			 </thead>
			<tbody style='font-family:helvetica;font-size:12px;'>
				<tr ng-repeat="x in filteredCluserEmail = (names | filter:searchText) | sizefilter:this | orderBy:predicate:reverse | slicefilter:paginator.getPage():paginator.getRowperPage():paginator.getItemCount() | limitTo:paginator.getRowperPage()" style='font-family:helvetica;font-size:11px;'>
				<td ng-repeat="y in tabletitle"><a href ng-click="view(x)">{{ x[y.a] | adatereverse:y.c }}</a></td>
				<td><a href ng-click="update(x)" style='color:blue;'><span class='glyphicon glyphicon-pencil'></span></a></td>
                    		<td width='30'>&nbsp;</td>
				<td nowrap><a href ng-click="delete(x)" style='color:red;'><span class='glyphicon glyphicon-trash'></span></a></td>
					
				</tr><tr><td colspan='{{tabletitle.length + 3}}'></td></tr>
			</tbody>
		</table>
		<div ng-if="filteredCluserEmail.length >= paginator.getRowperPage()" class="col-md-12" align="center" style='font-size:11px' ng-include="'app/components/paginator.html'"></div>
	</div>

	<div class="col-md-12" ng-show='viewCluserEmailFlag'>
		<br /><a href class='btn btn-info btn-sm customColor' ng-click="backlisting()"><span class='glyphicon glyphicon-step-backward'></span> &nbsp;Back to listing</a><br />
		<br />
		<table class='table-striped' style="margin: 0 0 150px 30px;font-size:13px;font-family: Roboto">		
			<tr ng-repeat="y in tabletitleContent | filter: {b: '!picture'} track by $index"><td nowrap ><strong> {{ y.b }} </strong></td><td> &nbsp; </td><td>{{ selectedItem[y.a] | adatereverse:y.c }} </td></tr>
		</table>
	</div>

	<div class="col-md-12" ng-show='createCluserEmailFlag'>
		<br />
		<a href class='btn btn-info btn-sm customColor' ng-click="backlisting()"><span class='glyphicon glyphicon-step-backward'></span> &nbsp;Back to listing</a>
		<br />
		 <div class="col-md-2"></div>
		 <div class="col-md-8">
		   <div class="row" ng-repeat="y in tabletitleContent | filter: {t: '!dontshow' }"  style="margin: 0 0 20px 0;font-size:12px;font-familly:Roboto">

			<div class="input-group"  ng-if="y.t === 'input'">
				<span class="input-group-addon input11"><i class="glyphicon glyphicon-{{y.d}} input14"></i>&nbsp;{{y.b}}</span>
				<input type="text" class="form-control input-sm" ng-model="selectedItem[y.a]" ng-change="cleaninput(y.a)"  ng-readonly="action==='update' && (y.a==='name' || y.a === 'provider') ">
			</div>
                              
		   </div><br />
		</div>
		<div class="col-md-2"></div>
		<div class="col-md-7"></div>
		<div class="col-md-5">
			<a href ng-click='savenewCluserEmail(action);' class="btn btn-success btn-sm" style="color:white;width:150px"><i class='glyphicon glyphicon-save'></i> &nbsp;{{ buttonlabel }} </a><br />
		</div>
		</div>
</div>

</div>
</div>
</div>

<script>

var token = <?php echo "'".$_SESSION['user_backoffice']['token']."';"; ?>
var email =  <?php echo "'".$_SESSION['user_backoffice']['email']."';";?>

app.controller('CluserEmailController', ['$scope', '$http', '$timeout', 'adminServiceApi', function($scope, $http, $timeout, adminServiceApi) {
	var todaydate = new Date();
	var aorder = (function() { var i, arr=[]; for(i = 0; i < 30; i++) arr.push(i); return arr; })();
	
	$scope.paginator = new Pagination(25);
	$scope.email = email;
	$scope.predicate = '';
	$scope.reverse = false;
	$scope.nonefunc = function() {};
 	$scope.listprovider = [ "CHOPE", "HGW", "QUANDO"];
 	$scope.liststatus = ["active", "inactive"];
 	$scope.filteredCluserEmail = [];
 	
 	$scope.tabletitle = [ {a:'clustname', b:'Name', c:'' , q:'down', cc: 'black' }, {a:'clustcategory', b:'Category', c:'' , q:'down', cc: 'black' }, {a:'clustparent', b:'Parent', c:'' , q:'down', cc: 'black' }, {a:'clusterlistcontent', b:'Content', c:'' , q:'down', cc: 'black' } ];	
	$scope.tabletitleContent = [{a:'clustname', b:'Name', c:'' , q:'down', cc: 'black' }, {a:'clustcategory', b:'Category', c:'' , q:'down', cc: 'black' }, {a:'clustparent', b:'Parent', c:'' , q:'down', cc: 'black' }, {a:'clustcontent', b:'Content', c:'100' , q:'down', cc: 'black', t:'input' }];
        
	$scope.ObjectCluserEmail = function() {
		var oo, obj = {};
	
		if($scope.names.length < 0)
			return obj;
		
		oo = $scope.names[0];
		Object.keys(oo).map(function(ll) { obj[ll] = oo[ll]; });
		
		return obj;
		};
       		
	$scope.initorder = function() {
		$scope.tabletitle = $scope.bckups;
		$scope.predicate = "vorder";
		$scope.reverse = true;
	};

	$scope.reorder = function(item, alter) {
		alter = alter || "";
		if (alter !== "")  item = alter;
		$scope.reverse = ($scope.predicate === item) ? !$scope.reverse : false;
		$scope.predicate = item;
	};
    
	adminServiceApi.readCluserEmail($scope.email, token).then(function(response) {              
		$scope.selectedItem= null;
		$scope.names = [];	
		console.log('RESPONSE', response);
		if(response.status !== 1)
			return;

		response.data.map(function(oo, index, arr) {
			oo.clusterlistcontent = (oo.clustcontent.length > 50) ? oo.clustcontent.substr(0, 50)+"..." : oo.clustcontent;
			$scope.names.push(oo);                         
			});   
		console.log('DATA',   $scope.names);       
        });
 
	$scope.reset = function(item) {
		$scope.listCluserEmailFlag = false;
		$scope.viewCluserEmailFlag = false
		$scope.createCluserEmailFlag = false;
		$scope[item] = true;	
		};

	$scope.backlisting = function() {
		$scope.reset('listCluserEmailFlag');
		};
		
	$scope.removeCluserEmailData = function(oo) {
		for(i = 0; i < data.length; i++)
        	if(data[i].name === oo.name)
                return data.splice(i, 1);
		};
   		
	$scope.view = function(oo) {
		oo.astatus = (oo.status === 'active') ? 'active' : 'inactive';
		$scope.selectedItem = oo;
		$scope.reset('viewCluserEmailFlag');
		};

	$scope.create = function() {
		$scope.selectedItem = new $scope.ObjectCluserEmail();
		$scope.reset('createCluserEmailFlag');
		$scope.buttonlabel = "Save new user roles";
		$scope.action = "create";
		return false;
		}

	$scope.update = function(oo) {
		oo.astatus = (oo.status === 'active') ? 'active' : 'inactive';
		$scope.selectedItem = oo;
		$scope.reset('createCluserEmailFlag');
		$scope.buttonlabel = "Update categories";
		$scope.action = "update";
		};

	$scope.savenewCluserEmail = function(action) {

		return;
		
		
		var u, msg, oo = $scope.selectedItem, valAr = [], mandatory = ['name', 'restaurant', 'credentials'], err = 0;
      
		if(action === "create") {
			mandatory.map(function(ll) { if(!oo[ll] || oo[ll] === "") err++; });
			if(err > 0)
            	return alert( " Please fill mandatory fields (" + mandatory.join(", ") + ")");
 
 			adminServiceApi.createCluserEmail(oo, $scope.email, token).then(function(response) {
				if(response.status === 1) {
					$scope.names.push($scope.selectedItem);
					alert("CluserEmail  has been created");
					}
				else alert("CluserEmail has NOT been created: " + response.errors);
				});
				//window.location.reload();
                    }
		else if(action == "update") {
			oo.status = (oo.astatus === 'active') ? 'active' : 'inactive';
			oo.action = (oo.astatus !== 'active');
			adminServiceApi.updateCluserEmail(oo, $scope.email, token).then(function(response) {
				if(response.status === 1) alert("CluserEmail  has been updated");
				else alert("CluserEmail has NOT been updated: " + reponse.errors);
				});
			}
		$scope.backlisting();	
		};
		
	$scope.delete = function(oo) {	

		return;
		
		
		return alert("'remove' function not activated");
		adminServiceApi.deleteCluserEmail(oo, $scope.email, token).then(function(response) {
			if(response.status === 1) {
				$scope.removeCluserEmailData(oo);
				alert(email + " CluserEmail  has been deleted");
				}
			else alert(email + " CluserEmail has NOT been deleted: " + reponse.errors);
			});

		$scope.backlisting();	
			
		};

	function validateEmail(email) {
		if(typeof email !== "string" || email.length < 4)
			return false;
			
    		var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    		return re.test(email);
		}			
}]);

	
</script>

