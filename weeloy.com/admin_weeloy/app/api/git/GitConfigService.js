 (function(app) {
     app.service('GitConfigService',['$http', '$q', function ($http, $q) {
          console.log("GitConfigService");
          this.DevMasterDiff = function() {
            var defferred = $q.defer();
            $http({method: 'GET', url: '../api/services.php/git/devmasterdiff', cache: true })
              .success(function(response) {
                  defferred.resolve(response);
                  // this callback will be called asynchronously
                  // when the response is available.
                  //alert("GitConfigService.DevMasterDiff() successful! " + status + " " + data + " " + count + " " + error);
                  //alert("GitConfigService.DevMasterDiff() successful! response: " + response);
                  console.log('GitConfigService.DevMasterDiff(): Successful!');
              }).error(function(response) {
                  console.log('GitConfigService.DevMasterDiff(): Oops and error');
              });
              return defferred.promise;
          };
     }]);
 })(angular.module('app.api.git.gitconfig', []));