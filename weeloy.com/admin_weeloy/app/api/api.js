(function(app) {
    app.factory('API', API);
    API.$inject = [
        'VerifyEmailService',
        'CancelPolicyService',
        'NotificationConfigurationService',
        'SectionConfigService',
        'GitConfigService' 
    ]; 
    function API(
        VerifyEmailService,
        CancelPolicyService,
        NotificationConfigurationService,
        SectionConfigService,
        GitConfigService
    ) {
        var service = {
            verifyemail: VerifyEmailService,
            cancelpolicy:CancelPolicyService,
            notifyconfiguration:NotificationConfigurationService,
            sectionconfig :SectionConfigService,
            gitconfig :GitConfigService
        };
        return service;
    }
})(angular.module('app.api', [
    'app.api.restaurant.verifyemail',
    'app.api.restaurant.cancelpolicy',
    'app.api.notification.notifyconfiguration',
    'app.api.restaurant.sectionconfig',
    'app.api.git.gitconfig',
]));