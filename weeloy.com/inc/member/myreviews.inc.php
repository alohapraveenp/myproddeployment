<?php

require_once("lib/class.member.inc.php");
require_once("lib/class.review.inc.php");
require_once("lib/class.booking.inc.php");
require_once("lib/class.media.inc.php");


$member = new WY_Member();
$bookings = new WY_Booking();
$reviews = new WY_Review();
$media = new WY_Media();
$member->getMember($_SESSION['user']['email']);
$included_data['profile_picture'] = $media->getUserProfilePicture($_SESSION['user']['email']);
$image_resto = array();
//if($_GET['f']=='post'|| !isset($_GET['f'])){
//    $bookings->getUserBookings($member->email, array('include_reviews'=>true, 'expected_reviews' => true)); 
//    $included_data['expected_reviews'] = $bookings->bookings;
//    foreach ($bookings->bookings as $book){
//        if(!isset($image_resto[$book->restaurantinfo->restaurant])){
//            $images = new WY_Media($book->restaurantinfo->restaurant);
//            $image_resto[$book->restaurantinfo->restaurant] = $images->getDefaultPicture($book->restaurantinfo->restaurant);
//        } 
//    }
//
//}else{
    $reviews->getUserReviews($member->email, array('last'=>'30','win_only'=>true));
    $included_data['conducted_reviews'] = $reviews->reviews;
    
    
    
    foreach ($reviews->reviews as $review){
        
        if(!isset($image_resto[$review->restaurant])){
            $images = new WY_Media($review->restaurant);
            $image_resto[$review->restaurant] = $images->getDefaultPicture($review->restaurant);
        } 
    }
//}

$included_data['image_resto'] = $image_resto;
$included_data['active_filter'] = $_GET['f'];