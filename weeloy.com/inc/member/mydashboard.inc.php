<?php

require_once("lib/class.member.inc.php");
require_once 'lib/class.booking.inc.php';
require_once("lib/class.review.inc.php");

$member = new WY_Member();
$last_bookings = new WY_Booking();
$last_wins = new WY_Booking();
$last_reviews = new WY_Review();

$member->getMember($_SESSION['user']['email']);
$last_bookings->getUserBookings($member->email, array('last'=>'3'));
$last_wins->getUserBookings($member->email, array('last'=>'3','win_only'=>true));
$last_reviews->getUserReviews($member->email, array('last'=>'3','win_only'=>true));


//$included_data['last_bookings'] = $last_bookings->bookings;
//$included_data['last_wins'] = $last_wins->bookings;
//$included_data['last_reviews'] = $last_reviews->reviews;
//var_dump($bookings);die;