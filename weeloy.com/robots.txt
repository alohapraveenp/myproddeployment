User-agent: *
Allow: /
Disallow: /modules/login/login.php*
Disallow: /restaurant.php*
Disallow: /index_user.php*
Disallow: /ressources/maps/*
Disallow: /restaurant/thailand/bangkok/wheel/rotation/rotation.php*
Disallow: /restaurant/*/rotation/rotation.php*	
Disallow: /modules/widget/*
Disallow: /img/upload/restaurant/*
Disallow: /img
Disallow: /img/
Disallow: /img/*
Disallow: /search/restaurant
Disallow: /500
Disallow: /info_faq
Disallow: /info_about
Disallow: /info_partner
Disallow: /search_restaurant.php*
Disallow: /index.php/component/*
Disallow: /login/login.php
Disallow: /restaurant/blog
Disallow: /restaurant/restaurant/*
Disallow: /restaurant/*/catering
Disallow: /restaurant/*/wheel-details
Disallow: /restaurant/thailand/*
Disallow: /search/restaurant/singapore
Disallow: /search/restaurant/bangkok
Disallow: /search/restaurant/hongkong
Disallow: /search_restaurant.php?*
Disallow: /restaurant/malaysia/*
Disallow: /info-*
Disallow: /ressources/*
Disallow: /modules/*

Allow: /search/restaurant/singapore
Allow: /search/restaurant/bangkok
Allow: /search/restaurant/phuket
Allow: /search/restaurant/hongkong






User-agent: *
Disallow: /blog/cgi-bin
Disallow: /blog/wp-admin
Disallow: /blog/wp-includes
Disallow: /blog/wp-content/plugins
Disallow: /blog/wp-content/cache
Disallow: /blog/wp-content/themes
Disallow: /blog/category
Disallow: /blog/tag
Disallow: /blog/author
Disallow: /blog/trackback
Disallow: /blog/*trackback
Disallow: /blog/*trackback*
Disallow: /blog/*/trackback
Disallow: /blog/*?*
Disallow: /blog/*.html/$
Disallow: /blog/*feed*

# Google Image
User-agent: Googlebot-Image
Disallow:
Allow: /blog/*

# Google AdSense
User-agent: Mediapartners-Google*
Disallow:
Allow: /blog/*

Sitemap: http://www.weeloy.com/sitemap.xml

# 
