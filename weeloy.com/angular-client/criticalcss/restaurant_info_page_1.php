<style>

body {
    background-color: #fff
}

button,
input {
    font: inherit;
    color: inherit
}

button {
    overflow: visible;
    text-transform: none
}

button::-moz-focus-inner,
input::-moz-focus-inner {
    border: 0
}


.btn,
.form-control,
body {
    line-height: 1.42857143
}

.glyphicon-user:before {
    content: "\e008"
}

.glyphicon-lock:before {
    content: "\e033"
}

*,
:after,
:before {
    box-sizing: border-box
}

html {
    font-size: 10px;
    -webkit-tap-highlight-color: transparent
}

body {
    font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
    font-size: 14px;
    color: #333
}

.img-responsive {
    display: block;
    max-width: 100%;
    height: auto
}

h1,
h2,
h3 {
    font-family: inherit;
    font-weight: 500;
    line-height: 1.1;
    color: inherit;
    margin-top: 20px
}

h2 {
    font-size: 30px
}

h3 {
    font-size: 24px
}

.btn,
.form-control {
    font-size: 14px;
    background-image: none
}

p {
    margin: 0 0 10px
}

.text-right {
    text-align: right
}

.btn,
.text-center {
    text-align: center
}

ul {
    margin-top: 0
}

ul ul {
    margin-bottom: 0
}

.container {
    padding-right: 15px;
    padding-left: 15px;
    margin-right: auto;
    margin-left: auto
}

@media (min-width:768px) {
    .container {
        width: 750px
    }
}

@media (min-width:992px) {
    .container {
        width: 970px
    }
}

@media (min-width:1200px) {
    .container {
        width: 1170px
    }
}

.nav {
    padding-left: 0;
    margin-bottom: 0;
    list-style: none
}

.nav>li,
.nav>li>a {
    display: block;
    position: relative;
    padding: 10px 15px
}


.nav>li>a>img {
    max-width: none
}

.navbar {
    position: relative;
    min-height: 50px;
    margin-bottom: 20px;
    border: 1px solid transparent
}

.navbar-collapse {
    padding-right: 15px;
    padding-left: 15px;
    overflow-x: visible;
    -webkit-overflow-scrolling: touch;
    border-top: 1px solid transparent;
    -webkit-box-shadow: inset 0 1px 0 rgba(255, 255, 255, .1);
    box-shadow: inset 0 1px 0 rgba(255, 255, 255, .1)
}

.navbar-fixed-top .navbar-collapse {
    max-height: 340px
}

@media (max-device-width:480px) and (orientation:landscape) {
    .navbar-fixed-top .navbar-collapse {
        max-height: 200px
    }
}

.container>.navbar-collapse,
.container>.navbar-header {
    margin-right: -15px;
    margin-left: -15px
}

@media (min-width:768px) {
    .navbar {
        border-radius: 4px
    }
    .navbar-header {
        float: left
    }
    .navbar-collapse {
        width: auto;
        border-top: 0;
        -webkit-box-shadow: none;
        box-shadow: none
    }
    .navbar-collapse.collapse {
        display: block!important;
        height: auto!important;
        padding-bottom: 0;
        overflow: visible!important
    }
    .navbar-fixed-top .navbar-collapse {
        padding-right: 0;
        padding-left: 0
    }
    .container>.navbar-collapse,
    .container>.navbar-header {
        margin-right: 0;
        margin-left: 0
    }
    .navbar-fixed-top {
        border-radius: 0
    }
}

.navbar-fixed-top {
    position: fixed;
    right: 0;
    left: 0;
    z-index: 1030;
    top: 0;
    border-width: 0 0 1px
}

.navbar-brand {
    float: left;
    height: 50px;
    padding: 15px;
    font-size: 18px;
    line-height: 20px
}

.navbar-brand>img {
    display: block
}

.navbar-toggle {
    position: relative;
    float: right;
    padding: 9px 10px;
    margin-top: 8px;
    margin-right: 15px;
    margin-bottom: 8px;
    background-color: transparent;
    background-image: none;
    border: 1px solid transparent;
    border-radius: 4px
}

.navbar-toggle .icon-bar {
    display: block;
    width: 22px;
    height: 2px;
    border-radius: 1px
}

.navbar-toggle .icon-bar+.icon-bar {
    margin-top: 4px
}

@media (min-width:768px) {
    .navbar>.container .navbar-brand {
        margin-left: -15px
    }
    .navbar-toggle {
        display: none
    }
}

.navbar-nav {
    margin: 7.5px -15px
}

.navbar-nav>li>a {
    padding-top: 10px;
    padding-bottom: 10px;
    line-height: 20px
}

@media (min-width:768px) {
    .navbar-nav {
        float: left;
        margin: 0
    }
    .navbar-nav>li {
        float: left
    }
    .navbar-nav>li>a {
        padding-top: 15px;
        padding-bottom: 15px
    }
    .navbar-right {
        float: right!important;
        margin-right: -15px
    }
    .navbar-right~.navbar-right {
        margin-right: 0
    }
}

.navbar-default {
    background-color: #f8f8f8;
    border-color: #e7e7e7
}

.navbar-default .navbar-brand,
.navbar-default .navbar-nav>li>a {
    color: #777
}

.navbar-default .navbar-toggle {
    border-color: #ddd
}

.navbar-default .navbar-toggle .icon-bar {
    background-color: #888
}

.modal-content,
.panel {
    background-color: #fff
}

.navbar-default .navbar-collapse {
    border-color: #e7e7e7
}

.panel {
    margin-bottom: 20px;
    border: 1px solid transparent;
    border-radius: 4px
}

.panel-body {
    padding: 15px
}

.panel-info {
    border-color: #bce8f1
}

.modal {
    position: fixed;
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;
    z-index: 1050;
    display: none;
    overflow: hidden;
    -webkit-overflow-scrolling: touch;
    outline: 0
}

.modal.fade .modal-dialog {
    -webkit-transition: -webkit-transform .3s ease-out;
    -o-transition: -o-transform .3s ease-out;
    transition: transform .3s ease-out;
    -webkit-transform: translate(0, -25%);
    -ms-transform: translate(0, -25%);
    -o-transform: translate(0, -25%);
    transform: translate(0, -25%)
}

.modal-dialog {
    position: relative;
    width: auto;
    margin: 10px
}

.modal-content {
    position: relative;
    -webkit-background-clip: padding-box;
    background-clip: padding-box;
    border: 1px solid #999;
    border: 1px solid rgba(0, 0, 0, .2);
    border-radius: 6px;
    outline: 0;
    -webkit-box-shadow: 0 3px 9px rgba(0, 0, 0, .5);
    box-shadow: 0 3px 9px rgba(0, 0, 0, .5)
}

@media (min-width:768px) {
    .modal-dialog {
        width: 600px;
        margin: 30px auto
    }
    .modal-content {
        -webkit-box-shadow: 0 5px 15px rgba(0, 0, 0, .5);
        box-shadow: 0 5px 15px rgba(0, 0, 0, .5)
    }
}

.clearfix:after,
.clearfix:before,
.container:after,
.container:before,
.form-horizontal .form-group:after,
.form-horizontal .form-group:before,
.nav:after,
.nav:before,
.navbar-collapse:after,
.navbar-collapse:before,
.navbar-header:after,
.navbar-header:before,
.navbar:after,
.navbar:before,
.panel-body:after,
.panel-body:before {
    display: table;
    content: " "
}

.clearfix:after,
.container:after,
.form-horizontal .form-group:after,
.nav:after,
.navbar-collapse:after,
.navbar-header:after,
.navbar:after,
.panel-body:after {
    clear: both
}

.pull-right {
    float: right!important
}

.pull-left {
    float: left!important
}


@font-face {
    font-family: FontAwesome;
    src: url(https://static.weeloy.com/fonts/fontawesome-webfont.eot?v=4.4.0);
    src: url(https://static.weeloy.com/fonts/fontawesome-webfont.eot?#iefix&v=4.4.0) format('embedded-opentype'), url(https://static.weeloy.com/fonts/fontawesome-webfont.woff2?v=4.4.0) format('woff2'), url(https://static.weeloy.com/fonts/fontawesome-webfont.woff?v=4.4.0) format('woff'), url(https://static.weeloy.com/fonts/fontawesome-webfont.ttf?v=4.4.0) format('truetype'), url(https://static.weeloy.com/fonts/fontawesome-webfont.svg?v=4.4.0#fontawesomeregular) format('svg');
    font-weight: 400;
    font-style: normal
}

.fa {
    display: inline-block;
    font: normal normal normal 14px/1 FontAwesome;
    font-size: inherit;
    text-rendering: auto;
    -webkit-font-smoothing: antialiased
}

.fa-close:before {
    content: "\f00d"
}

.fa-shopping-cart:before {
    content: "\f07a"
}

.fa-facebook:before {
    content: "\f09a"
}

.sr-only {
    position: absolute;
    width: 1px;
    height: 1px;
    margin: -1px;
    padding: 0;
    overflow: hidden;
    clip: rect(0, 0, 0, 0);
    border: 0
}

header,
nav {
    display: block
}

html {
    font-family: sans-serif;
    -ms-text-size-adjust: 100%;
    -webkit-text-size-adjust: 100%
}

body {
    margin: 0
}

h1 {
    font-size: 2em;
    margin: .67em 0
}

b {
    font-weight: 700
}

img {
    vertical-align: middle;
    border: 0
}

button,
input {
    font-family: inherit;
    font-size: 100%;
    margin: 0;
    line-height: normal
}

button,
input[type=submit] {
    -webkit-appearance: button;
    cursor: pointer
}

input[type=checkbox] {
    box-sizing: border-box;
    padding: 0
}

button::-moz-focus-inner,
input::-moz-focus-inner {
    padding: 0
}


.wrapper {
    min-height: 600px
}



header .navbar-default {
    border: none;
    border-radius: 0;
    box-shadow: none;
    min-height: 0;
    background-color: rgba(39, 103, 136, .8);
    background-image: none;
    margin-bottom: 0
}

header .navbar-default .navbar-header .navbar-toggle {
    margin-top: 3px!important;
    margin-bottom: 3px!important;
    background: #1fa2e1!important;
    border: 1px solid transparent!important
}

header .navbar-default .navbar-header .navbar-toggle .icon-bar {
    background-color: #fff
}

header .navbar-default .navbar-header .navbar-brand {
    padding: 4px 0;
    margin-left: 0!important
}

header .navbar-default .navbar-header .navbar-brand img {
    max-height: 100%
}

header .navbar-default .navbar-nav li a {
    color: #fff;
    font-family: proximanova;
    font-size: 14px
}

header .navbar-default .navbar-right {
    margin-right: 10px
}

header .navbar-default .loggedin-menu li {
    z-index: 1
}

header .navbar-default .loggedin-menu li a {
    padding: 2px
}

header .navbar-default .loggedin-menu li a img {
    max-height: 95%
}










header .navbar-default .navbar-brand,
header .navbar-default .navbar-nav li a {
    line-height: 40px;
    height: 40px;
    padding-top: 0
}





.restaurant-info-page {
    font-family: proximanova;
    background: #F5F5F5
}

.restaurant-info-page .btn {
    background-image: none;
    border-radius: 2px
}

.restaurant-info-page .cart-message {
    margin-top: 20px;
    padding-top: 10px;
    border-top: 1px solid #eee
}

.restaurant-info-page .cart-message a {
    font-size: 12px
}

.restaurant-info-page .cart-message .fa {
    font-size: 24px;
    color: #8899a6
}

.restaurant-info-page .custom_button {
    background-color: #ddd;
    background-image: linear-gradient(to bottom, #2aa8cf, #2aa8cf);
    border-radius: 0;
    color: #fff;
    padding: 3px 31px;
    text-transform: uppercase
}

.restaurant-info-page .banner_restaurant {
    display: block;
    height: 522px
}

.restaurant-info-page .trans-block {
    min-height: 522px;
    position: relative;
    display: block;
    padding: 40px 0 0;
    text-align: center;
    background-color: rgba(255, 255, 255, .5)
}

.restaurant-info-page .trans-block h1 {
    display: block;
    font-family: proximanova-semibold;
    font-size: 30px;
    color: #222;
    margin: 0;
    padding-bottom: 15px;
    padding-top: 15px
}

.restaurant-info-page .trans-block p {
    display: block;
    padding-bottom: 36px;
    font-size: 16px;
    color: #222;
    margin-bottom: 0
}

.restaurant-info-page .div-name-address {
    background: rgba(255, 255, 255, .75);
    height: 220px
}

.restaurant-info-page .bar-logo {
    display: block;
    padding-top: 15px;
    padding-bottom: 15px;
    height: 150px
}

.restaurant-info-page .bar-logo img {
    margin: 0 auto;
    max-height: 100%
}

.restaurant-info-page .circle-img {
    display: block;
    position: absolute;
    margin-left: auto;
    margin-right: auto;
    left: 0;
    right: 0;
    bottom: -56px;
    z-index: 1
}

.restaurant-info-page .circle-img p {
    background: 0 0
}

.restaurant-info-page .panel-body {
    background-color: #fff
}

.restaurant-info-page .panel-body ul {
    padding-left: 0;
    list-style: none;
    margin-bottom: 0
}

.restaurant-info-page .panel-body ul li .menu-item-header {
    overflow: auto
}

.restaurant-info-page .panel-body ul li .menu-item-header div:first-child {
    margin-bottom: 0
}

.restaurant-info-page .panel-body ul li .menu-item-header div:first-child span:last-child {
    font-family: proximanova;
    font-size: 13px;
    color: #000;
    float: right
}

.restaurant-info-page .panel-body ul li .menu-item-header div:first-child .menu-title {
    font-family: proximanova-semibold!important;
    font-size: 14px;
    color: #000;
    margin-bottom: 0;
    margin-right: 5px
}

.restaurant-info-page .panel-body ul li .menu-item-header .price-and-cart {
    color: #333;
    font-size: 12px;
    overflow: hidden
}

</style>