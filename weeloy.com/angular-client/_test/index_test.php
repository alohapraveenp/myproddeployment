<?php

include __DIR__ . '/includes/header.php';
?>

<!DOCTYPE html>
<html lang="en" ng-app="WeeloyApp" ng-controller="RootCtrl">

    <head>
        <meta charset="UTF-8">
        <title ng-bind="title">
            <?php echo $title; ?>
        </title>
        <meta name="fragment" content="!"> 
        <meta name="description" content="<?php echo $description; ?>" />
        <meta property="og:title" content="<?php echo $FacebookMetaTitle; ?>" />
        <meta property="og:type" content="website" />
        <meta property="og:image" content="<?php echo $header_image ?>" />
        <meta property="og:description" content="<?php echo $FacebookMetaDescription; ?>" />
        <meta property="og:locale" content="en_US" />
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="owner" content="weeloy.com">
        <meta name="apple-itunes-app" content="app-id=973030193">
        <base href="<?php echo __ROOTDIR__; ?>/">
        <meta name="google-site-verification" content="iHu3Km_-ufs5DlCTZxayDBqSxOG8p2u26vWFqCfTY98" />
        <meta name="msvalidate.01" content="90FBBB685EF1AC19990BADECF586BE25" />
        <link rel="shortcut icon" href="favicon.ico" title="favoris icone">

        <script type="text/javascript">

<?php
if (isset($_SESSION['user']['email'])) {
    $loggedin = 'true';
    $user = $_SESSION['user'];
} else {
    $loggedin = 'false';
    $user = null;
}

if (isset($_SESSION['cart']) && isset($_SESSION['cart']['items'])) {
    $cart = $_SESSION['cart']['items'];
} else {
    $cart = [];
}

if (isset(parse_url(__BASE_URL__)['path'])) {
    $path = parse_url(__BASE_URL__)['path'];
} else {
    $path = '/';
}




?>
                            var BASE_URL = "<?php echo __BASE_URL__; ?>";
                            var BASE_PATH = "<?php echo $path; ?>";
                            var FB_ID = '<?php echo BOOKING_APP_ID; ?>';
                            var FB_WEBSITE_ID = '<?php echo WEBSITE_FB_APP_ID; ?>';
                            var loggedin = <?php echo $loggedin; ?>;
                            var user = <?php echo ($user != null) ? json_encode($user) : 'null'; ?>;
                            var UserSession = <?php echo isset($_SESSION['user']) ? json_encode($_SESSION['user']) : 'null'; ?>;
                            if (UserSession.search_city == undefined) {
                                UserSession.search_city = 'Singapore';
                            }
                            var cart = <?php echo json_encode($cart); ?>;
        </script>
        <?php

            switch ($page) {
                case 'home_page':
                    include __DIR__ . '/criticalcss/home_page.php';
                    break;
                case 'restaurant_info_page':
                    //include __DIR__ . '/criticalcss/restaurant_info_page.php';
                    break;
                case 'search_page':
                    //include __DIR__ . '/criticalcss/search_page.php';
                    break;
                default:
                    //echo '<link rel="stylesheet" href="client/assets/css/app.min.css">';
                    break;
            }
        
        ?>
        
        
 <script>
    // include angular loader, which allows the files to load in any order
    //@@NG_LOADER_START@@
/*
 AngularJS v1.4.9
 (c) 2010-2015 Google, Inc. http://angularjs.org
 License: MIT
*/
(function(){'use strict';function d(b){return function(){var a=arguments[0],e;e="["+(b?b+":":"")+a+"] http://errors.angularjs.org/1.4.9/"+(b?b+"/":"")+a;for(a=1;a<arguments.length;a++){e=e+(1==a?"?":"&")+"p"+(a-1)+"=";var d=encodeURIComponent,c;c=arguments[a];c="function"==typeof c?c.toString().replace(/ \{[\s\S]*$/,""):"undefined"==typeof c?"undefined":"string"!=typeof c?JSON.stringify(c):c;e+=d(c)}return Error(e)}}(function(b){function a(c,a,b){return c[a]||(c[a]=b())}var e=d("$injector"),n=d("ng");
b=a(b,"angular",Object);b.$minErr=b.$minErr||d;return a(b,"module",function(){var c={};return function(b,d,h){if("hasOwnProperty"===b)throw n("badname","module");d&&c.hasOwnProperty(b)&&(c[b]=null);return a(c,b,function(){function c(a,b,d,e){e||(e=f);return function(){e[d||"push"]([a,b,arguments]);return g}}function a(c,e){return function(a,d){d&&"function"===typeof d&&(d.$moduleName=b);f.push([c,e,arguments]);return g}}if(!d)throw e("nomod",b);var f=[],k=[],l=[],m=c("$injector","invoke","push",
k),g={_invokeQueue:f,_configBlocks:k,_runBlocks:l,requires:d,name:b,provider:a("$provide","provider"),factory:a("$provide","factory"),service:a("$provide","service"),value:c("$provide","value"),constant:c("$provide","constant","unshift"),decorator:a("$provide","decorator"),animation:a("$animateProvider","register"),filter:a("$filterProvider","register"),controller:a("$controllerProvider","register"),directive:a("$compileProvider","directive"),config:m,run:function(a){l.push(a);return this}};h&&m(h);
return g})}})})(window)})(window);
//# sourceMappingURL=client/bower_components/angular-loader/angular-loader.min.js.map

//@@NG_LOADER_END@@

    // include a third-party async loader library
    /*!
     * $script.js v1.3
     * https://github.com/ded/script.js
     * Copyright: @ded & @fat - Dustin Diaz, Jacob Thornton 2011
     * Follow our software http://twitter.com/dedfat
     * License: MIT
     */
    !function(a,b,c){function t(a,c){var e=b.createElement("script"),f=j;e.onload=e.onerror=e[o]=function(){e[m]&&!/^c|loade/.test(e[m])||f||(e.onload=e[o]=null,f=1,c())},e.async=1,e.src=a,d.insertBefore(e,d.firstChild)}function q(a,b){p(a,function(a){return!b(a)})}var d=b.getElementsByTagName("head")[0],e={},f={},g={},h={},i="string",j=!1,k="push",l="DOMContentLoaded",m="readyState",n="addEventListener",o="onreadystatechange",p=function(a,b){for(var c=0,d=a.length;c<d;++c)if(!b(a[c]))return j;return 1};!b[m]&&b[n]&&(b[n](l,function r(){b.removeEventListener(l,r,j),b[m]="complete"},j),b[m]="loading");var s=function(a,b,d){function o(){if(!--m){e[l]=1,j&&j();for(var a in g)p(a.split("|"),n)&&!q(g[a],n)&&(g[a]=[])}}function n(a){return a.call?a():e[a]}a=a[k]?a:[a];var i=b&&b.call,j=i?b:d,l=i?a.join(""):b,m=a.length;c(function(){q(a,function(a){h[a]?(l&&(f[l]=1),o()):(h[a]=1,l&&(f[l]=1),t(s.path?s.path+a+".js":a,o))})},0);return s};s.get=t,s.ready=function(a,b,c){a=a[k]?a:[a];var d=[];!q(a,function(a){e[a]||d[k](a)})&&p(a,function(a){return e[a]})?b():!function(a){g[a]=g[a]||[],g[a][k](b),c&&c(d)}(a.join("|"));return s};var u=a.$script;s.noConflict=function(){a.$script=u;return this},typeof module!="undefined"&&module.exports?module.exports=s:a.$script=s}(this,document,setTimeout)

    // load all of the dependencies asynchronously.
    $script(['client/weeloy.min.js'
    ], function() {
      // when all is done, execute bootstrap angular application
      //angular.bootstrap(document, ['myApp']);
      console.log('dsa');
    });
  </script>
  
   
    <script>
  // include loadCSS here...
(function(w){
	"use strict";
	/* exported loadCSS */
	var loadCSS = function( href, before, media ){
		// Arguments explained:
		// `href` [REQUIRED] is the URL for your CSS file.
		// `before` [OPTIONAL] is the element the script should use as a reference for injecting our stylesheet <link> before
			// By default, loadCSS attempts to inject the link after the last stylesheet or script in the DOM. However, you might desire a more specific location in your document.
		// `media` [OPTIONAL] is the media type or query of the stylesheet. By default it will be 'all'
		var doc = w.document;
		var ss = doc.createElement( "link" );
		var newMedia = media || "all";
		var ref;
		if( before ){
			ref = before;
		}
		else {
			var refs = ( doc.body || doc.getElementsByTagName( "head" )[ 0 ] ).childNodes;
			ref = refs[ refs.length - 1];
		}

		var sheets = doc.styleSheets;
		ss.rel = "stylesheet";
		ss.href = href;
		// temporarily set media to something inapplicable to ensure it'll fetch without blocking render
		ss.media = "only x";

		// wait until body is defined before injecting link. This ensures a non-blocking load in IE11.
		function ready( cb ){
			if( doc.body ){
				return cb();
			}
			setTimeout(function(){
				ready( cb );
			});
		}
		// Inject link
			// Note: the ternary preserves the existing behavior of "before" argument, but we could choose to change the argument to "after" in a later release and standardize on ref.nextSibling for all refs
			// Note: `insertBefore` is used instead of `appendChild`, for safety re: http://www.paulirish.com/2011/surefire-dom-element-insertion/
		ready( function(){
			ref.parentNode.insertBefore( ss, ( before ? ref : ref.nextSibling ) );
		});
		// A method (exposed on return object for external use) that mimics onload by polling until document.styleSheets until it includes the new sheet.
		var onloadcssdefined = function( cb ){
			var resolvedHref = ss.href;
			var i = sheets.length;
			while( i-- ){
				if( sheets[ i ].href === resolvedHref ){
					return cb();
				}
			}
			setTimeout(function() {
				onloadcssdefined( cb );
			});
		};

		// once loaded, set link's media back to `all` so that the stylesheet applies once it loads
		if( ss.addEventListener ){
			ss.addEventListener( "load", function(){
				this.media = newMedia;
			});
		}
		ss.onloadcssdefined = onloadcssdefined;
		onloadcssdefined(function() {
			if( ss.media !== newMedia ){
				ss.media = newMedia;
			}
		});
		return ss;
	};
	// commonjs
	if( typeof exports !== "undefined" ){
		exports.loadCSS = loadCSS;
	}
	else {
		w.loadCSS = loadCSS;
	}
}( typeof global !== "undefined" ? global : this ));
  // load a file
  loadCSS( "client/assets/css/app.min.css" );
</script>
<noscript><link href="https://static.weeloy.com/css/app.min.css" rel="stylesheet"></noscript>
        
        
    </head>

    <body>

        <header <?php
        if ($no_header == true) {
            echo "style='display:none;'";
        }
        ?>>
            <nav class="navbar navbar-default navbar-fixed-top">
                <div class="container">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="<?php echo __BASE_URL__; ?>" title="Reserve restaurant with weeloy">
                            <img src="https://static3.weeloy.com/images/logo/logo_w_t_small.png" alt="Weeloy restaurant reservation"></a>
                    </div>
                    
                    <div ng-if="true" ng-include="'../app/shared/partial/_menu.tpl.html'"></div>

                </div>
            </nav>
            <div class="modal fade" id="loginModal" tabindex="-1" role="dialog" aria-labelledby="myLoginModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-content">
                            <!-- Include meta tag to ensure proper rendering and touch zooming -->
                            <meta name="viewport" content="width=device-width, initial-scale=1">
                            <!-- Include bootstrap stylesheets -->
                            <div class="container_modal">
                                <div class="mainbox">
                                    <div class="panel panel-info">
                                        <div class="panel-body">
                                            <h1 id="title" ></h1>
                                            <div class="loginbox">
                                                <div id="facebook" class="login-form">
                                                    <a class="btn btn-lg btn-social btn-facebook" id="loginBtn" ng-click="LoginWithFacebook()" title="Log in using your Facebook account">
                                                        <i class="fa fa-facebook"></i> {{Str.template.LoginModalLoginWithFacebook}}
                                                    </a>
                                                    <div id="status_fb"></div>
                                                </div>
                                                <div class="or-spacer">
                                                    <div class="mask"></div>
                                                    <span><i>or</i></span>
                                                </div>
                                            </div>
                                            <!-- LoginForm -->
                                            <div ng-if="showForm == 'loginForm'" ng-include="'../app/shared/partial/_login_form.tpl.html'"></div>
                                            <!-- LoginForm -->
                                            <!-- SignUpForm -->
                                            <div ng-if="showForm == 'signupForm'" ng-include="'../app/shared/partial/_signup_form.tpl.html'"></div>
                                            <!-- SignUpForm -->
                                            <!-- Forgot Password Form -->
                                            <div ng-if="showForm == 'forgotPasswordForm'" ng-include="'../app/shared/partial/_forgot_password_form.tpl.html'"></div>
                                            <!-- Forgot Password Form -->
                                            <div style="margin-bottom: 20px;" class="form-inline logmenu">
                                                <p ng-show="showForm == 'loginForm' || showForm == 'signupFrom'">
                                                    <a href="#" ng-click="showForm = 'forgotPasswordForm'" style="color: #333;font-family: proximanova;font-size: 16px;line-height: 16px;" ng-bind="Str.template.TextForgotPassword"></a>
                                                </p>
                                                <p ng-show="showForm != 'loginForm'">
                                                    <a href="#" ng-click="showForm = 'loginForm'"  style="color: #333;font-family: proximanova;font-size: 16px;line-height: 16px;"  ng-bind="TextYouPreferToLogin"></a>
                                                </p>
                                            </div>
                                            <div id="registerTag" ng-show="showForm != 'signupForm'">
                                                <div style="border-top: 1px solid#888; padding-top:15px; font-size:85%">
                                                    {{Str.template.TextDontHaveAccount}} <a href="#" ng-click="showForm = 'signupForm'" id="register"> {{Str.template.TextRegisterHere}} </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-2"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <div class="wrapper">
            <div ng-if="true" ng-include="'../app/shared/partial/_message_wrapper.tpl.html'"></div>
            <div ng-view>
                
                
                <?php
                switch ($page) {
                    case 'home_page':
                        //include __DIR__ . '/includes/home_page.php';
                        break;
                    case 'search_page':
                        //include __DIR__ . '/includes/search_page.php';
                        break;
                    case 'restaurant_info_page':
                        //include __DIR__ . '/includes/restaurant_info_page.php';
                        break;
                    default:
                        break;
                }
                ?>
                
            </div>
        </div>
        <footer <?php
                if ($no_footer == true) {
                    echo "style='display:none;'";
                }
                ?>>
            <div class="follow-us">
                <div class="container">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <h3 ng-bind="Str.template.FindUs"></h3>
                        <ul>
                            <li><a rel="nofollow" target="_blank" href="https://www.facebook.com/weeloy.sg" id='social-facebook' title="follow weeloy on facebook"><span class="fa fa-facebook"></span></a></li>
                            <li><a rel="nofollow" target="_blank" href="https://twitter.com/weeloyasia" id='social-twitter' title="follow weeloy on twitter"><span class="fa fa-twitter"></span></a></li>
<!--                            <li><a rel="nofollow" target="_blank" href="https://www.linkedin.com/company/weeloy-pte-ltd" id='social-linkedin' title="follow weeloy on linkedin"><span class="fa fa-linkedin"></span></a></li>-->
                            <li><a rel="nofollow" target="_blank" href="https://www.instagram.com/weeloysingapore/" id='social-linkedin' title="follow weeloy on instagram"><span class="fa fa-instagram"></span></a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="footer-bg">
                <div class="container">
                    <div>
                        <h3 ng-bind="Str.template.FooterInformation"></h3>
                        <ul class="footer-col-content">
                            <li>
                                <a href="blog"  title="food blog by weeloy"  onclick="document.location.href = 'https://www.weeloy.com/blog';
                                                        return false;" style="cursor: pointer" ng-bind="Str.template.FooterInformationBlog"></a>
                            </li>
                            <li>
                                <a href="how-it-works"  title="weeloy concept how it works" ng-bind="Str.template.FooterInformationHowItWorks"></a>
                            </li>
                            <li>
                                <a href="contact" title="weeloy contact page" ng-bind="Str.template.FooterInformationContactUs"></a>
                            </li>
                            <li>
                                <a href="partner"  title="weeloy partner page" ng-bind="Str.template.FooterInformationPartner"></a>
                            </li>
                            <li>
                                <a href="faq" title="faq weeloy" ng-bind="Str.tempate.FooterInformationFaq"></a>
                            </li>
                            <li>
                                <a target="_blank" href="terms-and-conditions-of-service" ng-bind="Str.template.FooterInformationTermOfService"></a>
                            </li>
                            <li>
                                <a target="_blank" href="privacy-policy" ng-bind="Str.template.FooterInformationPrivacyPolicy"></a>
                            </li>
                        </ul>
                    </div>
                    <div>
                        <h3 ng-bind="Str.template.FooterOurLocation"></h3>
                        <ul class="footer-col-content">
                            <li><a href="search/singapore" title="find and reserve restaurant in singapore">Singapore</a></li>
                            <li><a href="search/bangkok" title="find and reserve restaurant in bangkok">Bangkok</a></li>
                            <li><a href="search/phuket" title="find and reserve restaurant in phuket">Phuket</a></li>
                            <li><a href="search/hong%20kong" title="find and reserve restaurant in hong-kong">Hong Kong</a></li>
                        </ul>
                    </div>
                    <div>
                        <h3 ng-bind="Str.template.FooterMobile"></h3>
                        <div class="footer-col-content">
                            <p>
                                <a class="mobile-picture-div" rel="nofollow" target="_blank" href="http://itunes.apple.com/app/id973030193" title="Download Weeloy iOS app" >
                                    <img width="196" height="60" src="https://static2.weeloy.com/images/home_picture/app_store/app-store-badge_en.png" alt="Available on the App Store">
                                </a>
                            </p>
                            <p>
                                <a class="mobile-picture-div" rel="nofollow" target="_blank" href="https://play.google.com/store/apps/details?id=com.weeloy.client" title="Download Weeloy Android app">
                                    <img width="196" height="60" src="https://static2.weeloy.com/images/home_picture/app_store/play-store-badge_en.png" alt="Get it on Google Play">
                                </a>
                            </p>
                        </div>
                    </div>
                    <div>
                        <h3 ng-bind="Str.template.FooterNewsLetter"></h3>
                        <div class="footer-col-content">
                            <form method="POST" ng-submit="SubmitNewletter(newletter_email)" id="newsletterform" name="newsletterform" novalidate>
                                <input class="newsletter_email" id="email" name="newletter_email" ng-model="newletter_email" type="email" placeholder="{{Str.template.FooterNewsLetterPlaceHolder}}" required = "true">
                                <button id="submit-follow" ng-bind="Str.template.FooterNewsLetterBtn"></button>

                            </form>
                        </div>
                       
                    </div>
                </div>

                <div class="copyright">
                    <p class="text-center" ng-bind="Str.template.FooterCopyright"></p>
                    <p class="text-center">
                        <a href="http://www.instantssl.com/wildcard-ssl.html" title="Secured reservation system by Weeloy">
                            <img src="https://static2.weeloy.com/images/footer/comodo_secure_113x59_transp.png" alt="Secured reservation system by Weeloy - Wildcard SSL" width="113" height="59" style="border: 0px;">
                        </a>
                    </p>
                </div>
            </div>
        </footer>
        


        <?php
        
        
//        if (strpos(__BASE_URL__, 'localhost') !== false) {
//            echo '<script src="client/weeloy.js" defer></script>';
//        } else {
//            echo '<script src="client/weeloy.min.js" defer></script>';
//        }
        ?>   
        <!-- Google Tag Manager -->
        <noscript>
        <iframe src="//www.googletagmanager.com/ns.html?id=GTM-TQZ2W7" height="0" width="0" style="display:none;visibility:hidden"></iframe>
        </noscript>
        <script>
                            (function (w, d, s, l, i) {
                                w[l] = w[l] || [];
                                w[l].push({
                                    'gtm.start': new Date().getTime(),
                                    event: 'gtm.js'
                                });
                                var f = d.getElementsByTagName(s)[0],
                                        j = d.createElement(s),
                                        dl = l != 'dataLayer' ? '&l=' + l : '';
                                j.async = true;
                                j.src =
                                        '//www.googletagmanager.com/gtm.js?id=' + i + dl;
                                f.parentNode.insertBefore(j, f);
                            })(window, document, 'script', 'dataLayer', 'GTM-TQZ2W7');
        </script>
        <!-- End Google Tag Manager -->     

    </body>

</html>
