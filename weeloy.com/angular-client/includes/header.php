<?php
header("Access-Control-Allow-Origin: https://staticxx.facebook.com");

    
require_once "conf/conf.init.inc.php";
require_once "lib/wpdo.inc.php";
require_once "conf/conf.session.inc.php";
require_once "lib/class.media.inc.php";
require_once "lib/class.debug.inc.php";

$app_version = '1.74';

$bkIP = WY_debug::getIP("ERROR", "REDIRECT", "Invalid reques 8" . $_SERVER['REQUEST_URI'] . ", ip=" . $bkIP);

$page         = '';
$header_image = '';
if ($_SERVER['REQUEST_URI'] == '/') {
    $page = 'home_page';
}
$is_reward      = false;
$is_validurl    = true;
$is_validResurl = true;

parse_str($_SERVER['QUERY_STRING']);

//accept lower only
if ($_SERVER['QUERY_STRING'] !== '' || parse_url($_SERVER['REQUEST_URI'], PHP_URL_QUERY) != '') {

} else if ($_SERVER['REQUEST_URI'] !== strtolower($_SERVER['REQUEST_URI'])) {

    //WY_debug::recordDebug("ERROR", "REDIRECT", "Invalid reques 1" . $_SERVER['REQUEST_URI'] . ", ip=" . $bkIP);
    header('HTTP/1.1 301 Moved Permanently');
    //header("HTTP/1.0 404 Not Found");
    //include __DIR__ . '/404.php';
    header("Location: ".strtolower($_SERVER['REQUEST_URI']));
    exit();

}
    
if(is_wrong_url($_SERVER['REQUEST_URI'])){
    //WY_debug::recordDebug("ERROR", "REDIRECT", "Invalid reques 2" . $_SERVER['REQUEST_URI'] . ", ip=" . $bkIP);
    header("HTTP/1.0 404 Not Found");
    include __DIR__ . '/404.php';
    exit();
}
    
    
//for localhost
if ($_SERVER['REQUEST_URI'] == '/weeloy.com/') {
    $page = 'home_page';
}

if (preg_match('/\/search(.*)/i', $_SERVER['REQUEST_URI'])) {
    $page = 'search_page';
}
if (preg_match('/\/restaurant(.*)/i', $_SERVER['REQUEST_URI'])) {
    $page = 'restaurant_info_page';


    
    if (preg_match('/\/restaurant\/([a-z-]+)\/([a-z0-9-]+)/', $_SERVER['REQUEST_URI'], $matches)) {
        $city            = $matches[1];
        $restaurant_name = $matches[2];
    }

    if (preg_match('/\/restaurant\/([a-z-]+)\/([a-z0-9-]+)\/dining-rewards/', $_SERVER['REQUEST_URI'], $matches)) {

        $is_reward       = true;
        $city            = $matches[1];
        $restaurant_name = $matches[2];
        $s               = explode("/", $_SERVER['REQUEST_URI']);

        if (strstr($_SERVER['HTTP_HOST'], 'localhost')) {
            if (isset($s[6]) && $s[6] != "") {
                $is_validurl = false;
            }
        } else {
            if (isset($s[5]) && $s[5] != "") {
                $is_validurl = false;
            }
        }

    }
    if (preg_match('/\/restaurant\/([a-z-]+)\/([a-z-]+)\/([a-z0-9-]+)\/dining-rewards/', $_SERVER['REQUEST_URI'], $matches)) {

        $is_reward       = true;
        $city            = $matches[2];
        $restaurant_name = $matches[3];
        $s               = explode("/", $_SERVER['REQUEST_URI']);
        if (strstr($_SERVER['HTTP_HOST'], 'localhost')) {
            if (isset($s[7]) && $s[7] != "") {
                $is_validurl = false;
            }
        } else {
            if (isset($s[6]) && $s[6] != "") {
                $is_validurl = false;
            }
        }

    }

    switch ($city) {
        case 'bangkok':
            $country = 'thailand';
            break;
        case 'phuket':
            $country = 'thailand';
            break;
        case 'singapore':
            $country = 'singapore';
            break;
        case 'hong-kong':
            $country = 'hong-kong';
            break;
        case 'paris':
            $country = 'france';
            break;
        case 'seoul':
            $country = 'korea';
            break;
        case 'mumbai':
            $country = 'india';
            break;
        default:
            $country = 'singapore';
            break;
    }
    $RestaurantID = '';
    switch ($country) {
        case 'singapore':
            $RestaurantID .= 'SG';
            break;
        case 'hong-kong':
            $RestaurantID .= 'HK';
            break;
        case 'thailand':
            $RestaurantID .= 'TH';
            break;
        case 'malaysia':
            $RestaurantID .= 'MY';
            break;
        case 'france':
            $RestaurantID .= 'FR';
            break;
        case 'korea':
            $RestaurantID .= 'KR';
            break;
        case 'india':
            $RestaurantID .= 'IN';
            break;
        default:
            $RestaurantID .= 'SG';
            break;
    }

    $RestaurantID .= '_';

    switch ($city) {
        case 'singapore':
            $RestaurantID .= 'SG';
            break;
        case 'hong-kong':
            $RestaurantID .= 'HK';
            break;
        case 'bangkok':
            $RestaurantID .= 'BK';
            break;
        case 'phuket':
            $RestaurantID .= 'PK';
            break;
        case 'kuala-lumpur':
            $RestaurantID .= 'KL';
            break;
        case 'paris':
            $RestaurantID .= 'PR';
            break;
        case 'seoul':
            $RestaurantID .= 'SE';
            break;
        case 'mumbai':
            $RestaurantID .= 'MU';
            break;
        default:
            $RestaurantID .= 'SG';
            break;
    }

    
    
    $RestaurantID .= '_R_';
    $restaurant_name = ucfirst($restaurant_name);
    $restaurant_name = preg_replace_callback('/-[a-z]/', function ($m) {return strtoupper($m[0]);}, $restaurant_name);
    $restaurant_name = str_replace('-', '', $restaurant_name);
    $RestaurantID .= $restaurant_name;
    $db = getConnection();

    if ($is_reward === true) {
//        if ($is_validurl === false) {
//            header("HTTP/1.0 404 Not Found");
//            include __DIR__ . '/404.php';
//            exit();
//        }
        $sql  = "SELECT title, restaurant, is_displayed, status FROM restaurant WHERE restaurant='{$RestaurantID}' AND is_wheelable =1 limit 1";
        $stmt = $db->prepare($sql);
        $stmt->bindParam("query", $query);
        $stmt->execute();
        $restaurant = $stmt->fetch(PDO::FETCH_ASSOC);

    } else {
        $s = explode("/", $_SERVER['REQUEST_URI']);

        if (strstr($_SERVER['HTTP_HOST'], 'localhost')) {
            if (isset($s[5]) && $s[5] != "") {
                $is_validResurl = false;
            }
        } else {
            if (isset($s[4]) && $s[4] != "") {
                $is_validResurl = false;
            }
        }

        $sql  = "SELECT title, restaurant, is_displayed, status FROM restaurant WHERE restaurant='{$RestaurantID}' limit 1";
        $stmt = $db->prepare($sql);
        $stmt->bindParam("query", $query);
        $stmt->execute();
        $restaurant = $stmt->fetch(PDO::FETCH_ASSOC);
    }

    if ($restaurant['is_displayed'] != 1 && $restaurant['status'] == 'active') {
        $sql  = "SELECT url FROM restaurant WHERE restaurant='{$RestaurantID}' limit 1 ";
        $stmt = $db->prepare($sql);
        $stmt->bindParam("query", $query);
        $stmt->execute();
        $restaurant_url = $stmt->fetch(PDO::FETCH_ASSOC);
        if (!empty($restaurant_url['url'])) {
            header("Location: " . $restaurant_url['url']);
        } else {
            //WY_debug::recordDebug("ERROR", "REDIRECT", "Invalid reques 3" . $_SERVER['REQUEST_URI'] . ", ip=" . $bkIP);
            header("HTTP/1.0 404 Not Found");
            include __DIR__ . '/404.php';
            // header("Location: 404");
        }
        exit();
    }

    if ($restaurant['is_displayed'] != 1 && empty($time)) {
            //WY_debug::recordDebug("ERROR", "REDIRECT", "Invalid reques 4" . $_SERVER['REQUEST_URI'] . ", ip=" . $bkIP);
            header("HTTP/1.0 404 Not Found");
            include __DIR__ . '/404.php';
            // header("Location: 404");
        exit();
    }

    try {
        if ($restaurant && count($restaurant) > 0) {
            if ($restaurant['restaurant'] != $RestaurantID) {
                //WY_debug::recordDebug("ERROR", "REDIRECT", "Invalid reques 5" . $_SERVER['REQUEST_URI'] . ", ip=" . $bkIP);
                throw new Exception("Page Not Found", 404);
            }
//            if($is_validResurl===false){
            //                 header("HTTP/1.0 404 Not Found");
            //                 include __DIR__ . '/404.php';
            //            }
            $restaurant_title = $restaurant['title'];
            $media            = new WY_Media;
            $header_image     = $media->getDefaultPicture($RestaurantID);
        } else {
            //WY_debug::recordDebug("ERROR", "REDIRECT", "Invalid reques 6" . $_SERVER['REQUEST_URI'] . ", ip=" . $bkIP);
            throw new Exception("Page Not Found", 404);
        }
    } catch (Exception $e) {
        //WY_debug::recordDebug("ERROR", "REDIRECT", "Invalid reques 7" . $_SERVER['REQUEST_URI'] . ", ip=" . $bkIP);
        //header('HTTP/1.1 301 Moved Permanently');
        header("HTTP/1.0 404 Not Found");
        include __DIR__ . '/404.php';
        // header("Location: 404");
        exit();
    }
}

if (preg_match('/\/contact/i', $_SERVER['REQUEST_URI'])) {
    $page = 'contact_page';
    header("HTTP/1.0 200 OK");
}
if (preg_match('/\/partner/i', $_SERVER['REQUEST_URI'])) {
    $page = 'partner_page';
    header("HTTP/1.0 200 OK");
}
if (preg_match('/\/faq/i', $_SERVER['REQUEST_URI'])) {
    $page = 'faq_page';
    header("HTTP/1.0 200 OK");
}
if (preg_match('/\/terms-and-conditions-of-service/i', $_SERVER['REQUEST_URI'])) {
    $page = 'terms_and_services_page';
}
if (preg_match('/\/privacy-policy/i', $_SERVER['REQUEST_URI'])) {
    $page = 'privacy_policy_page';
}
if (preg_match('/\/how-it-works/i', $_SERVER['REQUEST_URI'])) {
    $page = 'how_it_works_page';
}
if (preg_match('/\/mybookings/i', $_SERVER['REQUEST_URI'])) {
    $page = 'my_booking_page';
}
if (preg_match('/\/booknow/i', $_SERVER['REQUEST_URI'])) {
    $page = 'booknow';
}
if (preg_match('/\/section-booking/i', $_SERVER['REQUEST_URI'])) {
    $page = 'section_booking';
}
if (preg_match('/\/myorders/i', $_SERVER['REQUEST_URI'])) {
    $page = 'my_orders_page';
}
if (preg_match('/\/myreviews/i', $_SERVER['REQUEST_URI'])) {
    $page = 'my_reviews_page';
}
if (preg_match('/\/myaccount/i', $_SERVER['REQUEST_URI'])) {
    $page = 'my_account_page';
}
if (preg_match('/\/allevents/i', $_SERVER['REQUEST_URI'])) {
    $header_image = 'https://static4.weeloy.com/images/vday/valentine-day-weeloy-restaurant.jpg';
    $page         = 'event';
}


if (preg_match('/\/dine-and-win-contest-2016/i', $_SERVER['REQUEST_URI'])) {
    $header_image = 'https://static4.weeloy.com/images/headers/dine-contest-banner.jpg';
    $page         = 'contest2016';
}


if (preg_match('/\/404/i', $_SERVER['REQUEST_URI'])) {
    
    //WY_debug::recordDebug("ERROR", "REDIRECT", "Invalid reques 8" . $_SERVER['REQUEST_URI'] . ", ip=" . $bkIP);
    $page = '404';
    header("HTTP/1.0 404 Not Found");
}
switch ($page) {
    case 'search_page':
        $title                   = 'Search find and book your favorite restaurant in ' . $_SESSION['user']['search_city'];
        $description             = 'Find where to eat in ' . $_SESSION['user']['search_city'] . ' with fun Discover and book best restaurant in Asia Choose the top restaurant, spin the wheel and get exclusive discount';
        $FacebookMetaTitle       = $title;
        $FacebookMetaDescription = $description;
        break;
    case 'restaurant_info_page':
        $title                   = $restaurant_title . ' - Book with Weeloy and Get Rewarded';
        $description             = 'Book a Restaurant in ' . $city . ' at ' . $restaurant_title . ' with Weeloy and get yourself rewarded with exclusive deals and promotions. Simply book your table and spin the wheel!';
        $FacebookMetaTitle       = $title;
        $FacebookMetaDescription = $description;
        break;
    case 'restaurant_group_page':
        $title                   = 'All Restaurant  - Reserve your table now';
        $description             = 'All Restaurant with Weeloy. Reserve your table with one of the finest restaurants we have lined up for you and hurry!';
        $FacebookMetaTitle       = $title;
        $FacebookMetaDescription = $description;
        break;
    case 'contact_page':
        $title                   = 'Contact Weeloy - Book Best Restaurants in ' . $_SESSION['user']['search_city'] . ' and Get Rewarded';
        $description             = 'Weeloy is Free, Easy, Fun. Book Best Restaurants in ' . $_SESSION['user']['search_city'] . ' with Weeloy and discover exclusive promotions. Book your table and spin the wheel at your restaurant in ' . $_SESSION['user']['search_city'];
        $FacebookMetaTitle       = $title;
        $FacebookMetaDescription = $description;
        break;
    case 'partner_page':
        $title                   = 'Weeloy Partner â€“ Market Your Restaurant Online';
        $description             = 'Weeloy is Free, Easy, Fun. Let Weeloy be your preferred technology partner. Online restaurant booking system for guests to enjoy promotions.';
        $FacebookMetaTitle       = $title;
        $FacebookMetaDescription = $description;
        break;
    case 'faq_page':
        $title                   = 'FAQ Weeloy - How to book and review restaurant online';
        $description             = 'Wonder where to eat? Discover new restaurants new places Get special promotions in your city Make reservation, win discount, eat best food, review your experience';
        $FacebookMetaTitle       = $title;
        $FacebookMetaDescription = $description;
        break;
    case 'terms_and_services_page':
        $title                   = 'Terms and conditions of services Weeloy.com | Dine with discount';
        $description             = 'General terms, Prohibited and acceptable uses, Service, Responsibility. Which discounts can you win with Weeloy? Try the best food in Asia with special offer';
        $FacebookMetaTitle       = $title;
        $FacebookMetaDescription = $description;
        break;
    case 'privacy_policy_page':
        $title                   = 'Privacy Policy and terms | Singapore new place | Weeloy';
        $description             = 'Data policies, restaurant newsletter, personal account conditions Book a restaurant online and win special discounts, rewards Enjoy your diner and post best review';
        $FacebookMetaTitle       = $title;
        $FacebookMetaDescription = $description;
        break;
    case 'how_it_works_page':
        $title                   = 'How it works with Weeloy';
        $description             = 'Discover the Weeloy concept. Book your restaurant and get rewarded in 3 steps';
        $FacebookMetaTitle       = $title;
        $FacebookMetaDescription = $description;
        break;
    case 'my_booking_page':
        $title                   = 'Bookings | book restaurant | weeloy.com';
        $description             = 'Manage your booking, Spin the wheel, book your favorite restaurant and get rewards Weeloy.com';
        $FacebookMetaTitle       = $title;
        $FacebookMetaDescription = $description;
    case 'my_orders_page':
        $title                   = 'Bookings | book restaurant | weeloy.com';
        $description             = 'Manage your booking, Spin the wheel, book your favorite restaurant and get rewards Weeloy.com';
        $FacebookMetaTitle       = $title;
        $FacebookMetaDescription = $description;
    case 'my_reviews_page':
        $title                   = 'Reviews | manage reviews | weeloy.com';
        $description             = 'Manage your reviews, Spin the wheel, book your favorite restaurant and get rewards Weeloy.com';
        $FacebookMetaTitle       = $title;
        $FacebookMetaDescription = $description;
        break;
    case 'my_account_page':
        $title                   = 'Account | find best restaurant | weeloy.com';
        $description             = 'Manage your account, Spin the wheel, book your favorite restaurant and get rewards Weeloy.com';
        $FacebookMetaTitle       = $title;
        $FacebookMetaDescription = $description;
        break;
    case 'event':
        $title                   = 'All Restaurant Events - Reserve your table now';
        $description             = 'All Restaurant Events with Weeloy. Reserve your table with one of the finest restaurants we have lined up for you and hurry!';
        $FacebookMetaTitle       = $title;
        $FacebookMetaDescription = $description;
        break;
    case 'cny':
        $title                   = 'Chinese New Year 2016 with Weeloy - Reserve your table now';
        $description             = 'Chinese New Year 2016 with Weeloy. Chinese new year restaurant menu. Reserve your table with one of the finest restaurants we have lined up for you and hurry!';
        $FacebookMetaTitle       = $title;
        $FacebookMetaDescription = $description;
        break;
    case 'mother_day':
        $title                   = 'Amazing Mother s Day Deals and Menus with Weeloy - Reserve your table now';
        $description             = 'Amazing Mother s Day Deals and Menus 2016 with Weeloy. Mother s Day 2016 restaurant menu. Reserve your table with one of the finest restaurants we have lined up for you and hurry!';
        $FacebookMetaTitle       = $title;
        $FacebookMetaDescription = $description;
        break;
    case 'vday':
        $title                   = 'Weeloy Easter Menus & Promotions - Book your Easter brunch today!';
        $description             = 'Enjoy a scrumptious Easter brunch this March as we dine in the company of great friends and family. Read about the special Easter menus, promotions and various kids activities our restaurants have line-up for you. Book with Weeloy.com early to secure your seats.';
        $FacebookMetaTitle       = $title;
        $FacebookMetaDescription = $description;
    case 'fday':
        $title                   = "Father's Day Menus & Promotions";
        $description             = "Let your dad know how important he means to you this Father's Day with a delectable meal together at one of the finest restaurants we have line-up for you. Savour this lovely occasion to express your gratitude for his love and care all these while. ";
        $FacebookMetaTitle       = $title;
        $FacebookMetaDescription = $description;
        break;
    case 'booknow':
        $title                   = $restaurant_title . ' - Book with Weeloy - instant confirmation';
        $description             = 'Book a Restaurant in ' . $city . ' at ' . $restaurant_title . ' with Weeloy and get yourself rewarded with exclusive deals and promotions. Instant confirmation';
        $FacebookMetaTitle       = $title;
        $FacebookMetaDescription = $description;
        break;
    case 'section_booking':
        $title                   = $restaurant_title . ' - Book with Weeloy - instant confirmation';
        $description             = 'Book a Restaurant in ' . $city . ' at ' . $restaurant_title . ' with Weeloy and get yourself rewarded with exclusive deals and promotions. Instant confirmation';
        $FacebookMetaTitle       = $title;
        $FacebookMetaDescription = $description;
        break;
    case 'contest2016':
        $title                   = 'Dine and Win contest 2016 - book your restaurant with Weeloy Win $50 vouchers';
        $description             = 'Win $50 dining vouchers when you book a restaurant on Weeloy from 10 September – 10 November, 2016. Double your chance when you book via Weeloy mobile app!';
        $FacebookMetaTitle       = $title;
        $FacebookMetaDescription = $description;
        break;    
        
    default:
        
        if(!isset($_SESSION['user']['search_city'])){
            $city = 'Singapore';
        }else{
            $city = $_SESSION['user']['search_city'];
        }
        $title                   = "Book Best Restaurants in " . $city . " and Get Rewarded with Weeloy.";
        $description             = "Book Best Restaurants in " . $city . " with Weeloy and discover exclusive deals and promotions. Book your table and spin the wheel at your restaurant in " . $city;
        $FacebookMetaTitle       = $title;
        $FacebookMetaDescription = $description;
        break;
}

$no_header = false;
if (filter_input(INPUT_GET, 'dspl_h') == 'f') {
    $no_header = true;
}

$no_footer = false;
if (filter_input(INPUT_GET, 'dspl_f') == 'f') {
    $no_footer = true;
}

function is_wrong_url($url){
    
    if(preg_match('/\/search\/.*\/checkout[\/]?/', $url)){
        return true;
    }
    if(preg_match('/\/search\/.*\/how-it-works[\/]?/', $url)){
        return true;
    }
    if(preg_match('/\/search\/.*\/partner[\/]?/', $url)){
        return true;
    }
    if(preg_match('/\/search\/.*\/faq[\/]?/', $url)){
        return true;
    }
    if(preg_match('/\/search\/.*\/restaurant[\/]?/', $url)){
        return true;
    }
    if(preg_match('/\/search\/.*\/client\/assets[\/]?/', $url)){
        return true;
    }
    if(preg_match('/\/search\/.*\/search[\/]?/', $url)){
        return true;
    }
    if(preg_match('/\/restaurant\/.*\/client\/assets[\/]?/', $url)){
        return true;
    }
    if(preg_match('/\/restaurant\/.*\/faq[\/]?/', $url)){
        return true;
    }
    if(preg_match('/\/restaurant\/.*\/contact[\/]?/', $url)){
        return true;
    }
    if(preg_match('/\/restaurant\/.*\/how-it-works[\/]?/', $url)){
        return true;
    }
    if(preg_match('/\/restaurant\/.*\/myaccount[\/]?/', $url)){
        return true;
    }
    if(preg_match('/\/restaurant\/.*\/mybookings[\/]?/', $url)){
        return true;
    }
    if(preg_match('/\/restaurant\/.*\/myorder[\/]?/', $url)){
        return true;
    }
    if(preg_match('/\/restaurant\/.*\/myreviews[\/]?/', $url)){
        return true;
    }
    if(preg_match('/\/restaurant\/.*\/partner[\/]?/', $url)){
        return true;
    }
    if(preg_match('/\/restaurant\/.*\/privacy-policy[\/]?/', $url)){
        return true;
    }    
    if(preg_match('/\/restaurant\/.*\/terms-and-conditions-of-service[\/]?/', $url)){
        return true;
    }if(preg_match('/\/restaurant\/.*\/restaurant[\/]?/', $url)){
        return true;
    }
    if(preg_match('/\/restaurant\/.*\/search[\/]?/', $url)){
        return true;
    }
    
    if(preg_match('/\/restaurant\/.*\/checkout[\/]?$/', $url)){
        return false;
    }
    
    if(preg_match('/\/restaurant\/.*\/checkout[\/]?/', $url)){
        return true;
    }
    
    
    if(preg_match('/\/search\/search[\/]?$/', $url)){
        return false;
    }
    
    return false;
}