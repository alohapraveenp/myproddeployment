<?php

include __DIR__ . '/includes/header.php';


//echo '<br><br><br>';
////var_dump($_SESSION);
//
//        echo '<pre>';
//        print_r($_SESSION);
//        echo  '</pre>';



?>

<!DOCTYPE html>
<html lang="en" ng-app="WeeloyApp" ng-controller="RootCtrl">

    <head>
        <meta charset="UTF-8">
        <title ng-bind="title">
            <?php echo $title; ?>
        </title>
        <meta name="description" content="<?php echo $description; ?>" />
        <meta property="og:title" content="<?php echo $FacebookMetaTitle; ?>" />
        <meta property="og:type" content="website" />
        <meta property="og:image" content="<?php echo $header_image ?>" />
        <meta property="og:description" content="<?php echo $FacebookMetaDescription; ?>" />
        <meta property="og:locale" content="en_US" />
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="owner" content="weeloy.com">
        <meta name="apple-itunes-app" content="app-id=973030193">
        <base href="<?php echo __ROOTDIR__; ?>/">
        <meta name="google-site-verification" content="iHu3Km_-ufs5DlCTZxayDBqSxOG8p2u26vWFqCfTY98" />
        <meta name="p:domain_verify" content="31b6b6859db50148d7d5e4df9a75345c"/>
        <meta name="msvalidate.01" content="90FBBB685EF1AC19990BADECF586BE25" />
        <link rel="shortcut icon" href="favicon.ico" title="favoris icone">

        <?php
            // include __DIR__ . '/criticalcss/restaurant_info_page.php';
            include __DIR__ . '/criticalcss/home_page.php';

            $ua = strtolower($_SERVER['HTTP_USER_AGENT']);
            if(stripos($ua,'android') !== false) { // && stripos($ua,'mobile') !== false) {
                    echo '<link rel="stylesheet" type="text/css" href="client/assets/css/app.min.css?v='.$app_version.'">';
            }
        ?>

        <script type="text/javascript">


<?php
if (isset($_SESSION['user']['email'])) {
    $loggedin = 'true';
    $user = $_SESSION['user'];
} else {
    $loggedin = 'false';
    $user = null;
}

if (isset($_SESSION['cart']) && isset($_SESSION['cart']['items'])) {
    $cart = $_SESSION['cart']['items'];
} else {
    $cart = [];
}

if (isset(parse_url(__BASE_URL__)['path'])) {
    $path = parse_url(__BASE_URL__)['path'];
} else {
    $path = '/';
}
?>
                            var BASE_URL = "<?php echo __BASE_URL__; ?>";
                            var BASE_PATH = "<?php echo $path; ?>";
                            var FB_ID = '<?php echo BOOKING_APP_ID; ?>';
                            var FB_WEBSITE_ID = '<?php echo WEBSITE_FB_APP_ID; ?>';
                            var loggedin = <?php echo $loggedin; ?>;
                            var user = <?php echo ($user != null) ? json_encode($user) : 'null'; ?>;
                            var UserSession = <?php echo isset($_SESSION['user']) ? json_encode($_SESSION['user']) : 'null'; ?>;
                            if (UserSession.search_city == undefined) {
                                UserSession.search_city = 'Singapore';
                            }
                            var cart = <?php echo json_encode($cart); ?>;
        </script>
    </head>

    <body>

        <header <?php
if ($no_header == true) {
    echo "style='display:none;'";
}
?>>
            <nav class="navbar navbar-default navbar-fixed-top">
                <div class="container">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand-home" href="<?php echo __BASE_URL__; ?>" title="Reserve restaurant with weeloy">
                            <img width="116" height="32"  class="icon-weeloy" src="images/logo_title.svg" alt="Weeloy restaurant reservation">
<!--                            <img   class="icon-weeloy_small" src="images/logo-2.jpg" alt="Weeloy restaurant reservation" style='object-fit: cover;  height: 70px;'>-->
                        </a>
                    </div>

                    <div ng-if="true" ng-include="'../app/shared/partial/_menu.tpl.html'"></div>
                </div>

            </nav>
            <div class="modal fade" id="loginModal" tabindex="-1" role="dialog" aria-labelledby="myLoginModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-content">
                            <!-- Include meta tag to ensure proper rendering and touch zooming -->
                            <meta name="viewport" content="width=device-width, initial-scale=1">
                            <!-- Include bootstrap stylesheets -->
                            <div class="container_modal">
                                <div class="mainbox">
                                    <div class="panel panel-info">
                                        <div class="panel-body">
                                            <h1 id="title" ></h1>
                                            <div class="loginbox">
                                                <div id="facebook" class="login-form">
                                                    <a class="btn btn-lg btn-social btn-facebook" id="loginBtn" ng-click="LoginWithFacebook()" title="Log in using your Facebook account">
                                                        <i class="fa fa-facebook"></i> {{Str.template.LoginModalLoginWithFacebook}}
                                                    </a>
                                                    <div id="status_fb"></div>
                                                </div>
                                                <div class="or-spacer">
                                                    <div class="mask"></div>
                                                    <span><i>or</i></span>
                                                </div>
                                            </div>
                                            <!-- LoginForm -->
                                            <div ng-if="showForm == 'loginForm'" ng-include="'../app/shared/partial/_login_form.tpl.html'"></div>
                                            <!-- LoginForm -->
                                            <!-- SignUpForm -->
                                            <div ng-if="showForm == 'signupForm'" ng-include="'../app/shared/partial/_signup_form.tpl.html'"></div>
                                            <!-- SignUpForm -->
                                            <!-- Forgot Password Form -->
                                            <div ng-if="showForm == 'forgotPasswordForm'" ng-include="'../app/shared/partial/_forgot_password_form.tpl.html'"></div>
                                            <!-- Forgot Password Form -->
                                            <div style="margin-bottom: 20px;" class="form-inline logmenu">
                                                <p ng-show="showForm == 'loginForm' || showForm == 'signupFrom'">
                                                    <a href="#" ng-click="showForm = 'forgotPasswordForm'" style="color: #333;font-family: proximanova;font-size: 16px;line-height: 16px;" ng-bind="Str.template.TextForgotPassword"></a>
                                                </p>
                                                <p ng-show="showForm != 'loginForm'">
                                                    <a href="#" ng-click="showForm = 'loginForm'"  style="color: #333;font-family: proximanova;font-size: 16px;line-height: 16px;"  ng-bind="TextYouPreferToLogin"></a>
                                                </p>
                                            </div>
                                            <div id="registerTag" ng-show="showForm != 'signupForm'">
                                                <div style="border-top: 1px solid#888; padding-top:15px; font-size:85%">
                                                    {{Str.template.TextDontHaveAccount}} <a href="#" ng-click="showForm = 'signupForm'" id="register"> {{Str.template.TextRegisterHere}} </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-2"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <div class="wrapper">
            <div ng-if="true" ng-include="'../app/shared/partial/_message_wrapper.tpl.html'"></div>
            <div ng-view>


<?php
switch ($page) {
    case 'home_page':
        //include __DIR__ . '/includes/restaurant_info_page_test.php';
        include __DIR__ . '/includes/home_page.php';
        break;
    case 'search_page':
        //include __DIR__ . '/includes/search_page.php';
        break;
    case 'restaurant_info_page':
        include __DIR__ . '/includes/restaurant_info_page_test.php';
        break;
    default:
        break;
}
?>

            </div>
        </div>

        <?php if(empty($no_footer)) {echo "<footer ng-include=\"'../app/shared/partial/_footer.tpl.html'\"></footer>";}?>

        <script>




                            var cb = function () {
                                var l = document.createElement('link');
                                l.rel = 'stylesheet';
                                l.href = 'client/assets/css/app.min.css?v=<?php echo "$app_version";?>';
                                var h = document.getElementsByTagName('head')[0];
                                h.parentNode.insertBefore(l, h);
                            };
                            var raf = requestAnimationFrame || mozRequestAnimationFrame ||
                                    webkitRequestAnimationFrame || msRequestAnimationFrame;
                            if (raf)
                                raf(cb);
                            else
                                window.addEventListener('load', cb);
        </script>

<?php
if (strpos(__BASE_URL__, 'localhost') !== false || strpos(__BASE_URL__, '.dev') !== false) {
    echo '<script src="client/weeloy.js?v='.$app_version.'" defer></script>';
} else {
    echo '<script src="client/weeloy.min.js?v='.$app_version.'" defer></script>';
}
?>



        <!-- Google Tag Manager -->
        <noscript>
        <iframe src="//www.googletagmanager.com/ns.html?id=GTM-TQZ2W7" height="0" width="0" style="display:none;visibility:hidden"></iframe>
        </noscript>
        <script>
                            (function (w, d, s, l, i) {
                                w[l] = w[l] || [];
                                w[l].push({
                                    'gtm.start': new Date().getTime(),
                                    event: 'gtm.js'
                                });
                                var f = d.getElementsByTagName(s)[0],
                                        j = d.createElement(s),
                                        dl = l != 'dataLayer' ? '&l=' + l : '';
                                j.async = true;
                                j.src =
                                        '//www.googletagmanager.com/gtm.js?id=' + i + dl;
                                f.parentNode.insertBefore(j, f);
                            })(window, document, 'script', 'dataLayer', 'GTM-TQZ2W7');
        </script>
        <!-- End Google Tag Manager -->
 </body>

</html>
