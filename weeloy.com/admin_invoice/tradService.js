app.service('invoiceService',['$http',function($http){

	this.invSections = function(params){
		return $http.post("../api/services.php/invoice/getinvoicesection/", params)
    			.then(function(response) {return response.data;});
	};

	this.invReadContent = function(params){
		return $http.post("../api/services.php/invoice/getinvoicereadcontent/", params)
    			.then(function(response) {return response.data;});
	};

	this.invWriteContent = function(params){
		return $http.post("../api/services.php/invoice/getinvoicewritecontent/", params)
    			.then(function(response) {return response.data;});
	};

	this.invNewElement = function(params){
		return $http.post("../api/services.php/invoice/getinvoicenewelement/", params)
    			.then(function(response) {return response.data;});
	};


}]);

