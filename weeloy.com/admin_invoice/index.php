<?php

// don't move those line as cookie header need to send prior to anything
require_once("conf/conf.init.inc.php");
require_once("lib/wpdo.inc.php");
require_once("lib/gblcookie.inc.php");
require_once("lib/class.login.inc.php");
require_once("lib/class.coding.inc.php");
require_once("conf/conf.session.inc.php");
require_once("lib/class.media.inc.php");
	
require_once("lib/class.images.inc.php");	
require_once("../inc/utilities.inc.php");
require_once("lib/class.analytics.inc.php");
require_once("lib/class.restaurant.inc.php");
require_once("lib/class.member.inc.php");
require_once("lib/class.cluster.inc.php");

?>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1">
<title>Weeloy - translation</title>
<link rel="icon" href="/favicon.ico" type="image/gif" sizes="16x16">
    <link href="../css/carousel.css" rel="stylesheet" type="text/css">
    <link href="../css/bullet.css" rel="stylesheet" type="text/css">
    <link href="../css/admin-style.css?1" rel="stylesheet" type="text/css"/>
    <link href="../css/dropdown.css" rel="stylesheet" type="text/css">
    <link href="../css/login.css" rel="stylesheet" type="text/css">
    <link href="../css/modal.css" rel="stylesheet" type="text/css">


    <link href="../css/bootstrap33.min.css" rel="stylesheet" type="text/css" />
    <link href="../css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
    <link href="../css/jquery-ui.css" rel="stylesheet" type="text/css">
    <link href="../css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="../css/bootstrap-social.css" rel="stylesheet" type="text/css"/>
    <link href="../css/login-modal.css" rel="stylesheet" type="text/css"/>

    <link href='https://fonts.googleapis.com/css?family=Dosis:400,600,700bold|Roboto:400,900,700italic,700,500italic,500,400italic' rel='stylesheet' type='text/css'>

    <script type="text/javascript" src="../js/jquery.min.js"></script>
    <script type="text/javascript" src="../js/angular.min.js"></script>
    <script type="text/javascript" src="../js/jquery-ui.js"></script>
    <script type="text/javascript" src="../js/bootstrap.min.js"></script>
    <script type="text/javascript" src="../js/ui-bootstrap-tpls-0.12.1.min.js"></script>

<style> 
.headertitle { font-size:14px; font-weight:bold; color:white; font-family:helvetica;}
.headersubtitle { font-size:10px; color:white; font-weight:bold; font-family:helvetica; padding-left:10px; }
.mwhite { color:white; }
.leftm { padding-bottom:20px; color:black; }
	
</style>

<!--[if lt IE 9]>
<script src="js/html5.js"></script>
<script src="js/selectivizr-min.js"></script>
<script src="js/modernizr.js"></script>
<link rel="stylesheet" type="text/css" href="css/ie.css" media="screen" />
<![endif]-->
</head>


<body ng-app="backoffice-page" ng-controller="boHomeController">

<script>var app = angular.module('backoffice-page',['ui.bootstrap', 'FacebookProvider', 'ngStorage']); </script>

    <div class="ProfileBox">
        <a href="index.php"><img src="../images/logo_w.png" /></a><hr>
        <div ng-if='loggedin'>
        <h4>{{ language | uppercase}}</h4>
        <hr>
		<div ng-repeat="section in tablecontent" class="row leftm">
        <a href='javascript:;' ng-click="process(section.name);">{{section.name | uppercase}}</a>
		</div>
		<hr>
        </div>
		<div class="row leftm" ng-if="editcreatemode != ''";>
        <button class="btn btn-default btn-sm" ng-click="process(editcreatemode);">{{editcreatemode | capitalize}} </button>
		<hr>
		</div>
		<div class="row leftm">
        <button class="btn btn-default btn-sm" ng-click="loginout(logaction);">{{logaction | capitalize}} </button>
		</div>
		<div class="row leftm" ng-if="logaction == 'logout'">
        <button class="btn btn-default btn-sm" ng-click="loginout('preferences');">Preferences </button>
		</div>
		<hr/>
        <p id="sessiontime" class="small"></p>
    </div>


    <div class="move">
		<div class="container">
<!--			<content-item ng-repeat="item in interfaceRegister" content="item" myTemplates="templateData"></content-item> -->
			<div class="row">
				<div white-bg">
				<p>
					<content-item ng-repeat="item in translatedata" content="item" myTemplates="templateData"></content-item>
				</p>
				</div>
			 </div>
		</div>
  	<div ng-include="'bottomTemplate.html'" ng-if="notEditingMode"></div>
    </div>

<div id="fb-root"></div>

<script type="text/javascript" src="../js/alog.js"></script>
<script type="text/javascript" src="../js/facebookRun.js"></script>
<script type="text/javascript" src="../js/facebookProvider.js"></script>
<script type="text/javascript" src="../js/ngStorage.min.js"></script>
<script type="text/javascript" src="../js/loginService.js"></script>
<script type="text/javascript" src="../js/formControl.js"></script>
<script type="text/javascript" src="tradService.js"></script>
<script type="text/javascript" src="boHomeController.js"></script>

<script type="text/ng-template" id="loginBackoffice.html">
	<content-item ng-repeat="item in interfaceLogin" content="item" myTemplates="templateData"></content-item>
</script>

<script type="text/ng-template" id="forgotBackoffice.html">
	<content-item ng-repeat="item in interfaceForgot" content="item" myTemplates="templateData"></content-item>
</script>

<script type="text/ng-template" id="chgpassBackoffice.html">
	<content-item ng-repeat="item in interfaceChange" content="item" myTemplates="templateData"></content-item>
</script>

<script type="text/ng-template" id="registerBackoffice.html"> 
	<content-item ng-repeat="item in interfaceRegister" content="item" myTemplates="templateData"></content-item>
</script>

<script>
var cookiename = <?php echo "'" . getCookiename('translation') . "'"; /* connected to the alog.js script file */ ?>; 

</script>

</body>
</html>
