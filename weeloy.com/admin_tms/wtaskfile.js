module.exports = function(grunt) {
        // Project configuration.
        grunt.initConfig({
        dirs: {
            output: '../admin_tms/'
        },
        uglify: {
           options: {
                banner: '/* Weeloy copyright(c) 2014. All rights reserved */\r'
            },            
            my_target: {
        		files: {
                    '<%= dirs.output %>weeloy_tms.min.js': ['<%= dirs.output %>weeloytmshide.js']
                }
            }
        },
        obfuscator: {
                files: [
                    '<%= dirs.output %>boHomeController.js',
                    '<%= dirs.output %>tmsPunchchart.js'
                ],
            entry: '<%= dirs.output %>boHomeController.js',
            out: '<%= dirs.output %>weeloy_tms.ob.min.js',
            strings: true,
            root: __dirname
        },
        concat: {
            options: {
                separator: ';',
            },
            dist: {
                src: [
 /*
                    '../js/facebookProvider.js',
                    '../js/loginService.js',
                     '../js/formControl.js',
                    '../js/ngStorage.min.js',
                    '../backoffice/inc/libService.js',
  */                   
                   
                    '<%= dirs.output %>twowaywindows.js',
                    '<%= dirs.output %>fonts/cufonfonts.js',
                    '<%= dirs.output %>tmsService.js',
                    '<%= dirs.output %>tmsRaphaelTimeline.js',
                    '<%= dirs.output %>tmsRaphaelLib.js',
                    '<%= dirs.output %>tmsRaphaelDrag.js',
                    '<%= dirs.output %>tmsRaphael.js',
                    '<%= dirs.output %>tmsPunchchart.js',
                    '<%= dirs.output %>boHomeController.js',
                    '../backoffice/inc/backofficelib.js',
                    '../backoffice/inc/paginator.js',
                    
                    
                ],
                dest: '<%= dirs.output %>weeloytmshide.js',
            },
        },
	'string-replace': {
	    dist: {
		files: {
			  '<%= dirs.output %>indexV2hide.php': '<%= dirs.output %>indexV2.php'
			},
		options: {
		  replacements: [{
			pattern: 'weeloy_tms.min.js',
			replacement: 'weeloytmshide.js'
		  }]
		}
		  }
		},
        watch: {
            scripts: {
                files: [
                    '<%= dirs.output %>fonts/cufonfonts.js',
                    '<%= dirs.output %>tmsService.js',
                    '<%= dirs.output %>tmsRaphaelTimeline.js',
                    '<%= dirs.output %>tmsRaphaelLib.js',
                    '<%= dirs.output %>tmsRaphaelDrag.js',
                    '<%= dirs.output %>tmsRaphael.js',
                    '<%= dirs.output %>tmsPunchchart.js',
                    '<%= dirs.output %>boHomeController.js'
                ],
                tasks: [ 'concat', 'uglify', 'obfuscator'],
                options: {
                    spawn: false,
                },
            },
        },
   });

    // Load the plugin that provides the "uglify" task.
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-obfuscator');
    grunt.loadNpmTasks('grunt-string-replace');
    
    // Default task(s).
    grunt.registerTask('default', [ 'concat', 'uglify', 'string-replace']);
    //grunt.registerTask('default', [ 'concat', 'uglify', 'obfuscator']);
    //grunt.registerTask('default', ['watch']);
};
