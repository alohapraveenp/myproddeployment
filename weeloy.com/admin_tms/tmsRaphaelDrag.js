/*
 *  ┌─────────────────────────────────────────────┐ 
 *  │ TMS 1.0 - JavaScript Vector Library             │ 
 *  ├─────────────────────────────────────────────┤ 
 *  │ Copyright © 2015 Weeloy. All rights reserved.   │ 
 *  ├─────────────────────────────────────────────┤ 
 *  │ Company: https://www.weeloy.com				  │ 
 *  ├─────────────────────────────────────────────┤ 
 *  │ Author Richard Kefs							  │ 
 *  └─────────────────────────────────────────────┘ 
 *
*/


function DragObj(qO) {

	var queueObj = qO, ox, oy;
	
	// many different drag method
	// draggrp for group of object that have been transformed, i.g, rotated
	// dragpset for dragging paper set
	// dragrect for creating a selection rectangle
	
	return {
		draggrpstart: function(x,y,e) { 
			var myq = this.data('wraper').parent;
			var oo, type, label;
			
			myq.startgrpDrag(); 
			this.data('myX', this.attr("x"));
			this.data('myY', this.attr("y"));

			if(this.data('wraper').objclass === "table")	
				otherobj = myq.groupObj.getSelectedObj();
						
			this.data('otherobj', otherobj);
			for(i = 0; i < otherobj.length; i++) {
				oo = otherobj[i];
				type = oo.data('wraper').type;
				label = oo.data('wraper').label;
				oo.data('type', type);
				oo.data('label', label);
				oo.data('myX', oo.attr("x"));
				oo.data('myY', oo.attr("y"));
				if(type === "c") {
 					oo.data('mycX', oo.attr("cx"));
					oo.data('mycY', oo.attr("cy"));
				}
				oo.data('myangle', oo.data('wraper').angle);
				}
			},
		
		draggrpmove: function(dx,dy,x,y,e) { 
			var type, label, nangle, deltax, deltay, oo;
			this.data('wraper').parent.connectgrpHide();

			otherobj = this.data('otherobj');
			for(i = 0; i < otherobj.length; i++) {
				oo = otherobj[i];
				type = oo.data("type");
				label = oo.data("label");
				nangle = -oo.data('myangle');
				deltax = (dx * Math.cos(nangle*Math.PI / 180)) - (dy * Math.sin(nangle*Math.PI / 180));
				deltay = (dy * Math.cos(nangle*Math.PI / 180)) + (dx * Math.sin(nangle*Math.PI / 180));
				oo.attr({ x: oo.data('myX') + deltax, y: oo.data('myY') + deltay });
				if(type === "c")  {
					oo.attr({ cx: oo.data('mycX') + deltax, cy: oo.data('mycY') + deltay });
					}
				}

			this.data('wraper').parent.connectgrpShow();
			},
			
		draggrpend: function(e) {  this.data('wraper').parent.endgrpDrag();  }, 

		dragpsetstart: function(x,y,e) { var me = this.data("wraper"); me.data('mytransform', this.transform()); },
		dragpsetstartlabel: function(x,y,e) { var q, me = this.data("wraper"); me.data('mytransform', this.transform()); if(queueObj.hoverviewer) queueObj.stophoverViewer();  },
		dragpsetmove: function(dx,dy,x,y,e) { var me = this.data("wraper"); me.transform(this.data('mytransform') + 'T'+dx+','+dy); },
		dragpsetend: function(e) { var me = this.data("wraper"); me.data('mytransform', this.transform()); }, 
		dragpsetendObj: function(e) { var me = this.data("wraper"), conf = this.data("booking"), npers = parseInt(this.data("npers")), tbl = this.data("tbl"); me.data('mytransform', this.transform()); 
								queueObj.reassignResa(tbl, conf, npers, this.getBBox()); queueObj.restarthoverViewer(); }, 

		dragrectstart: function(x,y,e) { this.ow = this.attr('width'); this.oh = this.attr('height'); },
		dragrectmove: function(dx,dy,x,y,e) {  var width = this.ow + dx, height = this.oh + dy; if(width < 0 || height < 0) return; this.attr({width: width, height: height });  },
		dragrectend: function() { queueObj.groupObj.squareSelect(this.data("x"), this.data("y"), this.attr('width'), this.attr('height')); this.remove(); },
	
		dragstart: function() {
			var me = this;
			var tbl = me.data("wraper");
			var oo = tbl.obj;
			ox = oy = 0; 
			
			tbl.objresa.hide();
			oo.attr({opacity: .5});   
			},
		
		dragmove: function(dx, dy) {
			var me = this;
			var tbl = me.data("wraper");
			var oo = tbl.obj;
			var deltaX = (dx - ox);
			var deltaY = (dy - oy);
			
			oo.attr({ transform: "...T" + deltaX + "," + deltaY }); 
			if(tbl.state === 1) {
				tbl.perimeter.attr({ transform: "...T" + deltaX + "," + deltaY }); 
				if(tbl.turncirc)
					tbl.turncirc.attr({ transform: "...T" + deltaX + "," + deltaY }); 
				tbl.growsqr.attr({ transform: "...T" + deltaX + "," + deltaY }); 
				if(tbl.obj.display)
					tbl.obj.display.attr({ transform: "...T" + deltaX + "," + deltaY }); 
				}
			ox = dx;
			oy = dy;

			tbl.bxy = getMatrix(tbl); 

			tbl.conObj.show();
			livedisplay(tbl, 'move', queueObj);
			},
		
		dragend: function(e) {
			var me = this;
			var tbl = me.data("wraper");
			var oo = tbl.obj;

			oo.attr({opacity: 1});
			tbl.bxy = getMatrix(tbl);
			tbl.cx = ((tbl.bxy[0]+tbl.bxy[4])/2);
			tbl.cy = ((tbl.bxy[1]+tbl.bxy[5])/2);
			tbl.objresa.resume();
			tbl.conObj.show();
		
			gx = tbl.bxy[0];
			gy = tbl.bxy[1];

			if(gx < 35 && gy > queueObj.ytrash-15 && gy < (queueObj.ytrash+40) && tbl.state == 1) {
				return queueObj.deleteTbl(tbl);
				}

			if(gx < 50) {
				oo.attr({ transform: "...T" + (-ox) + "," + (-oy) }); 
				oo.attr({opacity: 1});
				gx -= ox;
				gy -= oy;
				queueObj.resetSquare();
				return;
				}
			
			if(gx > 35 && tbl.state == 0) {
				tbl.state = 1;
				queueObj.createObj(tbl.selector);
				queueObj.insert(tbl);
				}
			
			tbl.transfrm |= 1;	// drag 1, scale 2, rotate 4	
			queueObj.qclick(e, tbl.obj);
			queueObj.setdisplay(tbl);
			livedisplay(tbl, 'end_move', queueObj);
			},

		dragrstart: function() {
			var me = this;
			var tbl = me.data("wraper");

			tbl.obj.attr({opacity: .5});   
			tbl.objresa.hide();
			//tbl.conObj.hide();

			// storing original coordinates
			rox = roy = 0;    
			sx = parseFloat(tbl.bxy[0].myPrecision(3));   
			sy = parseFloat(tbl.bxy[1].myPrecision(3));   
	
			tbl.obj.ow = parseFloat(tbl.obj.attr("width"));
			tbl.obj.oh = parseFloat(tbl.obj.attr("height"));
			if(tbl.type === "c") {
				tbl.obj.ow = parseFloat(tbl.perimeter.attr("width"));
				tbl.obj.oh = parseFloat(tbl.perimeter.attr("height"));
				}        
			},

		dragrmove: function(dx, dy) {
			var me = this;
			var tbl = me.data("wraper");

			var angle = tbl.angle;
			var nangle = -angle;
			var deltaX = dx - rox;
			var deltaY = dy - roy;
		
			rox = dx;
			roy = dy;
		
			width = parseFloat(tbl.obj.ow);
			height = parseFloat(tbl.obj.oh);

			width += (dx * Math.cos(nangle*Math.PI / 180)) - (dy * Math.sin(nangle*Math.PI / 180));
			height += (dy * Math.cos(nangle*Math.PI / 180)) + (dx * Math.sin(nangle*Math.PI / 180));
			if(width < 20 || height < 20)
				return;

			tbl.width = width;
			tbl.height = height;
			me.attr({ transform: "...T" + deltaX + "," + deltaY }); 
			tbl.perimeter.attr({width: width, height: height});
			
			if(tbl.type !== "c") {
				tbl.obj.attr({width: width, height: height});
				tbl.bxy = getMatrix(tbl);
				tbl.conObj.show();
			} else {
				var bb = tbl.perimeter.getBBox();
				var XX = Math.sqrt(Math.abs(deltaX)).toPrecision(1) * ((deltaX >= 0) ? 1 : -1);
				var YY = Math.sqrt(Math.abs(deltaY)).toPrecision(1) * ((deltaY >= 0) ? 1 : -1);
				dx = (bb.x + (bb.width/2)) - tbl.obj.attr("cx");
				dy = (bb.y + (bb.width/2)) - tbl.obj.attr("cy");
				tbl.obj.attr({ r: bb.width / 2 });
				tbl.obj.attr({ transform: "...T" + XX + "," + YY }); 
				// not working ?!
				// tbl.obj.attr({ cx: (bb.x + (bb.width/2)), cy: (bb.y + (bb.width/2)) });
				}
		
			livedisplay(tbl, 'scaling', queueObj);
			},

		dragrend: function(e) {
			var me = this;
			var tbl = me.data("wraper");


			tbl.obj.attr({opacity: 1});  
			tbl.bxy = getMatrix(tbl);
			tbl.cx = ((tbl.bxy[0]+tbl.bxy[4])/2);
			tbl.cy = ((tbl.bxy[1]+tbl.bxy[5])/2);
			tbl.objresa.resume();
			tbl.conObj.show();

			tbl.transfrm |= 2;	// drag 1, scale 2, rotate 4	
			if(tbl.transfrm >= 6) // rotate and scale
				queueObj.reNewImg(tbl);

			queueObj.qclick(e, tbl.obj);
			queueObj.setdisplay(tbl);
			livedisplay(tbl, 'end_scaling', queueObj);
			},
		
		dragcstart: function() {
			var me = this;
			var tbl = me.data("wraper");

			tbl.obj.attr({opacity: .5});   
		
			centerx = (tbl.bxy[0] + tbl.bxy[4]) / 2; 
			centery = (tbl.bxy[1] + tbl.bxy[5]) / 2; 

			px = py = angle = 0;
			},

		dragcmove: function(dx, dy) {
			var me = this;
			var tbl = me.data("wraper");
			var img = tbl.obj;
		
			var sAngle = Math.atan2(((tbl.bxy[1] + py)-centery),((tbl.bxy[0] + px) - centerx));
			var pAngle = Math.atan2(((tbl.bxy[1] + dy)-centery),((tbl.bxy[0] + dx) - centerx));        

			px = dx;
			py = dy;
			R = (pAngle - sAngle) * 180/Math.PI;  
		 
			val = (R > 0) ? 2 : -2;     	        	       		
			angle += val;
			tbl.angle += val;
			tbl.perimeter.attr({transform: "...R"+val });
			img.attr({transform: "...R"+val });

			x = img.attr("x");
			y = img.attr("y");
			w = tbl.width;
			h = tbl.height;
					
			var x1 = parseFloat(img.matrix.x(x, y).myPrecision(3));
			var y1 = parseFloat(img.matrix.y(x, y).myPrecision(3));
			deltaX = x1 - tbl.turncirc.cx;
			deltaY = y1 - tbl.turncirc.cy;

			tbl.turncirc.attr({ transform: "...T" + deltaX + "," + deltaY }); 
			tbl.turncirc.cx = x1;
			tbl.turncirc.cy = y1;

			x1 = parseFloat(img.matrix.x(x + w, y + h).myPrecision(3));
			y1 = parseFloat(img.matrix.y(x + w, y + h).myPrecision(3));
			deltaX = x1 - tbl.growsqr.cx;
			deltaY = y1 - tbl.growsqr.cy;

			tbl.growsqr.attr({ transform: "...T" + deltaX + "," + deltaY }); 
			tbl.growsqr.cx = x1;
			tbl.growsqr.cy = y1;

			livedisplay(tbl, 'rotating', queueObj);
			},

		dragcend: function(e) {
			var me = this;
			var tbl = me.data("wraper");
		
			tbl.obj.attr({opacity: 1});  			 
			tbl.bxy = getMatrix(tbl);					

			tbl.transfrm |= 4;	// drag 1, scale 2, rotate 4	
			if(tbl.transfrm >= 6) // rotate and scale
				queueObj.reNewImg(tbl);

			queueObj.qclick(e, tbl.obj);
			queueObj.setdisplay(tbl);
			livedisplay(tbl, 'end_rotating', queueObj);
			},
		}
}

