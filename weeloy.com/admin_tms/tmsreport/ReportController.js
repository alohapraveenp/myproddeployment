String.prototype.capitalize = function() { var s = this; if(s && s.length > 1) s = s.substring(0,1).toUpperCase()+s.substring(1); return s; };

app.controller('ReportController', ['$scope', '$filter', '$uibModal', '$log', '$timeout', 'apiService', 'extractService', 'ModalService', function($scope, $filter, $uibModal, $log, $timeout, apiService, extractService, ModalService) {
   $scope.activity = "bookings";
   $scope.mydata = new apiService.ModalDataBooking();
   $scope.restaurant = getArg('restaurant');
   $scope.restotitle = getArg('title').replace(/_/g, " ");
   $scope.token = getArg('token');
   $scope.meal = getArg('meal');
   $scope.date = getArg('date');
   $scope.startslot = 16;
   $scope.endslot = 32
   $scope.totalpax = 0;
   if($scope.meal !== 'lunch') {
		$scope.meal = 'dinner';
		$scope.startslot = 32;
		$scope.endslot = 64;
	} else $scope.meal = 'lunch'

   var nicedate = (typeof $scope.date === "string" && $scope.date.length === 10) ? moment($scope.date).format('MMMM D, YYYY') : moment().format('MMMM D, YYYY');
   $scope.title = "TMS " + $scope.meal.capitalize() + " Report, " + nicedate;

   if(!$scope.restaurant || !$scope.token) {
		$scope.title = "FAILURE";
		return alert("Missing vital arguments, restaurant or token. Unable to continue");
	}

   $scope.alllooksgood = true;
   $scope.paginator = new Pagination(200);
   $scope.names = [];
   $scope.tabletitle = [ {a:'time', b:'Time', c:'', l:'', q:'down', cc: 'black' }, {a:'tablename', b:'Table', c:'', l:'', q:'down', cc: 'black' }, {a:'booking', b:'Reference/VIP', c:'', l:'', q:'down', cc: 'black' }, {a:'fullname', b:'Details', c:'', l:'100', q:'down', cc: 'black' }, {a:'pers', b:'Covers', c:'', l:'15', q:'down', cc: 'black' }, {alter: 'vcdate', a:'cdate', b:'Taken on', c:'date', l:'', q:'down', cc: 'black' },{a:'sudotype', b:'By', c:'', l:'40', q:'down', cc: 'black' } ];
   $scope.subtabletitle = [ {a:'mobile', b: '', p:'subinfo9', c:'', l:'', q:'down', cc: 'black' }, 
   						    {a:'comment', b: '', p:'subinfo', c:'', l:'', q:'down', cc: 'black' }, 
   						    {a:'notestext', b: '', p:'subinfo1', c:'', l:'', q:'down', cc: 'black' }, 
   						    {a:'notescode', b: '', p:'subinfo2', c:'', l:'', q:'down', cc: 'black' }, 
   						    {a:'birth', b: '', p:'subinfo3', c:'date', l:'', q:'down', cc: 'black' }, 
   						    {a:'lastvisit', b: 'last visit: ', p:'subinfo4', c:'date', l:'', q:'down', cc: 'black' },
   						    {a:'firstvisit', b: 'first visit: ', p:'subinfo5', c:'date', l:'', q:'down', cc: 'black' },
   						    {a:'repeatguest', b: 'repeat: ', p:'subinfo6', c:'', l:'', q:'down', cc: 'black' },
 						    {a:'additioninfo', b: 'additional info: ', p:'subinfo7', c:'', l:'', q:'down', cc: 'black' },
     						{a:'profilcode', b: 'code: ', p:'subinfo8', c:'', l:'', q:'down', cc: 'black' },
     						{a:'payment', b: 'payment: ', p:'subinfo4', c:'', l:'', q:'down', cc: 'black' },
   						    ];

/*
				<p ng-if="x.mobile !==''" class="subinfo9">{{ x.mobile }}</p>
				<p ng-if="x.comment !==''" class="subinfo">{{ x.comment }}</p>
				<p ng-if="x.notestext !==''"  class="subinfo1">{{ x.notestext }}</p>
				<p ng-if="x.notescode !==''"  class="subinfo2">{{ x.notescode }}</p>
				<p ng-if="x.birth !==''"  class="subinfo3">last visit: {{ x.birth  | adatereverse:'date' }}</p>
				<p ng-if="x.lastvisit !==''"  class="subinfo4">last visit: {{ x.lastvisit  | adatereverse:'date'  }}</p>
				<p ng-if="x.firstvisit !==''"  class="subinfo5">first visit:{{ x.firstvisit  | adatereverse:'date'  }}</p>
				<p ng-if="x.repeatguest !==''"  class="subinfo6">repeat: {{ x.repeatguest }}</p>
				<p ng-if="x.additioninfo !==''"  class="subinfo7">additional info: {{ x.additioninfo }}</p>
				<p ng-if="x.profilcode !==''"  class="subinfo8">code: {{ x.profilcode }}</p>
*/

   $scope.predicate = 'index';
   $scope.reverse = false;
   $scope.selectedItem = [];
	$scope.dateChange = function() {
		if (angular.isDefined($scope.mydata) && $scope.mydata !== null && angular.isDefined($scope.mydata.theDate) && $scope.mydata.theDate !== null) {
			var strUrl = window.location.toString();
			var index = strUrl.indexOf("&date=");
			var dateUrl = "&date="+moment($scope.mydata.theDate).format("YYYY-MM-DD");
			var url = index < 0 ? strUrl + dateUrl : strUrl.slice(0, index)+ dateUrl;
			location.assign(url);
		}
	}

	$scope.mydata.setInit(angular.isDefined($scope.date) && $scope.date !== null ? $scope.date : new Date(), "00:00", $scope.dateChange, 5, null, null);

	$scope.readbooking = function(options) {
		var mode = angular.isDefined($scope.date) && $scope.date !== null ? "adate":"today";
		options = angular.isDefined($scope.date) && $scope.date !== null ? $scope.date : "";

		apiService.readBooking($scope.restaurant, mode, options, $scope.token).then(function(response) {
			var index = 0, slot = 0;
			var today = angular.isDefined($scope.date) && $scope.date !== null ? $scope.date : moment().format('YYYY-MM-DD');
			if(response.data.status !== 1)
				return;
			var data = response.data.data;
			var showpayment = (["SG_SG_R_Pollen", "SG_SG_R_Esquina"].indexOf($scope.restaurant) > -1);
			
			$scope.names = [];	
			$scope.totalpax = 0;
			data.map(function(x) { x.slot = x.time.timetoslot(); x.hh = parseInt(x.time.substr(0, 2)); });
			data.sort(function(x,y){ return y.vtime - x.vtime; });		// by date : return new Date(b.ddate) - new Date(a.ddate);
			data.reverse();
			data.map(function(oo) {
				if(oo.date !== today || oo.bkstatus === 'noshow' || oo.bkstatus === 'cancel')
					return;
				oo.index = index++;
				console.log('fullname', oo, oo.fullname);
				oo.fullname = oo.fullname.replace(/nofirstname/, '').replace(/nolastname/, '').trim();
				if(oo.fullname === "")
					oo.fullname = "---";
				oo.mobile = (oo.phone !== "+65 99999999") ? oo.phone : "";
				if(typeof oo.notestext === "string" && oo.notestext.length > 4)
					oo.notestext = oo.notestext.replace(/u200e/g, '');
				if(oo.mealtype === $scope.meal)
					$scope.totalpax += parseInt(oo.pers);
				oo.hourtitle = "";
				oo.payment = (showpayment && oo.booking_deposit_id !== "") ? "yes" : "";
				if(oo.slot != slot && parseInt(oo.slot / 2) !== parseInt(slot / 2)) {
					slot = oo.slot;
					oo.hourtitle = parseInt(oo.slot / 2) + ":00 ";
					}	
					$scope.names.push(oo);
				});
			$scope.paginator.setItemCount($scope.names.length);		   
			$scope.initorder();
			$scope.getProfils();
			});
		};
		
	$scope.getProfils = function() {
		$scope.names.map(function(oo) {
			oo.additioninfo = oo.birth = oo.lastvisit = oo.firstvisit = oo.repeatguest = oo.profilcode = "";
			apiService.readshortProfile($scope.restaurant, oo.email, oo.phone, $scope.token).then(function(response) {
				var obj, labelAr, infoAr = [];
				oo.profil = response.data;
				if(!oo.profil || !oo.profil.content)
					return;
				try {
					obj = JSON.parse(oo.profil.content.replace(/’/g, "\""));
					Object.keys(obj).map(function(ll) {
						if(obj[ll] instanceof Array) obj[ll] = obj[ll].join(',');
						if(typeof obj[ll] === 'string' && obj[ll].length > 0) {
							if(ll !== "codekey0000") infoAr.push(ll + '=' + obj[ll]);
							else oo.profilcode = obj[ll];
							}
						});
				} catch(e) { console.error("JSON-TMS", e.message, oo);  return; }
				oo.additioninfo = (infoAr.length > 0) ? infoAr.join(";") : "";
				oo.birth = (oo.profil.birth !== "0000-00-00") ? oo.profil.birth : "";
				oo.lastvisit = oo.profil.lastvisit;
				oo.repeatguest = (parseInt(oo.profil.repeatguest) > 0) ? oo.profil.repeatguest : "";
				if(oo.repeatguest === "")  oo.lastvisit = oo.firstvisit = "";
				});
			});
		};
			
    $scope.initorder = function() {
		$scope.predicate = "vdate";
		$scope.reverse = false;
	};

   $scope.reorder = function(item, alter) {
		alter = alter || "";
		if (alter !== "")  item = alter;
		$scope.reverse = ($scope.predicate === item) ? !$scope.reverse : false;
		$scope.predicate = item;
	};
	
   $scope.readbooking();
   			
   function getArg(label) {
   	if(typeof label !== "string" || label.length < 2) return null;
	var strAr, start = document.location.search.indexOf(label +'=');
	if(start < 0) return null;
	strAr = document.location.search.substring(start+label.length+1).split("&");
	return strAr[0]; 	
   	}
}]);