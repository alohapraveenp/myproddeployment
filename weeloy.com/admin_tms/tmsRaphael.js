/*
 *  ┌─────────────────────────────────────────────┐ 
 *  │ TMS 1.0 - JavaScript Vector Library             │ 
 *  ├─────────────────────────────────────────────┤ 
 *  │ Copyright © 2015 Weeloy. All rights reserved.   │ 
 *  ├─────────────────────────────────────────────┤ 
 *  │ Company: https://www.weeloy.com				  │ 
 *  ├─────────────────────────────────────────────┤ 
 *  │ Author Richard Kefs							  │ 
 *  └─────────────────────────────────────────────┘ 
 *
*/

app.service('tmsLayout', [ 'ModalService', function(ModalService) {
	
	var paper = null;
	var self = this;
	var draglib = null;
	var emptyTbl = null;
	var rpcolor = '#0793bb';
	var nitem = 0, size = 50, id_index = 0;
	var ObjTableContent = [];
	var myElement = [];
	var floorheight = 1000;
	var floorwidth = 1500;
	var floordefault = "floor_vynilgrey.jpg";	//"floor_tile_choc.jpg"
	
    var mydata = {name: "", id:"", npers:4, aArray:[0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20], action: "" };
    var showModal = function (title, initname, id, npers, template, qO, func) {

		if(template === null)
			template = "modalgen1.html";
		mydata.name = initname;
		mydata.id = id;
		mydata.npers = npers;
		qO.keyCodeEnable = false;
		
        var modalOptions = {
            closeButtonText: 'Cancel',
            actionButtonText: 'Save',
            headerText: title,
            mydata: mydata,
			submit:function(result){ qO.keyCodeEnable = true; $modalInstance.close(result); },
			myclose:function(){ qO.keyCodeEnable = true; $modalInstance.dismiss('cancel'); }        
        	};
        
        ModalService.setTemplate(template);
        
        var $modalInstance = ModalService.showModal({}, modalOptions);
        $modalInstance.result.then(function (result) {
             if(func) func(result);
             else queueObj.scope.alert(result.name, result.id);
        });
    };											

	var queueObj = { 
		index:0, 
		selected: -1,
		tablette: false,
		svfconsole: null,
		clock: null,
		glbdisplay: null,
		glbdisplaypaper: null,
		glbselect: null,
		fontable: null,
		fontstate: null,
		fontdeco: null,
		myfloor: null,
		bbox: null,
		colorstate: { "free":"brown", "left":"yellow", "to come":"purple", "waiting":"purple", "seated":"red", "partially seated":"pink", "main course":"green", "not ready":"marron", "paying":"gold", "blocked":"black", "no show":"brown", "clear":"cyan" },
		objselect: {x:0, y:0, width:0, height:0 },
		groupObj: null,
		allobj: [],
		data: [],
		floors: [],
		floorname: '',
		otherObj: [],
		layoutName: "",
		captains: "",
		mealtype: "",
		afternoontea: "",
		axes: null,
		keyCodeEnable: true,
		ytrash: 0,
		previous: -1,
		editmode: -1,
		mydata: mydata,
		prompt:0,
		fixedviewmode: 0,
		viewlinks: true,
		hoverviewer: false,
		preferences: {},
		selectedbooking: ["", "", 99, "", ""], // booking, tbname, pax, state, nxtbkg
		scope: null, // this is to trigger top menu event


		setClock: function(s) {
			console.log('SETTING THE CLOCK', s, (queueObj.clock !== null));
			if(queueObj.clock !== null)
				queueObj.clock.attr({text: s });
			},

		setFixView: function(flg) {
			this.fixedviewmode = flg;
			console.log('FIXED View', this.fixedviewmode, flg);
			},
			
		setselectedbooking: function(req, arg) {
			switch(req) {
				case "all":
					if(arg instanceof Array === false || arg.length !== this.selectedbooking.length)
					return;
					for(var i = 0; i < arg.length; i++) 
						if(typeof arg[i] !== typeof this.selectedbooking[i])
							arg[i] = (typeof this.selectedbooking === "string") ? "" : 99;
					
					this.selectedbooking = arg.slice(0);
					break;
				case "booking":
					this.selectedbooking[0] = arg;
					break;
				case "tablename":
					this.selectedbooking[1] = arg;
					break;
				case "pax":
					this.selectedbooking[2] = arg;
					break;
				case "state":
					this.selectedbooking[3] = arg;
					break;
				case "nextbooking":
					this.selectedbooking[4] = arg;
					break;
				}
			},
			
		getselectedbooking: function(req) {
			switch(req) {
				case "booking": return this.selectedbooking[0];
				case "tablename" : return this.selectedbooking[1];
				case "pax": return this.selectedbooking[2];
				case "state": return this.selectedbooking[3];
				case "nextbooking": return this.selectedbooking[4];
				case "all": return this.selectedbooking;
				default: return "";
				}
			},
			
		setprompt: function(prompt) {
			this.prompt = prompt;
			},
			
		setPref: function(pref, value) {
			this.preferences[pref] = value;
			},
			
		getaPref: function(pref) {
			return (typeof this.preferences[pref] === 'number' && this.preferences[pref] === 1);	
			},
									
		myalert: function(val, force) {
			if(this.editmode === 1 || (this.prompt === 1 && typeof force === "undefined"))
				return alert(val);
			else queueObj.scope.alert(val, force); // apply
			},

		myconfirm: function(val, force) {	// apply not working with confirm
			if(this.editmode === 1 || (this.prompt === 1 && typeof force === "undefined"))
				return confirm(val);
			return true;
			},
						
		getfloors: function() {
			return this.floors;
			},
			
		addfloors: function(floor) {
			if(typeof floor === "string" && floor !== "") {
				floor = floor.replace(/^.*\//g, '');
				this.floors.push(floor);
				}
			},

		sortfloors: function() {
			if(this.floors.length > 1)
				this.floors.sort();
			},
			
		getfloorname: function() {
			return this.floorname;
			},
		
		enablecaptains: function() {
			this.captains = "1";
			},
				
		setfloorname: function(floor) {
			if(this.floors.indexOf(floor) < 0 && floor !== "MULTIPLEFLOOR")
				floor = floordefault;
			this.floorname = floor; 
			if(floor !== "MULTIPLEFLOOR")
				this.myfloor.attr({fill: 'url(images/' + floor + ')'});
			},

		createfloor: function(floor) {
			var self = this;
			if(this.floors.indexOf(floor) < 0)
				floor = floordefault;
			this.floorname = floor;
			this.myfloor = paper.rect(0, 0, floorwidth, floorheight).attr({fill: 'url(images/' + floor + ')'});
			if(this.editmode !== 0) {			
				this.myfloor.click(function(){ 	self.resetSquare(); self.previous = -1; if(self.glbselect) self.glbselect.remove(); });
				this.myfloor.dblclick(function(e) {
					var offset = $('#raphael').offset();
					if(this.glbselect) this.glbselect.remove();
					this.glbselect = paper.rect(e.pageX - (offset.left + 5), e.pageY - (offset.top+5), 10, 10).attr({fill: "#000", "fill-opacity": 0, stroke: "#0ff", "stroke-width": 1, "stroke-dasharray":"-"});
					this.glbselect.data("x", e.pageX - (offset.left+5));
					this.glbselect.data("y", e.pageY - (offset.top+5));
					this.glbselect.drag(draglib.dragrectmove, draglib.dragrectstart, draglib.dragrectend);
					//this.glbselect.trigger("drag");
					}); 
				}
			},	
			
		clear: function() {
			this.index = 0;
			this.selected = -1;
			if(this.glbselect) this.glbselect.remove();
			if(this.glbdisplay) this.glbdisplay.remove();
			if(this.myfloor) this.myfloor.remove();
			if(this.bbox) this.bbox.remove();
	
			queueObj.data = [];
			queueObj.otherObj = [];
			queueObj.keyCodeEnable = false;
			},

		erase: function(ind) {
			var tbl = this.data[ind];
			this.groupObj.erase(tbl);
			this.resetSquare();
			if(tbl.growsqr) tbl.growsqr.remove();
			if(tbl.turncirc) tbl.turncirc.remove();
			if(tbl.perimeter) tbl.perimeter.remove();
			if(tbl.obj) {
				if(tbl.obj.display) tbl.obj.display.remove();
				tbl.obj.remove();
				}
			if(tbl.objresa) tbl.objresa.erase();
			tbl.conObj.clear();
			
			this.data.splice(ind, 1);
			this.index--;
			},
				
		clean: function() {
			var i, cn;

			for(i = cn = 0; i < this.data.length; i++) 
				if(this.data[i].npers > 0 && zeroplace(this.data[i].name)) {
					this.data[i].npers = 0;
					cn++;
					}
			this.myalert('Number of object cleaned: '+cn);
			},

		setEditmode: function(editmode) {
			this.editmode = editmode;
			},
			
		getEditmode: function(editmode) {
			return this.editmode;
			},

		setViewerconnect: function(arg) {
			var i, cmd;
			if(arg instanceof Array === false || arg.length < 1)
				return;
			for(i = 0; i < arg.length; i++)  {
				cmd = arg[i];
				switch(cmd) {
					case "Hide Viewer" : 
					case "Show Viewer" : 
						if(cmd !== "Show Viewer") 
							this.svfconsole.show();
						else this.svfconsole.hide();
						break;
						
					case "Viewer in Back":
					case "Viewer in Front":
						if(cmd !== "Viewer in Back") {
							this.svfconsole.toBack();
							this.glbdisplaypaper.toBack();
							this.svfconsole.update = null;
							if(queueObj.myfloor) 
								queueObj.myfloor.toBack();
							}
						else {
							this.svfconsole.update = function() { queueObj.svfconsole.toFront();};
							this.svfconsole.toFront();
							this.svfconsole.show();
							}
						break;
						
					case "Hover":
					case "No Hover":
						this.hoverviewer = (cmd !== "Hover");
						if(this.hoverviewer) {	// reset previous transformation
							this.svfconsole[0].transform("");
							this.svfconsole[1].transform("");
							}
						break;
						
					case "Show Connections":
					case "Hide Connections":
						this.viewlinks = (cmd !== "Show Connections");
						break;
					}
				}
			},
						
		getTableInfo: function() {
			var i, tbl, val, alltables=[], error=[], oo = {};
			for(i = 0; i < this.data.length; i++)
				if(this.data[i].label.substring(0, 7) !== "no name") {
					tbl = this.data[i];
					
					if(tbl.npers <= 0) continue;
					
					if(alltables.indexOf(tbl.label) !== -1) {
						error.push(tbl.label + "," + tbl.npers);
						continue;
						}
					
					if(typeof tbl.captain !== "string")	
						tbl.captain = "";
						
					val = tbl.npers;
					if(tbl.attribution > 0) // manual attribution only
						val += 0x100;
					alltables.push([val, tbl.label, tbl.npers, tbl.captain, tbl.mealtype]);
					}
			alltables.sort(function(a, b) { var dd = (a[0] & 0xff) - (b[0] & 0xff); return (dd !== 0) ? dd : ((a[1] > b[1]) ? 1 : -1); });
			for(i = 0; i < alltables.length; i++)
				oo[alltables[i][1]] = alltables[i][0];
			
			if(error.length > 0) this.myalert('!!!! DUPLICATE TABLE NAMES = ' + error.join(', '));
			return { data: oo, fulldata: alltables };
			},
			
		allocateResa: function(tbl) {
			var conf = this.getselectedbooking("booking"), 
				tbname = this.getselectedbooking("tablename"), 
				pax = this.getselectedbooking("pax"), 
				state = this.getselectedbooking("state");
			
			if(this.getaPref('unmatchtablesize')) 
				pax = 0;	
				
			if(conf.length < 6 || tbname !== "" || pax > tbl.npers || state === "no show")
				return;
			
			tbname = tbl.label;	
			if(this.myconfirm("Confirm that you want to attribute the table '"+tbl.label+"' to booking "+conf) === false)
				return;

			this.setselectedbooking("tablename", tbname);
			queueObj.scope.assigntablebkgfromlayout(conf, tbl.label, 'add');			
			},
				
		reassignResa: function(tbl, conf, npers, box) {
			var i, cc, dd, x = box.x+25, y = box.y, w = 25, h = 25, tt, hlight, mode;
			
			for(i = 0; i < this.data.length; i++) {
				tt = this.data[i];
				if(Math.abs(tt.cx - x) < w && Math.abs(tt.cy - y) < h && tt.npers > 0) 
					break;
				}

			if(i >= this.data.length || !tt.objresa || tt.label === tbl.label)
				return tbl.objresa.redraw();
			
			cc = tt.objresa.getresa(); 
			dd = tbl.objresa.getresa(); 
			if(dd[4] === "" && this.getaPref('unmatchtablesize') === false && tt.npers < npers)
				return tbl.objresa.redraw();
			mode = (dd[4] === "") ? "reallocate" : "connect";
			if(cc[2] > 0 && cc[0] !== conf) {
				tbl.objresa.redraw();
				this.myalert('Table '+ tt.label + ' is already occupied. System cannot '+ mode + ' the table for booking = '+conf);
				return;
				}
				
			if(cc[2] > 0 && cc[0] === conf) {
				tbl.objresa.redraw();
				if(this.myconfirm('Do you want to deconnect the booking ' + conf + ' ' + tbl.label + ' from table '+tt.label) === false)
					return;
				return queueObj.scope.assigntablebkgfromlayout(conf, tt.label, 'delete'); // apply
				}
				
			if(this.myconfirm('Do you want to '+ mode + ' the booking ' + conf + ' ' + tbl.label + ' to table '+tt.label) === false)
				return tbl.objresa.redraw();

			if(mode === "connect") {
				tbl.objresa.redraw();
				return queueObj.scope.assigntablebkgfromlayout(conf, tt.label, 'add');	// apply
				}
				
			hlight = tbl.objresa.gethlight();
			
			tbl.objresa.erase();
			tbl.objresa.setresa("", "", 0, "", -1, "", "", "", "", "");

			tt.objresa.setresa(dd[0], dd[1], dd[2], dd[3], hlight, dd[4], dd[5], dd[6], dd[7], dd[8], dd[9]);
			tt.objresa.showresa();
			tt.objresa.redraw();
			queueObj.scope.assigntablebkgfromlayout(conf, tt.label, 'change');  // apply
			},
		
		setallnextbkg: function(obj) {
			var i, tbname, oo;
			for(i = 0; i < this.data.length; i++) {
				tbname = this.data[i].label;
				this.data[i].nxtbkg = "";
				this.data[i].fullbkg = "";
				if(obj[tbname] instanceof Array) {
					this.data[i].fullbkg = oo = obj[tbname];
					this.data[i].nxtbkg = oo[0].booking + '\t' + oo[0].time + '\t' + oo[0].fullname + '\t' + oo[0].npers;
					}
				}
			},
				
		showResaInfo: function(tables, booking, guestname, pax, state, hlight, nxtbkg, captain, duration, cleartime, seated, flg) {
			var i, k, tablename, tmpA, tbl = null, tt;
			tables = tables || "";

			if(tables === "") return;
			if(tables.indexOf(",") >= 0)
				tmpA = tables.split(",");
			else tmpA = [tables];

			for(k = 0; k < tmpA.length; k++) {
				tablename = tmpA[k];
				for(i = 0; i < this.data.length; i++) {
					if(this.data[i].label === tablename)
						break;
					}
				if(i >= this.data.length) 
					continue;

				tt = this.data[i];				
				tt.objresa.setresa(booking, guestname, pax, nxtbkg, null, captain, duration, cleartime, seated, flg);
				tt.objresa.setstate(state).highlight();
				tt.objresa.showresa();
				if(hlight !== 0) {
					tt.objresa.setbackgroundcolor(hlight);	
					}	

				if(tbl === null) tbl = tt;
				else if(tbl.conObj.isconnect(tt) === false) tbl.conObj.connect(tt);					
				}			
			},

		resetResaInfo: function(tables, booking) {
			var i, k, tmpA, tablename, confA, tbl = null, tt;
			if(tables === "" || typeof tables !== 'string') return;
			if(tables.indexOf(",") >= 0)
				tmpA = tables.split(",");
			else tmpA = [tables];
			
			for(k = 0; k < tmpA.length; k++) {
				tablename = tmpA[k];
				for(i = 0; i < this.data.length; i++) {
					if(this.data[i].label === tablename)
						break;
					}
				if(i >= this.data.length) 
					continue;
			
				tt = this.data[i];
				confA = tt.objresa.getresa();
				if(confA[0] === booking) {
					tt.objresa.setresa("", "", 0, "", null, "", "", "");
					tt.objresa.setstate("clear");
					}
				if(tbl === null) tbl = tt;
				else if(tbl.conObj.isconnect(tt)) tbl.conObj.deconnect(tt);					
				}
			},
				
		getTblSelected: function() {
			return this.groupObj.getSelectedTbl();
			},
					
		getTblId: function(id) {
			var ind;
			
			if(id < 0 || (ind = this.getIndexId(id)) < 0)
				return null;
			return this.data[ind];
			},
					
		getcolorPicker: function() {
			return this.colorstate;
			},

		printmsg: function(msg) {
			if(queueObj.glbdisplay)
				queueObj.glbdisplay.attr('text', msg);
			},
							
		setLayoutName: function(name) {
			this.layoutName = name;
			},

		setLayoutPrefixtable: function(prefixtable) {
			this.prefixtable = prefixtable;
			},

		setLayout: function(obj, namefilter) {
			var ind, i, k, limit, vv, selector, tbl, tt, tmpAr, color, fontcolor, radiant, fillcolor, captain, mealtype;
			
			// clear all the table
			for(i = 0; this.data.length > 0 && i < 500; i++) {
				this.erase(this.data.length - 1);
				}

			for(i = 0; i < this.allobj.length; i++) {
				if(this.allobj[i] && this.allobj[i].id) {
					if(this.allobj[i].display)
						this.allobj[i].display.remove();
					this.allobj[i].remove();
					}
				}
			
			this.allobj = [];
			this.data = [];
			
			// clear all additionnal objects			
			for(i = this.otherObj.length; i > 0; i--) {
				if(tt = this.otherObj.pop()) 
					tt.remove();
				}
				
			this.printmsg('Drag me');
				
			limit = obj.nelement;
			this.layoutName = obj.layoutName;
			this.captains = (typeof obj.captains === 'string') ? obj.captains : "";
			this.mealtype = (["SG_SG_R_Pollen", "SG_SG_R_TheFunKitchen"].indexOf(this.scope.restaurant) > -1) ? "1" : "0",
			this.afternoontea = this.mealtype;

			this.prefixtable = (typeof obj.prefixtable === 'string') ? obj.prefixtable : "";
			for(i = 0; i < limit; i++) {
				vv = this.normalise(obj.data[i]);
				selector = ObjTableContent.length;
				fillcolor = (typeof vv.fill === 'string') ? vv.fill : "";
				color = (typeof vv.color === 'string') ? vv.color : "";
				fontcolor = (typeof vv.color === 'string') ? vv.font : "";
				radiant = (typeof vv.radiant === 'string') ? vv.radiant : "";
				captain = (typeof vv.captain === 'string') ? vv.captain : "";
				mealtype = (typeof vv.mealtype === 'number') ? vv.mealtype : 0;

				if(this.captains === "1" && typeof namefilter === "string" && namefilter !== "" && namefilter !== captain && vv.npers > 0)
					continue;
					
				ObjTableContent.push({ type: vv.type, x: [vv.x, vv.y, vv.width, vv.height], width: vv.width, height: vv.height, angle: vv.angle, name: 'images/'+vv.src, label: vv.label, npers: vv.npers, attribution: vv.attribution, mealtype: mealtype, z: vv.z, scaling: 1, state: 1, stroke: 1, color: color, fill: fillcolor, font: fontcolor, radiant: radiant, subtype: 'table', captain: captain });
				tbl = new this.createObj(selector);
				tbl.conObj.mydata("data", vv.connection);
				if(vv.state !== "") tbl.objresa.setstate(vv.state).show();
				this.insert(tbl);
				this.resetSquare();
				this.setSquare(tbl);
				}

			limit = this.data.length;
			for(i = 0; i < limit; i++) {
				tbl = this.data[i];
				tt = tbl.conObj.mydata("data");
				if(tt === "") continue;
				tmpAr = tt.split(',');
				for(k = 0; k < tmpAr.length; k++) {
					ind = this.getIndexLabel(tmpAr[k]);
					tbl.conObj.connect(this.data[ind]);
					}
				}

			if(obj.floor !== "undefined" && obj.floor !== "")
				this.setfloorname(obj.floor);
			this.resetSquare();
			
			if(obj.floor === "MULTIPLEFLOOR") {
				this.printmsg('This is a multiple floor layout.\nEditing will be disregarded/not saved');
				}
				
			},			
					
		normalise: function(obj) {
			obj.x = parseFloat(obj.x);
			obj.y = parseFloat(obj.y);
			obj.width = parseFloat(obj.width);
			obj.height = parseFloat(obj.height);
			obj.angle = parseFloat(obj.angle);
			if(typeof obj.z === 'undefined') obj.z = 1;
			if(typeof obj.attribution === 'undefined') obj.attribution = 0;
			if(typeof obj.mealtype === 'undefined') obj.mealtype = 0;
			obj.z = parseInt(obj.z);
			obj.attribution = parseInt(obj.attribution);
			obj.mealtype = parseInt(obj.mealtype);
			return obj;
			},
				
		setinfo: function(data) {
			var id, ind, tbl, i, renew;
							
			id = parseInt(data.id);
			if(id < 0 || (ind = this.getIndexId(id)) < 0)
				return -1;

			data.name = data.name.replace(/[^\d\w]/g, "");
			if(typeof data.name !== 'string' || data.name === '') {
				this.myalert("This is an empty string. Table has NOT been renamed.");
				return;
				}
				
			for(i = 0; i < this.data.length; i++)
				if(this.data[i].label.substring(0, 7) !== "no name") 
					if( this.data[i].label === data.name && this.data[i].id !== id && parseInt(this.data[i].npers) > 0) {
						this.myalert("!!! DUPLICATE NAMES :" + data.name + ". Table has NOT been renamed. " + this.data[i].npers);
						return;
						}
				
			tbl = this.data[ind];
			tbl.label = data.name;
			if(tbl.type === "i") {
				tbl.npers = parseInt(data.npers);
				tbl.attribution = (data.flagAr[0].v) ? 1 : 0;
				tbl.mealtype = (data.flagAr[1].v) ? 1 : 0;
				tbl.mealtype += ((data.flagAr[2].v) ? 2 : 0);
				tbl.mealtype += ((data.flagAr[3].v) ? 4 : 0);
			} else {
				tbl.fill = data.fill;
				tbl.color = data.border;
				tbl.font = data.font;
				renew = (tbl.radiant === "" && data.radiant !== "");
				tbl.radiant = data.radiant;	
				if(renew === false)
					tbl.obj.attr({ 'stroke': hslToRgb(tbl.color), 'stroke-opacity': opacity(tbl.color), 'fill': hslToRgbRdnt(tbl.fill, tbl.radiant, tbl.type), 'fill-opacity': opacity(tbl.fill) });
				else queueObj.genObj(tbl, tbl.name);
				}
				
			if(tbl.obj.display) 
				tbl.obj.display.remove();
				
			if(tbl.npers !== 0)
				queueObj.papertext(tbl);
			},
		
		papertext: function(tbl) {
			var pp, display, pset;
			var x = tbl.cx - 12, y = tbl.cy, size = (tbl.npers > 0) ? 10 : 16;
			var font = (tbl.npers > 0) ? this.fontable : this.fontdeco;
			if(tbl.label && tbl.label.length > 5)
				x -= ((tbl.label.length - 5) * (4 + (size - 10)));
			pset = paper.set();
			if(this.editmode === 0) // make it bigger
				pp = paper.rect(x-5, y-20, 30, 40, 5).attr({"fill":"white", 'stroke':'none', 'fill-opacity': '0.01', 'cursor':'default'});
			else pp = paper.rect(x-5, y-10, 5, 5).attr({"fill":"white", 'stroke':'none', 'fill-opacity': '0.01', 'cursor':'default'});
	
			//display = paper.text(tbl.cx - 12, tbl.cy, tbl.label).attr({'text-anchor': 'start', 'x': x, 'cursor': 'default', 'font-size': '10px', 'font-family': 'helvetica', 'fill': hslToRgb(tbl.font), 'fill-opacity': opacity(tbl.font) });
			display = paper.print(x, y, tbl.label , font, size).attr({'cursor': 'default', 'fill': hslToRgb(tbl.font), 'fill-opacity': opacity(tbl.font) });
			pset.push(pp);
			pset.push(display);
			pset.data("wraper", tbl);
			if(this.editmode > 0) {
				pset.attr("cursor", "pointer");	
				pset.drag(draglib.dragmove, draglib.dragstart, draglib.dragend);	
				pset.click(function(e){ queueObj.qclick(e, this); });
				pset.dblclick(function(e){ queueObj.qdblclick(e, this); });
				}
			else {
				pset.click(function(e){ var tbl = this.data("wraper"); queueObj.tblClick(e, tbl); });
				pset.dblclick(function(e){ var tbl = this.data("wraper"); queueObj.tbldblClick(e, tbl); });
				if(queueObj.tablette)
					pset.hover(function(e) { var tbl = this.data("wraper"); queueObj.tbldblClick(e, tbl, ''); });
				else pset.hover(function(e) { var tbl = this.data("wraper"); queueObj.tblHover(e, tbl, ''); });
				}
			tbl.obj.display = pset;
			},
			
		triggeranim: function(booking, mode) {
			var i, cc;
			for(i = 0; i < this.data.length; i++) 
				if(this.data[i].objresa) {
					cc = this.data[i].objresa.getresa();
					if(cc[0] === booking)
						return this.data[i].objresa.triggeranim(mode);
					}
			},
			
		setstate: function(id, state) {
			var ind;
					
			if(id < 0 || (ind = this.getIndexId(id)) < 0)
				return -1;
				
			this.data[ind].objresa.setstate(state).highlight();
			},
				
		clearstate: function(id) {
			this.setstate(id, 'clear');
			},
				
		clearallstate: function() {

			for(var i = 0; i < this.data.length; i++) {
				this.data[i].objresa.setstate('clear');
				this.data[i].conObj.deconnectAll();
				}
			},
				
		emptylist: function(name) {
			var object = {
				"layoutName": name,
				"restaurant": this.scope.restaurant,
				"nelement": 0,
				"floor": floordefault,
				"captains": "",
				"data": []				
				};
			return object;
			},						

		list: function() {
			var i, tbl;
			var object = {
				"layoutName": this.layoutName,
				"restaurant": this.scope.restaurant,
				"nelement": this.data.length,
				"floor": this.floorname,
				"captains": this.captains,
				"prefixtable": this.prefixtable,
				"data": []				
				};
						
			for(i = 0; i < this.data.length; i++) {
				if(this.data[i].label === "no name")
					this.data[i].label += " " + this.data[i].id;
				}

			for(i = 0; i < this.data.length; i++) {
				tbl = this.data[i];
				object.data.push(
					{ 
					"id": tbl.id,
					"label": tbl.label,
					"npers": tbl.npers,
					"attribution": tbl.attribution,
					"mealtype": tbl.mealtype,
					"type": tbl.type,
					"src": tbl.name.replace(/.*\//g, ''),
					"x": tbl.bxy[0].myPrecision(3),
					"y": tbl.bxy[1].myPrecision(3),
					"angle": tbl.angle.myPrecision(3),
					"z": tbl.z,
					"color": tbl.color,
					"fill": tbl.fill,
					"font": tbl.font,
					"radiant": tbl.radiant,
					"width": tbl.width.myPrecision(3),
					"height": tbl.height.myPrecision(3),
					"connection": tbl.conObj.getToConnection(),				
					"captain": tbl.captain,				
					"state": tbl.objresa.getstate()				
					});
				}
			return object;
			},
			
		insert: function(tbl) {
			if(this.editmode === 1) {
				var axisy = [ 'accra', 'adan', 'adana', 'ageo', 'agra', 'ahwaz', 'akesu', 'amman', 'anda', 'anlu', 'anqiu', 'arak', 'baku', 'baoji', 'bari', 'batam', 'benxi', 'bijie', 'bogor', 'bole', 'bonn', 'botou', 'brno', 'bursa', 'busan', 'cairo', 'cali', 'cebu', 'cenxi', 'chiba', 'cixi', 'daan', 'daegu', 'dakar', 'dali', 'davao', 'daye', 'dehui', 'delhi', 'dhaka', 'doha', 'dubai', 'durg', 'embu', 'enshi', 'essen', 'ezhou', 'fife', 'fuan', 'fuji', 'fuxin', 'gaoan', 'gaomi', 'gaya', 'gaza', 'gent', 'gifu', 'giza', 'graz', 'guixi', 'gumi', 'hama', 'hami', 'haora', 'hebi', 'hefei', 'heze', 'homs', 'iasi', 'içel', 'ipoh', 'izmir', 'jammu', 'jeju', 'jian', 'jilin', 'jimo', 'jixi', 'kabul', 'kano', 'karaj', 'kayes', 'kazan', 'kiel', 'kobe', 'kochi', 'konya', 'kota', 'kure', 'kyiv', 'kyoto', 'lagos', 'laiwu', 'laixi', 'lanxi', 'lasa', 'lima', 'linyi', 'lipa', 'liuan', 'lome', 'luxi', 'lviv', 'lyon', 'medan', 'mesa', 'miami', 'milan', 'miluo', 'minsk', 'mito', 'mopti', 'mosul', 'naha', 'nara', 'natal', 'nehe', 'nice', 'oita', 'omsk', 'oran', 'orel', 'orsk', 'osaka', 'oslo', 'otsu', 'paris', 'patna', 'perm', 'perth', 'pune', 'qixia', 'qods', 'qufu', 'quito', 'rabat', 'rasht', 'riga', 'rome', 'rugao', 'ruian', 'safi', 'sakai', 'salem', 'sari', 'segou', 'seoul', 'sfax', 'sofia', 'soka', 'suez', 'sumy', 'surat', 'suwon', 'taian', 'tampa', 'thane', 'tokyo', 'tomsk', 'tula', 'tunis', 'tver', 'ulsan', 'vigo', 'wuan', 'wuhan', 'wuhu', 'wujin', 'wuwei', 'wuxi', 'wuxue', 'xinji', 'xinmi', 'xinyi', 'xinyu', 'yaan', 'yazd', 'yibin', 'yidu', 'yiwu', 'yulin', 'yushu', 'yuxi', 'yuyao', 'zaria', 'zhuji', 'zibo', 'zunyi'];
				if(tbl.label.substring(0, 7) === "no name") 
					tbl.label = (this.data.length < axisy.length) ? axisy[this.data.length] : "ZZ" + this.data.length;
				}
			
			this.data.push(tbl);
			this.index = this.data.length;
			},
				 					
		triggerSave: function() {
			queueObj.scope.lysave();
			},
		
		triggerBooking: function(e, tbl, conf) {
			queueObj.scope.triggerBooking(conf); // apply
			queueObj.tblHover(e, tbl, conf); 
			},
			
		tblClick: function(e, tbl) {
			if(queueObj.tablette || true)
				return queueObj.tbldblClick(e, tbl);

			livedisplay(tbl, 'hover', queueObj); 
			},
					
		tbldblClick: function(e, tbl) {
			var self = queueObj, dd, conf;
			
			if(tbl.objresa && queueObj.fixedviewmode !== 1) {
				dd = tbl.objresa.getresa();
				tb_conf = dd[0];
				conf = self.getselectedbooking("booking"); 

				if(self.getselectedbooking("state") === "no show") {
					if(tbl.objresa.display) 
						tbl.objresa.display.attr({"cursor": "default"});
					return;
					}
					
				if(tb_conf === conf)
					tbl.objresa.setmode('connect');
				else if(tb_conf === "not-attributed") 
					self.allocateResa(tbl);
				}
				
			livedisplay(tbl, 'hover', queueObj); 
			},
					
		tblHover: function(e, tbl, conf) {
			if(tbl.objresa) {
				tbl.objresa.front(); 
				} 
			if(queueObj.hoverviewer) {
				if(conf !== "") {
					var dx, dy, attr, oo = queueObj.svfconsole, pp;
					var x = parseInt(tbl.bxy[0])+20, y = parseInt(tbl.bxy[1])+50;
					oo.toFront();			
					oo.show();
					pp = oo[0]; // rect
					pp.attr({x: x, y: y});
					pp = oo[1]; // text
					pp.attr({x: x+20, y: y+90});
					livedisplay(tbl, 'hover', queueObj); 
					}
				}			
			else livedisplay(tbl, 'hover', queueObj); 
			},

		tblHoverOut:  function(e, tbl, conf) {
			if(queueObj.hoverviewer) {
				queueObj.glbdisplay.attr({text: "" }); 
				queueObj.svfconsole.hide();
				}
			},

		qclick : function(e, oo) {
			var that = oo.data("wraper");
			var self = queueObj;

			if(queueObj.editmode < 1) 
				return queueObj.tblClick(e, that);
				

			if(that.bxy[0] < 50) return;
			if(self.groupObj.getSelectedTbl() !== that){
				self.groupObj.setSelectedTbl(that);
				that.objresa.resume();
				}
			if (e.shiftKey) {
				var aTbl;
				if(self.previous >= 0) {
					if(self.groupObj.getnbSelectTbl() > 0) {	
						return self.groupObj.insert(that);
						}
					else if(aTbl = self.getTblId(self.previous)) {
						self.groupObj.insert(that);
						self.groupObj.insert(aTbl);
						return;
						}
					}
				}
			self.previous = that.id;
			self.resetSquare();
			self.setSquare(that);

			},
			
		qdblclick : function(e, oo) {
			var tbl = oo.data("wraper"), self = queueObj, template;

			if(queueObj.editmode < 1) 
				return queueObj.tbldblClick(e, that);

			tbl.bxy = getMatrix(tbl); 
			self.printmsg('double clicking id '+tbl.id);
			template = "modalgen1.html";
			if(tbl.npers > 0) {
				template = "modalgen3.html";
				self.mydata.captainflag = (self.captains === "1"); 
				self.mydata.flagAr = [{l:"manual table attribution only", v: (tbl.attribution !== 0) }];
				if(self.mealtype === "1") {
					self.mydata.flagAr.push({l:"lunch auto-attribution only", v: ((tbl.mealtype & 1) !== 0)});
					self.mydata.flagAr.push({l:"dinner auto-attribution only", v: ((tbl.mealtype & 2) !== 0)});
					if(self.afternoontea === "1")
						self.mydata.flagAr.push({l:"Afternoon Tea auto-attribution only", v: ((tbl.mealtype & 4) !== 0)});
					}
				}
			else if(tbl.type !== "i") {
				template = "modalgen4.html";
				self.mydata.fill = (typeof tbl.fill === 'string' && tbl.fill.length > 6) ? tbl.fill : "#ff0000";
				self.mydata.border = (typeof tbl.color === 'string' && tbl.color.length > 6) ? tbl.color : "#00ff00";
				self.mydata.font = (typeof tbl.font === 'string' && tbl.font.length > 6) ? tbl.font : "#000000";
				self.mydata.radiant = (typeof tbl.radiant === 'string' && tbl.radiant.length > 6) ? tbl.radiant : "";
				console.log('TEMPLATE', self.mydata, tbl);
				}
				
			showModal('Table Information', tbl.label, tbl.id, tbl.npers, template, queueObj, function(data) { queueObj.setinfo(data); });
			},
			
		setdisplay: function(tbl) {
			if(tbl.obj.display)
				tbl.obj.display.remove();

			if(tbl.npers !== 0 && tbl.label.substring(0, 7) !== "no name") 
				queueObj.papertext(tbl);
			},
			
		shownameTbl: function() {
			var tbl = this.groupObj.getSelectedTbl();
			if(tbl === null)
				return;
			
			if(tbl.npers === 0) tbl.npers = -1;		// show the name
			else if(tbl.npers === -1) tbl.npers = 0;		// don't show the name
			else return;
			
			this.setdisplay(tbl);	
			},


		renameTbl: function() {
			var tbl, tblAr = this.groupObj.getAllSelectTbl();
			if(tblAr.length === 1) {
				tbl = tblAr[0];
				if(tbl && tbl.obj)
					this.qdblclick(null, tbl.obj);
				}
			},
					
		deleteTbl: function(tbl) {
			var i, ind, tblAr;
			
			if(typeof tbl !== "undefined") tblAr = [ tbl ];
			else tblAr = this.groupObj.getAllSelectTbl();
				
			if(tblAr.length < 1)
				if(tbl = this.groupObj.getSelectedTbl())
					tblAr.push(tbl);

			for(i = 0; i < tblAr.length; i++) {
				tbl = tblAr[i];
				if(this.myconfirm("Confirm that you want to delete the table '"+tbl.label+"'", 1) === false)
					continue;
				if((ind = this.getIndexId(tbl.id)) < 0)
					continue;
					
				this.printmsg('deleted ' + tbl.id);
				this.erase(ind);			
				}
			return 1;
			},
		
		getIndexId: function(id) {
			for(var ind = 0; ind < this.data.length; ind++)
				if(this.data[ind].id === id)
					return ind;
			return -1;
			},

		getIndexLabel: function(label) {
			for(var ind = 0; ind < this.data.length; ind++)
				if(this.data[ind].label === label)
					return ind;
			return -1;
			},
						
		tblselect: function(tbl) {
			this.reNewImg(tbl);
			tbl.obj.undrag();
			tbl.obj.drag(draglib.draggrpmove, draglib.draggrpstart, draglib.draggrpend);
			tbl.perimeter = getBox(tbl.bxy, 0, tbl, paper);
			tbl.perimeter.data('wraper', tbl);
			tbl.objresa.hide();
			},
			
		tbldeselect: function(tbl) {
			tbl.bxy = getMatrix(tbl);
			this.reNewImg(tbl);
			tbl.objresa.resume();
			this.setdisplay(tbl);
			},
					
		startgrpDrag: function() {
			this.displaygrpHide();
			},
						
		endgrpDrag: function() {
			//this.connectgrpShow();
			this.groupObj.clear(); 
			},

		displaygrpHide: function() {
			var i, tblAr = this.groupObj.getAllSelectTbl();
			for(i = 0; i < tblAr.length; i++)
				if(tblAr[i].obj.display)
					tblAr[i].obj.display.remove();
			},
						
		connectgrpHide: function() {
			var i, tblAr = this.groupObj.getAllSelectTbl();
			for(i = 0; i < tblAr.length; i++)
				tblAr[i].conObj.hide();
			},
			
		connectgrpShow: function() {
			var i, tblAr = this.groupObj.getAllSelectTbl();
			for(i = 0; i < tblAr.length; i++)
				tblAr[i].conObj.partialshow(tblAr);
			},
		
		moveTbl: function(x, y, cmd) {
			var tbl, dx, dy;
				
			if(x < -1 || x > 1 || y < -1 || y > 1) return;

			if(this.groupObj.getnbSelectTbl() > 1) {	
				if(cmd === 'move') {
					this.connectgrpHide();
					this.groupObj.moveSelect(x, y);
					this.connectgrpShow();
					}
				return;
				}

			if((tbl = this.groupObj.getSelectedTbl()) === null)
				return -1;
			
			if(cmd === 'front') {
				if(x > 0) {
					if(tbl.growsqr) tbl.growsqr.toFront();
					if(tbl.turncirc) tbl.turncirc.toFront();
					if(tbl.perimeter) tbl.perimeter.toFront();
					if(tbl.obj) tbl.obj.toFront();
					tbl.z = 1;
					}			
				else {
					cmd = 'back';
					if(tbl.growsqr) tbl.growsqr.toBack();
					if(tbl.turncirc) tbl.turncirc.toBack();
					if(tbl.perimeter) tbl.perimeter.toBack();
					if(tbl.obj) tbl.obj.toBack();
					if(queueObj.myfloor) queueObj.myfloor.toBack();
					tbl.z = -1;					
					}
				}
				
			if(cmd === 'turn') {
				if(tbl.growsqr)
					tbl.growsqr.remove();
				if(tbl.turncirc)
					tbl.turncirc.remove();
				tbl.obj.attr({ transform: "...R" + x });
				tbl.perimeter.attr({ transform: "...R" + x });
				tbl.angle += x;
				tbl.bxy = getMatrix(tbl);
				}
				
			else if(cmd === 'scale') {
				tbl.growsqr.remove();
				tbl.turncirc.remove();
				if(x * y === 0) { // scale by 1 px
					dx = x;
					dy = y;
				} else { // scale by 1%
					dx = tbl.width * (parseFloat(x) / 100);
					dy = tbl.height * (parseFloat(y) / 100);
					}

				if(tbl.width + dx < 3 || tbl.height + dy < 3)
					return;

				tbl.width += dx;
				tbl.height += dy;				
				
				tbl.obj.attr({width: tbl.width, height: tbl.height});
				tbl.perimeter.attr({width: tbl.width, height: tbl.height});
				tbl.bxy = getMatrix(tbl);
				}
							
			else if(cmd === 'move') {
				tbl.obj.attr({ transform: "...T" + x + "," + y });
				if(tbl.perimeter)
					tbl.perimeter.attr({ transform: "...T" + x + "," + y });
				if(tbl.growsqr)
					tbl.growsqr.attr({ transform: "...T" + x + "," + y });
				if(tbl.turncirc)
					tbl.turncirc.attr({ transform: "...T" + x + "," + y });
				tbl.bxy = getMatrix(tbl);
				tbl.cx = ((tbl.bxy[0]+tbl.bxy[4])/2);
				tbl.cy = ((tbl.bxy[1]+tbl.bxy[5])/2);
				tbl.objresa.resume();
				tbl.conObj.show();
				queueObj.setdisplay(tbl);	
				}
				
			livedisplay(tbl, cmd, queueObj);
			},
		
		moveTblSelected: function(dx, dy) {
			this.groupObj.moveSelect(dx, dy);
			},
		
		alignV: function() {
			this.groupObj.alignSelect('v');
			},
			
		alignH: function() {
			this.groupObj.alignSelect('h');
			},
		
		connectTbl: function() {
			var tblAr = this.groupObj.getAllSelectTbl();
			if(tblAr.length !== 2) {
				this.myalert("Can only connect 2 objects");
				return;
				}
				
			return tblAr[0].conObj.connect(tblAr[1]);
			},
		
		createViewer: function(x,y) {
			var svfconsole;
			if(queueObj.svfconsole)
				queueObj.svfconsole.remove()
			if(queueObj.glbdisplay)
            			queueObj.glbdisplay.remove();
			if(queueObj.glbdisplaypaper)
            			queueObj.glbdisplaypaper.remove();

			queueObj.glbdisplaypaper = paper.rect(x, y, 200, 200, 10).attr("fill", "#ffffff");
			queueObj.glbdisplay = paper.text(x+50, y, "Drag me").attr({'text-anchor': 'start', 'x':x+20, 'y':y+90 });	//.attr({'text-anchor': 'start'})
			svfconsole = paper.set();
			svfconsole.push(queueObj.glbdisplaypaper);
			svfconsole.push(queueObj.glbdisplay).attr("cursor", "hand").drag(draglib.dragpsetmove, draglib.dragpsetstart, draglib.dragpsetend);
			svfconsole.data("wraper", svfconsole);
			svfconsole.data("parent", queueObj);
			queueObj.svfconsole = svfconsole;
			this.svfconsole.update = null;
			},	
		
		stophoverViewer: function() {
			queueObj.glbdisplay.attr({text: "" }); 
			queueObj.svfconsole.hide();
			queueObj.hoverviewer = false;
			queueObj.waitviewer = true;
			},
					
		restarthoverViewer: function() {
			if(queueObj.waitviewer === true) {
				queueObj.waitviewer = false;
				queueObj.hoverviewer = true;
				queueObj.svfconsole.show();
				}
			},
					
		deconnectTbl: function() {
			var tblAr = this.groupObj.getAllSelectTbl();
			if(tblAr.length !== 2) {
				this.myalert("Can only deconnect 2 objects");
				return;
				}
				
			return tblAr[0].conObj.deconnect(tblAr[1]);
			},
				
		connectHide: function() {
			var tbl;
			
			if(tbl = this.groupObj.getSelectedTbl())
				return tbl.conObj.hide();
			},
				
		getBBox: function() {
			var atbl, obj;
			
			if(this.bbox) {
				this.bbox.remove();
				this.bbox = null;
				return;
				}

			if(atbl = this.groupObj.getSelectedTbl()) {
				obj = atbl.obj.getBBox();
				this.bbox = paper.rect(obj.x, obj.y, obj.width, obj.height).attr("stroke", 'yellow').attr({"stroke-dasharray":"-"});
				}
			},

		resetSquare: function() {
			var i;
			
			for(i = 0; i < this.data.length; i++) {
				this.clearBoxes(this.data[i]);
				}
				
			this.groupObj.setSelectedTbl(null); 
			this.groupObj.clear(); 
			},

		setSquare: function(tbl) {
			var delta;
			
			delta = 5;
    		tbl.bxy = getMatrix(tbl);
			tbl.turncirc = null;
			
			tbl.perimeter = getBox(tbl.bxy, 0, tbl, paper);
			tbl.growsqr = getBox(tbl.bxy, 2, tbl, paper);
			tbl.growsqr.img = tbl.obj;
			tbl.growsqr.data("wraper", tbl);
			tbl.growsqr.drag(draglib.dragrmove, draglib.dragrstart, draglib.dragrend);
			if(tbl.type !== "c") {
				tbl.turncirc = getBox(tbl.bxy, 3, tbl, paper);
				tbl.turncirc.delta = delta;
				tbl.turncirc.img = tbl.obj;
				tbl.turncirc.data("wraper", tbl);
				tbl.turncirc.drag(draglib.dragcmove, draglib.dragcstart, draglib.dragcend);
				}				
			tbl.parent.groupObj.setSelectedTbl(tbl);
			},
		
		clearBoxes: function(tbl) {
			if(tbl.perimeter) tbl.perimeter.remove();
			if(tbl.growsqr) tbl.growsqr.remove();
			if(tbl.turncirc) tbl.turncirc.remove();
			},
			
		duplicateObj: function() {
			var i, tbl, tblAr, newTblAr = [], selector;
			
			tblAr = this.groupObj.getAllSelectTbl();
			tbl = this.groupObj.getSelectedTbl();
			if(tblAr.length < 1 && tbl)
				tblAr.push(tbl);
				
			for(i = 0; i < tblAr.length; i++) {
				tbl = tblAr[i];
				selector = ObjTableContent.length;
				ObjTableContent.push({ label: "no name", name: tbl.name, type: tbl.type, x: [tbl.bxy[0]+10, tbl.bxy[1]+10, tbl.width, tbl.height], width: tbl.width, height: tbl.height, angle: tbl.angle, npers: tbl.npers, attribution: tbl.attribution, mealtype: tbl.mealtype, z: tbl.z+1, scaling: tbl.scaling, state: tbl.state, stroke: 0, color: tbl.color, fill: tbl.fill, font: tbl.font, radiant: tbl.radiant, subtype: 'table', captain: tbl.captain });
				tbl = new this.createObj(selector);
				newTblAr.push(tbl);
				this.insert(tbl);
				this.resetSquare();
				this.setSquare(tbl);
				}

			this.groupObj.clear();
			for(i = 0; i < newTblAr.length; i++)
				this.groupObj.insert(newTblAr[i]);
			},
			
		reNewImg: function(tbl) {
			tbl.transfrm = 0;
			this.clearBoxes(tbl);
			queueObj.genObj(tbl, tbl.name);
			},

		setBlockTables: function(btables) {
			this.data.map(function(tbl) {
				if(btables.indexOf(tbl.label) > -1)
					queueObj.toggleImage(tbl, "blocked");
				else if(tbl.name !== "" && tbl.togglename.indexOf("_black.svg") > -1)
					queueObj.toggleImage(tbl, "reset");					
				});
			},
					
		toggleImage: function(tbl, state) {
			var imagename = tbl.name;
			switch(state) {
				case 'seated':
					imagename = tbl.name.replace('.svg', '_red.svg');
					break;
				case 'waiting':
					imagename = tbl.name.replace('.svg', '_purple.svg');
					break;
				case 'partially seated':
					imagename = tbl.name.replace('.svg', '_pink.svg');
					break;
				case 'blocked':
					imagename = tbl.name.replace('.svg', '_black.svg');
					break;
				case 'paying':
					imagename = tbl.name.replace('.svg', '_gold.svg');
					break;
				case 'main course':
					imagename = tbl.name.replace('.svg', '_green.svg');
					break;
				case 'not ready':
					imagename = tbl.name.replace('.svg', '_marron.svg');
					break;
				case 'reset':
					imagename = tbl.name;
					break;
				}
				
			//if(elementsAr.indexOf(imagename) < 0) imagename = tbl.name;
			if(tbl.togglename !== imagename) {
				tbl.togglename = imagename;
				queueObj.genObj(tbl, tbl.togglename);
				if(tbl.npers > 0) 
					queueObj.papertext(tbl);
				}
			},
		
		genObj: function(tbl, name) {
			if(tbl.obj.display) 
				tbl.obj.display.remove();
			tbl.obj.remove();
			tbl.x[0] = tbl.bxy[0]; tbl.x[1] = tbl.bxy[1]; tbl.x[3] = tbl.width; tbl.x[4] = tbl.height; 
			tbl.cx = ((tbl.bxy[0]+tbl.bxy[4])/2);
			tbl.cy = ((tbl.bxy[1]+tbl.bxy[5])/2);
			tbl.obj = queueObj.drawObj(tbl, name); //paper.image(imagename, tbl.bxy[0], tbl.bxy[1], tbl.width, tbl.height);
			},
					
		drawObj: function(tbl, name, subtype) {
			var oo, hh, cursor;
			switch(tbl.type) {
			  case "p":
				oo = paper.path(NGon(tbl.x[0], tbl.x[1], tbl.x[2], tbl.x[3]));
				break;
			  case "r":
				oo = paper.rect(tbl.x[0], tbl.x[1], tbl.width, tbl.height);
				break;
			  case "s":
				oo = paper.rect(tbl.x[0], tbl.x[1], tbl.width, tbl.height, 10);
				break;
			  case "c":
			  	hh = tbl.width / 2;
				oo = paper.circle(tbl.x[0]+hh, tbl.x[1]+hh, hh);			
				break;			
			  case "e":
			  	hh = tbl.width / 2;
				oo = paper.ellipse(tbl.x[0]+hh, tbl.x[1]+hh, tbl.x[2], tbl.x[3]);
				break;
			  case "i":
				oo = paper.image(name, tbl.x[0], tbl.x[1], tbl.width, tbl.height);
				break;
			  case "d": // dummy, use for tmp purpose
			  default:
			  	return null;
				}
			
			if(subtype !== 'model')
				this.allobj.push(oo);
				
			cursor = (queueObj.editmode === 0) ? "default" : "pointer";
			oo.attr("cursor", cursor);	
			
			if(tbl.type !== "i") 
				oo.attr({ 'stroke': hslToRgb(tbl.color), 'stroke-opacity': opacity(tbl.color), 'fill': hslToRgbRdnt(tbl.fill, tbl.radiant, tbl.type), 'fill-opacity': opacity(tbl.fill) });

			if(tbl.angle) 
				oo.attr({transform: '...R'+tbl.angle+','+tbl.bxy[0]+','+tbl.bxy[1] });

			oo.data("wraper", tbl);
			if(queueObj.editmode > 0) {
				oo.drag(draglib.dragmove, draglib.dragstart, draglib.dragend);	
				oo.click(function(e){ queueObj.qclick(e, this); });			
				oo.dblclick(function(e){ queueObj.qdblclick(e, this); });
			} else {
				oo.click(function(e) { var tbl = this.data("wraper"); queueObj.tblClick(e, tbl); });			
				oo.dblclick(function(e) { var tbl = this.data("wraper"); queueObj.tbldblClick(e, tbl); });
				oo.hover(function(e) { var tbl = this.data("wraper"); queueObj.tblHover(e, tbl, ''); });
				oo.mouseout(function(e) { var tbl = this.data("wraper"); queueObj.tblHoverOut(e, tbl, ''); });
				}
				
			oo.display = null;
			return oo;
			},
						
		createObj: function(selector) {
			var pp, tbl, svfconsole, vv, display, font, cursor;	
			if(selector < 1 && selector >= ObjTableContent.length) {
				return;
				}
			
			vv = ObjTableContent[selector];
			if(typeof vv.label !== "string") vv.label = "no name";
			if(typeof vv.npers !== "number") vv.npers = 2;
			if(typeof vv.mealtype !== "number") vv.mealtype = 0;
			if(vv.label === "no name")
				vv.label += ' ' + id_index; // give a unique id

			if(vv.x[0] < 0) vv.x[0] = 10;
			if(vv.x[0] >= floorwidth - 10) vv.x[0] = floorwidth - 100;		
			if(vv.x[1] < 0) vv.x[1] = 10;
			if(vv.x[1] >= floorheight - 10) vv.x[1] = floorheight - 100;
			if(typeof vv.captain !== "string") vv.captain = "";
			if(typeof vv.subtype !== "string") {
				vv.subtype = 'table';
				}

			//if(typeof vv.name === 'undefined')
			var tbl = {
				obj: 0,
				id: id_index,
				label: vv.label,
				npers: vv.npers,
				objclass: 'table',
				type: vv.type,
				x: vv.x.slice(0),
				width: vv.width,
				height: vv.height,
				name: vv.name,
				scaling: vv.scaling,
				stroke: vv.stroke,
				color: vv.color,
				fill: vv.fill,
				font: vv.font,
				radiant: vv.radiant,
				state: vv.state,
				angle: vv.angle,
				z: vv.z,
				selector: selector,
				transfrm: 0,
				con_id: -1,
				connection: null,
				conObj: null,	// you cannot refer to this yet !
				parent: null,
				attribution: vv.attribution,
				mealtype: vv.mealtype,
				cx: vv.x[0] + (vv.x[2]/2),
				cy: vv.x[1] + (vv.x[3]/2),
				bxy: [vv.x[0], vv.x[1], vv.x[0]+vv.x[2], vv.x[1], vv.x[0]+vv.x[2], vv.x[1]+vv.x[3], vv.x[0], vv.x[1]+vv.x[3] ],
				togglename: vv.name,
				captain: vv.captain,
				subtype: vv.subtype
				};
			
			tbl.draglib = draglib;
			tbl.parent = queueObj;
			id_index++;
			
			tbl.conObj = Createconnection(tbl, paper, queueObj.myalert);		
			tbl.objresa = new Createstate(tbl, 'none', queueObj.colorstate, paper, queueObj.toggleImage);
			tbl.obj = queueObj.drawObj(tbl, tbl.name, tbl.subtype);

			if(tbl.obj === null) 
				return tbl;
				
			if(tbl.angle) {
				tbl.bxy = getMatrix(tbl); 
				tbl.cx = tbl.bxy[0] + ((tbl.bxy[4] - tbl.bxy[0])/2);
				tbl.cy = tbl.bxy[1] + ((tbl.bxy[5] - tbl.bxy[1])/2);
				}

			if( tbl.label === "clock" && queueObj.editmode === 0) {
				console.log('FOUND CLOCK');
				queueObj.clock = paper.text(tbl.cx - 2, tbl.cy + 1, '----').attr({'cursor': "pointer"}).attr({'font-size': 16, 'fill': hslToRgb(tbl.font), 'fill-opacity': opacity(tbl.font) });
				return tbl;
				}

			queueObj.setdisplay(tbl);
	
			return tbl;
			}		
		};

	// var ObjTableContent = [ { type: "c", id: 1, x: [25, 25, 10, 0], stroke:2, color: "green" }, { type: "p", id: 2, x: [25, 25+(size*1), 6, 10], stroke:2, color: "blue" }, { type: "p", id: 3, x: [25, 25+(size*2), 8, 10], stroke:2, color: "red" }, { type: "r", id: 4, x: [17, 17+(size*3), 16, 16], stroke:2, color: "brown" }, { type: "r", id: 5, x: [13, 17+(size*4), 24, 16], stroke:2, color: "violet" }, { type: "e", id: 6, x: [25, 25+(size*5), 18, 8], stroke:2, color: "orange" }, { type: "i", id: 2, x: [1, 12+(size*0), 48, 24], name:"2seater_eli.svg", scaling: 3 }, { type: "i", id: 1, x: [1, 9+(size*1), 48, 30], name:"2seater_cir.svg", scaling: 3 }, { type: "i", id: 3, x: [1, 12+(size*2), 48, 24], name:"2seater_rnd.svg", scaling: 3 } ]; 

	this.drawing = function(scope, editmode, tablette) {
		
		// only once
		if(paper !== null)
			return;
				
		var firefox_bug = ["10_seater.svg", 168, 75, "2_seater_rectangle.svg", 77, 38, "2_seater_roundtable.svg", 64, 41, "2_seater_squaretable.svg", 65, 41, "3_seater_octagontable.svg", 75, 73, "3_seater_roundtable.svg", 69, 69, "4_seater_octagontable.svg", 77, 77, "4_seater_rectangletable.svg", 72, 76, "4_seater_roundtable.svg", 77, 79, "4_seater_squaretable.svg", 76, 77, "6_seater_all_sides.svg", 113, 75, "6_seater_face_to_face.svg", 103, 76, "8_seater.svg", 119, 119, "8_seater_rectangle.svg", 132, 77, "booth_left_sided.svg", 76, 76, "booth_right_sided.svg", 76, 76, "plant_1.svg", 46, 45, "plant_2.svg", 52, 52, "plant_3.svg", 54, 53, "separator.svg", 80, 6];
		var limitImage = imagesRes.length, oo, 
			tt = "",
			nitem = 0,
			index = 0,
			width, height, scale, scaling, sx, sy, cc, pp, 
			tbl, i, k, npers, imagename, 
			svfconsole, mysquare;
			
		queueObj.scope = scope; // this is to trigger top menu event
		
		for(i = 0; i < limitImage; i++)  {
			if(imagesRes[i].src.indexOf('floor') > -1) 
				queueObj.addfloors(imagesRes[i].src);
			}

		queueObj.sortfloors();
		queueObj.setEditmode(editmode);
		if(editmode === 0)
			limitImage = 0;

		for(i = 0; i < limitImage; i++) {
			if(typeof imagesRes[i] === "undefined" || imagesRes[i].src === "" || typeof imagesRes[i].naturalHeight === "undefined")
				continue;
			imagename = imagesRes[i].src;
			if(imagename.indexOf('floor') > -1) {
				continue;
				}					
			if(imagename.indexOf('.jpg') > -1) {
				continue;
				}
			if(imagename.match(/_(red|green|gold|green|purple|pink|black|marron)\.svg/)) 
				continue;
									
			width = imagesRes[i].naturalWidth;
			height = imagesRes[i].naturalHeight;
			if(width === 0 || height === 0) {
				index = firefox_bug.indexOf(elementsAr[i]);
				if(index < 0) continue;
				width = firefox_bug[index+1];
				height = firefox_bug[index+2];
				}
			//tt += elementsAr[i] + ' ' + width + ' ' + height + '\n'; 
			if(width > height) {
				scale = 48 / width;
				height *= scale;
				width = 48;
				sx = (50 - width) / 2;
				sy = (50 - height) / 2;
				}
			else {
				scale = 48 / height;
				width *= scale;
				height = 48;				
				sx = (50 - width) / 2;
				sy = (50 - height) / 2;
				}
			scaling = (1/scale)/2;
		
			npers = (zeroplace(imagesRes[i].src)) ? 0 : 2;					
			ObjTableContent.push({ type: "i", x: [sx, sy+(size*nitem), width, height], width: width, height: height, angle: 0, label: 'no name', name: imagesRes[i].src, npers: npers, attribution: 0, z: 1, scaling: scaling, state: 0, stroke: 0, color: "#000000", fill: "#ffffff", font: '#000000', radiant: '', subtype: 'model' });
			nitem++;
			}
		
		if(editmode !== 0) {
			var figure = { circle:"c", rectangle:"r", rounded:"s" } ;
			for(oo in figure)
				if(figure.hasOwnProperty(oo)) {
					ObjTableContent.push({ type: figure[oo], x: [10, (size*nitem)+10, size-20, size-20], width: 30, height: 30, angle: 0, label: 'object', name: oo, npers: 0, attribution: -1, z: 1, scaling: 0, state: 0, stroke: 0, color: "#000000", fill: "#000000", font: '#000000', radiant: '', subtype: 'model' });
					nitem++;
					}
			}
		
		if(floorheight < (nitem + 1) * size) floorheight = 	(nitem + 1) * size;	
		paper = Raphael('raphael', floorwidth, floorheight);
		queueObj.fontable = paper.getFont("MonospaceTypewriter"); 
		queueObj.fontstate = paper.getFont("GillSansC Bold");// Monofonto
		queueObj.fontdeco = paper.getFont("JackInput");// Onuava;Sifonn;Helvetica CE;MonospaceTypewriter;JackInput;Monofonto
		queueObj.fontsmall = paper.getFont("Geneva");// Onuava;Sifonn;Helvetica CE;MonospaceTypewriter;JackInput;Monofonto
		
		draglib = new DragObj(queueObj);
		queueObj.groupObj = Groupselection(queueObj, paper);
		
						
		for(i = 0; i < nitem; i++) {
			cc = (i < ObjTableContent.length && ObjTableContent[i].type === "i") ? "#ffffff" : rpcolor;
			mysquare = paper.rect(1, (i*size), 49, 49).attr("fill", cc);
			if(cc === rpcolor) mysquare.click(turnMeBlue);
			}
			
		if(nitem > 0) {	// create a trash on in editing mode
			queueObj.ytrash = (nitem * size);
			mysquare = paper.rect(1, (nitem * size), 49, 49).attr("fill", "#ffffff");
			oo = paper.image("images/trash.png", 10, (nitem * size)+10, 30, 30);
			}
		
		ObjTableContent.push({ type: "d", x: [0, 0, 0, 0], width: 0, height: 0, angle: 0, label: 'no name', name: "vide", npers: 0, attribution: -1, mealtype: 0, z: 1, scaling: 0, state: 0, stroke: 0, color: "#000000", fill: "#ffffff", font: '#000000', radiant: '' });	
		for(i = 0; i < ObjTableContent.length; i++) {
			if(tbl = queueObj.createObj(i))
				myElement.push(tbl);
			}
		emptyTbl = tbl;
		emptyTbl.objclass = "empty";
		
		queueObj.createViewer(100, 10);
		if(editmode === 0) {
			//queueObj.svfconsole.dblclick(function(){ showModal('General Information', 'data', -1, -1, "modalgen1.html", queueObj, null); });
			if(queueObj.hoverviewer)
				queueObj.svfconsole.hide();
			}
		
		queueObj.tablette = tablette;
		queueObj.grid = (function() {
			var i;
			var state = 'hide';
			var path = paper.set();				
		
			for(i = 50; i < floorwidth; i += 50) {
				path.push(paper.path("M 50,"+i+" L "+(floorwidth - 50)+","+i).attr("stroke-dasharray", ".").attr("stroke-width", 1).attr("stroke", "grey"));
				path.push(paper.path("M "+i+ ","+0+" L "+i+","+(floorheight - 50)).attr("stroke-dasharray", ".").attr("stroke-width", 1).attr("stroke", "grey"));
				}
			path.toBack();
			path.hide();
			return {
				toggle: function() { if(state === 'hide') { state = 'show'; path.show(); } else { state = 'hide'; path.hide(); } }
				};
			})();

		queueObj.createfloor(floordefault);
		if(queueObj.myfloor) 
			queueObj.myfloor.toBack();
			
		scope.setTmsPrompt();
		
		$(document).keydown(function(e) { return processKey(e, queueObj); });
		console.log('DONE Drawing');
		};

	this.reset = function() {
		queueObj.clear();
		if(paper) paper.remove();
		};
				
	this.enableKey = function() {
		queueObj.keyCodeEnable = true;
		};
		
	this.disableKey = function() {
		queueObj.keyCodeEnable = false;
		};
		
	this.clearState = function(id) {
		queueObj.clearstate(id);
		};
		
	this.clearallstate = function() {
		queueObj.clearallstate();
		};
		
	this.setstate = function(id, state) {
		queueObj.setstate(id, state);
		};
		
	this.getData = function() {
		return queueObj.list();
		};

	this.getemptyData = function(name) {
		return queueObj.emptylist(name);
		};

	this.setLayoutName = function(name) {
		queueObj.setLayoutName(name);
		};

	this.setLayout = function(obj) {
		queueObj.setLayout(obj, '');
		};

	this.setLayoutPrefixtable = function(prefixtable) {
		queueObj.setLayoutPrefixtable(prefixtable);
		};

	this.setLayoutCaptain = function(obj, name) {
		queueObj.setLayout(obj, name);
		};

	this.getcolorPicker = function() {
		return queueObj.getcolorPicker();
		};

	this.getTableInfo = function() {
		return queueObj.getTableInfo();
		};

	this.setViewerconnect = function(arg) {
		return queueObj.setViewerconnect(arg);
		};
						
	this.showResaInfo = function(tablename, booking, guestname, npers, state, highlight, nxtbooking, captain, duration, cleartime, seated, flg) {
		return queueObj.showResaInfo(tablename, booking, guestname, npers, state, highlight, nxtbooking, captain, duration, cleartime, seated, flg);
		};

	this.resetResaInfo = function(tablename, booking) {
		return queueObj.resetResaInfo(tablename, booking);
		};
	
	this.clean = function() {
		return queueObj.clean();
		};
		
	this.getFloors = function() {
		return { data: queueObj.getfloors(), index: queueObj.getfloorname() };
		};
		
	this.setfloorName = function(floor) {
		return  queueObj.setfloorname(floor);
		};

	this.enablecaptains = function() {
		return  queueObj.enablecaptains();
		};

	this.setPrompt = function(prompt) {
		return  queueObj.setprompt(prompt);
		};

	this.setPref = function(pref, value) {
		return  queueObj.setPref(pref, value);
		};
		
	this.setSelectedBooking = function(bkgAr) {
		return queueObj.setselectedbooking("all", bkgAr);
		};

	this.setallnextbkg = function(obj) {
		return queueObj.setallnextbkg(obj);
		};
				
	this.triggerAnim = function(booking, mode) {
		return queueObj.triggeranim(booking, mode);
		};
		
	this.shortcuts = function(action) {
		return processRemoteKey(action, queueObj);
		};
		
	this.setBlockTables = function(btables) {
		return queueObj.setBlockTables(btables);
		};	
	this.setClock = function(s) {
		return queueObj.setClock(s);
		};
	this.setFixView = function(flg) {
		return queueObj.setFixView(flg);
		};
}]);

