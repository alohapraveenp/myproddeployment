<?php

define("__SERVERNAME__", $_SERVER['SERVER_NAME']);

define("__ROOTDIR__", "");
define("__SERVERROOT__", $_SERVER['DOCUMENT_ROOT']); 

define("__BASE_URL__", "https://www.weeloy.asia");
define("__BASE_URL_WHITE_LABEL__", "https://www.feasting.asia");

$tmp_path = $_SERVER['DOCUMENT_ROOT'] . __ROOTDIR__ . "/tmp/";
if (strpos($tmp_path,'/api/') !== false) {$tmp_path = str_replace('/api/', '/', $tmp_path);}
define("__TMPDIR__",  $tmp_path);

define("__SHOWDIR__", __ROOTDIR__ . "/weeloy_media/upload/restaurant/");
define("__SHOWDIRUSER__", __ROOTDIR__ . "/weeloy_media/upload/user/");
define("__SHOWDIRCONTEST__", __ROOTDIR__ . "weeloy_media/upload/event/singapore/");
define("__MEDIADIR__", __ROOTDIR__ . "/weeloy_media/");
define("__UPLOADDIR__", $_SERVER['DOCUMENT_ROOT'] . __SHOWDIR__);
define("__UPLOADDIRUSER__", $_SERVER['DOCUMENT_ROOT'] . __SHOWDIRUSER__);
define("__UPLOADDIRCONTEST__", $_SERVER['DOCUMENT_ROOT'] . __SHOWDIRCONTEST__);

define("AWS", strstr($_SERVER['HTTP_HOST'], 'localhost') === false);
define("__S3HOST__", "https://media.weeloy.com/");
define("__S3HOST1__", "https://media1.weeloy.com/");
define("__S3HOST2__", "https://media2.weeloy.com/");
define("__S3HOST3__", "https://media3.weeloy.com/");
define("__S3HOST4__", "https://media4.weeloy.com/");
define("__S3HOST5__", "https://media5.weeloy.com/");
define("__S3DIR__", "upload/restaurant/");
define("__S3DIRUSER__", "upload/user/");

define("PAYPALRETURNURL", "https://www.weeloy.asia/payment-success?payment_id=");
define("PAYPALCANCELURL", "https://www.weeloy.asia/");
define("PAYPALIPN", "https://www.weeloy.asia/modules/shoppingcart/paypal_ipn.php");

define("MAX_FILE_SIZE", 1500000);	// max size in bytes for each uploaded file
define("PHOTO_CONTEST_MAX_FILE_SIZE", "10485760");// max size in bytes for each uploaded file

define("__SESSION_LIFE__", 5400);
define("__SESSION_LIFE_MINUTE__", 90);
define("__SESSION_TMS_MINUTE__", 840);
define("__SESSION_LIFE_MINUTE_8H__", 480);

//Facebook Booking form app
define('BOOKING_APP_ID', '755782447874157');
define('BOOKING_APP_SECRET', 'cc494eee79bce533707e4423877006ea');

//Facebook website app app dev
define('WEBSITE_FB_APP_ID', '1590587811210971');
define('WEBSITE_FB_APP_SECRET', '535d344eb1f08a575ca5556bd37fd159');
define('WEBSITE_FB_Link', 'https://www.weeloy.asia/bookingdetails?b=');

//sms credentials
define('SMS_USERNAME','philippebenedetti');
define('SMS_APP_KEY','fua4p8f9MGRaLY05I4ImEngRZac6N6');
define('SMS_URL','https://rest.textmagic.com/api/v2/messages');

//spool history bucket
define("SPOOL_HISTORY_BUCKET", 'weeloy-spool-history-files');
define("SPOOL_HISTORY_QUEUE", 'migrate-spool');

define("REDIRECTION",'https://tracking.weeloy.asia/');



//Sanitize $_RESQUEST variable. Not the proper way to do, but the most simple and fast
if(!preg_match('/\/backoffice[\/]?/', $_SERVER['REQUEST_URI']) && preg_match('/\/admin_weeloy[\/]?/', $_SERVER['REQUEST_URI']) && preg_match('/\/admin_tms[\/]?/', $_SERVER['REQUEST_URI'])){
    $_REQUEST = filter_var_array($_REQUEST, FILTER_SANITIZE_STRING);
    $_POST = filter_var_array($_POST, FILTER_SANITIZE_STRING);
    $_GET = filter_var_array($_GET, FILTER_SANITIZE_STRING);
}



/** useful vars **/
$curPage = ''; if (isset($_REQUEST['page'])) $curPage = $_REQUEST['page'];

if (!defined('PHP_VERSION_ID')) {
   $version = explode('.',PHP_VERSION);

   define('PHP_VERSION_ID', ($version[0] * 10000 + $version[1] * 100 + $version[2]));
}

if (strstr($_SERVER['HTTP_HOST'], 'localhost:8888')){   
    set_include_path(get_include_path().':/Users/richardkefs/Sites/weeloy.com/');
    define("HTTP", "http://localhost:8888/weeloy.com/");
} else {

    set_include_path(get_include_path().':/var/www/html/');
    //define("HTTP", "http://$_[SERVERNAME]/weeloy.com/");  //must do it for dev and prod
}

define("PROTOCOL", "https");

if (strstr($_SERVER['HTTP_HOST'], 'localhost:8888')){   
    $Conf['TemplateDir'] 	= "templates/";
} else {
    $Conf['TemplateDir'] 	= "templates/";
}
