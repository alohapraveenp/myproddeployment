<?php
$Lang = '';
if (empty($_SESSION['user']['lang'])) {
    if (isset($_SERVER['HTTP_ACCEPT_LANGUAGE'])) {
        $Lang = explode(',', $_SERVER['HTTP_ACCEPT_LANGUAGE']);
        $Lang = strtolower(substr(chop($Lang[0]), 0, 2));
    }
} elseif (!empty($_REQUEST['lang']) &&
        ($_REQUEST['lang'] == 'fr' OR $_REQUEST['lang'] == 'es' OR $_REQUEST['lang'] == 'en' )) {

    $Lang = $_REQUEST['lang'];

    switch ($_REQUEST['lang']) {
        case 'fr':
            date_default_timezone_set('Europe/Paris');
            setlocale(LC_ALL, 'fr_FR.UTF-8');
            break;

        case 'es':
            date_default_timezone_set('Europe/Madrid');
            setlocale(LC_ALL, 'es_ES.utf-8');
            break;

        default:
            date_default_timezone_set('Asia/Singapore');
            setlocale(LC_ALL, 'en_US.utf-8');
            break;
    }
}

$translate = new translate($Lang, $pages->current_page);
$trsl = $translate->load($Lang);
