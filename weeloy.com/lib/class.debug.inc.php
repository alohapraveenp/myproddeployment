<?php
require_once 'lib/class.mail.inc.php';
    
$debug_type = array("LOG" => 0, "DEBUG" => 1, "ERROR" => 2, "ERROR-API" => 4, "ERROR-PDO" => 8, "API-LOG" => 16);

trait IpWorld {

    public static function getIP() {
        $real_client_ip = '';
        $headers = apache_request_headers();

        if (isset($headers["X-Forwarded-For"])) {
            $real_client_ip = $headers["X-Forwarded-For"];
        }

        if (!empty($real_client_ip))
            $adresse_ip = $real_client_ip;
        else
            $adresse_ip = $_SERVER['REMOTE_ADDR'];

        if ($adresse_ip == '::1')
            $adresse_ip = '127.0.0.1';

        $adresse_ip_tmp = explode(',', $adresse_ip);
        $adresse_ip = $adresse_ip_tmp[0];
        return ip2long($adresse_ip);
    }

}

class WY_debug {


    
    use IpWorld;

    //use CleanIng;

    var $type;
    var $iplong;
    var $category;
    var $cdate;
    var $content;

    function __construct() {
        $this->iplong = $this->getIP();
    }

    static function writeDebugIP($type, $category, $content, $ip) {

        global $debug_type;

        $data = pdo_single_select("SELECT count(*) as value from log_debug", "dwh");
        if (count($data) != 1 || intval($data["value"]) > 500) // can be called per ajax. Don't get overflown...
            return;

        $typevalue = 0;
        if (array_key_exists($type, $debug_type))
            $typevalue = $debug_type[$type];

        $category = clean_input($category);
        $content = clean_input($content);

        if (preg_match("/recordDebug/", $content) || preg_match("/Too many connections/", $content))
            return;

        pdo_settimezone('dwh');

        $e = new Exception();
        $trace = clean_input(print_r($e->getTraceAsString(), true));

        error_log('LOG_DEBUG => ' . $type . " " . $category . " : " . $content);

        $now = (strstr($_SERVER['HTTP_HOST'], 'localhost:8888')) ? "NOW()" : "DATE_ADD(NOW(), INTERVAL 8 HOUR)";
        pdo_exec("INSERT INTO log_debug(type, category, content, ip, cdate, trace) VALUES ('$typevalue', '$category', '$content', '$ip', $now, '$trace')", 'dwh');
    }

    function readDebug($type, $category, $firstdate, $lastdate) {

        global $debug_type;

        $typevalue = 0;
        if (array_key_exists($type, $debug_type))
            $typevalue = $debug_type[$type];

        return pdo_multiple_select("SELECT * from log_debug WHERE type = '$typevalue' and category = '$category' and cdate > '$firstdate' and cdate < '$lastdate'", "dwh");
    }

    function writeDebug($type, $category, $content) {
        $this->writeDebugIP($type, $category, $content, $this->iplong);
    }

    function updateOrInsertTimestamp($type, $category) {
        try {
            date_default_timezone_set("Asia/Singapore");
            $cdate = date("Y-m-d H:i:s");
            $check = "SELECT ID from log_debug where type = '$type' and category = '$category'";
            $insert = "INSERT into log_debug (cdate, type, category) VALUES ('$cdate', '$type', '$category')";
            error_log(__FILE__." ".__FUNCTION__." ".__LINE__." insert: ".$insert);
            $result = pdo_insert_unique($check, $insert, "dwh");
            if ($result > 0) {
                $sql = "UPDATE log_debug SET cdate = :cdate WHERE ID = :ID";
                $parameters = array('ID' => $result, 'cdate' => $cdate);
                pdo_update($sql, $parameters, "dwh");
            }
        } catch (PDOException $e) {
            error_log(__FILE__." ".__FUNCTION__." ".__LINE__." PDOException: ".$e);
            api_error($e, __FILE__." ".__FUNCTION__." ".__LINE__); 
        }
        catch (Exception $e) {
            error_log(__FILE__." ".__FUNCTION__." ".__LINE__." Exception: ".$e);
            api_error($e, __FILE__." ".__FUNCTION__." ".__LINE__);
        }
    }
    public static function recordDebug($type, $category, $content) {
        self::writeDebugIP($type, $category, $content, self::getIP());
    }

    public static function checkOnceADay($category) {
    	$category = clean_input($category);
        date_default_timezone_set("Asia/Singapore");
        $cdate = date("Y-m-d");
    	
        $data = pdo_single_select("SELECT cdate FROM log_debug where category = '$category' and date(cdate) = '$cdate' limit 1", "dwh");
        return count($data);
    }
    	
    public static function countError($email) {
        if (!in_array($email, array("richard@kefs.me", "richard.kefs@weeloy.com", "philippe.benedetti@weeloy.com")))
            return -1;

        $data = pdo_single_select("SELECT count(*) as value from log_debug", "dwh");
        return (count($data) < 1 || !isset($data['value'])) ? 0 : intval($data['value']);
    }

    static function sendEmailDebug($subject, $body, $opts = NULL, $restaurant = NULL, $booking_id = NULL) {
        $email = new JM_Mail();
        $recipient = array('support@weeloy.com','philippe.benedetti@weeloy.com');

        if(empty($opts)){
            $opts['from'] = array('support@weeloy.com' => 'Debug weeloy');
        }
                
        return $email->sendmail($recipient, $subject, $body, $opts, $restaurant, $booking_id);
    }

}

?>
