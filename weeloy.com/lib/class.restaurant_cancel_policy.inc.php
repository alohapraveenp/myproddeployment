<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


require_once 'lib/class.restaurant.inc.php';
require_once("lib/class.analytics.inc.php");
require_once("lib/class.booking.inc.php");
require_once('lib/class.allote.inc.php');
require_once('lib/class.payment.inc.php');


class WY_Restaurant_policy  {

    public function bacchanalia_clpolicy($restaurant) {
        if (empty($restaurant)) {
            return false;
        }

        $tmpArr = [];
        $sql = "SELECT * FROM bacchanalia_cancel_policy  WHERE restaurant ='$restaurant' ";
        $data = pdo_multiple_select($sql);
        $j = 1;
        foreach ($data as $con) {
            if ($con['mealtype'] == 'lunch' && intval($con['min_pax']) == 1 && intval($con['max_pax']) == 4) {
                $range = "";
            }
            if ($con['mealtype'] == 'lunch' && intval($con['min_pax']) == 5 && intval($con['max_pax']) >= 5) {
                $range = array('pax' => $con['min_pax'], 'message' => "Any cancellation received 9am later to booking date will incur a charge of ", "mealype" => $con['mealtype'], 'amount' => $con['price']);
            }

            if ($con['mealtype'] == 'dinner' && intval($con['min_pax']) == 1 && intval($con['max_pax']) == 4) {
                $range = array('pax' => $con['min_pax'], 'message' => "Any cancellation received 6pm later to booking date will incur a charge of ", "mealype" => $con['mealtype'], 'amount' => $con['price']);
//                                $tmpArr['tnc1'] =$range;
            }
            if ($con['mealtype'] == 'dinner' && intval($con['min_pax']) == 5 && intval($con['max_pax']) >= 5) {
                $range = array('pax' => $con['min_pax'], 'message' => "Any cancellation received untill 6pm previous day will incur a charge of ", "mealype" => $con['mealtype'], 'amount' => $con['price']);
            }

            if ($range != '') {
                array_push($tmpArr, $range);
            }
        }

        
        return $tmpArr;
    }

    public function getDefaultPrice($restaurant) {
        $sql = "SELECT * FROM bacchanalia_cancel_policy  WHERE restaurant ='$restaurant' ";
        $data = pdo_multiple_select($sql);
        $priceArr = array('lunch_charge' => '', 'dinner_charge' => '');
        foreach ($data as $con) {
            if ($con['mealtype'] == 'lunch' && $con['min_pax'] == 5 && $con['max_pax'] >= 5) {
                $priceArr['lunch_charge'] = $con['price'];
            }
            if ($con['mealtype'] == 'dinner' && $con['min_pax'] == 1 && $con['max_pax'] == 4) {
                $priceArr['dinner_charge'] = $con['price'];
            }
        }
        return $priceArr;
    }

    public function isRequiredccDetails($restaurant, $rtime, $pax,$rdate) {
       
        $mealType = WY_Allote::getMealType($rtime);

        if (WY_restaurant::s_checkbkdeposit($restaurant) <= 0) {
            return $isRequired = false;
        }
        if($restaurant == 'SG_SG_R_Nouri' && $mealType =='lunch' ){
            return $isRequired = false;
        }
        if($restaurant === 'SG_SG_R_Bacchanalia'){
            $isRequired = $this->baccCCflag($restaurant, $rtime, $pax,$mealType);
        }else{
            $isRequired = $this->isRequiredpayment($restaurant, $rtime, $pax,$rdate);
        }
        // $sql = "SELECT * FROM bacchanalia_cancel_policy  WHERE restaurant ='$restaurant' AND mealtype ='$mealType' ";

        // $data = pdo_multiple_select($sql);
        // $priceArr = array();
        // $isRequired = false;

        // foreach ($data as $con) {

        //     if ($con['mealtype'] == 'lunch' && $pax > 4) {
        //         $isRequired = true;
        //     }
        //     if ($con['mealtype'] == 'dinner') {

        //         $isRequired = true;
        //     }
        // }

        return $isRequired;
    }
    public function baccCCflag($restaurant, $rtime, $pax,$mealType){

         $sql = "SELECT * FROM bacchanalia_cancel_policy  WHERE restaurant ='$restaurant' AND mealtype ='$mealType' ";

        $data = pdo_multiple_select($sql);
        $priceArr = array();
        $isRequired = false;

        foreach ($data as $con) {

            if ($con['mealtype'] == 'lunch' && $pax > 4) {
                $isRequired = true;
            }
            if ($con['mealtype'] == 'dinner') {

                $isRequired = true;
            }
        }
        return $isRequired;
    }
    public function isRequiredpayment($restaurant, $rtime, $pax,$rdate){
        $product ='';
        if($restaurant == 'SG_SG_R_Pollen'){
            $festivedate ='2017-05-14';
            $product = (intval($pax) == 5) ? 'fivepax' : 'abovefivepax'; 
            if (strtotime($festivedate) == strtotime($rdate) ){
                $product ='festive';
              
            }
        }

        $data = $this->getResPayment($restaurant,$product);
        $isRequired  = false;
        if(count($data) > 0 ){
             $isRequired =  (intval($pax) >= intval($data['pax'])) ? true : false; 
        }
        return $isRequired;

    }


    public function getResChargeDetails($charge_id, $ref_id) {
        $booking = new WY_Booking();

        $booking->getBooking($ref_id);

        $isPayment = 1;
        $sql = "SELECT * FROM  payment  WHERE  payment_id  = '$charge_id' and object_id ='$ref_id'   limit 1 ";
        $data = pdo_single_select($sql);
    
        // if no config, load default values
        if (count($data) <= 0) {
            $isPayment = 0;
            $sql = " SELECT id,data,amount,curency as currency,receiver,paykey,object_id,card_id,status,payment_method,token FROM booking_deposit  WHERE  paykey = '$charge_id' AND object_id='$ref_id' limit 1 ";
            $data = pdo_single_select($sql);
        }

        date_default_timezone_set('Asia/Singapore');
        if (count($data) > 0) {

            $data['isPayment'] = $isPayment;

            if ($data['status'] == 'COMPLETED') {
                $meal_type = WY_Allote::getMealType($booking->rtime);

                $datetime = date('Y-m-d H:i:s', strtotime($booking->rdate . " +12 hours"));
                 //date("Y-m-d H:i:s", strtotime($date1 . " +3 hours"));
                $time_dateTo = strtotime($datetime);
                $interval = $time_dateTo - strtotime("now");
                $interval_day = $interval / (60 * 60 * 24);

                if ($meal_type == 'dinner') {
                   
                    if ($booking->cover < 5) {
                        $data['refund_amount'] = 0;
                       
                        if ($interval_day <=0) {
                            if (date('H:i') >='12:00') {
                                $data['refund_amount'] = $data['amount'];
                                
                            }
                        }
                    } else {

                        $data['refund_amount'] = 0;

                        if ($interval_day >= 1 && $interval_day < 2) {

                            if (date('H:i') >='18:00') {
                                $data['refund_amount'] = $data['amount'];
                            }
                        }
                        if ($interval_day < 1) {
                            $data['refund_amount'] = $data['amount'];
                        }
                    }
                } else {
                    
                        $data['refund_amount'] = 0;
                        $datetime = date('Y-m-d H:i:s', strtotime($booking->rdate . " +9 hours"));
                        $time_dateTo = strtotime($datetime);
                        $interval = $time_dateTo - strtotime("now");
                        $interval_day = $interval / (60 * 60 * 24);
                    
                    if ($interval_day <=0) {
                      
                        if (date('H:i')  >='09:00') {
  
                            $data['refund_amount'] = $data['amount'];
                        } 
                    }
                }
            }
            if ($data['status'] == 'pending_payment') {
                $data['refund_amount'] = "0";
            }
            if (isset($data['refund_amount'])) {
                $data['refund_amount'] = $data['refund_amount'];
            } else {
                $data['refund_amount'] = "0";
            }
        }

        return $data;
    }

    public function getBacchaDeadline($restaurant, $rdate, $rtime, $pax) {
        $meal_type = WY_Allote::getMealType($rtime);
        $datetime1 = date('Y-m-d', strtotime($rdate));
        $datetime = date('F j, Y', strtotime("$rdate"));
        $deadline['isSplConfig'] = false;

        $deadline = array('date' => $datetime, 'charge' => 60, 'message' => '','isSplConfig' => false,'pax' => $pax);
        if ($meal_type == 'lunch' && $pax >= 5) {
            $deadline['date'] = $datetime . ', 9.00am';
            $deadline['charge'] = 30;
        } else if ($meal_type == 'dinner' && $pax <= 4) {
            $deadline['date'] = $datetime . ', 12.00pm';
            $deadline['charge'] = 60;
        } else if ($meal_type == 'dinner' && $pax >= 5) {
            $prev_day = date('F j, Y', strtotime('-1 day', strtotime($rdate)));
            $deadline['date'] = $prev_day . ', 6.00pm';
            $deadline['charge'] = 60;
        }
        $message = "Booking cancelled after " . $deadline['date'] . " will be charged SGD " . $deadline['charge'] . " (subject to GST) per guest. Should you have any difficulties cancelling a booking please call the restaurant hotline at +65 9179 4552";
        $deadline['message'] = $message;
        $deadline['requiredccdetails'] = $this->isRequiredccDetails($restaurant, $rtime, $pax,$rdate);
        return $deadline;
    }
    
    public function getPaymentPolicy($restaurant,$amount,$date,$time,$pax,$product){
        $meal_type = WY_Allote::getMealType($time);
        $res = new WY_Restaurant();

        switch ($restaurant) {
            case 'TH_BK_R_Medinii': 
            case 'TH_BK_R_BangkokHeightz':
                  $policy = $this->getMediniCancelPolicy($restaurant,$amount,$date,$meal_type,$pax,$product,$time);
                break;
            case 'SG_SG_R_Bacchanalia':
                  $policy = $this->getBacchaDeadline($restaurant,$date,$time,$pax);
                break;
            case 'SG_SG_R_Esquina':
                   $policy = $this->getEsquinaCancelPolicy($restaurant,$amount,$date,$meal_type,$pax,$product,$time);
                break;
//            case 'SG_SG_R_BurntEnds':
//            case 'SG_SG_R_TheOneKitchen':
//                $policy = $res->getCancelPolicyAll($restaurant,'',$amount,$product);
//                break;
            default:
                
                $policy = $res->getCancelPolicyAll($restaurant,'',$amount,$product,$date,$time,$pax);
        }
        return $policy;
        
        
    }
    
    function getMediniCancelPolicy($restaurant,$amount,$date,$meal_type,$pax,$product,$time){
   
       $rdate = date('Y-m-d',strtotime($date));
       $isNgFlg = 12;
     
       $session_type = $this->getSessiontype($restaurant,$rdate);

       if($session_type === 'normal' ){
            $product ='normal';
       }
  
       if($session_type === 'festive' && empty($product)){
            $product = 'festive';
       }

        //$session_type ='festive';
        $res = new WY_Restaurant();
        $currency = $res->getCurrency($restaurant);
        $deadline = array('session_type' =>$session_type,'date' => $date, 'charge' => '', 'message' => '');
        $sql = "SELECT * FROM restaurant_cancel_policy  WHERE restaurant ='$restaurant' AND  product = '$product'";
        $data = pdo_single_select($sql);
       
        $isRequired = false;
        $deadline['isSplConfig'] = false;

        if(count($data) > 0){
            $price = $data['lunch_charge'];
            $deadline['perpax_price'] = $price;
            $total_amount = intval($pax) * intval($data['lunch_charge']);
            $deadline['charge'] = $price  ;
        }
           $message = "Restaurant will collecting 100% deposit per couple (Deposit amount " .$currency." ".$price."). Deposits and final payments are non-refundable";
        if($restaurant == 'TH_BK_R_BangkokHeightz'){
           $message = "Restaurant will collecting 100% deposit per couple for 5-course dinner set menu with unlimited beverages (Deposit amount " .$currency." ".$price."). Deposits and final payments are non-refundable";  
        }
    
        $deadline['message'] = $message;
        if (WY_restaurant::s_checkbkdeposit($restaurant) <= 0) {
            $isRequired = false;
        }

        if($session_type == 'festive'){
            if(intval($time) >= 18.00 && intval($time) <= 23.30 ){
                 $isRequired = true;
                 $deadline['isSplConfig'] = true;
            }else{
                $isRequired =  (intval($pax) >= 6) ? true : false; 
                if($restaurant == 'TH_BK_R_BangkokHeightz'){
                    $isRequired = false;
                }
                $price = $this->getMedinidefaultPrice($restaurant,'normal');
                $deadline['perpax_price'] = $price;
                $total_amount = intval($pax) * intval($data['lunch_charge']);
                $deadline['charge'] = $price  ;
                $isNgFlg = 13;
                 //$isRequired = false; 
            }  
        }else{
             $isRequired =  (intval($pax) >= 6) ? true : false;
                if($restaurant == 'TH_BK_R_BangkokHeightz'){
                    $isRequired = false;
                } 
             $isNgFlg = 13; 
        }
        $deadline['requiredccdetails'] = $isRequired;
        if($isNgFlg === 13){
             $deadline['message'] = "Restaurant will collecting 50% deposit per guest (Total Deposit amount " .$currency." ".$total_amount."). Deposits and final payments are non-refundable. For all bookings during Festive season or special events, deposit is required for all paxs. For booking not during Festive season or special events, deposit is required from 6 pax or more";
        }

        return $deadline;

    }
    function getEsquinaCancelPolicy($restaurant,$amount,$date,$meal_type,$pax,$product,$time){
   
       $rdate = date('Y-m-d',strtotime($date));
       $isNgFlg = 12;
       $service_charge = 5.00;
        $res = new WY_Restaurant();
        $currency = $res->getCurrency($restaurant);
        $deadline = array('session_type' =>$session_type,'date' => $date, 'charge' => '', 'message' => '');
         $data = $this->getResPayment($restaurant);
       
        $isRequired = false;
        $deadline['isSplConfig'] = false;

        if(count($data) > 0){
            $deadline['isSplConfig'] = true;
            $price = $data['lunch_charge'];
            $deadline['perpax_price'] = $price;
            $total_amount = intval($pax) * intval($data['lunch_charge']);
            $deadline['charge'] = $price  ;
            $deadline['refund_amount'] = $price - $service_charge;
        }
        $message = "Restaurant will be collecting " .$currency." ".$price."nett deposit upon booking 5 paxs and above. Deposits are non-refundable within 24 hours, more than 24 hours will be refund SGD " . $deadline['refund_amount'] . "nett. Should you require any assistance cancelling a booking, please call the restaurant hotline at +65 6222 1616";
            
        $deadline['message'] = $message;
        if (WY_restaurant::s_checkbkdeposit($restaurant) <= 0) {
            $isRequired = false;
        }
          $isRequired =  (intval($pax) >= intval($data['pax'])) ? true : false; 
          $deadline['requiredccdetails'] = $isRequired;
          
        return $deadline;

    }
    
    
    function getSessiontype($restaurant,$date){
         $type ='normal';
         $sql = "SELECT product FROM restaurant_cancel_policy  WHERE restaurant ='$restaurant' AND start <= '$date' AND end >= '$date' ";
         $data = pdo_single_select($sql);
      
        if(count($data) >0){
           $type ='festive';
       }
       return $type;

    }

    function getMedinidefaultPrice($restaurant,$product){
        $price = 588;
        $sql = "SELECT lunch_charge FROM restaurant_cancel_policy  WHERE restaurant ='$restaurant' AND  product ='$product' ";
         $data = pdo_single_select($sql);
      
        if(count($data) >0){
            $price = $data['lunch_charge'];
       }
       return $price;

    }
    
    public function getCancelPaymentDetails($reference_id){
       $booking = new WY_Booking();
       $payment = new WY_Payment();
       $booking->getBooking($reference_id);
       if (WY_restaurant::s_checkbkdeposit($booking->restaurant) <= 0) {
            $isRequired = false;
       }
       $restaurant = $booking->restaurant;
       //get data from booking deposit details
        //if cc details means only get from  
            $depositdetails = $payment->getBookingDepDetails($reference_id);
       //get booking payment Details
       switch ($restaurant) {
            case 'SG_SG_R_TheOneKitchenss':   //not in use
                $paymentDetails = $payment->getBookingDepDetails($reference_id);
                break;
            case 'SG_SG_R_Bacchanalia':
                $paymentDetails = $this->getResChargeDetails($booking->deposit_id,$reference_id);
                break;
            case 'SG_SG_R_BurntEnds':
                  $paymentDetails = $booking->getDepositDetailsByID($booking->deposit_id,$booking->rdate,$booking->rtime,$booking->restaurant,$depositdetails['payment_method'],$reference_id);
                break;
            default:
                 $paymentDetails = $booking->getDepositDetailsByID($booking->deposit_id,$booking->rdate,$booking->rtime,$booking->restaurant,$depositdetails['payment_method'],$reference_id);
        }
        return $paymentDetails;
    
    }
    public function getDepositPaymentPolicy($restaurant,$amount,$date,$time,$pax,$product){
        switch ($restaurant) {
            case 'SG_SG_R_Esquina':
                $paymentDetails = $this->getBkDepositDetails($restaurant,$date,$time,$pax,$product);
                break;
            default:
                 $paymentDetails = $booking->getBkDepositDetails($restaurant,$date,$time,$pax,$product);
        }
        
    }
    public function getResPayment($restaurant,$product = ''){
        $where = " AND product LIKE '' ";
        if(!empty($product)){
            $where = " AND product LIKE '$product'";
        }
        $sql = "SELECT * FROM restaurant_cancel_policy  WHERE restaurant ='$restaurant' $where";
        $data = pdo_single_select($sql);
        return $data;
    }
    
    public function getBkDepositDetails($restaurant,$date,$time,$pax,$product = ''){
        $isRequired = false;
        if (WY_restaurant::s_checkbkdeposit($restaurant) > 0) {
            $isRequired = true;
        }
        $deadline = array('session_type' =>$product,'date' => $date, 'deposit_charge' => '', 'message' => '','cancel_charge' => '');
        $charge = $this->getResPayment($restaurant,$product);
        $isRequired =  (intval($pax) >= intval($charge['pax'])) ? true : false; 
        if(count($charge) > 0){
            if($isRequired){
                $price = $data['lunch_charge'];
                $deadline['perpax_price'] = $price;
                $total_amount = intval($pax) * intval($data['lunch_charge']);
                $deadline['deposit_charge'] = $price;
                $datetime = date('Y-m-d H:i:s', strtotime($date));
                $time_dateTo = strtotime($datetime);
                $interval = $time_dateTo - strtotime("now");
                $interval_hrs = $interval / (60 * 60);
                
                if($interval_hrs < $charge['duration'] ){
                    $deadline['cancel_charge'] = 0;
                }else{
                    if($restaurant == 'SG_SG_R_Esquina' )
                       $deadline['cancel_charge'] = $total_amount = intval($pax) * 45;
                  else
                     $deadline['cancel_charge'] = $total_amount = intval($pax) * intval($data['lunch_charge']);
                  
              }
            }
        }
        $record['payment_type'] = $payment_type;
        $deadline['charge'] = $deadline['cancel_charge'];
        $deadline['isSplConfig'] = $isChksplconfig;
        $deadline['requiredccdetails'] = $isRequired;
        $record['pax'] = $pax ;
        return $deadline;

    }

}
