<?php

/**
 *	Richard Kefs (c) 2014
 *
 * 	Contains all classes definition for login session.
 *
*/


class WY_Blokedlist {

    private $restaurant = '';   
    public $result = 0;
    
    function __construct($restaurant) {
        $this->restaurant = clean_text($restaurant);
    }

    public function getBlockedUser() {
        $this->result = 1;
        $sql = "SELECT * FROM blocklist WHERE restaurant = '$this->restaurant'";
        return pdo_multiple_select($sql);
    }
    
    public function saveBlockedUser($email,$mobile,$status,$firstname,$lastname,$reason){
        $this->result = 1;
        $mobile = preg_replace("/[^0-9]/", "", $mobile);
        $sql = "SELECT * FROM blocklist WHERE restaurant = '$this->restaurant' and email = '$email' and mobile ='$mobile' ";
        $result = pdo_single_select($sql);
        if(count($result)>0){
            $list = pdo_exec("update blocklist  set status ='$status' WHERE restaurant = '$this->restaurant' and email = '$email' limit 1");
        }else{
             $sql = "INSERT INTO blocklist (restaurant,email, mobile, firstname,lastname,reason,status) VALUES ('$this->restaurant','$email', '$mobile','$firstname','$lastname','$reason','$status')";
   
             $list =  pdo_insert($sql);
        }
        return 1;
        
    }
    
    public function checkblockedUser($email,$mobile){
        $result = array();
        $isBlocked = false;
      
        $mobile = preg_replace("/[^0-9]/", "", $mobile);
        $sql = "SELECT * FROM blocklist WHERE restaurant = '$this->restaurant' AND (email = '$email' OR mobile = '$mobile')";

        $result = pdo_single_select($sql);
        if(count($result)>0){
            if($result['status'] == 'blocked'){
                $isBlocked =true;
            }
            
        }
        return $isBlocked;

    }
    
   
}