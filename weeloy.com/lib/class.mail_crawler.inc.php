<?php

require_once("lib/class.payment.inc.php");
require_once('lib/class.payment_stripe.inc.php');
require_once('lib/class.debug.inc.php');

class WY_Mail_crawler {

    private $account_id = 0;
    private $account_name = '';
    private $internal_meesage_id = '';
    private $email_sender = '';
    private $restaurant = '';
    private $restaurant_name = '';
    private $require_payment = '';
    private $hostname = '';
    private $credentials = '';
    private $provider = '';
    private $mailbox = 'INBOX';
    private $last_crawled_id = 0;
    private $stream = NULL;
    private $emails = NULL;
    private $message_limit = 50;
    private $parser_path = '';
    var $result;
    var $msg;
    
    public function __construct($account) {
        $this->account_id = $account['ID'];
        $this->account_name = $account['name'];
        $this->email_sender = $account['email_sender'];
        $this->restaurant = $account['restaurant'];
        $this->restaurant_name = $account['restaurant_name'];
        $this->require_payment = $account['require_payment'];
        $this->hostname = $account['hostname'];
        $this->credentials = $account['credentials'];
        $this->mailbox = $account['mailbox'];
        $this->provider = $account['provider'];
        $this->last_crawled_id = $account['last_crawled_id'];
        $this->parser_path = __ROOTDIR__ . 'modules/crawler/';

        if (strstr($_SERVER['HTTP_HOST'], 'localhost:8888'))
  	      $this->parser_path = __SERVERROOT__ . "/". __ROOTDIR__ .  '/modules/crawler/';

    }

    public function open_connection() {
        $tmp_credentials = explode(':', $this->credentials);
        $username = $tmp_credentials[0];
        $password = $tmp_credentials[1];
        if (!stristr($this->hostname, '{'))
            $this->hostname = '{' . $this->hostname . '}';
        error_log(__FILE__." ".__FUNCTION__." ".__LINE__.": hostname: ".$this->hostname." credentials: ".$this->credentials." username: ".$username." password: ".$password." mailbox: ".$this->mailbox);
        $this->stream = imap_open($this->hostname . $this->mailbox, $username, $password) or die('Cannot connect to Gmail: ' . imap_last_error());
    }

    public function close_connection() {
        imap_close($this->stream);
    }

    public function getUnparsedEmail() {

        $yesterday = strtotime('-3 day', strtotime("now"));
        $tstamp = strtotime('-3 day', $yesterday);

        $recent_messages = @imap_search($this->stream, 'SINCE "' . date("j F Y", $tstamp) . '"', SE_UID);

        if ($recent_messages == false) {
            echo 'no message';
            echo '<br/>';
            return false;
        }

        $this->emails = imap_fetch_overview($this->stream, implode(',', array_slice($recent_messages, 0, $this->message_limit)), FT_UID);
        return true;
    }

    public function parseEmails() {
        error_log(__FILE__." ".__FUNCTION__." ".__LINE__." ".count($this->emails).": emails: ");
        foreach ($this->emails as $email) {
            error_log(__FILE__." ".__FUNCTION__." ".__LINE__." parsing ".$this->provider."...");
            //if (!$this->checkExecutionTime($time_start))
            //    break;
            // get msg header and stream uid
            $msg = $this->parseHeader($email);
            
            $date = new DateTime($email->date);
            $now = new DateTime(NULL, new DateTimeZone('Asia/Singapore')); 
            $interval = $date->diff($now);
            if($interval->format('%h') > 1)
                WY_debug::sendEmailDebug('Email consolidation WARNING', 'Warning on '.$this->account_name);
            //check not parsed already 
//            if ($this->last_crawled_id >= $msg['uid']) {
//                echo 'already parsed ' . $msg['uid'] . '<br/>';
//                continue;
//            }
            //error_log(__FILE__." ".__FUNCTION__." ".__LINE__.": msg: ".print_r($msg, true));
            if ($this->email_sender == $msg['email']) {
                //get body
                //imap_fetchstructure
                $msg_body = imap_fetchbody($this->stream, $msg['uid'], 1, FT_UID);
                $header = imap_fetchheader($this->stream, $msg['uid'], FT_UID);
                $body = imap_body($this->stream, $msg['uid'], FT_UID);
                $msg_source = $header . $body;
                //$message_body['plain_html'] = imap_fetchbody($this->stream, $msg['uid'], 1, FT_UID);
                //save to db
                if ($this->insertDB($msg, $msg_source))
                    echo '<br/><br/>Inserted in DB ' . $msg['uid'] . '<br/>';

                // traitement email 
                $data = array();
                switch ($this->provider) {
                    case 'CHOPE':
                        require_once $this->parser_path . 'chope/parser_chope.php';
                        $data = parse_chope($msg_source, $this->account_name);
                        break;

                    case 'HGW':
                        require_once $this->parser_path . 'hgw/parser_hgw.php';
                        $data = parse_hgw($msg_source, $this->account_name);
                        break;

                    case 'QUANDOO':
                        require_once $this->parser_path . 'quandoo/parser_quandoo.php';
                        $data = parse_quandoo($msg_source, $this->account_name);
                        break;

					// this is an exception, make sure that you return from this switch
                    case 'MailChimp':
                        require_once("modules/crawler/mailchimp/mailchimp.php");
                        if (parse_mailchimp($msg_source, $this->account_name)) {
                            //imap_delete($this->stream, $msg['uid'], FT_UID);
                            error_log(__FILE__." ".__FUNCTION__." ".__LINE__.": Successfully processed ".$this->account_name);
                            echo $this->account_name. " successfully processed!<br/>";
                            $this->moveEmails($msg['uid'], 'SUCCESS_' . $this->account_name . '.' . $this->provider);
                        } else {
                            echo $this->account_name. " process failed!<br/>";
                            error_log(__FILE__." ".__FUNCTION__." ".__LINE__.": Error processing ".$this->account_name);
                            $this->moveEmails($msg['uid'], 'ERR_' . $this->account_name . '.' . $this->provider);
                        }
                        imap_expunge($this->stream);
                        continue;
                        
                    default:
                        break;
                	}
                
                if (empty($data["status"]) || empty($data["processing"]) || intval($data["status"]) < 0) {
                    echo 'unable to parse message <br/>';
                    $this->updateParseResult($this->internal_meesage_id, 'failed');
                    $this->moveEmails($msg['uid'], 'ERR_' . $this->account_name . '.' . $this->provider);
                    //return array(-1, "unable to parse message");
                }  
                else {
                    $booking = new WY_Booking();

                    $res = new WY_restaurant();
                    $payment = new WY_Payment();
                    $respolicy = new WY_Restaurant_policy;
                    $bkstatus = '';
                    if ($this->require_payment && $data['processing'] == 'booking') {
                        $requiredccDetails = false;
                        if ($this->restaurant == 'SG_SG_R_TheFunKitchen' || $this->restaurant == 'SG_SG_R_Bacchanalia') {
                            $res->getRestaurant($this->restaurant);

                            $requiredccDetails = $respolicy->isRequiredccDetails($this->restaurant, $data["rtime"], $data["cover"]);
                            if ($requiredccDetails)
                                $bkstatus = "pending_payment";
                        }
                    }

                    $booking->ProcessExternalBooking($data, array($this->restaurant_name => $this->restaurant), $this->provider, $bkstatus);

                    if ($booking->result < 0) {
                        echo "Error - " . $this->restaurant . ", " . $this->restaurant_name . ", " . $this->provider . ", " . $this->account_name . " :" . $booking->msg;
		                WY_debug::recordDebug("ERROR", "CRWLER", "Error - " . $this->restaurant . ", " . $this->restaurant_name . ", " . $this->provider . ", " . $this->account_name . " :" . $booking->msg);
                        $this->updateParseResult($this->internal_meesage_id, 'parsed');
                        $this->moveEmails($msg['uid'], 'ERR_'.$this->account_name . '.' . $this->provider);
                    } else {

                        //check if need to notify
                        //send notification
                        if ($data["processing"] == 'booking' && $res->checkbkdeposit() != 0 && $requiredccDetails) {

                            $notify = new WY_Notification();
                            $booking->force_notification = true;
                            $booking->tracking = $booking->tracking;
                            $notify->notify($booking, 'booking_pending', true);
                            //$logger->LogEvent($loguserid, 700, $bkconfirmation, 'CALLCENTER', '', date("Y-m-d H:i:s"));
                        }

                        //send notification
                        if ($data["processing"] == 'cancellation' && ($booking->restaurant == 'SG_SG_R_TheFunKitchen' || $booking->restaurant == 'SG_SG_R_Bacchanalia' )) {
                            //$depositdetails = $payment->getDepositDetails($booking->deposit_id, $booking->confirmation);
                            $paymentDetails = $respolicy->getResChargeDetails($booking->deposit_id, $booking->confirmation);
                            if (count($paymentDetails) > 0) {

                                $stripe = new WY_Payment_Stripe();

                                //$isPayment =1;
                                //$depositStatus = $paymentDetails['status'];
                                $depositAmount = $paymentDetails['amount'];
                                $tnc_amount = $paymentDetails['refund_amount'];

                                //$refundAmount = $paymentDetails['refund_amount'];
                                $payment_method = $paymentDetails['payment_method'];
                                if ($res->status == 'cancel') {
                                    echo format_api(0, "FAIL", 0, "Confirmation has already been canceled ");
                                    return;
                                }

                                if ($tnc_amount > 0) {
                                    if (trim($payment_method) === "carddetails") {
                                        $resData = $stripe->createStripePayment($booking->deposit_id, $booking->restaurant, $booking->confirmation, $booking->email, $tnc_amount, $type);
                                        $status = $resData['status'];
                                    }
                                }
                                if ($tnc_amount < 1 || $status == "succeeded") {
                                    $notify = new WY_Notification();
                                    $booking->force_notification = true;
                                    $booking->tracking = $booking->tracking;
                                    $notify->notify($booking, 'cancel', true);
                                    //$logger->LogEvent($loguserid, 700, $bkconfirmation, 'CALLCENTER', '', date("Y-m-d H:i:s"));
                                }
                            }
                        }

                        echo "Done <br />";
                        $this->updateParseResult($this->internal_meesage_id, 'success');
                        $this->moveEmails($msg['uid'], $this->account_name . '.' . $this->provider);
                        echo "Email stored in folder <br />";
                    }
                }
                //$this->setLastParsedId($msg['uid']);
            }
        }
    }

    public function parseHeader($header) {
        // parses header object returned from imap_fetch_overview    
        if (!isset($header->from)) {
            return false;
        } else {
            $from_arr = imap_rfc822_parse_adrlist($header->from, 'gmail.com');
            $fi = $from_arr[0];
            $msg = array(
                "uid" => (isset($header->uid)) ? $header->uid : 0,
                "personal" => (isset($fi->personal)) ? @imap_utf8($fi->personal) : "",
                "email" => (isset($fi->mailbox) && isset($fi->host)) ? $fi->mailbox . "@" . $fi->host : "",
                "mailbox" => (isset($fi->mailbox)) ? $fi->mailbox : "",
                "host" => (isset($fi->host)) ? $fi->host : "",
                "subject" => (isset($header->subject)) ? @imap_utf8($header->subject) : "",
                "message_id" => (isset($header->message_id)) ? $header->message_id : "",
                "in_reply_to" => (isset($header->in_reply_to)) ? $header->in_reply_to : "",
                "udate" => (isset($header->udate)) ? $header->udate : 0,
                "date_str" => (isset($header->date)) ? $header->date : ""
            );
            // handles fetch with uid and rfc header parsing
            if ($msg['udate'] == 0 && isset($header->date)) {
                $msg['udate'] = strtotime($header->date);
            }
            $msg['rx_email'] = '';
            $msg['rx_personal'] = '';
            $msg['rx_mailbox'] = '';
            $msg['rx_host'] = '';
            if (isset($header->to)) {
                $to_arr = imap_rfc822_parse_adrlist($header->to, 'gmail.com');
                $to_info = $to_arr[0];
                if (isset($to_info->mailbox) && isset($to_info->host)) {
                    $msg['rx_email'] = $to_info->mailbox . '@' . $to_info->host;
                }
                if (isset($to_info->personal))
                    $msg['rx_personal'] = $to_info->personal;
                if (isset($to_info->mailbox))
                    $msg['rx_mailbox'] = $to_info->mailbox;
                if (isset($to_info->host))
                    $msg['rx_host'] = $to_info->host;
            }
            return $msg;
        }
    }

    public function moveEmails($message, $destination) {
        $res = imap_mail_move($this->stream, $message, $destination, CP_UID);
        if (!$res) {
            imap_createmailbox($this->stream, $this->hostname . $destination);
            imap_mail_move($this->stream, $message, $destination, CP_UID);
        }
        imap_mail_move($this->stream, $message, $destination, CP_UID);
        return true;
    }

    public function getLastParsedId() {
        return $this->last_crawled_id;
    }

    public function setLastParsedId($id) {
        if ($this->last_crawled_id == $id) {
            return true;
        }
        $this->last_crawled_id = $id;
        $sql = "UPDATE `crawler_accounts` SET `last_crawled_id` = '$id' WHERE `crawler_accounts`.`ID` = $this->account_id;";

        return pdo_exec($sql);
    }

    public function insertDB($msg, $msg_body) {

        $sql = "INSERT INTO `crawler_email` (`ID`, `account_id`, `uid`, `personal`, `email`, `mailbox`, `host`, `subject`, `message_id`, `in_reply_to`, `udate`, `date_str`, `rx_email`, `rx_personal`, `rx_mailbox`, `rx_host`, `body`) "
                . "VALUES (NULL, '$this->account_id','$msg[uid]', '$msg[personal]', '$msg[email]', '$msg[mailbox]', '$msg[host]', :subject, '$msg[message_id]', '$msg[in_reply_to]', '$msg[udate]', '$msg[date_str]', '$msg[rx_email]', '$msg[rx_personal]', '$msg[rx_mailbox]', '$msg[rx_host]', :msg_body);";
        //$res = pdo_insert($sql);

        $db = getConnection();
        $stmt = $db->prepare($sql);
        $stmt->execute([':subject' => $msg['subject'], ':msg_body' => $msg_body]);
        $res = $db->lastInsertId();

        if ($res) {
            $this->internal_meesage_id = $res;
            return true;
        }
        return false;
    }

    public function updateParseResult($ID, $status) {
        $sql = "UPDATE `crawler_email` SET `parsed_result` = '$status' WHERE `crawler_email`.`ID` = $ID;";
        if (pdo_exec($sql)) {
            return true;
        }
        return false;
    }

    public static function getAccounts() {
        return pdo_multiple_select("SELECT * FROM crawler_accounts WHERE status = 'active' ORDER BY rand()");
    }

    public static function getOneAccount($name) {
        return pdo_multiple_select("SELECT * FROM crawler_accounts WHERE status = 'active' and name = '$name' limit 1");
    }    
}

class WY_Crawler {

    var $result;
    var $msg;

    public function __construct() { 
    	$this->result = 1;
    	$this->msg = "";
    	}
    
    function crwlr_read() {
        $data = pdo_multiple_select("SELECT * FROM crawler_accounts");
        $this->result = (count($data) > 0) ? 1 : -1;
        return $data;
    	}
    function crwlr_create($content) {
    	$args = array("name", "restaurant", "restaurant_name", "hostname", "email_sender", "mailbox", "require_payment", "provider", "credentials", "status");
    	foreach($args as $label) {
    		if(!isset($content[$label])) {
    			$this->msg = "Invalid args";
    			return $this->result = -1;
    			}
    		$$label = clean_input($content[$label]);
    		}
        $data = pdo_multiple_select("SELECT name FROM crawler_accounts where name = '$name' and provider = '$provider' limit 1");
        if(count($data) > 0) {
        	$this->msg = "duplicate names";
        	return $this->result = -1;
        	}
        
        $status	= "inactive";
    	pdo_exec("INSERT INTO crawler_accounts (name, restaurant, restaurant_name, hostname, email_sender, mailbox, require_payment, provider, credentials, status) value ('$name', '$restaurant', '$restaurant_name', '$hostname', '$email_sender', '$mailbox', '$require_payment', '$provider', '$credentials', '$status') ");
    	}
    	
    function crwlr_update($content) {
    	$args = array("name", "restaurant", "restaurant_name", "hostname", "email_sender", "mailbox", "require_payment", "provider", "credentials", "status");
    	foreach($args as $label) {
    		if(!isset($content[$label])) {
    			$this->msg = "Invalid args";
    			return $this->result = -1;
    			}
    		$$label = clean_input($content[$label]);
    		}
        $data = pdo_multiple_select("SELECT name FROM crawler_accounts where name = '$name' and provider = '$provider' limit 1");
        if(count($data) < 1) {
        	$this->msg = "name with provider does not exists";
        	return $this->result = -1;
        	}
        	
    	pdo_exec("UPDATE crawler_accounts SET restaurant = '$restaurant', restaurant_name = '$restaurant_name', hostname = '$hostname', email_sender = '$email_sender', mailbox = '$mailbox', require_payment = '$require_payment', credentials = '$credentials', status = '$status' where name = '$name' and provider = '$provider' limit 1 ");
    	}


    function crwlr_toggleactivate($content) {
    	
    	$args = array("name", "provider", "status");
    	foreach($args as $label) {
    		if(!isset($content[$label])) {
    			$this->msg = "Invalid args";
    			return $this->result = -1;
    			}
    		$$label = clean_input($content[$label]);
    		}

        pdo_exec("UPDATE crawler_accounts SET status = '$status' WHERE name = '$name' and provider = '$provider' limit 1");
		return $this->result = 1;    	
		}

    function crwlr_delete($content) {
    	}
}