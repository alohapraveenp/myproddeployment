<?php

require_once 'lib/composer/vendor/autoload.php';

use Aws\Sns\SnsClient;

class SNS {

    private $client = '';
    

    public function __construct() {
        $this->client = SnsClient::factory(array(
                    'key' => awsAccessKey,
                    'secret' => awsSecretKey,
                    'region' => 'ap-southeast-1'
        ));
    }
    public function setPlatformEndpoint($device_id,$user_id,$type,$appname){
        
        if($this->is_test_env()){
            if($type == 'ios' ){
                $platformArn = $appname == 'weeloypro' ? 'arn:aws:sns:ap-southeast-1:728152602357:app/APNS_SANDBOX/DEV_IOS_WP' : 'arn:aws:sns:ap-southeast-1:728152602357:app/APNS_SANDBOX/DEV_IOS_W';
                $topicArn =$appname == 'weeloypro' ? 'arn:aws:sns:ap-southeast-1:728152602357:DEV_IOS_WP' : 'arn:aws:sns:ap-southeast-1:728152602357:DEV_IOS_WP'; 
            } 
        }else{
            if($type == 'ios' ){
                 $platformArn = $appname == 'weeloypro' ? 'arn:aws:sns:ap-southeast-1:728152602357:app/APNS/PROD_IOS_WP' : 'arn:aws:sns:ap-southeast-1:728152602357:app/APNS/PROD_IOS_W';
                 $topicArn =$appname == 'weeloypro' ? 'rn:aws:sns:ap-southeast-1:728152602357:PROD_IOS_WP' : 'arn:aws:sns:ap-southeast-1:728152602357:PROD_IOS_W'; 
            }
        }
        if($type == 'android' ){
            $platformArn = $appname == 'weeloypro' ? 'arn:aws:sns:ap-southeast-1:728152602357:app/GCM/PROD_ANDROID_WP' : 'arn:aws:sns:ap-southeast-1:728152602357:app/GCM/PROD_ANDROID_W';
            $topicArn =$appname == 'weeloypro' ? 'arn:aws:sns:ap-southeast-1:728152602357:PROD_ANDROID_WP' : 'arn:aws:sns:ap-southeast-1:728152602357:PROD_ANDROID_W'; 
        }


        $result = $this->client->createPlatformEndpoint(array(
            // PlatformApplicationArn is required
            'PlatformApplicationArn' => $platformArn,
            // Token is required
            'Token' => $device_id,                      //'9bd32fbd05842191113e4e4eb182fdb0f483478cd46c11ccdd2107d916969fa2', 
            'CustomUserData' => $user_id,
             'Enabled'=>"true",
            'Attributes' => array(
               // ... repeated
            ),
        ));
        $subscripe = $this->client->subscribe(array(
            // TopicArn is required
            'TopicArn' => $topicArn,
            // Protocol is required
            'Protocol' => @'application',
            'Endpoint' => $result['EndpointArn'],
        ));

        
        return $subscripe;
    }
    
  
    
    public function createapplication(){
      
        $params =array(
                'Name'       => 'ios_weeloy',
                'Platform'   => 'APNS',
                'Attributes' => array( 
                  'PlatformCredential' => file_get_contents(dirname(__FILE__) . '../../ressources/notifications/apn_weeloy1.pem'),
                  'PlatformPrincipal'  => file_get_contents(dirname(__FILE__) . '../../ressources/notifications/apn_weeloy.crt'),
                ),
        );
       
        
        $result = $this->client->createPlatformApplication(array(
                'Name'       => 'ios_weeloy',
                'Platform'   => 'APNS',
                'Attributes' => array( 
                  'PlatformCredential' => file_get_contents(dirname(__FILE__) . '../../ressources/notifications/privatekey.pem'),
                  'PlatformPrincipal'  => file_get_contents(dirname(__FILE__) . '../../ressources/notifications/certificate.pem'),
                ),
        ));

    }
    
    function getEndpoints($device_type){
        
            $result = $this->client->listEndpointsByPlatformApplication(array(
                'PlatformApplicationArn' => 'arn:aws:sns:ap-southeast-1:728152602357:app/APNS/IOS_Prod_weeloy',
               ));
            return $result;
    }
    
    public function notify_apns($message,$appname,$type){
        if($this->is_test_env()){
            if($type == 'ios' ){
                $platformArn = $appname == 'weeloypro' ? 'arn:aws:sns:ap-southeast-1:728152602357:app/APNS_SANDBOX/DEV_IOS_WP' : 'arn:aws:sns:ap-southeast-1:728152602357:app/APNS_SANDBOX/DEV_IOS_W';
                $topicArn =$appname == 'weeloypro' ? 'arn:aws:sns:ap-southeast-1:728152602357:DEV_IOS_WP' : 'arn:aws:sns:ap-southeast-1:728152602357:DEV_IOS_WP'; 
            } 
        }else{
            if($type == 'ios' ){
                 $platformArn = $appname == 'weeloypro' ? 'arn:aws:sns:ap-southeast-1:728152602357:app/APNS/PROD_IOS_WP' : 'arn:aws:sns:ap-southeast-1:728152602357:app/APNS/PROD_IOS_W';
                 $topicArn =$appname == 'weeloypro' ? 'rn:aws:sns:ap-southeast-1:728152602357:PROD_IOS_WP' : 'arn:aws:sns:ap-southeast-1:728152602357:PROD_IOS_W'; 
            }
        }
        if($type == 'android' ){
            return true;
            //$platformArn = $appname == 'weeloypro' ? 'arn:aws:sns:ap-southeast-1:728152602357:app/APNS/PROD_IOS_WP' : 'arn:aws:sns:ap-southeast-1:728152602357:app/APNS/PROD_IOS_W';
        }
        $result = $this->getEndpoints($device_type);

        $result = $this->client->publish(array('Message' => $message,
                                   'TargetArn' => $topicArn,
        ));

    }
    
    
    function is_test_env() {

        if (strpos($_SERVER['HTTP_HOST'], 'api') !== false) {
            return false;
        }

        if (strpos($_SERVER['HTTP_HOST'], 'www') !== false) {
            return false;
        }

        if (strpos($_SERVER['HTTP_HOST'], 'prod') !== false) {
            return false;
        }

        if (strpos($_SERVER['HTTP_HOST'], 'codersvn') !== false) {
            return false;
        }

        return true;
    }
     
 

    
}
    
    