<?php

require_once("imagelib.inc.php");
$GLOBALS['picture_typelist'] = array('restaurant', 'event', 'logo', 'chef', 'menu', 'promotion', 'sponsor', 'profile', 'foodselfie','category');

class WY_Media {

    private $id;
    private $name;
    private $object_type;
    private $object_id;
    private $restaurant;
    private $media_type;
    private $path;
    private $status;
    private $description;
    private $globalshow;
    private $dirname;
    private $smallPicSize = '500/';
    private $mediumPicSize = '1024/';
    private $largePicSize = '1440/';
    var $tmpdir;
    var $s3;
    var $bucket;
    var $msg;

    //var $oldimg;
    //var $logo;
    //var $chef;
    //var $rPictures;  // restaurant pictures
    //var $ePictures;  // event pictures
    //var $storedPictures;  // event pictures
    //var $msg;  // event pictures
    //var $s3;
    //var $bucket;
    //var $object_type;

    function __construct($theRestaurant = '', $object_id = '', $type = 'restaurant') {
        if (AWS) {
            $this->globalshow = __S3HOST__ . __S3DIR__;
        } else {
            $this->globalshow = __BASE_URL__ . __SHOWDIR__;
        }
        if (AWS) {
            $this->globalmedia = __S3HOST__;
        } else {
            $this->globalmedia = __MEDIADIR__;
        }
        if (AWS) {
            switch ($type) {
                case 'restaurant': $this->dirname = __S3DIR__ . "$theRestaurant/";
                    break;

                case 'user':
                    $unique_folder_name = md5($theRestaurant);
                    $this->object_id = $object_id;
                    $this->dirname = __S3DIRUSER__ . "$object_id/$unique_folder_name/";
                    break;
                case 'foodselfie':
                    $unique_folder_name = 'foodselfie2015';
                    $this->dirname = "event/singapore/$unique_folder_name/";
                    break;
                 case 'category': $this->dirname = __S3DIR__ . "$theRestaurant/";
                    break;
            }
        } else {
            switch ($type) {
                case 'restaurant': $this->dirname = __UPLOADDIR__ . "$theRestaurant/";
                    break;
                case 'user':
                    // MD5 email
                    $unique_folder_name = md5($theRestaurant);
                    $this->object_id = $object_id;
                    $this->dirname = __UPLOADDIRUSER__ . "$object_id/$unique_folder_name/";
           
                    break;
                case 'foodselfie':
                    $unique_folder_name = 'foodselfie2015';
                    $this->dirname = __UPLOADDIRCONTEST__ . "$unique_folder_name/";
                   
                    //$theRestaurant ='foodselfie2015';
                    break;
                case 'category': $this->dirname = __S3DIR__ . "$theRestaurant/";
                    break;
            }
        }
        $this->tmpdir = __TMPDIR__;
        $this->restaurant = $theRestaurant;
    }

    public function getName() {
        return $this->name;
    }

    public function getDescription() {
        return $this->description;
    }

    public function getPath() {
        return $this->path;
    }

    public function constructPath() {
        return $this->path;
    }

    public function getPartialPath() {
        return $this->globalshow . $this->restaurant . '/';
    }

    public function getFullPath($size = 'small', $file_name = '') {
        if (!empty($file_name)) {
            $this->name = $file_name;
        }
        $path = '';

       // if (AWS) {
            $random = rand(1, 5);
            switch ($random) {
                case 1:
                    $this->globalshow = __S3HOST1__ . __S3DIR__;
                    break;
                case 2:
                    $this->globalshow = __S3HOST2__ . __S3DIR__;
                    break;
                case 3:
                    $this->globalshow = __S3HOST3__ . __S3DIR__;
                    break;
                case 4:
                    $this->globalshow = __S3HOST4__ . __S3DIR__;
                    break;
                case 5:
                    $this->globalshow = __S3HOST5__ . __S3DIR__;
                    break;
                default :
                    $this->globalshow = __S3HOST__ . __S3DIR__;
                    break;
            }
      //  } else {
      //      $this->globalshow = 'http://localhost:8888' . __SHOWDIR__;
      //   }

        switch ($size) {
            case '100':
                $path = $this->globalshow . $this->restaurant . '/100/' . $this->name;
                break;
            case '140':
                $path = $this->globalshow . $this->restaurant . '/140/' . $this->name;
                break;
            case '180':
                $path = $this->globalshow . $this->restaurant . '/180/' . $this->name;
                break;
            case '270':
                $path = $this->globalshow . $this->restaurant . '/270/' . $this->name;
                break;
            case '300':
                $path = $this->globalshow . $this->restaurant . '/300/' . $this->name;
                break;
            case '325':
                $path = $this->globalshow . $this->restaurant . '/325/' . $this->name;
                break;
            case '360':
                $path = $this->globalshow . $this->restaurant . '/360/' . $this->name;
                break;
            case '450':
                $path = $this->globalshow . $this->restaurant . '/450/' . $this->name;
                break;
            case '500':
                $path = $this->globalshow . $this->restaurant . '/500/' . $this->name;
                break;
            case '600':
                $path = $this->globalshow . $this->restaurant . '/600/' . $this->name;
                break;
            case '700':
                $path = $this->globalshow . $this->restaurant . '/700/' . $this->name;
                break;
            case 'small':
                $path = $this->globalshow . $this->restaurant . '/' . $this->name;
                break;
            case 'medium':
                $path = $this->globalshow . $this->restaurant . '/' . $this->mediumPicSize . $this->name;
                break;
            case 'large':
                $path = $this->globalshow . $this->restaurant . '/' . $this->largePicSize . $this->name;
                break;
        }

        if ($path == '') {
            $path = $this->globalshow . $this->restaurant . '/' . $this->name;
        }
        return $path;
    }

    public function getFullPathWheelvalue($wheelvalue) {

        $path = '';
        if (AWS) {
            $random = rand(1, 5);
            switch ($random) {
                case 1:
                    $this->globalmedia = __S3HOST1__;
                    break;
                case 2:
                    $this->globalmedia = __S3HOST2__;
                    break;
                case 3:
                    $this->globalmedia = __S3HOST3__;
                    break;
                case 4:
                    $this->globalmedia = __S3HOST4__;
                    break;
                case 5:
                    $this->globalmedia = __S3HOST5__;
                    break;
                default :
                    $this->globalmedia = __S3HOST__;
                    break;
            }
        } else {
            $this->globalmedia = __BASE_URL__ . __MEDIADIR__;
        }


        if ($path == '') {
            $path = $this->globalmedia . "upload/wheelvalues/wheelvalue_" . $wheelvalue . ".png";
        }
        return $path;
    }

    function showPicture($subdir, $name, $size = '') {

        if (!empty($size)) {
            $size = $size . '/';
        }

        if ($subdir == "images")
            return "$subdir/" . $size . $name;

        return $this->globalshow . "$subdir/" . $size . $name;
    }

    public function setMedia($data) {
        $this->id = $data['id'];
        $this->name = $data['name'];
        $this->object_type = $data['object_type'];
        $this->restaurant = $data['restaurant'];
        $this->media_type = $data['media_type'];
        $this->path = $data['path'];
        $this->status = $data['status'];
        $this->description = $data['description'];
    }

    function insertMedia($name, $object_type, $restaurant, $media_type, $path) {
        $sql = "INSERT INTO media ('name','object_type','restaurant',media_type','path','status') VALUES ('$name', '$object_type','$restaurant', '$media_type', '$path', 'active')";
        return pdo_exec($sql);
    }

    function deleteMedia($restaurant, $name, $category) {
        global $picture_typelist;

        $log = $this->deleteImg($restaurant, $name, $category);
        if ($log > 0)
            pdo_exec("DELETE FROM media WHERE name = '$name' and object_type = '$category' and restaurant = '$restaurant' limit 1");
        return $log;
    }

    function updateMedia($restaurant, $name, $category, $type, $description, $status, $morder) {
        pdo_exec("update media set object_type = '$category', description = '$description', status = '$status', morder = '$morder' where restaurant = '$restaurant' and name = '$name' and media_type = 'picture' limit 1");
        return 1;
    }

    function getRestaurantPictures($restaurant,$type='restuarant') {
        if($type==='restaurant'){
            $sql = "SELECT id, name, object_type, restaurant, media_type, path, description, status FROM media WHERE object_type = 'restaurant' && restaurant = '$restaurant' && media_type = 'picture' && status = 'active' ORDER BY morder";
        }else{
          $sql = "SELECT id, name, object_type, restaurant, media_type, path, description, status FROM media WHERE object_type IN( 'restaurant','event' ) && restaurant = '$restaurant' && media_type = 'picture'  ORDER BY morder";  
        }
        $data = pdo_multiple_select($sql);
        $tmpAr = array();
        foreach ($data as $d) {
            $obj = new WY_Media;
            $obj->setMedia($d);
            $tmpAr[] = $obj;
            unset($obj);
        }
        return $tmpAr;
    }

    function getRestaurantGallery($restaurant) {
        $sql = "SELECT id, name, object_type, restaurant, media_type, path, description, status, morder FROM media WHERE object_type = 'restaurant' && restaurant = '$restaurant' && media_type = 'picture' && status = 'active' ORDER BY morder ASC";
        $data = pdo_multiple_select($sql);
        $tmpAr = array();
        foreach ($data as $d) {
            $obj = new WY_Media;
            $obj->setMedia($d);
            $tmpAr[] = $obj;
            unset($obj);
        }
        return $tmpAr;
    }
    
    function getRestaurantRandomPictures($restaurant, $page) {
        $where = '';
        $limit = '';
        if($restaurant != 'random' ){
            $where .= " AND restaurant = ".$restaurant;
        }
        if(!empty($page)){
            $limit_start = $page * 24;
            $limit .= ' LIMIT '.$limit_start.',24';
        }
        
        $sql = "SELECT name, media.restaurant, path FROM media, restaurant WHERE restaurant.restaurant = media.restaurant and restaurant.status = 'active' and media.status = 'active' and media_type = 'picture' and object_type = 'restaurant' " . $where . $limit;

        return pdo_multiple_select($sql);
    }
    


    function checkRestaurantMedia($restaurant, $name) {

        $data = pdo_multiple_select("SELECT name FROM media WHERE restaurant = '$restaurant' && name = '$name' LIMIT 1 ");
        return (count($data) > 0);
    }

    function checkProfileMedia($restaurant, $name) {

        $data = pdo_single_select("SELECT count(*) as number FROM media WHERE restaurant = '$restaurant' && object_type = 'profile'");
        if ($data['number'] > 1) {
            pdo_exec("DELETE FROM media WHERE object_type = 'profile' && restaurant = '$restaurant' && media_type = 'picture'");
            //pdo_exec("UPDATE media SET status= 'inactive' WHERE restaurant = '$restaurant' && object_type = 'profile' && status = 'active'"); 
            //$data = pdo_single_select("SELECT count(*) as number FROM media WHERE restaurant = '$restaurant' && object_type = 'profile' && status = 'active'");
        }
        if ($data['number'] == 1) {
            return true;
        } else {
            return false;
        }
    }
    function checktagMedia($tag) {

        $data = pdo_single_select("SELECT count(*) as number FROM media WHERE restaurant = '$tag' && object_type = 'category'");
        if ($data['number'] > 1) {
            pdo_exec("DELETE FROM media WHERE object_type = 'category' && restaurant = '$tag' && media_type = 'picture'");
            //pdo_exec("UPDATE media SET status= 'inactive' WHERE restaurant = '$restaurant' && object_type = 'profile' && status = 'active'"); 
            //$data = pdo_single_select("SELECT count(*) as number FROM media WHERE restaurant = '$restaurant' && object_type = 'profile' && status = 'active'");
        }
        if ($data['number'] == 1) {
            return true;
        } else {
            return false;
        }
    }

    function getRestaurantMedia($restaurant, $type ='picture') {

//        if ($type != "picture")
//            $type = "picture";  // we will add other type
        $sql = "SELECT name, object_type, restaurant, media_type, path, description, morder, status FROM media WHERE restaurant = '$restaurant' && media_type = '$type' ORDER BY object_type, morder,name";
        $data = pdo_multiple_select($sql);
        $tmpAr = array();
        $k = 0;
        foreach ($data as $d) {
            $tmpAr[$k]['name'] = $d['name'];
            $tmpAr[$k]['morder'] = $d['morder'];
            $tmpAr[$k]['object_type'] = $d['object_type'];
            $tmpAr[$k]['restaurant'] = $d['restaurant'];
            $tmpAr[$k]['media_type'] = $d['media_type'];
            $tmpAr[$k]['path'] = $d['path'];
            $tmpAr[$k]['description'] = $d['description'];
            $tmpAr[$k]['status'] = $d['status'];
            $k++;
        }
        return $tmpAr;
    }
    function getCategoryMedia($restaurant, $type) {

        if ($type != "picture")
            $type = "picture";  // we will add other type
        $sql = "SELECT name, object_type, restaurant, media_type, path, description, morder, status FROM media WHERE object_type = '$restaurant' && media_type = '$type' ORDER BY object_type, morder,name";
        $data = pdo_multiple_select($sql);
        $tmpAr = array();
        $k = 0;
        foreach ($data as $d) {
            $tmpAr[$k]['name'] = $d['name'];
            $tmpAr[$k]['morder'] = $d['morder'];
            $tmpAr[$k]['object_type'] = $d['object_type'];
            $tmpAr[$k]['restaurant'] = $d['restaurant'];
            $tmpAr[$k]['media_type'] = $d['media_type'];
            $tmpAr[$k]['path'] = $d['path'];
            $tmpAr[$k]['description'] = $d['description'];
            $tmpAr[$k]['status'] = $d['status'];
            $k++;
        }
        return $tmpAr;
    }

    function setRestaurant($restaurant) {
        $this->restaurant = $restaurant;
    }

    function sponsorlist($restaurant) {
        $sql = "SELECT name FROM media WHERE object_type = 'sponsor' && restaurant = '$restaurant' && media_type = 'picture' && status = 'active' ";
        return pdo_multiple_select_index($sql);
    }

    function getRestaurantPictureNames($restaurant) {
        $sql = "SELECT name FROM media WHERE object_type = 'restaurant' && restaurant = '$restaurant' && media_type = 'picture' && status = 'active' ";
        return pdo_multiple_select_index($sql);
    }

    function getRestaurantPictureType($restaurant, $type) {
        $sql = "SELECT name FROM media WHERE object_type = '$type' && restaurant = '$restaurant' && media_type = 'picture' && status = 'active' ";
        return pdo_multiple_select_index($sql);
    }

    function getEventFullPath($restaurant, $pic_name, $size = 'small') {
        $path = $this->globalshow . $restaurant . '/' . $pic_name;
        return $path;
    }

    function getEventPictureNames($restaurant) {
        $sql = "SELECT name FROM media WHERE object_type = 'event' && restaurant = '$restaurant' && media_type = 'picture' && status = 'active' ";
        return pdo_multiple_select_index($sql);
    }

    function getDefaultPicture($restaurant, $size = 'small') {
        $sql = "SELECT id, name, object_type, restaurant, media_type, path, status, description FROM media 
                WHERE object_type = 'restaurant' && restaurant = '$restaurant' && media_type = 'picture' && status = 'active' ORDER BY morder, name ASC LIMIT 1 ";

        $data = pdo_single_select($sql);
        if(count($data)<1){
            return false;
        }
        $this->setMedia($data);
        return $this->getFullPath($size);
    }

    function getRandomPicture($restaurant, $size = 'small') {
        //$sql = "SELECT id, name, object_type, restaurant, media_type, path, status, description FROM media 
        //        WHERE object_type = 'restaurant' && restaurant = '$restaurant' && media_type = 'picture' && status = 'active' ORDER BY morder, name ASC LIMIT 1 ";

        $sql = "SELECT id, name, object_type, restaurant, media_type, path, status, description FROM media 
                WHERE object_type = 'restaurant' && restaurant = '$restaurant' && media_type = 'picture' && status = 'active' ORDER BY RAND() LIMIT 1 ";

        $data = pdo_single_select($sql);
        if(count($data)<1){
            return false;
        }
        $this->setMedia($data);

        return $this->getFullPath($size);
    }

    function getEventDefaultPicture($event, $size = 'small') {
        $sql = "SELECT id, name, object_type, restaurant, media_type, path, status, description FROM media 
                WHERE object_type = 'restaurant' && restaurant = '$event' && media_type = 'picture' && status = 'active' ORDER BY morder, name ASC LIMIT 1 ";

        $data = pdo_single_select($sql);
        if(count($data)<1){
            return false;
        }
        $this->setMedia($data);
        return $this->getFullPath($size);
    }

    function getWidgetDefaultPicture($restaurant, $size = 'small') {
        $sql = "SELECT id, name, object_type, restaurant, media_type, path, status, description FROM media 
                WHERE object_type = 'restaurant' && restaurant = '$restaurant' && media_type = 'picture' && status = 'active' ORDER BY RAND() LIMIT 1 ";

        $data = pdo_single_select($sql);
        if(count($data)<1){
            return false;
        }
        $this->setMedia($data);
        return $this->getFullPath($size);
    }

    function getLogo($restaurant) {
        $sql = "SELECT id, name, object_type, restaurant, media_type, path, status, description FROM media 
                WHERE object_type = 'logo' && restaurant = '$restaurant' && media_type = 'picture' && status = 'active' ORDER BY morder LIMIT 1 ";
        $data = pdo_single_select($sql);
        if(count($data)<1){
            return false;
        }
        $this->setMedia($data);
        return $this->getFullPath('small');
    }

    function getChef($restaurant) {
        $sql = "SELECT id, name, object_type, restaurant, media_type, path, status, description FROM media 
                WHERE object_type = 'chef' && restaurant = '$restaurant' && media_type = 'picture' && status = 'active' ORDER BY morder LIMIT 1 ";
        $data = pdo_single_select($sql);
        if(count($data)<1){
            return false;
        }
        $this->setMedia($data);
        return $this->getFullPath('small');
    }

    function getWheel($restaurant) {
        return $this->globalshow . $restaurant . '/wheel.png';
    }

    function getWheelValue($value) {
        return $this->globalshow . '../wheelvalues/wheelvalue_' . $value . '.png';
    }

    function getUserProfilePicture($user) {
        $sql = "SELECT id, name, object_type, restaurant, media_type, path, status, description FROM media 
                WHERE object_type = 'profile' && restaurant = '$user' && media_type = 'picture' && status = 'active' ORDER BY morder ASC, id DESC LIMIT 1 ";
        $data = pdo_single_select($sql);
        if (!$data)
            return NULL;
        $this->setMedia($data);
        $path = $data['path'] . $this->name;
        return $path;
    }

    function uploadImage($restaurant, $category, $type = 'restaurant', $extra = NULL, $media_type = 'picture') {
        error_log(__FILE__." ".__FUNCTION__." ".__LINE__.": restaurant: ".$restaurant." category: ".$category." type: ".$type);
        $log = 0;
        $msg = "Unable to Upload image. Try again";
        ini_set('memory_limit', '2048M');
        if (isset($_POST["Stage"]) && $restaurant != "")
            if ($_POST["Stage"] == "Load") {
                list($log, $msg) = $this->modifyRecord($category);
                if (intval($log) <= 0)
                    return array("-1", $msg);
                $filename = $msg;
                $Ar[0] = $this->addImg($restaurant, $filename, $category, $type, $extra, $media_type);
                $Ar[1] = $this->msg;
                return $Ar;
            }
        return array(-1, "Unknow Error");
    }

    /* photoContest upload function */

    function uploadphotoContest($obj, $category) {
        $type = $category;

        $bkEmail = $obj['bkemail'];
        error_log("uploadImage = " . $bkEmail);

        $log = 0;
        $msg = "Unable to Upload image. Try again";

        //list($log, $msg,$ofname) = $this->modifyRecord($category);
        list($log, $msg) = $this->modifyRecord($category);
        echo "after modifycall function";

        if (intval($log) <= 0)
            return array("-1", $msg);
        echo "after return array list";
        $filename = $msg;
        $ofname = $filename;
        echo "before addphoto";
        $Ar[0] = $this->addPhoto($obj, $bkEmail, $filename, $ofname, $category, $type);
        $Ar[1] = $this->msg;
        echo "after addPhoto";
        return $Ar;
    }

    function addPhoto($obj, $target_object, $filename, $ofname, $category, $type) {
    
        if (isset($obj)) {
            $bkEmail = $obj['bkemail'];
            $bkusername = $obj['bkusername'];
            $bkconfirmation = $obj['bkconfirmation'];
            $displayName = $obj['displayName'];
            $wheelwin = $obj['wheelwin'];
            $restaurant = $obj['restaurant'];
            $reviewCount = $obj['reviewCount'];
        }
        echo "after object";
        global $picture_typelist;

        if ($filename == "" || !in_array($category, $picture_typelist))
            return $this->retstatus(-1, "Invalid Category  or empty image");

        if ($this->l_file_exists($filename) == false)
            return $this->retstatus(-1, "The file has not been uploaded " . $filename);


        if ($type == 'foodselfie') {
            //$key = $this->object_id .'/'.md5($target_object);
            $path = $this->dirname . $filename;
        }

        if ($obj['type'] == 'admin') {
            if ($obj['datacount'] > 0) {
                $response = pdo_exec("update  _2015_photo_contest set uploadurl = '$path', filename='$filename',originalfname='$ofname'  where displayname ='$displayName'");
            } else {
                $sql = "INSERT INTO _2015_photo_contest (email,username,confirmation,displayname,fblike,share,filename,originalfname,uploadurl,reviews,wheelwin,restaurant_name,totalcount) values('$bkEmail','$bkusername','$bkconfirmation','$displayName','0','0','$filename','$ofname','$path','$reviewCount','$wheelwin','$restaurant','0')";
                pdo_exec($sql);
            }
        } else {
            $sql = "INSERT INTO _2015_photo_contest (email,username,confirmation,displayname,fblike,share,filename,originalfname,uploadurl,reviews,wheelwin,restaurant_name,totalcount) values('$bkEmail','$bkusername','$bkconfirmation','$displayName','0','0','$filename','$ofname','$path','$reviewCount','$wheelwin','$restaurant','0')";
            echo $sql;
            $result = pdo_exec($sql);
            //var_dump($result);
        }
        return $this->retstatus(1, "The image '" . $ofname . "' has been uploaded ($category) :status=>pending ");
    }

    function modifyRecord($category) {
        //error_log(print_r(debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS), true));
        $file = $_FILES['file'];
        $allowedExts = array("gif", "jpeg", "jpg", "png", "pdf");
        $allowedTypes = array("image/gif", "image/jpeg", "image/jpg", "image/png", "application/pdf","binary/octet-stream");
        $extension = strtolower(pathinfo($_FILES["file"]["name"], PATHINFO_EXTENSION));
        if ((!in_array($file['type'], $allowedTypes)) || ($_FILES["file"]["size"] >= PHOTO_CONTEST_MAX_FILE_SIZE) || !in_array($extension, $allowedExts))
            return array("-1", "Invalid file " . $file['name'] . " type = " . $file['type'] . " size = " . $file['size'] . " MAXSIZE " . PHOTO_CONTEST_MAX_FILE_SIZE);
        $filename = str_replace(' ', '_', $file['name']);
        if($category == 'foodselfie' || $category == 'profile'){
            $tmp_ext = explode('.', $filename);
            $ext = '.'.$tmp_ext[count($tmp_ext)-1];
            $filename = uniqid().$ext;
        }
        //var_dump((!in_array($file['type'], $allowedTypes)) || ($_FILES["file"]["size"] >= PHOTO-CONTEST_MAX_FILE_SIZE) || !in_array($extension, $allowedExts));die;
        if ($file["error"] > 0)
            return array("-1", "Return Code: " . $file['error'] . "<br>");
        if ($category != 'profile' && $this->l_file_exists($filename))
            return array("-1", $filename . " already exists. ");
        $tmp_image = "l_" . $filename;
        error_log(__FILE__." ".__FUNCTION__." ".__LINE__.": file: ".print_r($file, true)." filename: ".$filename." tmp_image: ".$tmp_image." tmp_name: ".$file['tmp_name']." size: ".filesize($file['tmp_name']));
        $scope = ($category != "sponsor") ? NULL : "sponsor";
        if ($category != "sponsor" && $extension != "pdf") {
            error_log(__FILE__." ".__FUNCTION__." ".__LINE__.": -> process(".$file['tmp_name'].")");
           $resData = $this->process($file['tmp_name'], $filename);
        } else {
            error_log(__FILE__." ".__FUNCTION__." ".__LINE__.": -> upload()");
            $this->upload($file['tmp_name'], $tmp_image, $file['type']);
        }
        if (file_exists($file['tmp_name']))
            unlink($file['tmp_name']);
        return array("1", $filename);
    }
    function addImg($target_object, $image, $category, $type, $extra_field, $media_type = 'picture') {
        global $picture_typelist;

        if ($image == "" || !in_array($category, $picture_typelist))
            return $this->retstatus(-1, "Invalid Category  or empty image");

        if ($this->l_file_exists($image) == false)
            return $this->retstatus(-1, "The file has not been uploaded " . $image);
        //kala added this line for user profile pic update
        if ($type != 'user') {
            if ($this->checkRestaurantMedia($target_object, $image) == true)
                return $this->retstatus(-1, "The image name already exists");
        }

        $sql = '';
        
        
        switch ($type) {
            case 'user':
                $key = $this->object_id . '/' . md5($target_object);
                //if user already have profile pic means update new pic else create new pic
                   
                if ($this->checkProfileMedia($target_object, $image) == false) {
                    $sql = "INSERT INTO media (object_type, restaurant, media_type, name, path, status) VALUES ('$category', '$target_object', '$media_type', '$image', '/upload/user/$key/','active') ";
                } else {
                    $type = "profile";
                    $sql = "UPDATE media SET path = '/upload/user/$key/',name='$image' WHERE object_type = '$type' && restaurant = '$target_object' && media_type = '$media_type'&& status='active'";
                }
                break;
            case 'selfie-contest-2015':
                
                $key = md5($target_object);
                $path = $this->dirname.$image;
                $sql = "INSERT INTO _2015_photo_contest (email, username, confirmation, displayname, fblike,share,filename,originalfname,uploadurl,reviews,wheelwin,restaurant_name,totalcount) values('$target_object','$extra_field[username]','$extra_field[confirmation]','$extra_field[display_name]','0','0','$image','','$path','','$extra_field[wheelwin]','$extra_field[title]','0')";
                break;
            case 'category':
                if ($this->checktagMedia($target_object) == false) {
                    $sql = "INSERT INTO media (object_type, restaurant, media_type, name, path, status) VALUES ('$category', '$target_object', '$media_type', '$image', '/upload/$category/$target_object/','active') ";
                }
                else {
                    $sql = "UPDATE media SET path = '/upload/$category/$target_object/',name='$image' WHERE object_type = '$category' && restaurant = '$target_object' && media_type = '$media_type'&& status='active'";
                }
                break;
            default:
                $sql = "INSERT INTO media (object_type, restaurant, media_type, name, path, status) VALUES ('$category', '$target_object', '$media_type', '$image', '/upload/restaurant/$target_object/','active') ";
        }

        pdo_exec($sql);
        return $this->retstatus(1, "The image '" . $image . "' has been uploaded ($category)");
    }

    function getProfilePic($type, $userId) {

        $sql = "SELECT id, name, object_type, restaurant, media_type, path, status, description FROM media 
                WHERE object_type = 'profile' && restaurant = '$userId' && media_type = 'picture' && status = 'active' ORDER BY morder ASC, id DESC LIMIT 1 ";
        $data = pdo_single_select($sql);

        if (count($data) < 1) {
            return NULL;
        }else{
           return $data; 
        }
    }

    function deleteImg($restaurant, $image, $category) {
        global $picture_typelist;

        if ($image == "" || !in_array($category, $picture_typelist))
            return $this->retstatus(-1, "Empty image or wrong Type " . $image);

        $arg = ($category != "sponsor") ? NULL : "sponsor";
        if ($this->l_file_exists($image) == true) {
            $this->l_unlink($image, $arg);
        }

        if ($this->checkRestaurantMedia($restaurant, $image) == false)
            return $this->retstatus(-1, "The image does not exist");

        pdo_exec("DELETE FROM media WHERE object_type = '$category' && restaurant = '$restaurant' && media_type = 'picture' && name = '$image'");

        return $this->retstatus(1, "The image has been deleted " . $image);
    }

    function getWheelSource($wheelvalue) {
        return $this->getFullPathWheelvalue($wheelvalue);
    }
    
    function getVideoRestaurantDishes($restaurant) { // dynamic video link 
        $link ='';
        $sql = "SELECT id, name, object_type, restaurant, media_type, path, status, description FROM media 
                WHERE object_type ='restaurant' && restaurant ='$restaurant' && media_type = 'video' && status = 'active' LIMIT 1 ";
        $data = pdo_single_select($sql);
            $length = count($data);
        if($length>0)
            $link = $data['path'];
        return $link; 
    }
    function l_file_exists($fname) {

        if (AWS) {
            require_once("conf/conf.s3.inc.php");
            return $this->getS3()->getObjectInfo($this->bucket, $this->dirname . $fname);
        } else {
            return file_exists($this->dirname . $fname);
        }
    }

    function l_unlink($fname, $scope = NULL) {
        if (AWS) {
            require_once("conf/conf.s3.inc.php");
            if ($scope == NULL) {
                $this->getS3()->deleteObject($this->bucket, $this->dirname . '1440/' . $fname);
                $this->getS3()->deleteObject($this->bucket, $this->dirname . '1024/' . $fname);
                $this->getS3()->deleteObject($this->bucket, $this->dirname . '500/' . $fname);
            }
            return $this->getS3()->deleteObject($this->bucket, $this->dirname . $fname);
        } else {
            error_log(__FILE__." ".__FUNCTION__." ".__LINE__.": dirname ".$this->dirname." fname: ".$fname);
            if ($scope == NULL) {
                unlink($this->dirname . '1440/' . $fname);
                unlink($this->dirname . '1024/' . $fname);
                unlink($this->dirname . '500/' . $fname);
            }
            return unlink($this->dirname . $fname);
        }
    }

    function upload($name, $fname, $content_type = 'image/jpeg', $object_type = '') {
        if (AWS && file_exists($name) && filesize($name)) {
            require_once 'conf/conf.s3.inc.php';
            if ($object_type == 'wheel') {
                $cache_value_browser = "1";
                $cache_value_cdn = "1";
            } else {
                $cache_value_browser = "1296000";
                $cache_value_cdn = "86400";
            }
            error_log(__FILE__." ".__FUNCTION__." ".__LINE__." tmp: ".$this->tmpdir." name: ".$name." size ".filesize($name)." dirname: ".$this->dirname." fname: ".$fname." content " .$content_type);
            $this->getS3()->putObjectFile($name, $this->bucket, $this->dirname . $fname, S3::ACL_PUBLIC_READ, array("Cache-Control" => "max-age=$cache_value_browser, s-maxage=$cache_value_cdn", "Content-Type" => "$content_type"), array("Cache-Control" => "max-age=$cache_value_browser, s-maxage=$cache_value_cdn", "Content-Type" => "$content_type"));
            error_log(__FILE__." ".__FUNCTION__." ".__LINE__." tmp: ".$this->tmpdir." name: ".$name." size ".filesize($name)." dirname: ".$this->dirname." fname: ".$fname." content " .$content_type);
        } else
            error_log(__FILE__." ".__FUNCTION__." ".__LINE__.": Invalid file: ".$name." size: ".filesize($name));
        //error_log(__FILE__." ".__FUNCTION__." ".__LINE__." name: ".$name." size: ".filesize($name));
    }
    function process($oname, $fname) {
        if (AWS) {
            require_once("conf/conf.s3.inc.php");
            //$onameTmp = $this->tmpdir . $oname;
            $destination = $this->tmpdir . $fname;
            if (strpos($oname, '/api/') !== false)
                $oname = str_replace('/api/', '/', $oname);
            if (strpos($destination, '/api/') !== false)
                $destination = str_replace('/api/', '/', $destination);
            //error_log(__FILE__." ".__FUNCTION__." ".__LINE__.": ".$oname." size ".filesize($oname)." fname: ".$fname." destination: ".$destination);
            if (file_exists($oname) && filesize($oname)) {
                // 1440:
                if (!resizecompress($oname, $destination, 1440) || filesize($destination) > filesize($oname)) {
                    error_log(__FILE__." ".__FUNCTION__." ".__LINE__.": 1440 UnOptimized resizecompress of ".$destination." failed! Try Optimized...");
                    if (!resizecompress($oname, $destination, 1440, true))
                        error_log(__FILE__." ".__FUNCTION__." ".__LINE__.": 1440 Optimized resizecompress of ".$destination." failed!");
                } 
                if (file_exists($destination) && filesize($destination)) {
                    //error_log(__FILE__." ".__FUNCTION__." ".__LINE__.": Uploading 1440 ".$destination." to S3...");
                    $this->upload($destination, '1440/' . $fname);
                    unlink($destination);
                }
                // 1024:
                if (!resizecompress($oname, $destination, 1024) || filesize($destination) > filesize($oname)) {
                    error_log(__FILE__." ".__FUNCTION__." ".__LINE__.": 1024 UnOptimized resizecompress of ".$destination." failed! Try Optimized...");
                    if (!resizecompress($oname, $destination, 1024, true))
                        error_log(__FILE__." ".__FUNCTION__." ".__LINE__.": 1024 Optimized resizecompress of ".$destination." failed!");
                } 
                if (file_exists($destination) && filesize($destination)) {
                    error_log(__FILE__." ".__FUNCTION__." ".__LINE__.": Uploading 1024 ".$destination." to S3...");
                    $this->upload($destination, '1024/' . $fname);
                    unlink($destination);
                }
                // 500:
                if (!resizecompress($oname, $destination, 500) || filesize($destination) > filesize($oname)) {
                    error_log(__FILE__." ".__FUNCTION__." ".__LINE__.": 500 UnOptimized resizecompress of ".$destination." failed! Try Optimized...");
                    if (!resizecompress($oname, $destination, 500, true))
                        error_log(__FILE__." ".__FUNCTION__." ".__LINE__.": 500 Optimized resizecompress of ".$destination." failed!");
                } 
                if (file_exists($destination) && filesize($destination)) {
                    //error_log(__FILE__." ".__FUNCTION__." ".__LINE__.": Uploading 500 ".$destination." to S3...");
                    $this->upload($destination, '500/' . $fname);
                    unlink($destination);
                }                
                // default:
                if (!resizecompress($oname, $destination) || filesize($destination) > filesize($oname)) {
                    error_log(__FILE__." ".__FUNCTION__." ".__LINE__.": Default UnOptimized resizecompress of ".$destination." failed! Try Optimized...");
                    if (!resizecompress($oname, $destination, true))
                        error_log(__FILE__." ".__FUNCTION__." ".__LINE__.": Default Optimized resizecompress of ".$destination." failed!");
                } 
                if (file_exists($destination) && filesize($destination)) {
                    //error_log(__FILE__." ".__FUNCTION__." ".__LINE__.": Uploading Default ".$destination." to S3...");
                    $this->upload($destination, $fname);
                    unlink($destination);
                }      
                return true;
            } else
                //error_log(__FILE__." ".__FUNCTION__." ".__LINE__.": Invalid input file: ".$oname." size: ".filesize($oname));
            return false;
        } else {
            $this->l_folder_exist($this->dirname . '1440/');
            $this->l_folder_exist($this->dirname . '1024/');
            $this->l_folder_exist($this->dirname . '500/');
            resizecompress($this->dirname . $oname, $this->dirname . '1440/' . $fname, 1440);
            resizecompress($this->dirname . $oname, $this->dirname . '1024/' . $fname, 1024);
            resizecompress($this->dirname . $oname, $this->dirname . '500/' . $fname, 500);
            return resizecompress($this->dirname . $oname, $this->dirname . $fname);
        }
    }

    function l_resize($oname, $fname, $size) {
        if (AWS) {
            require_once("conf/conf.s3.inc.php");

            $onameTmp = $this->tmpdir . $this->restaurant . $oname;
            $fnameTmp = $this->tmpdir . $this->restaurant . $fname;

            if (strpos($onameTmp, '/api/') !== false)
                $onameTmp = str_replace('/api/', '/', $onameTmp);
            if (strpos($fnameTmp, '/api/') !== false)
                $fnameTmp = str_replace('/api/', '/', $fnameTmp);
            error_log(__FILE__." ".__FUNCTION__." ".__LINE__." tmp: ".$this->tmpdir." onameTmp: ".$onameTmp." fnameTmp: ".$fnameTmp);
            if ($this->getS3()->getObject($this->bucket, $this->dirname . $oname, $onameTmp)) {
                if (resizecompress($onameTmp, $fnameTmp, $size, true)) {
                    $this->l_move_uploaded_file($fnameTmp, $size . '/' . $fname);
                }
                if (file_exists($oname)) {
                    $this->l_unlink($oname);
                }
                if (file_exists($onameTmp)) {
                    unlink($onameTmp);
                }
                if (file_exists($fnameTmp)) {
                    unlink($fnameTmp);
                }
                return true;
            }
            return false;
        } else {
            $this->l_folder_exist($this->dirname . $size . '/');
            return resizecompress($this->dirname . $oname, $this->dirname . $size . '/' . $fname, $size);
        }
    }

    function l_folder_exist($path = NULL) {
        if (empty($path)) {
            $path = $this->dirname;
        };
        if (AWS) {
            return true;
        } else {
            $res = (file_exists($path) !== true);
            if ($res) {
                mkdir($path, 0777, true);
            }
        }
    }

    function retstatus($log, $mess) {
        $this->msg = $mess;
        return $log;
    }

    function getS3() {
           if (!isset($this->s3)) {
            $this->bucket = awsBucket;
            $this->s3 = new S3(awsAccessKey, awsSecretKey);
            //$this->s3->putBucket($this->bucket, S3::ACL_PUBLIC_READ);
        }
        return $this->s3;
    }
    
    function l_resizecompress($oname, $fname) {
        if (AWS) {
            require_once("conf/conf.s3.inc.php");


            $onameTmp = $this->tmpdir . $this->restaurant . $oname;
            $fnameTmp = $this->tmpdir . $this->restaurant . $fname;


            if (strpos($onameTmp, '/api/') !== false) {
                $onameTmp = str_replace('/api/', '/', $onameTmp);
            }
            if (strpos($fnameTmp, '/api/') !== false) {
                $fnameTmp = str_replace('/api/', '/', $fnameTmp);
            }
         
            if ($this->getS3()->getObject($this->bucket, $this->dirname . $oname, $onameTmp)) {
                        
                if (resizecompress($onameTmp, $fnameTmp, 1440)) {
                    $this->l_move_uploaded_file($fnameTmp, '1440/' . $fname);
                }
                if (resizecompress($onameTmp, $fnameTmp, 1024)) {
                      $this->l_move_uploaded_file($fnameTmp, '1024/' . $fname);
                }
                if (resizecompress($onameTmp, $fnameTmp, 500)) {
                      $this->l_move_uploaded_file($fnameTmp, '500/' . $fname);
                }
                if (resizecompress($onameTmp, $fnameTmp)) {
                   $this->l_move_uploaded_file($fnameTmp, $fname);
                }
                if (file_exists($oname)) {
                    $this->l_unlink($oname);
                }
                if (file_exists($onameTmp)) {
                    unlink($onameTmp);
                }
                if (file_exists($fnameTmp)) {
                    unlink($fnameTmp);
                }
                return true;
            }
            return false;
        } else {

            $this->l_folder_exist($this->dirname . '1440/');
            $this->l_folder_exist($this->dirname . '1024/');
            $this->l_folder_exist($this->dirname . '500/');
            resizecompress($this->dirname . $oname, $this->dirname . '1440/' . $fname, 1440);
            resizecompress($this->dirname . $oname, $this->dirname . '1024/' . $fname, 1024);
            resizecompress($this->dirname . $oname, $this->dirname . '500/' . $fname, 500);
            return resizecompress($this->dirname . $oname, $this->dirname . $fname);
        }
    }
    

}

?>