<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 * Description of class
 *
 * @author kala
 */


require_once("imagelib.inc.php");
$GLOBALS['picture_typelist'] = array('restaurant', 'event', 'logo', 'chef', 'menu', 'promotion', 'sponsor', 'profile', 'foodselfie','category','dailyspecial');

class DSB_Media {

    private $id;
    private $name;
    private $object_type;
    private $object_id;
    private $restaurant;
    private $media_type;
    private $path;
    private $status;
    private $description;
    private $globalshow;
    private $dirname;
    private $smallPicSize = '500/';
    private $mediumPicSize = '1024/';
    private $largePicSize = '1440/';
    var $tmpdir;
    var $s3;
    var $bucket;
    var $msg;

    //var $oldimg;
    //var $logo;
    //var $chef;
    //var $rPictures;  // restaurant pictures
    //var $ePictures;  // event pictures
    //var $storedPictures;  // event pictures
    //var $msg;  // event pictures
    //var $s3;
    //var $bucket;
    //var $object_type;

    function __construct($theRestaurant = '', $object_id = '', $type = 'restaurant') {
        if (AWS) {
            $this->globalshow = __S3HOST__ . __S3DIR__;
        } else {
            $this->globalshow = __BASE_URL__ . __SHOWDIR__;
        }
        if (AWS) {
            $this->globalmedia = __S3HOST__;
        } else {
            $this->globalmedia = __MEDIADIR__;
        }
        if (AWS) {
            switch ($type) {
                case 'restaurant': $this->dirname = __S3DIR__ . "$theRestaurant/";
                    break;

                case 'user':
                    $unique_folder_name = md5($theRestaurant);
                    $this->object_id = $object_id;
                    $this->dirname = __S3DIRUSER__ . "$object_id/$unique_folder_name/";
                    break;
                 case 'dailyspecial': $this->dirname = __S3DIR__ . "$theRestaurant/";
                    break;
                 case 'category': $this->dirname = __S3DIR__ . "$theRestaurant/";
                    break;
            }
        } else {
            switch ($type) {
                case 'restaurant': $this->dirname = __UPLOADDIR__ . "$theRestaurant/";
                    break;
                case 'user':
                    // MD5 email
                    $unique_folder_name = md5($theRestaurant);
                    $this->object_id = $object_id;
                    $this->dirname = __UPLOADDIRUSER__ . "$object_id/$unique_folder_name/";
                    break;
                case 'dailyspecial': $this->dirname = __S3DIR__ . "$theRestaurant/";
                    break;
                case 'category': $this->dirname = __S3DIR__ . "$theRestaurant/";
                    break;
            }
        }
        
        $this->tmpdir = __TMPDIR__;
        $this->restaurant = $theRestaurant;
    }
    public function getName() {
        return $this->name;
    }

    public function getDescription() {
        return $this->description;
    }

    public function getPath() {
        return $this->path;
    }

    public function constructPath() {
        return $this->path;
    }

    public function getPartialPath() {
        return $this->globalshow . $this->restaurant . '/';
    }

    public function getFullPath($size = 'small', $file_name = '') {
        if (!empty($file_name)) {
            $this->name = $file_name;
        }
        $path = '';

       // if (AWS) {
            $random = rand(1, 5);
            switch ($random) {
                case 1:
                    $this->globalshow = __S3HOST1__ . __S3DIR__;
                    break;
                case 2:
                    $this->globalshow = __S3HOST2__ . __S3DIR__;
                    break;
                case 3:
                    $this->globalshow = __S3HOST3__ . __S3DIR__;
                    break;
                case 4:
                    $this->globalshow = __S3HOST4__ . __S3DIR__;
                    break;
                case 5:
                    $this->globalshow = __S3HOST5__ . __S3DIR__;
                    break;
                default :
                    $this->globalshow = __S3HOST__ . __S3DIR__;
                    break;
            }
      //  } else {
      //      $this->globalshow = 'http://localhost:8888' . __SHOWDIR__;
      //   }

        switch ($size) {
            case '100':
                $path = $this->globalshow . $this->restaurant . '/100/' . $this->name;
                break;
            case '140':
                $path = $this->globalshow . $this->restaurant . '/140/' . $this->name;
                break;
            case '180':
                $path = $this->globalshow . $this->restaurant . '/180/' . $this->name;
                break;
            case '270':
                $path = $this->globalshow . $this->restaurant . '/270/' . $this->name;
                break;
            case '300':
                $path = $this->globalshow . $this->restaurant . '/300/' . $this->name;
                break;
            case '325':
                $path = $this->globalshow . $this->restaurant . '/325/' . $this->name;
                break;
            case '360':
                $path = $this->globalshow . $this->restaurant . '/360/' . $this->name;
                break;
            case '450':
                $path = $this->globalshow . $this->restaurant . '/450/' . $this->name;
                break;
            case '500':
                $path = $this->globalshow . $this->restaurant . '/500/' . $this->name;
                break;
            case '600':
                $path = $this->globalshow . $this->restaurant . '/600/' . $this->name;
                break;
            case '700':
                $path = $this->globalshow . $this->restaurant . '/700/' . $this->name;
                break;
            case 'small':
                $path = $this->globalshow . $this->restaurant . '/' . $this->name;
                break;
            case 'medium':
                $path = $this->globalshow . $this->restaurant . '/' . $this->mediumPicSize . $this->name;
                break;
            case 'large':
                $path = $this->globalshow . $this->restaurant . '/' . $this->largePicSize . $this->name;
                break;
        }

        if ($path == '') {
            $path = $this->globalshow . $this->restaurant . '/' . $this->name;
        }
        return $path;
    }
    function uploadImage($restaurant, $category, $type = 'restaurant', $extra = NULL, $media_type = 'picture', $tag = '') {


        $log = 0;
        $msg = "Unable to Upload image. Try again";

        ini_set('memory_limit', '2048M');

                list($log, $msg) = $this->modifyRecord($category);

                if (intval($log) <= 0)
                    return array("-1", $msg);

                $filename = $msg;

                $Ar[0] = $this->addImg($restaurant, $filename, $category, $type, $extra, $media_type, $tag);
                $Ar[1] = $this->msg;

                return $Ar;
 
    }
    function addImg($target_object, $image, $category, $type, $extra_field, $media_type = 'picture', $tags = '') {
        global $picture_typelist;

        if ($image == "" || !in_array($category, $picture_typelist))
            return $this->retstatus(-1, "Invalid Category  or empty image");

        if ($this->l_file_exists($image) == false)
            return $this->retstatus(-1, "The file has not been uploaded " . $image);
        //kala added this line for user profile pic update
        if ($type != 'user') {
            if ($this->checkRestaurantMedia($target_object, $image) == true)
                return $this->retstatus(-1, "The image name already exists");
        }

        $sql = '';
        
        
        switch ($type) {
            case 'user':
                $key = $this->object_id . '/' . md5($target_object);
                //if user already have profile pic means update new pic else create new pic
                   
                if ($this->checkProfileMedia($target_object, $image) == false) {
                    $sql = "INSERT INTO dsb_media (object_type, restaurant, media_type, name, path, status) VALUES ('$category', '$target_object', '$media_type', '$image', '/upload/user/$key/','active') ";
                } else {
                    $type = "profile";
                    $sql = "UPDATE dsb_media SET path = '/upload/user/$key/',name='$image' WHERE object_type = '$type' && restaurant = '$target_object' && media_type = '$media_type'&& status='active'";
                }
                break;

            case 'category':
                    $sql = "INSERT INTO dsb_media (object_type, restaurant, media_type, name, path, tags, status) VALUES ('$category', '$target_object', '$media_type', '$image', '/upload/$category/$target_object/', '$tags', 'active') ";
                break;
            default:
                $sql = "INSERT INTO dsb_media (object_type, restaurant, media_type, name, path, tags, status) VALUES ('$category', '$target_object', '$media_type', '$image', '/upload/restaurant/$target_object/','$tags', 'active') ";
        }
        pdo_exec($sql,"aurora");
        return $this->retstatus(1, "The image '" . $image . "' has been uploaded ($category)");
    }
    function modifyRecord($category) {

        $file = $_FILES['file'];

        $allowedExts = array("gif", "jpeg", "jpg", "png", "pdf");
        $allowedTypes = array("image/gif", "image/jpeg", "image/jpg", "image/png", "application/pdf","binary/octet-stream");
        $extension = strtolower(pathinfo($_FILES["file"]["name"], PATHINFO_EXTENSION));


        if ((!in_array($file['type'], $allowedTypes)) || ($_FILES["file"]["size"] >= PHOTO_CONTEST_MAX_FILE_SIZE) || !in_array($extension, $allowedExts))
            return array("-1", "Invalid file " . $file['name'] . " type = " . $file['type'] . " size = " . $file['size'] . " MAXSIZE " . PHOTO_CONTEST_MAX_FILE_SIZE);

        $filename = str_replace(' ', '_', $file['name']);
        
        if($category == 'foodselfie' || $category == 'profile'){
            $tmp_ext = explode('.', $filename);
            $ext = '.'.$tmp_ext[count($tmp_ext)-1];
            $filename = uniqid().$ext;
        }
        //var_dump((!in_array($file['type'], $allowedTypes)) || ($_FILES["file"]["size"] >= PHOTO-CONTEST_MAX_FILE_SIZE) || !in_array($extension, $allowedExts));die;
        if ($file["error"] > 0)
            return array("-1", "Return Code: " . $file['error'] . "<br>");

        if ($category != 'profile') {
            if ($this->l_file_exists($filename))
                return array("-1", $filename . " already exists. ");
        }
        $tmp_image = "l_" . $filename;
       
        $this->l_move_uploaded_file($file['tmp_name'], $tmp_image, $file['type']);
        
        
        $scope = ($category != "sponsor") ? NULL : "sponsor";

        error_log("FILE TYPE " . $file['type']);

        if ($category != "sponsor" && $extension != "pdf") {
            $resData = $this->l_resizecompress($tmp_image, $filename);
        } else {
            error_log("FILE TYPE 1  " . $file['type']);
            $this->l_rename($tmp_image, $filename, $scope, NULL, $file['type']);
        }

        $cflg = "";

        if ($this->l_file_exists($filename)) {

            $this->l_unlink($tmp_image, 'tmp_image');
            $cflg = "(cr) ";
        } else {
              error_log("FILE TYPE 2  " . $file['type']);
            $this->l_rename($tmp_image, $filename, NULL, NULL, $file['type']);
        }
        
        return array("1", $filename);
    }
    function l_move_uploaded_file($name, $fname, $content_type = 'image/jpeg', $object_type = '') {
        if (AWS) {
            require_once 'conf/conf.s3.inc.php';
            if ($object_type == 'wheel') {
                $cache_value_browser = "1";
                $cache_value_cdn = "1";
            } else {
                $cache_value_browser = "1296000";
                $cache_value_cdn = "86400";
            }

            $this->getS3()->putObjectFile($name, $this->bucket, $this->dirname . $fname, S3::ACL_PUBLIC_READ, array("Cache-Control" => "max-age=$cache_value_browser, s-maxage=$cache_value_cdn", "Content-Type" => "$content_type"), array("Cache-Control" => "max-age=$cache_value_browser, s-maxage=$cache_value_cdn", "Content-Type" => "$content_type"));
            return unlink($name);
        } else {
            // move_uploaded_file only works in the context of file upload
            return rename($name, $this->dirname . $fname);
        }
    }

    function l_resizecompress($oname, $fname) {
        if (AWS) {
            require_once("conf/conf.s3.inc.php");


            $onameTmp = $this->tmpdir . $this->restaurant . $oname;
            $fnameTmp = $this->tmpdir . $this->restaurant . $fname;


            if (strpos($onameTmp, '/api/') !== false) {
                $onameTmp = str_replace('/api/', '/', $onameTmp);
            }
            if (strpos($fnameTmp, '/api/') !== false) {
                $fnameTmp = str_replace('/api/', '/', $fnameTmp);
            }
         
            if ($this->getS3()->getObject($this->bucket, $this->dirname . $oname, $onameTmp)) {
                        
                if (resizecompress($onameTmp, $fnameTmp, 1440)) {
                    $this->l_move_uploaded_file($fnameTmp, '1440/' . $fname);
                }
                if (resizecompress($onameTmp, $fnameTmp, 1024)) {
                      $this->l_move_uploaded_file($fnameTmp, '1024/' . $fname);
                }
                if (resizecompress($onameTmp, $fnameTmp, 500)) {
                      $this->l_move_uploaded_file($fnameTmp, '500/' . $fname);
                }
                if (resizecompress($onameTmp, $fnameTmp)) {
                   $this->l_move_uploaded_file($fnameTmp, $fname);
                }
                if (file_exists($oname)) {
                    $this->l_unlink($oname);
                }
                if (file_exists($onameTmp)) {
                    unlink($onameTmp);
                }
                if (file_exists($fnameTmp)) {
                    unlink($fnameTmp);
                }
                return true;
            }
            return false;
        } else {

            $this->l_folder_exist($this->dirname . '1440/');
            $this->l_folder_exist($this->dirname . '1024/');
            $this->l_folder_exist($this->dirname . '500/');
            resizecompress($this->dirname . $oname, $this->dirname . '1440/' . $fname, 1440);
            resizecompress($this->dirname . $oname, $this->dirname . '1024/' . $fname, 1024);
            resizecompress($this->dirname . $oname, $this->dirname . '500/' . $fname, 500);
            return resizecompress($this->dirname . $oname, $this->dirname . $fname);
        }
    }

    function l_resize($oname, $fname, $size) {
        if (AWS) {
            require_once("conf/conf.s3.inc.php");

            $onameTmp = $this->tmpdir . $this->restaurant . $oname;
            $fnameTmp = $this->tmpdir . $this->restaurant . $fname;

            if (strpos($onameTmp, '/api/') !== false) {
                $onameTmp = str_replace('/api/', '/', $onameTmp);
            }
            if (strpos($fnameTmp, '/api/') !== false) {
                $fnameTmp = str_replace('/api/', '/', $fnameTmp);
            }
            if ($this->getS3()->getObject($this->bucket, $this->dirname . $oname, $onameTmp)) {
                if (resizecompress($onameTmp, $fnameTmp, $size, true)) {
                    $this->l_move_uploaded_file($fnameTmp, $size . '/' . $fname);
                }
                if (file_exists($oname)) {
                    $this->l_unlink($oname);
                }
                if (file_exists($onameTmp)) {
                    unlink($onameTmp);
                }
                if (file_exists($fnameTmp)) {
                    unlink($fnameTmp);
                }
                return true;
            }
            return false;
        } else {
            $this->l_folder_exist($this->dirname . $size . '/');
            return resizecompress($this->dirname . $oname, $this->dirname . $size . '/' . $fname, $size);
        }
    }
    function l_rename($oname, $fname, $scope = NULL, $object_type = '', $file_type = "image/jpeg") {
        if (AWS) {
            require_once("conf/conf.s3.inc.php");
            if ($object_type == 'wheel') {
                $cache_value_browser = "1";
                $cache_value_cdn = "1";
            } else {
                $cache_value_browser = "1296000";
                $cache_value_cdn = "86400";
            }
              error_log("FILE TYPE  3  " .$file_type);
            if ($scope == NULL) {
                $this->getS3()->copyObject($this->bucket, $this->dirname . '1440/' . $oname, $this->bucket, $this->dirname . '1440/' . $fname, S3::ACL_PUBLIC_READ, array("Cache-Control" => "max-age=$cache_value_browser, s-maxage=$cache_value_cdn", "Content-Type" => $file_type), array("Cache-Control" => "max-age=$cache_value_browser, s-maxage=$cache_value_cdn", "Content-Type" => $file_type));
                //$this->l_unlink($oname);
                $this->getS3()->copyObject($this->bucket, $this->dirname . '1024/' . $oname, $this->bucket, $this->dirname . '1024/' . $fname, S3::ACL_PUBLIC_READ, array("Cache-Control" => "max-age=$cache_value_browser, s-maxage=$cache_value_cdn", "Content-Type" => $file_type), array("Cache-Control" => "max-age=$cache_value_browser, s-maxage=$cache_value_cdn", "Content-Type" => $file_type));
                //$this->l_unlink($oname);
                $this->getS3()->copyObject($this->bucket, $this->dirname . '500/' . $oname, $this->bucket, $this->dirname . '500/' . $fname, S3::ACL_PUBLIC_READ, array("Cache-Control" => "max-age=$cache_value_browser, s-maxage=$cache_value_cdn", "Content-Type" => $file_type), array("Cache-Control" => "max-age=$cache_value_browser, s-maxage=$cache_value_cdn", "Content-Type" => $file_type));
                $this->getS3()->copyObject($this->bucket, $this->dirname . '350/' . $oname, $this->bucket, $this->dirname . '350/' . $fname, S3::ACL_PUBLIC_READ, array("Cache-Control" => "max-age=$cache_value_browser, s-maxage=$cache_value_cdn", "Content-Type" => $file_type), array("Cache-Control" => "max-age=$cache_value_browser, s-maxage=$cache_value_cdn", "Content-Type" => $file_type));
            }
            $this->getS3()->copyObject($this->bucket, $this->dirname . $oname, $this->bucket, $this->dirname . $fname, S3::ACL_PUBLIC_READ, array("Cache-Control" => "max-age=$cache_value_browser, s-maxage=$cache_value_cdn", "Content-Type" => $file_type), array("Cache-Control" => "max-age=$cache_value_browser, s-maxage=$cache_value_cdn", "Content-Type" => $file_type));
            return $this->l_unlink($oname);
        } else {

            if ($scope == NULL) {
                rename($this->dirname . '1440/' . $oname, $this->dirname . '1440/' . $fname);
                rename($this->dirname . '1024/' . $oname, $this->dirname . '1024/' . $fname);
                rename($this->dirname . '500/' . $oname, $this->dirname . '500/' . $fname);
            }
            return rename($this->dirname . $oname, $this->dirname . $fname);
        }
    }
    function l_unlink($fname, $scope = NULL) {
        if (AWS) {
            require_once("conf/conf.s3.inc.php");
            if ($scope == NULL) {
                $this->getS3()->deleteObject($this->bucket, $this->dirname . '1440/' . $fname);
                $this->getS3()->deleteObject($this->bucket, $this->dirname . '1024/' . $fname);
                $this->getS3()->deleteObject($this->bucket, $this->dirname . '500/' . $fname);
            }
            return $this->getS3()->deleteObject($this->bucket, $this->dirname . $fname);
        } else {
            if ($scope == NULL) {
                unlink($this->dirname . '1440/' . $fname);
                unlink($this->dirname . '1024/' . $fname);
                unlink($this->dirname . '500/' . $fname);
            }
            return unlink($this->dirname . $fname);
        }
    }

    function l_file_exists($fname) {

        if (AWS) {
            require_once("conf/conf.s3.inc.php");
            return $this->getS3()->getObjectInfo($this->bucket, $this->dirname . $fname);
        } else {
            return file_exists($this->dirname . $fname);
        }
    }
     function l_folder_exist($path = NULL) {
        if (empty($path)) {
            $path = $this->dirname;
        };
        if (AWS) {
            return true;
        } else {
            $res = (file_exists($path) !== true);
            if ($res) {
                mkdir($path, 0777, true);
            }
        }
    }
    function getS3() {
           if (!isset($this->s3)) {
            $this->bucket = awsBucket;
            $this->s3 = new S3(awsAccessKey, awsSecretKey);
            //$this->s3->putBucket($this->bucket, S3::ACL_PUBLIC_READ);
        }
        return $this->s3;
    }
    function retstatus($log, $mess) {
        $this->msg = $mess;
        return $log;
    }
    
    
    
}