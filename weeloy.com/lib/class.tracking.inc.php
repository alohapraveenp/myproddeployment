<?php

class WY_tracking {

    public $ID; 
    public $campaign; 
    public $type;
    public $creation_date;
    public $campaign_token;
    public $redirect_to;
    public $lcount;
    public $short_link;
    
    public $restaurant_id;
   
    function __construct() {

    }

    public function getCampaign($token){
        $sql = 'SELECT ID, campaign, type, campaign_token, restaurant_id,lcount,short_link, redirect_to FROM marketing_campaigns WHERE campaign_token = :campaign_token';
        //$sth->bindParam(':campaign_token', $token, PDO::PARAM_STR, 48);s
        
        $db = getConnection();
        $stmt = $db->prepare($sql);
        $stmt->bindParam(':campaign_token', $token, PDO::PARAM_STR, 48);
        
        if($stmt->execute() && $campaign = $stmt->fetch(PDO::FETCH_BOTH)){
            $this->ID = $campaign['ID'];
            $this->campaign = $campaign['campaign'];
            $this->type = $campaign['type'];
            $this->campaign_token = $campaign['campaign_token'];
            $this->redirect_to = $campaign['redirect_to'];
            $this->restaurant_id = $campaign['restaurant_id'];
            $this->lcount = $campaign['lcount'];
            $this->short_link = $campaign['short_link'];
        }
    }
    public function getNumberRedirect($source){
        if($source == 'gourmand'){
            $source = 'w-gourmand';
        }
        if($source == 'marie-france'){
            $source = 'w-marie-france';
        }
        if($source == 'drurylane'){
            $source = 'w-marie-france';
        }
        $sql = "SELECT COUNT(id) as count FROM log_users WHERE action = 900 AND source = '$source'";
//       / return $sql;
        try {
        $data = pdo_single_select($sql, 'dwh');
        }catch (PDOException $e) {
            api_error($e, "error no data available");
        }
        return $data['count'];
    }
    public function updateClick($token,$newCount){
       if(isset($token)){
     
            $sql =  "UPDATE marketing_campaigns set lcount ='$newCount' where campaign_token ='$token' "; 
            $id= pdo_exec($sql);
            return 1;
       } 
        return 0;
    }
                
}