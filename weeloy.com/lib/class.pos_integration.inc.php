<?php

require_once('integrations/AGILYSYS/agilysys_xml_api.php');

class WY_POS_integration {

    private $pos = '';
    private $pos_object = NULL;
    private $restaurant;
    private $confirmation;
    private $table;
    private $new_table;
    private $pax;
    private $mode = 'test';
    var $result;
    var $msg = "";

    function __construct($booking) {
        $this->restaurant = $booking->restaurant;
        $this->confirmation = $booking->confirmation;
        $this->table = $booking->tablename;
        $this->new_table = $booking->restable;
        $this->pax = $booking->cover;
        $this->partner_booking_id = $booking->partner_booking_id;

        $this->getPOS($this->restaurant);
        
        if (WY_Restaurant::posEnable($this->restaurant) == 0) {	// $this->table != "TY04" || 
            $this->msg = "Invalid Table or testing not enabled " . $this->table . ' - ' . WY_Restaurant::posEnable($this->restaurant);
            return $this->result = -1;
        }

        switch ($this->pos) {
            case 'agilysys':
                $this->pos_object = new WY_Agilysys_xml_api($this->restaurant, $this->confirmation, $this->partner_booking_id, $this->table, $this->pax, $this->mode, $this->new_table);
                break;
            case 'revel':
                break;
            default:
                return $this->result = -1;
        }
        return $this->result = 1;
    }

    public function openOrder() {
        return $this->pos_object->openOrder();
    }

    public function closeOrder() {
        return $this->pos_object->closeOrder();
    }
    
    
    public function updateTable() {
        return $this->pos_object->updateTable();
    }

    public function saveOrderDetails() {
        return $this->pos_object->saveOrderDetails();
    }

    public function simulateItemsOrder() {
        return $this->pos_object->simulateItemsOrder();
    }

    public function getRemoteOrderDetails() {
        return $this->pos_object->getRemoteOrderDetails();
    }

    public function getOrderDetails() {
        return $this->pos_object->getOrderDetails();
    }

    public function getListOpenorder() {
        return $this->pos_object->getListOpenorder();
    }

    public function updateTableListOpenorder() {
        return $this->pos_object->updateTableListOpenorder();
    }
    
    public function getOrderSummary() {
        return $this->pos_object->getOrderSummary();
    }

    public function getOrderAmountDetails() {
        return $this->pos_object->getOrderAmountDetails();
    }

    public function getOrderAmount() {
        return $this->pos_object->getOrderAmount();
    }

    public function getOrderId() {
        return $this->pos_object->getOrderId();
    }

    public function updateFullMenu() {
        return $this->pos_object->updateFullMenu();
    }

    public function saveOpenCheckDetails() {
        return $this->pos_object->saveOpenCheckDetails();
    }
    
    private function getPOS($restaurant) {
        $this->pos = "";
		$allowtable = array("TY04", "TY15");
		$flgmode = ( strstr($_SERVER['HTTP_HOST'], 'www.weeloy.com') && in_array($this->table, $allowtable) );
        $whichmode = ($flgmode) ? "prod" : "test";

        switch ($restaurant) {            
            case 'SG_SG_R_Thanying':
	            WY_debug::recordDebug("DEBUG", "POS", "Restaurant " . $restaurant . ", whichmode =  " . $whichmode . ", table = " . $this->table);
				$this->pos = 'agilysys';
				if($flgmode) {
	                $this->mode = 'prod';
	                }
                break;

            case 'SG_SG_R_TheFunKitchen':
	            WY_debug::recordDebug("DEBUG", "POS", "Restaurant " . $restaurant . ", whichmode =  " . $whichmode . ", table = " . $this->table);
                $this->pos = 'agilysys';
                break;

            case 'SG_SG_R_MadisonRooms':
                $this->pos = 'revel';
                break;

            default:
                return $this->result = -1;
        }
        return $this->result = 1;
    }

    //    protected function __call($method, $arguments) {
//        if(empty($this->pos)){
//            $booking = $arguments[0];
//            $this->getPOS($booking->restaurant);
//        }
//    }
}
