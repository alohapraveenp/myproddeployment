<?php

class pages {

	public $current_page;
	public $pages;
	public $user_group;
	public $prefix='';
	private $template_dir;
	
	function __construct($pageName){
		global $Conf;
		
               
                
		$this->template_dir		= $Conf['TemplateDir'];
		$this->user_group 		= $_SESSION['user']['member_type'];
		$this->pages 			= $this->dump_pages();
		$this->current_page['name'] 	= $pageName;

            
                
		if ($this->current_page['name'] == "index" OR $this->current_page['name'] == null) 		{
			$this->current_page['name'] = "home";
		 	
		}elseif ($this->current_page['name'] == "admin_index" OR $this->current_page['name'] == null
			OR (	$this->current_page['name'] =='admin_home' && $this->user_group=='admin')) 	{
		
			$this->current_page['name'] = "admin_dashboard";
		}
		
		if(substr($this->current_page['name'],0,6) == 'admin_')	{
			$this->prefix	= 'admin_';	
                        
		}

		$this->get_page();
               
        if (!file_exists($this->template_dir.$this->current_page['content_twig'])) {

       // if (isset($_SESSION['timeout_idle']) && $_SESSION['timeout_idle'] < time()) {
//			header("Location:/logout.html");
 //       }			
			
			$this->current_page['name'] = 'template_error';
			$this->current_page['content_twig'] =  "template_error.twig";		
		}
                
          
		return true;
	}
	
	private function get_page($page=null){
	    
		if (empty($page)) {
			$page = $this->current_page['name'];
		}
	
		if (isset($page) && isset($this->pages[$page])) {

			if ($this->is_allowed()){
                        //if(true){
				$this->current_page 		= $this->pages[$page];	
				return true;
			} else {
                            
				if ($this->user_group == 'visitor' && $this->prefix == 'admin_' ) {
					$this->current_page	= $this->pages['admin_home'];
					
                                        return false;
					
				} else if($this->user_group == 'admin' && $this->prefix == 'admin_' ) {
					$this->current_page 	= $this->pages['admin_dashboard'];
					
                                        return false;
				
				}  else if($this->user_group == 'visitor' && $this->prefix != 'admin_' ) {
                                        
                                        header("Location:index.php");	
                                        //must be improve later
                                        $this->current_page 	= $this->pages['signup'];
                                        return false;
				} else {
                                        //must be improve later
                                        header("Location:index.php");
					$this->current_page 	= $this->pages['home'];
					return false;
				}

			}

		} else {
			
			$this->current_page 		= $this->pages['404'];
			$this->current_page['name'] 	= '404';
			header("Status: 404 Not Found");

			return false;
		}

	}

	private function is_allowed($page=null){
		if (empty($page)) {
			$page = $this->current_page['name'];
		}
		
		if (empty($this->pages[$page]['group'])) {
			return true;
		} else if (!empty($this->user_group) && ( $this->pages[$page]['group'] == $this->user_group ) ){	
			return true;
		} else if (!empty($this->user_group) && $this->pages[$page]['group'] == 'member' && $this->user_group !='visitor'){
			return true;
		} else {
			return false;
		}

	}

	function get_php(){
		$phpfilename = 'inc/'.$this->current_page['path'].'/'.$this->current_page['name'].'.inc.php';

		//if ($this->is_allowed() && file_exists($phpfilename)){
                if (file_exists($phpfilename)){
			return $phpfilename ;
		} else {
			return false;
		}
	}
	
	function add_subtemplate($sub_twig){
		//$sub_twig = $this->template_dir.$sub_twig;
                $sub_twig_long = $this->template_dir.$sub_twig;
                if (isset($sub_twig_long) && file_exists($sub_twig_long)) {
                     $this->current_page['sub_twig']= $sub_twig;
		} elseif(isset($sub_twig_long) && !file_exists($sub_twig_long)){
			$this->current_page['name'] = 'template_error';
                     $this->current_page['sub_twig']=  "template_error.twig";
		}
		return true;
	}
	
	function set_template($twig){
		
                $sub_twig_long = $this->template_dir.$twig;
                if (isset($sub_twig_long) && file_exists( $sub_twig_long)) {
                     $this->current_page['content_twig']= $twig;
		} elseif(isset($twig) && !file_exists($twig)){
			$this->current_page['name'] = 'template_error';
                     $this->current_page['content_twig']=  "template_error.twig";
		}
		return true;
	}
	
	function get_template_info(){
		return $this->current_page;
	}

	function dump_pages(){
		
            $sql = "SELECT * FROM page";
	    try {
                $db = getConnection();
                $stmt = $db->query($sql);
		if ($stmt->execute()) {
		    while($Data = $stmt->fetch(PDO::FETCH_OBJ)){
    
			    $aPages[$Data->name] = array (
							'id' 		=> $Data->id,
							'group' 	=> $Data->group,
							'name' 		=> $Data->name,
                                                        'path'          =>  $Data->path,
							'lang_files' 	=> explode(' ',$Data->lang_files),
							'content_twig' 	=>  $Data->path.'/'.$Data->content_twig,
			    			'title'		=> $Data->title,
			    			'desc'		=> $Data->desc
							);

		
		    }
		    if (isset($aPages))	{
			return $aPages;
		    }
		}
            }catch (PDOException $e) {
                echo '{"error":{"text":' . __FILE__." ".__FUNCTION__." ".__LINE__." ".$e->getMessage() . '}}';
            }
		
		
	    return false;
	}

	function set_breadcrumb($trs,$lang){
		$this->current_page['breadcrumb'][] = $this->current_page['name'];
		foreach($this->current_page['breadcrumb'] as $id =>$val){

			if (!empty($val) && isset( $trs['menu_'.$val] )){
				$this->current_page['breadcrumb'][$lang][$val] = $trs['menu_'.$val];
			}
		}
	}

	
	
	function register_sub_template($subtwig){
		
		$this->sub_template = $subtwig;
		
		return true;
	}


	function set_cache(){

	}



}

?>