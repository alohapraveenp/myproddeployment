<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
require_once ('lib/class.restaurant.inc.php');
require_once ('lib/class.payment.inc.php');
require_once ('lib/class.booking.inc.php');
require_once 'lib/composer/vendor/autoload.php';
require_once("conf/conf.init.inc.php");


class WY_Payment_Reddot {
    var $reference_id;
    var $amount;
    var $token;
    var $merchantId;
    var $transactionType;
    var $key;
    var $secret_key;
    var $redirect_url;
    var $back_url;
    var $notify_url;
    var $apiUrl;
    var $refundUrl;

    function __construct($reference_id, $amount, $token) {
        $this->reference_id = $reference_id;
        $this->amount = $amount;
        $this->token = $token;

        if ($this->is_test_env()) {
            $this->apiUrl = 'http://test.reddotpayment.com/connect-api/cgi-bin-live';
            $this->transactionType ='sale';
            $this->refundUrl = "http://test.reddotpayment.com/instanpanel/api/payment";
//            $this->merchantId = '1000089330';
//            $this->transactionType ='sale';
//            $this->key = '9560b02e6b27f41fc691e53b8bf6c47e0b81731f';
           
        }else{
            //$this->merchantId = '0000022175';
            $this->transactionType ='sale';
            //$this->key = 'B4eX2UPppBs48dq0gwJOBSIkCyALKtdIK4QenwZx'; 
            //$this->secret_key = '7bNxRWOZyjAivoSpyhlLwgW1jjDA2ak5REozLKnMBwK12grfHOXqyEXHIzLdqCvQHzKQyXZ6b1baMsFeUsos5vQ7sZ4EFaaXlOrFRsV9r2hIFe1VeasL1FfQoixPLBSN' ;
            $this->apiUrl = "https://connect.reddotpayment.com/merchant/cgi-bin-live";
            $this->refundUrl = "https://connect.reddotpayment.com/instanpanel/api/payment";
        }
//            $this->merchantId = '0000022175';
//            $this->transactionType ='sale';
//            $this->key = 'B4eX2UPppBs48dq0gwJOBSIkCyALKtdIK4QenwZx'; 
//            $this->secret_key = '7bNxRWOZyjAivoSpyhlLwgW1jjDA2ak5REozLKnMBwK12grfHOXqyEXHIzLdqCvQHzKQyXZ6b1baMsFeUsos5vQ7sZ4EFaaXlOrFRsV9r2hIFe1VeasL1FfQoixPLBSN' ;
//            $this->apiUrl = "https://connect.reddotpayment.com/merchant/cgi-bin-live";

    }
    public function getReddotcredential($restaurant){
        $res = new WY_restaurant();
        $credentials = $res->getRestaurantPaymentId($restaurant);
        if(isset($credentials)){
            $this->merchantId = $credentials['reddot_mid'];
            $this->key = $credentials['reddot_key'];
            $this->secret_key = $credentials['reddot_secret_key'];
        }
        
    }
    
    public function makeDeposit(){
        
        $booking = new WY_Booking();
        $payment = new WY_Payment();
        $res = new WY_restaurant();
        $booking->getBooking($this->reference_id);
        $this->getReddotcredential($booking->restaurant);
        $currency = $res->getCurrency($booking->restaurant);
        $payToken = md5(time());
        $this->notify_url = __BASE_URL__."/modules/booking/deposit/deposit_confirm_redirect.php?booking_deposit_id=$this->reference_id&token=$payToken&type=reddot&reaction=bkdepsoit"; 
        $this->back_url = __BASE_URL__."/modules/booking/book_form.php?bkrestaurant=".$booking->restaurant;
        $this->redirect_url = __BASE_URL__."/modules/booking/deposit/deposit_confirmation_confirmed.php"; 
        $paymentObj = $this->getReddotObject($this->amount,$currency,$this->reference_id,$booking->email,$booking->restaurant,$this->token);
        $paymentObj['status'] = 'success';
        $params = json_encode($paymentObj);
        $depDetails = $payment->geDepositById($this->reference_id,'booking_id');
        if(count($depDetails) > 0){
            $depDetails = $payment->updatePayment($this->reference_id,$this->amount);
        }
        if(count($depDetails) <= 0 ){
          $payment_id = $payment->saveDeposit($params,$this->amount, $booking->restaurant,$payToken,'','pending_payment','reddot', $this->reference_id,$currency);  
        }
        //$payment_id = $payment->saveDeposit($params,$this->amount, $booking->restaurant,$payToken,'','pending_payment','reddot', $this->reference_id,$currency);
        return $paymentObj ;
    }

    public function getReddotObject($amount,$currency,$reference_id,$email,$restaurant,$token){
        $requestArray = array(
                'action' => 'reddot',
                'amount' => $amount,
                'email' => $email,
                'order_number' => $reference_id,
                'merchant_id' => $this->merchantId,
                'currency_code' => $currency,
                'merchant_reference' =>'booking-'.$reference_id,
                'return_url' => $this->redirect_url,
                'transaction_type' => 'Sale',
                'key' => $this->key
        );
        $signature = $this->sign_payment_request($this->secret_key, $requestArray);
        $requestArray["signature" ] = $signature;
        $requestArray["reddot_url" ] = $this->apiUrl;
        return $requestArray;
        
    }

    function getReddotDepositObj($amount,$currency,$reference_id,$email,$restaurant,$booking_deposit_id){
        $requestArray = array(
                'response_type' =>'json',
                'action_type' => 'refund',
                'order_number' => $reference_id,
                'mid' => $this->merchantId,
                'amount' => $amount,
                'currency' => $currency,
                'transaction_id' =>$booking_deposit_id
        );
        $signature = $this->sign_payment_request($this->secret_key, $requestArray);
        $requestArray["signature" ] = $signature;
        return $requestArray;

    }
    function sign_payment_request($secret_key, $params) {
        
        $requestsString = '';
        $chosenFields = array_keys($params);
        sort($chosenFields);

        foreach ($chosenFields as $field) {
          if (isset($params[$field])) {
            $requestsString .= $field . '=' . ($params[$field]) .'&';
          }
        }

        $requestsString .= 'secret_key=' . $secret_key;
        $signature = md5($requestsString);

        return $signature;
    }

    public function makeRefund(){

        $booking = new WY_Booking();
        $payment = new WY_Payment();
        $res = new WY_restaurant();
        $booking->getBooking($this->reference_id);
        $this->getReddotcredential($booking->restaurant);
        $currency = $res->getCurrency($booking->restaurant);
        $paymentObj = $this->getReddotDepositObj($this->amount,$currency,$this->reference_id,$booking->email,$booking->restaurant,$booking->deposit_id);
        $result = $this->sendrefund($this->refundUrl,$paymentObj);
        $result = json_decode($result, true);
        if(isset($result)){
            if($result['result_status'] == 'accepted'){
              $status = 'REFUNDED';
              $payment->updateRefundDetails($status,$booking->deposit_id,$this->amount);
            }
        }
        return $result;

    }

    function sendrefund ($url, $data=array()) 
    {
             // Get cURL resource //
             $curl = curl_init();

             $postfields = substr((http_build_query($data) . "&"), 0, -1);

             // Set some options - we are passing in a user agent too here //
             curl_setopt_array ($curl, array(
             CURLOPT_RETURNTRANSFER => 1,
             CURLOPT_URL => $url,
             CURLOPT_POST => 1,
             CURLOPT_POSTFIELDS => $postfields,
             CURLOPT_SSL_VERIFYPEER => FALSE,
             CURLOPT_SSL_VERIFYHOST => FALSE
             ));

             // Send the request & save response to $resp //
             $resp = curl_exec($curl); //echo $url;
             // Close request to clear up some resources //
             curl_close($curl);
             return $resp;
    }
    function is_test_env() {

        if (strpos($_SERVER['HTTP_HOST'], 'api') !== false) {
            return false;
        }

        if (strpos($_SERVER['HTTP_HOST'], 'www') !== false) {
            return false;
        }

        if (strpos($_SERVER['HTTP_HOST'], 'prod') !== false) {
            return false;
        }
        if (strpos($_SERVER['HTTP_HOST'], 'codersvn') !== false) {
            return false;
        }

        return true;
    }
   
}


