<?php

require_once("lib/class.booking.inc.php");
require_once("lib/class.marketing_campaign.inc.php");
class WY_Event {

    use CleanIng;

    var $dirname;
    var $globalshow;
    var $restaurant;
    var $name;
    var $description;
    var $start;
    var $end;
    var $city;
    var $country;
    var $order;
    var $picture;
    var $timeline;
    var $eventInfo;
    var $result;
    var $msg;
    var $orderID;
    var $event_id;
    var $date;
    var $email;
    var $restaurantinfo;
    var $restaurant_title;
    var $restaurant_email;
    var $status;
    var $tnc;
    var $object_id;

    function __construct($theRestaurant = NULL) {
        $this->restaurant = $theRestaurant;
        $this->dirname = __UPLOADDIR__ . "$theRestaurant/";
        if (AWS) {
            $this->globalshow = __S3HOST__ . __S3DIR__;
        } else {
            $this->globalshow = __SHOWDIR__;
        }
    }

    private function retval($val, $msg) {
        $this->result = $val;
        $this->msg = $msg;
        return array($val, $msg);
    }

    private function getUniqueCode() {

        //        Why link to restaurant? 
        for ($i = 0; $i < 100; $i++) {
            $uniqueID = rand(10000000, 100000000 - 1);
            $data = pdo_single_select("SELECT eventID FROM event WHERE restaurant ='$this->restaurant' and eventID = '$uniqueID' limit 1");
            if (count($data) <= 0)
                return $uniqueID;
        }
        return false;
    }

    function insert($eventname) {

        $eventname = $this->clean_input($eventname);
        if ($eventname == "")
            return $this->retval(-1, "Event name is invalid -$eventname-");

        $theRestaurant = $this->restaurant;
        $uid = $this->getUniqueCode();
        if ($uid === false)
            return $this->retval(-1, "Unable to create a unique ID");

        $sql_check = "SELECT name FROM event where name = '$eventname' and restaurant = '$theRestaurant'  limit 1";
        $sql_ins = "INSERT INTO  event (name, restaurant, eventID) VALUES ('$eventname', '$theRestaurant', '$uid')";
        $this->result = pdo_insert_unique($sql_check, $sql_ins);
        return ($this->result == 0) ? $this->retval($uid, "Event $eventname has been created") : $this->retval(-1, "Event $eventname already exists. No new event created");
    }

    function insertEvent($obj) {
        $arg = array('eventname', 'title', 'morder', 'city', 'country', 'start', 'end', 'description', 'picture', 'featured', 'pdf_link', 'display', 'price', 'maxpax', 'tnc', 'type', 'menu', 'event_times', 'deposit', 'bookings','perbookingprice');
        for ($i = 0; $i < count($arg); $i++) {
            $tt = $arg[$i];
            $$tt = (isset($obj[$tt])) ? $this->clean_input($obj[$tt]) : "";
        }
        if ($eventname == "")
            return $this->retval(-1, "Event name is invalid -$eventname-");
        $uid = $this->getUniqueCode();
        if ($uid === false)
            return $this->retval(-1, "Unable to create a unique ID");
        $theRestaurant = $this->restaurant;
        if ($featured == '')
            $featured = 0;
        if ($perbookingprice == '')
            $perbookingprice = 0;
        $start = date('Y-m-d',strtotime($start));
        $end = date('Y-m-d',strtotime($end));
        $sql_check = "SELECT name FROM event where name = '$eventname' and restaurant = '$theRestaurant'  limit 1";
        $sql_ins = "INSERT INTO  event (name, restaurant, eventID, morder, title, city, country, start, end, description, picture, is_homepage, pdf_link, display, price, maxpax, tnc, type, menu, event_times, deposit, bookings,is_perbookingprice) VALUES ('$eventname', '$theRestaurant', '$uid', '$morder', '$title', '$city', '$country', '$start', '$end', '$description', '$picture', $featured, '$pdf_link', '$display', '$price', '$maxpax', '$tnc', '$type', '$menu', '$event_times', '$deposit', '$bookings','$perbookingprice')";
        $this->result = pdo_insert_unique($sql_check, $sql_ins);
        return ($this->result == 0) ? $this->retval($uid, "Event $eventname has been created") : $this->retval(-1, "Event $eventname already exists. No new event created");
    }

    function updateEvent($obj) {
        $arg = array('eventname', 'title', 'morder', 'city', 'country', 'start', 'end', 'description', 'picture', 'featured', 'pdf_link', 'display', 'price', 'maxpax', 'tnc', 'type', 'menu', 'event_times', 'deposit', 'bookings','perbookingprice');
        for ($i = 0; $i < count($arg); $i++) {
            $tt = $arg[$i];
            $$tt = (isset($obj[$tt])) ? $this->clean_input($obj[$tt]) : "";
        }
		
        $theRestaurant = $this->restaurant;
        $morder = intval(preg_replace("/[^0-9]/", "", $morder));

        if ($eventname == "")
            return $this->retval(-1, "Undefined Event Name");
        if ($featured == '')
            $featured = 0;
        if ($perbookingprice == '')
            $perbookingprice = 0;
        $affected_rows = pdo_exec("Update event set morder='$morder', title='$title', city='$city', country='$country', start='$start', end='$end', description='$description', picture='$picture', is_homepage=$featured, pdf_link='$pdf_link', display='$display', price='$price', maxpax='$maxpax', tnc='$tnc', type='$type', menu='$menu', event_times='$event_times', deposit='$deposit', bookings='$bookings',is_perbookingprice=$perbookingprice where name='$eventname' and restaurant = '$theRestaurant' limit 1");
        $this->result = 1;
        return $this->retval(1, "Event $eventname has been updated");
    }

    function delete($eventname) {
        $eventname = $this->clean_input($eventname);
        if ($eventname == "")
            return $this->retval(-1, "Event name is invalid -$eventname-");

        $theRestaurant = $this->restaurant;
        $affected_rows = pdo_exec("DELETE FROM event WHERE name = '$eventname' and restaurant = '$theRestaurant' LIMIT 1");
        return $this->retval(1, "Event $eventname has been deleted");
    }

    function readEventName() {
        $this->result = 1;
        $theRestaurant = $this->restaurant;
        return pdo_column("SELECT name FROM event where restaurant = '$theRestaurant' ORDER BY morder");
    }

    function readAllEvents() {
        $this->result = 1;
        $theRestaurant = $this->restaurant;
        return pdo_multiple_obj("SELECT ID, name, eventID, maxpax, restaurant, title, morder, city, country, start, end, description, picture, is_homepage, pdf_link, display, price, tnc, type,is_perbookingprice menu FROM event where restaurant = '$theRestaurant' ORDER BY morder");
    }

    function getEvents() {
        $this->result = 1;
        $theRestaurant = $this->restaurant;
        $cmds = array();
        $cmds["evbooking"] = array();
        $cmds["currency"] = WY_restaurant::getCurrency($this->restaurant);
        $cmds["evbookable"] = (WY_restaurant::s_checkeventbooking($this->restaurant) != 0) ? 1 : 0;
        $cmds["event"] = pdo_multiple_select("SELECT ID, eventID, name, maxpax, restaurant, title, morder, city, country, start, end, description, picture, is_homepage, pdf_link, display, price, tnc, type, menu, event_times, deposit, bookings,is_perbookingprice FROM event WHERE restaurant ='$theRestaurant' ORDER by morder");
        if ($cmds["evbookable"] == 1)
            $cmds["evbooking"] = pdo_multiple_select("SELECT event_id as eventID, orderID, pax as pax, amount, curency as currency, first_name as firstname, last_name as lastname, email, phone, status,paykey, object_id,created_at as cdate FROM event_booking WHERE restaurant ='$theRestaurant' order by created_at");
        return $cmds;
    }

    function getActiveEvents($privacy = NULL) {
        try {
            $where = '';
            if(!empty($privacy))
                $where .= " AND type = '$privacy' ";
            $this->result = 1;
            $theRestaurant = $this->restaurant;
            return !empty($theRestaurant) ? pdo_multiple_select("SELECT ID,eventID, name, restaurant, maxpax, title, morder, city, country, start, end, description, picture, is_homepage, pdf_link, display, event_times,deposit,bookings,price, tnc, type,is_perbookingprice, menu FROM event WHERE restaurant ='$theRestaurant' AND display <= CURDATE() AND end >= CURDATE() $where ORDER by morder")
            : pdo_multiple_select("SELECT ID,eventID, name, restaurant, maxpax, title, morder, city, country, start, end, description, picture, is_homepage, pdf_link, display, event_times,deposit,bookings,price, tnc, type, menu FROM event WHERE display <= CURDATE() AND end >= CURDATE() $where ORDER by morder");
        } catch (PDOException $e) {
            $this->result = 0;
            error_log(__FILE__." ".__FUNCTION__." ".__LINE__." PDOException: ".$e);
        } catch (Exception $e) {
            $this->result = 0;
            error_log(__FILE__." ".__FUNCTION__." ".__LINE__." Exception: ".$e);
        }
    }
    function getDetailedActiveEvents($privacy = NULL) {
        try {
            $where = '';
            if(!empty($privacy))
                $where .= " AND type = '$privacy' ";
            $this->result = 1;
            $theRestaurant = $this->restaurant;
            return !empty($theRestaurant) ? pdo_multiple_select("SELECT e.ID, eventID, name, e.restaurant, r.title as restaurant_name, r.address, r.tel, r.url, r.email, maxpax, e.title, morder, e.city, e.country, start, end, e.description, picture, is_homepage, pdf_link, display, price, e.tnc, type, menu FROM event e join restaurant r on e.restaurant=r.restaurant WHERE e.restaurant ='$theRestaurant' AND display <= CURDATE() AND end >= CURDATE() $where ORDER by morder")
            : pdo_multiple_select("SELECT e.ID, eventID, name, e.restaurant, r.title as restaurant_name, r.address, r.tel, r.url, r.email, maxpax, e.title, morder, e.city, e.country, start, end, e.description, picture, is_homepage, pdf_link, display, price, e.tnc, type, menu FROM event e join restaurant r on e.restaurant=r.restaurant WHERE display <= CURDATE() AND end >= CURDATE() $where ORDER by morder");
        } catch (PDOException $e) {
            $this->result = 0;
            error_log(__FILE__." ".__FUNCTION__." ".__LINE__." PDOException: ".$e);
        } catch (Exception $e) {
            $this->result = 0;
            error_log(__FILE__." ".__FUNCTION__." ".__LINE__." Exception: ".$e);
        }
    }
    function getEventBooking() {
        $this->result = 1;
        $theRestaurant = $this->restaurant;
        return pdo_multiple_select("SELECT event_id as eventID, orderID, pax as pax, amount, curency as currency, first_name as firstname, last_name as lastname, email, phone, status, created_at as cdate FROM event_booking WHERE restaurant ='$theRestaurant' order by created_at");
    }

    function getEventSearch($content) {

        $this->result = 1;
        $content = $this->clean_input($content);
        $type = (preg_match("/^ID=/", $content)) ? 'byid' : 'byreq';
        $content = preg_replace("/^[^=]+=/", "", $content);

        // by request
        if ($type == 'byreq') {
            $req = "%" . $content . "%";
            $query = "name like '$req' ";
            if($content === 'Father'){
                $query = "(name like '$req' OR  name like '%dad%')";
            }
            $sql = "SELECT e.*, r.title, r.region as area FROM event e, restaurant r WHERE $query and e.restaurant = r.restaurant AND display <= CURDATE() AND end >= CURDATE()";
        } else {
            $ids = preg_replace("/[^\d,]/", "", $content);
            $sql = "SELECT e.*, r.title, r.region as area FROM event e, restaurant r WHERE e.restaurant = r.restaurant AND e.eventID IN ($ids)";
        }
        $data = pdo_multiple_select($sql);
        if (count($data) <= 0) {
            error_log(__FILE__." ".__FUNCTION__." ".__LINE__." content: ".print_r($content, true)." sql: ".$sql." ".count($data)." records");
            $this->result = -1;
        }
        return $data;
    }
    function getEventLandingPageInformation($type,$tag ='') {
        
         $this->result = 1;
         $where ="type='$type'";
         if(!empty($tag)){
             $where ="type='$type' AND tag = '$tag'";
         }
        $data = pdo_single_select("SELECT * from home_categories where $where limit 1");
        if (count($data) <= 0)
            $this->result = -1;
        return $data;
    }
    function getComingEvents() {
        $this->result = 1;
        $theRestaurant = $this->restaurant;
        return pdo_multiple_select("SELECT eventID, name, maxpax, title, morder, city, country, start, end, description, picture, is_homepage, pdf_link, display, price, tnc, type, menu FROM event WHERE end >= CURDATE() AND restaurant ='$theRestaurant' ORDER by morder");
    }

    function getTimeline() {
        $this->result = 1;
        $theRestaurant = $this->restaurant;
        if ($theRestaurant != "")
            $this->Timeline = pdo_multiple_select("SELECT name, title, maxpax, morder, city, country, start, end, description, picture, is_homepage, pdf_link, display, restaurant, eventID, price, tnc, type, menu FROM event where restaurant = '$theRestaurant' and end > CURDATE() ORDER BY start");
        else
            $this->Timeline = pdo_multiple_select("SELECT name, title, maxpax, morder, city, country, start, end, description, picture, is_homepage, pdf_link, display, restaurant, eventID, price, tnc, type, menu FROM event where end > CURDATE() ORDER BY start");
    }

    function saveEventBooking($event_id, $no_pax, $amount, $curency, $time, $receiver, $paykey, $first_name, $last_name, $email, $phone, $company, $special_request, $status = 'pending', $order_id) {
        $bktracking ='';
         //if(isset(filter_input(INPUT_COOKIE, 'weelredir30', FILTER_SANITIZE_STRING))) {
        if(isset($_COOKIE['weelredir30'])){
            $campaign = new WY_MarketingCampaign();
            //$campaign_id = 'EDM1'; //need to extarct campaign id
            $token = $_COOKIE['weelredir30_bis'];
            $campaign_id = $campaign->getCampaignId($token, $email);
            $bktracking = (empty($bktracking)) ? "marketing" . "$campaign_id" : $bktracking . "|" . "marketing" . "$campaign_id";
        }
        $rdate = date('Y-m-d', strtotime($time));
        $rtime = date('H:i:s', strtotime($time));
        $booking = new WY_Booking();
        $booking->createBooking($receiver, $rdate, $rtime, $email, $phone, $no_pax, '', $first_name, $last_name, '', '', $special_request, 'event', $bktracking, '', $company, '', 'to come', '', '', $status, '', '', $browser = NULL);
        $object_id = $booking->confirmation;   
        $sql = "INSERT INTO  event_booking (restaurant,event_id, orderID,pax, amount, curency, time, receiver, paykey, object_id,first_name, last_name, email,phone, company, special_request,status) VALUES ('$receiver','$event_id', '$order_id','$no_pax', '$amount', '$curency', '$time', '', '$paykey','$object_id', '$first_name', '$last_name','$email', '$phone','$company','$special_request', '$status')";
        $result = pdo_insert($sql);

        return $result;
    }

    function readEventById($event_id) {
        $this->result = 1;
        return pdo_single_select("SELECT * FROM event where eventID = '$event_id' ORDER BY morder");
    }

    function getEventTicket($order_id) {
 
        $res = new WY_restaurant;
        $sql = "SELECT event_booking.id, event_booking.restaurant,event_booking.event_id, event_booking.object_id,event_booking.receiver,event_booking.orderID,event_booking.pax, event_booking.paykey,event_booking.amount, event_booking.curency, event_booking.time, event_booking.first_name, event_booking.last_name, event_booking.email, event_booking.phone, event_booking.company,event_booking.status, event_booking.special_request, event_booking.created_at, event_booking.updated_at,payment.payment_method FROM event_booking,payment WHERE event_booking.orderID = '{$order_id}' and event_booking.paykey=payment.payment_id limit 1";
        $data = pdo_single_select($sql);

        if (count($data) > 0) {
            
            $this->restaurant = $data['restaurant'];
            $this->orderID = $data['orderID'];
            $this->event_id = $data['event_id'];
            $this->date =  $data['time'];
            $tmp_date = strtotime($data['time']);
            $data['event_date'] =  date("F j, Y, g:i a" ,$tmp_date);
            $this->order = $data;
            $this->email = $data['email'];
            $this->object_id = $data['object_id'];

            $res->getRestaurant($data['restaurant']);
            $this->restaurantinfo = $res;
            $this->restaurant_email = $res->email;
            $this->restaurant_title = $res->title;
            $evndertails = $this->readEventById($data['event_id']);

            $this->name = $evndertails['name'];
            $this->description = $evndertails['description'];
            $this->tnc = $evndertails['tnc'];

            $this->status = $data['status'];
            
            
        }
       return $data;

        //$data['restaurantinfo'] =$res;
     
    }

    public function getUniqueCodeByEvent() {

        //        Why link to restaurant? 
        for ($i = 0; $i < 100; $i++) {
            $uniqueID = rand(10000000, 100000000 - 1);
            $data = pdo_single_select("SELECT orderID FROM event_booking WHERE receiver ='$this->restaurant' and orderID = '$uniqueID' limit 1");
            if (count($data) <= 0)
                return $uniqueID;
        }
        return false;
    }
    function getEvBookingDetails($confirmation){
        $this->result = 1;
        if(empty($confirmation))
            return 0;
        $data = pdo_single_select("SELECT tracking FROM booking where confirmation = '$confirmation'");
        if(count($data) >0)
           return $data['tracking'];
        else
           return 0;

    }
    
    

}

?>
