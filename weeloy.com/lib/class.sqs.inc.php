<?php

require_once 'lib/composer/vendor/autoload.php';

use Aws\Sqs\SqsClient;

class SQS {

    private $client = '';
    private $queue = '';
    private $queueUrl = '';
    private $body = '';
    private $attributes = [];

    public function __construct() {
        $this->client = SqsClient::factory(array(
                    'key' => awsAccessKey,
                    'secret' => awsSecretKey,
                    'region' => awsRegion,
                    'version' => '2012-11-05'
        ));
    }
    public function setQueue($queue) {
        $this->queue = $queue;
        $this->queueUrl=$this->getQueueUrl($queue);
    }
    
    public function getQueueUrl($queue){
        $result = $this->client->getQueueUrl(array(
            // QueueName is required
            'QueueName' => $queue
        ));
        $queueUrl = $result->get('QueueUrl');
        return $queueUrl;
        
    }
    
    public function setBody($body) {
        $this->body = $body;
    }

    public function setAttribute($name, $value, $type = 'String') {
        $this->attributes[$name] = array('StringValue' => $value,'DataType' => $type);
    }

    public function sendMessage() {
        if (!isset($this->body) || empty($this->body))
            trigger_error(__FILE__. " ".__FUNCTION__." ".__LINE__." empty message body!");
       $result = $this->client->sendMessage(array(
            'QueueUrl' => $this->queueUrl,
            'MessageBody' => $this->body,
            'MessageAttributes' => $this->attributes
        ));
      
    }
}
