<?php

require_once("lib/class.restaurant.inc.php");
class WY_EvOrder {

    use CleanIng;

    private $restaurant;
    private $eventID;
    private $type;
    private $cdate;
    private $rdate;
    private $rtime;
    private $total;
    private $price;
    private $currency;
    private $tax;
    private $access; // pickup or delivery
    private $email;
    private $phone;
    private $address;
    private $zip;
    private $country;
    private $status;
    private $idescription;
    private $unit_price;
    private $icurrency;
    
    var $msg;
    var $result;

    public function __construct($theRestaurant = NULL) {
        $this->restaurant = $theRestaurant;
    }

    private function error_msg($result, $msg) {

        $this->msg = $msg;
        return $this->result = $result;
    }

    private function getUniqueCode() {

        //        Why link to restaurant? 
        for ($i = 0; $i < 100; $i++) {
            $uniqueID = rand(10000000, 100000000 - 1);
            $data = pdo_single_select("SELECT eventID FROM event_management WHERE restaurant ='$this->restaurant' and eventID = '$uniqueID' limit 1");
            if (count($data) <= 0)
                return $uniqueID;
        }
        return false;
    }

    public function getOrders($email ='',$type ='single') {
        $this->msg = "";
        $this->result = 1;
        $res = new WY_restaurant;
        $resto = array();
        $qeryresto = " ev.restaurant = '$this->restaurant' and r.restaurant = ev.restaurant";	
        if ($type == "multiple") {
            $data = $res->getListAccountRestaurant($email);
           
            $cn = count($data);
            if ($cn <= 0) {
                $this->msg = "invalid account $email";
                return $this->result = -1;
            }
            if ($cn > 0 && $cn < 15) {
                foreach ($data as $row) {
                     $res->getRestaurant($row['restaurant']);
                    if($res->takeOutRestaurant()){
                        $resto[] = "'" . $row['restaurant'] . "'";
                        $restolist = implode(",", $resto);
                        $qeryresto = " ev.restaurant in ($restolist) and ev.restaurant  = r.restaurant";
                        $restaurant = $resto[0];
                    }
                }
            }
       }
            
        $data = pdo_multiple_select("SELECT ev.*,r.title as restitle FROM event_management ev,restaurant r WHERE $qeryresto ORDER BY rdate");
		if(count($data) < 1)
			$this->result = -1;
        return $data;
    }

    public function createOrder($cmds) {

        $orders = $cmds["orders"];
       
        $eventID = preg_replace("/[^a-zA-Z0-9]/", "", $orders["eventID"]);
        if (strlen($eventID) < 7)
            return $this->error_msg(-1, "invalid ID " . $eventID);

        $data = pdo_single_select("SELECT eventID FROM event_management WHERE restaurant ='$this->restaurant' and eventID = '$eventID' limit 1");
        if (count($data) > 0) {
            $eventID = $this->getUniqueCode();
            if ($eventID == false)
                return $this->error_msg(-1, "unable to create a unique ID ");
        }

        $content = array("status", "rdate", "rtime","occasion", "currency", "type", "company", "title", "firstname", "lastname", "email", "phone", "address", "zip","pax");
        for ($i = 0; $i < count($content); $i++) {
            $tt = $content[$i];
            $$tt = $this->clean_input($orders[$tt]);
        }

        $status = "";
        $now = (strstr($_SERVER['HTTP_HOST'], 'localhost:8888')) ? "NOW()" : "DATE_ADD(NOW(), INTERVAL 8 HOUR)";
        $sql = "INSERT INTO event_management (restaurant,eventID,cdate,rdate,rtime,occasion,currency,type,status,company,title,firstname,lastname,email,phone,address,zip,pax) VALUES ("
                . "'$this->restaurant','$eventID',$now,'$rdate','$rtime','$occasion','$currency','$type','$status','$company','$title','$firstname','$lastname','$email','$phone','$address','$zip','$pax')";

        pdo_exec($sql);
        
        $this->msg = $eventID;
        return $this->result = 1;
    }

    public function updateOrders($cmds) {

        $this->msg = "";
        $orders = $cmds["orders"];

        $eventID = preg_replace("/[^a-zA-Z0-9]/", "", $orders["eventID"]);
        if (strlen($eventID) < 7)
            return $this->error_msg(-1, "invalid ID " . $eventID);
        
        $data = pdo_single_select("SELECT eventID FROM event_management WHERE restaurant ='$this->restaurant' and eventID = '$eventID' limit 1");
        if (count($data) <= 0)
            return $this->error_msg(-1, "ID does not exists " . $eventID);
        $field = array("setup", "tnc", "tnc_cancel", "billing", "milestones", "payment_method","more" );
        	foreach($field as $label){
 		    if(isset($cmds[$label])) {
		    	$val = $this->clean_input($cmds[$label]);
        		pdo_exec("update event_management set $label='$val' where restaurant='$this->restaurant' and eventID='$eventID' limit 1");
                    }
                }

        $content = array("status", "rdate","rtime", "occasion", "currency", "type", "company", "title", "firstname", "lastname", "email", "phone", "address", "zip","pax");
        for ($i = 0; $i < count($content); $i++) {
            $tt = $content[$i];
            $$tt = $this->clean_input($orders[$tt]);
        }

        $rdate = date('Y-m-d',strtotime($rdate));
 
        pdo_exec("update event_management set status='$status', rdate='$rdate',rtime='$rtime', occasion='$occasion', currency='$currency', type='$type', company='$company', title='$title', firstname='$firstname', lastname='$lastname', email='$email', phone='$phone', address='$address', zip='$zip',pax='$pax' where restaurant='$this->restaurant' and eventID='$eventID' limit 1");

        return $this->result = 1;
    }


    public function updateOrdersMenus($cmds) {

        $this->msg = "";
        
        $eventID = $this->clean_input($cmds["eventID"]);
        $menus = $cmds["menus"];
        //$obj = json_decode($menus);
        $eventID = preg_replace("/[^a-zA-Z0-9]/", "", $eventID);

        if (strlen($eventID) < 7)
            return $this->error_msg(-1, "invalid ID " . $eventID);

        $data = pdo_single_select("SELECT eventID FROM event_management WHERE restaurant ='$this->restaurant' and eventID = '$eventID' limit 1");
        if (count($data) <= 0)
            return $this->error_msg(-1, "ID does not exists " . $eventID);

        $content = array("restaurant", "eventID", "menuID", "menuItemID", "displayID", "morder", "typeflag", "notes", "status");
       	foreach($menus as $row) {
			for ($i = 0; $i < count($content); $i++) {
 				$tt = $content[$i];
				$$tt = $this->clean_input($row[$tt]);
				}
			$data = pdo_single_select("SELECT eventID, status FROM event_menu WHERE restaurant ='$this->restaurant' and eventID='$eventID' and menuID='$menuID' and menuItemID='$menuItemID' limit 1");
			if (count($data) > 0) {
				if($status == "deleted") {
					if($displayID == "") // this is category, delete subitems
						$data = pdo_exec("update event_menu set status='deleted' WHERE restaurant ='$this->restaurant' and eventID='$eventID' and displayID='$menuID'");
					$data = pdo_exec("update event_menu set status='deleted' WHERE restaurant ='$this->restaurant' and eventID='$eventID' and menuID='$menuID' and menuItemID='$menuItemID' limit 1");
					}
				else if($data["status"] != "deleted") pdo_exec("update event_menu set status='$status', displayID='$displayID', morder='$morder', typeflag = '$typeflag', notes='$notes' where restaurant ='$this->restaurant' and eventID='$eventID' and menuID='$menuID' and menuItemID='$menuItemID' limit 1");
				}
			else if($status != "deleted") 
				pdo_exec("INSERT INTO event_menu (restaurant, eventID, menuID, menuItemID, displayID, morder, typeflag, notes, status) VALUES ('$restaurant', '$eventID', '$menuID', '$menuItemID', '$displayID', '$morder', '$typeflag', '$notes', '$status')" );
			}

        pdo_exec("DELETE FROM event_menu WHERE eventID = '$eventID' and restaurant ='$this->restaurant' and status ='deleted'");
        return $this->result = 1;
    }

    public function readOrdersMenus($eventID) {
        $eventID = $this->clean_input($eventID);
        $data = pdo_multiple_select("SELECT eventID, menuID, menuItemID, displayID, morder, typeflag, notes, status FROM event_menu WHERE restaurant ='$this->restaurant' and eventID = '$eventID' ");
        $this->result = (count($data) > 0) ? 1 : -1;
        return $data;
		}
		

    public function deleteOrders($eventID) {
        
        $eventID = $this->clean_input($eventID);

        pdo_exec("DELETE FROM event_management WHERE eventID = '$eventID' and restaurant ='$this->restaurant' limit 1");
        pdo_exec("DELETE FROM event_menu WHERE eventID = '$eventID' and restaurant ='$this->restaurant' limit 1");

        $this->msg = "";
        return $this->result = 1;
    }
    
   public function updateOrderStatus($eventID,$status){
       $now = (strstr($_SERVER['HTTP_HOST'], 'localhost:8888')) ? "NOW()" : "DATE_ADD(NOW(), INTERVAL 8 HOUR)";
      
       $data = pdo_exec("update event_management set status='$status' WHERE restaurant ='$this->restaurant' and eventID='$eventID'");
       $this->msg = "";
       return $this->result = 1;
   }
   
   public function updateEventOrder($payment_id,$status,$object_id){
        $ev_status = pdo_exec("update event_booking  set status='$status' where paykey ='$payment_id' limit 1");
        $ev_booking = pdo_exec("update booking  set status='$status' where confirmation ='$object_id' limit 1");
        return 1;
    }
        
}

?>
