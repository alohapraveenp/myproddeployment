<?php

require_once 'lib/composer/vendor/autoload.php';
require_once 'lib/class.payment.inc.php';
require_once 'lib/class.restaurant.inc.php';
require_once("lib/class.analytics.inc.php");
require_once("conf/conf.session.inc.php");
require_once("lib/class.notification.inc.php");
require_once("lib/class.event.inc.php");
require_once("lib/class.event_management.inc.php");

/**
 * Description of class
 *
 * @author philippe benedetti
 */
class WY_Payment_Stripe extends WY_Payment {

    var $secret_key;
    var $user_id;
    var $restaurant;

    public function stripeCredentials($restaurant) {
        $res = new WY_restaurant();
        $credentials = $res->getRestaurantPaymentId($restaurant);

        $stripe = array(
            "secret_key" => $credentials['stripe_secret_key'],
            "publishable_key" => $credentials['stripe_public_key'],
        );
        $this->secret_key = $stripe['secret_key'];
        $this->restaurant = $restaurant;

        return $stripe;
    }

    public function createStripeCustomer($confirmation, $restaurant, $email, $token, $type = 'website', $mode = 'booking') {
        $booking = new WY_Booking();
        $res = new WY_restaurant();
        $logger = new WY_log('website');
        $returnArr = array();

        $loguserid = (isset($_SESSION['user']['user_id'])) ? $_SESSION['user']['user_id'] : "0";
        $ref_id = '';

        if ($mode === 'booking') {
            $booking->getBooking($confirmation);
            $ref_id = $booking->bookid;
        }

        $getRest = $res->getRestaurant($restaurant);
//        $credentials = $res->getRestaurantPaymentId($restaurant);
//
//           $stripe = array(
//                "secret_key" => $credentials['stripe_secret_key'],
//                "publishable_key" => $credentials['stripe_public_key'],
//            );
//           
//        \Stripe\Stripe::setApiKey($stripe['secret_key']);
        $this->stripeCredentials($restaurant);
        \Stripe\Stripe::setApiKey($this->secret_key);

        $params = array(
            'email' => $email,
            'source' => trim($token)
        );
        try {

            $resObject = \Stripe\Customer::create($params);
            $returnArr = array('result' => 1, 'message' => 'success', 'id' => $resObject->id, 'card_id' => $resObject->default_source);
            $logger->LogEvent($loguserid, 1002, $res->ID, $ref_id, '', date("Y-m-d H:i:s"));
            $this->stripe_api_log($confirmation, $restaurant, $this->getHeader(), $params, $resObject, 200);

            return $returnArr;
        } catch (\Stripe\Error\Card $e) {
            // Since it's a decline, \Stripe\Error\Card will be caught

            $body = $e->getJsonBody();
            $err = $body['error'];
            $this->stripe_api_log($confirmation, $restaurant, $this->getHeader(), $params, $e, $e->getHttpStatus());
            $resultArr['result'] = 0;
            $resultArr['message'] = $err['message'];
        } catch (\Stripe\Error\RateLimit $e) {
            // Too many requests made to the API too quickly
            $body = $e->getJsonBody();
            $err = $body['error'];
            $this->stripe_api_log($confirmation, $restaurant, $this->getHeader(), $params, $e, $e->getHttpStatus());
            $resultArr['result'] = 0;
            $resultArr['message'] = $err['message'];
        } catch (\Stripe\Error\InvalidRequest $e) {

            $body = $e->getJsonBody();
            $err = $body['error'];
            $this->stripe_api_log($confirmation, $restaurant, $this->getHeader(), $params, $e, $e->getHttpStatus());
            $resultArr['result'] = 0;
            $resultArr['message'] = $err['message'];
        } catch (\Stripe\Error\Authentication $e) {

            $body = $e->getJsonBody();
            $err = $body['error'];
            $this->stripe_api_log($confirmation, $restaurant, $this->getHeader(), $params, $e, $e->getHttpStatus());
            $resultArr['result'] = 0;
            $resultArr['message'] = $err['message'];
        } catch (\Stripe\Error\ApiConnection $e) {
            // Network communication with Stripe failed
            $body = $e->getJsonBody();
            $err = $body['error'];
            $this->stripe_api_log($confirmation, $restaurant, $this->getHeader(), $params, $e, $e->getHttpStatus());
            $resultArr['result'] = 0;
            $resultArr['message'] = $err['message'];
        } catch (\Stripe\Error\Base $e) {
            // Display a very generic error to the user, and maybe send
            // yourself an email
            $body = $e->getJsonBody();
            $err = $body['error'];
            $this->stripe_api_log($confirmation, $restaurant, $this->getHeader(), $params, $e, $e->getHttpStatus());
            $resultArr['result'] = 0;
            $resultArr['message'] = $err['message'];
        } catch (Exception $e) {

            $body = $e->getJsonBody();
            $err = $body['error'];
            $this->stripe_api_log($confirmation, $restaurant, $this->getHeader(), $params, $e, $e->getHttpStatus());
            $resultArr['result'] = 0;
            $resultArr['message'] = $err['message'];
        }
        $logger->LogEvent($loguserid, 1003, $res->ID, $ref_id, '', date("Y-m-d H:i:s"));

        return $returnArr;


//stripe  customer response store in weeloy db
        if (isset($resObject->id)) {

            $arg = array(
                'object_id',
                'email',
                'stripe_id',
                'card_id',
                'object',
                'address_city',
                'address_country',
                'address_line1',
                'address_line2',
                'address_state',
                'address_zip',
                'address_zip_check',
                'brand',
                'country',
                'cvc_check',
                'dynamic_last4',
                'exp_month',
                'exp_year',
                'fingerprint',
                'funding',
                'last4',
                'name',
                'tokenization_method',
                'created',
                'livemode',
                'account_balance',
                'business_vat_id');
            $response = array(
                $confirmation,
                $resObject->email,
                $resObject->id,
                $resObject->sources->data[0]->id,
                $resObject->object,
                $resObject->sources->data[0]->address_city,
                $resObject->sources->data[0]->address_country,
                $resObject->sources->data[0]->address_line1,
                $resObject->sources->data[0]->address_line2,
                $resObject->sources->data[0]->address_state,
                $resObject->sources->data[0]->address_zip,
                $resObject->sources->data[0]->address_zip_check,
                $resObject->sources->data[0]->brand,
                $resObject->sources->data[0]->country,
                $resObject->sources->data[0]->cvc_check,
                $resObject->sources->data[0]->dynamic_last4,
                $resObject->sources->data[0]->exp_month,
                $resObject->sources->data[0]->exp_year,
                $resObject->sources->data[0]->fingerprint,
                $resObject->sources->data[0]->funding,
                $resObject->sources->data[0]->last4,
                $resObject->sources->data[0]->name,
                $resObject->sources->data[0]->tokenization_method,
                $resObject->created,
                $resObject->livemode,
                $resObject->account_balance,
                $resObject->business_vat_id);
            $this->savestriperesponse($arg, $response);
        }
    }

    private function saveCardDetails($amount, $paymentId, $confirmation, $restaurant, $payToken, $token, $email, $deposit_method) {

        $dataArr = array(
            'email' => $email,
            'source' => $token
        );
        $logger = new WY_log('website');
        $loguserid = (isset($_SESSION['user']['user_id'])) ? $_SESSION['user']['user_id'] : "0";

        $sql = pdo_exec("UPDATE booking SET booking_deposit_id = '$paymentId' WHERE confirmation = '$confirmation' ");
        //insert deposit details in deposit table
        $depsoit_id = $this->saveDeposit(json_encode($dataArr), $amount, $restaurant, $payToken, $paymentId, '', $deposit_method, $confirmation);
        return true;
    }

//    function stripeDepositPayment($restaurant, $confirmation, $email, $token, $amount, $title, $tracking, $city, $extCustomer, $fingerprint) {
//            //not using now
//        if ($extCustomer === true) {
//            $customerId = $this->checkFingerprint($email, $fingerprint);
//        }
//        if (empty($customerId)) {
//            $customer = $this->createStripeCustomer($confirmation, $restaurant, $email, $token);
//            if ($customer['result'] === 1) {
//                $customerId = $customer['id'];
//                $this->saveStripeFingerprint($email, $customerId, $fingerprint);
//            }
//        }
//
//        // PAYMENT
//        $currency = 'SGD';
//        $DepositAmount = $amount * 100;
//
//        $paymentArr = array(
//            "amount" => $DepositAmount,
//            "currency" => $currency,
//            "customer" => $customerId,
//            "description" => 'Booking : ' . $confirmation
//        );
//
//
//        $charge = $this->createStripePayment($customerId, $restaurant, $confirmation, $email, $amount, 'stripe');
//
//        if (isset($charge) && $charge['status'] === "succeeded") {
//            // update payment key in booking table
//
//            $sql = pdo_exec("UPDATE booking SET booking_deposit_id = '{$charge['payment_id']}' WHERE confirmation = '$confirmation' ");
//
////            //insert payment details in payment table
////            $this->savePayment($amount, 'booking_deposit', $customerId, $restaurant, $charge->id, 'stripe', 'payment_created', 0, 0, 'payment_created');
//            //insert deposit details in deposit table
//            $depsoit_id = $this->saveDeposit(json_encode($paymentArr), $amount, $restaurant, $payToken, $charge['payment_id'], '', 'stripe', $confirmation);
//
//            $returnUrl = $url . $depsoit_id . '&token=' . $payToken . '&paytype=stripe&status=COMPLETED';
//        } else {
//            //   $logger->LogEvent($_SESSION['user']['user_id'], 1000, 3, $_POST['confirmation'],'stripe-failed', date("Y-m-d H:i:s"));  
//            $returnUrl = $url . $depsoit_id . '&token=' . $payToken . '&paytype=stripe&actionType=cancel';
//        }
//
//        return $returnUrl;
//    }
//    function stripeSaveCustomer($restaurant, $confirmation, $email, $token, $amount, $title, $tracking, $city, $extCustomer, $fingerprint, $type = 'weeloy') {
//// not in use now
//        if ($extCustomer === true) {
//            $isSuccess = 1;
//            $customerId = $this->checkFingerprint($email, $fingerprint);
//        }
//
//        if (empty($customerId)) {
//            $isSuccess = 0;
//            $customer = $this->createStripeCustomer($confirmation, $restaurant, $email, $token);
//            $customerId = $customer['id'];
//            if ($customer['result'] === 1) {
//                $this->saveStripeFingerprint($email, $customerId, $fingerprint);
//                $isSuccess = 1;
//            }
//        }
//
//        $payToken = md5(time());
//
//        //customer details insert in booking_depsosit
//        if ($isSuccess === 1) {
//            if (!empty($customerId)) {
//                $isBooking = $this->checkValidBooking($confirmation);
//                if ($isBooking === 1) {
//                    $resultatSaveCardDetails = $this->saveCardDetails($amount, $customerId, $confirmation, $restaurant, $payToken, $token, $email, 'carddetails');
//                    
//                    if($resultatSaveCardDetails){
//                        //pdo_exec("update booking  set status ='' where confirmation ='$confirmation' limit 1");
//                        $notify = $this->updatePayStatus($confirmation, $customerId, 'COMPLETED', 'carddetails');
//                        if($notify){
//                            $booking = new WY_Booking();
//                            $booking->getBooking($confirmation);
//                            $notification = new WY_Notification();
//                            $notification->notify($booking, 'booking');
//                        }
//                        return true;
//                    }
//                }
//            }
//        }else{
//            $booking = new WY_Booking();
//            $booking->getBooking($confirmation);
//            $notification = new WY_Notification();
//            $notification->notify($booking, 'booking_pending');
//        }
//
//        return false;
//    }

    function stripeCreateCustomerMobile($restaurant, $confirmation, $email, $token, $amount, $type) {

        //get stripe credentials
        $this->stripeCredentials($restaurant);
        \Stripe\Stripe::setApiKey($this->secret_key);

        try {
            $tokenObj = \Stripe\Token::retrieve($token);
            $fingerprint = $tokenObj->card->fingerprint;

            $customer = $this->getCustomerId($email, $fingerprint, $confirmation, $restaurant, $token);
            $customerId = $customer['customer_id'];
            $cardId = $customer['card_id'];

            $result = $this->updatePaymentId($customerId, $confirmation);

            if ($result === 1) {
                $payToken = md5(time());
                $depsoit_id = $this->saveDeposit("", $amount, $restaurant, $payToken, $customerId, '', 'carddetails', $confirmation);
            }
            $this->stripe_api_log($confirmation, $restaurant, $this->getHeader(), $token, $tokenObj, 200);
            $resultArr['result'] = 1;
            $resultArr['message'] = 'success';
        } catch (\Stripe\Error\Card $e) {
            $body = $e->getJsonBody();
            $err = $body['error'];
            // Since it's a decline, \Stripe\Error\Card will be caught
            $this->stripe_api_log($confirmation, $restaurant, $this->getHeader(), $token, $e, $e->getHttpStatus());
            $resultArr['result'] = 0;
            $resultArr['message'] = $err['message'];
        } catch (\Stripe\Error\RateLimit $e) {
            // Too many requests made to the API too quickly
            $body = $e->getJsonBody();
            $err = $body['error'];
            $this->stripe_api_log($confirmation, $restaurant, $this->getHeader(), $token, $e, $e->getHttpStatus());
            $resultArr['result'] = 0;
            $resultArr['message'] = $err['message'];
        } catch (\Stripe\Error\InvalidRequest $e) {
            $body = $e->getJsonBody();
            $err = $body['error'];
            $this->stripe_api_log($confirmation, $restaurant, $this->getHeader(), $token, $e, $e->getHttpStatus());
            $resultArr['result'] = 0;
            $resultArr['message'] = $err['message'];
        } catch (\Stripe\Error\Authentication $e) {
            $body = $e->getJsonBody();
            $err = $body['error'];
            $this->stripe_api_log($confirmation, $restaurant, $this->getHeader(), $token, $e, $e->getHttpStatus());
            $resultArr['result'] = 0;
            $resultArr['message'] = $err['message'];
        } catch (\Stripe\Error\ApiConnection $e) {
            $body = $e->getJsonBody();
            $err = $body['error'];
            $this->stripe_api_log($confirmation, $restaurant, $this->getHeader(), $token, $e, $e->getHttpStatus());
            $resultArr['result'] = 0;
            $resultArr['message'] = $err['message'];
            // Network communication with Stripe failed
        } catch (\Stripe\Error\Base $e) {
            // Display a very generic error to the user, and maybe send
            // yourself an email
            $body = $e->getJsonBody();
            $err = $body['error'];
            $this->stripe_api_log($confirmation, $restaurant, $this->getHeader(), $err['param'], $e, $e->getHttpStatus());
            $resultArr['result'] = 0;
            $resultArr['message'] = $err['message'];
        } catch (Exception $e) {
            $body = $e->getJsonBody();
            $err = $body['error'];
            $this->stripe_api_log($confirmation, $restaurant, $this->getHeader(), $err['param'], $e, $e->getHttpStatus());
            $resultArr['result'] = 0;
            $resultArr['message'] = $err['message'];
        }
        return $resultArr;
    }

    function stripeMobilePaymentWithCustomerCreation($restaurant, $confirmation, $email, $token, $amount, $type) {
        $this->stripeCredentials($restaurant);
        \Stripe\Stripe::setApiKey($this->secret_key);

        try {
            $tokenObj = \Stripe\Token::retrieve($token);
            $fingerprint = $tokenObj->card->fingerprint;

            $customerId = $this->getCustomerId($email, $fingerprint, $confirmation, $restaurant, $token);

            $this->createStripePayment($customerId, $restaurant, $confirmation, $email, $amount);


            $resultArr['result'] = 1;
            $resultArr['message'] = 'success';
        } catch (\Stripe\Error\Card $e) {
            // Since it's a decline, \Stripe\Error\Card will be caught
            $body = $e->getJsonBody();
            $err = $body['error'];
            $this->stripe_api_log($confirmation, $restaurant, $this->getHeader(), $token, $e, $e->getHttpStatus());
            $resultArr['result'] = 0;
            $resultArr['message'] = $err['message'];
        } catch (\Stripe\Error\RateLimit $e) {
            // Too many requests made to the API too quickly
            $body = $e->getJsonBody();
            $err = $body['error'];
            $this->stripe_api_log($confirmation, $restaurant, $this->getHeader(), $token, $e, $e->getHttpStatus());
            $resultArr['result'] = 0;
            $resultArr['message'] = $err['message'];
        } catch (\Stripe\Error\InvalidRequest $e) {
            $body = $e->getJsonBody();
            $err = $body['error'];
            $this->stripe_api_log($confirmation, $restaurant, $this->getHeader(), $token, $e, $e->getHttpStatus());
            $resultArr['result'] = 0;
            $resultArr['message'] = $err['message'];
        } catch (\Stripe\Error\Authentication $e) {
            $body = $e->getJsonBody();
            $err = $body['error'];
            $this->stripe_api_log($confirmation, $restaurant, $this->getHeader(), $token, $e, $e->getHttpStatus());
            $resultArr['result'] = 0;
            $resultArr['message'] = $err['message'];
        } catch (\Stripe\Error\ApiConnection $e) {
            $body = $e->getJsonBody();
            $err = $body['error'];
            $this->stripe_api_log($confirmation, $restaurant, $this->getHeader(), $token, $e, $e->getHttpStatus());
            $resultArr['result'] = 0;
            $resultArr['message'] = $err['message'];
            // Network communication with Stripe failed
        } catch (\Stripe\Error\Base $e) {
            // Display a very generic error to the user, and maybe send
            // yourself an email
            $body = $e->getJsonBody();
            $err = $body['error'];
            $this->stripe_api_log($confirmation, $restaurant, $this->getHeader(), $token, $e, $e->getHttpStatus());
            $resultArr['result'] = 0;
            $resultArr['message'] = $err['message'];
        } catch (Exception $e) {

            $body = $e->getJsonBody();
            $err = $body['error'];
            $this->stripe_api_log($confirmation, $restaurant, $this->getHeader(), $token, $e, $e->getHttpStatus());
            $resultArr['result'] = 0;
            $resultArr['message'] = $err['message'];
        }
        return $resultArr;
    }

    function stripeDepositRefund($payment_id, $restaurant, $confirmation, $email, $token, $amount,$type = 'booking') {

        $booking = new WY_Booking();
        if($type == 'booking'){
            $booking->getBooking($confirmation);
            $id = $booking->bookid;
        }else{
            $id = $confirmation; 
        }
         $logger = new WY_log('backoffice');
        // NEED TO REFUND IN STRIPE
        $this->stripeCredentials($restaurant);

        \Stripe\Stripe::setApiKey($this->secret_key);
        $refundAmount = $amount * 100;

        $payment_id = trim($payment_id);
        $options = array('amount' => '$refundAmount');
        try {

            $refund = \Stripe\Refund::create(array(
                        "charge" => $payment_id,
                        "amount" => intval($refundAmount)
            ));

            $status = $refund->status;
            if ($status === 'succeeded') {
                $status = "REFUNDED";
            }
            $this->updateRefundDetails($status, $payment_id, $amount);
            $returnArr['status'] = $refund->status;
            $returnArr['message'] = 'success';

            $loguserid = (isset($_SESSION['user_backoffice']['id'])) ? $_SESSION['user_backoffice']['id'] : "unknown";
            $logger->LogEvent($loguserid, 714, $confirmation, $payment_id, $amount, date("Y-m-d H:i:s"));

            return $returnArr;
        } catch (\Stripe\Error\Card $e) {
            // Since it's a decline, \Stripe\Error\Card will be caught
            $body = $e->getJsonBody();
            $err = $body['error'];
            $returnArr['status'] = 'failed';
            $returnArr['message'] = $err['message'];

            $this->stripe_api_log($confirmation, $restaurant, $this->getHeader(), $paymentArr, $e, $e->getHttpStatus());
        } catch (\Stripe\Error\RateLimit $e) {
            // Too many requests made to the API too quickly
            $body = $e->getJsonBody();
            $err = $body['error'];
            $returnArr['status'] = 'failed';
            $returnArr['message'] = $err['message'];
            $this->stripe_api_log($confirmation, $restaurant, $this->getHeader(), $paymentArr, $e, $e->getHttpStatus());
        } catch (\Stripe\Error\InvalidRequest $e) {
            $body = $e->getJsonBody();
            $err = $body['error'];
            $returnArr['status'] = 'failed';
            $returnArr['message'] = $err['message'];
            $this->stripe_api_log($confirmation, $restaurant, $this->getHeader(), $paymentArr, $e, $e->getHttpStatus());
        } catch (\Stripe\Error\Authentication $e) {
            $body = $e->getJsonBody();
            $err = $body['error'];
            $returnArr['status'] = 'failed';
            $returnArr['message'] = $err['message'];
            $this->stripe_api_log($confirmation, $restaurant, $this->getHeader(), $paymentArr, $e, $e->getHttpStatus());
        } catch (\Stripe\Error\ApiConnection $e) {
            $body = $e->getJsonBody();
            $err = $body['error'];
            $returnArr['status'] = 'failed';
            $returnArr['message'] = $err['message'];
            $this->stripe_api_log($confirmation, $restaurant, $this->getHeader(), $paymentArr, $e, $e->getHttpStatus());

            // Network communication with Stripe failed
        } catch (\Stripe\Error\Base $e) {
            // Display a very generic error to the user, and maybe send
            // yourself an email
            $body = $e->getJsonBody();
            $err = $body['error'];
            $returnArr['status'] = 'failed';
            $returnArr['message'] = $err['message'];
            $this->stripe_api_log($confirmation, $restaurant, $this->getHeader(), $paymentArr, $e, $e->getHttpStatus());
        } catch (Exception $e) {
            $body = $e->getJsonBody();
            $err = $body['error'];
            $returnArr['status'] = 'failed';
            $returnArr['message'] = $err['message'];
            $this->stripe_api_log($confirmation, $restaurant, $this->getHeader(), $paymentArr, $e, $e->getHttpStatus());
        }
        $logger->LogEvent($loguserid, 1007, $id, '', '', date("Y-m-d H:i:s"));

        return $returnArr;
    }

    function updateStripeStatus($payment_id, $objStatus, $amount) {
        $bk_payment = pdo_exec("update payment  set amount ='$amount', object_status ='$objStatus'  where payment_id ='$payment_id' limit 1");
        $bk_deposit = pdo_exec("update booking_deposit  set amount ='$amount'  where paykey='$payment_id' limit 1");
        return 1;
    }

    public function createStripePayment($payment_id, $restaurant, $confirmation, $email, $amount, $source = 'website', $type = 'carddetails', $mode = 'Booking', $card_id = null) {
        $res = new WY_restaurant();
        $booking = new WY_Booking();
        $refid = '';
        $logger = new WY_log('website');
        if ($source == 'backoffice') {
            $loguserid = (isset($_SESSION['user_backoffice']['id'])) ? $_SESSION['user_backoffice']['id'] : "unknown";
            $action = 717;
        } else {
            $loguserid = (isset($_SESSION['user']['user_id'])) ? $_SESSION['user']['user_id'] : "0";
            $action = 706;
        };
        date_default_timezone_set("Asia/Singapore");
        pdo_settimezone();

        $this->stripeCredentials($restaurant);
        \Stripe\Stripe::setApiKey($this->secret_key);

        $res->getRestaurant($restaurant);

        if ($mode === 'Booking') {
            $booking->getBooking($confirmation);
            $refid = $confirmation;
            $ref_type = 'booking_deposit';
            $data = $this->getDepositDetails($payment_id, $confirmation);
            if (count($data) > 0) {
                $card_id = $data['card_id'];
            }
            //check if the booking is active or cancel
            if($booking->status =='cancel'){
                return  array('status' => 'failed', 'message' => 'Booking already cancelled', 'payment_id' => ''); 
            }
        } else {
            $ref_type = 'event_deposit';
        }

        $currency = 'SGD';
        $DepositAmount = $amount * 100;

        if ($card_id == null || empty($card_id)) {

            $paymentArr = array(
                "amount" => $DepositAmount,
                "currency" => $currency,
                "customer" => trim($payment_id),
                "description" => $mode . ': ' . $confirmation
            );
        } else {

            $paymentArr = array(
                "amount" => $DepositAmount,
                "currency" => $currency,
                "customer" => trim($payment_id),
                "card" => $card_id,
                "description" => $mode . ': ' . $confirmation
            );
        }


        //card_18eLaFDdoStEhOoYDWlvI68b
        $payment_status = 'failed';
        $returnArr = array('status' => 'failed', 'message' => '', 'payment_id' => '');

        try {
            $charge = \Stripe\Charge::create($paymentArr);
            $chargeId = $charge->id;
            $status = $charge->status;
            $resObject = $charge;
            $arg = array(
                'object_id',
                'email',
                'stripe_id',
                'card_id',
                'object',
                'address_city',
                'address_country',
                'address_line1',
                'address_line2',
                'address_state',
                'address_zip',
                'address_zip_check',
                'brand',
                'country',
                'cvc_check',
                'dynamic_last4',
                'exp_month',
                'exp_year',
                'fingerprint',
                'funding',
                'last4',
                'name',
                'tokenization_method',
                'created',
                'livemode',
                'account_balance',
                'business_vat_id',
                'paid',
                'refunded',
                'amount',
                'amount_refunded');
            //$response=array($confirmation,$email,$resObject->id,$resObject->source->id,$resObject->object,$resObject->source->address_city,$resObject->source->address_country,$resObject->source->address_line1,$resObject->source->address_line2,$resObject->source->address_state,$resObject->source->address_zip,$resObject->source->address_zip_check,$resObject->source->brand,$resObject->source->country,$resObject->source->cvc_check,$resObject->source->dynamic_last4,$resObject->source->funding,$resObject->source->last4,$resObject->source->name,$resObject->currency,$resObject->amount,$resObject->balance_transaction,$resObject->amount_refunded,$resObject->captured,$resObject->description,$resObject->destination,$resObject->dispute,$resObject->failure_code,$resObject->failure_message,$resObject->livemode,'',$resObject->invoice,$resObject->paid,$resObject->refunded,$resObject->amount,$resObject->amount_refunded);
            $response = array(
                $confirmation,
                $email,
                $resObject->id,
                $resObject->source->id,
                $resObject->object,
                $resObject->source->address_city,
                $resObject->source->address_country,
                $resObject->source->address_line1,
                $resObject->source->address_line2,
                $resObject->source->address_state,
                $resObject->source->address_zip,
                $resObject->source->address_zip_check,
                $resObject->source->brand,
                $resObject->source->country,
                $resObject->source->cvc_check,
                $resObject->source->dynamic_last4,
                $resObject->source->funding,
                $resObject->source->last4,
                $resObject->source->name,
                $resObject->currency,
                $resObject->amount,
                $resObject->balance_transaction,
                $resObject->amount_refunded,
                $resObject->captured,
                $resObject->description,
                $resObject->destination,
                $resObject->dispute,
                $resObject->failure_code,
                $resObject->failure_message,
                $resObject->livemode,
                $resObject->invoice);
            $this->savestriperesponse($arg, $response);
            if ($status === 'succeeded') {
                $status = "COMPLETED";
            }

            $payment_id = $this->savePayment($amount, $ref_type, $payment_id, $restaurant, $chargeId, $type, $status, 0, 0, 'payment_created');
            $payment_status = $charge->status;
            $returnArr['payment_id'] = $chargeId;
            $returnArr['status'] = $charge->status;

            $logger->LogEvent($loguserid, $action, $refid, $chargeId, $amount, date("Y-m-d H:i:s"));

            //init status booking_deposit  in payment table customer id
            //update PaymentId in booking Table after sucessFull payment creation
            $this->updatePaymentId($chargeId, $confirmation);

            $this->stripe_api_log($confirmation, $restaurant, $this->getHeader(), $paymentArr, $charge, 200);

            $returnArr['message'] = 'success';

            return $returnArr;

            //error_log("payemnt_creation" . print_r($charge, true));
        } catch (\Stripe\Error\Card $e) {
            // Since it's a decline, \Stripe\Error\Card will be caught
            $body = $e->getJsonBody();
            $err = $body['error'];
            $returnArr['status'] = 'failed';
            $returnArr['message'] = $err['message'];

            $this->stripe_api_log($confirmation, $restaurant, $this->getHeader(), $paymentArr, $e, $e->getHttpStatus());
        } catch (\Stripe\Error\RateLimit $e) {
            // Too many requests made to the API too quickly
            $body = $e->getJsonBody();
            $err = $body['error'];
            $returnArr['status'] = 'failed';
            $returnArr['message'] = $err['message'];
            $this->stripe_api_log($confirmation, $restaurant, $this->getHeader(), $paymentArr, $e, $e->getHttpStatus());
        } catch (\Stripe\Error\InvalidRequest $e) {
            $body = $e->getJsonBody();
            $err = $body['error'];
            $returnArr['status'] = 'failed';
            $returnArr['message'] = $err['message'];
            $this->stripe_api_log($confirmation, $restaurant, $this->getHeader(), $paymentArr, $e, $e->getHttpStatus());
        } catch (\Stripe\Error\Authentication $e) {
            $body = $e->getJsonBody();
            $err = $body['error'];
            $returnArr['status'] = 'failed';
            $returnArr['message'] = $err['message'];
            $this->stripe_api_log($confirmation, $restaurant, $this->getHeader(), $paymentArr, $e, $e->getHttpStatus());
        } catch (\Stripe\Error\ApiConnection $e) {
            $body = $e->getJsonBody();
            $err = $body['error'];
            $returnArr['status'] = 'failed';
            $returnArr['message'] = $err['message'];
            $this->stripe_api_log($confirmation, $restaurant, $this->getHeader(), $paymentArr, $e, $e->getHttpStatus());

            // Network communication with Stripe failed
        } catch (\Stripe\Error\Base $e) {
            // Display a very generic error to the user, and maybe send
            // yourself an email
            $body = $e->getJsonBody();
            $err = $body['error'];
            $returnArr['status'] = 'failed';
            $returnArr['message'] = $err['message'];
            $this->stripe_api_log($confirmation, $restaurant, $this->getHeader(), $paymentArr, $e, $e->getHttpStatus());
        }
        $logger->LogEvent($loguserid, 1005, $refid, '', '', date("Y-m-d H:i:s"));
        return $returnArr;
    }

//    public function createStripeToken($confirmation, $restaurant, $number, $exp_month, $exp_year, $cvc, $email, $name,$type='booking') {
//         //not in use now
//
//        $booking = new WY_Booking();
//        $res = new WY_restaurant();
//        $res->getRestaurant($restaurant);
//        $booking->getBooking($confirmation);
//        $logger = new WY_log("website");
//        $loguserid = (isset($_SESSION['user']['user_id'])) ? $_SESSION['user']['user_id'] : "0";
//        $isSuccess = false;
//        $isCustomer = false;
//        $this->stripeCredentials($restaurant);
//        \Stripe\Stripe::setApiKey($this->secret_key);
//        $tokenArr['token'] = null;
//        $tokenArr['customer_id'] = null;
//        $tokenArr['isCustomer'] = $isCustomer;
//        $tokenArr['fingerprint'] = null;
//        $tokenArr['isSuccess'] = $isSuccess;
//        try {
//            $params = array(
//                "card" => array(
//                    "number" => $number,
//                    "exp_month" => intval($exp_month),
//                    "exp_year" => intval($exp_year),
//                    "cvc" => $cvc,
//                    "name" => $name
//            ));
//            
//
//            $resObject = \Stripe\Token::create($params);
//            $isSuccess = true;
//            $logger->LogEvent($loguserid, 1000, $res->ID, $booking->bookid, '', date("Y-m-d H:i:s"));
//
//            /*             * ************************** */
//            $stripe_customer_id = "";
//            $fingerprint = $resObject->card->fingerprint;
//
//            if (strlen($params['card']['number']) === 16) {
//                $params['card']['number'] = substr_replace($params['card']['number'], 'XXXXXXXX', 4, 8);
//            } else {
//                $params['card']['number'] = substr_replace($params['card']['number'], 'XXXXXXXX', 4, 6);
//            }
//
//            $customer_id = $this->checkFingerprint($email, $fingerprint);
//
//            //token response insert in payment_stripe table
//            if (isset($resObject->card->fingerprint)) {
//                $stripe_customer_id = $customer_id;
//            }
//
//            if (!empty($customer_id)) {
//
//                try {
//                    $retrive = \Stripe\Customer::retrieve($customer_id);
//                    if ($retrive->id && !$retrive->deleted) {
//                        $isCustomer = true;
//                    }
//                } catch (\Stripe\Error\ApiConnection $e) {
//                    $this->stripe_api_log($confirmation, $restaurant, $this->getHeader(), $params, $e, $e->getHttpStatus());
//                    $logger->LogEvent($loguserid, 1001, $res->ID, $booking->bookid, '', date("Y-m-d H:i:s"));
//                } catch (\Stripe\Error\InvalidRequest $e) {
//                    $this->stripe_api_log($confirmation, $restaurant, $this->getHeader(), $params, $e, $e->getHttpStatus());
//                    $logger->LogEvent($loguserid, 1001, $res->ID, $booking->bookid, '', date("Y-m-d H:i:s"));
//                }
//            }
//
//            $tokenArr = array(
//                'token' => $resObject->id,
//                'customer_id' => $stripe_customer_id,
//                'isCustomer' => $isCustomer,
//                'fingerprint' => $fingerprint,
//                'isSuccess' => $isSuccess
//            );
//            
//            $arg = array(
//                'object_id', 
//                'email', 
//                'stripe_id', 
//                'card_id', 
//                'object', 
//                'address_city', 
//                'address_country', 
//                'address_line1', 
//                'address_line2', 
//                'address_state',
//                'address_zip', 
//                'address_zip_check', 
//                'brand', 
//                'country', 
//                'cvc_check', 
//                'dynamic_last4', 
//                'exp_month',
//                'exp_year',
//                'fingerprint', 
//                'funding', 
//                'last4', 
//                'name', 
//                'tokenization_method',
//                'client_ip', 
//                'created', 
//                'livemode', 
//                'type');
//            
//            $response = array(
//                $confirmation, 
//                $email, 
//                $resObject->id, 
//                $resObject->card->id,
//                $resObject->card->object, 
//                $resObject->card->address_city, 
//                $resObject->card->address_country, 
//                $resObject->card->address_line1,
//                $resObject->card->address_line2,
//                $resObject->card->address_state,
//                $resObject->card->address_zip,
//                $resObject->card->address_zip_check,
//                $resObject->card->brand, 
//                $resObject->card->country,
//                $resObject->card->cvc_check, 
//                $resObject->card->dynamic_last4, 
//                $resObject->card->exp_month, 
//                $resObject->card->exp_year, 
//                $resObject->card->fingerprint, 
//                $resObject->card->funding,
//                $resObject->card->last4, 
//                $resObject->card->name, 
//                $resObject->card->tokenization_method,
//                $resObject->client_ip, 
//                $resObject->created, 
//                $resObject->livemode, 
//                $resObject->type);
//            
//            $this->savestriperesponse($arg, $response);
//
//            $this->stripe_api_log($confirmation, $restaurant, $this->getHeader(), $params, $resObject, 200);
//            return $tokenArr;
//
//
//            /*             * ************************** */
//        } catch (\Stripe\Error\Card $e) {
//             $e_json = $e->getJsonBody();
//            $error = $e_json['error'];
//           
//            // Since it's a decline, \Stripe\Error\Card will be caught
//            $this->stripe_api_log($confirmation, $restaurant, $this->getHeader(), $params, $error, $e->getHttpStatus());
//           
//        } catch (\Stripe\Error\RateLimit $e) {
//            // Too many requests made to the API too quickly
//
//            $this->stripe_api_log($confirmation, $restaurant, $this->getHeader(), $params, $e, $e->getHttpStatus());
//            //$logger->LogEvent($loguserid, 1001, $booking->bookid, '', '', date("Y-m-d H:i:s"));
//        } catch (\Stripe\Error\InvalidRequest $e) {
//
//            $this->stripe_api_log($confirmation, $restaurant, $this->getHeader(), $params, $e, $e->getHttpStatus());
//            //$logger->LogEvent($loguserid, 1001, $booking->bookid, '', '', date("Y-m-d H:i:s"));
//        } catch (\Stripe\Error\Authentication $e) {
//
//            $this->stripe_api_log($confirmation, $restaurant, $this->getHeader(), $params, $e, $e->getHttpStatus());
//            //$logger->LogEvent($loguserid, 1001, $booking->bookid, '', '', date("Y-m-d H:i:s"));
//        } catch (\Stripe\Error\ApiConnection $e) {
//            $this->stripe_api_log($confirmation, $restaurant, $this->getHeader(), $params, $e, $e->getHttpStatus());
//            //$logger->LogEvent($loguserid, 1001, $booking->bookid, '', '', date("Y-m-d H:i:s"));
//            // Network communication with Stripe failed
//        } catch (\Stripe\Error\Base $e) {
//            // Display a very generic error to the user, and maybe send
//            // yourself an email
//            $this->stripe_api_log($confirmation, $restaurant, $this->getHeader(), $params, $e, $e->getHttpStatus());
//            //$logger->LogEvent($loguserid, 1001, $booking->bookid, '', '', date("Y-m-d H:i:s"));
//        } catch (Exception $e) {
//            $this->stripe_api_log($confirmation, $restaurant, $this->getHeader(), $params, $e, $e->getHttpStatus());
//            //$logger->LogEvent($loguserid, 1001, $booking->bookid, '', '', date("Y-m-d H:i:s"));
//        }
//        $logger->LogEvent($loguserid, 1001, $res->ID, $booking->bookid, '', date("Y-m-d H:i:s"));
//        
//        if($isSuccess == false){
//            $booking = new WY_Booking();
//            $booking->getBooking($confirmation);
//            $notification = new WY_Notification();
//            $notification->notify($booking, 'booking_pending');
//        }
//
//        return $tokenArr;
//    }

    function saveStripeFingerprint($email, $customerId, $fingerprint) {
        $data = pdo_single_select("SELECT * from payment_stripe_customer_details where email = '$email' AND fingerprint='$fingerprint' AND restaurant= '$this->restaurant' limit 1 ");

        if (count($data) > 0) {
            $result = $data;
            pdo_exec("update payment_stripe_customer_details  set customer_id='$customerId' where email = '$email' AND fingerprint='$fingerprint' AND restaurant= '$this->restaurant' limit 1 ");
        } else {
            $sql = "INSERT INTO payment_stripe_customer_details (email,customer_id, fingerprint,restaurant) VALUES ('$email','$customerId', '$fingerprint','$this->restaurant')";
            $result = pdo_insert($sql);
        }
        return $result;
    }

    function checkFingerprint($email, $fingerprint) {
        $customer_id = "";
        //$data = pdo_single_select("SELECT * from payment_stripe_customer_details where email = '$email' AND fingerprint='$fingerprint' limit 1 ");
        $data = pdo_single_select("SELECT * from payment_stripe_customer_details where email = '$email' AND restaurant= '$this->restaurant' limit 1 ");
        if (count($data) > 0) {
            return $customer_id = $data['customer_id'];
        }
        return $customer_id;
    }

    function checkifcardExist($email, $fingerprint, $customer_id) {
        $cardfinger = "";
        if (empty($fingerprint)) {
            return $cardfinger;
        }
        $data = pdo_single_select("SELECT * from payment_stripe_customer_details where email = '$email' AND fingerprint='$fingerprint' AND customer_id='$customer_id'  AND restaurant= '$this->restaurant' limit 1 ");
        if (count($data) > 0) {
            return $cardfinger = $data['fingerprint'];
        }
        return $cardfinger;
    }

    function savestriperesponse($arg, $in_array) {

        $in_values = '"' . implode('","', $in_array) . '"';
        $in_fields = implode(",", $arg);

        $sql = "INSERT INTO payment_stripe($in_fields) values ($in_values)";

        $result = pdo_insert($sql);
        return $result;
    }

    public function getStripeCardInfoByEmail($email) {
        $data = pdo_multiple_select("SELECT card_id,last4 from payment_stripe where email = '$email' ");
        $card = array();
        foreach ($data as $d) {
            array_push($card, array(
                'last4' => $d['last4'],
                'card_id' => $d['card_id']
            ));
        }
        array_unique($card, SORT_REGULAR);

        return $card;
    }

    function getCustomerId($email, $fingerprint, $confirmation, $restaurant, $token, $mode = 'booking') {
        $this->stripeCredentials($restaurant);
        $customerId = $this->checkFingerprint($email, $fingerprint);
        //$customerId ='';
        $isCustomer = false;
        $customerArr = array();
        \Stripe\Stripe::setApiKey($this->secret_key);

        if (!empty($customerId)) {

            try {

                $customerArr['card_id'] = '';
                $retrive = \Stripe\Customer::retrieve($customerId);
                $cufingerprint = $this->checkifcardExist($email, $fingerprint,$customerId);
                $customerArr['customer_id'] = $customerId;

                $deleted = false;
                if (isset($retrive->deleted)) {
                    $deleted = $retrive->deleted;
                }
                if ($retrive->id && !$deleted) {
                    if (!empty($cufingerprint)) {
                        foreach ($retrive->sources->data as $card) {
                            if ($cufingerprint == $card->fingerprint) {

                                $currentCardID = $card->id;
                                $customerArr['card_id'] = $currentCardID;
                            }
                        }
                    }
                    if (empty($cufingerprint)) {

                        $retrive->sources->create(array("source" => $token));
                        $retrive->save();

                        $getCard = \Stripe\Customer::retrieve($customerId);


                        foreach ($getCard->sources->data as $card) {

                            if ($fingerprint == $card->fingerprint) {

                                $currentCardID = $card->id;
                                $customerArr['card_id'] = $currentCardID;
                            }
                        }

                        // saving new card fingerprint
                        $this->saveStripeFingerprint($email, $customerId, $fingerprint);
                    }

                    $isCustomer = true;
                }
            } catch (\Stripe\Error\ApiConnection $e) {
                
            } catch (\Stripe\Error\InvalidRequest $e) {
                
            }
        }

        if (!$isCustomer) {

            $customer = $this->createStripeCustomer($confirmation, $restaurant, $email, $token, $type = 'weeloy', $mode);

            if ($customer['result'] === 1) {
                $customerId = $customer['id'];
                $customerArr['customer_id'] = $customerId;
                $customerArr['card_id'] = $customer['card_id'];
                $this->saveStripeFingerprint($email, $customerId, $fingerprint);
            }
        }

        return $customerArr;
    }

    //booking  payment
    //CCDO 

    
    function createBookingPaymentDetails($restaurant,$reference_id,$email,$amount,$token,$mode='create'){
      
        $tokenObj=$this->stripeToken($reference_id, $restaurant, $email,$token,'booking');

        $payToken = md5(time());
        if ($tokenObj['isSuccess'] === true) {
            $customer = $this->getCustomerId($email, $tokenObj['fingerprint'], $reference_id, $restaurant, $tokenObj['token'], 'Booking');
            $customerId = $customer['customer_id'];
            $cardId = $customer['card_id'];
            if (isset($customerId) && !empty($customerId)) {
                $resultatSaveCardDetails = $this->saveCardDetails($amount, $customerId, $reference_id, $restaurant, $payToken, $tokenObj['token'], $email, 'carddetails');
                $tokenObj['status'] = 'COMPLETED';
                if ($resultatSaveCardDetails) {
                    //pdo_exec("update booking  set status ='' where confirmation ='$confirmation' limit 1");
                    $notify = $this->updatePayStatus($reference_id, $customerId, $cardId, 'COMPLETED', 'carddetails');
                    if ($notify) {
                        $booking = new WY_Booking();
                        $booking->getBooking($reference_id);
                        $notification = new WY_Notification();
                        $notification->notify($booking, 'booking');
                    }
                    $tokenObj['status'] = 'COMPLETED';
                    return $tokenObj;
                }
            } else {
                $booking = new WY_Booking();
                $booking->getBooking($reference_id);
                $notification = new WY_Notification();
                $notification->notify($booking, 'booking_pending');
                $tokenObj['status'] = 'pending';
                return $tokenObj;
            }
        } else {
            $booking = new WY_Booking();
            $booking->getBooking($reference_id);
            $notification = new WY_Notification();
            $notification->notify($booking, 'booking_pending');
            $tokenObj['status'] = 'pending';
            return $tokenObj;
        }
    }

    
    function updateBookingPaymentDetails($restaurant, $reference_id, $email, $amount, $token, $mode = 'update') {

        $tokenObj = $this->stripeToken($reference_id, $restaurant, $email, $token, 'booking');
  

        $payToken = md5(time());
        if ($tokenObj['isSuccess'] === true) {
            $customer = $this->getCustomerId($email, $tokenObj['fingerprint'], $reference_id, $restaurant, $tokenObj['token'], 'Booking');
            $customerId = $customer['customer_id'];
            $cardId = $customer['card_id'];
            if (isset($customerId) && !empty($customerId)) {
                //check if already created ccdetails or not
                $tokenObj['status'] = 'COMPLETED';
                $isExistccDetails = $this->getDepositDetails($customerId, $reference_id);

                if ($isExistccDetails) {
                
                    $resultatSaveCardDetails = $this->updateccDetails($customerId, $reference_id, $amount,'COMPLETED');
                } else {
                    $resultatSaveCardDetails = $this->saveCardDetails($amount, $customerId, $reference_id, $restaurant, $payToken, $tokenObj['token'], $email, 'carddetails');
                    $this->updatePayStatus($reference_id, $customerId, $cardId, 'COMPLETED', 'carddetails');
                }
                if ($resultatSaveCardDetails) {
//                    $booking = new WY_Booking();
//                    $booking->getBooking($reference_id);
//                    $notification = new WY_Notification();
//                    $notification->notify($booking, 'update');
                     $tokenObj['status'] = 'COMPLETED';
                    return $tokenObj;
                }
            } else {
                $tokenObj['status'] = 'pending';
                return $tokenObj;
            }
        } else {
            $tokenObj['status'] = 'pending';
            return $tokenObj;
        }
    }


    function stripeBookingDeposit($restaurant, $reference_id, $email, $amount, $token) {
        
        $tokenObj = $this->stripeToken($reference_id, $restaurant, $email, $token,'booking');
        $payToken = md5(time());
        $tokenObj['payment_id'] = '';
        if ($tokenObj['isSuccess'] === true) {
            $customer = $this->getCustomerId($email, $tokenObj['fingerprint'], $reference_id, $restaurant, $tokenObj['token'], 'Booking');
            $customerId = $customer['customer_id'];
            $cardId = $customer['card_id'];
            $tokenObj['payment_id'] = $customerId;
            $result = $this->createStripePayment($customerId, $restaurant, $reference_id, $email, $amount, 'website',$type = 'stripe', $mode = 'booking', $cardId);
            if ($result['status'] == 'succeeded') {
                $result['status'] = "COMPLETED";
                $tokenObj['status'] = "COMPLETED";
                //$sql = pdo_exec("UPDATE booking SET booking_deposit_id = '{$charge['payment_id']}' WHERE confirmation = '$confirmation' ");
                  $paymentArr = array(
                        'email' => $email,
                        'source' => $token
                    );
                $resultatSaveCardDetails = $this->saveCardDetails($amount, $result['payment_id'], $reference_id, $restaurant, $payToken, $tokenObj['token'], $email, 'stripe');
                $notify = $this->updatePayStatus($reference_id, $result['payment_id'], $cardId, 'COMPLETED', 'carddetails');
                if ($notify) {
                        $booking = new WY_Booking();
                        $booking->getBooking($reference_id);
                        $notification = new WY_Notification();
                        $notification->notify($booking, 'booking');
                }

            }else{
                $booking = new WY_Booking();
                $booking->getBooking($reference_id);
                $notification = new WY_Notification();
                $notification->notify($booking, 'booking_pending');
                $tokenObj['status'] = 'pending';
            }
            return $result;
        }
        $tokenObj['status'] = 'pending';
        return $tokenObj;
    }

    //event booking


    function createEventPayment($restaurant, $reference_id, $email, $orderID, $amount, $event_id, $token, $type, $tracking = '') {

        $event = new WY_Event();
        $event_management = new WY_EventManagement();
        $tokenObj = $this->stripeToken($reference_id, $restaurant, $email, $token, 'event');
        $tokenObj['payment_id'] = '';


        if ($tokenObj['isSuccess'] === true) {
            $customer = $this->getCustomerId($email, $tokenObj['fingerprint'], $reference_id, $restaurant, $tokenObj['token'], 'Event');
            $customerId = $customer['customer_id'];
            $cardId = $customer['card_id'];

            $tokenObj['payment_id'] = $customerId;
            $result = $this->createStripePayment($customerId, $restaurant, $reference_id, $email, $amount,'','creditcard', 'Event', $cardId);
            if ($result['status'] == 'succeeded') {
                $result['status'] = "COMPLETED";
                $tokenObj['status'] = "COMPLETED";
                $tokenObj['payment_id'] = $result['payment_id'];
                $notification = new WY_Notification();
                if ($type == 'private_event') {
                    
                    $data = $result;
                    $data['event_id'] = $reference_id ;
                    $data['type'] = 'creditcard';
                    $data['card_id'] = $cardId;
                    $data['status'] = 'confirmed';
                    $data['amount'] = $amount;
                    $updatePaymentDetails = $event_management::saveEvPaymentDetails($data);
                    $args = $event_management::getEventProjet(array('event_id' => $reference_id));
                    $event_management::notifyEventMangement($args, 'event_management');
                } else {
                    $this->updateEventPaymentId($reference_id, $orderID, $result['status'], $result['payment_id'], $cardId);
                    $event->getEventTicket($orderID);

             
                    if(!empty($tracking)){
                        $event->bktracking='WEBSITE';
                        $event->white_label = true;
                    }
                    $notification->notifyEvent($event, 'event_booking');
                }
            }
            return $result;
        }
        $tokenObj['status'] = 'pending';
        return $tokenObj;
    }

    function stripeToken($reference_id, $restaurant, $email, $token, $type = 'booking') {

        $res = new WY_restaurant();
        $res->getRestaurant($restaurant);
        $logger = new WY_log('website');
        $loguserid = (isset($_SESSION['user']['user_id'])) ? $_SESSION['user']['user_id'] : "0";
        $isSuccess = false;
        $isCustomer = false;

        $this->stripeCredentials($restaurant);
        \Stripe\Stripe::setApiKey($this->secret_key);

        $tokenArr['token'] = null;
        $tokenArr['customer_id'] = null;
        $tokenArr['isCustomer'] = $isCustomer;
        $tokenArr['fingerprint'] = null;
        $tokenArr['isSuccess'] = $isSuccess;
        $tokenArr['card_id'] = null;

        try {
//            $params = array(
//                "card" => array(
//                    "number" => $number,
//                    "exp_month" => intval($exp_month),
//                    "exp_year" => intval($exp_year),
//                    "cvc" => $cvc,
//                    "name" => $name
//            ));
//            $resObject = \Stripe\Token::create($params);

             $resObject = \Stripe\Token::retrieve($token);


            $isSuccess = true;

            /*             * *************************chekcing fingerprint  * ************************** */
            $stripe_customer_id = "";
            $fingerprint = $resObject->card->fingerprint;

//            if (strlen($params['card']['number']) === 16) {
//                $params['card']['number'] = substr_replace($params['card']['number'], 'XXXXXXXX', 4, 8);
//            } else {
//                $params['card']['number'] = substr_replace($params['card']['number'], 'XXXXXXXX', 4, 6);
//            }

            $customer_id = $this->checkFingerprint($email, $fingerprint);


            //token response insert in payment_stripe table
            if (isset($resObject->card->fingerprint)) {
                $stripe_customer_id = $customer_id;
            }

            if (!empty($customer_id)) {
         
                try {
                    $retrive = \Stripe\Customer::retrieve($customer_id);
            
                    if ($retrive->id && !$retrive->deleted) {
                        $isCustomer = true;
                    }
                } catch (\Stripe\Error\ApiConnection $e) {

                    $this->stripeCatchError($reference_id,$restaurant,$this->getHeader(),$tokenArr,$e);
                } catch (\Stripe\Error\InvalidRequest $e) {
                    $this->stripeCatchError($reference_id,$restaurant,$this->getHeader(),$tokenArr,$e);
                }
            }
   
            $tokenArr = array(
                'token' => $resObject->id,
                'customer_id' => $stripe_customer_id,
                'isCustomer' => $isCustomer,
                'fingerprint' => $fingerprint,
                'card_id' => $resObject->card->id,
                'isSuccess' => $isSuccess
            );

            return $tokenArr;
        } catch (\Stripe\Error\Card $e) {
            $this->stripe_api_log($reference_id, $restaurant, $this->getHeader(), $tokenArr, $e, $e->getHttpStatus());
        } catch (\Stripe\Error\RateLimit $e) {
            $this->stripe_api_log($reference_id, $restaurant, $this->getHeader(), $tokenArr, $e, $e->getHttpStatus());
        } catch (\Stripe\Error\InvalidRequest $e) {
            $this->stripe_api_log($reference_id, $restaurant, $this->getHeader(), $tokenArr, $e, $e->getHttpStatus());
        } catch (\Stripe\Error\Authentication $e) {
            $this->stripe_api_log($reference_id, $restaurant, $this->getHeader(), $tokenArr, $e, $e->getHttpStatus());
        } catch (\Stripe\Error\ApiConnection $e) {
            $this->stripe_api_log($reference_id, $restaurant, $this->getHeader(), $tokenArr, $e, $e->getHttpStatus());
        } catch (\Stripe\Error\Base $e) {
            $this->stripe_api_log($reference_id, $restaurant, $this->getHeader(), $tokenArr, $e, $e->getHttpStatus());
        } catch (Exception $e) {
            $this->stripe_api_log($reference_id, $restaurant, $this->getHeader(), $tokenArr, $e, $e->getHttpStatus());
        }

        return $tokenArr;
    }

    function stripeCatchError($reference_id, $restaurant, $header, $params, $e) {
        $this->stripe_api_log($reference_id, $restaurant, $this->getHeader(), $params, $e, $e->getHttpStatus());
    }


}
