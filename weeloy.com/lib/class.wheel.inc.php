<?php

// don't use the table wheeeldetails
// produce wheel for standart wheel
// move getBestOffer -> wheel class
// generate Image wheel coporate
// play wheel corpo

require_once "lib/wpdo.inc.php";
require_once("lib/wglobals.inc.php");

class WY_wheel {

    use CleanIng;

    var $dirname;
    var $showdirname;
    var $showglb;
    var $restaurant;
    var $type;
    var $wheel;
    var $wheelvalue;
    var $wheelpart;
    var $wheelval;
    var $wheeldesc;
    var $wheelorder;
    var $wheelversion;
    var $jsonwheel;
    var $script;
    var $data;
    var $wheelconfoffer;
    var $wheelconfvalue;
    var $wheeltitle;
    var $valid;
    var $image;
    var $imagewheel;
    var $result;
    var $msg;

    function __construct($theRestaurant, $source, $type) {
        $this->init($theRestaurant, $source, $type);
    }

    private function init($theRestaurant, $source, $type) {
        $this->restaurant = $theRestaurant;
        $this->wheelversion = time();
        $this->type = $this->clean_input($type);
        $this->result = 0;
        $this->msg = "";
        $this->readConfig();
    }

    private function readConfig() {

        global $wheeloffers;

        $this->wheelconfoffer = $wheeloffers;

        $theRestaurant = $this->restaurant;
        $data = pdo_multiple_select("SELECT offer, description, value, morder FROM wheeloffer WHERE restaurant = '$theRestaurant' and type = 'EXTRA' order by morder");

        foreach ($data as $row) {
            $this->wheelconfoffer[] = $row['offer'];
            $this->wheelconfoffer[] = intval($row['value']);
            $this->wheelconfoffer[] = $row['description'];
        }

        $this->wheelconfvalue = array();
        $limit = count($this->wheelconfoffer);
        for ($i = 0; $i < $limit; $i += 3) {
            $this->wheelconfvalue[$this->wheelconfoffer[$i]] = $this->wheelconfoffer[$i + 1];
            $this->wheeltitle[] = $this->wheelconfoffer[$i];
        }

        //$this->wheelconfvalue["NOT FOUND"] = 0;
        //$this->wheeltitle[] = "NOT FOUND";
    }

    function getWheel() {
        $this->msg = "";
        $this->result = 1;

        $this->readWheel();
        if ($this->wheel == "") {
            $this->msg = "Unable to find the wheel for -$theRestaurant-";
            $this->result = -1;
            return array();
        }
        return $wdata = array("wheel" => $this->jsonwheel, "wheelversion" => $this->wheelversion);
    }
    
    function getWheelVersion() {
        $sql = "SELECT wheelversion FROM wheel WHERE restaurant = '$this->restaurant' and type = '$this->type' limit 1";
        
        $data = pdo_single_select($sql);
        return $data['wheelversion'];
    }
    

    function readWheel() {

        $theRestaurant = $this->restaurant;
        $type = $this->type;
        $this->wheelversion = 0;
        $this->wheel = "";
        $this->wheelvalue = 0;

        if ($type == ''){
            $data = pdo_single_select("SELECT wheel, wheelversion, wheelvalue FROM restaurant WHERE restaurant = '$theRestaurant' limit 1");
        }
        else{
            $data = pdo_single_select("SELECT wheeloffers as wheel, wheelversion, wheelvalue FROM wheel WHERE restaurant = '$theRestaurant' and type = '$type' limit 1");
        }

        if (isset($data['wheelversion'])) {
            $this->wheelversion = $data['wheelversion'];
            $this->wheel = $data['wheel'];
            $this->wheelvalue = $data['wheelvalue'];
            $this->decodewheel();
        }
        return $this->wheel;
    }

    static function getBestOffer($theRestaurant, $nbOffer, $cpp_type = NULL) {
        try {
            if($cpp_type == NULL)
                $cpp_type = '';
            if($cpp_type == 'cpp_credit_suisse')
                $cpp_type = 'corporate';
            //$sql = "SELECT distinct offer, description, value as score FROM wheeloffer WHERE restaurant = '$theRestaurant' and type = ''  ORDER BY value DESC LIMIT $nbOffer";
            if($nbOffer == 1){
                //$data = pdo_single_select($sql);
                $sql = "SELECT distinct offer, description, value as score FROM wheeloffer WHERE restaurant = '$theRestaurant' AND type LIKE '$cpp_type' ORDER BY value DESC LIMIT $nbOffer";
                $data = pdo_single_select($sql);
                if(count($data) > 0) {
                    $data['offer_cpp'] = $data['offer'];
                    $data['description_cpp'] = $data['description'];
                }
            } else {
                $sql = "SELECT distinct offer, description, value as score FROM wheeloffer WHERE restaurant = '$theRestaurant' and type = ''  ORDER BY value DESC LIMIT $nbOffer";
                $data = pdo_multiple_select($sql);
            }
        } catch (Exception $e) {
            error_log(__FILE__." ".__FUNCTION__." ".__LINE__." Exception: ".$e);
        }
        return $data;
    }

    // not used	
    static function getBestOfferDescription($theRestaurant, $wheel, $cpp_type = NULL) {
        return pdo_single_select("SELECT offer, description, value as score FROM wheeloffer WHERE offer ='$wheel' AND (restaurant = '$theRestaurant' OR restaurant = 'GLOBAL') LIMIT 1");
    }

    private function decodewheel() {

        $str = explode("|", $this->wheel);
        $this->wheelvalue = intval(substr($str[0], 6));
        $this->valid = (substr($this->wheel, 0, 5) == "VALID");
        $this->wheelpart = array();
        $this->wheelval = array();
        $this->wheelorder = array();
        $this->wheeldesc = array();

        $jsonwheel = "\"value\":\"" . $str[0] . "\"";
        for ($i = 1; $i < 25; $i++) {
            $jsonwheel .= ", \"slice" . $i . "\":\"" . $str[$i] . "\"";
            $this->wheelpart[] = $parts = $str[$i];
            $this->wheelval[] = (isset($this->wheelconfvalue[$parts])) ? $this->wheelconfvalue[$parts] : 1;
            $this->wheelorder[] = $i;
            $index = array_search($parts, $this->wheeltitle); // do not forget !== as array search can return 0 as valid index, could be interpreted a false !
            $this->wheeldesc[] = ($index !== false) ? $this->wheelconfoffer[($index * 3) + 2] : "NOT FOUND";
        }
        $this->jsonwheel = $jsonwheel;
    }

    private function encodewheel() {
        $this->wheel = $jsonwheel = "";
        $this->wheelvalue = 0;

        $this->wheelval = array();
        $this->wheelorder = array();
        $this->wheeldesc = array();
        for ($i = 0; $i < 24; $i++) {
            $parts = $this->wheelpart[$i];
            $this->wheelval[] = $this->wheelconfvalue[$parts];
            $this->wheelorder[] = $i + 1;
            $index = array_search($parts, $this->wheeltitle); // do not forget !== as array search can return 0 as valid index, could be interpreted a false !
            $this->wheeldesc[] = ($index !== false) ? $this->wheelconfoffer[($index * 3) + 2] : "NOT FOUND";
            $this->wheelvalue += $this->wheelconfvalue[$parts];
            $this->wheel .= "|" . $parts;
            $jsonwheel .= ", \"slice" . ($i + 1) . "\":\"" . $parts . "\"";
        }
        $this->wheel = "VALID" . "-" . $this->wheelvalue . "-" . $this->wheel;
        $this->jsonwheel = "\"value\":\"" . $this->wheelvalue . "\"" . $jsonwheel;
        $this->valid = true;
    }

    private function writewheel() {
        $theRestaurant = $this->restaurant;
        $type = $this->type;

        if ($type == 'EXTRA') {
            $this->msg = "Invalid Type 'EXTRA'";
            return $this->result = -1;
        }

        $this->encodewheel();

        $wheelversion = $this->wheelversion;
        $totalValue = $this->wheelvalue;
        $wheeldata = $this->wheel;
        $wheelconfoffer = implode('|||', $this->wheelconfoffer);

        // if wheel standard, write a copy in table restaurant => backward compatibility
        if ($type == "")
            pdo_exec("UPDATE restaurant SET wheelversion='$wheelversion', wheel='$wheeldata', wheelvalue='$totalValue' WHERE restaurant = '$theRestaurant' limit 1");

        // move the record to the archive wheel table: wheelold
        pdo_exec("INSERT INTO wheelold (restaurant, wheelversion, wheelvalue, wheeloffers, wheeldescriptions, type, status, creation_date) select restaurant, wheelversion, wheelvalue, wheeloffers, wheeldescriptions, type, status, creation_date from wheel where restaurant = '$theRestaurant' and type = '$type' limit 1");
        pdo_exec("UPDATE wheelold SET status = 'deleted' WHERE restaurant = '$theRestaurant' and type = '$type' limit 1");
        pdo_exec("delete from wheel WHERE restaurant = '$theRestaurant' and type = '$type'");

        // create a new wheel, including standard wheel
        pdo_exec("INSERT INTO wheel (restaurant, wheelversion, wheelvalue, wheeloffers, wheeldescriptions, type, status, creation_date) VALUES ('$theRestaurant', $wheelversion, $totalValue, '$wheeldata', '$wheelconfoffer', '$type', 'active', now() )");

        // save the 24 wheel parts

        for ($i = 0; $i < 24; $i++) {
            $offer = $this->wheelpart[$i];
            $description = $this->wheeldesc[$i];
            $morder = $this->wheelorder[$i];
            $value = $this->wheelval[$i];
            $data = pdo_single_select("SELECT ID FROM wheeloffer WHERE restaurant = '$theRestaurant' and type = '$type' and morder = '$morder' limit 1 ");
            $insertflag = (count($data) < 1);
            if ($insertflag)
                pdo_exec("insert into wheeloffer (restaurant, offer, description, value, type, morder) VALUES ('$theRestaurant', '$offer', '$description', '$value', '$type', '$morder')");
            else
                pdo_exec("update wheeloffer set offer='$offer', description='$description', value='$value' where restaurant='$theRestaurant' and type='$type' and morder='$morder' limit 1");
        }

        // $this->savedetails_parts();

        return $this->saveImgWheel();
    }

    // this is use for the backoffice	
    function save_wheel($arg) {

        if ($arg == "sponsor") {  // the first parts 24, 1, 2 are equal based on 1
            $_REQUEST['itemwheelvalue24'] = $_REQUEST['itemwheelvalue2'] = $_REQUEST['itemwheelvalue1'];
        }

        $this->wheelpart = array();
        for ($i = 1; $i <= 24; $i++) {
            $item = "itemwheelvalue" . $i;
            $this->wheelpart[] = $parts = (isset($_REQUEST[$item])) ? $_REQUEST[$item] : "NOT FOUND";
        }
        return $this->writewheel();
    }

    // this is use for the backoffice	
    function save_stdwheel() {
        global $stdwheeloffers;

        $this->wheelpart = array();
        for ($i = 0; $i < 24; $i++) {
            $this->wheelpart[] = $parts = $this->wheelconfoffer[$stdwheeloffers[$i] * 3];
        }
        return $this->writewheel();
    }

    function summary() {
        $this->readWheel();
        if (empty($this->wheel))
            return array();

        $parts = array();
        for ($i = 0; $i < 24; $i++) {
            if (empty($this->wheelpart[$i]))
                continue;
            else if (isset($parts[$this->wheelpart[$i]]))
                $parts[$this->wheelpart[$i]] ++;
            else
                $parts[$this->wheelpart[$i]] = 1;
        }
        arsort($parts);
        reset($parts);
        return $parts;
    }

    private function saveImgWheel() {

        $theRestaurant = $this->restaurant;
        $wheelversion = $this->wheelversion;
        $type = $this->type;
        /* a definir dans la config */
        $protocol = (strstr($_SERVER['HTTP_HOST'], 'localhost') == false) ? 'https' : 'http';

        if ($this->valid) {
            $this->writescript();
            $url = $protocol . '://' . $_SERVER['HTTP_HOST'] . __ROOTDIR__ . "/wheel/wheeldb.php?savewheel=w76849361&wheelversion=$wheelversion&restaurant=$theRestaurant&type=$type";
            //error_log("THE WHEEL IMAGE $url");		
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            $output = curl_exec($ch);
            curl_close($ch);

            $url = $protocol . '://' . $_SERVER['HTTP_HOST'] . __ROOTDIR__ . "/wheel/wheelrating.php?savewheel=w76849361&wheelversion=$wheelversion&restaurant=$theRestaurant&type=$type";
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            $output = curl_exec($ch);
            curl_close($ch);
        }
        return array(1, "Done");
    }

    private function writescript() {

        $this->image = new WY_Media($this->restaurant);

        $type_name = ($this->type == "") ? "" : $this->type . "_";
        $tmp_filename = $this->image->tmpdir . $this->restaurant . "_" . $type_name . "wheelscript.js";
        $filename = $type_name . 'wheelscript.js';

        if (!($fp = fopen($tmp_filename, 'w')))
            return;

        fprintf($fp, "var wheelmsg = [");
        for ($i = 0, $sep = ""; $i < 24; $i++, $sep = ",")
            fprintf($fp, "%s '%s' ", $sep, $this->wheelpart[$i]);

        for ($i = 0; $i < 24; $i++, $sep = ",") {
            if (($index = array_search($this->wheelpart[$i], $this->wheeltitle)) !== false) // do not forget !== as array search can return 0 as valid index, could be interpreted a false !
                fprintf($fp, "%s '%s' ", $sep, $this->wheelconfoffer[($index * 3) + 2]);
            else
                fprintf($fp, "%s '%s' ", $sep, "NOT FOUND: " . $search);
        }

        fprintf($fp, "];");
        fclose($fp);

        $this->image->l_move_uploaded_file($tmp_filename, $filename, 'application/javascript', 'wheel');
        @unlink($tmp_filename);
    }

    function savesponsor($sponsor) {
        pdo_exec("UPDATE restaurant SET wheelsponsor = '$sponsor' WHERE restaurant = '$this->restaurant' limit 1");
    }

    function extractpart($offer) {
        return pdo_single_select("select offer, description, value, morder from wheeloffer WHERE restaurant = '$this->restaurant' and offer = '$offer' and type = 'EXTRA' limit 1");
    }

    function extraslices() {
        return pdo_multiple_select("select offer from wheeloffer WHERE restaurant = '$this->restaurant' and type = 'EXTRA' order by morder");
    }

    function deletepart($offer) {
        pdo_exec("delete from wheeloffer WHERE restaurant = '$this->restaurant' and offer = '$offer' and type = 'EXTRA' limit 1");
    }

    function savepart($offer, $description, $value, $morder) {

        // we do not care on duplicate for morder, as long as type is 'EXTRA'
        $theRestaurant = $this->restaurant;
        $data = pdo_single_select("select ID from wheeloffer WHERE restaurant = '$theRestaurant' and offer = '$offer' and type = 'EXTRA' limit 1");

        if (count($data) < 1)
            pdo_exec("INSERT INTO wheeloffer (restaurant, offer, description, value, type, morder) VALUES ('$theRestaurant', '$offer', '$description', '$value', 'EXTRA', '$morder')");
        else
            pdo_exec("update wheeloffer set offer='$offer', description='$description', value='$value', morder='$morder' where restaurant='$theRestaurant' and offer = '$offer' and type = 'EXTRA' limit 1");
    }

    // for non standard wheel
    function readtypeWheel() {
        $theRestaurant = $this->restaurant;
        $type = $this->type;

        if ($type == 'EXTRA' || empty($type)) {
            $this->msg = "Invalid Type 'EXTRA'";
            return $this->result = -1;
        }

        $data = pdo_multiple_select("select offer, description, value, morder from wheeloffer WHERE restaurant = '$theRestaurant' and type = '$type' order by morder");
        $this->result = (count($data) >= 4) ? 1 : -1;
        return $data;
    }

    function readofferWheel() {
        $theRestaurant = $this->restaurant;
        $data = pdo_multiple_select("select offer, description, value, morder from wheeloffer WHERE restaurant = 'GLOBAL' or (restaurant = '$theRestaurant' and type = 'EXTRA') order by restaurant, morder");
        $this->result = (count($data) >= 4) ? 1 : -1;
        return $data;
    }

    function createtypeWheel($obj) {
        return updatetypeWheel($obj);
    }

    function updatetypeWheel($obj) {
        $this->result = 1;
        $this->msg = "";
        $type = $this->type;

        if ($type == 'EXTRA' || empty($type)) {
            $this->msg = "Invalid Type 'EXTRA' or standard";
            return $this->result = -1;
        }

        $limit = count($obj);
        if ($limit != 24) {
            $this->msg = "Invalid number of parts $limit";
            return $this->result = -1;
        }

        $this->wheelpart = array();
        $this->wheelval = array();
        $this->wheelorder = array();
        $this->wheeldesc = array();
        for ($i = 0; $i < $limit; $i++) {
            $oo = $obj[$i];
            $this->wheelpart[] = $oo['offer'];
            $this->wheeldesc[] = $oo['description']; // it will eventually be overwritten
            $this->wheelorder[] = $oo['morder']; // it will eventually be overwritten
            $this->wheelval[] = $oo['value']; // it will eventually be overwritten
        }
        $this->writewheel();
    }

    // not really used	
    function deletetypeWheel() {

        $theRestaurant = $this->restaurant;
        $type = $this->type;

        if ($type == 'EXTRA' || empty($type)) {
            $this->msg = "Invalid Type 'EXTRA' or standard";
            return $this->result = -1;
        }

        if (!empty($type)) { /* don't delete a standart wheel */
            pdo_exec("delete from wheeloffer WHERE restaurant = '$theRestaurant' and type = '$type'");
            pdo_exec("delete from wheel WHERE restaurant = '$theRestaurant' and type = '$type' limit 1");
        }
    }

    // don't use wheeldetails
    private function savedetails_parts() {

        //inserting data into weeldetails table

        $theRestaurant = $this->restaurant;
        $wheelversion = $this->wheelversion;
        $data = pdo_multiple_select("SELECT id FROM wheeldetails WHERE restaurant_id ='$theRestaurant'");
        if (count($data) > 0) {
            pdo_exec("DELETE FROM wheeldetails WHERE restaurant_id='$theRestaurant'");
        }

        for ($i = 0; $i < 24; $i++) {
            $offer = $this->wheelpart[$i];
            $data = pdo_single_select("SELECT ID FROM wheeloffer WHERE  offer ='$offer' AND (restaurant = '$theRestaurant' OR restaurant = 'GLOBAL') and type = 'EXTRA'  limit 1 ");
            $offerId = (count($data) > 0 && isset($data['id'])) ? $data['id'] : 0;
            pdo_exec("INSERT INTO wheeldetails (offer_id,restaurant_id, wheelversion, order_value) VALUES ('$offerId','$theRestaurant',$wheelversion,  $i)");
        }
    }

    function upateCheckOfferTable() {
        // SG_SG_R_TheFunKitchen

        $type = '';
        $data = pdo_multiple_select("SELECT restaurant.restaurant as restaurant, wheel, wheeloffers FROM restaurant, wheel WHERE wheel != '' and wheel.restaurant = restaurant.restaurant and wheel != wheeloffers and wheel.type = '' order by restaurant.restaurant");

        foreach ($data as $row) {
            printf("incoherent, %s<br>%s<br>%s<hr>", $row['restaurant'], $row['wheel'], $row['wheeloffers']);
        }

        if (count($data) > 0)
            exit;

        $data = pdo_multiple_select("SELECT restaurant, wheel FROM restaurant WHERE wheel != '' order by restaurant");
        foreach ($data as $row) {
            $this->init($row['restaurant'], 'api', '');
            $this->wheel = $row['wheel'];
            $this->decodewheel();
            for ($i = 0; $i < 24; $i++) {
                $offer = $this->wheelpart[$i];
                $description = $this->wheeldesc[$i];
                $morder = $this->wheelorder[$i];
                $value = $this->wheelval[$i];
                $tt = pdo_single_select("SELECT ID FROM wheeloffer WHERE restaurant = '$this->restaurant' and type = '$this->type' limit 1 ");
                $insertflag = (count($tt) < 1);
                if ($insertflag)
                    pdo_exec("insert into wheeloffer (restaurant, offer, description, value, type, morder) VALUES ('$this->restaurant', '$offer', '$description', '$value', '$this->type', '$morder')");
                else
                    pdo_exec("update wheeloffer set offer='$offer', description='$description', value='$value' where restaurant='$this->restaurant' and type='$this->type' and morder='$morder' limit 1");
            }
        }
    }

}

?>