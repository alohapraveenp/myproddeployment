<?php

/************************************************/

// this is SPECIAL KEY FLAGS
define("RESERVED_CREDITCARDDETAILS", 1);   // get Only credit card
define("RESERVED_DEPOSITPAYPAL", 2);   // credit card
define("RESERVED_BKDEPOSIT", 4);   // check depost booking display on backoffice 
define("RESERVED_TABLEPOS", 8);   // table Thanying TEMPORARY
define("RESERVED_TMS", 0x10);   // table Thanying TEMPORARY
define("CALLCENTERMEMBER", 0x20);   // table Thanying TEMPORARY
define("BOOKING15MN", 0x40);
define("TMSSYNC", 0x80); // sync all tms user
define("ONEMONTHBOOKING", 0x100); // sync all tms user
define("DAILYSPECIALBOARD", 0x200);	// only dsb
define("RESTRICTED_DSB", 0x400);	// only dsb
define("ONEYEARCALLCENTER", 0x800);	// 1 year booking window
define("CALLCENTERPAYMENT", 0x1000);	// callcenter Payement activation
define("MODIFYPAYMENTBK", 0x2000);


/************************************************/

define("TOO_SHORT", -1);
define("DOESNOT_EXISTS", -2);
define("ALREADY_EXISTS", -4);

define("NO_WALKING", 1);
define("SPONSOR_WHEEEL", 2);
define("OPEN24HOUR7BY7", 4);
define("PERPAXRESA", 8);   // booking per person. Standard is per table
define("CALLCENTEREXTRACTION", 0x10);   // booking per person. Standard is per table
define("FIVESLOTMEAL", 0x20);   // 5 slots meal, 150mn. if not set, normal slot time is set to 3 slots of 30mn -> 90mn
define("WITHPROMOTION", 0x40);   // 5 slots meal, 150mn. if not set, normal slot time is set to 3 slots of 30mn -> 90mn
define("ENABLEBOOKHOUR", 0x80);   // use the min field for max people
define("SMSNOTIFTORESTAURANT", 0x100);   // notify restaurant by sms
define("TAKEOUTRESTAURANT", 0x200);   // take out restaurant
define("RESTAURANTEXTERNALLINK", 0x400);   // activate external link
define("CORPORATEWHEEL", 0x800);   // corporate wheel
define("OFFPEAKWHEEL", 0x1000);   // off-peak wheel
define("PROMOCODEBKG", 0x2000);   // promo code display on booking page for restaurant website
define("OPTINBKG", 0x4000);   // opt-in for restaurant website
define("BOOKINGHOURBYSLOT", 0x8000);   // opt-in for restaurant website
define("BOOKINGWITHCHILDREN", 0x10000);   // input children for B2B website/facebook booking
define("CHECKDUPLICATE", 0x20000);   // check duplicate booking
define("GRABZ", 0x40000);   // check grabz restaurant
define("BKDEPOSIT", 0x80000);   // check depost booking display on backoffice 
define("EVENTBOOKING", 0x100000);   // check  price display on backoffice event creation
define("ADVANCEBOOKING", 0x200000);   // 6 month ahead
define("WEBSITEFIELDS", 0x400000);   // cms for WEB to avoid duplicate content
define("TWOSITTING", 0x800000);   // 2-sitting availability for dinner

define("DAILYCUTOFF", 0x1000000);   // daily cutoff
define("EMAILWHITELABEL", 0x2000000);   // white label email
define("TMSCAPTAIN", 0x4000000);   // captain features on layout
define("DEPOSITPAYPAL", 0x8000000);   // credit card
define("TABLEPOS", 0x10000000);   // table Thanying TEMPORARY
define("CREDITCARDDETAILS", 0x20000000);   // get Only credit card
define("MUTIPLEPRODUCTALLOTE", 0x40000000);    // Multiple Product Allotement

define("BLOCKEDGUESTLIST", 0x80000000);    // Block List

define("TWOFACTAUTHENTICATION", 0x100000000);   //GOOGLE TWO FACT AUTHENTICATION  

define("SMSTEMPLATE", 0x200000000);  // backoffice  load sms notification
define("ACTIVITYLOG", 0x400000000);  // backoffice Booking activity log 
define("TMSAPPLICATION", 0x800000000);  // backoffice Booking activity log 

define("SMSHEADER", 0x1000000000);  // sms heder from restaurant specific mobile number
define("PABX", 0x2000000000);  // backoffice  load sms notification
define("PAYMENTINVOICE", 0x4000000000);  //backoffice active invoice payment
define("DEPOSITADYEN", 0x8000000000);  // adyen payemnt gate way activation
define("DEPOSITREDDOT", 0x10000000000); 
define("TMSV2", 0x20000000000); // TMS Version 2


require_once("lib/class.wheel.inc.php");
require_once("lib/class.debug.inc.php");
require_once("lib/class.string.inc.php");


class WY_restaurant {

    use CleanIng;

    var $ID;
    var $dirname;
    var $showdirname;
    var $restaurant;
    var $hotelname;
    var $title;
    var $logo;
    var $cuisine;
    var $description;
    var $address;
    var $address1;
    var $city;
    var $country;
    var $zip;
    var $tel;
    var $fax;   // virtual
    var $email;
    var $smsid;
    var $restaurant_tnc;
    var $url;
    var $map;
    var $region;
    var $pricing;
    var $currency;
    var $paypal;
    var $pricerating;
    var $pricelunch;
    var $pricediner;
    var $rating;
    var $mealslot;
    var $dfproduct;
    var $dfmealtype;
    var $dfminpers;
    var $dfmaxpers;
    var $openhours;
    var $bookhours;
    var $bookwindow;
    var $award;
    var $GPS;
    var $status;
    var $is_displayed;
    var $is_wheelable;
    var $is_bookable;
    var $extraflag;
    var $reservedflag;
    var $wheel;
    var $wheelversion;
    var $wheelsponsor;
    var $wheelRescode;
    var $chef;
    var $chef_award;
    var $chef_logo;
    var $chef_origin;
    var $chef_believe;
    var $chef_description;
    var $chef_type;
    var $likes;
    var $bo_name;
    var $bo_tel;
    var $bo_email;
    var $mgr_name;
    var $mgr_tel;
    var $mgr_email;
    var $POS;
    var $data;
    var $images;
    var $ReviewData;
    var $MenuObj;
    var $Timeline;
    var $laptime;
    var $lastorderlunch;
    var $lastorderdiner;
    var $cutofflunch;
    var $cutoffdiner;
    var $bookinfo;
    var $bookcritical;
    var $bkgcustomcolor;
    var $where;
    var $glb_pricedesc;
    var $webrestodesc;
    var $webchefdesc;
    var $restogeneric;
    var $cn_row;
    var $msg;
    var $result;
    var $tkoutpayment;
    var $tkoutpickup;
    var $tkoutdeliver;
    var $tkouttax;
    var $tkoutminorder;
    var $tkout_tnc;
    var $bestpromotion;
    var $active_promotion;
    var $distance;
    var $internal_path;
    var $cluster;
    var $affiliate_program;
    var $booking_deposit_lunch;
    var $booking_deposit_dinner;
    var $deposit_cancel_tnc;
    var $aws_email_verification;
    var $tnc;
    var $specific_tnc;
    function __construct() {
        $this->qry = array();
        $this->where = $this->msg = "";
        $this->result = 1;
        $this->glb_pricedesc = array("$" => 20, "$$" => 40, "$$$" => 70, "$$$$" => 100, "$$$$$" => 5000);

        if (AWS) {
            $this->globalshow = __S3HOST__ . __S3DIR__;
        } else {
            $this->globalshow = __SHOWDIR__;
        }
    }

    public function setPriceSegment($currency) {
        switch ($currency) {
            default:
            case "USD":
            case "EUR":
            case "SGD":
            case "MYR":
                $this->glb_pricedesc = array("$" => 20, "$$" => 40, "$$$" => 70, "$$$$" => 100, "$$$$$" => 5000);
                return;
            case "HKD":
                $this->glb_pricedesc = array("$" => 200, "$$" => 400, "$$$" => 800, "$$$$" => 1500, "$$$$$" => 10000);
                return;
            case "THB":
                $this->glb_pricedesc = array("$" => 1000, "$$" => 2500, "$$$" => 5000, "$$$$" => 7000, "$$$$$" => 20000);
                return;
        }
    }

    function partialSave($data, $theRestaurant) {
        $Qry = "";
        reset($data);
        for ($sep = ""; list($label, $val) = each($data); $sep = ", ") {
            $val = preg_replace("/\'|\"/", "’", $val);
            $label = substr($label, 4);
            $Qry .= $sep . $label . "='" . $val . "'";
        }

        if (empty($Qry)) {
            return error_log("PARTIALSAVE: EMPTY QUERY" . print_r($data, true));
        }

        $Sql = "Update restaurant set $Qry where restaurant = '$theRestaurant' limit 1";
        pdo_exec($Sql);
    }

    function saveOpeninghours($openhours, $theRestaurant) {
        $Sql = "Update restaurant set openhours='$openhours' where restaurant = '$theRestaurant' limit 1";
        pdo_exec($Sql);
    }

    function saveBookinghours($bookhours, $theRestaurant) {
        $Sql = "Update restaurant set bookhours='$bookhours' where restaurant = '$theRestaurant' limit 1";
        pdo_exec($Sql);
    }

    function getBookWheelRestaurant($search_city = NULL, $nb_resto = 3, $in_list = array()) {
		$search_city = $this->clean_input($search_city);
    	$nb_resto = intval($nb_resto);
    	if($nb_resto < 1) 
    		$nb_resto = 3;

        $where = '';
        $limit = '';
        if (!empty($search_city)) {
            $where .= " AND city = '$search_city' ";
        }

        $limit .= ' LIMIT 0,' . $nb_resto;

        $this->restaurant = array();
        $this->cn_row = 0;
        return pdo_multiple_select("SELECT * from (SELECT restaurant, title, status, is_displayed, is_wheelable, is_bookable, extraflag, reservedflag, wheelvalue, region from restaurant WHERE is_displayed = '1' and is_wheelable = '1' and is_bookable = '1' and status != 'demo_reference' $where ORDER BY RAND() DESC $limit ) as t order by wheelvalue DESC");
    }

    //kala created fo foodselfie  restaurant
    function getContestParRestaurant($nb_resto = 4) {
        $nb_resto = 0;
        $in_array = ['SG_SG_R_Absinthe', 'SG_SG_R_GiardinoPizzaBarGrill', 'SG_SG_R_ArtistryCafe', 'SG_SG_R_Forlino', 'SG_SG_R_giardinoPizzeriaBar',
            'SG_SG_R_Match', 'SG_SG_R_ScrumptiousAtTheTurf', 'SG_SG_R_DruryLane', 'SG_SG_R_Lentrecote', 'SG_SG_R_TheLatinQuarter', 'SG_SG_R_SabioByTheSea', 'SG_SG_R_DaPaoloBistroBar', 'SG_SG_R_TheSteakHouse', 'SG_SG_R_XinYue'];

        $in_values = '"' . implode('","', $in_array) . '"';
        $limit = '';
        $limit .= ' LIMIT 0,' . $nb_resto;
        $this->restaurant = array();
        $this->cn_row = 0;

        $sql = "SELECT restaurant, title, status, is_displayed, is_wheelable, is_bookable, extraflag, reservedflag, wheelvalue, region from restaurant WHERE restaurant IN ('SG_SG_R_Shutters') 
			UNION SELECT * from (SELECT restaurant, title, status, is_displayed, is_wheelable, is_bookable, extraflag, reservedflag, wheelvalue, region from restaurant WHERE restaurant IN ($in_values)  and status != 'demo_reference'  ORDER BY RAND() DESC $limit ) as t order by wheelvalue DESC";

        return pdo_multiple_select($sql);
    }

    function getShowcaseRestaurant($search_city = NULL, $nb_resto = 3) {
    
    	$nb_resto = intval($nb_resto);
    	if($nb_resto < 1) 
    		$nb_resto = 3;
    	
        $mediadata = new WY_Media();
        //$imgdata = new WY_Images();

        $res_showcase = $this->getBookWheelRestaurant($search_city, $nb_resto);
        foreach ($res_showcase as $resto) {
            $this->getRestaurant($resto['restaurant']);
            $tmp = $resto;
            $tmp['cuisine'] = $this->cuisine;
            $tmp['description'] = $this->restaurantDescriptionToArray($this->description);

            $tmp['best_offer'] = $this->getBestOffer($resto['restaurant'], $this->is_wheelable, 1);

            $default_picture = $mediadata->getRandomPicture($resto['restaurant']);
            $tmp['image_path'] = $mediadata->getPath();
            $tmp['image'] = $mediadata->getName();

            $default_picture_tmp = explode('/', $default_picture);

            $review = new WY_Review($resto['restaurant']);
            $tmp['reviews'] = $review->getReviewsCount();

            $tmp['internal_path'] = $this->getRestaurantInternalPath();
            $tmp['pricing'] = $this->pricerating;

            $tmp['book_button'] = $this->BookButton();
            $showcase[] = $tmp;
        }
        return $showcase;
    }
    private function BookButton() {
        $booktitle = "BOOK SOON";
        if (($this->status == 'active' && $this->is_bookable) || $this->status == 'demo_reference')
            $booktitle = "BOOK NOW";
        if ($this->is_bookable == false && $this->status == 'active')
            $booktitle = "REQUEST NOW";
        return array('label' => $booktitle, 'style' => '');
    }
    function getSelfieContestShowcaseRestaurant($search_city = NULL, $nb_resto = 3) {
        $mediadata = new WY_Media();
        //$imgdata = new WY_Images();
        $res_showcase = $this->getContestParRestaurant($nb_resto);
        foreach ($res_showcase as $resto) {
            $this->getRestaurant($resto['restaurant']);
            $tmp = $resto;
            $tmp['cuisine'] = $this->cuisine;
            $tmp['description'] = $this->restaurantDescriptionToArray($this->description);

            $tmp['best_offer'] = $this->getBestOffer($resto['restaurant'], $this->is_wheelable, 1);

            $default_picture = $mediadata->getRandomPicture($resto['restaurant']);
            $tmp['image_path'] = $mediadata->getPath();
            $tmp['image'] = $mediadata->getName();

            $default_picture_tmp = explode('/', $default_picture);

            $review = new WY_Review($resto['restaurant']);
            $tmp['reviews'] = $review->getReviewsCount();

            $tmp['internal_path'] = $this->getRestaurantInternalPath();
            $tmp['pricing'] = $this->pricerating;

            $tmp['book_button'] = $this->BookButton();
            $showcase[] = $tmp;
        }

        return $showcase;
    }

    function microtime_float() {
        list($usec, $sec) = explode(" ", microtime());
        return ((float) $usec + (float) $sec);
    }

    function getTopRestaurant($search_city = NULL, $nb_resto = 3, $in_array = array()) {

        $mediadata = new WY_Media();
        if (empty($in_array)) {
            $sql = "SELECT * from restaurant_category where category ='featured' and status='active' and is_display LIKE '%HOME%' order by morder ASC limit 6 ";
            $res_category = pdo_multiple_select($sql);
            foreach ($res_category as $rest) {
                $in_array[] = $rest['restaurant'];
            }
        }

//        if(empty($in_array)){
//            $in_array = ['SG_SG_R_Skirt', 'SG_SG_R_ShinagawaRamenIzakayaRestaurant', 'SG_SG_R_TheKitchenTable', 'SG_SG_R_KinkiRestaurant', 'SG_SG_R_MitsubaJapaneseRestaurant', 'SG_SG_R_NaraJapaneseRestaurant' ];
//        }
        //$in_array=['SG_SG_R_TheFunKitchen','TH_PK_R_Vista','SG_SG_R_DruryLane']; //testing purpose only 
        $in_values = '"' . implode('","', $in_array) . '"';


        $showcase = array();
        $res_showcase = pdo_multiple_select("SELECT restaurant, title, status, is_displayed, is_wheelable, is_bookable, extraflag, reservedflag, wheelvalue, region from restaurant WHERE restaurant IN ($in_values) ORDER BY RAND()");
        foreach ($res_showcase as $resto) {
            $this->getRestaurant($resto['restaurant']);
            $tmp = $resto;
            $tmp['cuisine'] = $this->cuisine;

            $tmp['best_offer'] = $this->getBestOffer($resto['restaurant'], $this->is_wheelable, 1);

            $default_picture = $mediadata->getRandomPicture($resto['restaurant']);
            $tmp['image_path'] = $mediadata->getPath();
            $tmp['image'] = $mediadata->getName();

            $review = new WY_Review($resto['restaurant']);
            $tmp['reviews'] = $review->getReviewsCount();
            $tmp['internal_path'] = $this->getRestaurantInternalPath();
            $tmp['pricing'] = $this->pricerating;

            $tmp['book_button'] = $this->BookButton();
            $showcase[] = $tmp;
        }

        return $showcase;
    }

    function getRestaurantByCluster($parent_name, $cluster_name) {
        $cluster = new WY_Cluster();
        $cluster->getListSlaveCluster($parent_name, $cluster_name);
        foreach ($cluster->full_object as $c) {
            $restaurants[] = $c['clustname'];
        }
        return $this->getTopRestaurant('', count($restaurants), $restaurants);
    }

    function getRestaurantByTag($tag) {
        $sql = "SELECT restaurant FROM restaurant WHERE tags LIKE '%[$tag]%'";
        $data = pdo_multiple_select($sql);
        $restaurants = array();
        if (count($data) < 1) {
            return false;
        }
        foreach ($data as $d) {
            $restaurants[] = $d['restaurant'];
        }
        if (count($restaurants) < 1) {
            return false;
        }
        return $this->getTopRestaurant('', count($restaurants), $restaurants);
    }

    function getBlogArticles($search_city = NULL, $limit = 3) {
            ini_set('default_charset', 'utf-8');
        $sql = "SELECT p1.ID,p1.post_title,p1.post_status,p1.post_date,p1.post_content,p1.guid,wm2.meta_value FROM wy_posts 
                p1 LEFT JOIN wy_postmeta wm1 ON ( wm1.post_id = p1.ID AND wm1.meta_value IS NOT NULL AND wm1.meta_key = '_thumbnail_id' ) 
                LEFT JOIN wy_postmeta wm2 ON ( wm1.meta_value = wm2.post_id AND wm2.meta_key = 'amazonS3_info' AND wm2.meta_value IS NOT NULL ) 
                WHERE p1.post_status='publish' AND post_type = 'post' ORDER BY p1.post_date DESC Limit 3";

        $article = pdo_multiple_select($sql, 'blog');
        $identifier = 'x';
        $blog_article = array();
        foreach ($article as $ba) {
            $meta = unserialize($ba['meta_value']);
            $ba['imageUrl'] = $meta['key'];
            if (!empty($ba['imageUrl'])) {
                $filePath = pathinfo($ba['imageUrl']);
                $ba['imageUrl'] = $filePath['dirname'] . '/' . $filePath['filename'] . '-356' . $identifier . '220' . '.' . $filePath['extension'];
            }
            if (strlen($ba['post_content']) > 50)
                $ba['description'] = substr($ba['post_content'], 0, 50) . '...';
                $ba['description'] = preg_replace("/’/", "\"", $ba['description']);  //utf8_encode($ba['description']);
                $ba['description']  = preg_replace('/[^a-z0-9$¢£€¥ ]+/ui', ' ', $ba['description']);

            $blog_article[] = $ba;
        }

        return $blog_article;
    }

    function getGourmandRestaurants($country, $nb) {
        $resultat = array();

        $sql = "SELECT restaurant, title, cuisine, wheel, region, status, is_displayed, is_wheelable, is_bookable, extraflag, reservedflag, GPS from restaurant WHERE is_displayed = '1' "
                . "and is_wheelable = '1' and is_bookable = '1' "
                . "and status != 'demo_reference' "
                . "and country_iso_code = '$country' order by RAND() LIMIT $nb";

        $data = pdo_multiple_select($sql);
        if (count($data) <= 0)
            return array();

        foreach ($data as $row) {
            $res = array();
            $res['restaurant'] = $row['restaurant'];
            $res['name'] = $row['title'];
            $res['cuisine'] = $row['cuisine'];
            $res['region'] = $row['region'];
            $coord = explode(',', $row['GPS']);
            $res['lat'] = $coord[0];
            $res['lon'] = $coord[1];
            //$res['wheel'] = $row['wheel'];
            $res['best_offer'] = $this->getBestOffer($row['restaurant'], $row['is_wheelable'], 1);
            $resultat[] = $res;
        }
        return $resultat;
    }
    function readAllNames() {
        $this->result = 1;
        $this->msg = "";
        $data = pdo_multiple_select("SELECT restaurant, title from restaurant ORDER BY restaurant");
        if (count($data) > 0)
            return $data;
        $this->result = -1;
        return array();
    }
    static function isrestaurant($theRestaurant) {
        $theRestaurant = clean_input($theRestaurant);
        $data = pdo_single_select("SELECT title FROM restaurant WHERE restaurant ='$theRestaurant' limit 1");
        return (count($data) > 0);
    }
    static function getTitle($theRestaurant) {
        $theRestaurant = clean_input($theRestaurant);

        $data = pdo_single_select("SELECT title FROM restaurant WHERE restaurant ='$theRestaurant' limit 1");
        return (count($data) > 0) ? $data['title'] : "";
    }
    function buildRestaurantData(&$data) {
        $theRestaurant = $this->clean_input($data['restaurant']);
        //error_log(__FILE__." ".__FUNCTION__." ".__LINE__." processing ".$theRestaurant);
        $this->result = 1;
        $this->ID = $data['ID'];
        $this->restaurant = $data['restaurant'];
        $this->title = $data['title'];
        $this->logo = $data['logo'];
        $this->cuisine = $data['cuisine'];
        $this->wheel = $data['wheel'];
        $this->wheelversion = $data['wheelversion'];
        $this->chef = $data['chef'];
        $this->chef_award = $data['chef_award'];
        $this->chef_logo = $data['chef_logo'];
        $this->chef_origin = $data['chef_origin'];
        $this->chef_believe = $data['chef_believe'];
        $this->chef_type = $data['chef_type'];
        $this->chef_description = $data['chef_description'];
        $this->description = $data['description'];
        $this->address = $data['address'];
        $this->address1 = $data['address1'];
        $this->city = $data['city'];
        $this->country = $data['country'];
        $this->zip = $data['zip'];
        $this->region = $data['region'];
        $this->tel = $data['tel'];
        $this->fax = $data['fax'];
        $this->email = $data['email'];
        $this->smsid = $data['smsid'];
        $this->url = $data['url'];
        $this->map = $data['map'];
        $this->rating = $data['rating'];
        $this->creditcard = $data['creditcard'];
        $this->mealslot = $data['mealslot'];
        $this->dfmealtype = $data['dfmealtype'];
        $this->dfminpers = $data['dfminpers'];
        $this->dfmaxpers = $data['dfmaxpers'];
        $this->dfproduct = $data['dfproduct'];
        $this->GPS = $data['GPS'];
        $this->likes = $data['likes'];
        $this->award = $data['award'];
        $this->images = $data['images'];
        $this->pricing = $data['pricing'];
        $this->currency = $data['currency'];

        $this->setPriceSegment($this->currency);
        $this->pricerating = $data['pricerating'] = $this->PricingDollars($this->PriceMeal($data['pricing'], 3));
        $this->pricediner = $data['pricediner'] = (preg_match('/Dinner/', $data['mealtype'])) ? $this->PricingDollars($this->PriceMeal($data['pricing'], 1)) : "";
        $this->pricelunch = $data['pricelunch'] = (preg_match('/Lunch/', $data['mealtype'])) ? $this->PricingDollars($this->PriceMeal($data['pricing'], 2)) : "";
        $this->affiliate_program = $data['affiliate_program'] = ($data['extraflag'] & CORPORATEWHEEL) ? "cpp_credit_suisse" : 0;
        $this->internal_path = $data['internal_path'] = $this->getRestaurantInternalPath();

        $this->rating = $data['rating'];
        $this->stars = $data['stars'];
        $this->status = $data['status'];
        $this->is_displayed = $data['is_displayed'];
        $this->is_wheelable = $data['is_wheelable'];
        $this->is_bookable = $data['is_bookable'];
        $this->extraflag = $data['extraflag'];
        $this->reservedflag = $data['reservedflag'];
        $this->openhours = $data['openhours'];
        $this->bookhours = $data['bookhours'];
        $this->bookwindow = $data['bookwindow'];
        $this->restaurant_tnc = $data['restaurant_tnc'];
        $this->mealtype = $data['mealtype'];
        $this->wheelvalue = $data['wheelvalue'];
        $this->wheelsponsor = $data['wheelsponsor'];
        $this->wheelRescode = $data['wheelRescode'];
        $this->bo_name = $data['bo_name'];
        $this->bo_tel = $data['bo_tel'];
        $this->bo_email = $data['bo_email'];
        $this->mgr_name = $data['mgr_name'];
        $this->mgr_tel = $data['mgr_tel'];
        $this->mgr_email = $data['mgr_email'];
        $this->POS = $data['POS'];
        $this->laptime = $data['laptime'];
        $this->lastorderlunch = $data['lastorderlunch'];
        $this->lastorderdiner = $data['lastorderdiner'];
        $this->cutofflunch = $data['cutofflunch'];
        $this->cutoffdiner = $data['cutoffdiner'];
        $this->bookinfo = $data['bookinfo'];
        $this->bookcritical = $data['bookcritical'];

        $this->deposit_cancel_tnc = $data['deposit_cancel_tnc'];

        $this->tkoutpayment = $data['tkoutpayment'];
        $this->tkoutpickup = $data['tkoutpickup'];
        $this->tkoutdeliver = $data['tkoutdeliver'];
        $this->tkouttax = $data['tkouttax'];
        $this->tkoutminorder = $data['tkoutminorder'];
        $this->tkout_tnc = $data['tkout_tnc'];
        $this->bkgcustomcolor = $data['bkgcustomcolor'];

        $this->booking_deposit_lunch = $data['booking_deposit_lunch'];
        $this->booking_deposit_dinner = $data['booking_deposit_dinner'];

        $this->webrestodesc = $data['webrestodesc'];
        $this->webchefdesc = $data['webchefdesc'];
        $this->restogeneric = $data['restogeneric'];

        $this->cn_row = 1;

        $this->dirname = __UPLOADDIR__ . "$theRestaurant/";
        $this->showdirname = __SHOWDIR__ . "$theRestaurant/";
        $this->data = $data;
        $this->aws_email_verification = $data['aws_email_verification'];
    }
    
    function getRestaurant($theRestaurant) {
        $theRestaurant = $this->clean_input($theRestaurant);
        $this->ID = $this->restaurant = "";
        $this->result = -1;
        $sql = "SELECT ID, restaurant, title, hotelname, cuisine, wheel, "
                . "wheelversion, chef, chef_type, chef_award, chef_logo, chef_description, "
                . "chef_origin, chef_believe, logo, url, description, "
                . "address,address1, city, zip, country, tel, fax, email, smsid, map, "
                . "region, rating, creditcard, mealslot, award, GPS, likes, bo_name, bo_tel, bo_email, mgr_name, mgr_tel,mgr_tel,mgr_email, POS, images, "
                . "pricing, currency, rating, stars, openhours, bookhours, bookwindow, restaurant_tnc, mealtype, "
                . "dfmealtype, dfminpers, dfmaxpers, dfproduct, wheelvalue, wheelsponsor, wheelRescode, status, is_displayed, "
                . "is_wheelable, is_bookable, offer_type, extraflag, reservedflag, laptime, lastorderlunch, lastorderdiner, cutofflunch, cutoffdiner, bookinfo, bookcritical, deposit_cancel_tnc,"
                . "tkoutpayment, tkoutpickup, tkoutdeliver, tkouttax, tkoutminorder, tkout_tnc, tags, bkgcustomcolor, booking_deposit_lunch, booking_deposit_dinner, deposit_cancel_tnc, facebook_pid, webrestodesc, webchefdesc, restogeneric,aws_email_verification from restaurant WHERE restaurant = '$theRestaurant' LIMIT 1";
        $data = pdo_single_select($sql);
        if (count($data) <= 0) {
            error_log(__FILE__." ".__FUNCTION__." ".__LINE__." restaurant: ".$theRestaurant . " " . count($data));
            return array();
        }
        $this->buildRestaurantData($data);
        return $data;
    }
    function getEventTermsAndConditions($restaurant) {
        $theRestaurant = $this->clean_input($theRestaurant);
        $this->ID = $this->restaurant = "";
        $this->result = -1;
        $sql = "SELECT * from event_tnc WHERE restaurant = '$restaurant' LIMIT 1";
        $data = pdo_single_select($sql);
        if (count($data) <= 0) {
            error_log(__FILE__." ".__FUNCTION__." ".__LINE__." restaurant: ".$restaurant . " " . count($data));
            return array();
        }
        $this->tnc = $data['tnc'];
        $this->specific_tnc = $data['specific_tnc'];
        //error_log(__FILE__." ".__FUNCTION__." ".__LINE__." data: ".print_r($data, true));
        return $data;        
    }
	function updateGeneric($restaurant, $label, $data) {
	
		$this->result = 1;
        $restaurant = $this->clean_input($restaurant);
		if($this->restaurant != $restaurant)
			$this->getRestaurant($restaurant);
		
		if($this->result < 0)
			return;
			
		if($this->restogeneric === '')
			$this->restogeneric = "{’" . $label . "’:’" . $data . "’}";
		else {
			$generic = preg_replace("/’/", "\"", $this->restogeneric);
			$obj = json_decode($generic, true);
			$obj[$label] = $data;
			$generic = json_encode($obj);
			$this->restogeneric = preg_replace("/\"/", "’", $generic);
			}
        pdo_exec("Update restaurant set restogeneric='$this->restogeneric' where restaurant = '$restaurant' limit 1");
        return;
		}

	function readGeneric($label) {
		if($this->restogeneric === '')
			return "";
		$generic = preg_replace("/’/", "\"", $this->restogeneric);
		$obj = json_decode($generic, true);
		return (isset($obj[$label])) ? $obj[$label] : "";
		}
		
    function externalWebsiteInfo($theRestaurant) {

        $theRestaurant = $this->clean_input($theRestaurant);

//      $data = $this->getRestaurant($theRestaurant);

        $where = "WHERE restaurant = '$theRestaurant'";

        $this->ID = $this->restaurant = "";
        $this->result = -1;

        $sql = "SELECT webrestodesc, webrestodesc from restaurant $where LIMIT 1";

        $data = pdo_single_select($sql);

        if (count($data) <= 0) {
            error_log(__FILE__." ".__FUNCTION__." ".__LINE__." restaurant: ".$theRestaurant . " " . count($data));
            return array();
        }

        $chef = $data['webrestodesc'];
        $description = $this->restaurantDescriptionToArray($data['webrestodesc']);

        $resultat = array('chef' => $chef, 'description' => $description);

        $this->result = 1;
        return $resultat;
    }

    function rename($oldname, $newname) {

        $newname = preg_replace("/[^a-zA-Z0-9_]/", "", $newname);
        $oldname = preg_replace("/[^a-zA-Z0-9_]/", "", $oldname);

        if (strlen($newname) < 8 || strlen($oldname) < 8) {
            $this->msg = "empty names or too short";
            return -1;
        }
        $data = pdo_single_select("SELECT ID from restaurant where restaurant = '$oldname' limit 1");
        if (count($data) <= 0) {
            $this->msg = "the restaurant '$oldname' does not exist";
            return DOESNOT_EXISTS;
        }

        $data = pdo_single_select("SELECT ID from restaurant where restaurant = '$newname' limit 1");
        if (count($data) > 0) {
            $this->msg = "the restaurant '$newname' already exists";
            return ALREADY_EXISTS;
        }

        $tables = list_table_names();
        foreach ($tables as $tb) {

            $data = list_table_field($tb);
            if (in_array("restaurant", $data))
                pdo_exec("Update $tb set restaurant='$newname' where restaurant = '$oldname'");
            else if (in_array("restaurant_id", $data))
                pdo_exec("Update $tb set restaurant_id='$newname' where restaurant_id = '$oldname'");
        }

        // rename directory image ******
        $images = new WY_Images($oldname);
        $images->l_copy_restaurant_directory($oldname, $newname);
        //delete
        $images->l_delete_restaurant_directory($oldname);
        return 1;
    }

    function delete($name) {

        $name = preg_replace("/[^a-zA-Z0-9_]/", "", $name);

        if (strlen($name) < 8) {
            $this->msg = "empty names or too short";
            return -1;
        }

        $data = pdo_single_select("SELECT ID from restaurant where restaurant = '$name' limit 1");
        if (count($data) <= 0) {
            $this->msg = "the restaurant '$oldname' does not exist";
            return DOESNOT_EXISTS;
        }

        $tables = list_table_names();
        foreach ($tables as $tb) {

            $data = list_table_field($tb);
            if (in_array("restaurant", $data))
                pdo_exec("delete from $tb where restaurant = '$name'");
            else if (in_array("restaurant_id", $data))
                pdo_exec("delete from $tb where restaurant_id = '$name'");
        }

        // delete directory images ******
        $images = new WY_Images($name);
        $images->l_delete_restaurant_directory($name);

        return 1;
    }

    function getBkgohbs($restaurant) {

        $this->result = 1;
        $this->getRestaurant($restaurant);
        if ($this->result < 0)
            return "";

        return $this->bookhours;
    }

    function updateBkgohbs($restaurant, $bookhours) {

        if (substr($bookhours, 0, 6) != "BYSLOT") {
            $this->msg = "invalid format " . $bookhours;
            return $this->result = -1;
        }

        $this->result = 1;
        $this->getRestaurant($restaurant);
        if ($this->result < 0)
            return $this->result = -1;

        pdo_exec("update restaurant set bookhours='$bookhours' where restaurant = '$restaurant' limit 1");
        return $this->result;
    }
    function getActiveEvents($restaurant) {
        if (isset($restaurant) && !empty($restaurant)) {
            $sql = "SELECT e.ID, e.name, e.title, e.morder, e.city, e.country, e.start, e.end, e.description, e.picture, e.is_homepage,e.display, e.restaurant, r.title as restaurant_name FROM event e, restaurant r WHERE r.restaurant = '$restaurant' AND e.restaurant = r.restaurant AND e.display <= CURRENT_DATE() AND e.end >= CURRENT_DATE() AND e.type='private' ORDER BY RAND() Limit 4";
            $events = pdo_multiple_select($sql);
            error_log(__FILE__." ".__FUNCTION__." ".__LINE__.": ".count($events)." events");
            $active_event = array();
            foreach ($events as $ba) {
                if (strlen($ba['description']) > 100) {
                    $ba['description'] = substr($ba['description'], 0, 100) . '...';
                }
                $ba['internal_path'] = $this->getRestaurantInternalPath($ba['restaurant']);

                $active_event[] = $ba;
            }
            return $active_event;
        }
        return [];
    }
    function getHomeActiveEvents() {
        $sql = "SELECT e.ID, e.name, e.title, e.morder, e.city, e.country, e.start, e.end, e.description, e.picture, e.is_homepage,e.display, e.restaurant, r.title as restaurant_name FROM event e, restaurant r WHERE e.restaurant = r.restaurant AND e.display <= CURRENT_DATE() AND e.end >= CURRENT_DATE() AND e.is_homepage = 1 AND r.status='active' ORDER BY RAND() Limit 4";
        $events = pdo_multiple_select($sql);
        $active_event = array();
        foreach ($events as $ba) {
            if (strlen($ba['description']) > 100) {
                $ba['description'] = substr($ba['description'], 0, 100) . '...';
            }
            $ba['internal_path'] = $this->getRestaurantInternalPath($ba['restaurant']);

            $active_event[] = $ba;
        }

        return $active_event;
    }

    function duplicate($name, $newname) {

        $newname = preg_replace("/[^a-zA-Z0-9_]/", "", $newname);
        $name = preg_replace("/[^a-zA-Z0-9_]/", "", $name);

        if (strlen($newname) < 8 || strlen($name) < 8) {
            $this->msg = "empty names or too short";
            return -1;
        }

        $data = pdo_single_select("SELECT ID from restaurant where restaurant = '$name' limit 1");
        if (count($data) <= 0) {
            $this->msg = "the restaurant '$oldname' does not exist";
            return DOESNOT_EXISTS;
        }


        $data = pdo_single_select("SELECT ID from restaurant where restaurant = '$newname' limit 1");
        if (count($data) > 0) {
            $this->msg = "the restaurant '$newname' already exists";
            return ALREADY_EXISTS;
        }


        $tables = list_table_names();
        $found = 0;
        foreach ($tables as $tb) {

            //if($tb == "catering" || $tb == "catering_item") continue;
            if ($tb == "booking" || $tb == "review")
                continue;

            $data = list_table_field($tb);
            $case = "";
            if (($cn = array_search('ID', $data)) !== false)
                array_splice($data, $cn, 1);
            if (($cn = array_search('id', $data)) !== false)
                array_splice($data, $cn, 1);
            if (($cn = array_search('restaurant', $data)) !== false) {
                $case = "restaurant";
                array_splice($data, $cn, 1);
            }
            if (($cn = array_search('restaurant_id', $data)) !== false) {
                $case = "restaurant_id";
                array_splice($data, $cn, 1);
            }

            if ($case != "") {
                $variable = implode(",", $data);
                pdo_exec("insert into $tb ($case, $variable) select '$newname', $variable from $tb where $case = '$name'");
            }
            if ($tb == "menu" || $tb == "menu_categorie")
                $found++;
            if ($found == 2) {
                $found = 99;
                WY_debug::recordDebug("DUPLICATE-API", "RESTAURANT", "MENU -> " . $newname);
                WY_Menu::resetfullMenusID($newname);
            }
        }
        // duplicate directory images ******
        $images = new WY_Images($oldname);
        $images->l_copy_restaurant_directory($name, $newname);
        pdo_exec("update restaurant set title='$newname' where restaurant = '$newname' limit 1");

        return 1;
    }

    function reactivate($theRestaurant) {
        return pdo_exec("UPDATE restaurant SET status = '' where restaurant = '$theRestaurant' limit 1");
    }

    function deactivate($theRestaurant) {
        return pdo_exec("UPDATE restaurant SET status = 'deleted' where restaurant = '$theRestaurant' limit 1");
    }

    function getBestOffer($theRestaurant = NULL, $is_wheelable = NULL, $nb_offers = 1, $affiliate_program = NULL) {
        if (empty($theRestaurant)) {
            $theRestaurant = $this->restaurant;
            $affiliate_program = $this->affiliate_program;
        }
        //error_log(__FILE__." ".__FUNCTION__." ".__LINE__." restaurant: ".$theRestaurant." affiliate_program: ".$affiliate_program." is_wheelable: ".$is_wheelable." nb_offers: ".$nb_offers);
//////  PROMOTION SECTION
        return (empty($is_wheelable) || $is_wheelable == 0) ? $this->getActivePromotion($theRestaurant, $affiliate_program): WY_wheel::getBestOffer($theRestaurant, $nb_offers, $affiliate_program);
    }

    /*     * ********** home categories ******************** */
    function homecategories($title, $tag, $link, $description, $images, $city, $is_mobile, $status, $morder, $type = 'home') {
        $sql = "INSERT INTO home_categories (title,tag,description,link,images,status,city,is_mobile,morder,type) VALUES ('$title','$tag','$description','$link','$images','$status','$city','$is_mobile','$morder','$type')";
        if (pdo_exec($sql) <= 0)
            error_log(__FILE__." ".__FUNCTION__." ".__LINE__.": Failed to insert record!");
        return 1;
    }
    function CreateOrUpdateEvent($data, $file) {
        if (isset($file) && count($file)) {
            $media = new WY_Media($data['restaurant']);
            $media->upload($file['tmp_name'][0], basename($file['name'][0]));
            if ($media->addImg($data['restaurant'], $file['name'][0], $data['category'], $data['type'], null, 'picture', $data['tag']))
                error_log(__FILE__." ".__FUNCTION__." ".__LINE__.": Successfully add and upload image ".$file['name'][0]."!");
            else {
                error_log(__FILE__." ".__FUNCTION__." ".__LINE__.": Failed add image ".$file['name'][0]."!");
                return false;
            }
        } else
            error_log(__FILE__." ".__FUNCTION__." ".__LINE__.": Not file to upload!");
        if (isset($data['type']) && !empty($data['type']))
        try {
            $title = $data['title'];
            $tag = $data['tag'];
            $description = $data['description'];
            $link = $data['link'];
            $image = $data['images']['name'];
            $status = $data['status'];
            $city = $data['city'];
            $type = $data['type'];
//            $sql_check = "SELECT ID FROM home_categories WHERE type = '$type' LIMIT 1";
            $sql_ins = "INSERT INTO home_categories (title,tag,description,link,images,status,city,type) VALUES ('$title','$tag','$description','$link','$image','$status','$city','$type')";
            $result = pdo_insert($sql_ins);
            if ($result == 0)
                error_log(__FILE__." ".__FUNCTION__." ".__LINE__." New event saved");
            else {
                error_log(__FILE__." ".__FUNCTION__." ".__LINE__." Update existing event ".$type);
                $sql = "UPDATE home_categories SET title = :title, tag = :tag, description = :description, link=:link, images=:image, status=:status, city=:city, type=:type WHERE ID = :ID";
                $parameters = array('ID' => $result, 'title' => $title, 'tag' => $tag, 'link' => $link, 'description' => $description, 'image' => $image, 'city' => $city, 'status' => $status, 'type' => $type);
                pdo_update($sql, $parameters);
            }
        } catch(PDOException $e) {
            error_log(__FILE__." ".__FUNCTION__." ".__LINE__." PDOException: ".$e);
        }
    }
    function updatecategories($title, $tag, $link, $description, $images, $id, $city, $is_mobile, $status, $morder, $type = '') {

        $sql = "SELECT tag from home_categories where id='$id'";
        $res = pdo_single_select($sql);
        $tags = '[' . $res['tag'] . ']';

        $sql = "UPDATE home_categories SET title ='$title',tag='$tag',description='$description',link='$link',images='$images',status='$status',city='$city',is_mobile='$is_mobile',morder='$morder',type='$type' where id='$id'";
        pdo_exec($sql);
        if ($type === 'admin') {
            $data = pdo_multiple_select("SELECT restaurant,tags FROM restaurant where tags LIKE '%$tags%' ORDER BY restaurant ASC ");
            if (count($data) > 0) {
                foreach ($data as $row) {
                    $row['tags'] = str_replace($tags, "", $row['tags']);
                    if (strpos($row['tags'], $tag) !== false) {
                        $tags = $row['tags'];
                    } else {
                        $tags = $row['tags'] . '[' . $tag . ']';
                    }
                    $res = $row['restaurant'];
                    $sql = "UPDATE restaurant SET tags = '$tags' WHERE restaurant='$res'";
                    pdo_exec($sql);
                }
            }
        }
        return 1;
    }

    function deletecategories($id) {
        $status = 'inactive';
        $sql = "UPDATE home_categories SET status='$status' where id='$id'";
        pdo_exec($sql);
        return 1;
    }

    function getCategories($type) {
        $status = 'active';
        $limit = '';
        $mode = $type;
        if($type == 'home')
            $mode = $type;
        $where = "WHERE is_mobile = 0 AND status = '$status' AND type = '$mode'";
        if($type == 'backoffice'){
            $in_array =['bo-latestnews','bo-banner'];
            $mode = '"' . implode('","', $in_array) . '"';
            $sql = "SELECT * from home_categories WHERE status = '$status' AND type IN ($mode) ORDER BY morder ASC  ";
        } else if ($type === 'admin')
            $sql = "SELECT * from home_categories  ";
        else if ($type === 'footer')
            $sql = "SELECT * from home_categories  $where ORDER BY morder ASC limit 8 ";
        else if (preg_match("/^event_/", $type))
            $sql = "SELECT * from home_categories WHERE status='$status' AND type LIKE 'event_%'";
        else
            $sql = "SELECT * from home_categories $where ORDER BY morder ASC limit 4 ";
        $data = pdo_multiple_select($sql);
        error_log("QUERY " . $sql);
        //error_log(__FILE__." ".__FUNCTION__." ".__LINE__.": data: ".print_r($data, true));
        if (!preg_match("/^event_/", $type) && count($data) > 0) {
            $results = [];
            try {
            foreach ($data as $d) {
                if (isset($d['tag']) && $d['tag'] !== '') {
                    $tag = '[' . $d['tag'] . ']';
                    $sql = "SELECT count(*) as count,tags from restaurant where tags LIKE '%$tag%' ";
                    $res = pdo_multiple_select($sql);
                    //error_log(__FILE__." ".__FUNCTION__." ".__LINE__." res: ".print_r($res, true));
                    $d['restCount'] = $res[0]['count'];
                    $d['tags'] = $res[0]['tags'];
                }
                $data = @unserialize($d['images']);
                $d['images'] = ($data !== false || $d['images'] === 'b:0;') ? unserialize($d['images']) : $d['images'];
                //$results[] = $d;
                array_push($results, $d);
                //error_log(__FILE__." ".__FUNCTION__." ".__LINE__." ".count($results)." results: ");//.print_r($results, true));                
            }
            error_log(__FILE__." ".__FUNCTION__." ".__LINE__." ".count($results)." results: ");//.print_r($results, true)); 
            return $results;
            } catch (Exception $e) {
                error_log(__FILE__." ".__FUNCTION__." ".__LINE__." Exception: ".$e);
            }
        }
        error_log(__FILE__." ".__FUNCTION__." ".__LINE__." ".count($data)." data");//.print_r($data, true));
        return $data;
    }

    function getCategoriesMobile() {
        $status = 'active';
        $limit = '';
        $sql = "SELECT * from home_categories where status = '$status' AND is_mobile=1 ORDER BY morder ASC";
        $data = pdo_multiple_select($sql);
        if (count($data) > 0) {
            foreach ($data as $d) {
                $d['images'] = unserialize($d['images']);
                $resultat[] = $d;
            }
            return $resultat;
        } else {
            return $resultat = array();
        }
    }

    function getWheelButton($theRestaurant, $title, $imgdata, $width, $force_reload, $whtoken) {
        $dd = rand(100, 333);
        $dd = ($dd * 3) + 1;
        if ($dd % 2 != 0) {
            $dd += 2;
        }
        // 3 chars => not dividable by 2 nor 3
        $shwwhl = rand(10000, 99999) . $dd . rand(0, 9);
        $WheelImg = "<img src='" . $imgdata->getWheelSource($this->wheelvalue) . $force_reload . "' $width alt='$title'  />";
        return "<a rel='nofollow' href='wheel/rotation/rotation.php?restaurant=$theRestaurant&shwwhl=$shwwhl&whtoken=$whtoken' target='_blank'>$WheelImg</a>";
    }

    function getWheelButtonLink($theRestaurant) {
        $dd = rand(100, 333);
        $dd = ($dd * 3) + 1;
        if ($dd % 2 != 0)
            $dd +=2;  // 3 chars => not dividable by 2 nor 3                                                                                                                                
        $shwwhl = rand(10000, 99999) . $dd . rand(0, 9);

        return "index.php?page=wheel_details&restaurantname=$theRestaurant&shwwhl=$shwwhl";
    }

    function getBookButton($theRestaurant, $title, $label, $style = '', $class = '', $path_adaptor = '') {

        switch ($class) {
            case '':
                $res = "<button type='button' id='btn_book' class='btn custom_button' $style onclick=\"WindowOpen('" . $path_adaptor . "modules/booking/book_form.php?bkrestaurant=" . $theRestaurant . "&bktitle=" . $title . "');\">$label</button>";
                break;

            case 'btn-green':
                $res = "<button type='button' id='btn_book' class='btn btn-green' $style onclick=\"WindowOpen('" . $path_adaptor . "modules/booking/book_form.php?action=request&bkrestaurant=" . $theRestaurant . "&bktitle=" . $title . "');\">$label</button>";
                break;

            case 'not-visible':
                $res = "<button style=' visibility: hidden;' type='button' id='btn_book' class='btn btn-green' $style \">$label</button>";

                break;
            default:
                $res = "<button type='button' id='btn_book' class='btn $class' $style >$label</button>";
                break;
        }
        return $res;

        /* 	$mobiletype = (isset($_SESSION['mobiletype']) && $_SESSION['mobiletype']);

          if($class != ""){
          $bk = "<button type='button' id='btn_book' class='btn $class' $style onclick='');\">$label</button>";
          }else{
          if(!$mobiletype) $bk = "<button type='button' id='btn_book' class='btn custom_button' $style onclick=\"WindowOpen('modules/booking/book_form.php?bkrestaurant=" . $theRestaurant . "&bktitle=" . $title . "');\">$label</button>";
          else $bk = "<a href='modules/booking/book_form.php?bkrestaurant=" . $theRestaurant . "&bktitle=" . $title . "' target='_blank' id='btn_book' class='btn custom_button' $style>$label.</a>";
          }
          return $bk;

         */
    }

    function getBookButtonActionLink($theRestaurant, $title, $path_adaptor = '') {

        switch ($class) {
            case '':
                $res = $path_adaptor . "modules/booking/book_form.php?bkrestaurant=" . $theRestaurant . "&bktitle=" . $title;
                break;

            case 'btn-green':
                $res = $path_adaptor . "modules/booking/book_form.php?action=request&bkrestaurant=" . $theRestaurant . "&bktitle=" . $title;
                break;

            case 'not-visible':
                $res = "";

                break;
            default:
                $res = "";
                break;
        }
        return $res;
    }

    function saveTakeout($tkoutpayment, $tkoutpickup, $tkoutdeliver, $tkouttax, $tkoutminorder, $tkout_tnc, $theRestaurant) {
        $tkoutpayment = $this->clean_input($tkoutpayment);
        $tkoutpickup = $this->clean_input($tkoutpickup);
        $tkoutdeliver = $this->clean_input($tkoutdeliver);
        $tkouttax = $this->clean_input($tkouttax);
        $tkoutminorder = $this->clean_input($tkoutminorder);
        $tkout_tnc = $this->clean_input($tkout_tnc);

        pdo_exec("UPDATE restaurant SET tkoutpayment = '$tkoutpayment', tkoutpickup = '$tkoutpickup', tkoutdeliver = '$tkoutdeliver', tkouttax = '$tkouttax', tkoutminorder = '$tkoutminorder', tkout_tnc = '$tkout_tnc' where restaurant = '$theRestaurant' limit 1");
    }
    function saveEventTermsAndConditions($restaurant, $tnc, $stnc) {
        $sql_check = "SELECT ID FROM event_tnc WHERE restaurant = '$restaurant' LIMIT 1";
        $sql_ins = "INSERT INTO event_tnc (restaurant, tnc, specific_tnc) VALUES ('$restaurant', '$tnc', '$stnc')";
        $result = pdo_insert_unique($sql_check, $sql_ins);
        if ($result == 0){
            $this->msg = "Restaurant: ".$restaurant." new tnc saved successfully!";
        }else {
            $this->msg = "Updating restaurant " . $restaurant;
            
            $sql = "UPDATE event_tnc SET tnc = :tnc, specific_tnc = :stnc WHERE ID = :ID";
            $parameters = array('tnc' => $tnc, 'stnc' => $stnc, 'ID' => $result);
            pdo_update($sql, $parameters);
        }

    }
    function getRestMulticontact() {
        if (!$this->smsNotifToRestaurant()) {
            return array();
        }
        $sql = "SELECT * from restaurant_contacts where restaurant = '$this->restaurant' AND notify_sms = 1 ";  //
        $data = pdo_multiple_select($sql);
        return $data;
    }
    function getRestaurantDetails($theRestaurant) {
        //error_log(__FILE__." ".__FUNCTION__." ".__LINE__." restaurant: ".$theRestaurant);
        $theRestaurant = clean_input($theRestaurant );
        $media = new WY_Media();
        $images = array();
        $menu = array();
        try {
            $restaurant = $this->getRestaurant($theRestaurant);
            if(!isset($restaurant) || empty($restaurant)) {
                echo format_api(-1, null, 0, "unable to retrieve " . $theRestaurant);
                return;
            }
                
            $restaurant['dinesfree'] = $restaurant['tnc'] = "";
            $restaurant['minonlineorder'] = $restaurant['tkoutminorder'];
            $restaurant['onlineordertnc'] = $restaurant['tkout_tnc'];
            $restaurant['onlineordertax'] = $restaurant['tkouttax'];
            $restaurant['booking_tnc'] = $restaurant['bookinfo'];
            $restaurant['booking_critical_info'] = $restaurant['bookcritical'];
            
            //description
            $restaurant['description'] = $this->restaurantDescriptionToArray($restaurant['description']);
            //booking deposit 
            $booking_deposit = NULL;
            if($restaurant['booking_deposit_lunch'] > 0)
                $booking_deposit['lunch'] = $restaurant['booking_deposit_lunch'];
            if($restaurant['booking_deposit_dinner'] > 0)
                $booking_deposit['dinner'] = $restaurant['booking_deposit_dinner'];
            $restaurant['booking_deposit'] = $booking_deposit;
            unset($restaurant['booking_deposit_lunch']);
            unset($restaurant['booking_deposit_dinner']);
            $chef = array('chef_name' => $restaurant['chef'],
                'chef_award' => $restaurant['chef_award'],
                'chef_origin' => $restaurant['chef_origin'],
                'chef_description' => $restaurant['chef_description'],
                'chef_believe' => $restaurant['chef_believe'],
                'chef_type' => $restaurant['chef_type'],
                'chef_image' => $restaurant['chef_logo']
                );
            unset($restaurant['chef']);
            unset($restaurant['chef_award']);
            unset($restaurant['chef_origin']);
            unset($restaurant['chef_description']);
            unset($restaurant['chef_believe']);
            unset($restaurant['chef_type']);
            unset($restaurant['chef_logo']);
            $restaurant['chef'] = $chef;
            //images
            $img_array = $media->getRestaurantGallery($restaurant['restaurant']);
            $i=0;
            $pictures=array();
            foreach ($img_array as $img) {
                $i=$i+1;
                $images[] = $img->getName();
                $des =$img->getDescription();
                $tag = $des!=="" ? $des : "picture $i";
                $obj=array(
                    'name'=>$img->getName(),
                    'tag'=>$tag,
                );
                array_push($pictures,$obj);
                    $i++;
            }


            //array_multisort($images);
            $restaurant['pictures'] = $pictures;
            $restaurant['images'] = $images;
          

            //video
            $restaurant['video'] = $media->getVideoRestaurantDishes($restaurant['restaurant']);

            //pricing
            $this->setPriceSegment($restaurant['currency']);
            $restaurant['pricing'] = array('lunch' => $this->PricingDollars($this->PriceMeal($restaurant['pricing'], 2)), 
                'dinner' => $this->PricingDollars($this->PriceMeal($restaurant['pricing'], 1)),
                'average' => $this->PricingDollars($this->PriceMeal($restaurant['pricing'], 3)));

            //convert open hours//
            $restaurant['openhours'] = $this->getOpeningHoursListing($restaurant['restaurant'], true);

            // menu
            $mobj = new WY_Menu($restaurant['restaurant']);
            $categories = $mobj->getCategorieList('public');

            //categories
            $categorie = array();
            foreach ($categories as $categorie) {
                $menus = $mobj->getCategorieItems($categorie['ID']);
                $menu[] = array('categorie' => $categorie, 'items' => $menus);
            }

            $restaurant['takeoutrestaurant'] = ($this->takeOutRestaurant() > 0) ? 1 : 0;
            $restaurant['externalLink'] = ($this->restaurantExtarnalLink() > 0)? 1:0;
            
            $restaurant['externalLink'] = ($this->restaurantExtarnalLink() > 0)? 1:0;
             if($restaurant['externalLink']==1)  {
                if(isset($restaurant['url'])){
                    $restaurant['trackingUrl'] =  $this->getTokenUrl($restaurant['restaurant'], $restaurant['url'], 'external') ;
                }
              }else{$restaurant['url'] ="";}
            //Payment Flg
           
            $restaurant['paypalflg']  = ($this->checkPaypalActive() != 0)? 1:0;
            $restaurant['payment'] = $this->chkPaymentInfo($restaurant['restaurant']);
      
            
            /// CPP creditsuisse
            $restaurant['affiliate_program'] = ($this->corporateWheel() != 0)?"cpp_credit_suisse":0;
                   
            $restaurant['multiprod_allotment'] = ($this->checkMultiProductAlllote() != 0) ? 1: 0;
            
            // burnt ends restaurant layout
            $restaurant['restaurant_layout'] = null;
            if($restaurant['restaurant'] == 'SG_SG_R_TheOneKitchen' || $restaurant['restaurant'] == 'SG_SG_R_BurntEnds'){
                $restaurant['restaurant_layout'] = 'https://media.weeloy.com/upload/restaurant/SG_SG_R_BurntEnds/burnt_ends_layout.jpg';
            }
            
              
            //best offer
            $tmp = $this->getBestOffer($restaurant['restaurant'], $restaurant['is_wheelable'], 3, $restaurant['affiliate_program']);
            if (!empty($tmp)) {
                $restaurant['best_offer'] = $tmp;
            }

            //book button
            $button = array('color'=> '#2AA8CF', 'text'=> 'Book Now');
            if($restaurant['status'] == 'comingsoon')
                $button = array('color'=> '#dddddd', 'text' => 'Booking coming soon');
            else if ($restaurant['is_wheelable'] && !$restaurant['is_bookable'])
                //$button = array('color'=> '#438c4f', 'text'=> 'Get Code Now');
                $button = array('color'=> '#2AA8CF', 'text'=> 'Request Now');
            
            $restaurant['book_button'] = $button;
            $restaurant['internal_path'] = $this->getRestaurantInternalPath($restaurant['restaurant']);
            
            // TMP PATCH 
            //$sql = "SELECT comment as reviewdesc, reviewdtvisite as reviewdtvisite, reviewdtcreate as reviewdtcreate, firstname as reviewguest,reviewgrade as reviewgrade FROM review, member WHERE review.user_id = member.email AND restaurant =:query  ORDER by reviewdtvisite DESC LIMIT 3";

            $rew = new WY_Review($restaurant['restaurant']);
            $review = $rew->getpartialUserReview(3);
            // TMP PATCH 
            // link to tncs
            $restaurant['weeloy_tnc'] = "https://www.weeloy.com/templates/tnc/fullterms.php?restaurant=" . $restaurant['restaurant'] . "&bktracking=";
            $db = null;
            $errors = null;
            //$data = array(
            //    "restaurantinfo" => $restaurant,
            //    'menu' => $menu,
            //    'review' => $review,
            //    'chef' => $chef);
            //return $data;
            return $restaurant;
            //echo format_api(1, $data, count($restaurant), $errors);
            //echo '{"restaurantinfo": ' . json_encode($restaurant) . ', "menu": ' . json_encode($menu) . ', "review": ' . json_encode($review) . '}';
        } catch (PDOException $e) {
            //api_error($e, "getRestaurantDetails");
            error_log(__FILE__." ".__FUNCTION__." ".__LINE__." Exception: ".$e->getMessage());
        }
        return null;
    }
    function ProcessRestaurantData(&$data, $user) {
        $cache = null;
        try {
            $restaurant = $data['restaurant'];
            $isFavourite = empty($user) ? '0' : pdo_single_select_with_params("SELECT is_favorite from members_restaurants_favorite where is_favorite = 1 and member = :user and restaurant = :restaurant", ['user'=>$user, 'restaurant'=>$restaurant]);
            // error_log(__FILE__." ".__FUNCTION__." ".__LINE__." isFavourtie: ". $isFavourite['is_favorite']);
            // $cache['is_favorite'] = empty($user) || !isset($isFavourite['is_favorite']) || $isFavourite['is_favorite'] == null ? '0' : $isFavorite['is_favorite'];
            $cache['is_favorite'] = $data['is_favorite'] =  !empty($user) && $isFavourite['is_favorite'] == 1 ? 1 : 0;
            $cache['pricerating'] = $data['pricerating'] = $this->PricingDollars($this->PriceMeal($data['pricing'], 3));
            $cache['pricediner'] = $data['pricediner'] = (preg_match('/Dinner/', $data['mealtype'])) ? $this->PricingDollars($this->PriceMeal($data['pricing'], 1)) : "";
            $cache['pricelunch'] = $data['pricelunch'] = (preg_match('/Lunch/', $data['mealtype'])) ? $this->PricingDollars($this->PriceMeal($data['pricing'], 2)) : "";
            $cache['affiliate_program'] = $data['affiliate_program'] =  ($data['extraflag'] & CORPORATEWHEEL) ? "cpp_credit_suisse" : 0;
            $cache['internal_path'] = $data['internal_path'] = $this->getRestaurantInternalPath($data['restaurant']);
            $cache['best_offer'] = $data['best_offer'] = $this->getBestOffer($data['restaurant'], $data['is_wheelable'], 1, $cache['affiliate_program']);
            // Reviews
            $review = new WY_Review;
            $r = $review->getReviewsCount();
            $cache['reviews'] = array('count'=>$r['count'], 'score'=>$r['score'], 'score_desc'=>$r['review_desc']);
            //book button
            $booktitle = "BOOK SOON";
            if (($data['status'] == 'active' && $data['is_bookable']) || $data['status'] == 'demo_reference')
                $booktitle = "BOOK NOW";
            if (!$data['is_bookable'] && $data['status'] == 'active')
                $booktitle = "REQUEST NOW";
            $cache['book_button'] = $data['book_button'] = array('label' => $booktitle, 'style' => '');
        } catch (PDOException $e) {
            error_log(__FILE__." ".__FUNCTION__." ".__LINE__." PDOException: ".$e);
            api_error($e, __FILE__." ".__FUNCTION__." ".__LINE__); 
        }
        catch (Exception $e) {
            error_log(__FILE__." ".__FUNCTION__." ".__LINE__." Exception: ".$e);
            api_error($e, __FILE__." ".__FUNCTION__." ".__LINE__);
        }
//        error_log("DATA" .print_r($data,true));
        $data['cache'] = json_encode($cache);
    }
    function getAssignedRestaurantNameList($email) {
        return pdo_multiple_select("SELECT restaurant FROM restaurant h, restaurants_managers mr WHERE h.RESTAURANT = mr.restaurant_id AND mr.status = 'active' AND mr.member_id = '$email'");
    }
	function getCityRestaurants($city, $user) {
        $starting = microtime(true);
        $data = [];
        try {
            if(WY_debug::checkOnceADay("RESTAURANT_CACHE") < 1) {
                $data = pdo_multiple_select("SELECT r.ID, r.restaurant, title, hotelname, cuisine, logo, url, address, address1, city, zip, country, tel, fax, email, smsid, map, region, rating, GPS, likes, extraflag, pricing, currency, rating, stars, openhours, bookhours, mealtype, wheelvalue, r.status, is_displayed, is_wheelable, is_bookable, m.path as image_path, m.name as image, m.morder from restaurant r left join (select * from (SELECT restaurant,path,name,morder FROM media where media_type='picture' and object_type='restaurant' order by restaurant,morder)m_ group by restaurant)m on r.restaurant=m.restaurant WHERE r.city='$city' and r.status='active' and r.restaurant != 'SG_SG_R_ThePaymentKitchen' and r.restaurant != 'SG_SG_R_TheFunKitchen' and r.restaurant != 'SG_SG_R_TheOneKitchen'", "business");
                error_log(__FILE__." ".__FUNCTION__." ".__LINE__." email: ".$user);
                if(!empty($user)) {
                    $restaurants = pdo_multiple_select("SELECT r.ID, r.restaurant, title, hotelname, cuisine, logo, url, address, address1, city, zip, country, tel, fax, email, smsid, map, region, rating, GPS, likes, extraflag, pricing, currency, rating, stars, openhours, bookhours, mealtype, wheelvalue, r.status, is_displayed, is_wheelable, is_bookable, m.path as image_path, m.name as image, m.morder FROM restaurant r join restaurants_managers mgr on r.restaurant = mgr.restaurant_id join (select * from (SELECT restaurant,path,name,morder FROM media where media_type='picture' and object_type='restaurant' order by restaurant,morder)m_ group by restaurant)m on r.restaurant = m.restaurant WHERE r.city='$city' AND mgr.member_id = '$user'", "business");
                    error_log(__FILE__." ".__FUNCTION__." ".__LINE__." ".count($restaurants)." restaurants associated with ".$user);
                    $data = array_merge($data, $restaurants);
                }
                $this->setPriceSegment($this->currency);
                $timestamp = microtime(true);
                for ($i = 0; $i < count($data); $i++) {
                    $this->ProcessRestaurantData($data[$i], $user);
                    $cache = $data[$i]['cache'];
                    $restaurant = $data[$i]['restaurant'];
                    $check = "SELECT ID from restaurant_cache where restaurant = '$restaurant'";
                    $insert = "INSERT into restaurant_cache (restaurant, cache) VALUES ('$restaurant', '$cache')";
                    $result = pdo_insert_unique($check, $insert, "business");
                    if ($result > 0) {
                        $sql = "UPDATE restaurant_cache SET cache = :cache WHERE ID = :ID";
                        $parameters = array('ID' => $result, 'cache' => $cache);
                        pdo_update($sql, $parameters);
                    }
                }
                $debug = new WY_debug;
                $debug->updateOrInsertTimestamp("API-LOG", "RESTAURANT_CACHE", "");
            } else {
                error_log(__FILE__." ".__FUNCTION__." ".__LINE__." email: ".$user);
                $data = pdo_multiple_select("SELECT r.ID, r.restaurant, title, hotelname, cuisine, logo, url, address, address1, city, zip, country, tel, fax, email, smsid, map, region, rating, GPS, likes, extraflag, pricing, currency, rating, stars, openhours, bookhours, mealtype, wheelvalue, r.status, is_displayed, is_wheelable, is_bookable, m.path as image_path, m.name as image, m.morder, cache.cache from restaurant r join restaurant_cache cache on r.restaurant=cache.restaurant left join (select * from (SELECT restaurant,path,name,morder FROM media where media_type='picture' and object_type='restaurant' order by restaurant,morder)m_ group by restaurant)m on r.restaurant=m.restaurant WHERE r.city='$city' and r.status='active' and r.restaurant != 'SG_SG_R_ThePaymentKitchen' and r.restaurant != 'SG_SG_R_TheFunKitchen' and r.restaurant != 'SG_SG_R_TheOneKitchen'", "business");
               
                if(!empty($user)) {
                    $restaurants = pdo_multiple_select("SELECT r.ID, r.restaurant, title, hotelname, cuisine, logo, url, address, address1, city, zip, country, tel, fax, email, smsid, map, region, rating, GPS, likes, extraflag, pricing, currency, rating, stars, openhours, bookhours, mealtype, wheelvalue, r.status, is_displayed, is_wheelable, is_bookable, m.path as image_path, m.name as image, m.morder, cache.cache from restaurant r join restaurants_managers mgr on r.restaurant = mgr.restaurant_id join restaurant_cache cache on r.restaurant=cache.restaurant join (select * from (SELECT restaurant,path,name,morder FROM media where media_type='picture' and object_type='restaurant' order by restaurant,morder)m_ group by restaurant)m on r.restaurant=m.restaurant WHERE r.city='$city' and r.restaurant = mgr.restaurant_id AND mgr.member_id = '$user'", "business");
                    error_log(__FILE__." ".__FUNCTION__." ".__LINE__." ".count($restaurants)." restaurants associated with ".$user);
                    $data = array_merge($data, $restaurants);
                }
                error_log(__FILE__." ".__FUNCTION__." ".__LINE__." cache ".count($data)." records!");
            }
            if(count($data) < 1) {
    			$this->result = -1;
    			$this->msg = "Unknow City: ".$city;
            }
        } catch (PDOException $e) {
            error_log(__FILE__." ".__FUNCTION__." ".__LINE__." PDOException: ".$e);
            api_error($e, __FILE__." ".__FUNCTION__." ".__LINE__); 
        }
        catch (Exception $e) {
            error_log(__FILE__." ".__FUNCTION__." ".__LINE__." Exception: ".$e);
            api_error($e, __FILE__." ".__FUNCTION__." ".__LINE__);
        }
        error_log(__FILE__." ".__FUNCTION__." ".__LINE__." ".count($data)." restaurants takes ".(microtime(true) - $starting)." s");
        return $data;
	}
    function getFavouriteRestaurants() {
        $starting = microtime(true);
        $data = [];
        try {
            // $restaurants = pdo_multiple_select("SELECT distinct restaurant.restaurant from restaurant join members_restaurants_favorite as fav on restaurant.restaurant = fav.restaurant where restaurant.status = 'active' limit 300");
            $restaurants = pdo_multiple_select("SELECT distinct(r.ID), r.restaurant,
                        r.title, r.hotelname, r.cuisine, r.wheel,
                        r.wheelversion, r.chef, r.chef_type, r.chef_award, r.chef_logo, r.chef_description,
                        r.chef_origin, r.chef_believe, r.logo, r.url, r.description,
                        r.address, r.address1, r.city, r.zip, r.country, r.tel, r.fax, r.email, r.smsid, r.map,
                        r.region, r.rating, r.creditcard, r.mealslot, r.award, r.GPS, r.likes, r.bo_name, r.bo_tel, r.bo_email, r.mgr_name, r.mgr_tel, r.mgr_tel, r.mgr_email, r.POS, r.images, r.pricing, r.currency, r.rating, r.stars, r.openhours, r.bookhours, r.restaurant_tnc, r.mealtype,
                        r.dfmealtype, r.dfminpers, r.dfmaxpers, r.dfproduct, r.wheelvalue, r.wheelsponsor, r.wheelRescode, r.status, r.is_displayed,
                        r.is_wheelable, r.is_bookable, r.offer_type, r.extraflag, r.reservedflag, r.laptime, r.lastorderlunch, r.lastorderdiner, r.cutofflunch, r.cutoffdiner, r.bookinfo, r.bookcritical, r.deposit_cancel_tnc,
                        r.tkoutpayment, r.tkoutpickup, r.tkoutdeliver, r.tkouttax, r.tkoutminorder, r.tkout_tnc, r.tags, r.bkgcustomcolor, r.booking_deposit_lunch, r.booking_deposit_dinner, r.deposit_cancel_tnc, r.facebook_pid, r.webrestodesc, r.webchefdesc, r.restogeneric, r.aws_email_verification
                        from restaurant as r join members_restaurants_favorite as fav on r.restaurant = fav.restaurant where r.status = 'active' limit 300");
            $this->setPriceSegment($this->currency);
            foreach ($restaurants as $i) {
                //array_push($data, $this->getRestaurantDetails($i['restaurant']));
                //$this->buildRestaurantData($i);
                $this->ProcessRestaurantData($i);
                unset($i['cache']);
                array_push($data, $i);
            }
            if(count($data) < 1) {
                $this->result = -1;
                $this->msg = "No favourite restaurants found!";
            }
        } catch (PDOException $e) {
            error_log(__FILE__." ".__FUNCTION__." ".__LINE__." PDOException: ".$e);
            api_error($e, __FILE__." ".__FUNCTION__." ".__LINE__); 
        }
        catch (Exception $e) {
            error_log(__FILE__." ".__FUNCTION__." ".__LINE__." Exception: ".$e);
            api_error($e, __FILE__." ".__FUNCTION__." ".__LINE__);
        }
        error_log(__FILE__." ".__FUNCTION__." ".__LINE__." ".count($data)." restaurants takes ".(microtime(true) - $starting)." s");
        return $data;
    }		    
    function getRestaurantSmsNumber($restaurant) {
        
        $smsid = '';
		$this->result = 1;
        $restaurant = $this->clean_input($restaurant);
		if($this->restaurant != $restaurant)
			$this->getRestaurant($restaurant);

 		if($this->result < 0)
			return;

       if ($this->checksmsheader() > 0 ) {
            $smsid = $this->smsid;
        }
        return $smsid;
    }
    
    function getManagerPhoneNumber() {
        return $this->mgr_tel;
    }

    function savereservedflag($theRestaurant, $reservedflag) {
	pdo_exec("update restaurant  set reservedflag = '$reservedflag' where restaurant = '$theRestaurant' limit 1");
    }

    function noWalking() {
        return $this->extraflag & NO_WALKING;
    }

    function sponsor() {
        return $this->extraflag & SPONSOR_WHEEEL;
    }

    function open24hour7() {
        return $this->extraflag & OPEN24HOUR7BY7;
    }

    function localTimeBooking() {
        return $this->extraflag & LOCALTIMERESA;
    }

    function perPaxBooking() {
        return $this->extraflag & PERPAXRESA;
    }

    function fiveSlotMeal() {
    	$slot = intval($this->mealslot);
    	$flag = $this->extraflag & FIVESLOTMEAL;
    	if($flag == 0)
    		return 0;
    	return ($slot > 0 && $slot < 10) ? $slot : 5;
    }

    function withPromotion() {
        return $this->extraflag & WITHPROMOTION;
    }

    function CCExtractionFormat() {
        return $this->extraflag & CALLCENTEREXTRACTION;
    }

    function enableBookhours() {
        return $this->extraflag & ENABLEBOOKHOUR;
    }

    function smsNotifToRestaurant() {
        return $this->extraflag & SMSNOTIFTORESTAURANT;
    }

    function takeOutRestaurant() {
        return $this->extraflag & TAKEOUTRESTAURANT;
    }

    function restaurantExtarnalLink() {
        return $this->extraflag & RESTAURANTEXTERNALLINK;
    }

    function corporateWheel() {
        return $this->extraflag & CORPORATEWHEEL;
    }

    function offpeakWheel() {
        return $this->extraflag & OFFPEAKWHEEL;
    }

    function promocodeBkg() {
        return $this->extraflag & PROMOCODEBKG;
    }

    function optinBkg() {
        return $this->extraflag & OPTINBKG;
    }

    function bookingHourBySlot() {
        return $this->extraflag & BOOKINGHOURBYSLOT;
    }

    function bookingwithChildren() {
        return $this->extraflag & BOOKINGWITHCHILDREN;
    }

    function checkduplicateBkg() {
        return $this->extraflag & CHECKDUPLICATE;
    }

    function grabzList() {
        return $this->extraflag & GRABZ;
    }

    function checkbkdeposit() {
        return $this->extraflag & BKDEPOSIT;
    }
    
    function checkadyen(){
        return $this->extraflag & DEPOSITADYEN;
    }
    
    function checkreddot(){
        return $this->extraflag & DEPOSITREDDOT;
    }
    
    static function s_checkbkdeposit($restaurant) {
    	$data = pdo_single_select("SELECT extraflag from restaurant where restaurant = '$restaurant' limit 1");
    	
        return (isset($data['extraflag'])) ? intval($data['extraflag']) & BKDEPOSIT : 0;
    }

    function checkeventbooking() {
        error_log(__FILE__." ".__FUNCTION__." ".__LINE__.": ".($this->extraflag & EVENTBOOKING));
        return $this->extraflag & EVENTBOOKING;
    }

    static function s_checkeventbooking($restaurant) {
    	$data = pdo_single_select("SELECT extraflag from restaurant where restaurant = '$restaurant' limit 1");
    	
        return (isset($data['extraflag'])) ? intval($data['extraflag']) & EVENTBOOKING : 0;
    }

    static function check_iswheelable($restaurant) {
     	$data = pdo_single_select("SELECT is_wheelable from restaurant where restaurant = '$restaurant' limit 1");
       	return (isset($data['is_wheelable']) && intval($data['is_wheelable']) == 1);
   	}
    	
    function advancebooking() {
        return $this->extraflag & ADVANCEBOOKING;
    }

	function getDinnerTime() {
		
		$time = "16:00";
    	if($this->bookwindow != "") {
    		$bkwAr = explode("|", $this->bookwindow);
    		$time = (count($bkwAr) > 2 && strlen($bkwAr[2]) > 1 && trim($bkwAr[2]) != "") ? $bkwAr[2] : "16:00";
    		}
    	return $time;
	}
		
    function getBookingwindow($plaform = "") {
       	
    	$label = array("bkwbk", "bkwcc");
    	$len = count($label);
    	$advanced = ($this->advancebooking() != 0);
		foreach($label as $vv)
			$$vv = ($advanced) ? 180 : 90;
    	
    	if($this->bookwindow != "" && $advanced) {
    		$bkwAr = explode("|", $this->bookwindow);
    		if($len > count($bkwAr)) 
    			$len = count($bkwAr);
    		
    		for($i = 0; $i < $len; $i++) {
 				$dd = intval($bkwAr[$i]);
 				if($dd > 30)
					${$label[$i]} = $dd;
				}
    		}
    		    		
		return ($plaform != "CALLCENTER") ? $bkwbk : $bkwcc;
    }

    function websitefields() {
        return $this->extraflag & WEBSITEFIELDS;
    }

    function twoSitting() {
        return $this->extraflag & TWOSITTING;
    }

    function dailyCutoff() {
        return $this->extraflag & DAILYCUTOFF;
    }

    function emailWhitelabel() {
        return $this->extraflag & EMAILWHITELABEL;
    }

    function tmsCaptain() {
        return $this->extraflag & TMSCAPTAIN;
    }

    function tmsApplication() {
        return $this->extraflag & TMSAPPLICATION;
    }

    function tmsV2() {
        return $this->extraflag & TMSV2;
    }

    function mutipleProdAllote() {
        $val = $this->checkMultiProductAlllote();
        $ll = strlen($this->dfproduct);
        $pp = strpos($this->dfproduct, "|");

        return ($ll < 7 || $val == 0 || $pp === false || $pp < 3 || $pp > ($ll - 3)) ? 0 : $val;
    }

    function checkPaypalActive() {
        return $this->extraflag & DEPOSITPAYPAL;
    }

    function checkCreditCardDetails() {
        return $this->extraflag & CREDITCARDDETAILS;
    }

    function checkMultiProductAlllote() {
        return $this->extraflag & MUTIPLEPRODUCTALLOTE;
    }
    
    function checkBlockedList() {
        return $this->extraflag & BLOCKEDGUESTLIST;
    }
    
    function checkPabx() {
        return $this->extraflag & PABX;
    }
    
    function onemonthbooking() {
        return $this->reservedflag & ONEMONTHBOOKING;
    }

    function checkCallcenterMgt() {
        return $this->reservedflag & CALLCENTERMEMBER;
     }
        
    function checkBooking15mn() {
        return $this->reservedflag & BOOKING15MN;
    }

    function checkTMSSync() {
        return $this->reservedflag & TMSSYNC;
    }

    function checkDailyspecialboard() {
        return $this->reservedflag & DAILYSPECIALBOARD;
    }

    function checkRestrictedDSB() {
        return $this->reservedflag & RESTRICTED_DSB;
    }

    function check1yearcallcenter() {
        return $this->reservedflag & ONEYEARCALLCENTER;
    }
    function checkcallcenterpayment() {
        return $this->reservedflag & CALLCENTERPAYMENT;
    }
    
    static function s_checkccpayment($restaurant) {
    	$data = pdo_single_select("SELECT reservedflag from restaurant where restaurant = '$restaurant' limit 1");
    	
        return (isset($data['reservedflag'])) ? intval($data['reservedflag']) & CALLCENTERPAYMENT : 0;
    }
    static function s_checkmodifypayment($restaurant) {
    	$data = pdo_single_select("SELECT reservedflag from restaurant where restaurant = '$restaurant' limit 1");
    	
        return (isset($data['reservedflag'])) ? intval($data['reservedflag']) & MODIFYPAYMENTBK : 0;
    }
    
    

    function checkauthentication(){
        return $this->extraflag & TWOFACTAUTHENTICATION;
    }
    
    function checksmstemplate(){
        return $this->extraflag & SMSTEMPLATE;
    }
    
    function checkactivitylog(){
        return $this->extraflag & ACTIVITYLOG;
    }

    function checksmsheader(){
       return $this->extraflag & SMSHEADER; 
    }
    
    function checktmspos(){
       return $this->extraflag & TABLEPOS; 
    }
    
    function checkdemoresto(){
       return ($this->status == 'demo_reference'); 
    }
    
    function checkpaymentinvoice(){
       return $this->extraflag & PAYMENTINVOICE; 
    }
    
 
    function getdemoResto() {
	return pdo_multiple_select_index("SELECT restaurant FROM restaurant where status = 'demo_reference'");
     }
 		   
    static function posEnable($theRestaurant) {
        $theRestaurant = clean_input($theRestaurant);

        $data = pdo_single_select("SELECT extraflag FROM restaurant WHERE restaurant ='$theRestaurant' limit 1");
        return (!empty($data['extraflag'])) ? (intval($data['extraflag']) & TABLEPOS) : 0;
    }

    function isDemo($theRestaurant) {
        if ($this->restaurant != $theRestaurant) {
            $this->getRestaurant($theRestaurant);
        }

        if ($this->restaurant != $theRestaurant) {
            $this->msg = "invalid restaurant " . $theRestaurant;
            $this->result = -1;
            return "";
        }

        return ($this->status == "demo");
    }
    function is_test_env() {

        if (strpos($_SERVER['HTTP_HOST'], 'api') !== false) {
            return false;
        }

        if (strpos($_SERVER['HTTP_HOST'], 'www') !== false) {
            return false;
        }

        if (strpos($_SERVER['HTTP_HOST'], 'prod') !== false) {
            return false;
        }

//        if (strpos($_SERVER['HTTP_HOST'], 'dev') !== false) {
//            return false;
//        }
        
        if (strpos($_SERVER['HTTP_HOST'], 'codersvn') !== false) {
            return false;
        }

        return true;
    }

    // apparently not use.
    function OpenToday($theRestaurant, $product = "") {

        $allote = $this->setUpAllote($theRestaurant, $product, "");
        if ($this->result < 0)
            return "";

        $ohvalue = $this->openhours;
        return $allote->ComputeOpenToday($ohvalue);
    }

    function reportAvailability($theRestaurant, $rdate, $product) {
        $allote = $this->setUpAllote($theRestaurant, $product, "");
        if ($this->result < 0)
            return "";

        $ohvalue = $allote->ohvalue;
        $data = $allote->getAlloteReportSlot($ohvalue, $rdate);
        $this->result = $allote->result;
        return $data;
    }

    function getPickupHours($theRestaurant) {

        $allote = $this->setUpAllote($theRestaurant, "", "");
        if ($this->result < 0)
            return "";

        $ohvalue = $this->openhours;
        return $allote->templatePickupHours($ohvalue, true);
    }

    function CheckDayAvailability($theRestaurant, $product, $rtime, $pax) {
        $allote = $this->setUpAllote($theRestaurant, $product, "");
        if ($this->result < 0)
            return "";

        $data = $allote->CheckDayAvailability($rtime, $pax);
        $this->result = $allote->result;
        return $data;
    }

    function CheckAvailability($theRestaurant, $rdate, $rtime, $pax, $booking, $product, $platform = "") {
        $allote = $this->setUpAllote($theRestaurant, $product, $platform);
        if ($this->result < 0)
            return "";

        $ret = $allote->getAllote1Slot($rdate, $rtime, $pax, $booking);
        $this->result = $allote->result;
        $this->msg = $allote->msg;
        return $ret;
    }

    // return template to use as a popover
    function getOpeningHoursListing($theRestaurant, $no_html = false) {

        $allote = $this->setUpAllote($theRestaurant, "", "");
        if ($this->result < 0)
            return "";

        if ($this->open24hour7())
            return array(array('day' => '', 'lunch' => 'Open Daily  ', 'dinner' => '24h'));

        $ohvalue = $this->openhours;
        return $allote->templateAlloteOpenHour($ohvalue, $no_html);
    }

    // return all open available 1/2 hour slice data for lunch and for dinner
    function getAlloteOpenHours($theRestaurant, $pax, $product, $platform = "") {

        $allote = $this->setUpAllote($theRestaurant, $product, $platform);
        if ($this->result < 0)
            return "";

        if ($allote->perpax != 1)
            $pax = 1;

        $data = $allote->AlloteComputeOpenHour($pax);
        $this->result = $allote->result;
        return $data;
    }

    function getAlloteRestaurant($theRestaurant, $year, $product) {

        $allote = $this->setUpAllote($theRestaurant, $product, "");
        if ($this->result < 0)
            return array(0);

        return $allote->getAlloteRestaurant($theRestaurant, $year);
    }

    function saveAlloteRestaurant($theRestaurant, $year, $size, $start, $lunchdata, $dinnerdata, $product) {
        $allote = $this->setUpAllote($theRestaurant, $product, "");
        if ($this->result < 0)
            return array(0);

        $data = $allote->saveAlloteRestaurant($theRestaurant, $year, $size, $start, $lunchdata, $dinnerdata);
        $this->result = $allote->result;
        $this->msg = $allote->msg;
        return $data;
    }
	
	private function getrestosection($product) {
        $data = pdo_single_select("SELECT * FROM restaurant_section_config WHERE restaurant ='$this->restaurant' and product = '$product' limit 1");
        if(!isset($data["restaurant"]) || $data["restaurant"] != $this->restaurant)
        	return array("product" => "");
        $data["generic"] = $generic = json_decode($data["additional_info"], true);
        $data["laptime"] = (isset($generic["section"]["laptime"])) ? intval($generic["section"]["laptime"]) : 0;
        $data["mealduration"] = (isset($generic["mealduration"])) ? intval($generic["mealduration"]) : 0;
        $data["extraallote"] = (isset($generic["extraallote"])) ? intval($generic["extraallote"]) : 0; // to enforce only 1 booking per mealtype
        return $data;
		}
		
    private function setUpAllote($theRestaurant, $product, $platform = "") {
        $this->result = 1;

        if ($this->restaurant != $theRestaurant)
            $this->getRestaurant($theRestaurant);

        if ($this->restaurant != $theRestaurant) {
            $this->msg = "invalid restaurant " . $theRestaurant;
            $this->result = -1;
            return "";
        }

        $allote = new WY_Allote($theRestaurant);
        $allote->mealduration = 3;
        $allote->perpax = 0;
        $allote->twositting = 0;
        $allote->multProduct = array(0);
        $allote->nowalking = 0;
        $allote->dailycutoff = 0;
        $allote->ohvalue = $this->openhours;

        if ($this->perPaxBooking() != 0)
            $allote->perpax = 1;

        if ($this->noWalking() != 0)
            $allote->nowalking = 1;

        if ($this->twoSitting() != 0)
            $allote->twositting = 1;

		$slot = $this->fiveSlotMeal();
        if ($slot != 0)
            $allote->mealduration = $slot;

		if ($this->dailyCutoff() != 0)
            $allote->dailycutoff = 1;

		$allote->Bookhoursflg = ($this->enableBookhours() != 0);
		$allote->HourBySlotflg = ($this->bookingHourBySlot() != 0);

		$allote->bookhours = $this->bookhours;
		// cannot have both, HBS will prevail
        if ($allote->Bookhoursflg && $allote->HourBySlotflg == false)
            $allote->ohvalue = $this->bookhours;
		
        $allote->laptime = floatval($this->laptime);
        $allote->cutofflunch = $this->cutofflunch;
        $allote->cutoffdiner = $this->cutoffdiner;
        $allote->maxdaybooking = $this->getBookingwindow($platform);

        $allote->product = $product;
        $allote->multiproduct = false;
        $allote->extraallote = 0;
        
        if ($this->mutipleProdAllote() && !empty($allote->product)) {
            $allote->multProduct = explode("|", $this->dfproduct);
            if (count($allote->multProduct) < 1 || !in_array($allote->product, $allote->multProduct)) {
            	$allote->msg = "invalid product " . $allote->product;
                $allote->result = -1;
                return $allote;
                }
	        $allote->multiproduct = true;
            }

		if($allote->multiproduct) {
			$allote->restosection = $this->getrestosection($allote->product);
            if($allote->restosection["mealduration"] > 0 &&  $allote->restosection["mealduration"] < 10)
            	$allote->mealduration = $allote->restosection["mealduration"];
            if($allote->restosection["laptime"] > 0 &&  $allote->restosection["laptime"] < 80)
            	$allote->laptime = $allote->restosection["laptime"];
            if($allote->restosection["extraallote"] > 0 &&  $allote->restosection["extraallote"] < 20)
            	$allote->extraallote = $allote->restosection["extraallote"];
			if($theRestaurant == "SG_SG_R_BurntEnds" && $product == "Chef table")
             	$allote->mealduration = 8;
             }
        return $allote;
    }

    function getPriceDescription($theRestaurant) {

        if ($this->restaurant != $theRestaurant) {
            $this->getRestaurant($theRestaurant);
        }

        if ($this->restaurant != $theRestaurant) {
            $this->msg = "invalid restaurant " . $theRestaurant;
            $this->result = -1;
            return "";
        }

        if (!empty($this->pricelunch) && preg_match("/Lunch/", $this->mealtype)) {
            $meals["Lunch"] = $this->pricediner;
        }

        if (!empty($this->pricediner) && preg_match("/Dinner/", $this->mealtype)) {
            $meals["Dinner"] = $this->pricediner;
        }

        $this->setPriceSegment($this->currency);
        $template = $sep = "";
        while (list($label, $segment) = each($meals)) {
            if (key_exists($segment, $this->glb_pricedesc)) {
                $value = $this->glb_pricedesc[$segment];
                if ($segment == "$") {
                    $template .= $sep . $label . " is under $" . $value;
                } else if ($segment == "$$$$$") {
                    $template .= $sep . $label . " is over $" . $this->glb_pricedesc[substr($segment, 1)];
                } else {
                    $template .= $sep . $label . " is between $" . $this->glb_pricedesc[substr($segment, 1)] . " and $" . $value;
                }

                $sep = "<br/>";
            }
        }
        return $template;
    }

    function getClusterCallCenter($email, $bkrestaurant, $bktitle) {

        if (empty($email))
            return array(-1, "", "", "", $bkrestaurant, $bktitle, $restAr, $phone, "", "");

 		$SubOption = array("phone", "maxpax", "minpax");
      	$restAr = $bookerAr = array();
        $account = "";
 
        foreach ($SubOption as $keyval) 
        	$$keyval = "";	

        $clusdata = $this->getCallCenterAccount($email);
        if (!empty($clusdata)) {
            $clusdataAr = explode(";", $clusdata);
            foreach ($clusdataAr as $keyval) {
				$tt = explode("=", $keyval);
				$ll = $tt[0];
				$vv = $tt[1];
				if ($ll == "email")
					$restAr = $this->getListAccountRestaurant($vv);
				else if($ll == "booker")
					$bookerAr = explode("|", $vv);
				else if(in_array($ll, $SubOption))
					$$ll = $vv;
				}
        }

        if(empty($clusdata)) {	// if there a cluster default for that domain. If Yes, just retrieve sub option
        	$account = strtolower(preg_replace("/^.*\@/",  "", $email));
	        $clusdata = $this->getCallCenterAccount($account, "MASTER");
			if (!empty($clusdata)) {
				$clusdataAr = explode(";", $clusdata);
				foreach ($clusdataAr as $keyval) {
					$tt = explode("=", $keyval);
					$ll = $tt[0];
					$vv = $tt[1];
					if(in_array($ll, $SubOption))
						$$ll = $vv;
					}
				}
			}

		//error_log('getCallCenterAccount ' . 'account=' . $account . ', phone=' . $phone . ', maxpax=' . $maxpax . ', minpax=' . $minpax);
	
        if (count($restAr) < 1)
            $restAr = $this->getListAccountRestaurant($email);

        if (!empty($bkrestaurant) && WY_Restaurant::isrestaurant($bkrestaurant)) {
            foreach ($restAr as $row)
                if (!empty($row['restaurant']) && $row['restaurant'] == $bkrestaurant)
                    break;
            if (empty($row['restaurant']) || $row['restaurant'] != $bkrestaurant)
                $restAr[] = array('restaurant' => $bkrestaurant, 'title' => WY_Restaurant::getTitle($bkrestaurant));
        }

        if (count($restAr) < 1)
            return array(-1, "", "", $email, $bkrestaurant, $bktitle, $restAr, $phone);

        $selectresto = "";
        $listrestoAr = array();

        foreach ($restAr as $row) {
            if (empty($row['restaurant']) || empty($row['title']))
                continue;

            if (empty($bkrestaurant)) {
                $bktitle = $row['title'];
                $bkrestaurant = $row['restaurant'];
            } else if (empty($bktitle) && $bkrestaurant == $row['restaurant'])
                $bktitle = $row['title'];

            if (count($listrestoAr) < 1)
                $listrestoAr[] = $bkrestaurant;

            if ($bkrestaurant != $row['restaurant'])
                $listrestoAr[] = $row['restaurant'];

            $selected = ($bkrestaurant == $row['restaurant']) ? "selected" : "";
            $selectresto .= "<option value = '" . $row['restaurant'] . "' " . $selected . ">" . $row['title'] . "</option>";
        }

        if (count($bookerAr) < 1) {
            $memdata = new WY_Member;
            $memdata->getMember($email);
            $bookerAr = array($memdata->firstname);
        }

        $selectbooker = "";
        if (count($bookerAr) > 0) {
			if(empty($booker)) $booker = $bookerAr[0];
			foreach ($bookerAr as $keyval) {
				$selected = ($keyval == $booker) ? "selected" : "";
				$selectbooker .= "<option $selected>" . $keyval . "</option>";
				}
        }

        return array(1, $selectresto, $selectbooker, $email, $bkrestaurant, $bktitle, $listrestoAr, $phone, $minpax, $maxpax);
    }

    function getBestwheel() {

        $this->getQueryrestaurant(NULL, "logo !='' and title !='' and wheelvalue > 70 order by wheelvalue DESC");
        $this->where = "";
    }

    function bestMustTry() {

        $this->getQueryrestaurant(NULL, "rating !='' and title !='' order by wheelvalue DESC");
        $this->where = "";
    }

    function topChef() {

        $this->getQueryrestaurant(NULL, "chef !='' and chef_award !='' and title !=''");
        $this->where = "";
    }

    function checkRestaurant($theRestaurant) {

        $sql = "SELECT ID, restaurant, title FROM restaurant where restaurant = '$theRestaurant' LIMIT 1";
        $data = pdo_single_select($sql);

        return (count($data) > 0);
    }

    function getListHotelRestaurant() {
        return pdo_column("SELECT distinct hotelname FROM restaurant where hotelname != '' ORDER by hotelname");
    }

    function getDefaultRestaurant($email, $membertype) {

        global $backoffice_listing_member_type_allowed;

        $where = 'WHERE 1';
        $join = '';

        if (in_array($membertype, $backoffice_listing_member_type_allowed)) {
            $join .= ' JOIN restaurants_managers mr ON mr.restaurant_id=h.restaurant ';
            $where .= " AND mr.member_id='" . $email . "' AND mr.status = 'active'";
        }

        $sql = "SELECT ID, restaurant, title FROM restaurant h $join $where LIMIT 1";

        $data = pdo_single_select($sql);
        $this->restaurant = $data['restaurant'];
        $this->title = $data['title'];
        $this->ID = $data['ID'];
    }

    function getSimpleListRestaurant() {
        return pdo_multiple_select_index("SELECT restaurant FROM restaurant order by restaurant");
    }

    function ismember_priviledge($membertype) {
        global $super_member_type_allowed;

        return (in_array($membertype, $super_member_type_allowed));
    }

    function getNbListRestaurant($email, $membertype) {

        $this->clear();

        $where = 'WHERE 1';
        $join = '';

        if ($this->ismember_priviledge($membertype) == false) {
            $join .= ' JOIN restaurants_managers mr ON mr.restaurant_id=h.restaurant ';
            $where .= " AND mr.member_id='" . $email . "' AND mr.status = 'active'";
        }

        $sql = "SELECT count(*) as number FROM restaurant h $join $where ";
        $data = pdo_single_select($sql);

        if (isset($data['number']))
            return intval($data['number']);
        return 0;
    }

    function getCallCenterAccount($email, $type = "SLAVE") {

        $cls = new WY_Cluster;
        $cls->read($type, $email, "CALLCENTER", "", "", "");  // should only return 1 answer
        if ($cls->result > 0) {
        	$content = $cls->clustcontent[0];
            $data = $cls->read("MASTER", $cls->clustparent[0], "CALLCENTER", "", "", "");
            if ($cls->result > 0) {
            	if($content != "")
            		$cls->clustcontent[0] = ($cls->clustcontent[0] . ";" . $content);
            	return $cls->clustcontent[0];		// this is an email and booker -> email=one@account.com;booker=name1|name2|name3...; add phone if necessary
            	}
        	}
        return ""; // bad a this point
    }

    function getListAccountRestaurant($member_id) {
        $sql = "SELECT restaurant, title FROM restaurant, restaurants_managers where member_id = '$member_id' && restaurant = restaurant_id and restaurants_managers.status = 'active' order by restaurant_id";
        return pdo_multiple_select($sql);
    }

    function getListRestaurant($email, $membertype, $orderBy = "title", $restrictedCountry = NULL) {

        $this->clear();

        $where = 'WHERE 1';
        $join = '';

        if ($this->ismember_priviledge($membertype) == false) {
            $join .= ' JOIN restaurants_managers mr ON mr.restaurant_id=h.restaurant ';
            $where .= " AND mr.member_id='" . $email . "' AND mr.status = 'active'";
        }

        if (!empty($restrictedCountry))
            $where .= " AND h.country = '$restrictedCountry' ";
        $sql = "SELECT restaurant, title, city, country FROM restaurant h $join $where order by $orderBy";
        $data = pdo_multiple_select($sql);
        foreach ($data as $row) {
            $this->restaurant[] = $row['restaurant'];
            $this->title[] = $row['title'];
            $this->city[] = $row['city'];
            $this->country[] = $row['country'];
        }
    }

    function getListNotAssignedRestaurant($theMember) {
        $data = pdo_multiple_select("SELECT restaurant, title FROM restaurant h WHERE id
                        NOT IN (SELECT id
                        FROM restaurant h, restaurants_managers mr
                        WHERE h.RESTAURANT = mr.restaurant_id
                        AND mr.status = 'active' AND mr.member_id = '$theMember') order by title");
        foreach ($data as $row) {
            $this->restaurant[] = $row['restaurant'];
            $this->title[] = $row['title'];
        }
    }

    function getListAssignedRestaurant($theMember) {
        $data = pdo_multiple_select("SELECT restaurant, title
                        FROM restaurant h, restaurants_managers mr
                        WHERE h.RESTAURANT = mr.restaurant_id
                        AND mr.status = 'active' AND mr.member_id = '$theMember' order by title");
        foreach ($data as $row) {
            $this->restaurant[] = $row['restaurant'];
            $this->title[] = $row['title'];
        }
    }
    function getListNotAssignedAppManagerRestaurant($theMember) {
        // $data = pdo_multiple_select("SELECT restaurant, title FROM restaurant h WHERE id
        //                 NOT IN (SELECT id
        //                 FROM restaurant h, restaurant_app_managers ram
        //                 WHERE h.RESTAURANT = ram.restaurant_id
        //                 AND ram.status = 'active' AND ram.user_id = '$theMember') order by title");
        $data = pdo_multiple_select("SELECT restaurant, title FROM restaurant h WHERE id
                        NOT IN (SELECT id
                        FROM restaurant h, restaurants_managers ram
                        WHERE h.RESTAURANT = ram.restaurant_id
                        AND ram.status = 'active' AND ram.member_id = '$theMember') order by title");
        foreach ($data as $row) {
            $this->restaurant[] = $row['restaurant'];
            $this->title[] = $row['title'];
        }
    }

    function getListAssignedAppManagerRestaurant($theMember) {
        // $data = pdo_multiple_select("SELECT restaurant, title
        //                 FROM restaurant h, restaurant_app_managers ram
        //                 WHERE h.RESTAURANT = ram.restaurant_id
        //                 AND ram.status = 'active' AND ram.user_id = '$theMember' order by title");
        $data = pdo_multiple_select("SELECT restaurant, title
                        FROM restaurant h, restaurants_managers ram
                        WHERE h.RESTAURANT = ram.restaurant_id
                        AND ram.status = 'active' AND ram.member_id = '$theMember' order by title");
        foreach ($data as $row) {
            $this->restaurant[] = $row['restaurant'];
            $this->title[] = $row['title'];
        }
    }

    function is_city($city) {

        if (empty($city)) {
            return false;
        }

        $res = false;

        foreach ($GLOBALS['citylist'] as $city_val) {
            if (strpos(strtolower($city_val), strtolower($city)) !== false) {
                return $city;
            }
        }
        return $res;
    }

    function getRestaurantByManager($manager) {
        // $data = pdo_multiple_select("SELECT r.restaurant, title, m.name as logo, is_wheelable, is_bookable, extraflag, reservedflag "
        //         . "FROM restaurant r, restaurant_app_managers ram, media m "
        //         . "WHERE 1 AND m.restaurant = r.restaurant 
        //             AND ram.status = 'active'
        //             AND m.object_type = 'logo'
        //             AND m.media_type = 'picture' 
        //             AND ram.restaurant_id = r.restaurant 
        //             AND ram.user_id = '" . $manager . "' ");
        $data = pdo_multiple_select("SELECT r.restaurant, title, m.name as logo, is_wheelable, is_bookable, extraflag, reservedflag "
                . "FROM restaurant r, restaurants_managers ram, media m "
                . "WHERE 1 AND m.restaurant = r.restaurant 
                    AND ram.status = 'active'
                    AND m.object_type = 'logo'
                    AND m.media_type = 'picture' 
                    AND ram.restaurant_id = r.restaurant 
                    AND ram.member_id = '" . $manager . "' ");        
        $res = array();
        foreach ($data as $row) {

            $is_walkable = '1';
            if ($row['extraflag'] & NO_WALKING) {
                $is_walkable = '0';
            }

            $res[] = array('restaurant' => $row['restaurant'], 'title' => $row['title'], 'logo' => $row['logo'], 'is_wheelable' => $row['is_wheelable'], 'is_bookable' => $row['is_bookable'], 'is_walkable' => $is_walkable);
        }
        return $res;
    }

    //getgrabz list restaurant

    function getgrabzListRestaurant() {
        $data = pdo_multiple_select("SELECT ID,restaurant,title,address,city,zip,extraflag from restaurant");
        foreach ($data as $row) {
            if ($row['extraflag'] & GRABZ) {
                $restaurant[] = array(
                    'id' => $row['restaurant'],
                    'name' => $row['title'],
                    'address' => $row['address'] . "," . $row['city'] . " " . $row['zip']
                );
            }
        }
        return $restaurant;
    }

    /////////    TMP FOR PAGING COUNT   /////

    function countQueryrestaurant($filters, $where, $return_array = false, $nb_items = NULL, $page = NULL) {

        $this->clear();

        //free search - default bar
        $this->where = $where;
        $sep = ($where != "") ? ' and ' : ' ';
        $limit = '';
        if (isset($filters['free_search'])) {

            $free_search_tmp = $filters['free_search'];
            $this->where .= $sep . "(r.title like '%$free_search_tmp%' OR "
                    . "r.description like '%$free_search_tmp%' OR "
                    . "r.address like '%$free_search_tmp%' OR "
                    //. "r.city like '%$free_search_tmp%' OR "
                    . "r.cuisine like '%$free_search_tmp%' OR "
                    . "m.item_description like '%$free_search_tmp%') AND ";
        }

        //is search = city
        if (isset($_SESSION['user']['forced_city'])) {
            $city_tmp = $_SESSION['user']['forced_city'];
            $this->where .= $sep . "r.city = '$city_tmp'";
            $sep = " and ";
        } else {
            if (isset($_SESSION['user']['search_city'])) {
                $city_tmp = $_SESSION['user']['search_city'];
                $this->where .= $sep . "r.city = '$city_tmp'";
                $sep = " and ";
            } else {
                $this->where .= $sep . "r.city = 'Singapore'";
                $sep = " and ";
            }
        }

        if (isset($_SESSION['user']['forced_country'])) {
            $country_tmp = $_SESSION['user']['forced_country'];
            $this->where .= $sep . "r.country_iso_code = '$country_tmp'";
            $sep = " and ";
        } else {
            if (isset($_SESSION['user']['search_country'])) {
                $country_tmp = $_SESSION['user']['search_country'];
                $this->where .= $sep . "r.country_iso_code = '$country_tmp'";
                $sep = " and ";
            } else {
                $this->where .= $sep . "r.country_iso_code = 'SG'";
                $sep = " and ";
            }
        }



        if (isset($filters['cuisine']) && is_array($filters['cuisine'])) {
            $cuis = "";
            $spcuis = "";
            while (list($label, $val) = each($filters['cuisine'])) {
                if (trim($val) == "")
                    continue;
                $cuis .= $spcuis . "r.cuisine like '%$val%'";
                $spcuis = " or ";
            }
            if ($cuis != "") {
                $this->where .= $sep . "( $cuis )";
                $sep = " and ";
            }
        }

        if ($filters['wheelselect'] >= 0 && !empty($GLOBALS['wheeloffers']) && count($GLOBALS['wheeloffers']) < $filters['wheelselect'] * 3) {
            $this->where .= "wheel like '%" . preg_replace("/%/", "_", $GLOBALS['wheeloffers'][($filters['wheelselect'] * 3)]) . "%'";
            $sep = " and ";
        }


        /////if the first search is empty we return all restaurants we have.
        // if ($no_filter) { $this->where  = $sep = '';  }


        if (in_array($_SESSION['user']['member_type'], array('admin', 'super_weeloy', 'weeloy_sales'))) {
            $this->where .= $sep . "  r.status IN ('active', 'comingsoon', 'demo', 'demo_reference') AND is_displayed = 1 ";
            $sep = " and ";
        } else {
            $this->where .= $sep . "  r.status IN ('active', 'comingsoon') AND is_displayed = 1 ";
            $sep = " and ";
        }


        $this->restaurant = $this->map = $this->title = "";

        $qry_clause = ($this->where != "") ? " LEFT JOIN menu m ON m.restaurant = r.restaurant where 1 AND " . $this->where . ' ORDER BY r.status ASC, wheelvalue DESC' : "";


        //PAGINATION 
        if (!empty($nb_items) && !empty($page)) {
            $limit = " LIMIT " . ($page - 1) * $nb_items . ", $nb_items";
        }

        $data = pdo_single_select("SELECT count(distinct(r.ID)) as nb from restaurant r" . $qry_clause . $limit);
        return $data['nb'];

        //error_log("<br><br>QUERY CLAUSE = " . $this->cn_row . "=> " . $qry_clause);
    }

    public function getRestaurantPaymentId($restaurant) {
        if (empty($restaurant)) {
            return false;
        }
         $data = pdo_single_select("SELECT * from restaurant_payment WHERE  restaurant_id ='$restaurant' LIMIT 1");
        if ($this->is_test_env()) {
            //$data = array();
            $data1 = $data;
             $paypal_id = (isset($data['paypal_id'])) ? $data['paypal_id'] : "";
            if($restaurant == 'SG_SG_R_TheOneKitchen'){
                
                $data1 = array(
                   'paypal_id' => $paypal_id,
                   'stripe_secret_key' =>'sk_test_0rGTeyhuIlv29sjHdTorZJw1',
                   'stripe_public_key' => 'pk_test_aG4RNDkBJI81YsT2ljQGVnuW'
               );
            }
            if($restaurant == 'SG_SG_R_TheFunKitchen'){
                
                $data1 = array(
                   'paypal_id' => $paypal_id,
                   'stripe_secret_key' =>'sk_test_KvJrLHXBKgNZNuZ6LfOF9avu',
                   'stripe_public_key' => 'pk_test_s3YV7s0qIDZr5bFPq0jVG1TC'
               );
            }
            if($restaurant == 'SG_SG_R_Bacchanalia'){
//                $data = array(
//                   'paypal_id' =>'payment3@weeloy.com',
//                   'stripe_secret_key' =>'sk_test_Bkihh0qYsuxncBn2Zd4W4pjv',
//                   'stripe_public_key' => 'pk_test_6Ltg6iQ3e36ovDXp9G2TqvRO'
//               );
                $data1 = array(
                   'paypal_id' => $paypal_id,
                   'stripe_secret_key' =>'sk_test_KvJrLHXBKgNZNuZ6LfOF9avu',
                   'stripe_public_key' => 'pk_test_s3YV7s0qIDZr5bFPq0jVG1TC'
               );
            }
           return $data1;
           
        }else{
            
            return $data;
        }

      
//        $data = pdo_single_select("SELECT id,paypal_id,stripe_secret_key,stripe_public_key from restaurant_payment WHERE  restaurant_id='$restaurant' LIMIT 1");
//        return $data;
    }
// mobile payment info
    public function chkPaymentInfo($restaurant, $test = '') {
        
        $res = new WY_restaurant;
        $payment = array();
        $getRest = $this->getRestaurant($restaurant);
      
        $policy = $this->getCancelPolicyAll($restaurant);
        $imploded = array();
        // foreach ($cancelPolicy['range'] as $cp) {
        //     $imploded[] = $cp['duration'];
        // }
        $cancelpolicy = $policy['message'];
        $payment_option = [];
        if ($this->checkbkdeposit() > 0) {
            
            $paymentKey = $this->getRestaurantPaymentId($restaurant);

            $stripe_public_key = (isset($paymentKey['stripe_public_key'])) ? $paymentKey['stripe_public_key'] : "";
            $paypal_id = (isset($paymentKey['paypal_id'])) ? $paymentKey['paypal_id'] : "";
            $payment_option[] = array(
                'method' => 'stripe',
                'public_key' => $stripe_public_key
            );
            if ($this->checkCreditCardDetails() > 0) {
                $payment['type'] = "CCDO";
                $payment['payment_option'] = $payment_option;
                $payment['cancelpolicy'] = $cancelpolicy;
            } else {
                $payment['type'] = "DP";
                $payment['payment_option'] = $payment_option;
                $payment['cancelpolicy'] = $cancelpolicy;
                if ($this->checkPaypalActive() > 0) {
                    $payment_option[] = array('method' => 'paypal', 'public_key' => $paypal_id);
                    $payment['payment_option'] = $payment_option;
                }
            }

            $payment['booking_deposit_lunch'] = $getRest['booking_deposit_lunch'];
            $payment['booking_deposit_dinner'] = $getRest['booking_deposit_dinner'];
            $payment['restaurant_tnc'] = $policy['message'];
        }
        return $payment;
    }
    private function isAuthorized() {
        $email = "";
        if (!empty($_SERVER['PHP_AUTH_USER']))
            $email = $_SERVER['PHP_AUTH_USER'];
        else if (!empty($_SESSION['user']['email']))
            $email = $_SESSION['user']['email'];
        return $email;
    }
    /////////    TMP FOR PAGING    /////
    function find($request) {
       require_once 'lib/class.service.inc.php';
       $service = new WY_Service();
       
       
        foreach ($request as $label => $value) {
            $request[$label] = preg_replace("/(l|L)(\'|\")/", "", $request[$label]);
            $request[$label] = preg_replace("/\'|\"/", "", $request[$label]);
        }
        $no_result = false;
        $selectjoin_both = '';
        $leftjoin_both = '';
        $where = 1;
        $where_both = " AND (r.status like  '%" . $request['status'] . "%'";
        if ($request['status'] == 'both') {
            $where_both = " AND ((r.status like 'active' OR r.status like 'comingsoon') ";
        }
        // for tester only
        $email = $this->isAuthorized();
        if (!empty($_SERVER['PHP_AUTH_USER'] = $email )) {
       
            $emailAr = array('chris.danguien@weeloy.com', 'qatester7822@gmail.com', 'craig.fong@weeloy.com', 'philippe.benedetti@weeloy.com', 'gaolinch@hotmail.com', 'shawn.chen@weeloy.com', 'joel.lai@weeloy.com', 'charlotte.donahue@weeloy.com', 'soraya.kefs@weeloy.com', 'diana.wong@weeloy.com', 'nicolas.finck@weeloy.com', 'jerome.arbault@weeloy.com', 'richard@kefs.me', 'vs.kala@weeloy.com', 'victor.tan@weeloy.com', 'mail.singhsarabjit@gmail.com');
            if (in_array($email, $emailAr) || preg_match("/@weeloy.com/i", $email) == true)
                $where_both .= " OR r.status like  '%reference%'";
            else {
                if (!empty($_SESSION['user']['email'])) {
                    $email = $_SESSION['user']['email'];
                    if (in_array($email, $emailAr) || preg_match("/@weeloy.com/i", $email) == true)
                        $where_both .= " OR r.status like  '%reference%'";
                }
            }
        }
        $where_both .= ')';
        $limit_both = '';

        $item_page = 48;
        if (!empty($request['i_p'])) {
            $item_page = 12;
        }
        //limit calculation
        if (!empty($request['p'])) {
            $start = ($request['p'] - 1) * $item_page;
            $limit_both = " LIMIT $start,$item_page";
        }

        $where_union1 = '';
        $join_union2 = '';
        $where_union2 = '';


        if (!empty($request['free_search'])) {
            $where_union1 .= " AND (r.title like '%" . $request['free_search'] . "%'";
            $where_union1 .= " OR r.cuisine like '%" . $request['free_search'] . "%')";
            $where_union2 .= " AND me.item_description like '%" . $request['free_search'] . "%'  AND me.restaurant = r.restaurant  ";
            $join_union2 = ', menu me';
        }

        if (isset($request['city']) && !empty($request['city'])) {
            $where_both .= " AND city like '%" . $request['city'] . "%'";
        }

        // Request pricing
        $low_limit = $high_limit = 0;

        if (isset($request['pricing']) && !empty($request['pricing'])) {
            $currency = $this->getCityCurrency($request['city']);
            $this->setPriceSegment($currency);
            $res_tmp = $this->glb_pricedesc;

            switch ($request['pricing']) {
                case '1':
                    $where_both .= " AND pricing < " . $res_tmp['$'];
                    //$high_limit = $res_tmp['$'];
                    break;
                case '2':
                    $where_both .= " AND pricing >= " . $res_tmp['$'] . "  AND pricing < " . $res_tmp['$$'];
                    break;
                case '3':
                    $where_both .= " AND pricing >= " . $res_tmp['$$'] . "  AND pricing < " . $res_tmp['$$$'];
                    break;
                case '4':
                    $where_both .= " AND pricing >= " . $res_tmp['$$$'] . "  AND pricing < " . $res_tmp['$$$$'];
                    break;
            }
        }

        if (isset($request['cuisine']) && !empty($request['cuisine'])) {

            $queryAr = explode("|", $request['cuisine']);
            $limit_cuisine = count($queryAr);
            if ($limit_cuisine < 1) {
                $this->msg = "food is empty";
                $this->result = -1;
                return;
            }

            for ($i = 0, $sep = " AND ("; $i < $limit_cuisine; $i++, $sep = " or ") {
                $where_both .= $sep . " cuisine like '%" . $queryAr[$i] . "%'";
            }
            $where_both.=')';
        }

        if (isset($request['tags']) && !empty($request['tags'])) {

            $queryAr = explode("|", $request['tags']);
            $limit_tags = count($queryAr);
            if ($limit_tags < 1) {
                $this->msg = "food is empty";
                $this->result = -1;
                return;
            }

            for ($i = 0, $sep = " AND ("; $i < $limit_tags; $i++, $sep = " or ") {
                $where_both .= $sep . " r.tags like '%[" . $queryAr[$i] . "]%'";
            }
            $where_both.=')';
        }


        if (isset($request['area']) && !empty($request['area'])) {

            $queryAr = explode("|", $request['area']);
            $limit_area = count($queryAr);
            if ($limit_area < 1) {
                $this->msg = "food is empty";
                $this->result = -1;
                return;
            }

            for ($i = 0, $sep = " AND ("; $i < $limit_area; $i++, $sep = " or ") {
                $where_both .= $sep . " r.region like '%" . $queryAr[$i] . "%'";
            }
            $where_both.=')';
        }
        
        
        $where_both = preg_replace("/_/", " ", $where_both);  // for Kuala Lumpur

        if (!empty($request['is_favorite']) && !empty($email)) {
            $where_both.= " AND is_favorite = '" . $request['is_favorite'] . "'";
        }


        if (!empty($email)) {
            $selectjoin_both = ", IF(is_favorite = '1', 1, 0) as is_favorite";
            $leftjoin_both = "  LEFT JOIN members_restaurants_favorite mrf  ON mrf.restaurant = r.restaurant AND mrf.member = '$email' ";
        }
        if(!empty($request['is_service'])){
           $resto = $service->getServiceRes($request['is_service']);
           if(count($resto)>0){
                $restolist = implode(",", $resto);
                $where = " r.restaurant in ($restolist)";
           }else{
               return array('restaurant' => [], 'no_result' => true);
           }
           //$leftjoin_both = "LEFT JOIN restaurant_service rs ON r.restaurant = rs.restaurant_id AND rs.service_id = '$service_id' ";
        }
        
        if(!empty($request['latitude']) && !empty($request['longitude'])){
            $city_code = $this->getCountryIsoCode($request['city']);
            
            $area = $this->getGeoArea($request['latitude'],$request['longitude'],$city_code);
            //error_log("area " .print_r($area,true));
            if(count($area)>0){
                $arealist = implode(",", $area);
                $where_both  = "AND r.region in ($arealist)";
            }else{
               return array('restaurant' => [], 'no_result' => true);
            }
        }
        $sql_order_both = " ORDER BY status ASC, wheelvalue DESC, morder ASC $limit_both";

        if(isset($request['tags'])){
            $sql_order_both = " ORDER BY status ASC, rand() $limit_both";
        }
   
            //$sql_order_both = " ORDER BY status ASC, wheelvalue DESC, morder ASC $limit_both";
       
        //error_log(__FILE__." ".__FUNCTION__." ".__LINE__." limit_both: ".$limit_both." sql: ".$sql_order_both);
        $sql_union1 = "SELECT distinct(r.restaurant), r.ID, r.title, r.hotelname, r.cuisine, r.city, r.status, m2.name as logo, r.address, r.address1, r.map, r.region, r.rating, r.zip, r.country, r.GPS, r.likes, r.currency,r.pricing, r.mealtype, r.rating, r.openhours, r.wheel, IF(is_wheelable = '0',10,r.wheelvalue) as wheelvalue, "
                . "is_displayed, is_wheelable,is_bookable, r.extraflag, m.path as image_path, m.name as image, m.morder $selectjoin_both
						FROM restaurant r $leftjoin_both, media m , media m2
						WHERE $where 
						AND m.restaurant = r.restaurant 
						AND m.status = 'active'
						AND m.object_type = 'restaurant'
						AND m.media_type = 'picture'
					
						AND m2.restaurant = r.restaurant 
						AND m2.status = 'active'
						AND m2.object_type = 'logo'
						AND m2.media_type = 'picture'
					
						AND r.is_displayed = '1'
						" . $where_both . " 
                                                " . $where_union1 . "
						GROUP BY r.id";

        $sql_union2 = " UNION SELECT distinct(r.restaurant), r.ID, r.title, r.hotelname, r.cuisine, r.city, r.status, m2.name as logo, r.address, r.address1, r.map, r.region, r.rating, r.zip, r.country, r.GPS, r.likes, r.currency,r.pricing, r.mealtype, r.rating, r.openhours, r.wheel, IF(is_wheelable = '0',10,r.wheelvalue) as wheelvalue, "
                . "is_displayed, is_wheelable,is_bookable, r.extraflag, m.path as image_path, m.name as image, m.morder $selectjoin_both
							FROM restaurant r $leftjoin_both, media m , media m2 $join_union2

						WHERE 1 

						AND m.restaurant = r.restaurant 
						AND m.status = 'active'
						AND m.object_type = 'restaurant'
						AND m.media_type = 'picture'
					
						AND m2.restaurant = r.restaurant 
						AND m2.status = 'active'
						AND m2.object_type = 'logo'
						AND m2.media_type = 'picture'
					
						AND r.is_displayed = '1'

						" . $where_both . " 
                                                " . $where_union2 . "
                                                     
						GROUP BY r.id";

        if (!empty($request['free_search'])) {
            $sql = $sql_union1 . $sql_union2 . $sql_order_both;
        } else {
            $sql = $sql_union1 . $sql_order_both;
        }

          
        $sql_tmp = str_replace('me.item description', 'me.item_description', $sql);
        $binlog = true;

        $restaurant = pdo_multiple_select($sql_tmp);

        if (count($restaurant) < 1 && !isset($request['second_try']) && (!isset($request['p']) || $request['p'] < 2)) {
            unset($request['pricing']);
            unset($request['cuisine']);
            unset($request['free_search']);
            $request['second_try'] = true;
            $no_result = true;
            return $this->find($request);
        }

        $data = array();
        foreach ($restaurant as $row) {
            if (!empty($row['restaurant'])) {
                // if(!empty($row['wheel']) && $row['is_wheelable']){

                $row['affiliate_program'] = ($row['extraflag'] & CORPORATEWHEEL) ? "cpp_credit_suisse" : 0;

                $row['best_offer'] = $this->getBestOffer($row['restaurant'], $row['is_wheelable'], 1, $row['affiliate_program']);
                // }

                $row['internal_path'] = $this->getRestaurantInternalPath($row['restaurant']);

                $this->setPriceSegment($row['currency']);
                $row['pricing'] = $this->PricingDollars($this->PriceMeal($row['pricing'], 3));
                $row['pricelunch'] = (preg_match('/Lunch/', $row['mealtype'])) ? $this->PricingDollars($this->PriceMeal($row['pricing'], 1)) : "";
                $row['pricediner'] = (preg_match('/Dinner/', $row['mealtype'])) ? $this->PricingDollars($this->PriceMeal($row['pricing'], 1)) : "";

                //$row['internal_path']'] = $this->getRestaurantInternalPath();
                $booktitle = "BOOK SOON";
                $custombutton = "custom_button_book_soon";


                if (($row['status'] == 'active' && $row['is_bookable']) || $row['status'] == 'demo_reference') {
                    $booktitle = "BOOK NOW";
                    $custombutton = "";
                }
                if ($row['is_bookable'] == false && ($row['status'] == 'active' || $row['status'] == 'demo_reference')) {
                    $booktitle = "REQUEST NOW";
                    $custombutton = "btn-green";
                }
                $book_btn_tmp = array('label' => '' . $booktitle, 'style' => $custombutton);
                if (!isset($row['is_favorite'])) {
                    $row['is_favorite'] = '0';
                }
                $row['book_button'] = $book_btn_tmp;

                $reviews = new WY_Review($row['restaurant']);
                $reviews_count = $reviews->getReviewsCount();
                if ($reviews_count['count'] > 0) {
                    $row['reviews'] = $reviews_count;
                }

                // $row['reviews'] = array('count'=>'234','score'=>'4.54', 'score_desc'=>'excellent');
                $data[] = $row;
            }
        }
        $this->result = 1;
        //error_log(__FILE__." ".__FUNCTION__." ".__LINE__.": ".count($data)." results retrieved: ");
        //error_log(__FILE__." ".__FUNCTION__." ".__LINE__.": results retrieved: ".print_r($data, true));
        return array('restaurant' => $data, 'no_result' => isset($request['second_try']));
    }

    function getQueryrestaurant($filters, $where, $return_array = false, $nb_items, $page) {

        $this->clear();

        //free search - default bar
        $this->where = $where;
        $sep = ($where != "") ? ' and ' : ' ';
        $limit = '';
        if (isset($filters['free_search'])) {

            $free_search_tmp = $filters['free_search'];
            $this->where .= $sep . "(r.title like '%$free_search_tmp%' OR "
                    . "r.description like '%$free_search_tmp%' OR "
                    . "r.address like '%$free_search_tmp%' OR "
                    //. "r.city like '%$free_search_tmp%' OR "
                    . "r.cuisine like '%$free_search_tmp%' OR "
                    . "m.item_description like '%$free_search_tmp%') AND ";
        }

        //is search = city
        if (isset($_SESSION['user']['forced_city'])) {
            $city_tmp = $_SESSION['user']['forced_city'];
            $this->where .= $sep . "r.city = '$city_tmp'";
            $sep = " and ";
        } else {
            if (isset($_SESSION['user']['search_city'])) {
                $city_tmp = $_SESSION['user']['search_city'];
                $this->where .= $sep . "r.city = '$city_tmp'";
                $sep = " and ";
            } else {
                $this->where .= $sep . "r.city = 'Singapore'";
                $sep = " and ";
            }
        }

        if (isset($_SESSION['user']['forced_country'])) {
            $country_tmp = $_SESSION['user']['forced_country'];
            $this->where .= $sep . "r.country_iso_code = '$country_tmp'";
            $sep = " and ";
        } else {
            if (isset($_SESSION['user']['search_country'])) {
                $country_tmp = $_SESSION['user']['search_country'];
                $this->where .= $sep . "r.country_iso_code = '$country_tmp'";
                $sep = " and ";
            } else {
                $this->where .= $sep . "r.country_iso_code = 'SG'";
                $sep = " and ";
            }
        }



        if (isset($filters['cuisine']) && is_array($filters['cuisine'])) {
            $cuis = "";
            $spcuis = "";
            while (list($label, $val) = each($filters['cuisine'])) {
                if (trim($val) == "")
                    continue;
                $cuis .= $spcuis . "r.cuisine like '%$val%'";
                $spcuis = " or ";
            }
            if ($cuis != "") {
                $this->where .= $sep . "( $cuis )";
                $sep = " and ";
            }
        }

        if ($filters['wheelselect'] >= 0 && !empty($GLOBALS['wheeloffers']) && count($GLOBALS['wheeloffers']) < $filters['wheelselect'] * 3) {
            $this->where .= "wheel like '%" . preg_replace("/%/", "_", $GLOBALS['wheeloffers'][($filters['wheelselect'] * 3)]) . "%'";
            $sep = " and ";
        }


        /////if the first search is empty we return all restaurants we have.
        // if ($no_filter) { $this->where  = $sep = '';  }


        if (in_array($_SESSION['user']['member_type'], array('admin', 'super_weeloy', 'weeloy_sales'))) {
            $this->where .= $sep . "  r.status IN ('active', 'comingsoon', 'demo', 'demo_reference') AND is_displayed = 1 ";
            $sep = " and ";
        } else {
            $this->where .= $sep . "  r.status IN ('active', 'comingsoon') AND is_displayed = 1 ";
            $sep = " and ";
        }


        $this->restaurant = $this->map = $this->title = "";

        $qry_clause = ($this->where != "") ? " LEFT JOIN menu m ON m.restaurant = r.restaurant where 1 AND " . $this->where . ' ORDER BY r.status ASC, wheelvalue DESC' : "";


        //PAGINATION 
        if (!empty($nb_items) && !empty($page)) {
            $limit = " LIMIT " . ($page - 1) * $nb_items . ", $nb_items";
        }

        $data = pdo_multiple_select("SELECT distinct(r.ID), r.restaurant, r.title, r.hotelname, r.cuisine, r.wheel, r.city, r.chef, r.chef_type, r.chef_award, r.chef_logo, r.chef_description, r.chef_origin, r.chef_believe, r.logo, r.url, r.description, r.address, r.address1, r.tel, r.email, r.smsid, r.map, r.region, r.rating, r.zip, r.country, r.creditcard, r.mealslot, r.award, r.GPS, r.likes, r.images, r.pricing, r.currency, r.rating, r.stars, r.openhours, r.bookhours, r.mealtype, r.dfmealtype, r.dfminpers, r.dfmaxpers, IF(is_wheelable = '0',10,r.wheelvalue) as wheelvalue, "
                . "is_displayed, is_wheelable,is_bookable, r.extraflag, r.reservedflag, r.status from restaurant r" . $qry_clause . $limit);


        $this->cn_row = count($data);

        foreach ($data as $row) {
            $this->ID[] = $row['ID'];
            $this->restaurant[] = $row['restaurant'];
            $this->title[] = $row['title'];
            $this->logo[] = $row['logo'];
            $this->cuisine[] = $row['cuisine'];
            $this->wheel[] = $row['wheel'];
            $this->city[] = $row['city'];
            $this->chef[] = $row['chef'];
            $this->chef_award[] = $row['chef_award'];
            $this->chef_logo[] = $row['chef_logo'];
            $this->chef_origin[] = $row['chef_origin'];
            $this->chef_believe[] = $row['chef_believe'];
            $this->chef_type[] = $row['chef_type'];
            $this->chef_description[] = $row['chef_description'];
            $this->description[] = $row['description'];
            $this->address[] = $row['address'];
            $this->address1[] = $row['address1'];
            $this->tel[] = $row['tel'];
            $this->email[] = $row['email'];
            $this->smsid[] = $row['smsid'];
            $this->url[] = $row['url'];
            $this->map[] = $row['map'];
            $this->region[] = $row['region'];
            $this->rating[] = $row['rating'];
            $this->zip[] = $row['zip'];
            $this->country[] = $row['country'];
            $this->creditcard[] = $row['creditcard'];
            $this->mealslot[] = $row['mealslot'];
            $this->dfmealtype[] = $row['dfmealtype'];
            $this->dfminpers[] = $row['dfminpers'];
            $this->dfmaxpers[] = $row['dfmaxpers'];
            $this->GPS[] = $row['GPS'];
            $this->likes[] = $row['likes'];
            $this->award[] = $row['award'];
            $this->images[] = $row['images'];
            $this->pricing[] = $row['pricing'];
            $this->currency[] = $currency = $row['currency'];
            $this->setPriceSegment($currency);
            $this->pricerating[] = $this->PricingDollars($this->PriceMeal($row['pricing'], 3));
            $this->pricediner[] = (preg_match('/Dinner/', $row['mealtype'])) ? $this->PricingDollars($this->PriceMeal($row['pricing'], 1)) : "";
            $this->pricelunch[] = (preg_match('/Lunch/', $row['mealtype'])) ? $this->PricingDollars($this->PriceMeal($row['pricing'], 2)) : "";
            $this->rating[] = $row['rating'];
            $this->stars[] = $row['stars'];
            $this->openhours[] = $row['openhours'];
            $this->bookhours[] = $row['bookhours'];
            $this->mealtype[] = $row['mealtype'];
            $this->wheelvalue[] = $row['wheelvalue'];
            $this->status[] = $row['status'];
            $this->is_displayed[] = $row['is_displayed'];
            $this->is_wheelable[] = $row['is_wheelable'];
            $this->is_bookable[] = $row['is_bookable'];
            $this->extraflag[] = $row['extraflag'];
            $this->reservedflag[] = $row['reservedflag'];

            $this->internal_path[] = $this->getRestaurantInternalPath($row['restaurant']);

            $this->dirname[] = __UPLOADDIR__ . $row['restaurant'];
            $this->showdirname[] = __SHOWDIR__ . $row['restaurant'];
        }
        if ($return_array) {
            return $data;
        }
        //error_log("<br><br>QUERY CLAUSE = " . $this->cn_row . "=> " . $qry_clause);
    }

    function clear() {
        unset($this->ID);
        unset($this->dirname);
        unset($this->showdirname);
        unset($this->restaurant);
        unset($this->hotelname);
        unset($this->title);
        unset($this->logo);
        unset($this->cuisine);
        unset($this->description);
        unset($this->address);
        unset($this->address1);
        unset($this->city);
        unset($this->country);
        unset($this->zip);
        unset($this->tel);
        unset($this->email);
        unset($this->smsid);
        unset($this->restaurant_tnc);
        unset($this->url);
        unset($this->map);
        unset($this->region);
        unset($this->pricing);
        unset($this->currency);
        unset($this->creditcard);
        unset($this->pricerating);
        unset($this->pricelunch);
        unset($this->pricediner);
        unset($this->rating);
        unset($this->mealslot);
        unset($this->dfmealtype);
        unset($this->dfminpers);
        unset($this->dfmaxpers);
        unset($this->laptime);
        unset($this->lastorderlunch);
        unset($this->lastorderdiner);
        unset($this->cutofflunch);
        unset($this->cutoffdiner);
        unset($this->bookinfo);
        unset($this->bookcritical);
        unset($this->award);
        unset($this->GPS);
        unset($this->likes);
        unset($this->status);
        unset($this->is_displayed);
        unset($this->is_wheelable);
        unset($this->is_bookable);
        unset($this->extraflag);
        unset($this->reservedflag);
        unset($this->wheel);
        unset($this->wheelversion);
        unset($this->chef);
        unset($this->chef_award);
        unset($this->chef_logo);
        unset($this->chef_origin);
        unset($this->chef_believe);
        unset($this->chef_description);
        unset($this->chef_type);
        unset($this->bo_name);
        unset($this->bo_tel);
        unset($this->bo_email);
        unset($this->mgr_name);
        unset($this->mgr_tel);
        unset($this->mgr_email);
        unset($this->POS);
        unset($this->data);
        unset($this->images);
        unset($this->ReviewData);
        unset($this->MenuObj);
        unset($this->Timeline);
        unset($this->where);
        unset($this->cn_row);
        unset($this->msg);
    }

    static function getAWSEmailRestaurantStatus($restaurant) {
        $sql = "SELECT aws_email_verification FROM restaurant WHERE restaurant LIKE '$restaurant'";
        $data = pdo_single_select($sql);
        if (count($data) > 0) {
            return $data['aws_email_verification'];
        } else {
            return '';
        }
    }

    function getAWSEmailVerificationStatus($email) {
        $sql = "SELECT aws_email_verification FROM restaurant WHERE email LIKE '$email'";
        $data = pdo_single_select($sql);
        if (count($data) > 0) {
            return $data['aws_email_verification'];
        } else {
            return 0;
        }
    }

    function updateAWSEmailVerification($email, $status) {
        $sql = "UPDATE restaurant SET aws_email_verification = '$status' WHERE email LIKE '$email'";
        if (pdo_exec($sql)) {
            return 1;
        } else {
            return 0;
        }
    }

    //add and update area function
    function addGeoArea($country, $city, $area, $lat, $lng, $weeloyName) {

        $query = "SELECT  name FROM geo_area WHERE name ='$area'  LIMIT 1 ";
        $data = pdo_single_select($query);
        if (count($data) == 0) {

            $sql = "INSERT INTO geo_area (country,region,url,name,latitude,longitude,weeloy_name) VALUES ('$country','$city','','$area','$lat','$lng','$weeloyName')";
            pdo_exec($sql);
            return 1;
        } else {
            return 0;
        }
    }

    function updateCusion($cuisine, $type) {
        $query = "SELECT  cuisine FROM cuisine WHERE cuisine ='$cuisine'  LIMIT 1 ";
        $data = pdo_single_select($query);
        if (count($data) == 0) {
            if ($type == 'add') {
                $sql = "INSERT INTO cuisine (cuisine) VALUES ('$cuisine')";
            }
            if ($type == 'delete') {
                $sql = "DELETE FROM cuisine WHERE cuisine ='$cuisine' ";
            }
            pdo_exec($sql);
            return 1;
        } else {
            return 0;
        }
    }
    
    public function getGeoArea($lan,$lon,$city){
        $area =[];
        $sql = "SELECT name,latitude, longitude, 3956 * DEGREES(ACOS(COS(RADIANS($lan))
                * COS(RADIANS(latitude))
                * COS(RADIANS(longitude) - RADIANS($lon))
                + SIN(RADIANS($lan))
                * SIN(RADIANS(latitude))))
                AS distance_in_km
               FROM geo_area WHERE country='$city' ORDER BY distance_in_km ASC limit 10";
        $data = pdo_multiple_select($sql);
        if(count($data)>0){
                foreach ($data as $row) {
                    if(!empty($row['name'] ))
                    $area[] = "'" . $row['name'] . "'";
                }
       }
           
        return $area;
    } 
               

    function getcuisinilist($status) {
        $where = '';
        if ($status != '') {
            $where = " WHERE status = '$status' ";
        }
        $sql = "SELECT distinct ID, cuisine, status FROM cuisine $where ORDER BY cuisine";
        $data = pdo_multiple_select($sql);
        return $data;
    }

    function updateArea($country, $updateId, $area, $weeloyName, $lat, $lng, $method) {
        $query = "SELECT  name FROM geo_area WHERE country ='$country' and id ='$updateId' LIMIT 1 ";
        $data = pdo_single_select($query);
        if (count($data) > 0) {
            if ($method == 'update') {
                $query = "UPDATE geo_area set name ='$area',weeloy_name ='$weeloyName',latitude='$lat',longitude='$lng' where id ='$updateId' ";
            } else if ($method == 'remove') {
                $query = "DELETE from geo_area where id ='$updateId' ";
            }
            pdo_exec($query);
            return 1;
        } else {
            return 0;
        }
    }

    //Project-MD Start
    
    function md_restaurant() {

        $result = array();

        $sql = "SELECT res.restaurant, res.title, res.id AS restID, res.restogeneric, cont.mobile, cont.id AS contID"
                . " FROM restaurant res, restaurant_contacts cont"
                . " WHERE res.status IN ('active', 'demo_reference') AND res.restaurant = cont.restaurant AND cont.job_title = 'res-manager' ORDER BY title ASC";
        $data = pdo_multiple_select($sql);

        foreach ($data as $key => $resto) {
            $this->restogeneric = $resto['restogeneric'];
            $md_verified = $this->readGeneric('md_verified');
            
            if ($md_verified == 'N' OR $md_verified == '') {
                array_push($result, $resto);
            }
            
        }
        return $result;
    }


    function md_confirmOTP($restaurant, $otp) {

        $sql = "SELECT res.restaurant, res.restogeneric"
                . " FROM restaurant res"
                . " WHERE res.restaurant = '$restaurant' LIMIT 1";
        $data = pdo_multiple_select($sql);

        foreach ($data as $key => $resto) {
            $this->restogeneric = $resto['restogeneric'];
            $md_otp = $this->readGeneric('md_otp');
            
            if ($md_otp === $otp ) {
                $result = 1;
            } else {
                $result = 0;
            }
            
        }
        return $result;
    }

    //Project-MD Finish

    function updateCuisine($cuisine, $cuisineID, $method) {
        $query = "SELECT  cuisine FROM cuisine WHERE cuisine ='$cuisine'  LIMIT 1 ";
        $data = pdo_single_select($query);
        if (count($data) > 0) {
            if ($method == 'update') {
                $sql = "UPDATE cuisine set cuisine ='$cuisine' where ID ='$cuisineID' ";
            }
        }

        try {
            $db = getConnection();
            $stmt = $db->prepare($query);
            $stmt->execute();
            $cuisineCount = $stmt->fetchAll(PDO::FETCH_OBJ);
            if ($method == 'update') {
                $msg = $cuisine;
                $q = "UPDATE cuisine set cuisine ='$cuisine' where ID ='$cuisineID' ";
            } else {
                if (count($cuisineCount) > 0) {
                    $status = $method == 'pending' ? 'pending' : 'active';
                    $msg = $status;
                    $q = "UPDATE cuisine set status ='$status' where cuisine ='$cuisine' ";
                } else {
                    $errors = "Cuisine not found in database";
                    echo format_api(1, $cuisine, 1, $errors);
                }
            }
            $stmt = $db->prepare($q);
            $stmt->execute();
            $errors = null;
            echo format_api(1, $msg, 1, $errors);
        } catch (PDOException $e) {
            api_error($e, "app tracking addCuisine");
        }
    }

    function clean($str) {

        if ($str[0] == "|") {
            $str = substr($str, 1);
        }

        if ($str[strlen($str) - 1] == "|") {
            $str = substr($str, 0, strlen($str) - 1);
        }

        $str = preg_replace("/\|\|+/", "|", $str);
        $str = preg_replace("/\s+/", " ", $str);
        $str = trim($str);

        return $str;
    }

    function PricingDollars($pricing) {
        reset($this->glb_pricedesc);
        //error_log(__FILE__." ".__FUNCTION__." ".__LINE__.": pricing: ".$pricing);//.", glb_pricedesc: ".print_r($old_glb_pricedesc,true)." -> ".print_r($this->glb_pricedesc,true));
        while (list($label, $value) = each($this->glb_pricedesc))
            if ($pricing < $value) {
                //error_log(__FILE__." ".__FUNCTION__." ".__LINE__.": label: ".$label);
                return $label;
            }
        //error_log(__FILE__." ".__FUNCTION__." ".__LINE__.": label: $$$$$$");
        return "$$$$$$";
    }

    function PriceMeal($value, $choice) {
        if (empty($value))
            return 0;

        $dd = explode(",", $value);
        if (count($dd) < 1)
            return 0;

        if (count($dd) < 2)
            $dd[1] = 0;

        $dd[0] = intval($dd[0]);
        $dd[1] = intval($dd[1]);
        if ($choice == 1) {
            //error_log(__FILE__." ".__FUNCTION__." ".__LINE__." value: ".$value.", choice: ".$choice.", dd[0]: ".$dd[0]);
            return $dd[0];
        }
        if ($choice == 2) {
            //error_log(__FILE__." ".__FUNCTION__." ".__LINE__." value: ".$value.", choice 2: dd[0]: ".$dd[1]);
            return $dd[1];
        }
        //error_log(__FILE__." ".__FUNCTION__." ".__LINE__." value: ".$value.", choice: ".$choice." result: ".floor(($dd[0] + $dd[1]) / 2));
        return floor(($dd[0] + $dd[1]) / 2);
    }

    function getAreaRes($citycode, $area) {
        $sql = "SELECT  title FROM restaurant  where address like '%$area%' AND city_iso_code ='$citycode'";
        return pdo_multiple_select_index($sql);
    }

    public function getActivePromotion($restaurant, $cpp_type = NULL) {
        try {
            //$data = pdo_single_select("SELECT  offer, description FROM promotion WHERE restaurant = '$restaurant' AND start <= '" . date("Y-m-d") . "' AND end >= '" . date("Y-m-d") . "'  AND cpp_type LIKE '' ORDER BY value  LIMIT 1");
            if (isset($scp_type) && !empty($cpp_type)) {
                $data = pdo_single_select("SELECT  offer, description FROM promotion WHERE restaurant = '$restaurant' AND start <= '" . date("Y-m-d") . "' AND end >= '" . date("Y-m-d") . "'  AND cpp_type LIKE '$cpp_type' ORDER BY value  LIMIT 1");
                if (count($data) > 0) {
                    $data['offer_cpp'] = $data['offer'];
                    $data['description_cpp'] = $data['description'];
                    return $data;
                }
            } else
                $data = pdo_single_select("SELECT  offer, description FROM promotion WHERE restaurant = '$restaurant' AND start <= '" . date("Y-m-d") . "' AND end >= '" . date("Y-m-d") . "'  AND cpp_type LIKE '' ORDER BY value  LIMIT 1");
        } catch (Exception $e) {
            error_log(__FILE__." ".__FUNCTION__." ".__LINE__." Exception: ".$e);
        }
        return $data;
    }

    function getActiveCities($public_cities = true, $private_cities = false) {
        if ($public_cities && $private_cities)
            $where = '';
        if ($public_cities && !$private_cities)
            $where = ' AND public_city = 1 ';
        if (!$public_cities && $private_cities)
            $where = ' AND public_city = 0 ';
        $sql = "SELECT geo_city.id, city, city_iso_code, geo_city.country_iso_code, country FROM geo_city, geo_country WHERE geo_city.country_iso_code = geo_country.country_iso_code AND  geo_city.status = 'active' AND  geo_country.status = 'active' $where";
        $data = pdo_multiple_select($sql);
        return $data;
    }

    function getCityIsoCode($city) {
        $data = pdo_single_select("SELECT id,city_iso_code FROM geo_city WHERE geo_city.city = '$city'");
        return $data['city_iso_code'];
    }

    function getCountryIsoCode($country) {
        $sql = "SELECT country_iso_code FROM geo_country WHERE geo_country.country = '$country'";
        $data = pdo_single_select($sql);
        return $data['country_iso_code'];
    }

    function getCountryCityList() {
        $sql = "SELECT country, geo_city.country_iso_code as countrycode, city, geo_city.city_iso_code as citycode FROM geo_city, geo_country WHERE geo_country.country_iso_code = geo_city.country_iso_code";
        $data = pdo_multiple_select($sql);
        if(count($data) < 1)
        	return array();
        
        $countryAr = array();
        $cityAr = array();
        foreach($data as $row) {
        	$country = trim(strtolower($row['country']));
        	$country = preg_replace("/ /", "-", $country);
        	$city = trim(strtolower($row['city']));
        	$city = preg_replace("/ /", "-", $city);
        	$countryAr[$country] = $row['countrycode'];
       		$countryAr[$city] = $row['countrycode'];
        	$cityAr[$city] = $row['citycode'];
        	}
        
        return array('country' => $countryAr, 'city' => $cityAr);
     }

    function getCountyList($public_countries = true, $private_countries = false) {

        if ($public_countries && $private_countries) {
            $where = '';
        }
        if ($public_countries && !$private_countries) {
            $where = ' AND public_country = 1 ';
        }
        if (!$public_countries && $private_countries) {
            $where = ' AND public_country = 0 ';
        }

        $sql = "SELECT id, country_iso_code, country FROM geo_country WHERE status = 'active' $where";
        $data = pdo_multiple_select($sql);
        return $data;
    }

    function getAreaList($region = "SG") {

        $data = pdo_multiple_select("SELECT id,country,region,name, weeloy_name,latitude,longitude FROM geo_area WHERE region ='$region' ORDER BY name ASC ");
        return $data;
    }

    static function getRestaurantPabx($virtual) {
    	$virtualp = "+" . $virtual;
        return pdo_single_select("SELECT restaurant, tel as fixline, fax as virtual FROM restaurant WHERE fax = '$virtual' or fax = '$virtualp' limit 1");
     }

    static function getCurrencyList() {
        $sql = "SELECT money FROM geo_country WHERE status = 'active' order by money";
        return pdo_multiple_select_index($sql);
     }

    function getCountryCurrency($country) {
        $sql = "SELECT money FROM geo_country WHERE status = 'active' AND country LIKE '$country' limit 1";
        $data = pdo_single_select($sql);
        return $data['money'];
    }

    function getCityCurrency($city) {
        $sql = "SELECT geo_country.money FROM geo_country, geo_city WHERE  geo_country.country_iso_code = geo_city.country_iso_code AND geo_country.status = 'active' AND geo_city.city LIKE '$city' limit 1";
        $data = pdo_single_select($sql);
        return $data['money'];
    }

    public function getRestaurantByFacebookPageId($facebook_pid) {

        if (empty($facebook_pid))
            return array();

        $info = pdo_single_select("SELECT ID, restaurant, facebook_pid from restaurant WHERE facebook_pid LIKE '%$facebook_pid%' LIMIT 1");
        if (count($info) > 0 && !empty($info['restaurant'])) {
            $clus = new WY_Cluster;
            $clus->read("SLAVE", $facebook_pid, "FACEBOOKID", "", "", "");
            $content = ($clus->result > 0) ? $clus->clustcontent[0] : "";
            $data = $this->getRestaurant($info['restaurant']);
            if (count($data) > 0)
                $this->cluster = $content;
            return $data;
        }
        return array();
    }
    
    public function getRestPaypalId($restaurant) {
        if (empty($restaurant)) {
            return false;
        }
        $data = pdo_single_select("SELECT id,paypal_id from restaurant_payment WHERE  restaurant_id='$restaurant' LIMIT 1");
        return $data;
    }

    public function getRestaurantMinMaxPax($restaurant) {
        if (empty($restaurant)) {
            return false;
        }
        $data = pdo_single_select("SELECT dfminpers as min_pax, dfmaxpers as max_pax from restaurant WHERE  restaurant LIKE '$restaurant' LIMIT 1");
        return $data;
    }

    public function getRestaurantInternalPath($restaurant = NULL) {
        if (empty($restaurant))
            $restaurant = $this->restaurant;
        $restaurant_details = explode('_', $restaurant);
        //foreach ($restaurant_details as $details){
        //}
        //$type = $restaurant_details[2];
        $type = 'restaurant';
        $city = "";
        if (count($restaurant_details) >= 2) {
            switch ($restaurant_details[0]) {
                case 'SG':
                    $country = 'singapore';
                    break;
                case 'HK':
                    $country = 'hong-kong';
                    break;
                case 'TH':
                    $country = 'thailand';
                    break;
                case 'MY':
                    $country = 'malaysia';
                    break;
                case 'FR':
                    $country = 'france';
                    break;
                case 'KR':
                    $country = 'korea';
                    break;
                case 'IN':
                    $country = 'india';
                    break;
                default:
                    $country = 'singapore';
                    break;
            }
            switch ($restaurant_details[1]) {
                case 'SG':
                    $city = 'singapore';
                    break;
                case 'HK':
                    $city = 'hong-kong';
                    break;
                case 'BK':
                    $city = 'bangkok';
                    break;
                case 'PK':
                    $city = 'phuket';
                    break;
                case 'KL':
                    $city = 'kuala-lumpur';
                    break;
                case 'PR':
                    $city = 'paris';
                    break;
                case 'SE':
                    $city = 'seoul';
                    break;
                case 'MU':
                    $city = 'mumbai';
                    break;
                default:
                    $city = 'singapore';
                    break;
            }
        }
        $restaurant_name = preg_replace('/([A-Z])/', '-$1', str_replace('_', '', substr($restaurant, 8)));
        $restaurant_name = trim($restaurant_name, '-');
        $restaurant_name = strtolower($restaurant_name);

        // if($country == $city){
        //     $restaurant_url = $type . '/' . $country . '/' . $restaurant_name;
        // }else{
        //     $restaurant_url = $type . '/' . $country . '/' . $city . '/' . $restaurant_name;
        // }
        $restaurant_url = $type . '/' . $city . '/' . $restaurant_name;
        return $restaurant_url;
    }

    function restaurantDescriptionToArray($description) {

        $descriptionAR = array();
        $tmp = explode('||||', $description);
        $cleaner = array('<br>', '<br/>', '</br>', '<b>', '</b>');
        foreach ($tmp as $t) {
            if (!empty(trim($t))) {


                $itemDesc = array();
                $title = $this->get_string_between($t, "<b>", "</b>");
                $title = str_replace($cleaner, '', $title);
                $itemDesc['title'] = trim($title);

                $body = str_replace($title, '', $t);
                $tmpbody = explode('<br/>', trim($body));
                $tmpbody = str_replace($cleaner, '', $tmpbody);
                $tmpAr = array();
                foreach ($tmpbody as $tmp) {
                    if (!empty(trim($tmp))) {
                        $tmpAr[] = trim($tmp);
                    }
                }
                $itemDesc['body'] = $tmpAr;

                $descriptionAR[] = $itemDesc;
            }
        }
        return $descriptionAR;
    }

    function get_string_between($string, $start, $end) {
        $string = " " . $string;
        $ini = strpos($string, $start);
        if ($ini == 0)
            return "";
        $ini += strlen($start);
        $len = strpos($string, $end, $ini) - $ini;
        return substr($string, $ini, $len);
    }

    function getTokenUrl($restaurant, $url, $type) {
        $restaurant = trim($restaurant);
        $trackingUrl = $url;
        $sql = "SELECT campaign_token FROM marketing_campaigns mc WHERE mc.restaurant_id  = '$restaurant' AND mc.type='$type' LIMIT 1 ";
        $data = pdo_single_select($sql);

        if (!empty($data['campaign_token'])) {
            $trackingUrl = 'https://tracking.weeloy.com/' . $data['campaign_token'];
        }
        return $trackingUrl;
    }

    function getListAssignedTagRestaurant($tag) {
        $tag = '[' . $tag . ']';
        return pdo_multiple_select_index("SELECT restaurant FROM restaurant where tags LIKE '%$tag%' ORDER BY restaurant ASC ");
    }

    function getListNotAssignedTagRestaurant($tag) {
        $tag = '[' . $tag . ']';
        return pdo_multiple_select_index("SELECT restaurant FROM restaurant where tags !='$tag' ORDER BY restaurant ASC ");
    }
    function getTags() {
        return pdo_multiple_select("SELECT distinct(tags) FROM restaurant");
    }
    function updateTag($tag, $restaurant) {
        $tag = '[' . $tag . ']';
        $data = pdo_single_select("SELECT restaurant,tags FROM restaurant where restaurant ='$restaurant' ");
        //foreach ($data as $row) {
        if (isset($data['tags'])) {
            if (strpos($data['tags'], $tag) !== false) {
                $tags = $data['tags'];
            } else {
                $tags = $data['tags'] . $tag;
            }
            $res = $data['restaurant'];
            $sql = "UPDATE restaurant SET tags = '$tags' WHERE restaurant='$restaurant'";
            pdo_exec($sql);
        }
        return 1;
    }

    function removeTag($tag, $restaurant, $type) {

        if ($type === 'all') {
            $query = "DELETE from tag_list where name ='$tag' ";
            pdo_exec($query);
            $data = pdo_multiple_select("SELECT restaurant,tags FROM restaurant where tags LIKE '%$tag%' ORDER BY restaurant ASC ");
            foreach ($data as $row) {
                $tag = '[' . $tag . ']';
                $tags = str_replace($tag, '', $row['tags']);
                $res = $row['restaurant'];
                $sql = "UPDATE restaurant SET tags = '$tags' WHERE restaurant  ='$res'";
                pdo_exec($sql);
            }
        }
        if ($type === 'tag') {
            $tag = '[' . $tag . ']';
            $data = pdo_single_select("SELECT restaurant,tags FROM restaurant where restaurant ='$restaurant' ");
            if (isset($data['tags'])) {
                $tags = str_replace($tag, '', $data['tags']);
                $sql = "UPDATE restaurant SET tags = '$tags' WHERE restaurant  ='$restaurant'";
                pdo_exec($sql);
            }
        }
        return 1;
    }

    function ischangedUrl($theRestaurant, $url) {
        $data = pdo_single_select("SELECT url FROM restaurant WHERE restaurant = '$theRestaurant' LIMIT 1");

        if ($data['url'] === $url) {
            return false;
        } else {
            return true;
        }
    }

    function getTaglist() {
        $data = pdo_multiple_select("SELECT restaurant,tags FROM restaurant WHERE tags!=''");
        $tmpArr = array();
        if (count($data) > 0) {
            foreach ($data as $row) {
                $tags = explode(']', $row['tags']);
                foreach ($tags as $tg) {
                    if ($tg != "") {
                        $tag = str_replace(array('['), '', $tg);
                        array_push($tmpArr, $tag);
                    }
                }
            }
            //var_dump($tmpArr);
            return array_unique($tmpArr);
        }
    }

    public function saveCancelPolicy($policy) {

        $restaurant = $policy['restaurant'];
        $start = (isset($policy['start'])) ?  $policy['start'] : '';
        $end = (isset($policy['end'])) ?  $policy['end'] : '';
        $product = (isset($policy['product'])) ?  $policy['product'] : '';
//        $data1 = pdo_single_select("SELECT * FROM restaurant_cancel_policy  where restaurant LIKE '%$restaurant%' AND product LIKE '%$product%' ");
//        if (count($data1 > 0)) {
//            pdo_exec("delete from restaurant_cancel_policy where restaurant LIKE '%$restaurant%' AND product LIKE '%$product%' ");
//        }
        $query = "INSERT INTO restaurant_cancel_policy (restaurant,duration,percentage,belowperct,product,lunch_charge,dinner_charge,start,end,pax) VALUES ('$restaurant','{$policy['duration']}','{$policy['percentage']}','{$policy['belowperct']}','$product','{$policy['lunch']}','{$policy['dinner']}','$start','$end','{$policy['pax']}')";
        $data = pdo_insert($query);
//        $query_parts = array();
//        $rows = $policy['data'];
//        for ($x = 0; $x < count($rows); $x++) {
//            $query_parts[] = "('" . $restaurant . "', '" . $rows[$x]['range'] . "','" . $rows[$x]['policy'] . "','" . $product . "','" . $rows[$x]['lunch_charge'] . "','" . $rows[$x]['dinner_charge'] . "')";
//        }
//        $query .= implode(',', $query_parts);
     
        return $data;
    }
    public function updateCancelPolicy($policy) {

        $restaurant = $policy['restaurant'];
        $start = (isset($policy['start'])) ?  $policy['start'] : '';
        $end = (isset($policy['end'])) ?  $policy['end'] : '';
        $product = (isset($policy['product'])) ?  $policy['product'] : '';
        error_log("POICY".print_r($policy,true));
        
        $sql = "UPDATE restaurant_cancel_policy SET duration ='{$policy['duration']}',percentage ='{$policy['percentage']}',belowperct ='{$policy['belowperct']}',product ='$product',lunch_charge ='{$policy['lunch']}',dinner_charge ='{$policy['dinner']}',start ='$start',end ='$end',pax ='{$policy['pax']}' WHERE restaurant  ='$restaurant' and id ='{$policy['id']}'";
        pdo_exec($sql);

        return 1;
    }
    public function deleteCancelPolicy($policy) {
       $restaurant = $policy['restaurant'];
       pdo_exec("delete from restaurant_cancel_policy where restaurant LIKE '%$restaurant%' AND id ='{$policy['id']}' ");
       return 1;
    }

    public function getCancelPolicyAll($restaurant, $type = '', $amount = 0, $product = '',$date ='',$time ='',$pax = '1') {
   
		$this->result = 1;
        $restaurant = $this->clean_input($restaurant);
		if($this->restaurant != $restaurant)
			$this->getRestaurant($restaurant);
		
		if($this->result < 0) {
            WY_debug::recordDebug("INVALID RESTARUANT", "getCancelPolicyAll RESTAURANT", $restaurant);
			return;
			}

        $isChksplconfig = false;
        $where = " AND product LIKE ''  ";
        $isPerPax = 11;
        
        if(!empty($product)){
            $where = " AND product LIKE '$product'";
        }
        if($this->restaurant == 'SG_SG_R_Pollen' || $this->restaurant == 'SG_SG_R_LaTableDeLydia' ){
            $isPerPax = 12;
            $festivedate ='2017-05-14';
            $product = (intval($pax) == 5) ? 'fivepax' : 'abovefivepax'; 
            if (strtotime($festivedate) == strtotime($date) ){
                $product ='festive';
              
            }
             $where = " AND product LIKE '$product'";
        }
        $burntEndsProd = array('chef table','bar seats', 'counter seats');
        $amara_group = array('SG_SG_R_SilkRoad', 'SG_SG_R_Element');
        if (in_array($this->restaurant, $amara_group, true)) {
            $rdate = date('Y-m-d',strtotime($date));
            $isChksplconfig = true;
            $where = " AND product LIKE '' AND start <= '$rdate' AND end >= '$rdate' ";
            if(!empty($product)){
                $where = "AND product LIKE '$product' AND start <= '$rdate' AND end >= '$rdate' ";
            }

        }
        if ($type === 'admin') {
            $where ='';
        }
        
        $sql = "SELECT * FROM restaurant_cancel_policy where restaurant ='$this->restaurant' $where";
        if ($type === 'admin') {
           $data = pdo_multiple_select($sql);
            return $data;
        }
       
        $data = pdo_single_select($sql);
  
        $record = [];
        $range = [];
        $message = '';
        $lastRange = "";
        $payment_type = "";
        $charge = "";
        $isRequired = false;

      
        if ($this->checkbkdeposit() > 0) {
            $isRequired = true;
            $payment_type = ($this->checkCreditCardDetails() > 0) ? "CCDO" : 'DP';
        }
        $isReddot = ($this->checkreddot() > 0) ? 1 : 0;
        if(count($data) <= 0){
             $isRequired = false;
        }
        $msg1 ='';
        $chkbendsflg = 14;
        if (($restaurant == 'SG_SG_R_BurntEnds' || $restaurant == 'SG_SG_R_TheOneKitchen')  && !in_array(trim(strtolower($product)), $burntEndsProd, true)) {
             $chkbendsflg = 13;
        }
        if (count($data) >0) {
            $msgflg ='';
            if($isPerPax == 12){
                $isRequired =  (intval($pax) >= intval($data['pax'])) ? true : false; 
            }

            $pax = $data['pax'];
       
            $meal_type =  (empty($time)) ? 'lunch' : WY_Allote::getMealType($time) ; 
            $lastRange = $data['duration'];
            $charge =  ($meal_type == 'lunch') ? $data['lunch_charge'] : $data['dinner_charge'] ;
            if ($isChksplconfig) {
                $charge = $charge * 1.10 * 1.07 ;
                $charge = round($charge, 2);//gst & service charge
                $msgflg =' (inclusive of GST & service charge) per guest';
                $amount = $charge;
                $msg1 = "Any cancellation received 7 days prior to booking date will incur a minimum charge of SGD 80.00 nett."; 
            }
            $message = $msg1." Cancellation received within " . $data['duration'] . " hours prior to booking date will incur a charge of " . $this->currency ." " .$amount. $msgflg.". Failure to arrive at the restaurant will be treated as a no-show and the same amount will be charged. ";
            if($chkbendsflg == 13){
                $message = "In the event that the booking is not honoured in whole or in part by you, or is cancelled outside the permitted time frame of 3 working days prior, a 100% cancellation charge applies.";
            }
            if($isReddot){
                $message = "Restaurant will be collecting (Deposit amount " . $this->currency ." ".$data['lunch_charge'].")nett deposit upon booking " .$data['pax']." peoples and above. Deposits are non-refundable within ". $data['duration'] ." hours,  more than ".$data['duration']."  hours will be refund SGD " . $charge . "nett. Should you require any assistance in cancelling a booking, please contact the restaurant directly. ";
                if($isPerPax == 12 && $this->restaurant === 'SG_SG_R_Pollen'){
                    if(intval($data['pax']) == 5){

                       $deposit = $charge * intval($data['pax']);
                       $refundamount = 45.00;
                       //$message = "Restaurant will be collecting (Deposit amount " .$this->currency." ".$deposit.")nett deposit upon booking " .$data['pax']." peoples and above. Deposits are non-refundable within ". $data['duration'] ." hours,  more than ".$data['duration']."  hours will be refund SGD " . $refundamount . "nett. For reservation of more than 5 peoples, please call the restaurant directly at +65 6604 9988. ";
                       $message = $this->readGeneric('frontbook');
                    }else{
                       $deposit = $charge;
                       $refundamount = ($charge * intval($data['pax']))- 5;
                       //message = "Restaurant will be collecting (Deposit amount " .$this->currency." ".$deposit."/person)nett deposit upon booking " .$data['pax']." peoples and above. Deposits are non-refundable within ". $data['duration'] ." hours,  more than ".$data['duration']."  hours will be refunded SGD " . $refundamount . " nett. Should you require any assistance in cancelling a booking, please call the restaurant directly at +65 6604 9988";
                       $message = $this->readGeneric('ccbook');
                    }  
                    if($product == 'festive'){
                            $message = $this->readGeneric('festivebook');
                       } 
                }
//                  $message = "Restaurant will be collecting (Deposit amount " .$this->currency." ".$data['lunch_charge'].")nett deposit upon booking " .$data['pax']." peoples and above. Deposits are non-refundable within ". $data['duration'] ." hours,  more than ".$data['duration']."  hours will be refund SGD " . $charge . "nett. Should you require any assistance cancelling a booking, please contact the restaurant directly. ";
            }
            $range = array('duration' => "Any cancellation received within " . $data['duration'] . " hours prior to booking date will incur a charge of ", "percentage" => 100);
            if($restaurant == 'SG_SG_R_Nouri'){
                $message = $this->readGeneric('frontbook');
                if($meal_type === 'lunch'){
                    $isRequired = false;
                }
            }
            if($restaurant == 'SG_SG_R_Esquina'){
                $currency = "SGD";
                $charge = $data['lunch_charge'];
                $amount = $charge;
                $datetime = date('Y-m-d H:i:s', strtotime($date));
                $time_dateTo = strtotime($datetime);
                $interval = $time_dateTo - strtotime("now");
                $interval_hrs = $interval / (60 * 60);
                if($interval_hrs < $data['duration'] ){
                    $amount = 0;
                }
                $ref_amount = 45;
                $range = array('duration' => 'until  ' . $data['duration'] . ' hrs before the booking date No refund ', 'percentage' => "No refund");
                $message = "Restaurant will be collecting (Deposit amount " .$currency." ".$data['lunch_charge'].")nett deposit upon booking 5 paxs and above. Deposits are non-refundable within 24 hours,  more than 24 hours will be refund SGD " . $ref_amount . "nett. Should you require any assistance cancelling a booking, please call the restaurant hotline at +65 6222 1616. ";
                 
            }

            // if(empty($product)){
            //     $amount = $charge;
            // }
            //$message = $msg1." Cancellation received within " . $data['duration'] . " hours prior to booking date will incur a charge of " .$this->currency ." " .$amount. $msgflg.". Failure to arrive at the restaurant will be treated as a no-show and the same amount will be charged. ";
            //$range = array('duration' => "Any cancellation received within " . $data['duration'] . " hours prior to booking date will incur a charge of ", "percentage" => 100);
            
            
        }


        $record['message'] = $message; //str_replace('*', '<br/>*', $tnc);
        $record['message_array'] =$message;
        $record['payment_type'] = $payment_type;
        $record['lastRange'] = $lastRange;
        $record['range'] = $range;
        $record['charge'] = $charge;
        $record['isSplConfig'] = $isChksplconfig;
        $record['requiredccdetails'] = $isRequired;
        $record['pax'] = $pax ;


        return $record;
    }
    
    
    public  function getResDepositAmount($restaurant, $product = NULL){
        $where = " AND product LIKE '' ";
        if($restaurant == 'TH_BK_R_Medinii' || $restaurant == 'TH_BK_R_BangkokHeightz'){
            $product ='normal';
        }
        if(!empty($product)){
            $where = " AND product LIKE '$product'";
        }
        $sql = "SELECT lunch_charge,dinner_charge FROM restaurant_cancel_policy where restaurant ='$restaurant' $where";
        $data = pdo_single_select($sql);
        return $data;
        
    }


    public function getContactList($restaurant) {
        $sql = "SELECT * FROM restaurant_contacts WHERE restaurant LIKE '$restaurant'";
        $data = pdo_multiple_select($sql);
        return $data;
    }

    public function saveRestaurantContact($restaurant, $contact) {
        
        if (empty($contact['ID'])) {
            $sql = "INSERT INTO restaurant_contacts (`ID`, `restaurant`,`job_title`, `firstname`, `lastname`, `email`, `mobile`, `notify_email`, `notify_sms`,`sms_header`) VALUES (NULL, '$restaurant', '$contact[job_title]','$contact[firstname]', '$contact[lastname]', '$contact[email]', '$contact[mobile]', '$contact[notify_email]', '$contact[notify_sms]','$contact[sms_header]') ";
        } else {
            $sql = "UPDATE restaurant_contacts SET email ='$contact[email]' ,firstname='$contact[firstname]', lastname='$contact[lastname]', mobile='$contact[mobile]', notify_email='$contact[notify_email]', notify_sms='$contact[notify_sms]',job_title='$contact[job_title]',sms_header='$contact[sms_header]' WHERE ID  ='$contact[ID]' ";
        }
        $data = pdo_insert($sql);
        return $data;
    }

    public function deleteRestaurantContact($restaurant, $contact) {
        $sql = "DELETE FROM restaurant_contacts WHERE ID = '$contact[ID]' ";
        $data = pdo_exec($sql);
        return $data;
    }

    function checkRestaurantPartnership($theRestaurant, $identifier) {
        if ($this->restaurant != $theRestaurant)
            $this->getRestaurant($theRestaurant);

        if ($this->affiliate_program === $identifier) {
            return $this->affiliate_program;
        }
        return false;
    }

    public static function getCurrency($theRestaurant) {
        $theRestaurant = clean_input($theRestaurant);
        $data = pdo_single_select("SELECT currency FROM restaurant WHERE restaurant = '$theRestaurant' limit 1");
        return (!empty($data["currency"])) ? $data["currency"] : "";
    }
    public function registerDSBRestaurant($restaurant, $files) {
        if (isset($restaurant) && $restaurant != null && isset($restaurant['country']) && !empty($restaurant['country']) && isset($restaurant['city']) && !empty($restaurant['city']) && isset($restaurant['name']) && !empty($restaurant['name'])) {
            $id = $this->getCountryIsoCode($restaurant['country']) . '_' . $this->getCityIsoCode($restaurant['city']) . '_R_' . $restaurant['name'];
            $logo = '';
            $cover = '';
            $logoFileName = '';
            if (isset($restaurant['images']['logo']) && $restaurant['images']['logo'] != null && $restaurant['images']['logo']['media_type'] == 'picture') {
                $logo = $restaurant['images']['logo']['name'];
                if (!empty($logo))
                    $logoFileName = "Logo.".pathinfo($restaurant['images']['logo']['name'], PATHINFO_EXTENSION);
            }
            if (isset($files) && count($files)) {
                $media = new WY_Media($id);
                //error_log(__FILE__." ".__FUNCTION__." ".__LINE__.": ".count($files['name'])." files");
                $destination = "";
                for ($i = 0; $i < count($files['name']); $i++) {
                    error_log(__FILE__." ".__FUNCTION__." ".__LINE__.": processing ".$files['name'][$i]);
                    // Logo: 150x150; Cover: 2560x965
                    if ($files['name'][$i] == $logo) {
                        $media->upload($files['tmp_name'][$i], $logoFileName);
                    } else {
                        $cover = $files['name'][$i];
                        $media->upload($files['tmp_name'][$i], basename($files['name'][$i]));
                    }
                }
            }
            $sql_check = "SELECT ID FROM dsb_registration WHERE name = '".$id."' LIMIT 1";
            $sql_ins = "INSERT INTO dsb_registration (code, name, country, city, address, postcode, email, phone, owner, cuisine, url, pricepoints, logo, cover, status, owner_title, firstname, lastname, owner_email, owner_phone, type) VALUES ('$id','".$restaurant['name']."', '".$restaurant['country']."', '".$restaurant['city']."', '".$restaurant['address']."', '".$restaurant['postalcode']."', '".$restaurant['restaurantemail']."', '".WY_StringProcessor::FilterPhoneNumber($restaurant['restaurantphone'])."', '".$restaurant['owner']."', '".$restaurant['cuisines']."', '".$restaurant['website']."', '".$restaurant['pricepoint']."', '$logoFileName', '$cover', 'dsb', '".$restaurant['title']."', '".$restaurant['firstname']."', '".$restaurant['lastname']."', '".$restaurant['email']."', '".$restaurant['phone']."', '".$restaurant['type']."')";
            //error_log(__FILE__." ".__FUNCTION__." ".__LINE__.": sql_check: ".$sql_check.", sql_ins: ".$sql_ins);
            $result = pdo_insert_unique($sql_check, $sql_ins, "aurora");
            if ($result == 0)
                error_log(__FILE__." ".__FUNCTION__." ".__LINE__." New restaurant: ".$restaurant['name']." saved");
            else {
                $sql = "UPDATE dsb_registration SET name = :name, country = :country, city = :city, address = :address, postcode = :postcode, email = :email, phone = :phone, owner = :owner, cuisine = :cuisine, url = :url, pricepoints = :pricepoints, logo = :logo, cover = :cover, owner_title = :title, firstname = :firstname, lastname = :lastname, owner_email = :owner_email, owner_phone = :owner_phone, type = :type WHERE ID = :ID";
                $parameters = array('name' => $restaurant['name'], 'country' => $restaurant['country'], 'city' => $restaurant['city'], 'address' => $restaurant['address'], 'postcode' => $restaurant['postalcode'], 'email' => $restaurant['restaurantemail'], 'phone' => WY_StringProcessor::FilterPhoneNumber($restaurant['restaurantphone']), 'owner' => $restaurant['owner'], 'cuisine' => $restaurant['cuisines'], 'url' => $restaurant['website'], 'pricepoints' => $restaurant['pricepoint'], 'logo' => $logoFileName, 'cover' => $cover, 'owner_title' => $restaurant['title'], 'firstname' => $restaurant['firstname'], 'lastname' => $restaurant['lastname'], 'owner_email' =>$restaurant['email'], 'owner_phone' => $restaurant['phone'], 'type' => $restaurant['type'], 'ID' => $result);
                //error_log(__FILE__." ".__FUNCTION__." ".__LINE__.": sql: ".$sql." params: ".print_r($parameters, true));
                pdo_update($sql, $parameters, "aurora");
            }
            return true;
        } else
            error_log(__FILE__." ".__FUNCTION__." ".__LINE__.": Invalid input parameters: ".print_r($_REQUEST, true));
        return false;
    }
}

function getPricing($theRestaurant, $choice = 0) {

    $data = pdo_single_select("SELECT restaurant, pricing, currency FROM restaurant WHERE restaurant = '$theRestaurant' LIMIT 1");
    if (empty($data['pricing'])) {
        return array(80, 'SGD');
    }

    $value = $data['pricing'];
    $currency = $data['currency'];
    if (empty($value)) {
        return array(80, 'SGD');
    }

    $dd = explode(",", $value);
    if (count($dd) < 1) {
        return array(80, 'SGD');
    }

    if (count($dd) < 2) {
        $dd[1] = 0;
    }

    $dd[0] = intval($dd[0]);
    $dd[1] = intval($dd[1]);

    if ($choice == 1) {
        return array($dd[0], $currency);
    }

    if ($choice == 2) {
        return array($dd[1], $currency);
    }

    return array(floor(($dd[0] + $dd[1]) / 2), $currency);
}


?>
