<?php

class WY_Translation {
    
    var $msg;
    var $content;
    var $result;
    var $allow_langue  = array('en' , 'fr' , 'cn' , 'hk' , 'th' , 'my' , 'jp' , 'kr' , 'id' , 'es' , 'it' , 'de' , 'ru', 'pt', 'vi');


    function __construct() {
    	}
    	
    function readprofile($email) {
    
    	$cls = new WY_Cluster;
    	
    	$cls->read('SLAVE', $email, 'TRANSLATION', 'TRANSLATION', '', '');
    	if($cls->result < 0 || count($cls->clustcontent) < 1 || !isset($cls->clustcontent[0]) || $cls->clustcontent[0] == "") {
    		$this->result = -1;
    		$this->msg = "unable to find " . $email;
    		return array("", "");
    		}
    		 
    	$langue = "";
    	$title = array();
    	$contentAr = explode(";", $cls->clustcontent[0]);
    	if(substr($contentAr[0], 0, 7) == "langue=")
    		$langue = substr($contentAr[0], 7);
    	if(substr($contentAr[1], 0, 6) == "topic=") {
    		$titleAr = explode("|", substr($contentAr[1], 6));
    		foreach($titleAr as $topic)
    			$title[]["name"] = trim($topic);
    		}
		$this->result = 1;
		return array($langue, $title);
    	}

    function readzone($zone, $lang) {
		$this->content = pdo_multiple_select("SELECT zone, elem_name, object_name, lang, content from translation WHERE zone = '$zone' and lang = '$lang'");	
    	$this->result = (count($data) > 0) ? 1 : -1;
    	return $this->result;
    	}
    	
 	function readcontent($zone, $lang) {
      
 		if(!in_array($lang, $this->allow_langue)) {
 			$this->msg = "Invalid language " . $lang;
 			return $this->result;
 			}
 			
 		$this->content = array();
		$firstpass = pdo_multiple_select("SELECT zone, elem_name, object_name, lang, content from translation WHERE zone = '$zone' and lang = 'en' order by content");
		if(count($firstpass) < 1) {
			$this->msg = "Topic " . $zone . " does not exists";
			return $this->result = -7;
			}
		$i = 0; 
    
		foreach($firstpass as $data) {
			$elem_name = $data['elem_name'];
			$object_name = $data['object_name'];
			$row = pdo_single_select("SELECT content from translation WHERE zone = '$zone' and lang = '$lang' and elem_name = '$elem_name' and object_name = '$object_name' limit 1");	
			$content[$i]['translated'] = (count($row) > 0) ? $row['content'] : "";	
			$content[$i]['zone'] = $data['zone'];
			$content[$i]['elem_name'] = $data['elem_name'];
			$content[$i]['object_name'] = $data['object_name'];
			$content[$i]['content'] = $data['content'];
			$content[$i]['langue'] = $lang;
			$i++;
			}
		$this->content = $content;
		return $this->result = 1;
 		} 	

 	function writecontent($content, $lang) {
 		
		//error_log("TRANSLATION WRITE " . print_r($content, true));
 		foreach($content as $row) {
			$elem_name = $row['elem_name'];
			$object_name = $row['object_name'];
			$zone = $row['zone'];
			$value = $row['translated'];
			$lang = $row['langue'];
 			if(!in_array($lang, $this->allow_langue)) 
 				continue;
 			$sql = "INSERT INTO translation (zone, elem_name, object_name, lang, content) VALUES ('$zone', '$elem_name', '$object_name', '$lang', '$value') "
                . "ON DUPLICATE KEY UPDATE id=LAST_INSERT_ID(id), zone = '$zone', elem_name = '$elem_name', object_name = '$object_name', lang = '$lang', content = '$value' ";
			pdo_exec($sql);
 			}
 		$this->result = 1;
 		}
 		
 	function newelement($elem_name, $content, $zone) {
 		
 		$elem_name = clean_input($elem_name);
 		$content = clean_input($content);
 		$zone = strtoupper(clean_input($zone));
 		$lang = 'en'; // for the time being
 		$object_name = "";
 		
 		if(!isset($elem_name) || $elem_name == "" || !isset($content) || $content == "") {
 			$this->msg = "invalid element: [ " . $elem_name . " ] => " . $content;
 			return $this->result = -1;
 			}
 		
 		$row = pdo_single_select("SELECT content from translation WHERE zone = '$zone' and lang = '$lang' and elem_name = '$elem_name' and object_name = '$object_name' limit 1");	
		if(count($row) > 0) {
 			$this->msg = "element already exist: [ " . $elem_name . " ] => " . $content;
 			return $this->result = -1;
 			}
		
		pdo_exec("INSERT INTO translation (zone, elem_name, object_name, lang, content) VALUES ('$zone', '$elem_name', '$object_name', 'en', '$content') ");

		// check if translation already exists
		$row = pdo_single_select("SELECT zone, elem_name, object_name from translation WHERE (zone != '$zone' or elem_name != '$elem_name') and lang = 'en' and content = '$content' limit 1");	
		if(count($row) > 0) {
			$pzone = $row['zone'];
			$pelem_name = $row['elem_name'];
			$pobject_name = $row['object_name'];
			for($i = 0; $i < count($this->allow_langue); $i++) {
				$clg = $this->allow_langue[$i]; 
				if($clg != "en") {
					pdo_exec("INSERT INTO translation (zone, elem_name, object_name, lang, content) SELECT '$zone', '$elem_name', object_name, lang, content from translation WHERE zone = '$pzone' and elem_name = '$pelem_name' and object_name = '$pobject_name' and lang = '$clg' limit 1");
					error_log("INSERT INTO translation (zone, elem_name, object_name, lang, content) SELECT '$zone', '$elem_name', object_name, lang, content from translation WHERE zone = '$pzone' and elem_name = '$pelem_name' and object_name = '$pobject_name' and lang = '$clg' limit 1");
					}
				}
			}
		else {
			for($i = 0; $i < count($this->allow_langue); $i++) {
				$clg = $this->allow_langue[$i]; 
				if($clg != "en")
					pdo_exec("INSERT INTO translation (zone, elem_name, object_name, lang, content) VALUES ('$zone', '$elem_name', '$object_name', '$clg', '') ");
				}
			}
						
		$this->msg = "element has been created: [ " . $elem_name . " ] => " . $content;
		return $this->result = 1;
		
 		}
                
                
                
    function emailContent() {
            return $emailEle = array(
				'btn_gcalender' =>'Add to google calendar',
				'footermsg_bookingwallet' => 'Add your booking to your wallet',
				'addtional_info' => 'Additional Info',
                'food_selfie_message2' => 'Take a Food-Selfie photo when you dine, submit your entry for the contest & Win!',
                'title_reservation' => 'Dining Reservation',
                'title_dining' => 'Dining Reservation',
                'email_content' => 'Thank you for choosing',
                'booking_message' => 'Your reservation is confirmed',
                'booking_details' => 'Booking Details',
                'data_time' => 'Date & Time',
                'guests' => 'Guests',
                'confirmation_no' => 'Confirmation No',
                'weeloy_spin_code' => 'Weeloy Spin Code',
                'restaurant_name' => 'Restaurant Name',
                'address' => 'Address',
                'footer_content' => 'Book on the Go, Anytime, Anywhere',
                'app_message' => 'Try out our app via App Store or Google play below',
                'more_info' => 'more info',
                'request_no' => 'Request No',
                'request_content1' => 'Please note that',
                'request_details' => 'Request Details',
                'request_content2' => 'is on First come, First Serve basis',
                'title_request' => 'Dining Request',
                'title_spinwheel' => 'Congratulations',
                'title_spinwheel2' =>'You won an Offer',
                'wheelwin_content1' => 'You won',
                'wheelwin_content2' => 'by spinning the Weeloy Wheel',
                'terms_conditions' => 'Terms & Conditions',
                'phone no' => 'Phone no',
                'food_selfie_message1' => 'Your Join the Weeloy Food-Selfie Contest & Win a 3D2N holiday to Phuket for 2.',
                'btn_sharebooking' => 'Share your booking',
                'btn_invite' => 'Invite your friends',
                'btn_cancel' => 'Cancel booking',
                'footermsg_anytime' => 'Your booking with you, Anytime',
                 'title_dining' => 'Dining Reservation',
                'title_cancel' => 'Cancellation',
                'cancel_content1' => 'Your reservation with',
                'cancel_content2' => 'has been cancelled',
                'btn_update'=>'Update/Cancel Booking',
                'reminder'=>'Reminder',
                'reminder_content'=>'Just a gentle reminder of your upcoming reservation with',
                'title_review'=>'Thank you for your review',
                'review_details'=>'Review Details',
                'review_rateall'=>'Overall',
                'review_ratefood'=>'Food',
                'review_rateambiance'=>'Ambiance',
                'review_rateservice'=>'Service',
                'review_rateprice'=>'Price',
                'comment'=>'Comment',
                'special_request'=>'Special Request',
                'btn_writereview'=>'write a review',
                'review_reminder_content'=>'Thank you for your recent dining reservation at',
                'review_reminder_message'=>'Your feedback is important to us. As we want to improve your satisfaction everyday,  we would like to know how you enjoyed your dining experience at',
                'booking_message_need_reconfirm'=>'Your table has been blocked, we are sending you an e-mail to reconfirm the booking'
            );
       
}

    function getEmailContent($method,$lan) {
        $trad = new WY_Translation;
        //testing added staic data

        $trad->readcontent($method, $lan);
        $content = $trad->content;
        $data = array('row' => $content);

        $trans = $data['row'];

        $length = count($trans);

        $emailEle = $this->emailContent();

        foreach ($emailEle as $key => $ele) {
//            $lbl = strtolower($emailEle[$key]);
            if (isset($emailEle[$key])) {
                for ($i = 0; $i <= count($trans); $i++) {
                      if(isset($trans[$i]['content'])){
                        
                        if (strtolower($trans[$i]['content']) == strtolower($emailEle[$key])) {

                            if ($trans[$i]['translated'] != '') {
                                $cc = $trans[$i]['translated'];

                                $emailEle[$key] = $cc;
                            }
                            //echo $ele;
                        }
                    }
                }
            }
        }

        return $emailEle;
    }
}
