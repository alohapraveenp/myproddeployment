<?php

require_once("conf/conf.init.inc.php");
require_once("lib/wpdo.inc.php");
require_once("lib/class.debug.inc.php");

class WY_Thanying {

    private $url;
    private $port;
    private $profitcenter;
    private $employee;
    private $checktype;
    private $clientID;
    private $sessionID;
    private $authentification;
    private $header;
    private $body;
    private $restaurant;
    private $partner_id;
    private $mode;
    var $result;
    var $msg;

    public function __construct($mode) {

        $this->restaurant = 'SG_SG_R_Thanying';
        $this->result = 1;
        $this->partner_id = "";
        $this->mode =  'prod';
        $this->port = 7008;
        $this->sessionID = 0;
		$this->checktype = "1";
		$this->body = '';
		$this->header = array();

        if ($mode === "prod") {
			$this->url = "61.8.239.79";
			$this->profitcenter = 90;
			$this->employee = '99999';
			$this->clientID = 998;
			$this->authentification = "1234";
       	 } else {
			$this->url = 'agysremote.agilysys.asia';
			$this->authentification = "MSG";
			$this->profitcenter = 10;
			$this->employee = '104';
			$this->clientID = 50;
       	 	}
    }


    private function getRemoteOrderDetails($order_number,  $employee_id) {
        $this->setHeaderBody();
        //$order_number = $this->partner_id;
        //$employee_id = $this->employee;    
        //$order_number = 500339;
        
        
        $this->body .= '
        <order-detail-request-Body>
            <order-detail-request>
                <trans-services-header>
                    <client-id>' . $this->clientID . '</client-id>
                    <session-id>' . $this->sessionID . '</session-id>
                    <authentication-code>' . $this->authentification . '</authentication-code>
                </trans-services-header>
                <profitcenter-id>' . $this->profitcenter . '</profitcenter-id>
                <employee-id>' . $employee_id . '</employee-id>
                <order-number>' . $order_number . '</order-number>
            </order-detail-request>
        </order-detail-request-Body>';
        $this->setFooterBody();

        $response = $this->execute_query();

echo "Orders";
echo "<hr />" . htmlentities($response, ENT_QUOTES, "UTF-8") . "<hr />";

        //WY_debug::recordDebug("DEBUG", "POS XML 1 execute_query", 'getRemoteOrderDetails ' . $response);

        if ($response != "") {
            $Envelope = simplexml_load_string($response);
// xml name space declaration
            $Envelope->registerXPathNamespace('soap', 'http://schemas.xmlsoap.org/soap/envelope/');
            $Envelope->registerXPathNamespace('auth', 'http://developer.intuit.com/');

// you can use the prefixes that are already defined in the document
            $nodes = $Envelope->xpath('/SOAP-ENV:Envelope/SOAP-ENV:Body/order-detail-response-Body/order-detail-response/service-completion-status');
            $service_completion = (string) $nodes[0];

            // GET SUMMARY
            $nodes = $Envelope->xpath('/SOAP-ENV:Envelope/SOAP-ENV:Body/order-detail-response-Body/order-detail-response/order-detail-data/summary-data');
            $order_summary = $nodes[0];

            $res_json = json_encode($order_summary);
            $res_array = json_decode($res_json);

	    if($res_array != null)
	            $resultat['summary'] = get_object_vars($res_array);

            // GET DETAILS
            $nodes = $Envelope->xpath('/SOAP-ENV:Envelope/SOAP-ENV:Body/order-detail-response-Body/order-detail-response/order-detail-data/order-data');
            $order_details = $nodes[0];

            $res_json = json_encode($order_details);
            $res_array = json_decode($res_json);
      
           //WY_debug::recordDebug("DEBUG", "POS XML 2", 'getRemoteOrderDetails ' . count($res_array));

            $details = array();
	    if($res_array == null || count($res_array) < 1) {
	    	    $resultat['details'] = $details;
	            return $resultat;
		    }
		    
            foreach ($res_array as $res) {
                foreach ($res as $r) {
                    
                    $tmp = array();
                    if(!empty($r->{'@attributes'})){
                        $r = $r->{'@attributes'};
                    }
                    
                    $tmp['id'] = $r->{"id"};
                    $tmp['qty'] = $r->{"qty"};
                    $tmp['amount'] = $r->{"amount"};
                    $tmp['seat'] = $r->{"seat"};
                    $tmp['course'] = $r->{"course"};
                    $tmp['fired'] = $r->{"fired"};
                    $details[] = $tmp;
                }
            }
            $resultat['details'] = $details;
            return $resultat;
        }
        return null;
    }


    private function saveOrderDetails($order_details) {
        
        $order_summary = $order_details['summary'];
        $order_items = $order_details['details'];

        $order_number = $order_summary['order-number'];
        $order['order_number'] = $order_summary['order-number'];
        $order['order_amount'] = $order_summary['gross-sales-amount'];
        $order['table'] = $order_summary['table'];
        
        if( empty(strlen($order_summary['table'])) || strlen($order_summary['table']) < 1){
            $order['table'] = 0;
        }

        $restaurant = $this->restaurant;

		$now = (strstr($_SERVER['HTTP_HOST'], 'localhost:8888')) ? "NOW()" : "DATE_ADD(NOW(), INTERVAL 8 HOUR)";
        $sql = "INSERT INTO _integration_agilysys_order (restaurant, date, confirmation, table_num, order_id, amount) "
                . "VALUES ('$restaurant', $now, '', :table, :order_number, :order_amount) ON DUPLICATE KEY UPDATE table_num = :table, amount = :order_amount;";

        pdo_insert($sql, $order);

        if(count($order_items) > 0){
            $sql = "DELETE FROM _integration_agilysys_order_details WHERE restaurant = '$restaurant' AND order_id= '$order_number'";
            pdo_exec($sql);
        }
        
        foreach ($order_items as $items) {
            $sql = "INSERT INTO _integration_agilysys_order_details (restaurant, confirmation, order_id, item_id, qty, amount, seat, course, fired) "
                    . "VALUES ('$restaurant', '', '$order_number', :id, :qty, :amount, :seat, :course, :fired) ;";
            pdo_insert($sql, $items);
        }

        return true;
    }

    private function getListOpenorder() {

        $this->setHeaderBody();

        $this->body .= '
        <order-list-request-Body>
            <order-list-request>
                <trans-services-header>
                    <client-id>' . $this->clientID . '</client-id>
                    <session-id>' . $this->sessionID . '</session-id>
                    <authentication-code>' . $this->authentification . '</authentication-code>
                </trans-services-header>
                <order-search-criteria>
                </order-search-criteria>
            </order-list-request>
        </order-list-request-Body>';
        $this->setFooterBody();

        $resultat = $this->execute_query();

echo preg_replace("/\[/", "<br />[", print_r($this, true)) . "<hr />";        
echo "Request";
echo "<hr />" . htmlentities($this->body, ENT_QUOTES, "UTF-8") . "<hr />";
echo "Result";
echo "<hr />" . htmlentities($resultat, ENT_QUOTES, "UTF-8") . "<hr />";

		if ($resultat == "") {
			$this->result = -1;
			}
			
        if ($resultat != "") {
            $Envelope = simplexml_load_string($resultat);
// xml name space declaration
            $Envelope->registerXPathNamespace('soap', 'http://schemas.xmlsoap.org/soap/envelope/');
            $Envelope->registerXPathNamespace('auth', 'http://developer.intuit.com/');

// you can use the prefixes that are already defined in the document
            $nodes = $Envelope->xpath('/SOAP-ENV:Envelope/SOAP-ENV:Body/order-list-response-Body/order-list-response/service-completion-status');
            $service_completion = (string) $nodes[0];

            $order_ids = $Envelope->xpath('/SOAP-ENV:Envelope/SOAP-ENV:Body/order-list-response-Body/order-list-response/order-list');
            $res_json = json_encode($order_ids);
            $res_array = json_decode($res_json);

            $resultat = array();

            foreach ($res_array[0] as $res) {
                foreach ($res as $r) {
                    $tmp = array();
                    $r = $r->{'@attributes'};
                    $tmp['order-number'] = $r->{"order-number"};
                    $tmp['table'] = $r->{"table"};
                    $tmp['employee-id'] = $r->{"employee-id"};
                    $resultat[] = $tmp;
                }
            }
            return $resultat;
        }

        return array();
    }

    public function saveOpenCheckDetails(){
        $list_open_order = $this->getListOpenorder();
        foreach ($list_open_order as $open_order){
            $order = $this->getRemoteOrderDetails($open_order['order-number'], $open_order['employee-id']);
            
            if(!empty($order['summary']) && $order['summary']["total-amount"] > 0  ){
                $this->saveOrderDetails($order);
            }
        }
        return true;
    }
    
    public function updateFullMenu() {
        $this->setHeaderBody();
        $this->body .= '
            <item-data-request-Body>
                <item-data-request>
                    <trans-services-header>
                        <client-id>' . $this->clientID . '</client-id>
                        <session-id>' . $this->sessionID . '</session-id>
                        <authentication-code>' . $this->authentification . '</authentication-code>
                    </trans-services-header>
                </item-data-request>
            </item-data-request-Body>';
        $this->setFooterBody();

        $resultat = $this->execute_query();

        
        // continue from here

        $Envelope = new SimpleXMLElement($resultat);
        $Envelope->registerXPathNamespace('soap', 'http://schemas.xmlsoap.org/soap/envelope/');
        $Envelope->registerXPathNamespace('auth', 'http://developer.intuit.com/');


        $body = $Envelope->xpath('//SOAP-ENV:Envelope/SOAP-ENV:Body/item-data-response-Body/item-data-response/item-data');
        $res_json = json_encode($body);
        $res_array = json_decode($res_json);



        $keys_dico = array();
        foreach ($res_array as $key_cat => $cat_obj) {
            foreach ($cat_obj as $key => $obj) {
                //var_dump($key);
                if (isset($keys_dico[$key])) {
                    if ($keys_dico[$key] < strlen($obj)) {
                        $keys_dico[$key] = strlen($obj);
                    }
                } else {
                    $keys_dico[$key] = strlen($obj);
                }
            }
        }
        $keys = array();
        foreach ($keys_dico as $key) {
            $keys[] = $key;
        }
        //var_dump($keys );die;


        foreach ($res_array as $key_cat => $cat_obj) {

            $tmp = get_object_vars($cat_obj);

            $obj = array();

            $obj['itemid'] = $tmp['item-id'];
            $obj['itemdescription'] = $tmp['item-description'];
            $obj['itembuttontext'] = $tmp['item-button-text'];
            $obj['itembaseprice'] = $tmp['item-base-price'];
            $obj['itemproductclass'] = $tmp['item-product-class'];
            $obj['itemrevenueid'] = $tmp['item-revenue-id'];

            if (!empty($tmp['choicegroup-selection-data'])) {
                $obj['choicegroupselectiondata'] = json_encode($tmp['choicegroup-selection-data']);
            } else {
                $obj['choicegroupselectiondata'] = NULL;
            }


            //var_dump($obj['itemdescription']);die;

            $sql = "INSERT INTO _integration_agilysys_menu (restaurant, item_id, item_description, item_button_text, item_base_price, item_product_class, item_revenue_id, choicegroup_selection_data) "
                    . "VALUES ('SG_SG_R_Thanying', :itemid, :itemdescription, :itembuttontext, :itembaseprice, :itemproductclass, :itemrevenueid, :choicegroupselectiondata) "
                    . "ON DUPLICATE KEY UPDATE item_description = :itemdescription, item_button_text = :itembuttontext, item_base_price = :itembaseprice, item_product_class  = :itemproductclass, item_revenue_id = :itemrevenueid, choicegroup_selection_data = :choicegroupselectiondata";

            pdo_insert($sql, $obj);
        }
        return true;
    }
    
    private function execute_query() {

        $this->setHeader();

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_HTTPHEADER, $this->header);
        curl_setopt($ch, CURLOPT_URL, $this->url);
        curl_setopt($ch, CURLOPT_PORT, $this->port);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 20);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $this->body);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        $server_output = curl_exec($ch);

        curl_close($ch);
        return $server_output;
    }

    private function setHeader() {
        $this->header = array();
        $this->header[0] = "Content-Type: text/xml";
        $this->header[1] = 'Content-length: ' . strlen($this->body);
    }

    private function setHeaderBody() {
        $this->body = '    
            <SOAP-ENV:Envelope
            xmlns:xsi = "http://www.w3.org/1999/XMLSchema/instance"
            xmlns:SOAP-ENV= "http://schemas.xmlsoap.org/soap/envelope"
            xsi:schemaLocation= "http://www.infogenesis.com/schemas/ver1.4/POSTransGatewaySchema.xsd">
                <SOAP-ENV:Body xsi:type= "item-data-request-Body">';
    }

    private function setFooterBody() {
        $this->body .= '    
                </SOAP-ENV:Body>
            </SOAP-ENV:Envelope>';
    }

}

?>