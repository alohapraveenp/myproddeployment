<?php

class WY_DataCheck {

    function getPKColumn ($schemaname, $tablename) {
        $data = array();

        $pk_column = pdo_single_select("SELECT col.column_name FROM information_schema.table_constraints tab LEFT JOIN information_schema.key_column_usage col USING(constraint_name, table_schema, table_name) WHERE tab.constraint_type = 'PRIMARY KEY' AND tab.table_schema = '$schemaname' AND tab.table_name = '$tablename' LIMIT 1");

        if (count ($pk_column) > 0) {
            return $pk_column;
        } else {
            return $data;
        }

    }

    function getTableColumn ($schemaname, $tablename) {

        $tab_column = pdo_multiple_select("SELECT column_name FROM information_schema.columns WHERE table_schema = '$schemaname' AND table_name = '$tablename'");

        $this->result = (count($tab_column) < 1) ? -1 : 1;    

        return $tab_column;

    }

    function getDBTable ($schemaname) {

        $db_table = pdo_multiple_select("SELECT table_name FROM information_schema.tables WHERE table_schema = '$schemaname'");

        $this->result = (count($db_table) < 1) ? -1 : 1;    
        
        return $db_table;

    }

    function getSingleColumn($schemaname, $tablename, $columnname, $pk_column) {
        
        if (!empty($pk_column) ) {
        
            $data = pdo_multiple_select("SELECT '$schemaname' AS DB_Name, '$tablename' AS Table_Name, '$pk_column' AS Key_Column, $pk_column AS PK, '$columnname' AS Column_Name, replace($columnname,'’','\"') as JASON_MSG FROM $tablename WHERE $columnname LIKE '{%' ");
        } else {

            $data = pdo_multiple_select("SELECT '$schemaname' AS DB_Name, '$tablename' AS Table_Name, '' AS Key_Column, '' AS PK, '$columnname' AS Column_Name, replace($columnname,'’','\"') as JASON_MSG FROM $tablename WHERE $columnname LIKE '{%' ");   
        }
        
        $this->result = (count($data) < 1) ? -1 : 1;    
        return $data;

    }

    function getSingleTable($schemaname, $tablename, $columnname, $pk_column) {

        $data_array = '';

        $tab_column = $this->getTableColumn($schemaname, $tablename);

        foreach ($tab_column as $row) {

            $data = $this->getSingleColumn($schemaname, $tablename, $row['column_name'], $pk_column);

            if (count($data) > 0 ) {
                $data_array[] = $data;
            }

        } 
        $this->result = (count($data_array) < 1) ? -1 : 1;
        return $data_array;
    }
    
    function getFullSchema($schemaname, $tablename, $columnname, $pk_column) {
error_log("In full schema");
        $data_array = '';

        $db_table = $this->getDBTable($schemaname);   

        foreach ($db_table as $tab) {

            $tab_column = $this->getTableColumn($schemaname, $tab['table_name']);
            $pk_column = $this->getPKColumn ($schemaname, $tab['table_name']);

            foreach ($tab_column as $row) {

                if (count($pk_column) > 0) {
                    $data = $this->getSingleColumn($schemaname, $tab['table_name'], $row['column_name'], $pk_column['column_name']);
                } else {
                    $data = $this->getSingleColumn($schemaname, $tab['table_name'], $row['column_name'], '');
                }    
                if (count($data) > 0 ) {
                    $data_array[] = $data;
                }

            }     
        }

        $this->result = (count($data_array) < 1) ? -1 : 1;    
        //echo count($data);
        return $data_array;
    }

    function json_validate($string) {
        // decode the JSON data
        $result = json_decode($string);

        // switch and check possible JSON errors
        switch (json_last_error()) {
            case JSON_ERROR_NONE:
                $error = '';
                break;
            case JSON_ERROR_DEPTH:
                $error = 'The maximum stack depth has been exceeded.';
                break;
            case JSON_ERROR_STATE_MISMATCH:
                $error = 'Invalid or malformed JSON.';
                break;
            case JSON_ERROR_CTRL_CHAR:
                $error = 'Control character error, possibly incorrectly encoded.';
                break;
            case JSON_ERROR_SYNTAX:
                $error = 'Syntax error, malformed JSON.';
                break;
            // PHP >= 5.3.3
            case JSON_ERROR_UTF8:
                $error = 'Malformed UTF-8 characters, possibly incorrectly encoded.';
                break;
            // PHP >= 5.5.0
            case JSON_ERROR_RECURSION:
                $error = 'One or more recursive references in the value to be encoded.';
                break;
            // PHP >= 5.5.0
            case JSON_ERROR_INF_OR_NAN:
                $error = 'One or more NAN or INF values in the value to be encoded.';
                break;
            case JSON_ERROR_UNSUPPORTED_TYPE:
                $error = 'A value of a type that cannot be encoded was given.';
                break;
            default:
                $error = 'Unknown JSON error occured.';
                break;
        }

        return $error;
        
    }

}

?>
