<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
require_once ('lib/class.restaurant.inc.php');
require_once ('lib/class.payment.inc.php');
require_once ('lib/class.booking.inc.php');
require_once ('lib/class.notification.inc.php');
require_once 'lib/composer/vendor/autoload.php';


class WY_Payment_Adyen {
    var $reference_id;
    var $amount;
    var $token;

    function __construct($reference_id, $amount, $token) {
        $this->reference_id = $reference_id;
        $this->amount = $amount;
        $this->token = $token;
    }
    
    public function makeDeposit(){
        
        $booking = new WY_Booking();
        $res = new WY_restaurant();
        $booking->getBooking($this->reference_id);
        
        $client = new \Adyen\Client();
        $client->setApplicationName("Adyen PHP Api Library Example");
        $client->setUsername("ws@Company.WeeloyPteLtd655");
        $client->setPassword("WeeloyPteLtdSg002014");
       
        $client->setEnvironment(\Adyen\Environment::TEST);
        $service = new \Adyen\Service\Payment($client);
        
        $currency = $res->getCurrency($booking->restaurant);
        $depositAmount = $this->amount * 100;
        $payToken = md5(time());
        $paymentObj = $this->getAdyenPayObject($this->amount,$currency,$this->reference_id,$booking->email,$booking->restaurant,$this->token,"WeeloyPteLtdCOM556");
       
        $params = json_decode($paymentObj, true);
        $returnArr = array('status' => 'failed', 'message' => '', 'payment_id' => '');

         try {
            $result = $service->authorise($params);
            if(isset($result['additionalData']['refusalReasonRaw']) && $result['additionalData']['refusalReasonRaw'] == 'AUTHORISED'){
                $payment_id = $result['pspReference'];
                $status = $result['additionalData']['refusalReasonRaw'];
                $payment = new WY_Payment;
                $payment_status = 'COMPLETED';
                $returnArr['status'] = 'COMPLETED';
                $returnArr['message'] = 'paid';
                $returnArr['payment_id'] = $payment_id;
                //booking deposit details
                $details = $payment->saveDeposit(json_encode($params),$this->amount, $booking->restaurant,$payToken,$payment_id,$payment_status,'adyen', $this->reference_id,$currency);
                //save payment details in payment table
                $payment->savePayment($this->amount,'booking',$this->reference_id,$booking->restaurant,$payment_id,'adyen',$payment_status,0,0,$objStatus='pay',$currency);
                     //update booking & depsoit booking  table payment id and status
                
                $payment->updateccDetails($payment_id,$this->reference_id,$this->amount,$payment_status);
                $payoutObj = $this->makePayout();
                $payment->updatePayStatus($this->reference_id, $payment_id, $payoutObj['pspReference'], 'COMPLETED', 'adyen');
                $notification = new WY_Notification();
                $notification->notify($booking, 'booking');
                return $returnArr ;
            }
      
         } 
        catch (\Adyen\AdyenException $e) {
            $returnArr['message'] = 'Adyen Exception error';
            return $returnArr ;
           
        }

        
        
       
    }
    
    public function getAdyenPayObject($amount,$currency,$reference_id,$email,$restaurant,$token,$mc_acc){
        $depositAmount = $amount * 100;
        $json = '{
             "amount": {
                  "value": '.$depositAmount.',
                  "currency": "'.$currency.'"
                },
                "reference": "'.$reference_id.'",
                "merchantAccount": "WeeloyPteLtdCOM556",                                            
                "additionalData":{
                    "card.encrypted.json" : "'.$token.'"
                },
                "shopperIP": "",
                "shopperInteraction": "",
                "shopperEmail" : "'.$email.'",                                 
                "shopperReference" : "'.$restaurant.'",
                "recurring" : {
                   "contract" : "RECURRING"
                }

        }';
        return $json;
        
    }
    public function getAdyenPayoutObj($amount,$currency,$reference_id,$email,$restaurant,$token,$mc_acc){
        $depositAmount = $amount * 100;
        $json ='{
                    "bank": {
                        "bankName": "SEPA BANK",
                        "countryCode": "NL",
                        "iban": "NL81TEST0536169128",
                        "ownerName": "E.Klaassen"
                        },
                        "amount": {
                        "value": "'.$depositAmount.'",
                        "currency": "EUR"
                        },
                        "reference": "'.$reference_id.'",
                        "merchantAccount": "WeeloyPteLtdCOM556",
                        "shopperEmail": "'.$email.'",
                        "shopperReference":  "'.$restaurant.'",
                        "recurring": {
                             "contract" : "PAYOUT"
                        }
                       
                }';
        return $json;
        
    }
    
    public function makePayout(){
        $booking = new WY_Booking();
        $res = new WY_restaurant();
        $booking->getBooking($this->reference_id);
   
        $client = new \Adyen\Client();
        $client->setApplicationName("Adyen PHP Api Library Example");
        $client->setEnvironment(\Adyen\Environment::TEST);
        $client->setUsername("storePayout_337770@Company.WeeloyPteLtd655");
        $client->setPassword("WeeloyPte01testpayout");
        $service = new \Adyen\Service\Payout($client);
        $paymentObj = $this->getAdyenPayoutObj($this->amount,$currency,$this->reference_id,$booking->email,$booking->restaurant,$this->token,"WeeloyPteLtdCOM556");
        $params = json_decode($paymentObj, true);
         try {
            $result = $service->storeDetailsAndSubmit($params);
            return $result ;
        } 
        catch (\Adyen\AdyenException $e) {
             return $e ;
           
        }
        
    }
    
    public function makeRefund(){
        
        $client = new \Adyen\Client();
        $client->setApplicationName("Adyen PHP Api Library Example");
        $client->setUsername("ws@Company.WeeloyPteLtd655");
        $client->setPassword("WeeloyPteLtdSg002014");
       
        $client->setEnvironment(\Adyen\Environment::TEST);
        $service = new \Adyen\Service\Modification($client);

        $modificationAmount = array('currency' => 'EUR', 'value' => '750');
        $params = array(
            "merchantAccount" => $this->_merchantAccount,
            "modificationAmount" => $modificationAmount,
            "reference" => $pspReference . '_refund',
            "originalReference" => $pspReference
        );
        $result = $service->refund($params);
        $this->assertEquals('[refund-received]', $result['response']);
    }
   
    
    
}


