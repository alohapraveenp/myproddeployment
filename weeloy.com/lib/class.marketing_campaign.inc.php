<?php
require_once("lib/class.string.inc.php");
require_once("lib/SavePage/htmlSaveComplete.php");
class WY_MarketingCampaign {
    var $ID;
    var $campaign_id;
    var $restaurant_id;
    var $type;
    var $description;
    var $redirect_to;
    var $creation_date;
    var $campaign_token;
    var $s3;
    var $bucket;
    function __construct($theRestaurant = NULL) {
    }
    function create($campaign_id, $sub_id = 'a', $restaurant_id, $type, $redirect_to, $creation_date, $campaign_token, $group, $name, $shortUrl) {

        if (!empty($restaurant_id)) {
            $restaurant_id = trim($restaurant_id);
        }
        $sql = "INSERT INTO marketing_campaigns (campaign, sub_id,restaurant_id, type,redirect_to, creation_date, campaign_token,name,short_link,campaign_group,lcount,conversion) 
            VALUES ( '$campaign_id', '$sub_id','$restaurant_id', '$type', '$redirect_to', '$creation_date', '$campaign_token','$name','$shortUrl','$group',0,0)";
        if(isset($name) &&  $name ==='EXTLINK'){
         $sql = "SELECT  *  FROM marketing_campaigns where restaurant_id ='$restaurant_id' and type like '%$type%' ";
         $data = pdo_single_select($sql);
         if(count($data)>0){
            $sql =  "UPDATE marketing_campaigns set campaign ='$campaign_id',redirect_to='$redirect_to',campaign_token ='$campaign_token',name='$name',campaign_group='$group' where restaurant_id ='$restaurant_id' and type like '%$type%' "; 
             $id= pdo_exec($sql);
         }else{
            $id = pdo_exec($sql);
         }
        }else{
            $id = pdo_exec($sql);
        }
        
        

        if (isset($id)) {
            $this->ID = $id;
            $this->campaign_id = $campaign_id;
            $this->restaurant_id = $restaurant_id;
            $this->type = $type;
            $this->redirect_to = $redirect_to;
            $this->creation_date = $creation_date;
            $this->campaign_token = $campaign_token;
        }
    }

    function getTypes($type) {
        $sql = "SELECT  type  FROM marketing_campaigns where  type like '%$type%' ";
        $data = pdo_multiple_select($sql);
        $arrType = array();
        foreach ($data as $t) {
            $val = $t['type'];
            array_push($arrType, $val);
        }
        return array_values(array_unique($arrType));
    }

    function getCampaignList() {
        $sql = 'SELECT * FROM marketing_campaigns order by ID DESC';
        $data = pdo_multiple_select($sql);
        return $data;
    }

//kala created this function for booking id convert to base64url encode
    function base64url_encode($data) {
        return rtrim(strtr(base64_encode($data), '+/', '-_'), '=');
    }

    function base64url_decode($data) {
        return base64_decode(str_pad(strtr($data, '-_', '+/'), strlen($data) % 4, '=', STR_PAD_RIGHT));
    }

    function bitly_shorten($url) {

        $query = array(
            "longUrl" => $url,
            "access_token" => '8c32b835ba1cc5c4c0c98f40b2bda7849bac6fe9',
            "format" => 'json'// replace with your login
        );

        $query = http_build_query($query);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://api-ssl.bitly.com/v3/shorten?" . $query);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        $response = curl_exec($ch);
        curl_close($ch);
        $response = json_decode($response);

        if ($response->status_txt == 'OK' && $response->status_code == 200) {
            return $response->data->url;
        } else {
            return null;
        }
    }

    function getCampaignId($token, $email) {
        $campaignId = NULL;
        $sql = "SELECT  *  FROM marketing_campaigns where campaign_token = '$token' order by creation_date DESC LIMIT 1 ";
        $data = pdo_single_select($sql);
        if (count($data) > 0) {
            $campaignId = $data['campaign'];
            if (!empty($campaignId)) {
                $query = "UPDATE marketing_campaigns set conversion = conversion + 1 where ID ='$data[ID]' ";
                pdo_exec($query);
            }
        }
        //count if it s first booking with weeloy based on email
        $sql_first_booking = "SELECT ID from booking WHERE email LIKE '$email' AND tracking NOT LIKE '%facebook%' AND tracking NOT LIKE '%callcenter%' AND tracking NOT LIKE '%website%' AND tracking NOT LIKE '%remote%' LIMIT 1";
        $data_first_booking = pdo_single_select($sql_first_booking);
        if (count($data_first_booking) < 1) {
            $sql = "UPDATE marketing_campaigns set first_booking = first_booking + 1  where ID ='$data[ID]' ";
            pdo_exec($sql);
        }
        return $campaignId;
    }
    private function uploadMailChimpImage($name, $fname, $date, $content_type = 'image/jpeg') {
        if (file_exists($name) && filesize($name)) {
            require_once 'conf/conf.s3.inc.php';
            $cache_value_browser = "1296000";
            $cache_value_cdn = "86400";
            error_log(__FILE__." ".__FUNCTION__." ".__LINE__." Uploading name: ".$name." size ".filesize($name)." fname: ".$fname." content " .$content_type."...");
            $this->getS3("static.weeloy.com")->putObjectFile($name, "static.weeloy.com", "mailchimp/".$date."/".$fname, S3::ACL_PUBLIC_READ, array("Cache-Control" => "max-age=$cache_value_browser, s-maxage=$cache_value_cdn", "Content-Type" => "$content_type"), array("Cache-Control" => "max-age=$cache_value_browser, s-maxage=$cache_value_cdn", "Content-Type" => "$content_type"));
            unlink($name);
        } else
            error_log(__FILE__." ".__FUNCTION__." ".__LINE__.": Invalid file: ".$name." size: ".filesize($name));
    }
    private function uploadMailChimpPage($string, $date, $filename, $content_type = 'text/plain') {
        if (!empty($string) && !empty($filename)) {
            require_once 'conf/conf.s3.inc.php';
            $cache_value_browser = "1296000";
            $cache_value_cdn = "86400";
            error_log(__FILE__." ".__FUNCTION__." ".__LINE__." Uploading string size ".strlen($string)." content " .$content_type."...");
            $this->getS3("static.weeloy.com")->putObjectString($string, "static.weeloy.com", "mailchimp/".$date."/".$filename, S3::ACL_PUBLIC_READ, array("Cache-Control" => "max-age=$cache_value_browser, s-maxage=$cache_value_cdn", "Content-Type" => "$content_type"), array("Cache-Control" => "max-age=$cache_value_browser, s-maxage=$cache_value_cdn", "Content-Type" => "$content_type"));
        } else
            error_log(__FILE__." ".__FUNCTION__." ".__LINE__.": trying to upload empty string!");
    }    
    private function savePage($url, $date, $title) {
        if (!trim($url)) {
            //header('LOCATION: index.php?e=' . urlencode('No URL Specified'));
            error_log(__FILE__." ".__FUNCTION__." ".__LINE__.": Invalid URL: ".$url);
            return false;
        }
        $contentonly = false;
        $keepjs = true;
        $compress = true;
        # include the class
        try {
            $htmlSaveComplete = new htmlSaveComplete($url);
            $html = $htmlSaveComplete->getCompletePage($keepjs, $contentonly, $compress);
            if (!$html || !strlen($html)) {
                //header('LOCATION: index.php?e=' . urlencode('Error saving the page, please try again later.'));
                error_log(__FILE__." ".__FUNCTION__." ".__LINE__.": Error saving the page, please try again later!");
                return false;
            }
            error_log(__FILE__." ".__FUNCTION__." ".__LINE__.": saved html size ".strlen($html));
            $filename = $title.".html";
            $this->uploadMailChimpPage($html, $date, $filename);
            error_log(__FILE__." ".__FUNCTION__." ".__LINE__.": ".$url." successfully saved!");
            return true;
        } catch (Exception $e) {
            error_log(__FILE__." ".__FUNCTION__." ".__LINE__.": Exception: ".$e);
        }
        return false;
    }
    function processMailChimp($data, $file) {
        if (isset($file) && count($file))
            $this->uploadMailChimpImage($file['tmp_name'][0], $file['name'][0], $data['date']);
        $result = $this->savePage($data['url'], $data['date'], $data['title']);
        if ($result) {
            //error_log(__FILE__." ".__FUNCTION__." ".__LINE__.": data: ".print_r($data, true));
            $sql = "INSERT INTO mailchimp (title, description, tag, url, image, date) VALUES ('".$data['title']."','".$data['description']."', '".$data['tag']."', '".$data['url']."', '".$data['image']['name']."', '".$data['date']."')";
            //error_log(__FILE__." ".__FUNCTION__." ".__LINE__." sql: ".$sql);
            $result = pdo_insert($sql) > 0;
            if (!$result)
                error_log(__FILE__." ".__FUNCTION__." ".__LINE__." Failed to create new MailChimp!");
        } else
            error_log(__FILE__." ".__FUNCTION__." ".__LINE__.": Failed to save page ".$data['url']);
        error_log(__FILE__." ".__FUNCTION__." ".__LINE__.": ".$result?"success!":"failed!");
        return $result;
    }
    function deleteMailChimp($data) {
        $result = false;
        try {
            $sql = "DELETE from mailchimp WHERE title = '".$data['title']."' and url = '".$data['url']."'";
            if (pdo_exec($sql) > 0) {
                error_log(__FILE__." ".__FUNCTION__." ".__LINE__.": mailchimp ".$data['date']."/".$data['title'].".html deleted succesfully from DB");
                if ($this->getS3("static.weeloy.com")->deleteObject("static.weeloy.com", "mailchimp/".$data['date']."/".$data['title'].".html")) {
                    error_log(__FILE__." ".__FUNCTION__." ".__LINE__.": mailchimp ".$data['date']."/".$data['title'].".html deleted succesfully from S3");
                    $result = true;
                }
                else
                    error_log(__FILE__." ".__FUNCTION__." ".__LINE__.": Failed to delete mailchimp ".$data['date']."/".$data['title'].".html from S3!");
            } else
                error_log(__FILE__." ".__FUNCTION__." ".__LINE__.": Failed to delete mailchimp ".$data['date']."/".$data['title'].".html from DB!");                
        } catch (Exception $e) {
            error_log(__FILE__." ".__FUNCTION__." ".__LINE__.": Exception: ".$e);
        }
        return $result;
    }
    function downloadMailChimp($data) {
        try {
        if (isset($data)) {
            require_once 'conf/conf.s3.inc.php';
            $params = [];
            $sql = "SELECT date, title from mailchimp where ";
            if (isset($data['ID']) && !empty($data['ID'])) {
                $sql .= "ID = :id";
                $params['id'] = $data['ID'];
            } else {
                if (isset($data['title']) && !empty($data['title'])) {
                    $sql .= "title = :title ";
                    $params['title'] = $data['title'];
                }
                if (isset($data['url']) && !empty($data['url'])) {
                    $sql .= "AND url = :url ";
                    $params['url'] = $data['url'];
                }
                if (isset($data['date']) && !empty($data['date'])) {
                    $sql .= "AND date = :date";
                    $params['date'] = $data['date'];
                }
            }
            //error_log(__FILE__." ".__FUNCTION__." ".__LINE__." sql: ".$sql." params: ".print_r($params, true));
            if (isset($params) && count($params)) {
                $result = pdo_single_select_with_params($sql, $params);
                //error_log(__FILE__." ".__FUNCTION__." ".__LINE__.": result: ".print_r($result, true));
                if (count($result) > 0 && isset($result['date']) && !empty($result['date']) && isset($result['title']) && !empty($result['title'])) {
                    $filename = $result['title'].".html";
                    $mailchimp = $this->getS3("static.weeloy.com")->getObject($this->bucket, "mailchimp/".$result['date']."/".$filename);
                    //error_log(__FILE__." ".__FUNCTION__." ".__LINE__.": getObject returns ".print_r($mailchimp, true));
                    if (isset($mailchimp) && empty($mailchimp->error) && !empty($mailchimp->body) && strlen($mailchimp->body) && $mailchimp->code === 200) {
                        if(!is_dir("../magazines/".$result['date']))
                            mkdir("../magazines/".$result['date'], 0777, TRUE);
                        if (file_put_contents("../magazines/".$result['date']."/".$filename, $mailchimp->body) === false) {
                            error_log(__FILE__." ".__FUNCTION__." ".__LINE__.": Failed to save mailchimp to ../magazines/".$result['date']."/".$filename);
                            return false;
                        }
                         error_log(__FILE__." ".__FUNCTION__." ".__LINE__.": Successfully saved mailchimp to ../magazines/".$result['date']."/".$filename);
                        return true;
                    } else
                        error_log(__FILE__." ".__FUNCTION__." ".__LINE__.": Invalid mailchimp content!");
                } else
                    error_log(__FILE__." ".__FUNCTION__." ".__LINE__.": No record found!");
            } else
                error_log(__FILE__." ".__FUNCTION__." ".__LINE__.": Invalid input parameters!");
        } else
            error_log(__FILE__." ".__FUNCTION__." ".__LINE__.": NULL $data!");
        } catch (Exception $e) {
            error_Log(__FILE__." ".__FUNCTION__." ".__LINE__.": Exception ".$e);
        }
        return false;
    }
    function getMailChimps() {
        return pdo_multiple_select("SELECT * from mailchimp");
    }
    function getS3($bucket) {
        if (!isset($this->s3)) {
            $this->bucket = $bucket;
            $this->s3 = new S3(awsAccessKey, awsSecretKey);
            //$this->s3->putBucket($this->bucket, S3::ACL_PUBLIC_READ);
        }
        return $this->s3;
    }
}
?>