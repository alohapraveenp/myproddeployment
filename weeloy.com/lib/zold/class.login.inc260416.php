<?php

/**
 * 	Richard Kefs (c) 2014
 *
 * 	Contains all classes definition for login session.
 *
 */
define("_LOG_IN_", 0x1);
define("_LOG_OUT_", 0x2);
define("_NOT_REGISTER_", 0x100);

define("__COOKIENAME__", "weeloy_admin");
define("__COOKIELOG__", "weeloy_login");

define("ACTIVATED", 0x000000001);
define("__SALT__", "sax]as09%");

define("__pwd_max_length__", 12);
define("__newpwd_max_length__", 8);

define("LOGIN_ADMIN", "admin");
define("LOGIN_BACKOFFICE", "backoffice");
define("LOGIN_TRANSLATION", "translation");
define("LOGIN_TMS", "tms");
define("LOGIN_MEMBER", "member");

if(!defined("__SESSION_LIFE_MINUTE_8H__"))  define("__SESSION_LIFE_MINUTE_8H__", 240);
if(!defined("__LOGIN_8HOURS__"))  define("__LOGIN_8HOURS__", 0x80);

$login_type_allowed = array('backoffice', 'admin', 'translation', 'tms', 'api', 'member'); // api should be rename to api-backoffice
//
//define('FACEBOOK_SDK_V4_SRC_DIR', '../facebook-php-sdk-v4/src/Facebook/');
require_once 'lib/composer/vendor/facebook/php-sdk-v4/autoload.php';

//purpose ->dynamic load email resturant
require_once 'lib/class.event.inc.php';
require_once("lib/class.images.inc.php");
require_once("lib/class.media.inc.php");
require_once("lib/class.restaurant.inc.php");
require_once("lib/class.review.inc.php");

use Facebook\FacebookSession;
use Facebook\FacebookRequest;
use Facebook\GraphUser;
use Facebook\FacebookRequestException;
use Facebook\FacebookRedirectLoginHelper;

class WY_Arg {

    var $email;
    var $password;
    var $rpassword;
    var $npassword;
    var $lastname;
    var $firstname;
    var $mobile;
    var $country;
    var $timezone;
    var $userid;
    var $token;
    var $socialname;
    var $application;
    var $type_action;
    var $extrafield;

}

class WY_Login {

	use CleanIng;

    var $ui;
    var $msg;
    var $result;
    var $member;
    var $membertype;
    var $memberights;
    var $selection;
    var $email;
    var $firstname;
    var $signIn;
    var $token;
    var $cookie;
    var $duration;
    var $remote;
    var $platform;
    var $logapps;
    var $sessionlife;
    var $arg;
    var $login_type;

    public function __construct($loginType) {
        $this->login_type = $loginType;
        $this->platform = $loginType;
        $this->sessionlife = ($loginType == "member") ? 31536000 : __SESSION_LIFE__;  // 31536000 = 1 year
    }

    function init() {
        $this->signIn = array();
        $this->remote = 0;
        $this->logapps = "";
    }

    private function errormsg($msg, $retvalue) {
        $this->msg = $msg;
        $this->result = $retvalue;

        return $this->result;
    }


    private function getNewpassword($limit) {
        for ($str = "", $i = 0; $i < $limit; $i++) {
            $n = rand(1, 52) + 64;
            if ($n > 90) {
                $n += 6;
            }

            $str .= chr($n);
        }
        return $str;
    }

    private function setlogin($email, $chgpass, $platform, $extrafield) {

        $coding = new WY_Coding;

        $data = pdo_single_select("select ID, firstname,name,gender,mobile,salutation, member_type, member_permission from member where email = '$email' limit 1");
        $user_id = $data['ID'];
        $firstname = $data['firstname'];
        $name = $data['name'];
        $gender = $data['gender'];
        $mobile = $data['mobile'];
        $salutation =$data['salutation'];

        $member_type = $data['member_type'];
        $member_permission = $data['member_permission'];
        	
        // CPP 
        $affiliate_program = '';
          if(preg_match("/@credit-suisse.com|@weeloy.com/i", $email) == true){
            $affiliate_program = 'cpp_credit_suisse';
        }
        // CPP 

        $this->msg = ""; // "Welcome " . ucfirst($firstname);
        $this->result = 2;
        $IPaddr = ip2long($_SERVER['REMOTE_ADDR']);

		switch($this->login_type) {
			case LOGIN_BACKOFFICE:
				$this->duration = ($member_permission & __LOGIN_8HOURS__) ? __SESSION_LIFE_MINUTE_8H__ : __SESSION_LIFE_MINUTE__;
				break;
			case LOGIN_TRANSLATION:
			case LOGIN_TMS:
				$this->duration = __SESSION_TMS_MINUTE__;
				break;
			default:
				$this->duration = __SESSION_LIFE_MINUTE__;
				break;
			}
						
        error_log('SETLOGIN ' . $this->login_type . ":" . $this->duration);

        $token = $this->getNewpassword(__newpwd_max_length__);
        $date = new DateTime(gmdate("Y-m-d H:i"));
        $date->add(new DateInterval('PT' . ($this->duration) . 'M'));
        $info = $date->format('H-i-j');
        // this is a temporary refactoring. don't touch. will be modified soon. 18jan16
        if (($this->remote == 91 && $this->platform != 'api-ajax') || $this->platform == "member") {
            $info = "NOLIMIT";
            $this->duration = 24 * 60 * 6;
        }

        $this->signIn = array("user_id" => $user_id, "gender" => $gender, "salutation"=>$salutation,"firstname" => $firstname, "name" => $name, "mobile" => $mobile, "member_type" => $member_type, "member_permission" => $member_permission, "token" => $token, "email" => $email, "info" => $info, "chgpass" => $chgpass, "platform" => $platform, "affiliate_program" => $affiliate_program);

        $jsonStr = json_encode($this->signIn);
        $tokenCookie = $coding->myencode($jsonStr);

        pdo_exec("delete from logonehour where email = '$email' AND platform ='$platform' ");
        pdo_exec("insert into logonehour (email, token, duration, info, platform, extrafield) values ('$email', '$token', '$this->duration', '$info', '$platform', '$extrafield')");
        $this->recordLogProcess($email, $info, $IPaddr, $this->logapps, $platform, $member_permission, "logged");

        //USER: language
        //$_SESSION['user']['lang'] = $login->signIn['en'];
        $this->cookie = ":firstname=$firstname:expire=$info";
        $this->token = $token;
        
        if ($this->remote != 91) {
            SetCookie(__COOKIENAME__ . $platform, "", time() - 100000, "/"); // get rid off previous cookie
            SetCookie(__COOKIENAME__ . $platform, $tokenCookie, time() + $this->sessionlife, "/");
            SetCookie(__COOKIELOG__ . $platform, "", time() - 100000, "/"); // get rid off previous cookie
            SetCookie(__COOKIELOG__ . $platform, ":firstname=$firstname:expire=$info", time() + $this->sessionlife, "/");
        }
        // SET LOGIN SESSION
        $this->setLoginSession($platform);
        
        $this->msg = "You have successfully logged in, " . $this->signIn['firstname'];
        $this->result = 1;
        return $this->result;
    }

    private function setlogout($email, $platform) {

        $this->deleteLogins($email, $platform, "You have successfully logged out");
        return $this->result = 1;
    }

    private function setLoginSession($platform) {

        if ($platform == 'backoffice') {
            $_SESSION['user_backoffice']['gender'] = $this->signIn['gender'];
            $_SESSION['user_backoffice']['firstname'] = $this->signIn['firstname'];
            $_SESSION['user_backoffice']['lastname'] = $this->signIn['name'];
            $_SESSION['user_backoffice']['mobile'] = $this->signIn['mobile'];
            $_SESSION['user_backoffice']['id'] = $this->signIn['user_id'];
            $_SESSION['user_backoffice']['email'] = $this->signIn['email'];
            $_SESSION['user_backoffice']['member_type'] = $this->signIn['member_type'];
            $_SESSION['user_backoffice']['member_permission'] = $this->signIn['member_permission'];
        } else {

            $mobile_array['prefix'] = '';
            $mobile_array['mobile'] = $this->signIn['mobile'];

            $tmp = explode(' ', $this->signIn['mobile']);
            if (count($tmp) == 2 && substr($tmp[0], 0, 1) == '+') {
                $mobile_array['prefix'] = trim($tmp[0]);
                $mobile_array['mobile'] = trim($tmp[1]);
            } else {
                if (substr($this->signIn['mobile'], 0, 1) == '+') {

                    if (substr($this->signIn['mobile'], 1, 1) == '1') {
                        $mobile_array['prefix'] = trim(substr($this->signIn['mobile'], 0, 4));
                        $mobile_array['mobile'] = trim(substr($this->signIn['mobile'], 4));
                    } else {
                        $mobile_array['prefix'] = trim(substr($this->signIn['mobile'], 0, 3));
                        $mobile_array['mobile'] = trim(substr($this->signIn['mobile'], 3));
                    }
                }
            }

            
            $res = $this->signIn;
            
            $res['cookie'] = $this->cookie;
            $res['remote'] = $this->remote;
            $res['sessionlife'] = $this->sessionlife;
            $res['duration'] = $this->duration;
            
            $res['prefix'] = $mobile_array['prefix'];
            $res['mobile'] = $mobile_array['mobile'];
            $res['member_type'] = 'member';
            $res['lang'] = 'en';
            $res['affiliate_program'] = $this->signIn['affiliate_program'];
           
            $_SESSION['user'] = $res;
        }
        return true;
    }

    private function deleteLogins($email, $platform, $msg) {
        if (!empty($email)) {
            pdo_exec("delete from logonehour where email = '$email' AND platform = '$platform'");
        }

        if ($this->remote == 91) {
            return;
        }

        SetCookie(__COOKIENAME__ . $platform, "", time() - 10000, "/");
        SetCookie(__COOKIELOG__ . $platform, "", time() - 10000, "/");

        if (isset($_SESSION['login_type'])) {
            $tmp = $_SESSION['login_type'];
        }
        if ($platform == 'backoffice') {
            $_SESSION['user_backoffice'] = array();
        } else {
            if (!empty($_SESSION['user_backoffice'])) {
                $_SESSION['user'] = array();
                $_SESSION['user']['account'] = 'visitor';
            } else {
                $_SESSION = array();
                $_SESSION['user']['account'] = 'visitor';
                //$_SESSION['user']['lang'] = $this->signIn['lang'];
                $_SESSION['login_type'] = $tmp;
            }
        }


        $this->msg = $msg;
        return $this->result = -1;
    }

    function GetToken() {

        $platform = $this->platform;

        if (empty($_COOKIE[__COOKIENAME__ . $platform]) || empty($_COOKIE[__COOKIELOG__ . $platform])) {
            return array("", "", "", "", "", "");
        }

        $coding = new WY_Coding;
        $decoded = $coding->mydecode($_COOKIE[__COOKIENAME__ . $platform]);
        $this->signIn = json_decode($decoded, true);
        if (empty($this->signIn['token'])) {
            return array("", "", "", "", "", "");
        }

        return $this->signIn;
    }

    function logout($arg) {
        $this->signIn = $this->GetToken();
        if (!empty($this->signIn['email'])) {
            return $this->setlogout($this->signIn['email'], $this->platform);
        }

        if (isset($_SESSION['login_type'])) {
            $tmp = $_SESSION['login_type'];
        }

        if ($this->platform == 'backoffice') {
            $_SESSION['user_backoffice'] = array();
        } else {
            if (!empty($_SESSION['user_backoffice'])) {
                $_SESSION['user'] = array();
                $_SESSION['user']['account'] = 'visitor';
            } else {
                $_SESSION = array();
                $_SESSION['user']['account'] = 'visitor';
                //$_SESSION['user']['lang'] = $this->signIn['lang'];
                $_SESSION['login_type'] = $tmp;
            }
        }

        return $this->errormsg("Not logged In", -1);
    }

    function resyncduration() {
        // refactoring login to allow different duration. Default is __SESSION_LIFE_MINUTE__.
        // next line will eventually disappear

        pdo_exec("update logonehour set duration = '" . __SESSION_LIFE_MINUTE__ . "' where duration = '' and info != 'NOLIMIT'");
        pdo_exec("delete from logonehour where DATE_SUB(CURRENT_TIMESTAMP(),INTERVAL duration MINUTE) > the_ts and info != 'NOLIMIT'");
    }

    function checktoken($token) {
        $this->resyncduration();
        $data = pdo_single_select_index("SELECT token, logonehour.email, member_type, member_permission FROM logonehour, member WHERE token = '$token' and member.email = logonehour.email limit 1");
        if(count($data) > 0) {
        	$this->email = $data[1];
          	$this->membertype = $data[2];
        	$this->memberights = $data[3];
        	return $this->result = true;
        	}
        return $this->result = false;
    }

    function CheckLoginStatus() {

        $token = '';
        $platform = '';

        $this->resyncduration();
        $this->signIn = $this->GetToken();
        
        if (!empty($this->signIn['token'])) {
            $token = $this->signIn['token'];
        }
        if (!empty($this->signIn['platform'])) {
            $platform = $this->signIn['platform'];
        }

        if ($this->platform == "admin" && $this->platform != $platform)
            return $this->errormsg("Please Sign-in." . $this->platform, -7);

        if (empty($token) || empty($this->signIn['email']) || $this->email_validation($this->signIn['email']) < 0) {
            return $this->errormsg("Please Sign-in." . $this->platform, -7);
        }

        $email = $this->signIn['email'];
        $info = ($this->platform != LOGIN_MEMBER) ? "info != 'NOLIMIT'" : "info = 'NOLIMIT'";
        $data = pdo_single_select_index("SELECT logonehour.token, TO_SECONDS(CURRENT_TIMESTAMP()) - TO_SECONDS(logonehour.the_ts) , logonehour.the_ts, logonehour.email FROM logonehour, login WHERE logonehour.token = '$token' and login.email = logonehour.email and $info limit 1");
        if (count($data) <= 0) {
            $this->setlogout($email, $this->platform);
            return $this->errormsg("Please Sign in. ($token)", -2);
        }

        if ($email != $data[3]) {
            $debug = new WY_debug;
            $debug->writeDebug("ERROR", "LOGIN-STATUS", "incompatible email =  " . $email . " => " . $data[3]);
        }

        $data = pdo_single_select("SELECT email FROM login WHERE email = '$email' limit 1");
        if (count($data) < 1)
            $this->sessionlife = 0;

        $data = pdo_single_select("SELECT email, member_type FROM member WHERE email = '$email' limit 1");
        if (count($data) < 1)
            $this->sessionlife = 0;

        if ($this->platform != LOGIN_MEMBER) {
            $_SESSION['user']['member_type'] = $this->signIn['member_type'] = $data['member_type'];
        } else {
            $_SESSION['user_backoffice']['member_type'] = $this->signIn['member_type'] = $data['member_type'];
        }
        $this->remaining = $this->sessionlife - intval($data[1]);
        //Error_log("REAMINING SESSION :" . intval($this->remaining / 60));
        if ($this->remaining < 0) {
            return $this->deleteLogins($email, $this->platform, "session expired");
        }
        $data = pdo_single_select("SELECT token FROM logonehour WHERE token = '$token' limit 1");

        $statusAr = $this->GetSystemStatus(); // check for system status.
        if ($email != "Super_Admin@weeloy.com" && isset($statusAr['onmaintenance']) && $statusAr['onmaintenance'] && isset($statusAr['onmaintenance']) && $statusAr['redirection'] > 0) {
            return $this->deleteLogins($this->signIn['email'], $this->platform, "On maintenance");
        }

        $this->msg = "You are logged In " . ucfirst($this->signIn['firstname']);

        return $this->result = 1;
    }

    function check_password($password, $store_password, $tmp_password) {
        if ($store_password != $this->set_password($password)) {
            if ($tmp_password == '' || $tmp_password != $this->set_password($password)) {
                return $this->errormsg("This is wrong password or email ", -7);
            }
        }

        return 1;
    }

    function set_password($password) {
        $key = hash('sha256', $password . __SALT__);
        return $key;
    }

    function process_login_remote($email, $pass, $platform) {

        $this->remote = 91;
        $this->platform = $platform;
        $info = $member_permission = "";
        $chgpass = $store_password = $tmp_password = $extrafield = "";

        $data = pdo_single_select("SELECT Email, Password, tmpPassword, token FROM login where email = '$email' limit 1");
        if (count($data) > 0) {
            $store_password = $data['Password'];
            $tmp_password = $data['tmpPassword'];
        }
        $retval = $this->check_password($pass, $store_password, $tmp_password);
        if ($retval < 0) {
            $this->recordLogProcess($email, $info, "", "remote", $platform, $member_permission, "fail rmlogin");
            return "";
        }

        $this->setlogout($email, $platform);
        if ($this->setlogin($email, $chgpass, $platform, $extrafield))
            return $this->token;
        return "";
    }

    function process_remote_facebook($email, $facebookid, $facebooktoken, $plateform) {

        $this->remote = 91;
        $this->platform = $plateform;
        if ($this->process_facebook($email, $facebookid, $facebooktoken) > 0)
            return $this->token;
        return "";
    }

    function get_facebook_member($email, $facebookid, $facebooktoken) {

        $fbtokenFVerif = false;

        $apidfb = array('1590587811210971', '535d344eb1f08a575ca5556bd37fd159');

        $pos = strpos($facebookid, '_facebook_');
        if ($pos !== false) { // you are in facebook, testing facebook -> different application than the web. Get proper ID
            $apidfb[0] = '755782447874157';
            $apidfb[1] = '19dc4a4bb6f19ac9e6fca32d2ad1af1e'; // get the right number
            $facebookid = substr($facebookid, 0, $pos);
            //$fbtokenFVerif = true;
        }

        if (!empty($facebooktoken) && strlen($facebooktoken) > 150 && $fbtokenFVerif == false) {

            FacebookSession::setDefaultApplication($apidfb[0], $apidfb[1]);
            //dev FacebookSession::setDefaultApplication('344934015702933','26068ece51e88406aeb3854b5c77a293');
            // kefs.me FacebookSession::setDefaultApplication('1430473417270947','c36895496339b3423186127256691b8a');
            // Use one of the helper classes to get a FacebookSession object.
            //   FacebookRedirectLoginHelper
            //   FacebookCanvasLoginHelper
            //   FacebookJavaScriptLoginHelper
            // or create a FacebookSession with a valid access token:
            $session = new FacebookSession($facebooktoken);

            // Get the GraphUser object for the current user:

            try {
                $Request = new FacebookRequest($session, 'GET', '/me');
                $response = $Request->execute();
                $object = $response->getGraphObject();

                if ($object->getProperty('email') == $email && $object->getProperty('id') == $facebookid)
                    $fbtokenFVerif = true;
            } catch (FacebookRequestException $e) {
                // The Graph API returned an error
            } catch (\Exception $e) {
                // Some other error occurred
            }
        }

        if ($fbtokenFVerif == false)
            return $this->errormsg("Invalid Facebook ID", -1);

        $this->result = 1;
        $data = array("mobile" => "", "country" => "", "gender" => "");
        $fulldata = pdo_single_select("select ID, firstname, name, gender, mobile, country from member where email = '$email' limit 1");
        if (count($fulldata) > 0) {
            $data['mobile'] = $fulldata['mobile'];
            $data['country'] = $fulldata['country'];
            $data['gender'] = $fulldata['gender'];
        }
        return $data;
    }

    function process_facebook($email, $facebookid, $facebooktoken) {
        $chgpass = '';
        $extrafield = '';
        $this->logapps = "facebook";

        if (empty($facebookid))
            return $this->errormsg("Invalid Facebook ID", -1);

        $facebookid = $this->clean_number($facebookid);

        if (empty($facebookid))
            return $this->errormsg("Invalid Facebook ID Format", -1);

        $fbtokenFVerif = false;
        if (!empty($facebooktoken) && strlen($facebooktoken) > 150) {

            //$appsecret_proof= hash_hmac('sha256', $facebooktoken, '535d344eb1f08a575ca5556bd37fd159');

            FacebookSession::setDefaultApplication('1590587811210971', '535d344eb1f08a575ca5556bd37fd159');
            FacebookSession::enableAppSecretProof(false);

            // Weeloy FacebookSession::setDefaultApplication('1590587811210971','535d344eb1f08a575ca5556bd37fd159');
            // kefs.me FacebookSession::setDefaultApplication('1430473417270947','c36895496339b3423186127256691b8a');
            // Use one of the helper classes to get a FacebookSession object.
            //   FacebookRedirectLoginHelper
            //   FacebookCanvasLoginHelper
            //   FacebookJavaScriptLoginHelper
            // or create a FacebookSession with a valid access token:
            $session = new FacebookSession($facebooktoken);

            // Get the GraphUser object for the current user:

            try {
                $Request = new FacebookRequest($session, 'GET', '/me');
                $response = $Request->execute();
                $object = $response->getGraphObject();

                if ($object->getProperty('email') == $email && $object->getProperty('id') == $facebookid)
                    $fbtokenFVerif = true;
            } catch (FacebookRequestException $e) {
                var_dump($e->getResponse());
            } catch (\Exception $e) {
                // Some other error occurred
            }
        }

        if ($fbtokenFVerif == false) {
            $this->recordLogProcess($email, "Unverified FID", ip2long($_SERVER['REMOTE_ADDR']), "facebook", $this->platform, "", "fail facebook");
            return $this->errormsg("Invalid/Unverified Facebook ID Format", -1);
        }

        $data = pdo_single_select("SELECT email, facebookid, appliname, applitoken FROM login where email = '$email' limit 1");


        if (count($data) < 1) {
            $this->recordLogProcess($email, "Unknown User FID", ip2long($_SERVER['REMOTE_ADDR']), "facebook", $this->platform, "", "fail facebook");
            return $this->errormsg("Unknown User -$email-", -1);
        }

        // if the user id for facebook had never been set, set it for the only time.
        if (empty($data['facebookid'])) {
            $data['facebookid'] = $facebookid;
            pdo_exec("update login set facebookid='$facebookid' where email = '$email' limit 1");
        }

        if ($data['facebookid'] != $facebookid) {
            pdo_exec("update login set facebookid='$facebookid', appliname='facebook' where email = '$email' limit 1");
            //return $this->errormsg("Invalid Facebook ID " . $data['facebookid'] . " " . $facebookid, -1);
        }

        $this->setlogout($email, $this->platform);
        $this->setlogin($email, $chgpass, $this->platform, $extrafield);
        pdo_exec("update login set applitoken='$facebooktoken', appliname='facebook' where email = '$email' limit 1");
        return $this->result = 1;
    }

    function process_linkedin() {
        $this->logapps = "linkedin";
        return -1;
    }

    function process_twitter() {

        $this->logapps = "twitter";

        $data = pdo_single_select("SELECT email FROM member where name = '$arg->name' and firstname = '$arg->firstname' limit 1");
        if (count($data) <= 0) {
            return $this->errormsg("Unknown user through twitter", -1);
        }

        $email = $data['email'];
        return -1;
    }

    function recordLogProcess($email, $info, $IPaddr, $application, $platform, $permissions, $status) {
        pdo_exec("insert into logaccess (Email, Info, Createdate, IPaddr, application, type, permissions, status) values ('$email', '$info', NOW(), '$IPaddr', '$application', '$platform', '$permissions', '$status')");
    }

    function check_login_remote($email, $token, $platform) {

        $this->remote = 91;
        $this->platform = $platform;

        $this->resyncduration();

        // platform api => mobile, api-ajax => backoffice translation
        $info = ($this->platform == 'api') ? " and info = 'NOLIMIT'" : "";
        $data = pdo_single_select("SELECT logonehour.token, login.email FROM logonehour, login WHERE logonehour.token = '$token' $info and login.email = '$email' and login.email = logonehour.email limit 1");
        return $this->result = (count($data) > 0) ? 1 : -1;
    }

    function register_booking($email, $first, $last, $mobile, $country,$membercode=0) {

        $arg = new WY_Arg;
        $arg->email = $email;
        $arg->firstname = $first;
        $arg->name = $last;
        $arg->mobile = $mobile;
        $arg->country = $country;
        $this->clean_arg($arg);
        //membercode
         $arg->membercode = $membercode;

        $this->platform = $platform = 'member';
        $store_password = $chgpass = $extrafield = "";
        $arg->password = $this->getNewpassword(__newpwd_max_length__);

        $data = pdo_single_select("SELECT Email, Password, tmpPassword FROM login where email = '$email' limit 1");
        if (count($data) > 0) {
            $store_password = $data['Password'];
        }

        $arg->creation_from_booking = true;
        return $this->register($arg, $store_password, $chgpass, $extrafield);
    }

    function register_remote($arg) {

        $this->clean_arg($arg);
        $this->remote = 91;
        $this->platform = $platform = 'api';
        $email = $arg->email;
        $store_password = $chgpass = $extrafield = "";
        //random membercode generation
        $arg->membercode =rand(0, 9) . rand(0, 9) . rand(0, 9) . rand(0, 9);

        $data = pdo_single_select("SELECT Email, Password, tmpPassword FROM login where email = '$email' limit 1");
        if (count($data) > 0) {
            $store_password = $data['Password'];
        }

        return $this->register($arg, $store_password, $chgpass, $extrafield);
    }

    function register($arg, $store_password, $chgpass, $extrafield) {

        $email = $arg->email;

        if (empty($arg->name) || empty($arg->password) || empty($arg->mobile)) {
            return $this->errormsg("Empty value (name, password)", -1);
        }

        if ($this->email_validation($email) < 0)
            return $this->errormsg("Invalid email", -1);

        if (!empty($store_password)) {
            return $this->errormsg("$email email already exists. Unable to create new member ", -3);
        }
        // check unicity on phone number
        //$sql = "SELECT COUNT(ID) as mobile_exist FROM  `member` WHERE mobile =  '$arg->mobile'";
        //$data = pdo_single_select($sql);
        //if ($data['mobile_exist'] > 0) {
        //    return $this->errormsg("Mobile already exists. Unable to create new member ", -3);
        //}

        $password = $this->set_password($arg->password);

        $member_type = 'member';

        if ($this->platform != LOGIN_MEMBER) {
            if (isset($_SESSION['user']['member_type']) && !empty($_SESSION['user']['member_type'])) {
                $member_type = $_SESSION['user']['member_type'];
            }
        } else {
            if (isset($_SESSION['user_backoffice']['member_type']) && !empty($_SESSION['user_backoffice']['member_type'])) {
                $member_type = $_SESSION['user_backoffice']['member_type'];
            }
        }

        pdo_exec("delete from login where email = '$email' ");
        pdo_exec("delete from member where email = '$email' ");
        //echo "insert into member (email, name, firstname, mobile, country, member_type) values ('$email', '$arg->name', '$arg->firstname', '$arg->mobile', '$arg->country','$member_type');";die;
        pdo_exec("insert into member (email, name, firstname, mobile, country, member_type,membercode) values ('$email', '$arg->name', '$arg->firstname', '$arg->mobile', '$arg->country','$member_type','$arg->membercode') ");
        pdo_exec("insert into login (email, Password, tmpPassword) values ('$email', '$password', '')");

        /* Welcome email notification for after registeration */

        $data_object = $this;
        $data_object->firstname = $arg->firstname;
        $data_object->lastname = $arg->name;
        $data_object->country = $arg->country;
        //$data_object->restaurants = @$showcase;
        $creation_from_madison = '';
        try {
            if (!empty($arg->creation_from_madison)) {
                $creation_from_madison = $arg->creation_from_madison;
            }
        } catch (Exception $ex) {
            
        }

        if ($creation_from_madison) {
            return true;
        }
        
        if (!empty($arg->creation_from_booking)) {
                $link=$this->generateforgotpwdlink($email);
          
                $data_object->resetLink =$link;
                $data_object->is_forgotlink ='is_true';
     
              $this->notifyEmail($email, $data_object);
             return true;
        }
        
        $this->notifyEmail($email, $data_object);
        return $this->setlogin($email, $chgpass, $this->platform, $extrafield);
    }

    function logout_remote($email, $platform, $remote_type = 91) {
        $this->platform = $platform;
        $this->remote = $remote_type;
        $this->setlogout($email, $platform);
        return $this->result = 1;
    }
    function generateforgotpwdlink($email){
        $redirect_to="";
        $reset_token = md5(time());

        pdo_exec("update login set reset_token = '$reset_token', tmpTimeStamp = NOW() where email = '$email' limit 1");
        //$body = "Your password reset link " . __BASE_URL__ . "/reset-password?email=" . $email . '&token=' . $reset_token . '<br>This link is only avalaible in 2 days.';
        $data = pdo_single_select("SELECT name,firstname FROM member WHERE email = '$email' limit 1");

        if (count($data) > 0) {
            $data_object = $data;
        }
        $link = __BASE_URL__ . "/reset-password?email=" . $email . '&token=' . $reset_token. '&r_to='.$redirect_to;
        return $link;
    }

    function notifyEmail($recipient, $data_obj) {
     
        $mailer = new JM_Mail;
        $header['from'] = array('reservation@weeloy.com' => 'Weeloy.com');
        $header['replyto'] = array('customersupport@weeloy.com');

        $body = $mailer->getTemplate('register/register_member', $data_obj);

        $mailer->sendmail($recipient, "Welcome to Weeloy! ", $body, $header, NULL, 'welcome');
     
    }

    function process_login($type_action, $arg) {

        $type_action = $this->clean_input($type_action);

        $this->clean_arg($arg);

        if ($arg->application == "facebook") {
            return $this->process_facebook($arg->email, $arg->userid, $arg->token);
        } else if ($arg->application == "linkedin") {
            return $this->process_linkedin($arg->email, $arg->userid);
        } else if ($arg->application == "twitter") {
            return $this->process_twitter($arg->email, $arg->userid);
        }

        $this->msg = "";
        $this->result = 0;
        $extrafield = $arg->extrafield;
        $email = $arg->email;

        $chgpass = $store_password = $tmp_password = "";
        $tmpTimeStamp = 0;

        if (empty($email)) {
            return $this->errormsg("Please enter a valid email", -1);
        }

        if ($this->email_validation($email) < 0) {
            return $this->errormsg("Invalid email -$email-", -2);
        }

        if (empty($type_action)) {
            return $this->errormsg("Invalid request", -3);
        }

        pdo_exec("update login set tmpPassword = '' where tmpTimeStamp < NOW() and tmpPassword != ''");

        $data = pdo_single_select("SELECT email, Password, tmpPassword FROM login where email = '$email' limit 1");
        if (count($data) > 0) {
            $store_password = $data['Password'];
            $tmp_password = $data['tmpPassword'];
        }

        switch ($type_action) {
            case "Login":
                if (empty($store_password)) {
                    $this->recordLogProcess($email, "", ip2long($_SERVER['REMOTE_ADDR']), $arg->application, $this->platform, "", "unknown user");
                    return $this->errormsg("$email email does not exist.<br>Please login with another name or register", -4);
                }

                $retval = $this->check_password($arg->password, $store_password, $tmp_password);

                ////// TMP FIX pwd 8 car (20150227)
                /* if (strlen($arg->password) > 8 && $retval < 0) {
                  $password = $this->set_password($arg->password);
                  pdo_exec("update login set Password = '$password' where email = '$email' limit 1");
                  $data = pdo_single_select("SELECT Email, Password, tmpPassword FROM login where email = '$email' limit 1");
                  if (count($data) > 0) {
                  $store_password = $data['Password'];
                  $tmp_password = $data['tmpPassword'];
                  }
                  $retval = $this->check_password($arg->password, $store_password, $tmp_password);
                  } */
                ////// TMP FIX pwd 8 car (20150227)

                if ($retval < 0) {
                    $this->recordLogProcess($email, "", ip2long($_SERVER['REMOTE_ADDR']), $arg->application, $this->platform, "", "fail login");
                    return $retval;
                }

                $chgpass = ($tmp_password != "") ? "pass=2" : "";

                $data = pdo_single_select("SELECT status, member FROM member where email = '$email' limit 1");
                if (count($data) <= 0) {
                    return $this->errormsg("The user is not register. Please register or contact Weeloy", -1);
                }

                $status = $this->setlogin($email, $chgpass, $this->platform, $extrafield);
                return $status;

            case "AutoLogin":
                $this->setLoginSession($this->platform);
                //$status = $this->setlogin($email, $arg->token, $this->platform, $arg->extrafield);
                return true;

            case "Logout":
                return $this->setlogout($email, $this->platform);

            case "Register":
                return $this->register($arg, $store_password, $chgpass, $extrafield);

            case "LostPassword":
                if (empty($store_password)) {
                    return $this->errormsg("$email email does not exist.<br>Please login with another name or register", -1);
                }

                return $this->forgotPass($email, $this->platform);


            case "UpdatePassword":
                return $this->UpdatePassword($arg);

            default:
                return $this->errormsg("Invalid command -$type_action-", -1);
        }
    }

    public function forgotPass($email, $redirect_to) {
        // $tmp_password = $this->getNewpassword(__newpwd_max_length__);
        // $password = $this->set_password($tmp_password);
        $reset_token = md5(time());

        pdo_exec("update login set reset_token = '$reset_token', tmpTimeStamp = NOW() where email = '$email' limit 1");
        //$body = "Your password reset link " . __BASE_URL__ . "/reset-password?email=" . $email . '&token=' . $reset_token . '<br>This link is only avalaible in 2 days.';
        $data = pdo_single_select("SELECT name,firstname FROM member WHERE email = '$email' limit 1");

        if (count($data) > 0) {
            $data_object = $data;
        }

        $link = __BASE_URL__ . "/reset-password?email=" . $email . '&token=' . $reset_token . '&r_to=' . $redirect_to;
        $data_object['resetLink'] = $link;

        $header['from'] = array('reservation@weeloy.com' => 'Weeloy.com');
        $header['replyto'] = array('customersupport@weeloy.com');
        $mailer = new JM_Mail;
        $body = $mailer->getTemplate('register/forgot_password', $data_object);

        $mailer->sendmail($email, "Reset Password - Weeloy", $body, $header, NULL, 'reset_pwd');
        $this->msg = "A new password has been sent ($email)";
        return $this->result = 1;
    }

    public function UpdatePassword($arg) {

        $email = $arg->email;
        $oldPassword = urlencode($arg->password);
        $newPassword = $arg->npassword;
        $rpassword = $arg->rpassword;

        if (empty($email)) {
            return $this->errormsg("email is empty. Unable to update login information ", -1);
        }

        if (empty($oldPassword)) {
            return $this->errormsg("Passord is empty. Unable to update login information ", -1);
        }

        if ($newPassword != $rpassword) {
            return $this->errormsg("New and Retype password are different. Try again", -2);
        }

        $data = pdo_single_select("SELECT email, Password, tmpPassword FROM login where email = '$email' limit 1");
        if (count($data) <= 0) {
            return $this->errormsg("$email email does not exist.<br>Please login with another name or register", -1);
        }

        $store_password = $data['Password'];
        $tmp_password = $data['tmpPassword'];

        if (empty($oldPassword) || ($store_password != $this->set_password($oldPassword) && $tmp_password != $this->set_password($oldPassword))) {
            return $this->errormsg("This is wrong password or email", -2);
        }

        $newPassword = $this->set_password($newPassword);
        pdo_exec("Update login set Password='$newPassword', tmpPassword='' where email = '$email' limit 1");
        $this->msg = "Password has been updated";
        return $this->result = 1;
    }

    function DeleteMember($email) {

        pdo_exec("delete from member where email = '$email'");
        pdo_exec("delete from login where email = '$email'");
        pdo_exec("delete from logonehour where email = '$email'");

        return $this->result = 1;
    }

    function UpdateMember($value) {

        $fieldAr = $db->field_names("member");

        $query = $email = "";
        for ($i = 0, $sep = ""; $i < count($fieldAr); $i++, $sep = ", ") {
            if ($fieldAr[$i] != "ID" && array_key_exists($fieldAr[$i], $value)) {
                $query .= $sep . $fieldAr[$i] . " = '" . $value[$fieldAr[$i]] . "'";
                if ($fieldAr[$i] == "email") {
                    $email = $value["email"];
                }
            }
        }

        if (empty($email)) {
            return $this->errormsg("email empty. Unable to update member", -1);
        }

        $data = pdo_single_select("SELECT email FROM member where email = '$email' limit 1");
        if (count($data) <= 0) {
            return $this->errormsg("email does not exist. Unable to update User", -1);
        }

        pdo_exec("Update member set $query  where email = '$email' limit 1");
        return $this->result = 1;
    }

    function GetSystemStatus() {

        $data = pdo_single_select("SELECT Status FROM logtask WHERE Status like 'SYSTEM_STATUS:%' limit 1");
        if (count($data) <= 0) {
            return array("", "");
        }

        $SysAr = explode(":", $data['Status']);
        return array("onmaintenance" => intval($SysAr[0]), "redirection" => intval($SysAr[1]));
    }

    //https://developers.facebook.com/docs/reference/login/signed-request
    public static function getSignedRequest($signed_request = null) {

        $signed_request = isset($signed_request) ? $signed_request : $_REQUEST['signed_request'];

        if (isset($signed_request)) {
            $data_signed_request = explode('.', $signed_request); // Get the part of the signed_request we need.
            $jsonData = base64_decode($data_signed_request['1']); // Base64 Decode signed_request making it JSON.
            $objData = json_decode($jsonData, true); // Split the JSON into arrays.

            return $objData;
        }
        return false;
    }

    private function clean_arg($arg) {

        $this->arg = &$arg;
        $this->msg = "";
        $this->result = 0;

        if (isset($arg->email)) {
            $arg->email = $this->clean_input($arg->email);
        }

        if (isset($arg->password)) {
            $arg->password = substr($arg->password, 0, __pwd_max_length__);
        }

        if (isset($arg->rpassword)) {
            $arg->rpassword = $this->clean_input(substr($arg->rpassword, 0, __pwd_max_length__));
        }

        if (isset($arg->npassword)) {
            $arg->npassword = $this->clean_input(substr($arg->npassword, 0, __pwd_max_length__));
        }

        if (isset($arg->lastname)) {
            $arg->name = $this->clean_input($arg->lastname);
        }

        if (isset($arg->firstname)) {
            $arg->firstname = $this->clean_input($arg->firstname);
        }

        if (isset($arg->mobile)) {
            $arg->mobile = $this->clean_input($arg->mobile);
        }

        if (isset($arg->country)) {
            $arg->country = $this->clean_input($arg->country);
        }

        if (isset($arg->extrafield)) {
            $arg->extrafield = $this->clean_input($arg->extrafield);
        }

        if (isset($arg->userid)) {
            $arg->userid = $this->clean_input($arg->userid);
        }

        if (isset($arg->token)) {
            $arg->token = $this->clean_input($arg->token);
        }
    }

}

function set_action_login_type() {

    $action = $_SERVER['PHP_SELF'];
    if (preg_match('/backoffice/', $action))
        $_SESSION['login_type'] = LOGIN_BACKOFFICE;
    else if (preg_match('/admin_weeloy/', $action))
        $_SESSION['login_type'] = LOGIN_ADMIN;
    else if (preg_match('/admin_translation/', $action))
        $_SESSION['login_type'] = LOGIN_TRANSLATION;
    else if (preg_match('/admin_tms/', $action))
        $_SESSION['login_type'] = LOGIN_TMS;
    else $_SESSION['login_type'] = LOGIN_MEMBER;

    return $_SESSION['login_type'];
}

function get_valid_login_type($login_type) {

    global $login_type_allowed;

    if (!empty($login_type) && in_array($login_type, $login_type_allowed))
        return $login_type;

    if (!empty($_SESSION['login_type'])) {
        $login_type = $_SESSION['login_type'];
    }
    if (!empty($login_type) && in_array($login_type, $login_type_allowed))
        return $login_type;

    return LOGIN_MEMBER;
}

function set_valid_login_type($login_type) {

    switch ($login_type) {
        case "admin":
        case "admin_weeloy":
            $_SESSION['login_type'] = LOGIN_ADMIN;
            break;
        case "backoffice":
            $_SESSION['login_type'] = LOGIN_BACKOFFICE;
            break;
        case "translation":
            $_SESSION['login_type'] = LOGIN_TRANSLATION;
            break;
        case "tms":
            $_SESSION['login_type'] = LOGIN_TMS;
            break;
        case "member":
            $_SESSION['login_type'] = LOGIN_MEMBER;
            break;
        default:
            $_SESSION['login_type'] = LOGIN_MEMBER;
            break;
    }
    return $_SESSION['login_type'];
}

function getLoginURL($login_type) {

    switch ($login_type) {
        case LOGIN_BACKOFFICE:
            return __ROOTDIR__ . "/backoffice/index.php";

        case LOGIN_ADMIN:
            return __ROOTDIR__ . "/admin_weeloy/index.php";

        case LOGIN_TRANSLATION:
            return __ROOTDIR__ . "/admin_translation/index.php";

        case LOGIN_TMS:
            return __ROOTDIR__ . "/admin_tms/index.php";

        default:
            return __ROOTDIR__ . "/index.php";
    }
}

function getCookiename($type) {

    switch ($type) {
        case 'admin' :
            return __COOKIELOG__ . "admin";
        case 'backoffice' :
            return __COOKIELOG__ . "backoffice";
        case 'translation' :
            return __COOKIELOG__ . "translation";
        case 'tms' :
            return __COOKIELOG__ . "tms";
        case 'member' :
            return __COOKIELOG__ . "member";
        default: return "invalid";
    }
}

?>