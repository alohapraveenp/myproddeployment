<?php

require_once("lib/wpdo.inc.php");
require_once("lib/class.debug.inc.php");
require_once("lib/class.restaurant.inc.php");
require_once("lib/class.media.inc.php");
require_once("lib/class.booking.inc.php");
require_once("conf/conf.session.inc.php");
require_once("conf/conf.init.inc.php");
require_once("lib/class.translation.inc.php");
require_once('lib/class.mail.inc.php');
require_once('lib/class.sms.inc.php');
require_once('lib/class.pushnotif.inc.php');

class WY_Notification {

    public $sms_type = '';
    public $email_service = NULL;
    public $sms_service = NULL;
    public $apn_service = NULL;

    public $restaurant_configuration = array('email'=>true,'sms'=>true,'sms_premium'=>true,'apns'=>true);
    public $notification_type = array('email','sms','sms premium','push notification');
    public $smsprovider ='sms_premium';
    public $pname ='SMSP';
    


    
    public function __construct() {
        $this->email_service = new JM_Mail;
        $this->sms_service = new JM_Sms();
        $this->apn_service = new JM_Pushnotif();
    }

    public function getConfiguration($restaurant,$type){
      
        
        $notify =pdo_single_select("SELECT * FROM notification_type where label =' $type ' limit 1 ");
    
       if(count($notify)>0){
           $this->restaurant_configuration = array();
            $permission = explode('||',$notify['is_active']);
           
           foreach($permission as $p){
             
               if(in_array($p,$this->notification_type)){
                   $p =  strtolower($p);
                 
                   if($p==='sms premium'){$p ="sms_premium";}
                   if($p==='push notification'){$p ="push_notification";}
                  $this->restaurant_configuration[$p] =true;  
                  if($this->restaurant_configuration['sms_premium']){ $this->smsprovider ='sms_premium'; $this->pname ='SMSP';}
                  if($this->restaurant_configuration['sms']){ $this->smsprovider ='sms';$this->pname ='SMS';}
               }else{$this->restaurant_configuration[$p] =false;  }
           }
       }
        $res = pdo_single_select("SELECT * FROM notification_action where restaurant ='$restaurant' and action ='$type' limit 1 ");
       
         if(count($res)>0){
        
             $this->restaurant_configuration = array();
             $this->restaurant_configuration = array('email'=>$res['email'],'sms'=>$res['sms'],'sms_premium'=>$res['sms_premium'],'apns'=>$res['push_notification']);
             
              if($res['sms_premium']){ $this->smsprovider ='sms_premium';$this->pname ='SMSP';}
              if($res['sms']){ $this->smsprovider ='sms';$this->pname ='SMS';}
        }
  

     
    }


    public function getNotificationPreferences($target) {

        switch ($target) {
            case 'restaurant':

                break;
            case 'member':

                break;
        }
    }

    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    public function getDepositDetails($depositId){
        $data = pdo_single_select("SELECT * from booking_deposit where paykey = '$depositId' limit 1 ");
        if(count($data)>0){
            return $data;
        }
        return 0;
        
    }
    
    
    
        // assume that a getBooking was already done	
    function notifyBooking($booking,$qrcode_guest = NULL) {

        if (preg_match("/remote/i", $booking->tracking) == true) {
            return;
        }

        if (preg_match("/CHOPE/i", $booking->booker) == true) {
            return;
        }


        $mailer = new JM_Mail;
        $sms = new JM_Sms();
        $apn = new JM_Pushnotif();

        date_default_timezone_set('UTC');

        $date_sms = date("j/m/Y H:i", mktime(substr($booking->rtime, 0, 2), substr($booking->rtime, 3, 4), 0, intval(substr($booking->rdate, 5, 2)), intval(substr($booking->rdate, 8, 2)), intval(substr($booking->rdate, 0, 4))));
        $rdate = date("F j, Y, g:i a", mktime(substr($booking->rtime, 0, 2), substr($booking->rtime, 3, 4), 0, intval(substr($booking->rdate, 5, 2)), intval(substr($booking->rdate, 8, 2)), intval(substr($booking->rdate, 0, 4))));

        // Notification Member
        $rtime = preg_replace("/:00/", "h", $booking->rtime);
        $date_sms = date("j/m/Y H:i", mktime(substr($booking->rtime, 0, 2), substr($booking->rtime, 3, 4), 0, intval(substr($booking->rdate, 5, 2)), intval(substr($booking->rdate, 8, 2)), intval(substr($booking->rdate, 0, 4))));
        $rdate = date("F j, Y, g:i a", mktime(substr($booking->rtime, 0, 2), substr($booking->rtime, 3, 4), 0, intval(substr($booking->rdate, 5, 2)), intval(substr($booking->rdate, 8, 2)), intval(substr($booking->rdate, 0, 4))));
        $rdateres = date("l F j, Y, g:i a", mktime(substr($booking->rtime, 0, 2), substr($booking->rtime, 3, 4), 0, intval(substr($booking->rdate, 5, 2)), intval(substr($booking->rdate, 8, 2)), intval(substr($booking->rdate, 0, 4))));


        $rdate_date_only = date("F j, Y", mktime(substr($booking->rtime, 0, 2), substr($booking->rtime, 3, 4), 0, intval(substr($booking->rdate, 5, 2)), intval(substr($booking->rdate, 8, 2)), intval(substr($booking->rdate, 0, 4))));
        $rdate_time_only = date("H:i", mktime(substr($booking->rtime, 0, 2), substr($booking->rtime, 3, 4), 0, intval(substr($booking->rdate, 5, 2)), intval(substr($booking->rdate, 8, 2)), intval(substr($booking->rdate, 0, 4))));
        $date_gcalender = date("j-m-Y H:i", mktime(substr($booking->rtime, 0, 2), substr($booking->rtime, 3, 4), 0, intval(substr($booking->rdate, 5, 2)), intval(substr($booking->rdate, 8, 2)), intval(substr($booking->rdate, 0, 4))));

        $data_object = clone $booking;
        $data_object->rtime = $rtime;
        $data_object->rdate = $rdate;
        $data_object->rdateres = $rdateres;
        $data_object->siteUrl = __SERVERNAME__ . __ROOTDIR__;


        //$endTime = date("H:i:s", strtotime('+90 minutes', $this->rtime));
        //$date_gl = date("j/m/Y H:i", mktime(substr($endTime, 0, 2), substr($endTime, 3, 4), 0, intval(substr($endTime, 5, 2)), intval(substr($this->rdate, 8, 2)), intval(substr($this->rdate, 0, 4))));
        //$data_object->bkendtime = date("Ymd\THis\Z", strtotime($date_sms));


        date_default_timezone_set('UTC');
        $date_gcalender = strtotime($date_gcalender);
        $data_object->bkstartdate = gmdate("Ymd\THis\Z", $date_gcalender);

        $data_object->rdate_time_only = $rdate_time_only;
        $data_object->rdate_date_only = $rdate_date_only;

        $header['from'] = array('reservation@weeloy.com' => 'Weeloy.com');
        $header['replyto'] = array('info@weeloy.com');
//        if (isset($qrcode_guest)) {
//            $header['guest_qrcode'] = $qrcode_guest;
//        }

        $recipient = $booking->email;
        $recipient_mobile = $booking->mobile;
        $tracking = $booking->tracking;
        $data_object->isDeposit = false;
        if(isset($booking->deposit_id) && $booking->deposit_id !== ''){
            $depsoitDestails = $this->getDepositDetails($booking->deposit_id);
            if(count($depsoitDestails)>0){
                $data_object->isDeposit =true;
                $data_object->currency =$depsoitDestails['curency'];
                $data_object->amount =$depsoitDestails['amount'];
                $data_object->despositstatus =$depsoitDestails['status'];
            
            }
            
        }


        $is_white_label = false;
        if (preg_match("/CALLCENTER|WEBSITE|facebook/i", $booking->tracking) == true) {
            $is_white_label = true;
            $header['white_label'] = true;
            $header['replyto'] = array($booking->restaurantinfo->email);
        }
        $data_object->whiteLabel =$is_white_label;

        //$recipient = "philippe.benedetti@weeloy.com";
        $subject = "Reservation at " . $booking->restaurantinfo->title . " on " . $rdate;

        $data_object->baseemail = $booking->base64url_encode($booking->email);
        $data_object->baseconfirmation = $booking->base64url_encode($booking->confirmation);
        $data_object->baserdate = $booking->base64url_encode($rdate);

        //getting booking id purpose
        $data = pdo_single_select("SELECT id,restaurant from booking WHERE  confirmation = '$booking->confirmation' LIMIT 1");
        if (count($data) > 0) {
            $data_object->basebookid = $booking->base64url_encode($data['id']);
        }
        $data_object->shareLink = urlencode($data_object->siteUrl . "/modules/booking/bookingdetails.php/?b=" . $data_object->basebookid . "&rd=" . $data_object->baseconfirmation);
        $res = new WY_restaurant;
        $res->getRestaurant($booking->restaurant);
        $rescountry = $res->country;
        $data_object->rescountry = $rescountry;
        $data_object->restTel = $booking->restaurantinfo->tel;
        $data_object->ispromo = false;

        if (!$booking->restaurantinfo->is_wheelable && preg_match("/cpp_credit_suisse/i", $booking->tracking) == true) {
            $data = $res->getActivePromotion($booking->restaurant, $res->affiliate_program);
            if (count($data) > 0) {
                $data_object->offer = $data['offer_cpp'];
                $data_object->offer_description = $data['description_cpp'];
                $data_object->ispromo = true;
            }
        } else if (preg_match("/cpp_credit_suisse/i", $booking->tracking) == true) {
            $data_object->offermsgres = 'This is corporate booking. The corporate Wheel will be automatically selected in your Weeloy app.';
            $data_object->offermsgmem = 'Please bring your bussiness card to enjoy your corparate program Promotion';
            $data_object->iscpp = true;
        }


        /*         * ************************************* */
        //Email->restaurant terms &condition
        /*         * ************************************* */

        $data_object->restc = "";
        $res->lastorderdiner = "";
        $res->lastorderlunch = "";

        if (isset($res->bookinfo)) {
            $llorder = ($res->lastorderlunch != "12:00") ? $res->lastorderlunch : "";
            $ldorder = ($res->lastorderdiner != "18:00") ? $res->lastorderdiner : "";
            $showinfo = $res->bookinfo;

            if ($showinfo != "") {
                if ($llorder !== "")
                    $llorder = "Last order for lunch is " . $llorder;
                $data_object->llorder = $llorder;
                if ($ldorder !== "")
                    $ldorder = "Last order for diner is " . $ldorder;
                $data_object->ldorder = $ldorder;
            }

            $data_object->restc = $showinfo;
        }

        /**         * ************************************ */
        //Email->translation  dynamic content
        /*         * **************** ************************************* */

//        $emailEle = $this->getEmailContent();
             
        $trad = new WY_Translation;
        $emailEle = $trad->getEmailContent('EMAIL', $booking->language);
        $data_object->labelContent = $emailEle;

        /* End************************************************** */
        if (strpos($tracking, 'WEBSITE') !== false || strpos($tracking, 'facebook') !== false) {
            // WHITE LABEL 
            // Booking form on restaurant website

            $body = $mailer->getTemplate('callcenter/notifycallcenter_member', $data_object);

            $sms_message = "[NOREPLY] " . $booking->restaurantinfo->title . "\n" . $date_sms . "\n" . "Confirmation: " . $booking->confirmation . ", \n" . str_replace(" ", "", $booking->restaurantinfo->tel) . ' ' . $booking->restaurantinfo->address . " " . $booking->restaurantinfo->zip . ", \n" . $booking->restaurantinfo->city;
        } else if (strpos($tracking, 'CALLCENTER') !== false) {

            // WHITE LABEL 
            // Booking form on callcenter
            $body = $mailer->getTemplate('callcenter/notifycallcenter_member', $data_object);
            $sms_message = "[NOREPLY] " . $booking->restaurantinfo->title . "\n" . $date_sms . "\n" . "Confirmation: " . $booking->confirmation . ", \n" . str_replace(" ", "", $booking->restaurantinfo->tel) . ' ' . $booking->restaurantinfo->address . " " . $booking->restaurantinfo->zip . ", \n" . $booking->restaurantinfo->city;
        } else if (!$booking->restaurantinfo->is_bookable && $booking->restaurantinfo->is_wheelable) {
            // --> in weeloy.com 
            //   KALA
            //  UPDATE  notifyrequest_member and   notifylisting_member
            // Request weeloy.com
            $body = $mailer->getTemplate('booking/notifyrequest_member', $data_object);

            //$body = $mailer->getTemplate('newnotifyrequest_member', $data_object);
            //SMS UPDATE KALA
//            if ($res->country == 'Singapore') {
//                $sms_message = $this->restaurantinfo->title . "\n" . $date_sms . "\n" . "Reference: " . $this->confirmation . "\nWeeloy Code: " . $this->membCode . ",\n Book a restaurant with Weeloy & Win a Free $20 UBER ride!\n" . str_replace(" ", "", $this->restaurantinfo->tel);
//            } else {
//                $sms_message = $this->restaurantinfo->title . "\n" . $date_sms . "\n" . "Reference: " . $this->confirmation . "\nWeeloy Code: " . $this->membCode . ",\n" . str_replace(" ", "", $this->restaurantinfo->tel) . "\n" . $this->restaurantinfo->address . " " . $this->restaurantinfo->zip . ", \n" . $this->restaurantinfo->city;
//            }
            $sms_message = $booking->restaurantinfo->title . "\n" . $date_sms . "\n" . "Reference: " . $booking->confirmation . "\nWeeloy Code: " . $booking->membCode . ",\n" . str_replace(" ", "", $booking->restaurantinfo->tel) . "\n" . $booking->restaurantinfo->address . " " . $booking->restaurantinfo->zip . ", \n" . $booking->restaurantinfo->city;
        } else {

            //PASSBOOK
            if (!$is_white_label) {
                // add passbook for all bookings on weeloy.com 
                $passbook_link = __BASE_URL__ . '/passbook/' . base64_encode($booking->confirmation . '|||' . $booking->email);
                $data_object->passbook = $passbook_link;
            }
            // Booking weeloy.com
            if ($booking->restaurantinfo->is_wheelable) {
                $body = $mailer->getTemplate('booking/notifybooking_member', $data_object);
       
                //$body = $mailer->getTemplate('review/notifyreview_reminder', $data_object);
//                if ($res->country == 'Singapore') {
//                    //SMS UPDATE KALA
//                    $sms_message = $this->restaurantinfo->title . "\n" . $date_sms . "\n" . "Confirmation: " . $this->confirmation . "\nWeeloy Code: " . $this->membCode . ",\n Book a restaurant with Weeloy & Win a Free $20 UBER ride!\n" . str_replace(" ", "", $this->restaurantinfo->tel);
//                } else {
//                    // 
//                    $sms_message = $this->restaurantinfo->title . "\n" . $date_sms . "\n" . "Confirmation: " . $this->confirmation . "\nWeeloy Code: " . $this->membCode . ",\n" . str_replace(" ", "", $this->restaurantinfo->tel) . "\n" . $this->restaurantinfo->address . " " . $this->restaurantinfo->zip . ", \n" . $this->restaurantinfo->city;
//                }
                $sms_message = $booking->restaurantinfo->title . "\n" . $date_sms . "\n" . "Confirmation: " . $booking->confirmation . "\nWeeloy Code: " . $booking->membCode . ",\n" . str_replace(" ", "", $booking->restaurantinfo->tel) . "\n" . $booking->restaurantinfo->address . " " . $booking->restaurantinfo->zip . ", \n" . $booking->restaurantinfo->city;
            } else {

                // Listing only
                $body = $mailer->getTemplate('booking/notifylisting_member', $data_object);


//                if ($res->country == 'Singapore') {
//                   $sms_message = $this->restaurantinfo->title . "\n" . $date_sms . "\n" . "Confirmation: " . $this->confirmation . ",\n Book a restaurant with Weeloy & Win a Free $20 UBER ride!\n" . str_replace(" ", "", $this->restaurantinfo->tel);
//                    
//                } else {
//                    $sms_message = $this->restaurantinfo->title . "\n" . $date_sms . "\n" . "Confirmation: " . $this->confirmation . ",\n" . str_replace(" ", "", $this->restaurantinfo->tel) . "\n" . $this->restaurantinfo->address . " " . $this->restaurantinfo->zip . ", \n" . $this->restaurantinfo->city;
//                }
                $sms_message = $booking->restaurantinfo->title . "\n" . $date_sms . "\n" . "Confirmation: " . $booking->confirmation . ",\n" . str_replace(" ", "", $booking->restaurantinfo->tel) . "\n" . $booking->restaurantinfo->address . " " . $booking->restaurantinfo->zip . ", \n" . $booking->restaurantinfo->city;
                if ($booking->restaurant == 'SG_SG_R_Bacchanalia') {
                    $sms_message = $booking->restaurantinfo->title . "\n" . $date_sms . ",\n" . str_replace(" ", "", $booking->restaurantinfo->tel) . "\n" . $booking->restaurantinfo->address . " " . $booking->restaurantinfo->zip . ", \n" . $booking->restaurantinfo->city;
                }
                //SMS UPDATE KALA
            }
        }


        if (!empty($booking->email) && $booking->email != "no@email.com") {
            $mailer->sendmail($recipient, $subject, $body, $header, $booking->restaurant, $booking->confirmation);
        }
        $smsid = "Weeloy";
        if (preg_match("/walkin|CALLCENTER|tms|waiting|WEBSITE|facebook/i", $data_object->tracking) == true && !empty($data_object->restaurantinfo->smsid))
            $smsid = $data_object->restaurantinfo->smsid;
        $sms->sendSmsMessage($recipient_mobile, $sms_message, $smsid, $booking->restaurant, $booking->confirmation);
        //unset($header['guest_qrcode']);
        //unlink('tmp',$a->image(6));
        //restaurant Email 
        unset($header['passbook']);

        $notif_message = "New booking: " . $booking->restaurantinfo->title . " " . $booking->cover . 'pp - ' . $date_sms;
        if (!$is_white_label) {
            //get all device related to this account
            $sql = "SELECT md.mobile_id, md.user_id, md.mobile_type FROM mobile_devices md WHERE app_name = 'weeloy' AND md.status = 'active' AND  md.user_id = '$booking->email'";


            $data = pdo_multiple_select($sql);
            foreach ($data as $row) {
                $recipient = $row['mobile_id'];
                $pn_type = $row['mobile_type'];
                $apn->sendPushnotifMessage($recipient, $notif_message, $pn_type, null, 'weeloy', $booking->restaurant, $booking->confirmation); // null for options ?
            }
        }
        $recipient = $booking->restaurantinfo->email;

        if ($is_white_label === true) {
            $header['replyto'] = array($booking->email);
        }

        // $recipient = "support@weeloy.com";
        $subject = "New reservation: " . $rdate . ", " . $booking->salutation . " " . $booking->firstname . " " . $booking->lastname . " at " . $booking->restaurantinfo->title . " - " . $booking->confirmation;

        if (preg_match("/walkin|CALLCENTER|tms|waiting|WEBSITE|facebook/i", $tracking) == true) {
            $body = $mailer->getTemplate('callcenter/notifycallcenter_restaurant', $data_object);
        } else if (!$booking->restaurantinfo->is_bookable && $booking->restaurantinfo->is_wheelable) {
            $body = $mailer->getTemplate('booking/notifyrequest_restaurant', $data_object);
        } else {
            if ($booking->restaurantinfo->is_wheelable) {
                $body = $mailer->getTemplate('booking/notifybooking_restaurant', $data_object);
            } else {
                $body = $mailer->getTemplate('booking/notifylisting_restaurant', $data_object);
            }
        }
        $mailer->sendmail($recipient, $subject, $body, $header, $booking->restaurant, $booking->confirmation);


        //SMS to restaurant Manager
        if ($res->smsNotifToRestaurant()) {
             $sms_message = 'NEW BOOKING ' . $booking->restaurantinfo->title . "\n" . $date_sms . " " . $booking->cover . "pax\n" . "Confirmation: " . $booking->confirmation . "\nName " . $booking->firstname . ' ' . $booking->lastname . "\nPhone " . $booking->mobile;
            $this->sendSmsToRestaurant($res, $booking, $data_object,$sms_message, $smsid, 'SMSP');
            //$recipient_mobile = $res->getManagerPhoneNumber();
           

            //$sms->sendSmsMessage($recipient_mobile, $sms_message, $smsid, $this->restaurant, $this->confirmation);
        }
        $notif_message = "New booking: " . $date_sms . " " . $booking->cover . 'pp - ' . $booking->restaurantinfo->title;
        //get all device related to this account
        $sql = "SELECT md.mobile_id, md.user_id, md.mobile_type FROM restaurant_app_managers ram, mobile_devices md WHERE app_name = 'weeloy_pro' AND md.status = 'active' AND ram.user_id = md.user_id AND `restaurant_id` LIKE '" . $booking->restaurant . "'";
        $data = pdo_multiple_select($sql);
        foreach ($data as $row) {
            $recipient = $row['mobile_id'];
            $pn_type = $row['mobile_type'];
            $apn->sendPushnotifMessage($recipient, $notif_message, $pn_type, null, 'weeloy_pro', $booking->restaurant, $booking->confirmation); // null for options ?
        }
    }

    function notifyBookingWalkable($booking) {

        if (preg_match("/remote/i", $booking->tracking) == true)
            return;

        if (preg_match("/CHOPE/i", $booking->booker) == true)
            return;

        $mailer = new JM_Mail;
        $sms = new JM_Sms();
        // Notification Member

        $rtime = $booking->rtime;
        $rdate = $booking->rdate;
        //$date_sms = date("j/m/Y H:i", mktime(substr($this->time, 0, 2), substr($this->time, 3, 4), 0, intval(substr($this->date, 5, 2)), intval(substr($this->rdate, 8, 2)), intval(substr($this->rdate, 0, 4))));
        //$rdate = date("F j, Y, g:i a", mktime(substr($this->time, 0, 2), substr($this->time, 3, 4), 0, intval(substr($this->date, 5, 2)), intval(substr($this->rdate, 8, 2)), intval(substr($this->rdate, 0, 4))));

        $data_object = clone $booking;
        $data_object->rtime = $rtime;
        $data_object->rdate = $rdate;

        $recipient = $booking->email;
        $recipient_mobile = $booking->mobile;
        $res = new WY_restaurant;
        $res->getRestaurant($booking->restaurant);
        $header['from'] = array('reservation@weeloy.com' => 'Weeloy.com');
        $header['replyto'] = array($res->email);

        $subject = "Wheel Prize at " . $res->title . " on " . $rdate;
        $body = $mailer->getTemplate('wheelwin/notifywin_walkin_member', $this);

        $mailer->sendmail($recipient, $subject, $body, $header, $res->restaurant, $booking->confirmation);

        $sms_message = 'Congratulations, you won ' . $booking->wheelwin . ' by turning the Weeloy Wheel! Thank you for choosing ' . $res->title;


        $smsid = "Weeloy";
        if (preg_match("/CALLCENTER|WEBSITE|facebook/i", $data_object->tracking) == true && !empty($data_object->restaurantinfo->smsid))
            $smsid = $data_object->restaurantinfo->smsid;
        $sms->sendSmsMessage($recipient_mobile, $sms_message, $smsid, $this->restaurant, $this->confirmation);

        // Notification Restaurant

        $recipient = $res->email;
        $subject = "Wheel Prize at " . $res->title . " on " . $rdate;
        $body = $mailer->getTemplate('wheelwin/notifywin_walkin_restaurant', $data_object);

        $mailer->sendmail($recipient, $subject, $body, $header, $booking->restaurant, $booking->confirmation);
    }

    function notifynoshow($booking,$type = 'cancel') {
  

        if (preg_match("/remote/i", $booking->tracking) == true)
            return;

        if (preg_match("/CHOPE/i", $booking->booker) == true)
            return;

        $is_white_label = false;
        if (preg_match("/walkin|CALLCENTER|tms|waiting|WEBSITE|facebook/i", $booking->tracking) == true) {
            $is_white_label = true;
        }

        $mailer = new JM_Mail;
        $sms = new JM_Sms();

        $rtime = preg_replace("/:00/", "h", $booking->rtime);
        $date_sms = date("j/m/Y H:i", mktime(substr($booking->rtime, 0, 2), substr($booking->rtime, 3, 4), 0, intval(substr($booking->rdate, 5, 2)), intval(substr($booking->rdate, 8, 2)), intval(substr($booking->rdate, 0, 4))));
        $rdate = date("F j, Y, g:i a", mktime(substr($booking->rtime, 0, 2), substr($booking->rtime, 3, 4), 0, intval(substr($booking->rdate, 5, 2)), intval(substr($booking->rdate, 8, 2)), intval(substr($booking->rdate, 0, 4))));

        $data_object = clone $booking;
        $data_object->rtime = $booking->rtime;
        $data_object->rdate = $rdate;
        $data_object->guests = $booking->cover;

        $recipient = $booking->email;

        $recipient_mobile = $booking->mobile;
        $header['from'] = array('reservation@weeloy.com' => 'Weeloy.com');
        $header['replyto'] = array('info@weeloy.com');
        if ($is_white_label) {
            $header['white_label'] = true;
            $header['replyto'] = array($booking->restaurantinfo->email);
        }

        //need to update proper text message
        $subject = "Reservation noshow  at " . $booking->restaurantinfo->title . " on " . $rdate;
        $body = $mailer->getTemplate('cancel/notifynoshowbooking_member', $booking);

        $mailer->sendmail($recipient, $subject, $body, $header, $booking->restaurant, $booking->confirmation);
    }

    function notifyWin($booking) {

        if (preg_match("/remote/i", $booking->tracking) == true)
            return;

        if (preg_match("/CHOPE/i", $booking->booker) == true)
            return;

        $mailer = new JM_Mail;
        $sms = new JM_Sms();
        $apn = new JM_Pushnotif();
        $trad = new WY_Translation;
        // Notification Member
        $rtime = preg_replace("/:00/", "h", $booking->rtime);
        $date_sms = date("j/m/Y H:i", mktime(substr($booking->rtime, 0, 2), substr($booking->rtime, 3, 4), 0, intval(substr($booking->rdate, 5, 2)), intval(substr($booking->rdate, 8, 2)), intval(substr($booking->rdate, 0, 4))));
        $rdate = date("F j, Y, g:i a", mktime(substr($booking->rtime, 0, 2), substr($booking->rtime, 3, 4), 0, intval(substr($booking->rdate, 5, 2)), intval(substr($booking->rdate, 8, 2)), intval(substr($booking->rdate, 0, 4))));

        $data_object = clone $booking;
        $data_object->rtime = $rtime;
        $data_object->rdate = $rdate;

        $recipient = $booking->email;
        $recipient_mobile = $booking->mobile;
        $header['from'] = array('reservation@weeloy.com' => 'Weeloy.com');
        $header['replyto'] = array('info@weeloy.com');
        $res = new WY_restaurant;
        $res->getRestaurant($booking->restaurant);
        $white_label = false;
        $rescountry = $res->country;
        $data_object->rescountry = $rescountry;

        $emailEle = $trad->getEmailContent('EMAIL', $booking->language);
       
        $data_object->labelContent = $emailEle;

        $offer = $res->getBestOffer($booking->restaurant, $booking->wheelwin, $res->is_wheelable, 1);

        $data_object->offer_description = $offer['description'];

        //phil fix
        $data_object->offer_description = $booking->wheeldesc;

        $data_object->baseemail = $booking->base64url_encode($booking->email);
        $data_object->baseconfirmation = $booking->base64url_encode($booking->confirmation);

        $subject = "Wheel Prize at " . $booking->restaurantinfo->title . " on " . $rdate;
        $body = $mailer->getTemplate('wheelwin/notifywin_member', $data_object);
        if (preg_match("/walkin|CALLCENTER|tms|waiting|WEBSITE|facebook/i", $data_object->tracking) == true) {
            $white_label = true;
            $header['from'] = array('reservation@weeloy.com' => 'Weeloy.com');
            $header['replyto'] = array($booking->restauranstinfo->email);
        }
        $mailer->sendmail($recipient, $subject, $body, $header, $booking->restaurant, $booking->confirmation);

        //Kala updated
//        if ($res->country == 'Singapore') {
//            $sms_message = 'Congratulations, you won ' . $this->wheelwin . ' by turning the Weeloy Wheel!\nDouble your reward:$20 off on your first ride with UBER (more info in email)\n';
//        } else {
//            $sms_message = 'Congratulations, you won ' . $this->wheelwin . ' by turning the Weeloy Wheel! Thank you for choosing ' . $this->restaurantinfo->title;
//        }
        $sms_message = 'Congratulations, you won ' . $booking->wheelwin . ' by turning the Weeloy Wheel! Thank you for choosing ' . $booking->restaurantinfo->title;
        $smsid = "Weeloy";
        if (preg_match("/walkin|CALLCENTER|tms|waiting|WEBSITE|facebook/i", $data_object->tracking) == true && !empty($data_object->restaurantinfo->smsid))
            $smsid = $data_object->restaurantinfo->smsid;
        $sms->sendSmsMessage($recipient_mobile, $sms_message, $smsid, $booking->restaurant, $booking->confirmation);

        $notif_message = "Congratulations, you have won " . $booking->wheelwin . " by turning the Weeloy Wheel!";
        //get all device related to this account
        $sql = "SELECT md.mobile_id, md.user_id, md.mobile_type FROM mobile_devices md WHERE app_name = 'weeloy' AND md.status = 'active' AND  md.user_id = '$booking->email'";

        $data = pdo_multiple_select($sql);
        foreach ($data as $row) {
            $recipient = $row['mobile_id'];
            $pn_type = $row['mobile_type'];
            $apn->sendPushnotifMessage($recipient, $notif_message, $pn_type, null, 'weeloy', $booking->restaurant, $booking->confirmation); // null for options ?
        }

        // Notification Restaurant

        $recipient = $booking->restaurantinfo->email;
        if ($white_label === true) {
            $header['replyto'] = array($booking->email);
        }
        $subject = "Wheel Prize at " . $booking->restaurantinfo->title . " on " . $rdate;
        $body = $mailer->getTemplate('wheelwin/notifywin_restaurant', $data_object);

        $mailer->sendmail($recipient, $subject, $body, $header, $booking->restaurant, $booking->confirmation);
    }

    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    // assume that a getBooking was already done	
    function notifyReminderConsumer($confirmation) {

        $mailer = new JM_Mail;
        $sms = new JM_Sms();
        $apn = new JM_Pushnotif();

        $booking = new WY_Booking();
        $booking->getBooking($confirmation);
        if($booking->result < 0)
        	return;


        //global function
        $data_object = $this->notifyGlobalData($booking);


        $header['from'] = array('reservation@weeloy.com' => 'Weeloy.com');
        $header['replyto'] = array('info@weeloy.com');
//        if (isset($qrcode_guest)) {
//            $header['guest_qrcode'] = $qrcode_guest;
//        }

        $recipient = $booking->email;
        $recipient_mobile = $booking->mobile;
        $tracking = $booking->tracking;

        $is_white_label = false;


        //$recipient = "philippe.benedetti@weeloy.com";
        $subject = "Reservation at " . $booking->restaurantinfo->title . " on " . $rdate;

        $data_object->baseemail = $booking->base64url_encode($booking->email);

        $data_object->baseconfirmation = $booking->base64url_encode($booking->confirmation);
        $data_object->baserdate = $booking->base64url_encode($rdate);

        //getting booking id purpose
        $data = pdo_single_select("SELECT id,restaurant from booking WHERE  confirmation = '$booking->confirmation' LIMIT 1");
        if (count($data) > 0) {
            $data_object->basebookid = $booking->base64url_encode($data['id']);
        }
        $data_object->shareLink = urlencode($data_object->siteUrl . "/modules/booking/bookingdetails.php/?b=" . $data_object->basebookid . "&rd=" . $data_object->baseconfirmation);
        $res = new WY_restaurant;
        $res->getRestaurant($booking->restaurant);
        $rescountry = $res->country;
        $data_object->rescountry = $rescountry;
        $data_object->restTel = $booking->restaurantinfo->tel;

        $data_object->ispromo = false;
        if (!$booking->restaurantinfo->is_wheelable && preg_match("/cpp_credit_suisse/i", $booking->tracking) == true) {
            $data = $res->getActivePromotion($booking->restaurant, $res->affiliate_program);
            if (count($data) > 0) {
                $data_object->offer = $data['offer_cpp'];
                $data_object->offer_description = $data['description_cpp'];
                $data_object->ispromo = true;
            }
        }
        /*         * ************************************* */
        //Email->restaurant terms &condition
        /*         * ************************************* */
        $data_object->restc = "";
        $res->lastorderdiner = "";
        $res->lastorderlunch = "";
        if (preg_match("/CALLCENTER|WEBSITE|facebook/", $tracking) == true) {
            $is_white_label = true;
            $header['white_label'] = true;

            $header['replyto'] = array($booking->restaurantinfo->email);
        }

        if (isset($res->bookinfo)) {
            $llorder = ($res->lastorderlunch != "12:00") ? $res->lastorderlunch : "";
            $ldorder = ($res->lastorderdiner != "18:00") ? $res->lastorderdiner : "";
            $showinfo = $res->bookinfo;

            if ($showinfo != "") {
                if ($llorder !== "")
                    $llorder = "Last order for lunch is " . $llorder;
                $data_object->llorder = $llorder;
                if ($ldorder !== "")
                    $ldorder = "Last order for diner is " . $ldorder;
                $data_object->ldorder = $ldorder;
            }

            $data_object->restc = $showinfo;
        }

        /* End************************************************** */

        if (preg_match("/WEBSITE|facebook/", $tracking) == true) {
            // WHITE LABEL 
            // Booking form on restaurant website

            $body = $mailer->getTemplate('callcenter/notify_callcenter', $data_object);
            $sms_message = "[NOREPLY] Booking reminder: " . $booking->restaurantinfo->title . "\n" . $data_object->date_sms . "\n" . "Confirmation: " . $booking->confirmation . ", \n" . str_replace(" ", "", $booking->restaurantinfo->tel) . ' ' . $booking->restaurantinfo->address . " " . $booking->restaurantinfo->zip . ", \n" . $booking->restaurantinfo->city;
        } else if (preg_match("/CALLCENTER/", $tracking) == true) {

            // WHITE LABEL 
            // Booking form on callcenter
            $body = $mailer->getTemplate('callcenter/notify_callcenter', $data_object);
            $sms_message = "[NOREPLY] Booking reminder: " . $booking->restaurantinfo->title . "\n" . $data_object->date_sms . "\n" . "Confirmation: " . $booking->confirmation . ", \n" . str_replace(" ", "", $booking->restaurantinfo->tel) . ' ' . $booking->restaurantinfo->address . " " . $booking->restaurantinfo->zip . ", \n" . $booking->restaurantinfo->city;
        } else if (!$booking->restaurantinfo->is_bookable && $booking->restaurantinfo->is_wheelable) {
            // --> in weeloy.com 
            //   KALA
            //  UPDATE  notifyrequest_member and   notifylisting_member
            // Request weeloy.com
            $body = $mailer->getTemplate('booking/notifyrequest_reminder', $data_object);
            //$body = $mailer->getTemplate('newnotifyrequest_member', $data_object);
            //SMS UPDATE KALA
//            if ($res->country == 'Singapore') {
//                $sms_message = "Booking reminder: " . $booking->restaurantinfo->title . "\n" . $data_object->date_sms . "\n" . "Reference: " . $booking->confirmation . "\nWeeloy Code: " . $booking->membCode . ",\n Book a restaurant with Weeloy & Win a Free $20 UBER ride!\n" . str_replace(" ", "", $booking->restaurantinfo->tel);
//            } else {
//                $sms_message = "Booking reminder: " . $booking->restaurantinfo->title . "\n" . $data_object->date_sms . "\n" . "Reference: " . $booking->confirmation . "\nWeeloy Code: " . $booking->membCode . ",\n" . str_replace(" ", "", $booking->restaurantinfo->tel) . "\n" . $booking->restaurantinfo->address . " " . $booking->restaurantinfo->zip . ", \n" . $booking->restaurantinfo->city;
//            }
            $sms_message = "Booking reminder: " . $booking->restaurantinfo->title . "\n" . $data_object->date_sms . "\n" . "Reference: " . $booking->confirmation . "\nWeeloy Code: " . $booking->membCode . ",\n" . str_replace(" ", "", $booking->restaurantinfo->tel) . "\n" . $booking->restaurantinfo->address . " " . $booking->restaurantinfo->zip . ", \n" . $booking->restaurantinfo->city;
            $apn_message = "Booking reminder: " . $data_object->date_sms . ", " . $booking->restaurantinfo->title . ", " . $booking->cover . "pp";
        } else {
            // Booking weeloy.com
            if ($booking->restaurantinfo->is_wheelable) {

                $body = $mailer->getTemplate('booking/notifybooking_reminder', $data_object);
                //$body = $mailer->getTemplate('notifybooking_member', $data_object);

                if ($res->country == 'Singapore') {
                    //SMS UPDATE KALA
                    $sms_message = "Booking reminder: " . $booking->restaurantinfo->title . "\n" . $data_object->date_sms . "\n" . "Confirmation: " . $booking->confirmation . "\n" . str_replace(" ", "", $booking->restaurantinfo->tel) . "\n" . $booking->restaurantinfo->address . " " . $booking->restaurantinfo->zip . ", \n" . $booking->restaurantinfo->city;
                } else {
                    // 
                    $sms_message = "Booking reminder: " . $booking->restaurantinfo->title . "\n" . $data_object->date_sms . "\n" . "Confirmation: " . $booking->confirmation . "\nWeeloy Code: " . $booking->membCode . ",\n" . str_replace(" ", "", $booking->restaurantinfo->tel) . "\n" . $booking->restaurantinfo->address . " " . $booking->restaurantinfo->zip . ", \n" . $booking->restaurantinfo->city;
                }
                $apn_message = "Booking reminder: " . $data_object->date_sms . ", " . $booking->restaurantinfo->title . ", " . $booking->cover . "pp";
            } else {
                // Listing only
                $body = $mailer->getTemplate('booking/notifylisting_reminder', $data_object);

                if ($res->country == 'Singapore') {

                    $sms_message = "Booking reminder: " . $booking->restaurantinfo->title . "\n" . $data_object->date_sms . "\n" . "Confirmation: " . $booking->confirmation . ",\n" . str_replace(" ", "", $booking->restaurantinfo->tel) . "\n" . $booking->restaurantinfo->address . " " . $booking->restaurantinfo->zip . ", \n" . $booking->restaurantinfo->city;
                } else {

                    $sms_message = "Booking reminder: " . $booking->restaurantinfo->title . "\n" . $data_object->date_sms . "\n" . "Confirmation: " . $booking->confirmation . ",\n" . str_replace(" ", "", $booking->restaurantinfo->tel) . "\n" . $booking->restaurantinfo->address . " " . $booking->restaurantinfo->zip . ", \n" . $booking->restaurantinfo->city;
                }
                $apn_message = "Booking reminder: " . $data_object->date_sms . ", " . $booking->restaurantinfo->title . ", " . $booking->cover . "pp";
                //SMS UPDATE KALA
            }
        }
        if ($is_white_label === true) {
            $header['replyto'] = array($booking->email);
        }
        if (!empty($booking->email) && $booking->email != "no@email.com")
            $mailer->sendmail($recipient, $subject, $body, $header, $booking->restaurant, $booking->confirmation);


        if (!empty($recipient_mobile)) {
            $sms->sendSmsMessage($recipient_mobile, $sms_message, '', $booking->restaurant, $booking->confirmation, $booking->confirmation);
        }

        //$notif_message = "Reminder: You have a booking tomorrow at " . $booking->restaurantinfo->title . "";
        $sql = "SELECT md.mobile_id, md.user_id, md.mobile_type FROM mobile_devices md WHERE app_name = 'weeloy' AND md.status = 'active' AND  md.user_id = '$booking->email'";

        $data = pdo_multiple_select($sql);
        foreach ($data as $row) {
            $recipient = $row['mobile_id'];
            $pn_type = $row['mobile_type'];
            $apn->sendPushnotifMessage($recipient, $apn_message, $pn_type, null, 'weeloy', $booking->restaurant, $booking->confirmation); // null for options ?
        }
    }

    function notifyGlobalData($booking) {

        date_default_timezone_set('UTC');

        $date_sms = date("j/m/Y H:i", mktime(substr($booking->rtime, 0, 2), substr($booking->rtime, 3, 4), 0, intval(substr($booking->rdate, 5, 2)), intval(substr($booking->rdate, 8, 2)), intval(substr($booking->rdate, 0, 4))));
        $rdate = date("F j, Y, g:i a", mktime(substr($booking->rtime, 0, 2), substr($booking->rtime, 3, 4), 0, intval(substr($booking->rdate, 5, 2)), intval(substr($booking->rdate, 8, 2)), intval(substr($booking->rdate, 0, 4))));

        // Notification Member
        $rtime = preg_replace("/:00/", "h", $booking->rtime);
        $date_sms = date("j/m/Y H:i", mktime(substr($booking->rtime, 0, 2), substr($booking->rtime, 3, 4), 0, intval(substr($booking->rdate, 5, 2)), intval(substr($booking->rdate, 8, 2)), intval(substr($booking->rdate, 0, 4))));
        $rdate = date("F j, Y, g:i a", mktime(substr($booking->rtime, 0, 2), substr($booking->rtime, 3, 4), 0, intval(substr($booking->rdate, 5, 2)), intval(substr($booking->rdate, 8, 2)), intval(substr($booking->rdate, 0, 4))));

        $rdateres = date("l F j, Y, g:i a", mktime(substr($booking->rtime, 0, 2), substr($booking->rtime, 3, 4), 0, intval(substr($booking->rdate, 5, 2)), intval(substr($booking->rdate, 8, 2)), intval(substr($booking->rdate, 0, 4))));


        $rdate_date_only = date("F j, Y", mktime(substr($booking->rtime, 0, 2), substr($booking->rtime, 3, 4), 0, intval(substr($booking->rdate, 5, 2)), intval(substr($booking->rdate, 8, 2)), intval(substr($booking->rdate, 0, 4))));
        $rdate_time_only = date("H:i", mktime(substr($booking->rtime, 0, 2), substr($booking->rtime, 3, 4), 0, intval(substr($booking->rdate, 5, 2)), intval(substr($booking->rdate, 8, 2)), intval(substr($booking->rdate, 0, 4))));


        $data_object = clone $booking;
        $data_object->rtime = $rtime;
        $data_object->rdate = $rdate;
        $data_object->siteUrl = __SERVERNAME__ . __ROOTDIR__;
        $data_object->date_sms = $date_sms;
        $data_object->rdateres = $rdateres;

        $data_object->bkstartdate = gmdate("Ydm\THis\Z", strtotime($date_sms . 'UTC'));

        //$endTime = date("H:i:s", strtotime('+90 minutes', $booking->rtime));
        //$date_gl = date("j/m/Y H:i", mktime(substr($endTime, 0, 2), substr($endTime, 3, 4), 0, intval(substr($endTime, 5, 2)), intval(substr($booking->rdate, 8, 2)), intval(substr($booking->rdate, 0, 4))));
        $data_object->bkendtime = date("Ymd\THis\Z", strtotime($date_sms));

        $data_object->rdate_time_only = $rdate_time_only;
        $data_object->rdate_date_only = $rdate_date_only;
        $emailEle = $booking->getEmailContent();
        $data_object->baseemail = $booking->base64url_encode($booking->email);
        $data_object->baseconfirmation = $booking->base64url_encode($booking->confirmation);
        $data_object->labelContent = $emailEle;

        return $data_object;
    }

    function notifyReviewConsumer($confirmation) {
        $mailer = new JM_Mail;

        $booking = new WY_Booking();
        $booking->getBooking($confirmation);
        if($booking->result < 0)
        	return;

        $data_object = $this->notifyGlobalData($booking);

        $header['from'] = array('sales@weeloy.com' => 'Weeloy.com');
        $header['replyto'] = array('sales@weeloy.com');

        $recipient = $booking->email;

        $subject = "Review reminder on the " . $booking->restaurantinfo->title;
        $body = $mailer->getTemplate('review/notifyreview_reminder', $data_object);
        if (!empty($recipient) && $recipient != "no@email.com")
            $mailer->sendmail($recipient, $subject, $body, $header, $booking->restaurant, $booking->confirmation);
    }

    function notifyCatering($orderDetails, $items) {

        $mailer = new JM_Mail;
        $sms = new JM_Sms();
        $apn = new JM_Pushnotif();

        $data_object = $orderDetails;
        $data_object->siteUrl = __SERVERNAME__ . __ROOTDIR__;

        $header['from'] = array('reservation@weeloy.com' => 'Weeloy.com');
        $header['replyto'] = array('info@weeloy.com');
//        if (isset($qrcode_guest)) {
//            $header['guest_qrcode'] = $qrcode_guest;
//        }

        $recipient = $orderDetails->email;
        $recipient_mobile = $orderDetails->mobile;

        $res = new WY_restaurant;
        $res->getRestaurant($orderDetails->restaurant);

        $title = $res->title;
        $data_object->title = $title;
        $data_object->address = $res->address;
        $data_object->city = $res->city;
        $data_object->zip = $res->zip;
        $rescountry = $res->country;
        $data_object->rescountry = $rescountry;
        $data_object->restTel = $res->tel;
        $data_object->itemdetails = $items;
        $tmpArray = array();
        $menuobj = array('item_title' => '', 'quantity' => '');
        $menus = array('item');

        foreach ($items as $item) {
            $menuobj['item_title'] = $item->item_title;
            $menuobj['quantity'] = $item->quantity;
            array_push($tmpArray, $menuobj);
        }

        $data_object->itemdetails = $tmpArray;

        //$recipient = "philippe.benedetti@weeloy.com";
        $subject = "Online Order at " . $title . " on " . $orderDetails->delivery_date;

        $body = $mailer->getTemplate('catering/notify_member', $data_object);

        if (!empty($recipient) && $recipient != "no@email.com") {
            $mailer->sendmail($recipient, $subject, $body, $header, $orderDetails->restaurant, $orderDetails->orderID);
        }
        // $recipient = "support@weeloy.com";
        $recipient = $res->email;
        $body = $mailer->getTemplate('catering/notify_restaurant_catering', $data_object);
        $subject = "New Online Order at " . $res->title . " on " . $orderDetails->delivery_date;

        $res = $mailer->sendmail($recipient, $subject, $body, $header, $orderDetails->restaurant, $orderDetails->orderID);
    }

    function notifyreminderCatering($orderDetails) {

        $mailer = new JM_Mail;
        $sms = new JM_Sms();
        $apn = new JM_Pushnotif();

        $data_object = $orderDetails;
        $data_object->siteUrl = __SERVERNAME__ . __ROOTDIR__;

        $header['from'] = array('reservation@weeloy.com' => 'Weeloy.com');
        $header['replyto'] = array('info@weeloy.com');
//        if (isset($qrcode_guest)) {
//            $header['guest_qrcode'] = $qrcode_guest;
//        }

        $recipient = $orderDetails->email;
        $recipient_mobile = $orderDetails->phone;

        $res = new WY_restaurant;
        $res->getRestaurant($orderDetails->restaurant_id);
        $title = $res->title;
        $data_object->title = $title;
        $data_object->address = $res->address;
        $data_object->city = $res->city;
        $data_object->zip = $res->zip;
        $rescountry = $res->country;
        $data_object->rescountry = $rescountry;
        $data_object->restTel = $res->tel;

        //$recipient = "philippe.benedetti@weeloy.com";
        $subject = "Online Order at " . $title . " on " . $orderDetails->delivery_date;

        $body = $mailer->getTemplate('catering/notify_member_reminder', $data_object);


        if (!empty($recipient) && $recipient != "no@email.com") {
            $mailer->sendmail($recipient, $subject, $body, $header, $orderDetails->restaurant_id, $orderDetails->orderID);
        }
        // $recipient = "support@weeloy.com";
        $recipient = $res->email;
        $body = $mailer->getTemplate('catering/notify_restaurant_catering_reminder', $data_object);
        $subject = "New Online Order at " . $res->title . " on " . $orderDetails->delivery_date;
        $mailer->sendmail($recipient, $subject, $body, $header, $orderDetails->restaurant_id, $orderDetails->orderID);
    }

    function notifyUpdateBooking($confirmation) {

        $mailer = new JM_Mail;
        $sms = new JM_Sms();
        $apn = new JM_Pushnotif();

        $booking = new WY_Booking();
        $booking->getBooking($confirmation);
        if($booking->result < 0)
        	return;
        	
        if (preg_match("/remote/i", $booking->tracking) == true) {
            return;
        }
        if (preg_match("/CHOPE/i", $booking->booker) == true) {
            return;
        }

        //global function
        $data_object = $this->notifyGlobalData($booking);


        $header['from'] = array('reservation@weeloy.com' => 'Weeloy.com');
        $header['replyto'] = array('info@weeloy.com');
//        if (isset($qrcode_guest)) {
//            $header['guest_qrcode'] = $qrcode_guest;
//        }

        $recipient = $booking->email;
        $recipient_mobile = $booking->mobile;
        $tracking = $booking->tracking;

        $is_white_label = false;

  $prev_booking = pdo_single_select("SELECT rdate, rtime, cover, specialrequest, lastname, firstname, mobile, theTs from booking_modif WHERE  confirmation = '$booking->confirmation' order by theTs DESC LIMIT 1");
  
		$data_object->prev_bkrdate = date('F  j,Y', strtotime($prev_booking['rdate']));
		$data_object->prev_bkrtime =$prev_booking['rtime'];
		//echo   $data_object->prev_rdateres;
		//        $data_object->prev_bkrtime =preg_replace("/:00/", "h", $prev_booking['rtime']);
		//        $data_object->prev_bkrdate = date("F j, Y, g:i a", mktime(substr($prev_booking['rtime'], 0, 2), substr($prev_booking['rtime'], 3, 4), 0, intval(substr($prev_booking['rdate'], 5, 2)), intval(substr($prev_booking['rdate'], 8, 2)), intval(substr($prev_booking['rdate'], 0, 4))));
        $data_object->prev_bkcover = $prev_booking['cover'];
        $data_object->prev_bkspecialrequest = $prev_booking['specialrequest'];
        $data_object->prev_bklastname = $prev_booking['lastname'];
        $data_object->prev_bkfirstname = $prev_booking['firstname'];
        $data_object->prev_bkmobile = $prev_booking['mobile'];
        $data_object->prev_bookingtheTs = $prev_booking['theTs'];      

//          get previous booking information
//          $prev_booking = pdo_single_select("SELECT rdate, rtime, cover, specialrequest, lastname, firstname, mobile, theTs from booking_modif WHERE  confirmation = '$booking->confirmation' order by theTs DESC LIMIT 1");
//        $data_object->prev_booking['rdate'] = $prev_booking['rdate'];
//        $data_object->prev_booking['rtime'] = $prev_booking['rtime'];
//        $data_object->prev_booking['cover'] = $prev_booking['cover'];
//        $data_object->prev_booking['specialrequest'] = $prev_booking['specialrequest'];
//        $data_object->prev_booking['lastname'] = $prev_booking['lastname'];
//        $data_object->prev_booking['firstname'] = $prev_booking['firstname'];
//        $data_object->prev_booking['mobile'] = $prev_booking['mobile'];
//        $data_object->prev_booking['theTs'] = $prev_booking['theTs'];
		

            
            
        //$recipient = "philippe.benedetti@weeloy.com";
        $subject = "Update Reservation at " . $booking->restaurantinfo->title . " on " . $booking->rdate;

        $data_object->baseemail = $booking->base64url_encode($booking->email);

        $data_object->baseconfirmation = $booking->base64url_encode($booking->confirmation);
        $data_object->baserdate = $booking->base64url_encode($booking->rdate);
        //getting booking id purpose
        $data = pdo_single_select("SELECT id,restaurant from booking WHERE  confirmation = '$booking->confirmation' LIMIT 1");
        if (count($data) > 0) {
            $data_object->basebookid = $booking->base64url_encode($data['id']);
        }
        $data_object->shareLink = urlencode($data_object->siteUrl . "/modules/booking/bookingdetails.php/?b=" . $data_object->basebookid . "&rd=" . $data_object->baseconfirmation);
        $res = new WY_restaurant;
        $res->getRestaurant($booking->restaurant);
        $rescountry = $res->country;
        $data_object->rescountry = $rescountry;
        $data_object->restTel = $booking->restaurantinfo->tel;
        $data_object->ispromo = false;
        if (!$booking->restaurantinfo->is_wheelable) {
            $data = $res->getActivePromotion($booking->restaurant, $res->affiliate_program);
            if (count($data) > 0) {
                $data_object->offer = $data['offer_cpp'];
                $data_object->offer_description = $data['description_cpp'];
                $data_object->ispromo = true;
            }
        }
        /*         * ************************************* */
        //Email->restaurant terms &condition
        /*         * ************************************* */
        $data_object->restc = "";
        $res->lastorderdiner = "";
        $res->lastorderlunch = "";
        if (preg_match("/walkin|CALLCENTER|tms|waiting|WEBSITE|facebook|remote/", $booking->tracking) == true) {
            $is_white_label = true;
            $header['white_label'] = true;
            $header['replyto'] = array($booking->restaurantinfo->email);
        }
        if (isset($res->bookinfo)) {
            $llorder = ($res->lastorderlunch != "12:00") ? $res->lastorderlunch : "";
            $ldorder = ($res->lastorderdiner != "18:00") ? $res->lastorderdiner : "";
            $showinfo = $res->bookinfo;

            if ($showinfo != "") {
                if ($llorder !== "")
                    $llorder = "Last order for lunch is " . $llorder;
                $data_object->llorder = $llorder;
                if ($ldorder !== "")
                    $ldorder = "Last order for diner is " . $ldorder;
                $data_object->ldorder = $ldorder;
            }

            $data_object->restc = $showinfo;
        }

        /* End************************************************** */

        if (preg_match("/walkin|CALLCENTER|tms|waiting|WEBSITE|facebook|remote/", $booking->tracking) == true) {
            // WHITE LABEL 
            // Booking form on restaurant website

            $body = $mailer->getTemplate('callcenter/notifycallcenter_update_member', $data_object);
            $sms_message = "Booking update: " . $booking->restaurantinfo->title . "\n" . $data_object->date_sms . "\n" . "Confirmation: " . $booking->confirmation . ", \n" . str_replace(" ", "", $booking->restaurantinfo->tel) . ' ' . $booking->restaurantinfo->address . " " . $booking->restaurantinfo->zip . ", \n" . $booking->restaurantinfo->city;
        } else if (preg_match("/walkin|CALLCENTER|tms|waiting|WEBSITE|facebook|remote/", $booking->tracking) == true) {

            // WHITE LABEL 
            // Booking form on callcenter
            $body = $mailer->getTemplate('callcenter/notify_callcenter', $data_object);
            $sms_message = "Booking update: " . $booking->restaurantinfo->title . "\n" . $data_object->date_sms . "\n" . "Confirmation: " . $booking->confirmation . ", \n" . str_replace(" ", "", $booking->restaurantinfo->tel) . ' ' . $booking->restaurantinfo->address . " " . $booking->restaurantinfo->zip . ", \n" . $booking->restaurantinfo->city;
        } else if (!$booking->restaurantinfo->is_bookable && $booking->restaurantinfo->is_wheelable) {
            // --> in weeloy.com 
            //   KALA
            //  UPDATE  notifyrequest_member and   notifylisting_member
            // Request weeloy.com
            $body = $mailer->getTemplate('booking/notifyrequest_update', $data_object);
            //$body = $mailer->getTemplate('newnotifyrequest_member', $data_object);
            //SMS UPDATE KALA
//            if ($res->country == 'Singapore') {
//                $sms_message = "Booking update: " . $booking->restaurantinfo->title . "\n" . $data_object->date_sms . "\n" . "Reference: " . $booking->confirmation . "\nWeeloy Code: " . $booking->membCode . ",\n Win 3d2n to Phuket! Details: http://weeloy.co/win3d2n\n" . str_replace(" ", "", $booking->restaurantinfo->tel);
//            } else {
//                $sms_message = "Booking update: " . $booking->restaurantinfo->title . "\n" . $data_object->date_sms . "\n" . "Reference: " . $booking->confirmation . "\nWeeloy Code: " . $booking->membCode . ",\n" . str_replace(" ", "", $booking->restaurantinfo->tel) . "\n" . $booking->restaurantinfo->address . " " . $booking->restaurantinfo->zip . ", \n" . $booking->restaurantinfo->city;
//            } 
            $sms_message = "Booking update: " . $booking->restaurantinfo->title . "\n" . $data_object->date_sms . "\n" . "Reference: " . $booking->confirmation . "\nWeeloy Code: " . $booking->membCode . ",\n" . str_replace(" ", "", $booking->restaurantinfo->tel) . "\n" . $booking->restaurantinfo->address . " " . $booking->restaurantinfo->zip . ", \n" . $booking->restaurantinfo->city;
            $apn_message = "Booking update: " . $data_object->date_sms . ", " . $booking->restaurantinfo->title . ", " . $booking->cover . "pp";
        } else {
            if (!$is_white_label) {
                //$pushToken="b1fec0f5e1b8fc551a511030d73b6832fbf8a7995bee56c9dddc249e4154a6df";
                $_SESSION['push']['confirmation'] = $booking->confirmation;
                $pushToken = $booking->pushToken;
                $apn->sendIospassNotify($pushToken);
            }
            // Booking weeloy.com
            if ($booking->restaurantinfo->is_wheelable) {

                $body = $mailer->getTemplate('booking/notifybooking_update', $data_object);
                //$body = $mailer->getTemplate('notifybooking_member', $data_object);
//                if ($res->country == 'Singapore') {
//                    //SMS UPDATE KALA
//                    $sms_message = "Booking update: " . $booking->restaurantinfo->title . "\n" . $data_object->date_sms . "\n" . "Confirmation: " . $booking->confirmation . "\nWeeloy Code: " . $booking->membCode . ",\n Win 3d2n to Phuket! Details: http://weeloy.co/win3d2n\n" . str_replace(" ", "", $booking->restaurantinfo->tel);
//                } else {
//                    // 
//                    $sms_message = "Booking update: " . $booking->restaurantinfo->title . "\n" . $data_object->date_sms . "\n" . "Confirmation: " . $booking->confirmation . "\nWeeloy Code: " . $booking->membCode . ",\n" . str_replace(" ", "", $booking->restaurantinfo->tel) . "\n" . $booking->restaurantinfo->address . " " . $booking->restaurantinfo->zip . ", \n" . $booking->restaurantinfo->city;
//                }
                $sms_message = "Booking update: " . $booking->restaurantinfo->title . "\n" . $data_object->date_sms . "\n" . "Confirmation: " . $booking->confirmation . "\nWeeloy Code: " . $booking->membCode . ",\n" . str_replace(" ", "", $booking->restaurantinfo->tel) . "\n" . $booking->restaurantinfo->address . " " . $booking->restaurantinfo->zip . ", \n" . $booking->restaurantinfo->city;
                $apn_message = "Booking update: " . $data_object->date_sms . ", " . $booking->restaurantinfo->title . ", " . $booking->cover . "pp";
            } else {
                // Listing only
                $body = $mailer->getTemplate('booking/notifylisting_update', $data_object);

//                if ($res->country == 'Singapore') {
//
//                    $sms_message = "Booking update: " . $booking->restaurantinfo->title . "\n" . $data_object->date_sms . "\n" . "Confirmation: " . $booking->confirmation . ",\n Win 3d2n to Phuket! Details: http://weeloy.co/win3d2n\n" . str_replace(" ", "", $booking->restaurantinfo->tel);
//                } else {
//
//                    $sms_message = "Booking update: " . $booking->restaurantinfo->title . "\n" . $data_object->date_sms . "\n" . "Confirmation: " . $booking->confirmation . ",\n" . str_replace(" ", "", $booking->restaurantinfo->tel) . "\n" . $booking->restaurantinfo->address . " " . $booking->restaurantinfo->zip . ", \n" . $booking->restaurantinfo->city;
//                }
                $sms_message = "Booking update: " . $booking->restaurantinfo->title . "\n" . $data_object->date_sms . "\n" . "Confirmation: " . $booking->confirmation . ",\n" . str_replace(" ", "", $booking->restaurantinfo->tel) . "\n" . $booking->restaurantinfo->address . " " . $booking->restaurantinfo->zip . ", \n" . $booking->restaurantinfo->city;
                $apn_message = "Booking update: " . $data_object->date_sms . ", " . $booking->restaurantinfo->title . ", " . $booking->cover . "pp";
                //SMS UPDATE KALA
            }
        }
        if (!empty($booking->email) && $booking->email != "no@email.com")
            $mailer->sendmail($recipient, $subject, $body, $header, $booking->restaurant, $booking->confirmation);
        unset($_SESSION['push']['confirmation']);


//        if(!empty($recipient_mobile)){
//            $sms->sendSmsMessage($recipient_mobile, $sms_message, '');
//        }



        $recipient = $booking->restaurantinfo->email;
        // $recipient = "support@weeloy.com";
        $subject = "Reservation at " . $booking->restaurantinfo->title . " on " . $booking->rdate;
        if (preg_match("/walkin|CALLCENTER|tms|waiting|WEBSITE|facebook|remote/", $booking->tracking) == true) {
            $body = $mailer->getTemplate('callcenter/notifycallcenter_update_restaurant', $data_object);
        } else if (!$booking->restaurantinfo->is_bookable && $booking->restaurantinfo->is_wheelable) {
            $body = $mailer->getTemplate('booking/notifyrequest_restaurant', $data_object);
        } else {
            if ($booking->restaurantinfo->is_wheelable) {
                $body = $mailer->getTemplate('booking/notifybooking_update_restaurant', $data_object);
            } else {
                $body = $mailer->getTemplate('booking/notifylisting_update_restaurant', $data_object);
            }
        }
        if ($is_white_label === true) {
            $header['replyto'] = array($booking->email);
        }
        $mailer->sendmail($recipient, $subject, $body, $header, $booking->restaurant, $booking->confirmation);

        $notif_message = "Update booking: " . $data_object->date_sms . " " . $booking->cover . 'pp - ' . $booking->restaurantinfo->title;
    }

    function createAction($data) {
        if (isset($data)) {
            $level=$data['level'];
             $level=array_unique($level);
            $level = implode('||',$level);
            if ($data['mode'] === 'update') {
                $Sql = "UPDATE notification_type SET type = '{$data['type']}',action = '{$data['category']}',label = '{$data['label']}',is_active= '$level',status = '{$data['status']}' where  id='{$data['id']}' limit 1";
                pdo_exec($Sql);
            } else {
                pdo_exec("INSERT INTO notification_type (type, action, label, is_active,status) VALUES ('{$data['type']}','{$data['category']}', '{$data['label']}','$level','{$data['status']}')");
            }

            return 1;
        }
        return 0;
    }

    function getNotificationType() {
        
        $sql = "SELECT * from notification_type where status='active' order by ID ASC";
        $data = pdo_multiple_select($sql);
   
        return $data;
    }

    function updateAction($data) {
  
        if (isset($data)) {
            $sql = "SELECT * from notification_action where restaurant='{$data['restaurant']}' and action='{$data['label']}' limit 1";
            $getData = pdo_single_select($sql);

            if (count($getData) > 0) {
                $Sql = "UPDATE notification_action SET email = '{$data['email']}',sms = '{$data['sms']}',push_notification ='{$data['pushnotification']}',sms_premium = '{$data['smspremium']}'  where restaurant='{$data['restaurant']}' and action='{$data['label']}' limit 1";
                pdo_exec($Sql);
            } else {
                pdo_exec("INSERT INTO notification_action (restaurant, action, email, sms,push_notification,sms_premium) VALUES ('{$data['restaurant']}','{$data['label']}', '{$data['email']}', '{$data['sms']}','{$data['pushnotification']}','{$data['smspremium']}')");
            }
            return 1;
        } else {
            return 0;
        }
    }

    function getNotificationAction($theRestaurant) {
        $sql = "SELECT * from notification_action where restaurant='$theRestaurant'";
        $getData = pdo_multiple_select($sql);
       
        return $getData;
    }

    function getNotificationDateFormat($rdate, $rtime) {
        date_default_timezone_set('UTC');

        $date_sms = date("j/m/Y H:i", mktime(substr($rtime, 0, 2), substr($rtime, 3, 4), 0, intval(substr($rdate, 5, 2)), intval(substr($rdate, 8, 2)), intval(substr($rdate, 0, 4))));
        $date_gcalender = date("j-m-Y H:i", mktime(substr($rtime, 0, 2), substr($rtime, 3, 4), 0, intval(substr($rdate, 5, 2)), intval(substr($rdate, 8, 2)), intval(substr($rdate, 0, 4))));
        $bkdate = date("F j, Y, g:i a", mktime(substr($rtime, 0, 2), substr($rtime, 3, 4), 0, intval(substr($rdate, 5, 2)), intval(substr($rdate, 8, 2)), intval(substr($rdate, 0, 4))));
        // Notification Member

        $date_sms = date("j/m/Y H:i", mktime(substr($rtime, 0, 2), substr($rtime, 3, 4), 0, intval(substr($rdate, 5, 2)), intval(substr($rdate, 8, 2)), intval(substr($rdate, 0, 4))));
        //$rdate = date("F j, Y, g:i a", mktime(substr($rtime, 0, 2), substr($rtime, 3, 4), 0, intval(substr($rdate, 5, 2)), intval(substr($rdate, 8, 2)), intval(substr($rdate, 0, 4))));
        $rdateres = date("l F j, Y, g:i a", mktime(substr($rtime, 0, 2), substr($rtime, 3, 4), 0, intval(substr($rdate, 5, 2)), intval(substr($rdate, 8, 2)), intval(substr($rdate, 0, 4))));
        $rdate_date_only = date("F j, Y", mktime(substr($rtime, 0, 2), substr($rtime, 3, 4), 0, intval(substr($rdate, 5, 2)), intval(substr($rdate, 8, 2)), intval(substr($rdate, 0, 4))));
        $rdate_time_only = date("H:i", mktime(substr($rtime, 0, 2), substr($rtime, 3, 4), 0, intval(substr($rdate, 5, 2)), intval(substr($rdate, 8, 2)), intval(substr($rdate, 0, 4))));
        $rtime = preg_replace("/:00/", "h", $rtime);
        //$date_gcalender = date("j-m-Y H:i", mktime(substr($rtime, 0, 2), substr($rtime, 3, 4), 0, intval(substr($rdate, 5, 2)), intval(substr($rdate, 8, 2)), intval(substr($rdate, 0, 4))));
        $dataObj = array('date_sms' => $date_sms, 'rdate' => $bkdate, 'rtime' => $rtime, 'rdateres' => $rdateres, 'rdate_date_only' => $rdate_date_only, 'rdate_time_only' => $rdate_time_only, 'date_gcalender' => $date_gcalender);

        return $dataObj;
    }

    //depreciated
    function notifyHeader($from, $to, $type = 'member') {
        $hdFrom = array($from => 'Weeloy.com');
        $hdreplyto = array($to);
        return array('from' => $hdFrom, 'replyto' => $hdreplyto);
    }

    function checkisWhitelabel($tracking) {
        $is_white_label = false;
        if (preg_match("/walkin|CALLCENTER|tms|waiting|WEBSITE|facebook/i", $tracking) == true) {
            $is_white_label = true;
        }
        return $is_white_label;
    }

    function checkisRemote($tracking) {
        $is_remote = false;
        if (preg_match("/remote/i", $tracking) == true) {
            $is_remote = true;
        }
        return $is_remote;
    }

    function getNotificationObject($booking) {
        $dateObj = $this->getNotificationDateFormat($booking->rdate, $booking->rtime);
        $data_object = $booking;
        //booiking date & time user readable formate
        $data_object->rtime = $dateObj['rtime'];
        $data_object->rdate = $dateObj['rdate'];
        $data_object->rdateres = $dateObj['rdateres'];

        // google calender date & time formate
        date_default_timezone_set('UTC');

        $date_gcalender = strtotime($dateObj['date_gcalender']);

        $data_object->bkstartdate = gmdate("Ymd\THis\Z", $date_gcalender);


        $data_object->rdate_time_only = $dateObj['rdate_time_only'];
        $data_object->rdate_date_only = $dateObj['rdate_date_only'];
        $data_object->siteUrl = __SERVERNAME__ . __ROOTDIR__;

        $data_object->date_sms = $dateObj['date_sms'];

        $recipient = $booking->email;
        $recipient_mobile = $booking->mobile;
        $tracking = $booking->tracking;

        //check the booking is whitelabel or not 
        $is_white_label = $this->checkisWhitelabel($tracking);
        $data_object->whiteLabel = $is_white_label;
       

        $data_object->baseemail = $booking->base64url_encode($recipient);
        $data_object->baseconfirmation = $booking->base64url_encode($booking->confirmation);
        $data_object->baserdate = $booking->base64url_encode($dateObj['rdate']);
        $data_object->basebookid = $booking->base64url_encode($booking->bookid);

        //fb share link
        $data_object->shareLink = urlencode($data_object->siteUrl . "/modules/booking/bookingdetails.php/?b=" . $data_object->basebookid . "&rd=" . $data_object->baseconfirmation);

        $data_object->rescountry = $booking->restaurantinfo->country;
        $data_object->restTel = $booking->restaurantinfo->tel;
        $data_object->ispromo = false;

        $res = new WY_restaurant;
        $res->getRestaurant($booking->restaurant);

        /* checking cpp promotion and cpp booking */
        //promotion 
        if (!$booking->restaurantinfo->is_wheelable && preg_match("/cpp_credit_suisse/i", $tracking) == true) {
            $data = $res->getActivePromotion($booking->restaurant, $res->affiliate_program);
            if (count($data) > 0) {
                $data_object->offer = $data['offer_cpp'];
                $data_object->offer_description = $data['description_cpp'];
                $data_object->ispromo = true;
            }
        } else if (preg_match("/cpp_credit_suisse/i", $tracking) == true) {
            $data_object->offermsgres = 'This is corporate booking. The corporate Wheel will be automatically selected in your Weeloy app.';
            $data_object->offermsgmem = 'Please bring your bussiness card to enjoy your corparate program Promotion';
            $data_object->iscpp = true;
        }

        //Email Translation
        $trad = new WY_Translation;
        $emailEle = $trad->getEmailContent('EMAIL', $booking->language);
       
        $data_object->labelContent = $emailEle;

        

        return $data_object;
    }

    /*     * ***********************started notification booking ******************************* */

    //Notify booking confirmation

    function notifyBookingKala($qrcode_guest = NULL, $booking) {

        if (preg_match("/remote/i", $booking->tracking) == true) {
            return;
        }
        if (preg_match("/CHOPE/i", $booking->booker) == true) {
            return;
        }

        if ($booking->status == 'pending_payment') {
            // the notification should be done on payment validation
            return;
        }

        $mailer = new JM_Mail;
        $sms = new JM_Sms();
        $apn = new JM_Pushnotif();

        $data_object = $this->getNotificationObject($booking);

        $res = new WY_restaurant;
        $res->getRestaurant($booking->restaurant);
        //email subject
        /*         * *****************************Email subject & header section **************************** */

        $subject = "Reservation at " . $booking->restaurantinfo->title . " on " . $data_object->rdate;
        $headerObj = $this->notifyHeader('reservation@weeloy.com', 'info@weeloy.com');
        if ($data_object->whiteLabel == true) {
            $headerObj = $this->notifyHeader('reservation@weeloy.com', $booking->restaurantinfo->email);
            $header['white_label'] = true;
        }
        $header['from'] = $headerObj['from'];
        $header['replyto'] = $headerObj['replyto'];
        $recipient = $booking->email;

        /*         * *****************************End header section **************************** */


        /*         * *****************************member section **************************** */
        // WHITE LABEL 

        if ($data_object->whiteLabel == true) {
            $body = $mailer->getTemplate('callcenter/notifycallcenter_member', $data_object);
            $sms_message = $sms->getSmsMessage('callcenter', $booking, $data_object->date_sms);
        } else if (!$booking->restaurantinfo->is_bookable && $booking->restaurantinfo->is_wheelable) {
            $body = $mailer->getTemplate('booking/notifyrequest_member', $data_object);
            $sms_message = $sms->getSmsMessage('request', $booking, $data_object->date_sms);
        } else {
            //PASSBOOK
            if ($data_object->whiteLabel == false) {
                $passbook_link = __BASE_URL__ . '/passbook/' . base64_encode($booking->confirmation . '|||' . $booking->email);
                $data_object->passbook = $passbook_link;
            }
            if ($booking->restaurantinfo->is_wheelable) {
                $body = $mailer->getTemplate('booking/notifybooking_member', $data_object);
                $sms_message = $sms->getSmsMessage('is_wheelable', $booking, $data_object->date_sms);
            } else {
                // Listing only
                $body = $mailer->getTemplate('booking/notifylisting_member', $data_object);
                $sms_message = $sms->getSmsMessage('listing', $booking, $data_object->date_sms);

                if ($this->restaurant == 'SG_SG_R_Bacchanalia') {
                    $sms_message = $sms->getSmsMessage('noconfirmation', $booking, $data_object->date_sms);
                }
            }
        }
        //send mail
        if(!empty($booking->email) && $booking->email != "no@email.com") {
            $mailer->sendmail($recipient, $subject, $body, $header, $booking->restaurant, $booking->confirmation);
        }
        //send sms
        $smsid = "Weeloy";
        if (preg_match("/walkin|CALLCENTER|tms|waiting|WEBSITE|facebook/i", $data_object->tracking) == true && !empty($data_object->restaurantinfo->smsid))
            $smsid = $data_object->restaurantinfo->smsid;
        $sms->sendSmsMessage($recipient_mobile, $sms_message, $smsid, $booking->restaurant, $booking->confirmation);
        unset($header['passbook']);

        //push notiifcation
        if (!$data_object->whiteLabel) {
            $notif_message = $apn->getpushNotifyMessage($booking->restaurantinfo->title, $booking->cover, $data_object->date_sms, 'bknew');
            $apn->sendnotifyMultiple($recipient, $booking->restaurant, $booking->confirmation, $notif_message, 'member');
        }

        /*         * *****************************End member section **************************** */

        /*         * ******************************* start restuarant section ***************** */


        $recipient = $booking->restaurantinfo->email;

        if ($data_object->whiteLabel === true) {
            $header['replyto'] = array($booking->email);
        }


        // $recipient = "support@weeloy.com";
        $subject = "New reservation: " . $data_object->rdate . ", " . $booking->salutation . " " . $booking->firstname . " " . $booking->lastname . " at " . $booking->restaurantinfo->title . " - " . $booking->confirmation;

        if ($data_object->whiteLabel === true) {

            $body = $mailer->getTemplate('callcenter/notifycallcenter_restaurant', $data_object);
        } else if (!$booking->restaurantinfo->is_bookable && $booking->restaurantinfo->is_wheelable) {

            $body = $mailer->getTemplate('booking/notifyrequest_restaurant', $data_object);
        } else {
            if ($booking->restaurantinfo->is_wheelable) {

                $body = $mailer->getTemplate('booking/notifybooking_restaurant', $data_object);
            } else {
                $body = $mailer->getTemplate('booking/notifylisting_restaurant', $data_object);
            }
        }
        $mailer->sendmail($recipient, $subject, $body, $header, $booking->restaurant, $booking->confirmation);

        //SMS to restaurant Manager
        if ($res->smsNotifToRestaurant()) {

            $recipient_mobile = $res->getManagerPhoneNumber();

            $sms_message = 'NEW BOOKING ' . $booking->restaurantinfo->title . "\n" . $data_object->date_sms . " " . $booking->cover . "pax\n" . "Confirmation: " . $booking->confirmation . "\nName " . $booking->firstname . ' ' . $booking->lastname . "\nPhone " . $booking->mobile;

            $sms->sendSmsMessage($recipient_mobile, $sms_message, $smsid, $booking->restaurant, $booking->confirmation);
        }


        $notif_message = $apn->getpushNotifyMessage($booking->restaurantinfo->title, $booking->cover, $data_object->date_sms, 'bknewres');
        $apn->sendnotifyMultiple($recipient, $booking->restaurant, $booking->confirmation, $notif_message, 'restaurant');

        /*         * ******************************* END  restuarant section ***************** */
    }

    /*     * ***********************started notification booking cancel******************************* */

    function notifyCancel($booking) {

        if ($this->checkisRemote($booking->tracking)) {
            return;
        }

        if (preg_match("/CHOPE/i", $booking->booker) == true) {
            return;
        }


        $res = new WY_restaurant;
        $res->getRestaurant($booking->restaurant);
        $data_object = $this->getNotificationObject($booking);
        
        $provider ='sms_premium';
        
        //MOVE TO THE getNotificationObject 
       // $data_object->guests = $data_object->cover;
        //$this->checkNotifyConfig('cancel_member',$booking->restaurant,$data_object); 
             
         

         $this->checkNotifyConfig('cancel_member',$booking->restaurant,$data_object); 
        /* **************************** EMAIL MEMBER **************************** */
      
        if( $this->restaurant_configuration['email']){ 
               
            $header = $this->getHeader('cancel_member', $booking, $data_object);
            $recipient = $booking->email;
            $subject = $this->getSubject('cancel_member', $booking, $data_object);
            $body = $this->getEmailBody('cancel_member', $booking, $data_object);

            $this->email_service->sendmail($recipient, $subject, $body, $header, $booking->restaurant, $booking->confirmation);
        }
        
        /* *****************************End EMAIL MEMBER **************************** */
        
        /* ***************************** SMS MEMBER **************************** */

            if($this->restaurant_configuration[$this->smsprovider]){ 
            $header_sms = $this->getSmsHeader('cancel_member', $booking, $data_object);
            $recipient_mobile = $booking->mobile;
            $sms_message = $this->getSmsBody('cancel_member', $booking, $data_object);
            $this->sms_service->sendSmsMessage($recipient_mobile, $sms_message, $header_sms['from'], $booking->restaurant, $booking->confirmation,$this->pname );
        
       }
        /* ***************************** end SMS MEMBER **************************** */
        
        /* ***************************** PUSH NOTIF MEMBER **************************** */
       if($this->restaurant_configuration['apns']){ 
            $apn_message = $this->getApnBody('cancel_member', $booking, $data_object);
        //$recipient-> booking email
        //the white label test must be moved
       
        //if (!$data_object->whiteLabel) {
            
            //$apn_message = $this->apn_service->getpushNotifyMessage($booking->restaurantinfo->title, $booking->cover, $data_object->date_sms, 'bkcancel');
            $this->apn_service->sendnotifyMultiple($recipient, $booking->restaurant, $booking->confirmation, $apn_message, 'member');
       }
        //}
        /* ***************************** end PUSH NOTIF MEMBER **************************** */
 
        
       
       
       $this->checkNotifyConfig('cancel_restaurant',$booking->restaurant,$data_object); 

        /* **************************** EMAIL RESTAURANT **************************** */
       if($this->restaurant_configuration['email']){ 
         
        $header = $this->getHeader('cancel_restaurant', $booking, $data_object);
        $recipient = $booking->restaurantinfo->email;
        $subject = $this->getSubject('cancel_restaurant', $booking, $data_object);
        $body = $this->getEmailBody('cancel_restaurant', $booking, $data_object);
        $this->email_service->sendmail($recipient, $subject, $body, $header, $booking->restaurant, $booking->confirmation);
        /* *****************************End EMAIL RESTAURANT **************************** */
       }
    
        if($this->restaurant_configuration[$this->smsprovider]){ 
           
            /* ***************************** SMS MEMBER **************************** */
             
            $header_sms = $this->getSmsHeader('cancel_restaurant', $booking, $data_object);
            //$recipient_mobile = $res->getManagerPhoneNumber();
     
            
            // NEED a template for this sms body
            $sms_message = $this->getSmsBody('cancel_restaurant', $booking, $data_object);
             //$this->sms_service->sendSmsMessage($recipient_mobile, $sms_message, $header_sms['from'], $booking->restaurant, $booking->confirmation,$this->pname);
            
            //send multiple sms 
            $this->sendSmsToRestaurant($res,$booking,$data_object,$sms_message,$header_sms['from'],$this->pname);
            
            /* ***************************** end SMS MEMBER **************************** */

        }
        if($this->restaurant_configuration['apns']){ 
            $apn_message = $this->getApnBody('cancel_restaurant', $booking, $data_object);
            //$notif_message = $this->apn_service->getpushNotifyMessage($booking->restaurantinfo->title, $booking->cover, $data_object->date_sms, 'bkcancel');
            $this->apn_service->sendnotifyMultiple($recipient, $booking->restaurant, $booking->confirmation, $apn_message, 'restaurant');
        }

        /*         * ************************************ end restuarant section ***************** */
    }

//    DO NOT DELETE
//    
//    function notifyCancel($booking) {
//
//        if ($this->checkisRemote($booking->tracking)){
//            return;
//        }   
//
//        if (preg_match("/CHOPE/i", $booking->booker) == true){
//            return;
//        }
//          
//        $body = '';
//        $mailer = new JM_Mail;
//        $sms = new JM_Sms();
//        $apn = new JM_Pushnotif();
//        
//        $res = new WY_restaurant;
//        $res->getRestaurant($booking->restaurant);
//        
//        
//
//        $is_white_label = $this->checkisWhitelabel($booking->tracking);
//       
//
//
//
//        $rtime = preg_replace("/:00/", "h", $booking->rtime);
//        $date_sms = date("j/m/Y H:i", mktime(substr($booking->rtime, 0, 2), substr($booking->rtime, 3, 4), 0, intval(substr($booking->rdate, 5, 2)), intval(substr($booking->rdate, 8, 2)), intval(substr($booking->rdate, 0, 4))));
//        $rdate = date("l F j, Y, g:i a", mktime(substr($booking->rtime, 0, 2), substr($booking->rtime, 3, 4), 0, intval(substr($booking->rdate, 5, 2)), intval(substr($booking->rdate, 8, 2)), intval(substr($booking->rdate, 0, 4))));
//
//        
//
//        $data_object = $booking;
//        $data_object->rtime = $booking->rtime;
//        $data_object->rdate = $rdate;
//        $data_object->guests = $booking->cover;
//
//        $recipient = $booking->email;
//        $emailEle = $booking->getEmailContent();
//
//        $data_object->labelContent = $emailEle;
//
//        $recipient_mobile = $booking->mobile;
//        $header['from'] = array('reservation@weeloy.com' => 'Weeloy.com');
//        $header['replyto'] = array('info@weeloy.com');
//
//        $subject = "Reservation Cancelled at " . $booking->restaurantinfo->title . " on " . $rdate;
//        $sms_message = 'Your booking at ' . $booking->restaurantinfo->title . "\n on the " . $date_sms . "\n has been cancelled";
//        if ($is_white_label) {
//            $header['white_label'] = true;
//            $body = $mailer->getTemplate('cancel/cancelbooking_member_white_label', $booking);
//
//            $header['replyto'] = array($booking->restaurantinfo->email);
//            $sms_message = '[NOREPLY] Your booking at ' . $booking->restaurantinfo->title . "\n on the " . $date_sms . "\n has been cancelled";
//            //$body = $mailer->getTemplate('newcancelbooking_member', $booking);
//        } else {
//            //$body = $mailer->getTemplate('cancelbooking_member_white_label', $booking);
//            $body = $mailer->getTemplate('cancel/notifycancelbooking_member', $booking);
//        }
//        $mailer->sendmail($recipient, $subject, $body, $header, $booking->restaurant, $booking->confirmation);
//
//        $smsid = "Weeloy";
//        if ($is_white_label && !empty($data_object->restaurantinfo->smsid)) {
//            $smsid = $data_object->restaurantinfo->smsid;
//        }
//        $sms->sendSmsMessage($recipient_mobile, $sms_message, $smsid, $booking->restaurant, $booking->confirmation);
//        // Notification Restaurant
//
//        if (!$is_white_label) {
//            $notif_message = "Your booking at : " . $booking->restaurantinfo->title . "has been cancelled";
//            //get all device related to this account
//            $sql = "SELECT md.mobile_id, md.user_id, md.mobile_type FROM mobile_devices md WHERE app_name = 'weeloy' AND md.status = 'active' AND  md.user_id = '$booking->email'";
//
//            $data = pdo_multiple_select($sql);
//            foreach ($data as $row) {
//                $recipient = $row['mobile_id'];
//                $pn_type = $row['mobile_type'];
//                $apn->sendPushnotifMessage($recipient, $notif_message, $pn_type, null, 'weeloy', $booking->restaurant, $booking->confirmation); // null for options ?
//            }
//        }
//
//        $recipient = $booking->restaurantinfo->email;
//
//        $subject = "Cancellation Reservation at " . $booking->restaurantinfo->title . " on " . $rdate;
//
//        if ($is_white_label) {
//            $header['white_label'] = true;
//            $header['from'] = array('reservation@weeloy.com' => 'Weeloy.com');
//            $header['replyto'] = array($booking->email);
//            $body = $mailer->getTemplate('cancel/cancelbooking_restaurant_white_label', $booking);
//            //$body = $mailer->getTemplate('newcancelbooking_member', $data_object);
//        } else {
//            $body = $mailer->getTemplate('cancel/cancelbooking_restaurant', $data_object);
//        }
//        $mailer->sendmail($recipient, $subject, $body, $header, $booking->restaurant, $booking->confirmation);
//
//        //SMS to restaurant Manager
//        if ($res->smsNotifToRestaurant()) {
//            $recipient_mobile = $res->getManagerPhoneNumber();
//            $sms_message = 'BOOKING CANCELLED ' . $booking->restaurantinfo->title . "\n" . $date_sms . " " . $booking->cover . "pax\n" . "Confirmation: " . $booking->confirmation . "\nName " . $booking->firstname . ' ' . $booking->lastname . "\nPhone " . $booking->mobile;
//            $sms->sendSmsMessage($recipient_mobile, $sms_message, $smsid, $booking->restaurant, $booking->confirmation);
//        }
//
//        $notif_message = "Your booking at : " . $booking->restaurantinfo->title . "has been cancelled";
//
//        //get all device related to this account
//        $sql = "SELECT md.mobile_id, md.user_id, md.mobile_type FROM restaurant_app_managers ram, mobile_devices md WHERE app_name = 'weeloy_pro' AND md.status = 'active' AND ram.user_id = md.user_id AND `restaurant_id` LIKE '" . $booking->restaurant . "'";
//        $data = pdo_multiple_select($sql);
//        foreach ($data as $row) {
//            $recipient = $row['mobile_id'];
//            $pn_type = $row['mobile_type'];
//            $apn->sendPushnotifMessage($recipient, $notif_message, $pn_type, null, 'weeloy_pro', $booking->restaurant, $booking->confirmation); // null for options ?
//        }
//    }

    public function getSubject($type, $booking, $data_object) {
        $subject = '';
        switch ($type) {
            case 'cancel_member':
                $subject = "Reservation Cancelled at " . $booking->restaurantinfo->title . " on " . $data_object->rdate;
                break;
            case 'cancel_restaurant':
                $subject = "Cancellation Reservation: " . $data_object->rdate . ", " . $booking->salutation . " " . $booking->firstname . " " . $booking->lastname;
            default:
                break;
        }
        return $subject;
    }

    public function getHeader($type, $booking, $data_object) {
        $hearder = array();
        
        if ($data_object->whiteLabel == true) {

            $header['white_label '] = true;
            switch ($type) {
                case 'cancel_member':
                    $hearder['from'] = array('reservation@weeloy.com' => 'Weeloy.com');
                    $hearder['replyto'] = array($booking->restaurantinfo->email => $booking->restaurantinfo->title);
                    break;
            case 'cancel_restaurant':
                    $hearder['from'] = array('reservation@weeloy.com' => 'Weeloy.com');
                    $hearder['replyto'] = array($booking->email);
                    break;
                default:
                    break;
            }
        } else {
            switch ($type) {
                case 'cancel_member':
                    $hearder['from'] = array('reservation@weeloy.com' => 'Weeloy.com');
                    $header['replyto'] = 'info@weeloy.com';
                    break;

                default:
                    break;
            }
        }
        return $hearder;
    }

        public function getSmsHeader($type, $booking, $data_object) {
            $header = array();
            $header['from'] = 'Weeloy';
        if ($data_object->whiteLabel) {
                   $header['white_label '] = true;
            switch ($type) {
                case 'cancel_member':   
                case 'cancel_restaurant':
                    if (!empty($data_object->restaurantinfo->smsid)) {
                        $header['from'] = $data_object->restaurantinfo->smsid;
                    }
                    break;

                default:
                    break;
            }
        } else {
         
            switch ($type) {
                case 'cancel_member':
                case 'cancel_restaurant':
                    $header['from'] = 'Weeloy';
                    break;

                default:
                    break;
            }
        }
       
        return $header;
    }
    

        
        
        
    function getEmailBody($type, $booking, $data_object) {
        if ($data_object->whiteLabel == true) {
            switch ($type) {
                case 'cancel_member':
                    $template_name = 'cancel/cancelbooking_member_white_label';
                    break;
                case 'cancel_restaurant':
                    $template_name = 'cancel/cancelbooking_restaurant_white_label';
                    break;
            }
        } else {
            switch ($type) {
                case 'cancel_member':
                    $template_name = 'cancel/notifycancelbooking_member';
                    break;
                case 'cancel_restaurant':
                    $template_name = 'cancel/cancelbooking_restaurant';
                    break;
            }
        }
        //$body = $mailer->getTemplate($template_name, $booking);
        //return $body;
        return $this->email_service->getTemplate($template_name, $booking);
    }

    function getSmsBody($type, $booking, $data_object) {
        if ($data_object->whiteLabel == true) {
            switch ($type) {
                case 'cancel_member':
                    $template_name = 'cancel-callcenter';
                    break;
                case 'cancel_restaurant':
                    $template_name = 'cancel_restaurant';
                    break;
            }
        } else {
            switch ($type) {
                case 'cancel_member':
                    $template_name = 'cancel';
                    break;
                case 'cancel_restaurant':
                    $template_name = 'cancel_restaurant';
                    break;
            }
        }
        return $this->sms_service->getSmsMessage($template_name, $booking, $data_object->date_sms);
    }
    
    public function getApnBody($type, $booking, $data_object){
         if ($data_object->whiteLabel == true) {
            switch ($type) {
                case 'cancel_member':  
                case 'cancel_restaurant':
                    $template_name = 'cancel_restaurant';
                    break;
            }
         }else{
            switch ($type) {
                case 'cancel_member':
                    $template_name = 'cancel';
                    break;
                case 'cancel_restaurant':
                    $template_name = 'cancel_restaurant';
                    break;
            }
             
         }
//         getpushNotifyMessage($booking->restaurantinfo->title, $booking->cover, $data_object->date_sms, 'bkcancel');
         return $this->apn_service->getpushNotifyMessage($template_name, $booking, $data_object->date_sms);
    }
    
    public function sendSmsToRestaurant($res, $booking,$data_object,$sms_message, $headersms, $pname){
        $restManager =$res->getRestMulticontact();
            for($i=0;$i<count($restManager);$i++){
                $recipient_mobile =$restManager[$i]['mobile'];
                if (!empty($recipient_mobile)) {
                    error_log($data_object->whiteLabel);
                     if ($data_object->whiteLabel == true){
                         $paid =1;
                     }else{ $paid =($i > 0) ? 1 : 0;}

                   $this->sms_service->sendSmsMessage($recipient_mobile, $sms_message, $headersms, $booking->restaurant, $booking->confirmation,$pname,$paid);
                }
            }
    }
    
    public function checkNotifyConfig($type,$restaurant,$data_object){
        if ($data_object->whiteLabel == true) {
            switch ($type) {
                case 'cancel_restaurant':
                    $template_name = 'booking-cancel-restaurant-whitelabel';
                    break;
                case 'cancel_member':
                    $template_name = 'booking-cancel-member-whitelabel';
                    break;
            }
          }else{
            switch ($type) {
                case 'cancel_member':
                    $template_name = 'booking-cancel-member';
                    break;
                case 'cancel_restaurant':
                    $template_name = 'booking-cancel-restaurant';
                    break;
            }
             
         }
         
         $this->getConfiguration($restaurant,$template_name);
        
         
        
    }

}
