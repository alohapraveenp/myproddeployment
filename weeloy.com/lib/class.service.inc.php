<?php


require_once("lib/class.member.inc.php");
class WY_Service {

   private $ID;
   private $categorie;
   private $service;
   private $extravalue;
   

   function __construct() {

   }

   
   public function getCategories(){
       $sql = "SELECT sc.ID as categorie_id, sc.categorie as categorie FROM res_service_categorie sc WHERE 1";
       return pdo_multiple_select($sql);
   }
   
   public function getServices(){
       $sql = "SELECT sd.ID as service_id, sd.service as service, sd.categorie_id as categorie_id FROM res_service_details sd WHERE 1";
       return pdo_multiple_select($sql);
   }
   
   public function getRestaurantServices($restaurant, $option = NULL){
       $where = '';
       if($option == 'active_only'){
           $where = ' WHERE restaurant_id IS NOT NULL '; 
       }
       $sql = "SELECT sd.ID, sd.service, sd.categorie_id, sd.pico_service as pico_service, sd.css_class_service as css_class_service, IF(restaurant_id IS NULL, FALSE, TRUE)as active, sc.categorie, sc.pico_url as pico_categorie, sc.css_class as css_class_categorie "
               . "FROM  restaurant_service rs "
               . "RIGHT JOIN res_service_details sd ON rs.service_id = sd.ID AND restaurant_id = '$restaurant' "
               . "LEFT JOIN  res_service_categorie sc ON sc.ID = sd.categorie_id $where ORDER BY categorie_id";
       
       return pdo_multiple_select($sql);
   }
   
   public function updateServices($restaurant, $active_services){

        $services = '(';
        $insert_values = '';
        foreach ($active_services as $active_service){
            $services .= $active_service.',';
            $insert_values .= "('$restaurant',$active_service)," ;
        }
        $insert_values = rtrim($insert_values, ",");
        $services = rtrim($services, ","). ')';
        $sql = "INSERT INTO restaurant_service (restaurant_id, service_id) VALUES $insert_values ON DUPLICATE KEY UPDATE restaurant_id = '$restaurant'";
        $res = pdo_exec($sql);
        
        if($res >= 0){
           //remove old           
            $sql = "DELETE FROM  restaurant_service WHERE restaurant_id = '$restaurant' AND service_id NOT IN $services";
            $res = pdo_exec($sql);
            return $res;
        }
       return false;
   }
   
   public function getRestaurantServiceId($query){
       $sql = "SELECT ID,categorie_id FROM `res_service_details` WHERE `service` LIKE '%$query%' ";
       return pdo_single_select($sql);
   }
   public function getServiceRes($query){
       $service = $this->getRestaurantServiceId($query);
       $resto = [];
       error_log("COUNT ".count($service));
       if(count($service)>0){
            $id = $service['ID'];
            $sql = "SELECT restaurant_id FROM `restaurant_service` WHERE `service_id` LIKE '%$id%' ";
            $data = pdo_multiple_select($sql);
            if(count($data)>0){
                foreach ($data as $row) {
                        $resto[] = "'" . $row['restaurant_id'] . "'";

                        }
            }
        }
      return $resto;

   }
   
   function creatextlabel($labelname,$query,$status){
       $sql = "INSERT INTO extract_queries (label,query,status) VALUES ('$labelname','$query','$status')";
       $res = pdo_exec($sql);
       return $res;
   }
   function getlabellist($email){
       $sql = "SELECT * FROM extract_queries WHERE status='active'";
       $data = pdo_multiple_select($sql);
        $labelArr=array();
     $member = new WY_Member($email);
       $userRole = $member->getUserRole($email);

        foreach($data as $lb){
            $emailArr = $member->getUserAclEmail($lb['permission']);
             $role=strtolower($lb['permission']);
             if(isset($role)){
                 $roles=explode(',',$role);
                 if(in_array(strtolower($userRole),$roles)){
                     $labelArr[]=$lb;  
                 }
             }

        }
        return $labelArr;
   }
  /********************Tag Creation*****************/
   
   
   function createtag($data){
       $tag=$data['tag'];
        $sql = "INSERT INTO tag_list (name,status) VALUES ('$tag','active')";
       $result = pdo_exec($sql);
       return $result;
   }
    function getTaglist(){
        $sql = "SELECT * FROM tag_list WHERE status='active'";
        $data = pdo_multiple_select($sql);
         $tmpArr=array();
         if(count($data)>0){
             foreach ($data as $row) {
                 if($row['name']!==""){
                    if(!in_array($row['name'],$tmpArr)){
                       array_push($tmpArr,$row['name']);
                    }
                 }
             }
         }
         return $tmpArr;

    }
}
?>
