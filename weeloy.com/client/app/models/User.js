function User(options) {
    this.email = '';
    this.user_id = '';
    this.gender = '';
    this.firstname = '';
    this.name = '';
    this.lastname = '';
    this.mobile = '';
    this.member_type = '';
    this.member_permission = '';
    this.member_type = '';
    this.token = '';
    this.info = '';
    this.prefix = '';
    this.lang = '';
    this.affiliate_program = '';

    BaseModel.call(this, options);
}

User.prototype = new BaseModel();

User.prototype.getFirstName = function() {
    return this.firstname;
};

User.prototype.getLastName = function() {
    var lastname;
    if (this.lastname !== '') {
        lastname = this.lastname;
    } else {
        lastname = this.name;
    }
    return lastname;
};

User.prototype.getEmail = function() {
    return this.email;
};

User.prototype.getToken = function() {
    return this.token;
};

User.prototype.getPrefix = function() {
    return this.prefix;
};

User.prototype.getMobile = function() {
    return this.mobile;
};

User.prototype.getMobileNumberFormatted = function() {
    if (this.prefix !== '' && this.mobile !== '') {
        return this.prefix + ' ' + this.mobile;
    } else {
        return '';
    }
};

User.prototype.getUserId = function() {
    return this.user_id;
};
