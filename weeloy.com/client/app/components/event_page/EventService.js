var EventService = angular.module('EventService', []);
EventService.service('Event', ['$http', '$q', function($http, $q) {
    this.getEventEvent = function() {
        var API_URL = 'api/event/all'; 
        var defferred = $q.defer();
        $http.get(API_URL).success(function(response) {
            defferred.resolve(response);
        });
        return defferred.promise;
    };
    
    this.getInternalPath = function(restaurant) {
    var restaurant_details = restaurant.split('_');
    var city;
    switch (restaurant_details[1]) {
        case 'SG':
            city = 'singapore';
            break;
        case 'HK':
            city = 'hong-kong';
            break;
        case 'BK':
            city = 'bangkok';
            break;
        case 'PK':
            city = 'phuket';
            break;
        case 'KL':
            city = 'kuala-lumpur';
            break;
        default:
            city = 'singapore';
            break;
    }
    var type = 'restaurant';
    var restaurant_name = restaurant.substr(8);
    restaurant_name = restaurant_name.replace(/_/g, '');
    restaurant_name = restaurant_name.replace(/([A-Z])/g, '-$1');
    restaurant_name = restaurant_name.replace(/[-]+/, '-');
    restaurant_name = restaurant_name.toLowerCase();
    if (restaurant_name.charAt(0) == '-') {
        restaurant_name = restaurant_name.substr(1);
    }
    

    var restaurant_url = type + '/' + city + '/' + restaurant_name;
    return restaurant_url;
};

}]);
