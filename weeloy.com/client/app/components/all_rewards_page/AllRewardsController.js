var AllRewardsController = angular.module('AllRewardsController', [
    'TitleAndMetaTag'
]);
AllRewardsController.controller('AllRewardsCtrl', [
    '$rootScope',
    '$scope',
    '$routeParams',
    'API',
    'TitleAndMeta',
    'AuditLog',
    function($rootScope, $scope, $routeParams, API, TitleAndMeta,AuditLog) {
        var restaurant = new Restaurant();
        var RestaurantID = restaurant.getRestaurantIdFromUrlParam($routeParams.city, $routeParams.restaurant);
         $rootScope.page = "dining_reward";
          
        

        //hide on webview mobile app
        $scope.mobileWebview = ($routeParams.dspl_h === 'f' && $routeParams.dspl_f === 'f' );
    
        API.restaurant.getFullInfo(RestaurantID)
            .then(function(result) {
           
                $scope.disableBookButtton = 'false';
                $scope.restaurant = result;
                AuditLog.logevent(50, result.ID); 
                var title = result.getTitle() + ' - Wheel Details';
                TitleAndMeta.setTitle(title);
                if (result.getStatus() != 'active') {
                    $scope.disableBookButtton = true;
                }
            });
        API.reward.getReward(RestaurantID)
            .then(function(response) {
                if (response.status == 1) {
                    $scope.rewards = response.data;
                }
            });
           
    }
]);
