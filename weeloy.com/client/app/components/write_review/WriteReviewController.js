
var WriteReviewController = angular.module('WriteReviewController', ['ui.bootstrap', 'MyBookingReview', 'AuditLog']);
WriteReviewController.controller('WriteReviewCtrl', ['$rootScope', '$scope', '$location', 'AuditLog', 'API', function ($rootScope, $scope, $location, AuditLog, API) {

        $rootScope.page = "my_review";

        $scope.showreviewpostList = false;
        $scope.reviewRating = [{'ext': '5'}, {'very': '4'}, {'good': '3'}, {'average': '2'}, {'bellow': '1'}];
        AuditLog.logevent(50, '');
        if ($location.$$search.f == 'review') {

            $scope.method = $location.$$search.rt;
            var rating = $location.$$search.rt;
            var curRating = rating.split("-");
             $scope.rating = curRating[1];
             $scope.grade = $scope.rating;

            $scope.confirmation = $location.$$search.bk;
            setTimeout(function () {
                API.myreview.getBooking($scope.confirmation).then(function (response) {

                    $scope.restaurant = response.data.resto;
                    $scope.rest_title = response.data.title;
                    $scope.bookingId = response.data.booking;
                    $scope.user_id = response.data.email;
                    $scope.reviewList = response.data;
                    $scope.showreviewpostList = true;
                    $scope.showreviewList = false;
                    $scope.bookid = response.data.bookid;
                    if (typeof response.data.review.review_status !== 'undefined' && response.data.review.review_status === 'posted') {
                        $("#review-container").css('display', 'none');
                        $("#msg-alert").css('display', 'block');
                        $(".alert").addClass('alert-info');
                        $(".alert").html(' The review for this booking has already been posted.');
                    } else {
                        if (response.data.reviewcount === 0) {
                            $scope.RatingNow('create');
                        }
                    }

                });

            }, 1000);

            $scope.RatingNow = function (type) {

                var data = {
                    confirmation_id: $scope.bookingId,
                    user_id: $scope.user_id,
                    restaurant_id: $scope.restaurant,
                    grade: $scope.grade,
                    comment: $scope.comment,
                    'type': type
                };


                API.myreview.reviewPostGrade(data).then(function (response) {
                    $scope.type = type;
                    AuditLog.logevent(105, $scope.bookid);

                    if (response.data.id && response.data.id !== 'undefined')
                        if ($rootScope.UserSession.user_id && typeof $rootScope.UserSession.user_id !== 'undefined') {
                            $scope.returnId = response.data.id;
                            window.location.href = "myreviews?id=" + $scope.returnId;
                       } else {
                            if(type === 'update'){
                                $("#review-container").css('display', 'none');
                                $("#msg-alert").css('display', 'block');
                                $(".alert").addClass('alert-success');
                                $(".alert").html(' You has been  successfully posted the review for this booking.');
                            }

                        }

//                window.location.reload();
                });

            };


        }
       $scope.rateFunction = function(rating) {
            $scope.grade = rating;
        };
        $scope.close_msg = function () {
            setTimeout(function () {
                $('#msg-alert').fadeOut('slow');
                $(".alert").html('');
                $('#msg-alert').removeClass('alert-info');
                window.location.href = "myreviews";
            }, 10000); // <-- time in milliseconds  
        };

    }]);

