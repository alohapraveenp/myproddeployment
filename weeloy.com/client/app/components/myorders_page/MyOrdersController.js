(function(app) {
    app.controller('MyOrderCtrl', controller);
    controller.$inject = [
        '$rootScope',
        '$scope',
        '$location',
        '$routeParams',
        'API',
        'AuditLog'
    ];

    function controller($rootScope, $scope, $location, $routeParams, API,AuditLog) {
        $rootScope.MenuUserSelected = 'myorders';
        var monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
        var weekday = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
        var TotalPage = 0;
        
        $rootScope.page ='my_order';
        AuditLog.logevent(50,''); 
        if ($location.$$search.id === undefined) {
            $scope.showListOrders = true;
            var page;
            if ($location.$$search.page !== undefined && $location.$$search.page > 0) {
                page = $location.$$search.page;
            } else {
                page = 1;
            }
            $scope.page = page;
            $scope.shownoData = false;
            API.myorder.getOrders(page).then(function(response) {
                if (response.status == 1) {
                    if (response.data.orders !== undefined && response.data.orders.length > 0) {

                        $scope.shownoData = true;
                        response.data.orders.forEach(function(value, key) {
                            var dateStr = value.delivery_date.split(' ');
                            var rdate = dateStr[0].split('-');
                            var d = new Date(parseInt(rdate[0]), parseInt(rdate[1] - 1), parseInt(rdate[2]));
                            response.data.orders[key].time = monthNames[d.getMonth()] + ' ' + d.getDate() + ', ' + d.getUTCFullYear() + ', ' + value.delivery_time;
                        });
                    }
                    TotalPage = Math.ceil(response.data.total_orders / config.OrderPerPage);
                    if (TotalPage > 1) {
                        $scope.showPagination = true;
                    }
                    var pages = [];
                    for (i = 1; i <= TotalPage; i++) {
                        pages.push(i);
                    }
                    $scope.pages = pages;
                    $scope.orders = response.data.orders;
                }
            });
        } else {
            $scope.showOrderDetail = true;
            API.myorder.getOrderDetail($location.$$search.id).then(function(response) {
      
                 AuditLog.logevent(50,$location.$$search.id); 
                response.data.forEach(function(value, key) {
                    
                    var dateStr = value.cdate.split(' ');
                    var rdate = dateStr[0].split('-');
                    var d = new Date(rdate[0], rdate[1], rdate[2]);
                    response.data[key].time = monthNames[d.getMonth() - 1] + ' ' + d.getDate() + ', ' + d.getUTCFullYear();
                });
                $scope.items = response.data;
            });
        }
    }

})(angular.module('MyOrdersController', []));
