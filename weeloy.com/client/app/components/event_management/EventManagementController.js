//$(function () {
//    $('#wtimepicker3').datetimepicker({
//        locale: 'en',
//        format: 'YYYY-MM-DD'
//    });
//    $('#wtimepicker4').datetimepicker({
//        locale: 'en',
//        format: 'HH:mm',
//        stepping: 15,
//        defaultDate: new Date().setHours(19)
//    });
//
//
//});

(function (app) {
    app.controller('EventManagementCtrl', EventManagementCtrl);
    EventManagementCtrl.$inject = ['$rootScope', '$scope', '$http', '$routeParams', 'API', '$timeout', '$location'];

    function EventManagementCtrl($rootScope, $scope, $http, $routeParams, API, $timeout, $location) {

        var script = document.createElement('script');
        script.src = '//checkout.stripe.com/checkout.js';
        document.body.appendChild(script);
        script.src = '//js.stripe.com/v2/';
        
        document.body.appendChild(script);

        var restaurant = new Restaurant();
        var RestaurantID = restaurant.getRestaurantIdFromUrlParam($routeParams.city, $routeParams.restaurant);

//        init EventObject
        var EventObject = new EventManagement();
        $scope.selectioncomplete = false;
        $scope.menus = [];
        $scope.items = [];
        $scope.counters = [];
        $scope.tncsaccepted = false;
        $scope.mysection = '';
        $scope.swiftcode ='';
        $scope.paytype ='manual';
        $scope.isRequiredcc = false;
        $scope.depositamount = 0;
        $scope.pre_totalamount = 0;
        $scope.payable_amount = 0;
        $scope.pre_depositamount = 0;
        $scope.amount = 0;
        $scope.multipledata = ['media', 'timing', 'additional', 'payment'];
        //$scope.payment_method = ['creditcard', 'banktransfer', 'manual','nodeposit'];
        $scope.payment_method = [{'creditcard' :false, 'banktransfer' :false, 'manual' :false,'nodeposit' :false}];
        $scope.multiplemodel = {
            media: {name: ''},
            payment: {name: ''},
            additional: {name: ''},
            timing: {name: '', funct: '', venue: '', garantee: '', expected: ''}
        };

        $scope.onlyNumbers = /^\d+$/;

        $scope.dietaries = [
            {'title': 'Vegetarian'},
            {'title': 'Vegetarian, no egg'},
            {'title': 'Gluten Free'},
            {'title': 'No raw food'},
            {'title': 'No Crustaceans'}
        ];

        $scope.totalamount = 0;
        $scope.minimumorder = 0;
//        $scope.categories = [{
//                id: '27977631',
//                name: 'starters',
//                limit: 3,
//                current: null,
//            }, {
//                id: '92113479',
//                name: 'mains',
//                limit: 2,
//                current: null,
//            }
//        ];

  
        
        $scope.EventObject = EventObject;
        API.restaurant.getFullInfo(RestaurantID)
                .then(function (result) {
                    $scope.restaurant = result;
                });

        if ($routeParams.event_id) {
            $scope.event_id = $routeParams.event_id;
            API.eventmanagement.getEventManagementProject($scope.event_id)
                    .then(function (result) {
                      
                        $scope.status = result.data.event.status;
                        var ev = result.data.event;
                        $scope.status = result.data.event.status;
                        $scope.amount = parseInt(result.data.event.total_amount);
                        var amount = parseInt(result.data.event.total_amount);
                            $scope.pre_totalamount = result.data.event.total_amount;
                            $scope.totalamount = result.data.event.amount;

                        $scope.multipledata.map(function (ll) {
                            if (!ev.more[ll] || ev.more[ll] instanceof Array === false)
                                ev.more[ll] = [];
                            else
                                ev.more[ll].map(function (pp) {
                                    pp.name = pp.name.replace(/\|\|\|/g, '\n');
                                });
                        });
                        $scope.EventObject = new EventManagement(ev);
                         var  evdate  = $scope.EventObject.rdate.split('-');
                            $scope.EventObject.rdate = evdate[2]+"-"+evdate[1]+"-"+evdate[0];
                        if ($scope.EventObject.more) {
                            $scope.EventObject.more = JSON.parse($scope.EventObject.more.replace(/’/g, "\""));
                            $scope.minimumorder = $scope.EventObject.more.minimumprice;
                            if($scope.EventObject.more.deposit != 'nodeposit'){
                                $scope.EventObject.more.deposit = parseInt($scope.EventObject.more.deposit);
                            }
                            
                        }
                        $scope.getEvModifyDetails();

                        
                        if($scope.EventObject.payment_details){
                             $scope.EventObject.payment_details = JSON.parse($scope.EventObject.payment_details.replace(/’/g, "\""));
                             $scope.depositamount = $scope.EventObject.payment_details.deposit_amount;
                             
//                             if($scope.depositamount > 0){
//                                 $scope.minimumorder = $scope.depositamount;
//                             }
                        }
                        if ($scope.EventObject.payment_method) {
                            $scope.EventObject.payment_method = JSON.parse($scope.EventObject.payment_method.replace(/’/g, "\""));
                              var evp = $scope.EventObject.payment_method;
                            if (evp.nodeposit && evp.nodeposit === true ) {
                                 $scope.isRequiredcc = false; 
                                 $scope.paytype = 'waived';
                            }
                            else if(evp.creditcard === true  && evp.nodeposit === false  ){
                                 $scope.isRequiredcc = true; 
                            }else if(evp.creditcard === true  && evp.banktransfer === true && evp.manual === true ){
                                   $scope.isRequiredcc = false; 
                            }else if(evp.creditcard === true  && evp.banktransfer === true){
                                   $scope.isRequiredcc = true; 
                            }
                            else if(evp.manual === true  && evp.banktransfer === true  ){
                                 $scope.isRequiredcc = false; 
 
                            }
                            else if(evp.creditcard === true  || evp.manual === true  ){
                                 $scope.isRequiredcc = false; 
                            }
                            
                            else{
                                    $scope.isRequiredcc = true; 
                            } 
                        }

                    });
        }
        

        if ($routeParams.event_id) {
            $scope.event_id = $routeParams.event_id;
            API.eventmanagement.getEventManagementMenuProposition($scope.event_id)
                    .then(function (result) {

                        $scope.menus = result.data.menu;

                        $scope.menus.forEach(function (item, index) {
                            var section = '';
                            item.items.forEach(function (item2, index2) {
                                if (item2.item_description === 'section_or' || item2.item_description === 'section') {
                                    section = item2.item_description;
                                } else {
                                    $scope.menus[index].items[index2].section = section;
                                    if (section === 'section_or' && (($scope.menus[index].items[index2 + 1] && $scope.menus[index].items[index2 + 1].item_description === 'section') || !$scope.menus[index].items[index2 + 1])) {
                                        $scope.menus[index].items[index2].section = 'section_or_nd';
                                    }
                                }
                                item2.vegetarian = (item2.extraflag & 8) == 8;
                            });
                            $scope.menus[index].food = (item.typeflag & 1) == 1;
                            $scope.menus[index].drink = (item.typeflag & 2) == 2;
                            $scope.menus[index].expend = (item.typeflag & 4) == 4;
                            $scope.menus[index].setmeal = (item.typeflag & 8) == 8;
                            $scope.menus[index].option = (item.typeflag & 16) == 16;
                        });
                        $scope.setQte();
           
                    });
              
                    
        }


        // make a test on status
        var API_URL = '../../api/payment/stripe/credentials/' + RestaurantID;
        $http.get(API_URL).then(function (response) {
          
            if (response.data.data !== 'undefined') {
                $scope.publishable_key = response.data.data.publishable_key;
            }
        });
        
         $scope.logIt = function (value) {
          $scope.swiftcode = value;
          $scope.paytype = value;
        };

        $scope.status = '';

        // Close Checkout on page navigation:
        $(window).on('popstate', function () {
            handler.close();
        });


        $scope.selectItem = function (item, evt) {
            if ($scope.items.indexOf(item) < 0) {
                $scope.items.push(item);

                if (typeof $scope.counters[item.displayID] === 'undefined') {
                    $scope.counters[item.displayID] = 1;
                } else {
                    $scope.counters[item.displayID] = $scope.counters[item.displayID] + 1;
                }

                $scope.checkContinue();
//                var selected_category = _.find($scope.categories, {id: item.menuID});
//                if(typeof selected_category.current === 'undefined'){
//                    selected_category.current = 1;
//                }else{
//                    selected_category.current = selected_category.current + 1;
//                }
            }
        };


        $scope.setQte = function (item, evt, type) {
            $scope.totalamount = 0;
            $scope.menus.forEach(function (menu, key) {
                var sum = 0;
                var first_item = true;
                menu.totalprice = 0;
                //menu.qte = 0;
                if (menu.items) {
                    menu.items.forEach(function (item, key) {
                        item.qte = parseInt(item.qte);
                        if(!item.qte){
                            item.qte = 0;
                        }
                        sum = sum + parseInt(item.qte);
                       
                        if (item.price) {
                            item.totalprice = parseInt(item.price) * parseInt(item.qte);
                            menu.totalprice = parseInt(menu.totalprice) + parseInt(item.totalprice);
                        }
                        if (first_item) {
                            menu.qte = sum;
                        }
                        if (item.item_description === 'section_or') {
                            if (menu.qte < sum) {
                                menu.qte = sum;
                                 console.log("MENU Q TE " + menu.qte );
                            }
                            sum = 0;
                            if (first_item) {
                                first_item = false;
                            }
                        }
                    });
                }
                if (menu.qte < sum) {
                    menu.qte = sum;
                }
                if (menu.setmeal) {
                    menu.totalprice = menu.price * menu.qte;
                }
                if (menu.totalprice) {
                   $scope.totalamount = parseInt(menu.totalprice) + parseInt($scope.totalamount);
                }
            });

            if ($scope.items.indexOf(item) < 0) {
                $scope.items.push(item);
            }

            $scope.items = $scope.menus;

//            if ($scope.items.indexOf(item) < 0) {
//                $scope.items.push(item);
//            }

            //console.log($scope.items);   

        };


  
        $scope.checkContinue = function () {

            $scope.selectioncomplete = true;
            $scope.menus.forEach(function (menu, key) {

                if ($scope.selectioncomplete && ($scope.counters[menu.menuID] !== parseInt(menu.item_limit))) {
                    $scope.selectioncomplete = false;
                }
            });
        };

        $scope.SaveEventRequest = function (EventForm, EventObject) {
            
            var eventManagerObject = new EventManagement(EventObject);
            eventManagerObject.setRestaurant(RestaurantID);
            
            if (eventManagerObject.getNumberPeople > $scope.balPax) {
                alert("This event is currently available for " + $scope.balPax + " people only");
                return false;
            }
             
            if (EventForm.$invalid) {
                var top;
                if (EventForm.firstname.$invalid || EventForm.lastname.$invalid || EventForm.email.$invalid || EventForm.phone.$invalid) {
                    top = $('input[name="firstname"]').offset().top;
                    $(window).scrollTop(top - 200);
                }
                return false;
            }
            $scope.formObject = EventForm;
   
   
            API.eventmanagement.createEventManagementProject(eventManagerObject)
                    .then(function (result) {
                        $scope.status = 'request_sent';
                        eventManagerObject.setStatus('request_sent');
                        //$scope.restaurant = result;
                        $(window).scrollTop();
                    });

        };

        $scope.saveEventMenu = function (status, EventObject) {
            var event_id = '';
            var selected_items = [];
             var amount = $scope.totalamount,service_charge = 0,gst = 0 ;     
             if($scope.totalamount !== 0){
                 amount = $scope.totalamount * 1.10 * 1.07 ;
                 $scope.amount = amount;
                 service_charge = $scope.totalamount * 0.10;
                 gst = $scope.totalamount * 0.07 ;
             }  
             if($scope.pre_totalamount > 0 ){
                 if(parseInt($scope.pre_totalamount) >= parseInt(amount)) { 
                     $scope.isRequiredcc = false;
                     $scope.EventObject.payment_method.creditcard = false;
                     $scope.EventObject.payment_method.banktransfer = false;

                 }else{
                     $scope.payable_amount =  parseInt(amount) - parseInt($scope.pre_totalamount);
                 }
                    
             }
             
            $scope.items.forEach(function (value, key) {
                event_id = value.eventID;
                var data = { "id": value.menuItemID, "qte": value.qte };
                selected_items.push(data);
                    
            });
          
            API.eventmanagement.saveMenuSelection(event_id, selected_items, status)
                    .then(function (result) {
                   
                    });
            API.eventmanagement.updatePrice(RestaurantID,$scope.EventObject.eventID,$scope.EventObject.email,$scope.totalamount,gst,service_charge,amount,'menu_selected').then(function (result) {});
            $scope.getEvModifyDetails();
            $scope.status = 'menu_selected';
             
             
        };
        $scope.notifySaveMenuSelection = function () {
            $scope.EventObject.notify_type = 'event_menu_selected';
            
            API.eventmanagement.notifysaveMenu($scope.EventObject)
                    .then(function (result) {

                    });
        };

        //payment creation
        $scope.submitCreditCardDetails = function () {
            //$scope.setQte();
            var amount = $scope.totalamount * 1.10 * 1.07 ,
                mode = $("input:radio[name=optionsPaymentRadios]:checked").val();
            
            if($scope.payable_amount > 0){
                amount = $scope.payable_amount;
            }
            if($scope.EventObject.more.deposit != 'nodeposit'){
                amount = amount * ($scope.EventObject.more.deposit / 100);
            }
            amount = Math.round(amount);
          
            if(mode === 'creditcard'){
                var handler = StripeCheckout.configure({
                    key: $scope.publishable_key, //'pk_test_aG4RNDkBJI81YsT2ljQGVnuW',
                    token: function (token) {
                        var data = $scope.EventObject;
                        data.stripeToken = token.id;
                        data.restaurant_id = RestaurantID;
                        data.event_id = data.eventID;
                        data.amount = amount;
                        data.curency = 'SGD';
                        data.action = 'private_event';
                        data.controller = 'restaurant';
                        data.returnUrl = BASE_URL + '/' + $scope.restaurant.getInternalPath() + '/event-management/menu-selection/';
                        var form = document.createElement('form');
                        form.action = 'modules/payment/stripe/stripe_event_payment_form.php';
                        form.method = 'POST';
                        $.each(data, function (key, value) {
                            var element = document.createElement("Input");
                            element.setAttribute("type", "hidden");
                            element.setAttribute("name", key);
                            element.setAttribute("value", value);
                            var div = document.createElement("div");
                            form.appendChild(element);
                        });
                        document.body.appendChild(form);
                        form.submit();
                        // Use the token to create the charge with a server-side script.
                        // You can access the token ID with `token.id`
                    }
                });
                handler.open({
                    email: $scope.EventObject.email,
                    panelLabel: 'Submit'
                });
            } else {
                 
                API.eventmanagement.savepaymentdetails(RestaurantID, $scope.EventObject.eventID, 'pending_payment',$scope.paytype,amount,'','').then(function (result) {
                    $scope.status = 'pending_payment';
                });
                $scope.notifySaveMenuSelection(); 
            }
        };
        
        $scope.getEvModifyDetails = function(){
                           
            //var amount = $scope.totalamount * 1.10 * 1.07 ;

//            if($scope.pre_totalamount > 0 ){
//                if($scope.pre_totalamount > parseInt($scope.amount)) { 
//                    $scope.isRequiredcc = false;
//                    $scope.EventObject.payment_method.creditcard = false;
//                    $scope.EventObject.payment_method.banktransfer = false;
//                }
//           }

             API.eventmanagement.modifyDetails($scope.event_id)
                    .then(function (result) {
                    if(result.status === 1){
                        if(result.data.details.total_amount > 0){
                            $scope.pre_totalamount = result.data.details.total_amount;
                            $scope.depositamount = result.data.details.total_deposit;
                            $scope.pre_depositamount = result.data.details.total_deposit;
                            $scope.payable_amount =  parseInt($scope.amount) - parseInt(result.data.details.last_total);
                            if(result.data.details.last_total > parseInt($scope.amount) ){
                                $scope.isRequiredcc = false;
                                $scope.EventObject.payment_method.creditcard = false;
                                $scope.EventObject.payment_method.banktransfer = false; 
                            }
                                
                         }
                    }
                      
                    
             });
            
        };
       
       
       if ($('#sidebar-right') && $('#sidebar-right').offset()) {
            
            // make right side bar affix
            var sidebar = $('#sidebar-right');
            var sidebar_affix_top = -330;
            sidebar_offset = sidebar.offset();
            sidebar.css({
                position: 'fixed',
                top:  sidebar_offset.top + 'px',
                left: sidebar_offset.left + 'px',
                'max-width': '500px',
              
            });



            $(window).resize(function () {
                sidebar.css({
                    position: 'relative',
                    left: '0px',
                });
                sidebar.css({
                    left: sidebar.offset().left + 'px',
                    position: 'fixed',
                });
            });
            $(window).scroll(function () {
                if ($(window).scrollTop() + sidebar.height() + sidebar_affix_top > $('.event-booking').height()) {
                    sidebar.css({
                        top: ($('.event-booking').height() - $(window).scrollTop() - sidebar.height()) + 'px',
                    });
                } else {
                    if ($(window).scrollTop() + sidebar_affix_top > sidebar_offset.top) {
                      
                        sidebar.css({
                            position: 'fixed',
                            top: sidebar_affix_top + 'px',
                        });
                    } else {
                        sidebar.css({
                            position: 'fixed',
                            top: (sidebar_offset.top - $(window).scrollTop()) + 'px'
                        });
                    }
                }

            });
        }
    }

app.directive('jqdatepicker', function () {
    return {
        restrict: 'A',
        require: 'ngModel',
         link: function (scope, element, attrs) {
              var options = {
                        locale: 'en',
                        format: 'DD-MM-YYYY',
                        minDate: new Date()
                    };
                      var DateTimePicker = $(element).datetimepicker(options);
                     
                      DateTimePicker.on('dp.change', function (e) {
                         var DateSelected = moment(e.date._d).format('DD-MM-YYYY');
                         scope.EventObject.rdate = DateSelected;

                    });
        }
    };
});

app.directive('jtimedatepicker', function () {
    return {
        restrict: 'A',
        require: 'ngModel',
         link: function (scope, element, attrs) {
              var options = {
                        locale: 'en',
                        format: 'HH:mm',
                        stepping: 15,
                        defaultDate: new Date().setHours(19)
                    };
                      var DateTimePicker = $(element).datetimepicker(options);
                     
                      DateTimePicker.on('dp.change', function (e) {
                         var DateSelected = moment(e.date._d).format('HH:mm');
                         scope.EventObject.rtime = DateSelected;
                       
                    });
        }
    };
});

  
})(angular.module('EventManagementController', [
    'app.shared.filters.timeFormat', 'ShoppingCartEvent'
]));