var ContestController = angular.module('ContestController', ['TitleAndMetaTag']);
ContestController.controller('ContestCtrl', [
    '$rootScope', 
    '$scope',
    '$location',
    'TitleAndMeta',
    'deviceDetector',
    'AuditLog', function($rootScope, $scope, $location ,TitleAndMeta, deviceDetector, AuditLog) {
        $rootScope.page='contest-page';
        AuditLog.logevent(50, ''); 
        $scope.book_button_text = 'Book Now';
        $scope.isMobile = deviceDetector.isMobile();
        
        var title = 'Dine and Win contest 2016 - book your restaurant with Weeloy Win $50 vouchers';
        var description = 'Win $50 dining vouchers when you book a restaurant on Weeloy from 10 September – 10 November, 2016. Double your chance when you book via Weeloy mobile app!';
        TitleAndMeta.setTitle(title);
        TitleAndMeta.setMetaDescription(description);
        TitleAndMeta.setFaceBookMetaTitle(title);
        TitleAndMeta.setFaceBookMetaDescription(description);
        
        
        $scope.go = function ( path ) {
            $location.path( path );
        };


}]); 
