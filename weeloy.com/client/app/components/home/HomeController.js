var HomeController = angular.module('HomeController', ['NfSearch', 'HomeService', 'RestaurantItem', 'RestaurantCategoryItem', 'RestaurantArticleItem', 'RestaurantEventItem', 'RestaurantTagItem', 'HomePageVideo', 'ng.deviceDetector', "com.2fdevs.videogular", "com.2fdevs.videogular.plugins.overlayplay"]);
HomeController.controller('HomeCtrl', [
    '$rootScope',
    '$scope',
    '$location',
    'Home',
    '$sce',
    '$window',
    'deviceDetector',
    'AuditLog',
    function($rootScope, $scope, $location, Home, $sce, $window, deviceDetector,AuditLog) {
        $scope.randomcss = Math.round(Math.random());
        $scope.isMobile = deviceDetector.isMobile();
        $scope.screenWidth = $(window).width();
        $rootScope.headertitle ='THE BEST RESTAURANTS IN';
       
        $rootScope.page ='home';
           AuditLog.logevent(50,'');
        $(window).resize(function() {
            $scope.screenWidth = $(window).width();
            $scope.$apply();
        });
        $scope.cities = [{
            id: 1,
            name: 'Singapore',
            data: 'singapore',
        }, {
            id: 2,
            name: 'Bangkok',
            data: 'bangkok',
        }, {
            id: 3,
            name: 'Phuket',
            data: 'phuket',
        }, {
            id: 4,
            name: 'Hong Kong',
            data: 'hong-kong',
        }];


 console.log("EARCH CITY " + $rootScope.UserSession.search_city);
 console.log("CITY " + $scope.city);
 console.log(" HOST " + $location.host());
        //    $scope.config2 = {
        //                preload: "none",
        //                autoplay:"false",
        //                nativecontrols: "true",
        //                sources: [
        //                    {src: $sce.trustAsResourceUrl("client/assets/videos/banner_local.m4v"), type: "video/mp4"},
        //                    {src: $sce.trustAsResourceUrl("http://static.videogular.com/assets/videos/videogular.webm"), type: "video/webm"},
        //                    {src: $sce.trustAsResourceUrl("http://static.videogular.com/assets/videos/videogular.ogg"), type: "video/ogg"}
        //                ],
        //                theme: {
        //                    url: "http://www.videogular.com/styles/themes/default/latest/videogular.css"
        //                }
        //            };
//        var videos = [{
//                        'video': 'https://static2.weeloy.com/videos/banner/pluck/high/pluck.m4v'
//                    }, {
//                         'video': 'images/Sticky_pudding.mp4'
//                    }, {
//                        'video': 'images/grilled_swordfish.mp4'
//                    }];
//           alert(videos.video);
//                videos = videos[Math.floor(Math.random() * videos.length)];
//                document.getElementById("videoplayer").src = videos.video;
////           
////                document.getElementById("videoplayer").load();

        $scope.isShowTag = false;

        if ($location.host() === 'dev.weeloy.com' || $location.host() === 'localhost' ) {
            $scope.isShowTag = true;
        }

        Home.getToprestaurants().then(function(response) {
           
            $scope.TopRestaurants = response;
        }).catch(function(error) {

        });

        //blog articles   

        Home.getTopBlogArticles().then(function(response) {
            if(response && response.data){
                response.data.forEach(function(value, key) {
                    response.data[key].post_title = $sce.trustAsHtml(response.data[key].post_title);
                    response.data[key].description = $sce.trustAsHtml(response.data[key].description);
                });

            $scope.blogarticles = response.data;
            }

        }).catch(function(error) {

        });


        Home.getHomebanners().then(function(response) {
            var tempArr = [];
            var CateItem = response.data.categories;
            //          tempArr =[{
            //              'id':'1',
            //                     'title': "Chinese New Year",
            //                     'tag': "cny",
            //                     'description':'Celebrate Lunar New Year or the Year of Monkey',
            //                     'link': "https://www.weeloy.com/chinese-new-year-2016-restaurant-menu",
            //                     'images':[{'restaurant':'SG_SG_R_SilkRoad','image': 'CNYYushengSilk_Road.jpg'}],
            //           }];
            if (response.data.categories.length > 0) {
                response.data.categories.forEach(function(value, key) {
                    var obj = {
                        'id': response.data.categories[key].id,
                        'title': response.data.categories[key].title,
                        'tag': response.data.categories[key].tag,
                        'description': response.data.categories[key].description,
                        'link': response.data.categories[key].link,
                        'images': response.data.categories[key].images
                    };
                    tempArr.push(obj);
                });
            }
            $scope.Categories = tempArr;

        }).catch(function(error) {

        });

        Home.getHomeEvent().then(function(response) {
            $scope.events = response.data.event;
        }).catch(function(error) {

        });

        Home.getDynamicTag().then(function(response) {
            var tags = response.data.categories;
            var tempArr = [];
            if (response.data.categories.length > 0) {
                response.data.categories.forEach(function(value, key) {
                    var obj = {
                        'id': response.data.categories[key].id,
                        'title': response.data.categories[key].title,
                        'tag': response.data.categories[key].tag,
                        'link': response.data.categories[key].link,
                        'images': response.data.categories[key].images
                    };
                    tempArr.push(obj);
                });
            }

            $scope.tagCategories = tempArr;
        }).catch(function(error) {

        });



    }
]);
