var AllRewardsCPPService = angular.module('AllRewardsCPPService', []);
AllRewardsCPPService.service('RewardCPP', ['$http', '$q', function($http, $q) {
    this.getCPPReward = function(query) {
        var API_URL = 'api/cppwheeldescription/' + query + '/unique_only';
        var defferred = $q.defer();
        $http.get(API_URL, {
            cache: true
        }).success(function(response) {
            defferred.resolve(response);
        });
        
        return defferred.promise;
    };
}]);
