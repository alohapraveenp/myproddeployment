var MyAccountController = angular.module('MyAccountController', ['LoginService', 'AuditLog', 'ngFileUpload']);
MyAccountController.controller('MyAccountCtrl', ['$rootScope', '$scope', '$route', 'API', 'loginService', 'AuditLog', 'Upload', 'Notification', function($rootScope, $scope, $route, API, loginService, AuditLog, Upload, Notification, $timeout) {
    $rootScope.checkLoggedin();
    $rootScope.MenuUserSelected = 'myaccount';
    $rootScope.showAccountForm = 'myAccountForm';
    $rootScope.page = 'my_account';
    $scope.isProgress =false;

    var salutations = [{
            id: '1',
            name: 'Mr.'
        }, {
            id: '2',
            name: 'Mrs.'
        }, {
            id: '3',
            name: 'Ms.'
        }
    ];

    $scope.salutations = salutations;
    $scope.close_msg = function() {
        setTimeout(function() {
            $('#msg-alert').fadeOut('slow');
            $(".alert").html('');
            $('#msg-alert').removeClass('alert-success');
            $('#msg-alert').removeClass('alert-danger');
            $(".alert").css('display', 'none');

        }, 10000); // <-- time in milliseconds  
    }

    $scope.UpdateUser = {
        email: $rootScope.email,
        firstname: $rootScope.user.getFirstName(),
        lastname: $rootScope.user.getLastName(),
        mobile: $rootScope.user.getMobileNumberFormatted(),
        updated: {},
    }

    API.account.getUserAccount().then(function(response) {
        //AuditLog.logevent(50,'');

console.log(response);

        $scope.UpdateUser.email = response.email;
        $scope.UpdateUser.firstname = response.firstname;
        $scope.UpdateUser.lastname = response.name;
        $scope.UpdateUser.mobile = response.mobile;
        $scope.UpdateUser.salutation = response.salutation;
        $scope.UpdateUser.profilePic = response.profilePic;
            $scope.UpdateUser.membercode = response.membercode;
        if ($scope.UpdateUser.salutation != null) {
            angular.forEach($scope.salutations, function(value, index) {
                if (value.name.toLowerCase() == $scope.UpdateUser.salutation.toLowerCase()) {
                    $scope.UpdateUser.salutations = value;
                }
            });
        }
        AuditLog.logevent(50, '');


    });

    if ($scope.UpdateUser.salutation != null) {

        angular.forEach($scope.salutations, function(value, index) {
            if (value.name.toLowerCase() == $scope.UpdateUser.salutation.toLowerCase()) {
                console.log(value);
                //$scope.UpdateUser.salutation =value;
            }
        });
    }
    //        var OldUserData = new Object();
    //        angular.copy($scope.UpdateUser, OldUserData);
    //        $scope.UpdateAccount = function (user) {
    //            console.log(user);
    //            if (user.firstname != OldUserData.firstname) {
    //                update('firstname', user.firstname);
    //            }
    //            ;
    //            if (user.lastname != OldUserData.lastname) {
    //                update('name', user.lastname);
    //            }
    //            ;
    //           
    //            if (user.salutations != undefined) {
    //                if (OldUserData.salutation == undefined || user.salutations.name != OldUserData.salutation.name) {
    //                    update('salutation', user.salutations.name);
    //                    if(user.salutations.name.toLowerCase()=='mr' || user.salutations.name.toLowerCase()=='mrs' ){
    //                        update('gender', 'Male');
    //                    }
    //                    if(user.salutations.name.toLowerCase()=='ms' || user.salutations.name.toLowerCase()=='miss' ){
    //                        update('gender', 'Female');
    //                    }
    //                    
    //                }
    //                ;
    //            }
    //            ;
    //            if (user.mobile != OldUserData.mobile) {
    //                update('mobile', user.mobile);
    //            }
    //            ;
    //        };
    //        $scope.setFiles = function (element) {
    //
    //            $scope.$apply(function ($scope) {
    //                console.log('files:', element.files);
    //                $scope.file = element.files;
    //            });
    //        }
    //kala started code here

    //         $scope.onFileSelect = function (element) {
    //             console.log("hi");
    //              console.log(JSON.stringify(element.file));
    //                $scope.uploadProgress = 0;
    //                $scope.selectedFile = element.file;
    //            };
    
    $scope.reset= function(){
        $scope.showAccountForm='changePasswordForm';
    }
    $scope.resetForm= function(){
        $scope.showAccountForm='myAccountForm';
    }
    $scope.listSalutation = function(tt) {
        console.log("TRTR"+tt);
        for (i = 0; i < $scope.salutations.length; i++) {
            if ($scope.salutations[i].name === tt) {
                $scope.UpdateUser.salutation = $scope.salutations[i].name;
                break;
            }
        }


    };
 
    $scope.uploadProfile =function(element){

        var file =element.files[0];
        $scope.isProgress = true;

        file.upload = Upload.upload({
          url: "api/user/profilepicture/upload",
          file: file,
        }).then(function(response) {
            if (response.status > 0) {
                $scope.isProgress = false;
                var msg = 'You have successfully updated your details.';
                Notification.show('error', msg);
                console.log(response.data.data);
                if(response.data.data!==null){
               console.log("profilepicture12" +response.data.data );
                 $scope.UpdateUser.profilePic = response.data.data;
                       console.log("profilepicture" +$scope.UpdateUser.profilePic );
               }
            }else{
               $scope.isProgress = false;
               Notification.show('error', 'Oops!! Sorry Unexpected Error');
            }

      });
     
        
    };
    $scope.uploadPic = function(file, user) {
    

        var error = false,
            msg;
        var str = user.salutation;
//        user.salutation = str.replace('.', '');


        if (typeof user.salutation !== 'undefined') {
            if (user.salutation.toLowerCase() === 'mr.') {
                user.gender = 'Male';
            }
            if (user.salutation.toLowerCase() === 'ms.' || user.salutation.toLowerCase() === 'mrs.') {
                user.gender = 'Female';
            }
        }
        if (user.firstname === '' || typeof user.firstname === 'undefined') {
            error = true;
            msg = 'Please Enter Your Firstname';

        }
        if (user.lastname === '' || typeof user.lastname === 'undefined') {
            error = true;
            msg = 'Please Enter Your Lastname';

        }
        if (user.mobile === '' || typeof user.mobile === 'undefined') {
            error = true;
            msg = 'Please Enter Your mobile number';
        }

        if (user.gender === '' || typeof user.gender === 'undefined') {
            error = true;
            msg = 'Please Select Your Salutation ';


        }
            if(user.membercode==='' || typeof user.membercode===0){
                 error =true;
                 msg= 'Please Enter your membercode ';
                
               
            }
            if(error===true){
                 Notification.show('warning', msg);
            return false;
        }

        if (file) {
            $rootScope.page = 'MyAccount_uploadphoto';
            $scope.isProgress = true;
            file.upload = Upload.upload({
                url: "api/user/profile/update",
                data: {
                    email: user.email,
                    gender: user.gender,
                    firstname: user.firstname,
                    lastname: user.lastname,
                    mobile: user.mobile,
                    membercode: user.membercode,
                    salutation: user.salutation
                    
                },
                file: file,
            }).then(function(response) {

                AuditLog.logevent(30, '');
                if (response.status > 0) {
                    $scope.isProgress = false;
                    var msg = 'You have successfully updated your details.';
                    Notification.show('error', msg);
                    $scope.UpdateUser.email = response.data.data.email;
                    $scope.UpdateUser.firstname = response.data.data.firstname;
                    $scope.UpdateUser.lastname = response.data.data.name;
                    $scope.UpdateUser.mobile = response.data.data.mobile;
                    $scope.UpdateUser.salutation = response.data.data.salutation;
                    $scope.UpdateUser.profilePic = response.data.data.profilePic;
                     $scope.UpdateUser.membercode = response.data.data.membercode;

                } else {
                    $scope.isProgress = false;

                    Notification.show('error', 'Oops!! Sorry Unexpected Error');
                    //                                    $(".alert").css('display','block');
                    //                             $(".alert").addClass('alert-danger');
                    //                                 $(".alert").html('Oops!! Sorry Unexpected Error');
                    //                                $scope.close_msg();
                }

            });
        } else {
            $rootScope.page = 'my_account_userinfo';
            AuditLog.logevent(30, '');
            $scope.UpdateUserAccount(user);
        }

    }
    $scope.UpdateUserAccount = function(user) {
        user.file = $scope.picFile;
        API.account.updateUserProfile(user).then(function(response) {
            if (response) {
                Notification.show('success', 'You have successfully updated your details.');
            } else {
                Notification.show('error', 'Oops!! Sorry Unexpected Error');
            }
            $scope.UpdateUser.email = response.data.data.email;
            $scope.UpdateUser.firstname = response.data.data.firstname;
            $scope.UpdateUser.lastname = response.data.data.name;
            $scope.UpdateUser.mobile = response.data.data.mobile;
            $scope.UpdateUser.salutation = response.data.data.salutation;
            $scope.UpdateUser.profilePic = response.data.data.profilePic;

        });
    };


    //        function update(label, value) {
    //                      
    //            loginService.update(label, value).then(function (response) {
    //                 if (response.status == 1) {
    //                    // alert(response.errors);
    //                    if (label == 'firstname') {
    //                        $rootScope.user.data.firstname = $scope.UpdateUser.firstname;
    //                        $scope.UpdateUser.updated.firstname = true;
    //                    }
    //                    ;
    //                    if (label == 'name') {
    //                        $rootScope.user.data.lastname = user.lastname;
    //                        $scope.UpdateUser.updated.lastname = true;
    //                    }
    //                    ;
    ////                    if (label == 'gender') {
    ////                        $rootScope.user.data.gender = $scope.UpdateUser.gender.name.toLowerCase();
    ////                        $scope.UpdateUser.updated.gender = true;
    ////                    }
    ////                    ;
    //                    if (label == 'salutation') {
    //                      
    //                        $rootScope.user.data.salutation = $scope.UpdateUser.salutations.name.toLowerCase();
    //                        $scope.UpdateUser.updated.salutations = true;
    //                    }
    //                    ;
    //                    if (label == 'mobile') {
    //                        var mobile_value = value.split(' ');
    //                        $rootScope.user.data.prefix = mobile_value[0];
    //                        $rootScope.user.data.mobile = mobile_value[1];
    //                         $scope.UpdateUser.updated.mobile = true;
    //                    }
    //                    ;
    //                }
    //                ;
    //            });
    //        }
    //        ;

    //Phone Index

    $scope.country = 'Singapore'
    $scope.currentcc = 10;
        $scope.countries = [{'a': 'Australia', 'b': 'au', 'c': '+61'}, {'a': 'China', 'b': 'cn', 'c': '+86'}, {'a': 'Hong Kong', 'b': 'hk', 'c': '+852'}, {'a': 'India', 'b': 'in', 'c': '+91'}, {'a': 'Indonesia', 'b': 'id', 'c': '+62'}, {'a': 'Japan', 'b': 'jp', 'c': '+81'}, {'a': 'Malaysia', 'b': 'my', 'c': '+60'}, {'a': 'Myanmar', 'b': 'mm', 'c': '+95'}, {'a': 'New Zealand', 'b': 'nz', 'c': '+64'}, {'a': 'Philippines', 'b': 'ph', 'c': '+63'}, {'a': 'Singapore', 'b': 'sg', 'c': '+65'}, {'a': 'South Korea', 'b': 'kr', 'c': '+82'}, {'a': 'Thailand', 'b': 'th', 'c': '+66'}, {'a': 'Vietnam', 'b': 'vn', 'c': '+84'}, {'a': '', 'b': '', 'c': ''}, {'a': 'Canada', 'b': 'ca', 'c': '+1'}, {'a': 'France', 'b': 'fr', 'c': '+33'}, {'a': 'Germany', 'b': 'de', 'c': '+49'}, {'a': 'Italy', 'b': 'it', 'c': '+39'}, {'a': 'Russia', 'b': 'ru', 'c': '+7'}, {'a': 'Spain', 'b': 'es', 'c': '+34'}, {'a': 'Sweden', 'b': 'se', 'c': '+46'}, {'a': 'Switzerland', 'b': 'ch', 'c': '+41'}, {'a': 'UnitedKingdom', 'b': 'gb', 'c': '+44'}, {'a': 'UnitedStates', 'b': 'us', 'c': '+1'}, {'a': '', 'b': '', 'c': ''}, {'a': 'Afghanistan', 'b': 'af', 'c': '+93'}, {'a': 'Albania', 'b': 'al', 'c': '+355'}, {'a': 'Algeria', 'b': 'dz', 'c': '+213'}, {'a': 'Andorra', 'b': 'ad', 'c': '+376'}, {'a': 'Angola', 'b': 'ao', 'c': '+244'}, {'a': 'Antarctica', 'b': 'aq', 'c': '+672'}, {'a': 'Argentina', 'b': 'ar', 'c': '+54'}, {'a': 'Armenia', 'b': 'am', 'c': '+374'}, {'a': 'Aruba', 'b': 'aw', 'c': '+297'}, {'a': 'Austria', 'b': 'at', 'c': '+43'}, {'a': 'Azerbaijan', 'b': 'az', 'c': '+994'}, {'a': 'Bahrain', 'b': 'bh', 'c': '+973'}, {'a': 'Bangladesh', 'b': 'bd', 'c': '+880'}, {'a': 'Belarus', 'b': 'by', 'c': '+375'}, {'a': 'Belgium', 'b': 'be', 'c': '+32'}, {'a': 'Belize', 'b': 'bz', 'c': '+501'}, {'a': 'Benin', 'b': 'bj', 'c': '+229'}, {'a': 'Bhutan', 'b': 'bt', 'c': '+975'}, {'a': 'Bolivia', 'b': 'bo', 'c': '+591'}, {'a': 'BosniaandHerzegovina', 'b': 'ba', 'c': '+387'}, {'a': 'Botswana', 'b': 'bw', 'c': '+267'}, {'a': 'Brazil', 'b': 'br', 'c': '+55'}, {'a': 'Brunei', 'b': 'bn', 'c': '+673'}, {'a': 'Bulgaria', 'b': 'bg', 'c': '+359'}, {'a': 'BurkinaFaso', 'b': 'bf', 'c': '+226'}, {'a': 'Burundi', 'b': 'bi', 'c': '+257'}, {'a': 'Cambodia', 'b': 'kh', 'c': '+855'}, {'a': 'Cameroon', 'b': 'cm', 'c': '+237'}, {'a': 'CapeVerde', 'b': 'cv', 'c': '+238'}, {'a': 'CentralAfricanRepublic', 'b': 'cf', 'c': '+236'}, {'a': 'Chad', 'b': 'td', 'c': '+235'}, {'a': 'Chile', 'b': 'cl', 'c': '+56'}, {'a': 'ChristmasIsland', 'b': 'cx', 'c': '+61'}, {'a': 'CocosIslands', 'b': 'cc', 'c': '+61'}, {'a': 'Colombia', 'b': 'co', 'c': '+57'}, {'a': 'Comoros', 'b': 'km', 'c': '+269'}, {'a': 'CookIslands', 'b': 'ck', 'c': '+682'}, {'a': 'CostaRica', 'b': 'cr', 'c': '+506'}, {'a': 'Croatia', 'b': 'hr', 'c': '+385'}, {'a': 'Cuba', 'b': 'cu', 'c': '+53'}, {'a': 'Curacao', 'b': 'cw', 'c': '+599'}, {'a': 'Cyprus', 'b': 'cy', 'c': '+357'}, {'a': 'CzechRepublic', 'b': 'cz', 'c': '+420'}, {'a': 'DemocraticRepCongo', 'b': 'cd', 'c': '+243'}, {'a': 'Denmark', 'b': 'dk', 'c': '+45'}, {'a': 'Djibouti', 'b': 'dj', 'c': '+253'}, {'a': 'EastTimor', 'b': 'tl', 'c': '+670'}, {'a': 'Ecuador', 'b': 'ec', 'c': '+593'}, {'a': 'Egypt', 'b': 'eg', 'c': '+20'}, {'a': 'ElSalvador', 'b': 'sv', 'c': '+503'}, {'a': 'EquatorialGuinea', 'b': 'gq', 'c': '+240'}, {'a': 'Eritrea', 'b': 'er', 'c': '+291'}, {'a': 'Estonia', 'b': 'ee', 'c': '+372'}, {'a': 'Ethiopia', 'b': 'et', 'c': '+251'}, {'a': 'FalklandIslands', 'b': 'fk', 'c': '+500'}, {'a': 'FaroeIslands', 'b': 'fo', 'c': '+298'}, {'a': 'Fiji', 'b': 'fj', 'c': '+679'}, {'a': 'Finland', 'b': 'fi', 'c': '+358'}, {'a': 'FrenchPolynesia', 'b': 'pf', 'c': '+689'}, {'a': 'Gabon', 'b': 'ga', 'c': '+241'}, {'a': 'Gambia', 'b': 'gm', 'c': '+220'}, {'a': 'Georgia', 'b': 'ge', 'c': '+995'}, {'a': 'Ghana', 'b': 'gh', 'c': '+233'}, {'a': 'Gibraltar', 'b': 'gi', 'c': '+350'}, {'a': 'Greece', 'b': 'gr', 'c': '+30'}, {'a': 'Greenland', 'b': 'gl', 'c': '+299'}, {'a': 'Guatemala', 'b': 'gt', 'c': '+502'}, {'a': 'Guernsey', 'b': 'gg', 'c': '+44-1481'}, {'a': 'Guinea', 'b': 'gn', 'c': '+224'}, {'a': 'Guinea-Bissau', 'b': 'gw', 'c': '+245'}, {'a': 'Guyana', 'b': 'gy', 'c': '+592'}, {'a': 'Haiti', 'b': 'ht', 'c': '+509'}, {'a': 'Honduras', 'b': 'hn', 'c': '+504'}, {'a': 'Hungary', 'b': 'hu', 'c': '+36'}, {'a': 'Iceland', 'b': 'is', 'c': '+354'}, {'a': 'Iran', 'b': 'ir', 'c': '+98'}, {'a': 'Iraq', 'b': 'iq', 'c': '+964'}, {'a': 'Ireland', 'b': 'ie', 'c': '+353'}, {'a': 'IsleofMan', 'b': 'im', 'c': '+44-1624'}, {'a': 'Israel', 'b': 'il', 'c': '+972'}, {'a': 'IvoryCoast', 'b': 'ci', 'c': '+225'}, {'a': 'Jersey', 'b': 'je', 'c': '+44-1534'}, {'a': 'Jordan', 'b': 'jo', 'c': '+962'}, {'a': 'Kazakhstan', 'b': 'kz', 'c': '+7'}, {'a': 'Kenya', 'b': 'ke', 'c': '+254'}, {'a': 'Kiribati', 'b': 'ki', 'c': '+686'}, {'a': 'Kosovo', 'b': 'xk', 'c': '+383'}, {'a': 'Kuwait', 'b': 'kw', 'c': '+965'}, {'a': 'Kyrgyzstan', 'b': 'kg', 'c': '+996'}, {'a': 'Laos', 'b': 'la', 'c': '+856'}, {'a': 'Latvia', 'b': 'lv', 'c': '+371'}, {'a': 'Lebanon', 'b': 'lb', 'c': '+961'}, {'a': 'Lesotho', 'b': 'ls', 'c': '+266'}, {'a': 'Liberia', 'b': 'lr', 'c': '+231'}, {'a': 'Libya', 'b': 'ly', 'c': '+218'}, {'a': 'Liechtenstein', 'b': 'li', 'c': '+423'}, {'a': 'Lithuania', 'b': 'lt', 'c': '+370'}, {'a': 'Luxembourg', 'b': 'lu', 'c': '+352'}, {'a': 'Macao', 'b': 'mo', 'c': '+853'}, {'a': 'Macedonia', 'b': 'mk', 'c': '+389'}, {'a': 'Madagascar', 'b': 'mg', 'c': '+261'}, {'a': 'Malawi', 'b': 'mw', 'c': '+265'}, {'a': 'Maldives', 'b': 'mv', 'c': '+960'}, {'a': 'Mali', 'b': 'ml', 'c': '+223'}, {'a': 'Malta', 'b': 'mt', 'c': '+356'}, {'a': 'MarshallIslands', 'b': 'mh', 'c': '+692'}, {'a': 'Mauritania', 'b': 'mr', 'c': '+222'}, {'a': 'Mauritius', 'b': 'mu', 'c': '+230'}, {'a': 'Mayotte', 'b': 'yt', 'c': '+262'}, {'a': 'Mexico', 'b': 'mx', 'c': '+52'}, {'a': 'Micronesia', 'b': 'fm', 'c': '+691'}, {'a': 'Moldova', 'b': 'md', 'c': '+373'}, {'a': 'Monaco', 'b': 'mc', 'c': '+377'}, {'a': 'Mongolia', 'b': 'mn', 'c': '+976'}, {'a': 'Montenegro', 'b': 'me', 'c': '+382'}, {'a': 'Morocco', 'b': 'ma', 'c': '+212'}, {'a': 'Mozambique', 'b': 'mz', 'c': '+258'}, {'a': 'Namibia', 'b': 'na', 'c': '+264'}, {'a': 'Nauru', 'b': 'nr', 'c': '+674'}, {'a': 'Nepal', 'b': 'np', 'c': '+977'}, {'a': 'Netherlands', 'b': 'nl', 'c': '+31'}, {'a': 'NetherlandsAntilles', 'b': 'an', 'c': '+599'}, {'a': 'NewCaledonia', 'b': 'nc', 'c': '+687'}, {'a': 'Nicaragua', 'b': 'ni', 'c': '+505'}, {'a': 'Niger', 'b': 'ne', 'c': '+227'}, {'a': 'Nigeria', 'b': 'ng', 'c': '+234'}, {'a': 'Niue', 'b': 'nu', 'c': '+683'}, {'a': 'NorthKorea', 'b': 'kp', 'c': '+850'}, {'a': 'Norway', 'b': 'no', 'c': '+47'}, {'a': 'Oman', 'b': 'om', 'c': '+968'}, {'a': 'Pakistan', 'b': 'pk', 'c': '+92'}, {'a': 'Palau', 'b': 'pw', 'c': '+680'}, {'a': 'Palestine', 'b': 'ps', 'c': '+970'}, {'a': 'Panama', 'b': 'pa', 'c': '+507'}, {'a': 'PapuaNewGuinea', 'b': 'pg', 'c': '+675'}, {'a': 'Paraguay', 'b': 'py', 'c': '+595'}, {'a': 'Peru', 'b': 'pe', 'c': '+51'}, {'a': 'Pitcairn', 'b': 'pn', 'c': '+64'}, {'a': 'Poland', 'b': 'pl', 'c': '+48'}, {'a': 'Portugal', 'b': 'pt', 'c': '+351'}, {'a': 'Qatar', 'b': 'qa', 'c': '+974'}, {'a': 'RepublicCongo', 'b': 'cg', 'c': '+242'}, {'a': 'Reunion', 'b': 're', 'c': '+262'}, {'a': 'Romania', 'b': 'ro', 'c': '+40'}, {'a': 'Rwanda', 'b': 'rw', 'c': '+250'}, {'a': 'SaintBarthelemy', 'b': 'bl', 'c': '+590'}, {'a': 'SaintHelena', 'b': 'sh', 'c': '+290'}, {'a': 'SaintMartin', 'b': 'mf', 'c': '+590'}, {'a': 'Samoa', 'b': 'ws', 'c': '+685'}, {'a': 'SanMarino', 'b': 'sm', 'c': '+378'}, {'a': 'SaudiArabia', 'b': 'sa', 'c': '+966'}, {'a': 'Senegal', 'b': 'sn', 'c': '+221'}, {'a': 'Serbia', 'b': 'rs', 'c': '+381'}, {'a': 'Seychelles', 'b': 'sc', 'c': '+248'}, {'a': 'SierraLeone', 'b': 'sl', 'c': '+232'}, {'a': 'Slovakia', 'b': 'sk', 'c': '+421'}, {'a': 'Slovenia', 'b': 'si', 'c': '+386'}, {'a': 'SolomonIslands', 'b': 'sb', 'c': '+677'}, {'a': 'Somalia', 'b': 'so', 'c': '+252'}, {'a': 'SouthAfrica', 'b': 'za', 'c': '+27'}, {'a': 'SouthSudan', 'b': 'ss', 'c': '+211'}, {'a': 'SriLanka', 'b': 'lk', 'c': '+94'}, {'a': 'Sudan', 'b': 'sd', 'c': '+249'}, {'a': 'Suriname', 'b': 'sr', 'c': '+597'}, {'a': 'Swaziland', 'b': 'sz', 'c': '+268'}, {'a': 'Syria', 'b': 'sy', 'c': '+963'}, {'a': 'Taiwan', 'b': 'tw', 'c': '+886'}, {'a': 'Tajikistan', 'b': 'tj', 'c': '+992'}, {'a': 'Tanzania', 'b': 'tz', 'c': '+255'}, {'a': 'Togo', 'b': 'tg', 'c': '+228'}, {'a': 'Tokelau', 'b': 'tk', 'c': '+690'}, {'a': 'Tonga', 'b': 'to', 'c': '+676'}, {'a': 'Tunisia', 'b': 'tn', 'c': '+216'}, {'a': 'Turkey', 'b': 'tr', 'c': '+90'}, {'a': 'Turkmenistan', 'b': 'tm', 'c': '+993'}, {'a': 'Tuvalu', 'b': 'tv', 'c': '+688'}, {'a': 'Uganda', 'b': 'ug', 'c': '+256'}, {'a': 'Ukraine', 'b': 'ua', 'c': '+380'}, {'a': 'UnitedArabEmirates', 'b': 'ae', 'c': '+971'}, {'a': 'Uruguay', 'b': 'uy', 'c': '+598'}, {'a': 'Uzbekistan', 'b': 'uz', 'c': '+998'}, {'a': 'Vanuatu', 'b': 'vu', 'c': '+678'}, {'a': 'Vatican', 'b': 'va', 'c': '+379'}, {'a': 'Venezuela', 'b': 've', 'c': '+58'}, {'a': 'WallisandFutuna', 'b': 'wf', 'c': '+681'}, {'a': 'WesternSahara', 'b': 'eh', 'c': '+212'}, {'a': 'Yemen', 'b': 'ye', 'c': '+967'}, {'a': 'Zambia', 'b': 'zm', 'c': '+260'}, {'a': 'Zimbabwe', 'b': 'zw', 'c': '+263'}];

    $scope.phoneindex = function(code) {

 console.log("CHECKVALIDTEL4"+$scope.UpdateUser.mobile);
        var val = $scope.UpdateUser.mobile;

        val = val.trim();
        val = val.replace(/[^0-9 \+]/g, "");
        val = val.replace(/[ ]+/g, " ");

        if (val.indexOf(code + ' ') == 0) {
            val = val.replace(/^\+(\d{2,3})[ ]*\1[ ]*/, '+$1 '); // replace repetition of contry code
            return $scope.UpdateUser.mobile =val.trim() ; //$("#txt-mobile").val();
        }

        if (val.indexOf(code) == 0) {

            val = code + ' ' + val.substring(code.length);
            val = val.replace(/^\+(\d{2,3})[ ]*\1[ ]*/, '+$1 '); // replace repetition of contry code
            //return $scope.mobile = $("#txt-mobile").val();
            return $scope.UpdateUser.mobile = val.trim() ; //$("#txt-mobile").val();
        }

        if ((res = val.match(/^\+\d{2,3} /)) != null) {
            val = val.replace(/^\+\d{2,3} /, "");

            val = code + ' ' + val;

            if (val.substring(1) === '+') {
                val = val.replace(/^\+(\d{2,3})[ ]*\1[ ]*/, '+$1 ');
            }
            // replace repetition of contry code

            $("#txt-mobile").val(val.trim());
            return $scope.UpdateUser.mobile = val.trim() ; //$("#txt-mobile").val();
            //return $scope.mobile = val.trim();
        }

        if ((res = val.match(/^\+\d{2,3}$/)) != null) {
            val = val.replace(/^\+\d{2,3}/, "");
            val = code + ' ' + val;
            val = val.replace(/^\+(\d{2,3})[ ]*\1[ ]*/, '+$1 '); // replace repetition of contry code
            $("#txt-mobile").val(val.trim());
            return $scope.UpdateUser.mobile = val.trim() ; //$("#txt-mobile").val();
        }

        if (val.match(/^\+/) != null) {
            val = code + ' ' + val.substring(2);

            //                val = val.replace(/^\+(\d{2,3})[ ]*\1[ ]*/, '+$1 '); // replace repetition of contry code

            $("#txt-mobile").val(val.trim());
            return $scope.UpdateUser.mobile = val.trim() ;
        }

        val = val.replace(/\+ /, "");

        val = code + ' ' + val;
        val = val.replace(/^\+(\d{2,3})[ ]*\1[ ]*/, '+$1 '); // replace repetition of contry code


        return $scope.UpdateUser.mobile = $("#txt-mobile").val();
    };
    //        $scope.cleantel = function (obj) {
    //            obj = obj.replace(/[^0-9 \+]/g, '');
    //            return obj;
    //        }

    $scope.listPhoneIndex = function(tt) {
        for (i = 0; i < $scope.countries.length; i++)
            if ($scope.countries[i].a == tt) {
                $scope.currentcc = i;
                break;
            }

        if (i >= $scope.countries.length) {
            alert('Invalid Country Code');
            $scope.currentcc = 10; // Singapore
        }
        $scope.curflgicon = "famfamfam-flag-" + $scope.countries[$scope.currentcc].b;

        $scope.phoneindex($scope.countries[$scope.currentcc].c);

    }
    $scope.cleantel = function (obj) {  

        obj = obj.replace(/[^0-9 \+]/g, '');
 
        return obj;
    }
    $scope.checkvalidtel = function() {
        $scope.UpdateUser.mobile = $scope.cleantel($scope.UpdateUser.mobile);
       
        if ($scope.UpdateUser.mobile == "") {
            $scope.UpdateUser.mobile == $scope.countries[$scope.currentcc].c.trim() + ' ';
            return;
        }
        $scope.curflgicon = "famfamfam-flag-" + $scope.countries[$scope.currentcc].b;
        //$('#bkcountry').val($scope.countries[$scope.currentcc].a);
        $scope.phoneindex($scope.countries[$scope.currentcc].c);
    };
    //alert($scope.SignupForm);
    $scope.listPhoneIndex('Singapore');
    $scope.ChangePassword = function(old_password, new_password,confirm_password) {
        $rootScope.page = 'MyAccount-pwdchange';
        if(new_password!==confirm_password){
            alert("password does not match,Please enter same password again");
            return;
        }
        AuditLog.logevent(6, $rootScope.user.email);

        loginService.change($rootScope.user.email, old_password, new_password, $rootScope.user.getToken()).then(function(response) {
            if (response.status == 1) {
                alert(response.errors);
            };
        });
    };



}]);
