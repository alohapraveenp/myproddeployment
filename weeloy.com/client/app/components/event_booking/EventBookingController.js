(function (app) {
    app.controller('EventBookingCtrl', EventBookingCtrl);
    EventBookingCtrl.$inject = ['$rootScope', '$scope', '$http', '$location', '$routeParams', 'API'];
    

    function EventBookingCtrl($rootScope, $scope, $http, $location, $routeParams, API) {
       
        var script = document.createElement('script');
            script.src = 'https://checkout.stripe.com/checkout.js';
            document.body.appendChild(script);
             script.src = 'https://js.stripe.com/v2/';
            document.body.appendChild(script);
            $scope.timeflg = 12 ;
      
            
            var url = $location.path();
        $location.path(url).search({'bktracking':'WEBISTE','dspl_h': 'f','dspl_f': 'f'});
        
              $scope.isperbookingflg = 15;

//        $scope.DeliveryTimeRange = [
//            {time:'19:30', selected:true}
//        ];
//
//        $scope.tableSize = [
//            {pax:1, selected:false}, 
//            {pax:2, selected:false}, 
//            {pax:3, selected:false},
//            {pax:4, selected:false}
//        ];
//        $scope.DeliveryTimeRange = [
//            {time:'10:00', selected:false},
//            {time:'11:00', selected:false},
//            {time:'12:00', selected:false},
//            {time:'13:00', selected:false},
//            {time:'14:00', selected:false},
//            {time:'15:00', selected:false},
//            {time:'16:00', selected:false},
//            {time:'17:00', selected:false},
//            {time:'18:00', selected:false},
//            {time:'20:00', selected:false},
//            {time:'21:00', selected:false} ,
//            {time:'22:00', selected:false} ,
//            
//        ];
  
//        $scope.tableSize = [{pax:2, selected:true}, 
//            {pax:3, selected:false}, 
//            {pax:4, selected:false}, 
//            {pax:5, selected:false}, 
//            {pax:6, selected:false}, 
//            {pax:7, selected:false}, 
//            {pax:8, selected:false}];
         
        $scope.bktracking = $routeParams.bktracking;
          
        var restaurant = new Restaurant();
        var RestaurantID = restaurant.getRestaurantIdFromUrlParam($routeParams.city, $routeParams.restaurant);
        var Order = {
            pax: '2'
        };
        $scope.paymethod ='Paypal';

        $rootScope.$watch('user', function (user) {
            if (user !== undefined) {
                Order.first_name = user.getFirstName();
                Order.last_name = user.getLastName();
                Order.email = user.getEmail();
                Order.phone = user.getMobileNumberFormatted();
            }
        });
        if(RestaurantID === 'SG_SG_R_Bacchanalia'){
            $scope.DeliveryTimeRange = [
            {time:'19:00', selected:false}];
        }
            
        
        
        $scope.Order = Order;
        if($scope.DeliveryTimeRange){
            $scope.Order.time = $scope.DeliveryTimeRange[0].time;
        }
        API.restaurant.getFullInfo(RestaurantID)
                .then(function (result) {
                    $scope.restaurant = result;

                });
                
        $scope.SelectTime = function(time) {
            if($scope.DeliveryTimeRange){
                $scope.DeliveryTimeRange.forEach(function(value, key) {
                    $scope.DeliveryTimeRange[key].selected = false;
                });
                time.selected = true;
                $scope.Order.time = time.time;
            }else{
                 $scope.Order.time = "00:00";
            }
        };
        
        
        API.restaurant.getRestaurantEvent(RestaurantID)
                .then(function (response) {

                    if (response.status == 1 && response.count > 0) {
                        var bkdata = response.data.evtbookinglist;
                        var bkpax = 0, i;
                        response.data.event.forEach(function (value, key) {
                                if (value.eventID === $routeParams.event_id) {
                                     $scope.DeliveryTimeRange = [];
                                     value.restaurant = RestaurantID;
                                // if(value.restaurant === 'SG_SG_R_Esquina'){
                                //             value.price = 50.00;
                                // }
                                //$scope.RestaurantEvent = new RestaurantEvent(value);
                                $scope.maxpax = value.maxpax;
                                $scope.totalPax = value.maxpax;
                                if(value.bookings !=='0')
                                    $scope.maxpax = parseInt(value.bookings);
                                if(value.deposit !== '0')
                                    value.price = value.deposit;
                                if(value.is_perbookingprice ==='1'){
                                    $scope.isperbookingflg = 16;
                                }
                                 

                                // if(RestaurantID === 'SG_SG_R_Bacchanalia')
                                // 	$scope.maxpax = 4;
                                 if(value.event_times){
                                     var timeArr = value.event_times.split(',');
                                       timeArr.forEach(function (value, key) {
                                          $scope.DeliveryTimeRange.push({time:timeArr[key], selected:false});
                                     });
                                     
                                 }
                             $scope.RestaurantEvent = new RestaurantEvent(value);
                                $('#datetimepicker').datetimepicker({
                                    inline: true,
                                    locale: 'en',
                                    format: 'DD/MM/YYYY',
                                    // enabledDates: ['02/14/2016', '02/15/2016', '02/16/2016'],
                                    minDate: $scope.RestaurantEvent.getStartTime('YYYY-MM-DD'),
                                    maxDate: $scope.RestaurantEvent.getEndTime('YYYY-MM-DD'),
                                });
                                //$scope.DeliveryDate = moment($scope.RestaurantEvent.getStartTime('YYYY-MM-DD'));
                                 $scope.DeliveryDate = $scope.RestaurantEvent.getStartTime('YYYY-MM-DD');
                                 if(value.event_times){
                                      $scope.DeliveryDate = moment($scope.RestaurantEvent.getStartTime('YYYY-MM-DD'));
                                      console.log("ASDs" +  moment($scope.RestaurantEvent.getStartTime('YYYY-MM-DD')));
                                           
                              
                                 }
                                $('#datetimepicker').on("dp.change", function (e) {
                                    if(value.event_times){
                                        
                                       //$scope.DeliveryDate = moment($scope.RestaurantEvent.getStartTime('YYYY-MM-DD'));
                                    
                                       $scope.DeliveryDate = $scope.RestaurantEvent;// e.date; // e.date is momentjs object
                                        $scope.$apply();
                                    }else{
                                           $scope.timeflg = 13;
                                        $scope.DeliveryDate = moment(e.date._d).format('YYYY-MM-DD');
                                    }
                                   
//                                    scope.EventObject.rdate = DateSelected;
                                    //$scope.DeliveryDate = $scope.RestaurantEvent;// e.date; // e.date is momentjs object
                                    $scope.$apply();
                                });
                        console.log("DATE " + $scope.RestaurantEvent);
                                $scope.range = [];
                                $scope.startpax = 2;
                                if($scope.isperbookingflg === 16){
                                    $scope.startpax = $scope.maxpax;
                                    Order.pax = $scope.startpax;
                                }
                                for(i=$scope.startpax;i<=$scope.maxpax;i++) {
                                    $scope.range.push(i);
                                }
                     
                                for (var j = 0; j < bkdata.length; j++) {
                                    if (bkdata[j].status === 'COMPLETED') {
                                        if (bkdata[j].eventID === value.eventID) {
                                         bkpax += parseInt(bkdata[j].pax);
                                        }
                                    }
                                }
                                $scope.balPax = $scope.totalPax - bkpax;
                                
                              
                            }
                        });
                    }
                });

           
                
           var API_URL  = 'api/payment/stripe/credentials/'+RestaurantID;
                $http.get(API_URL).then(function (response) {
                    if(response.data.data !=='undefined'){
                        $scope.publishable_key = response.data.data.publishable_key;
                        //console.log($scope.publishable_key);

                    }

                });            


        /*
         * Save payment info to database
         */
        
        // Close Checkout on page navigation:
        $(window).on('popstate', function () {
            handler.close();
        });
        $scope.SaveOrderRequest = function (OrderForm, Order, RestaurantEvent, DeliveryDate) {

            if (Order.pax > $scope.balPax) {
                alert("This event is currently available for " + $scope.balPax + " people only");
                return false;
            }
//            if ($scope.timeflg === 12 && !Order.time) {
//                alert("Please select your time of venue.");
//                return false;
//            }


            if (OrderForm.$invalid) {

                var top;
                if (OrderForm.first_name.$invalid || OrderForm.last_name.$invalid || OrderForm.email.$invalid || OrderForm.phone.$invalid) {
                    top = $('input[name="first_name"]').offset().top;
                    $(window).scrollTop(top - 200);
                }
                return false;
            }
           
            if(Order.time){
               console.log("DATE in submit " + $scope.DeliveryDate);
                timeAr = Order.time.split(":");
                DeliveryDate.set({ hour:timeAr[0], minute:timeAr[1] });
                DeliveryDate = DeliveryDate.format('YYYY-MM-DD HH:mm:ss');
                      console.log("TIME " + DeliveryDate);
            }else{
                 DeliveryDate = DeliveryDate;
            }
    
           $scope.amount = Math.round(Order.pax * RestaurantEvent.getPrice() * 100)/100;
            if($scope.isperbookingflg === 16){
                $scope.amount = RestaurantEvent.getPrice();
            }
 
            var data = {
                amount: $scope.amount,
                curency: Order.curency || 'SGD',
                event_id: RestaurantEvent.getEventID(),
                pax: Order.pax,
                restaurant_id: $scope.restaurant.getRestaurantId(),
                DeliveryDate: DeliveryDate,
                special_request: Order.special_request || '',
                first_name: Order.first_name || '',
                last_name: Order.last_name || '',
                email: Order.email || '',
                phone: Order.phone || '',
                company: Order.company || '',
                returnUrl: BASE_URL + '/' + $scope.restaurant.getInternalPath() + '/order-details/',
                cancelUrl: BASE_URL + '/' + $scope.restaurant.getInternalPath() +'/event/'+ RestaurantEvent.getEventID(),
                ipnNotificationUrl: BASE_URL + '/modules/event_booking/event_booking.php?action=paypal_ipn',
                action: 'formdata',
                bktracking: $scope.bktracking,
            };
            $scope.orderDetails = data;
         
                    var handler = StripeCheckout.configure({
                         key: $scope.publishable_key,  
                    token: function (token) {
    //                    var data = $scope.orderDetails;
                        data.stripeToken = token.id;

                        var form = document.createElement('form');
                        form.action = 'modules/payment/stripe/stripe_event_payment_form.php';
                        form.method = 'POST';
                        $.each(data, function (key, value) {
                            var element = document.createElement("Input");
                            element.setAttribute("type", "hidden");
                            element.setAttribute("name", key);
                            element.setAttribute("value", value);
                            var div = document.createElement("div");
                            form.appendChild(element);
                        });

                        document.body.appendChild(form);
                        form.submit();
       
                    }
                });
            

            var mode = $("input:radio[name=optionsPaymentRadios]:checked").val();
            var url;
      
            if (mode === 'paypal') {
                url = BASE_URL + '/modules/event_booking/event_booking.php?action=insert';
                $http.post(url, data)
                        .then(function (response) {
                                 
                            if (response.data.status === 1) {
                                  //paypal form submit
                                var form = document.createElement('form');
                                form.action = response.data.data.paypal_url;
                                form.method = 'POST';
                                document.body.appendChild(form);
                                form.submit();
                                //location.href = response.data.data.paypal_url;
                            }
                        }, function (error) {
                        });
            } else {
                
                handler.open({
                    email: Order.email,
                    panelLabel: 'Submit'
                });



            }
        };


        // make right side bar affix
        var sidebar = $('#sidebar-right');
        var sidebar_affix_top = 60;
        var sidebar_offset = sidebar.offset();
        sidebar.css({
            position: 'fixed',
            top: sidebar_offset.top + 'px',
            left: sidebar_offset.left + 'px',
        });
        $(window).resize(function () {
            sidebar.css({
                position: 'relative',
                left: '0px',
            });
            sidebar.css({
                left: sidebar.offset().left + 'px',
                position: 'fixed',
            });
        });
        $(window).scroll(function () {
            if ($(window).scrollTop() + sidebar.height() > $('.event-booking').height()) {
                sidebar.css({
                    top: ($('.event-booking').height() - $(window).scrollTop() - sidebar.height()) + 'px',
                });
            } else {
                if ($(window).scrollTop() + sidebar_affix_top > sidebar_offset.top) {
                    sidebar.css({
                        position: 'fixed',
                        top: sidebar_affix_top + 'px',
                    });
                } else {
                    sidebar.css({
                        position: 'fixed',
                        top: (sidebar_offset.top - $(window).scrollTop()) + 'px',
                    });
                }
            }

        });
    }
})(angular.module('app.components.event-booking', [
    'app.shared.filters.timeFormat'
]));
