(function(app) {
    app.controller('EventBookingDetailCtrl', EventBookingDetailCtrl);
    EventBookingDetailCtrl.$inject = ['$rootScope', '$scope', '$http', '$routeParams', 'API'];


    function EventBookingDetailCtrl($rootScope, $scope, $http, $routeParams, API) {
        var restaurant = new Restaurant();
        var RestaurantID = restaurant.getRestaurantIdFromUrlParam($routeParams.city, $routeParams.restaurant);
    
        $scope.orderid = Math.floor((Math.random() * 10000) + 1);

        $scope.bktracking = $routeParams.bktracking;

        
        API.restaurant.getFullInfo(RestaurantID)
            .then(function(result) {
                $scope.restaurant = result;
                $scope.restaurant_url = BASE_URL + '/' + result.getInternalPath();
            });
        API.payment.getEventBooking($routeParams.order_id)
            .then(function(response) {
                $scope.Order = response.data;

        var myDate = new Date($scope.Order.time);
        var displayDate = myDate.getDate()+ '-' +myDate.getMonth()+ '-' +myDate.getFullYear() + " " + myDate.getHours() + ":"  +myDate.getMinutes();
        $scope.Order.date = displayDate;
            console.log(displayDate);
                API.restaurant.getRestaurantEvent(RestaurantID)
                    .then(function(response) {
                        if (response.status == 1 && response.count > 0) {
                            response.data.event.forEach(function(value, key) {
                                if (value.eventID === $scope.Order.event_id) {
                                    value.restaurant = RestaurantID;
                                    $scope.RestaurantEvent = new RestaurantEvent(value);
                                    console.log($scope.RestaurantEvent, $scope.RestaurantEvent.getPrice());
                                }
                            });
                        }
                    });
            })
            .catch(function(error) {
                console.log(error);
            });
    }
})(angular.module('app.components.event-booking-detail', [
    'app.shared.filters.timeFormat'
]));
