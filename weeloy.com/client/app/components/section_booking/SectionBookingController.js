(function(app) {
    app.controller('SectionBookingCtrl', SectionBookingCtrl);
    SectionBookingCtrl.$inject = [
        '$rootScope',
        '$scope',
        '$routeParams',
        'API',
        'TitleAndMeta',
        'Notification'
    ];

    function SectionBookingCtrl(
        $rootScope,
        $scope,
        $routeParams,
        API,
        TitleAndMeta,
        Notification
    ) {
        var restaurant = new Restaurant();
        var RestaurantID = restaurant.getRestaurantIdFromUrlParam($routeParams.city, $routeParams.restaurant);

        $scope.RestaurantID = RestaurantID;
        API.restaurant.getFullInfo(RestaurantID)
            .then(function(result) {
                result.setMediaServer($rootScope.mediaServer);
                var title = result.getTitle() + ' - Book with Weeloy and Get Rewarded';
                var description = 'Book a Restaurant in ' + result.getCity() + ' at ' + result.getTitle() + ' with Weeloy and get yourself rewarded with exclusive deals and promotions. Simply book your table and spin the wheel!';
                TitleAndMeta.setTitle(title);
                $scope.takeoutitle = (result.takeoutrestaurant > 0) ? '/ Take Out' : '';
                if (result.book_button.text.toLowerCase() == 'booking coming soon') {
                    $scope.disableBookButtton = true;
                }

                $scope.restaurant = result;
            })
            .catch(function(e) {
                console.log(e);
            });

        /**
         * Initial Section
         */
        $scope.sections = [
            { id: 1, name: 'Chef table' },
            { id: 2, name: 'Counter seats' },
            { id: 3, name: 'Bar seats' }
        ];
        $scope.section = $scope.sections[0];
        $scope.sectionID = $scope.sections[0].id;

        /**
         * Initial Pax
         */
        $scope.paxs = [];
        for (i = 1; i <= 10; i++) {
            $scope.paxs.push(i);
        }
        $scope.pax = $scope.paxs[0];

        /**
         * Initial Type
         */
        $scope.types = [{
            id: 1,
            name: 'lunch',
            times: [
                '10:00',
                '10:15',
                '10:30',
                '11:00'
            ]
        }, {
            id: 2,
            name: 'dinner',
            times: [
                '16:00',
                '16:15',
                '17:00'
            ]
        }];
        $scope.type = $scope.types[0];
        $scope.time = $scope.type.times[0];

        $scope.selectedDate = moment().format('YYYY-MM-DD');
        $scope.checkDateAfterChangePax = angular.copy($scope.selectedDate);

        var params = {
            restaurant: RestaurantID,
            product: $scope.section.name,
            time: $scope.time,
            pers: $scope.pax
        };

        function getDayavailable(params) {
            API.restaurant.dayavailable(params)
                .then(function(result) {
                    generateAvaiableDatePicker(result);
                })
                .catch(function(error) {
                    console.log(error);
                });

        }
        getDayavailable(params);

        function generateAvaiableDatePicker(dayavailable) {
            var enabledDates = [];
            if (dayavailable.data !== undefined) {
                var daylength = dayavailable.data.length;
                var timeNow = 0;
                var day;
                for (var i = 0; i < daylength; i++) {
                    if (dayavailable.data.charAt(i) === '1') {
                        day = moment().add(timeNow, 'days').format('YYYY-MM-DD');
                        enabledDates.push(day);
                    }
                    timeNow++;
                }
            }
            $scope.enabledDates = _.map(enabledDates, function(date) {
                return moment(date);
            });
        }

        $scope.changeSectionRadio = function(section) {
            $scope.sectionID = section.id;
            params.product = section.name;
            getDayavailable(params);
        };

        $scope.changePax = function(pax) {
            $scope.pax = pax;
            params.pers = pax;
            getDayavailable(params);
        };

        $rootScope.$on('selectedDate', function(event, selectedDate) {
            $scope.selectedDate = selectedDate;
        });

        $scope.changeType = function(type) {
            $scope.type = type;
            $scope.time = type.times[0];
        };

        $scope.changeTime = function(time) {
            $scope.time = time;
            params.time = time;
            getDayavailable(params);
        };

        $scope.bookNow = function() {
            if ($scope.sectionID === undefined | $scope.sectionID === null) {
                Notification.show('warning', 'Please select section');
                return false;
            }
            if ($scope.selectedDate === undefined | $scope.selectedDate === null) {
                Notification.show('warning', 'Please select date');
                return false;
            }
            if ($scope.pax === undefined | $scope.pax === null) {
                Notification.show('warning', 'Please select pax');
                return false;
            }
            if ($scope.type === undefined | $scope.type === null) {
                Notification.show('warning', 'Please select type');
                return false;
            }
            if ($scope.time === undefined | $scope.time === null) {
                Notification.show('warning', 'Please select time');
                return false;
            }
        };
    }

    app.directive('datePicker', ['$rootScope', function($rootScope) {
        return {
            strict: 'AE',
            scope: {
                enabledDates: '=',
                selectedDate: '=',
            },
            link: function(scope, elem, attrs) {
                var options = {
                    locale: 'en',
                    format: 'DD MMM, YYYY',
                    useCurrent: true,
                    showTodayButton: false,
                    inline: true,
                    keepOpen: true,
                    focusOnShow: false
                };
                $(elem).datetimepicker(options);
                scope.$watch('enabledDates', function(enabledDates) {
                    if ($(elem).data('DateTimePicker') !== undefined) {
                        $(elem).data('DateTimePicker').enabledDates(enabledDates);
                        $(elem).on("dp.change", function(e) {
                            var DateSelected = moment(e.date._d).format('YYYY-MM-DD');
                            $rootScope.$broadcast('selectedDate', DateSelected);
                        });
                    }
                });
            }
        };
    }]);

})(angular.module('app.components.section_booking', []));
