var DateTimePicker = angular.module('ShoppingCart');
DateTimePicker.directive('datetimepicker',  function () {
    return {
        restrict: 'A',
        require : 'ngModel',
        link : function (scope, element, attrs, ngModelCtrl) {

            element.datetimepicker({
                change:function (date) {

                    // Triggers a digest to update your model
                    scope.$apply(function () {
                        ngModelCtrl.$setViewValue(date);
                    });

                }
            });
        }
    };
});