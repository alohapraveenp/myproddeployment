(function(app) {
    app.directive('restaurantItem', directive);
    directive.$inject = ['$rootScope'];

    function directive($rootScope) {
        return {
            restrict: 'E',
            templateUrl: 'client/app/shared/partial/_restaurant_item.tpl.html',
            scope: {
                restaurant: '=',
                openInNewTab: '@',
                mediaServer: '=',
                indexcount: '=',
            },

            link: function(scope, element, attrs) {
                scope.restaurant.setMediaServer(scope.mediaServer);
                var imageSize = 500;
                var wheelSize = 150;
                var ImageWidth = imageSize;
                // default size of restaurant image is 500px;
                // default size of wheel image is 150px;

                var windowWidth = $(window).width();
                if (1199 < windowWidth) {
                    ImageWidth = Math.round(windowWidth * 0.8 * 0.45);
                }
                if (767 < windowWidth && windowWidth < 1200) {
                    ImageWidth = Math.round(windowWidth * 0.8 * 0.45);
                }
                if (windowWidth < 768) {
                    ImageWidth = windowWidth;
                }
                var wheelImageWidth = Math.round(ImageWidth * 0.8 * 0.3);
                if ($rootScope.imageSize === undefined && ImageWidth > 0) {
                    config.ImageSizes.forEach(function(value, key) {
                        if (value > ImageWidth && $rootScope.imageSize === undefined) {
                            imageSize = value;
                            $rootScope.imageSize = imageSize;
                        }
                    });
                } else {
                    imageSize = $rootScope.imageSize;
                }
                if (imageSize === 500 || imageSize === '/500/') {
                    imageSize = '/';
                } else {
                    if (!(imageSize[0] == '/' && imageSize.charAt[imageSize.length - 1] == '/')) {
                        imageSize = '/' + imageSize + '/';
                    }

                    imageSize = imageSize.replace(/\/\//g, '/');
                    if (typeof imageSize == 'undefined') {
                        imageSize = '/360/';
                    }
                }
                
               
                
                
                if ($rootScope.wheelSize === undefined && wheelImageWidth > 0) {
                    config.WheelImageSizes.forEach(function(value, key) {

                        if (value > wheelImageWidth && $rootScope.wheelSize === undefined) {
                            wheelSize = '/' + value + '/';
                            $rootScope.wheelSize = wheelSize;
                        }
                    });
                    // FIX fro refresh --> image   var ImageWidth = ($(element).find('.img-transparent').width()) == 1024px.
                    if ($rootScope.wheelSize === undefined && wheelSize === 0 && wheelImageWidth === 240.48) {
                        wheelSize = '/' + 150 + '/';
                        $rootScope.wheelSize = wheelSize;
                    }
                } else {
                    wheelSize = $rootScope.wheelSize;
                }

                if ($rootScope.wheelSize === undefined && wheelSize === 0 && wheelImageWidth > 150) {
                    wheelSize = '/' + 150 + '/';
                    $rootScope.wheelSize = wheelSize;
                }

                if (!(wheelSize[0] == '/' && wheelSize[wheelSize.length - 1] == '/')) {
                        wheelSize = '/' + wheelSize + '/';
                }
                
                if(wheelSize.indexOf('//') > -1){
                    wheelSize = '/150/';
                }

                scope.restaurant.wheelImageUrl = scope.restaurant.getWheelImage(wheelSize);

                if (scope.openInNewTab == 'true' && $(window).width() > 480) {
                    //scope.OpenRestaurantInNewTab = '_blank';
                }

                var img = document.createElement('img');
                img.src = scope.restaurant.getImage(imageSize);
                img.onload = function() {
                    $(element).find('.img-transparent').css('background', 'url(' + img.src + ')');
                };

                //define cpp partner program
                if (typeof $rootScope.UserSession.affiliate_program !== "undefined" && $rootScope.UserSession.affiliate_program !== "") {
                    if ($rootScope.UserSession.affiliate_program == scope.restaurant.affiliate_program) {
                        scope.affiliate_program = true;
                    }
                }

                scope.go = function(restaurant) {
                    document.location.href = "/" + restaurant.internal_path + "/booknow";
                    //WindowOpen(restaurant.internal_path + '/book-now');
                };

                $(element).hover(function() {
                    if (scope.restaurant.gmapMarker !== null) {
                        scope.restaurant.gmapMarker.setIcon('images/highlight-marker.png');
                    }
                }, function() {
                    if (scope.restaurant.gmapMarker !== null) {
                        scope.restaurant.gmapMarker.setIcon('images/gmap-marker.png');
                    }
                });


            },
        };
    }
})(angular.module('RestaurantItem', ['templates-main']));
