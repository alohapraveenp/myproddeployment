/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var ReviewProfile = angular.module('ReviewProfile', []);
ReviewProfile.directive('dimentionClass', function() {
    return {
        link: function(scope, element) {
            var img = element[0];
            img.onload = function() {
                //console.log($(img).attr('id'));
                img.className = img.width > img.height ? 'landscape' : 'portrait';
            };
        }
    };
});


