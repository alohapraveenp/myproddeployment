var RestaurantCategoryItem = angular.module('RestaurantCategoryItem', []);
RestaurantCategoryItem.directive('restaurantCategoryItem', ['$rootScope', function ($rootScope) {
        return {
            restrict: 'E',
            templateUrl: 'client/app/shared/partial/_restaurant_category_item.html',
            scope: {
                category: '=',
                openInNewTab: '@',
                mediaServer: '=',
            },
            link: function (scope, element, attrs) {
                var img = document.createElement('img');
                var imageSize = 500;
                var ImageWidth = imageSize;
                // default size of restaurant image is 500px;
                // default size of wheel image is 150px;

                var windowWidth = $(window).width();
                if (windowWidth < 768) {
                    ImageWidth = windowWidth;
                }
                if (767 < windowWidth < 1200) {
                    ImageWidth = Math.round(windowWidth * 0.8 * 0.45);
                }
                if (1199 < windowWidth) {
                    ImageWidth = Math.round(windowWidth * 0.8 * 0.25);
                }
                var wheelImageWidth = Math.round(ImageWidth * 0.8 * 0.3);
                if ($rootScope.imageSize === undefined && ImageWidth > 0) {
                    config.ImageSizes.forEach(function (value, key) {
                        if (value > ImageWidth && $rootScope.imageSize === undefined) {
                            imageSize = value;
                            $rootScope.imageSize = imageSize;
                        }
                    });
                } else {
                    imageSize = $rootScope.imageSize;
                }
                if (typeof imageSize == 'undefined') {
                    $rootScope.imageSize = imageSize = '/500/';
                }
                if (imageSize === 500 || imageSize === '/500/') {
                    imageSize = '/';
                } else {
                    if (!(imageSize[0] == '/' && imageSize.charAt[imageSize.length - 1] == '/')) {
                        imageSize = '/' + imageSize + '/';
                    }

                    imageSize = imageSize.replace(/\/\//g, '/');
                    if (typeof imageSize == 'undefined') {
                        imageSize = '/360/';
                    }
                }

                my_image = scope.category.images[Math.floor(Math.random() * scope.category.images.length)];
                // the image will load from random server and best size match with current screen width
                var url;
        
                if (scope.mediaServer === undefined) {
                
                    if(my_image.restaurant.match(/SG_SG_R_/g)){
                          url = '//media.weeloy.com/upload/restaurant/' + encodeURIComponent(my_image.restaurant.trim()) + imageSize + encodeURIComponent(my_image.image);
                    }else{
                        url = '//media.weeloy.com/upload/restaurant/' + encodeURIComponent(my_image.restaurant) + imageSize + encodeURIComponent(my_image.image); 
                    }
                  
                } else {
                    
                    if(my_image.restaurant.match(/SG_SG_R_/g)){
                       url = scope.mediaServer + '/upload/restaurant/' + encodeURIComponent(my_image.restaurant.trim()) + imageSize + encodeURIComponent(my_image.image);
                    }else{
                        url = scope.mediaServer + '/upload/restaurant/' + encodeURIComponent(my_image.restaurant) + imageSize + encodeURIComponent(my_image.image);
                    }
                    
                }
                //console.log("url="+url);
                img.src = url;

                if (scope.openInNewTab == 'true' && $(window).width() > 480) {
                    scope.OpenRestaurantInNewTab = '_blank';
                }
                img.onload = function () {
                    $(element).find('.img-transparent').css('background', 'url(' + url + ')');
                };
                scope.go = function (restaurant) {
                    document.location.href = "/" + restaurant.internal_path + "/booknow";
                    //WindowOpen(restaurant.internal_path + '/book-now');
                };


//                function WindowOpen(url) {
//                    window.open(url, '_blank', 'toolbar=no, scrollbars=yes, resizable=yes, top=30, left=30, width=800, height=900');
//                }
            },
        };
    }]);