/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var AuditLog = angular.module('AuditLog', []);
AuditLog.service('AuditLog', ['$rootScope','$http', function($rootScope,$http) {
   this.logevent = function(action,event) {
       var others ='';
     if($rootScope.audit_other && $rootScope.page ==='my_bookings'){
         others = $rootScope.audit_other ;
     }
       var API_URL ="api/v2/system/tracking/log";

            return $http({
                    url: API_URL,
                    method: "POST",
                    data: $.param({
                        'action': action,
                        'event' :event,
                        'page' :$rootScope.page,
                        'others' :others
                    }),
                     headers:  {'Content-Type': 'application/x-www-form-urlencoded'}
                    }).then(function(response) {
                     return response.data;
            });
       

    };   
        
}]);

