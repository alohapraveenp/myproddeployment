var LoginService = angular.module('LoginService', []);
LoginService.service('loginService', ['$http', '$q', function($http, $q) {


    this.login = function(email, passw) {

        console.log('rootscope 6');
        //console.log($rootScope.user);

        var defferred = $q.defer();
        $http.get("api/rmloginc/" + email + "/" + passw + "/web/1.0").then(function(response) {
            if (response.data.status === 1) {
                response.data.data.email = email;
                var user = new User(response.data.data);
                defferred.resolve(user);
            } else {
                defferred.reject(response.data.errors);
            }
        });
        return defferred.promise;
    };

    this.change = function(email, passw, npassw, token) {
        return $http.post("api/services.php/rmloginc/module/change/", {
                'email': email,
                'password': passw,
                'npassword': npassw,
                'token': token
            })
            .then(function(response) {
                return response.data;
            });
    };

    this.update = function(label, value) {
        return $http.get("api/updateProfile/" + label + '/' + value).then(function(response) {
            return response.data;
        });
    }

    this.forgot = function(email, redirect_to) {
        return $http.post("api/services.php/rmloginc/module/forgot", {
                'email': email,
                'redirect_to': redirect_to
            })
            .then(function(response) {
                return response.data;
            });
    };

    this.logout = function(email) {

        return $http.get("api/rmlogout/" + email + "/weeloy.com").then(function(response) {
            return response.data;
        });
    };

    this.register = function(user) {

        var url = 'api/addmember/' + user.email + '/' + user.mobile + '/' + user.password + '/' + user.last_name + '/' + user.first_name + '/web';
        return $http.get(url).then(function(response) {

            return response.data;
        });
    };

    this.checkstatus = function(email, token) {
        return $http.post("api/services.php/rmloginc/module/checkstatus/", {
                'email': email,
                'token': token
            })
            .then(function(response) {
                return response.data;
            });
    };

    this.loginfacebook = function(params) {
        var defferred = $q.defer();
        
        console.log('Ohuilippe');
        
         $http.post("api/rmloginc/module/loginfacebook/", params)
            .then(function(response) {
                if (response.data.status === 1) {
                    response.data.data.email = params.email;
                    var user = new User(response.data.data);
                    defferred.resolve(user);
                } else {
                    defferred.reject(response.data.errors);
                }
                
            });
            return defferred.promise;
    };   

}]);

LoginService.factory('DataService', function($http, URL) {
    var getData = function() {
        return $http.get(URL + 'content.json');
    };

    return {
        getData: getData
    };
});
