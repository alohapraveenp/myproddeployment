var TrustAsResource = angular.module('TrustAsResource', []);
TrustAsResource.filter('trustAsResource', ['$sce', function($sce) {
	return function(src) {
		return $sce.trustAsResourceUrl(src);
	};
}]);