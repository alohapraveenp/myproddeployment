(function(app) {
    app.service('MyReviewService', ['$q', '$http', function($q, $http) {
        this.getMyReviews = function() {
            var defferred = $q.defer();
            $http.get('api/getReviewList').success(function(response) {
                defferred.resolve(response);
            });
            return defferred.promise;
        };

        this.getBooking = function(query) {
            var defferred = $q.defer();
            var API_URL = 'api/bkconfirmation/' + query;
            $http.get(API_URL, {
                cache: true,
            }).success(function(response) {
                defferred.resolve(response);
            });
            return defferred.promise;
        };
        this.reviewPostGrade = function(data) {
              var defferred = $q.defer();
            $http.post('api/review/grade', data).success(function(response) {
                defferred.resolve(response);
            });
               
            return defferred.promise;
        };
    }]);

})(angular.module('app.api.account.myreview', []));
