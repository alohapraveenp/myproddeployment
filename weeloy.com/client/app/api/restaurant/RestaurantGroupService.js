(function (app) {
    app.service('RestaurantGroupService', ['$http', '$q', function ($http, $q) {
            this.getGroup = function (query) {
                var API_URL = 'api/v2/restaurant/groups/' + query + '/details';
                var defferred = $q.defer();
                $http.get(API_URL, {
                    cache: true
                }).success(function (response) {
                    var restaurants = [];
                    var data = [];
                    if (response.status == 1) {
                        response.data.restaurant.forEach(function (value) {
                            restaurants.push(new Restaurant(value));
                        });
                        data.push(response.data.info);
                        data.push(restaurants);
                    } else {
                        defferred.reject('No data found');
                    }
                    defferred.resolve(data);
                });
                return defferred.promise;
            };
        }]);
})(angular.module('app.api.restaurant.group', []));

