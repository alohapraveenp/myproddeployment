(function(app) {
    app.service('CateringService', ['$http', '$q', function($http, $q) {
        this.getCateringMenu = function(restaurant) {
            var defferred = $q.defer();
            var API_URL = 'api/restaurant/catering/menu/' + restaurant;
            $http.get(API_URL, {
                cache: true
            }).success(function(response) {
                defferred.resolve(response);
            });
            return defferred.promise;
        };
    }]);

})(angular.module('app.api.restaurant.catering', []));
