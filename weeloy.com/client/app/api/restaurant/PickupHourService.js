(function(app) {
    app.service('PickupHourService', ['$http', '$q', function($http, $q) {
        this.getPickupHours = function(restaurant) {
            var defferred = $q.defer();
            var API_URL = 'api/restaurant/getPickupHours/' + restaurant;
            $http.get(API_URL, {
                cache: true
            }).success(function(response) {
                defferred.resolve(response);
            });
            return defferred.promise;
        };
    }]);
})(angular.module('app.api.restaurant.pickuphour', []));
