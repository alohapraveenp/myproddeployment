#!/bin/bash

cp /usr/share/zoneinfo/Asia/Singapore /etc/localtime

mkdir -p /var/log/deploy/
LOGFILE="/var/log/deploy/02_prepare_files.log"

rm -f conf/conf.mysql.inc.php  >> ${LOGFILE} 2>&1
rm -f conf/conf.mysql_dev.inc.php  >> ${LOGFILE} 2>&1
mv conf/conf.mysql_prod_bean.inc.php conf/conf.mysql.inc.php  >> ${LOGFILE} 2>&1

rm -f conf/conf.init.inc.php  >> ${LOGFILE} 2>&1
rm -f conf/conf.init_dev.inc.php  >> ${LOGFILE} 2>&1
mv conf/conf.init_prod_bean.inc.php conf/conf.init.inc.php  >> ${LOGFILE} 2>&1

rm -f .htaccess  >> ${LOGFILE} 2>&1
rm -f .htaccess_dev  >> ${LOGFILE} 2>&1
rm -f .htaccess_prod  >> ${LOGFILE} 2>&1
mv .htaccess_prod_bean .htaccess  >> ${LOGFILE} 2>&1

rm -f tracking/.htaccess  >> ${LOGFILE} 2>&1
rm -f tracking/.htaccess_dev  >> ${LOGFILE} 2>&1
rm -f tracking/.htaccess_prod  >> ${LOGFILE} 2>&1
mv tracking/.htaccess_prod_bean tracking/.htaccess  >> ${LOGFILE} 2>&1

rm -f /etc/httpd/conf.d/vhosts.conf >> ${LOGFILE} 2>&1
mv .server/httpd/vhosts.conf /etc/httpd/conf.d/vhosts.conf  >> ${LOGFILE} 2>&1
mv .server/httpd/httpd_check_service.sh /etc/httpd/httpd_check_service.sh  >> ${LOGFILE} 2>&1
chmod +x /etc/httpd/httpd_check_service.sh

mkdir -p /root/.aws >> ${LOGFILE} 2>&1
rm -f /root/.aws/config >> ${LOGFILE} 2>&1
rm -f /root/.aws/credentials >> ${LOGFILE} 2>&1
mv .server/aws/config /root/.aws/config  >> ${LOGFILE} 2>&1
mv .server/aws/credentials /root/.aws/credentials  >> ${LOGFILE} 2>&1



chmod 777 tmp
chmod 777 tmp/*
mkdir -p tmp/twig_cache/
chmod 777 tmp/twig_cache
chmod 777 tmp/twig_cache/*
