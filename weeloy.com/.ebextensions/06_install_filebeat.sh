#!/bin/sh
LOGFILE="/tmp/06_install_filebeat.log"
rpm --import https://packages.elastic.co/GPG-KEY-elasticsearch >> ${LOGFILE} 2>&1
cp .server/elastic.repo /etc/yum.repos.d >> ${LOGFILE} 2>&1
yum -y install filebeat >> ${LOGFILE} 2>&1
cp .server/filebeat/filebeat.yml /etc/filebeat/filebeat.yml  >> ${LOGFILE} 2>&1
PATH=$PATH:/usr/share/filebeat/bin
export PATH
mv ${LOGFILE} /var/log/deploy
