<footer>
    <div class="follow-us">
        <div class="container">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <h3>Find us on</h3>
                <ul>
                        <li><a  target="_blank"  href="https://www.facebook.com/weeloy.sg"  id='social-facebook'><span class="fa fa-facebook"></span></a></li>
                        <li><a  target="_blank"  href="https://twitter.com/weeloyasia"  id='social-twitter'><span class="fa fa-twitter"></span></a></li>
                        <li><a  target="_blank"  href="https://www.linkedin.com/company/weeloy-pte-ltd"  id='social-linkedin'><span class="fa fa-linkedin"></span></a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="footer-bg">
        <div class="container">
            <div class="footer-col col-md-3 col-sm-6 col-xs-12">
                <h3>INFORMATION</h3>
                <ul class="footer-col-content">
                    <li><a href="blog" id='info-blog'>Food blog</a></li>
                    <li><a href="how-it-works" id='info-hiw'>How It Works</a></li>
                    <li><a href="info-contact" id='info-contact'>Contact Us</a></li>
                    <li><a href="info-partner" id='info-partner'>Partner</a></li>
                    <li><a href="info-faq" id='info-faq'>FAQ's</a></li>
                    <li><a target="_blank" href="https://www.weeloy.com/templates/tnc/termsservices.php" id='info-tns'>Terms and Conditions of Service</a></li>
                    <li><a target="_blank" href="https://www.weeloy.com/templates/tnc/termsconditions.php" id='info-tnc'>Privacy policy and Terms</a></li>
                </ul>
            </div>
            <div class="footer-col col-md-3 col-sm-6 col-xs-12">
                <h3>OUR LOCATIONS</h3>
                <ul class="footer-col-content">
                    <li><a href="#">Singapore</a></li>
                    <li><a href="#">Malaysia</a></li>
                    <li><a href="#">Thailand</a></li>
                </ul>
            </div>
            <div class="footer-col col-md-3 col-sm-6 col-xs-12">
                <h3>MOBILE</h3>
                <div class="footer-col-content">
                    <p>
                        <a  rel="nofollow" class="mobile-picture-div" target="_blank" href="http://itunes.apple.com/app/id973030193">
                            <img width="196" height="60" src="images/home_picture/app-store-badge_en.png" alt="Available on the App Store">
                        </a>
                    </p>
                    <p>
                        <a rel="nofollow"  class="mobile-picture-div" target="_blank" href="https://play.google.com/store/apps/details?id=com.weeloy.client">
                            <img width="196" height="60" src="images/home_picture/play-store-badge_en.png" alt="Get it on Google Play">
                        </a>
                    </p>
                </div>
            </div>
            <div class="footer-col col-md-3 col-sm-6 col-xs-12">
                <h3>NEWSLETTER</h3>
                <div class="footer-col-content">
                    <form method="POST" action="" id="newsletterform" name="newsletterform">
                        <input id="email" name="email" type="text" placeholder="e-mail address" />
                        <input id="submit-follow" onclick="verifFormNewsLetter(newsletterform);" type="button" value="Join" />
                        <br/>
                        <br/>
                        <p>
                            <span class="" type="label" id="newsletter_message"></span>
                        </p>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-log-12 col-md-12 col-sm-12 col-xs-12 copyright">
            <p class="text-center">Copyright &copy; 2015 Weeloy. All Rights Reserved.</p>
        </div>
    </div>
</footer>
