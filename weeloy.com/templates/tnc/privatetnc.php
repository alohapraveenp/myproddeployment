<?php
require_once("conf/conf.init.inc.php");
require_once("lib/wpdo.inc.php");
require_once("lib/wglobals.inc.php");
require_once('conf/conf.session.inc.php');
require_once("lib/class.media.inc.php");
require_once("lib/class.restaurant.inc.php");

$arglist = array('bkrestaurant', 'bktitke', 'bktracking');
foreach ($arglist as $label) {
    if (!isset($_REQUEST[$label]))
        $_REQUEST[$label] = "";
    $$label = $_REQUEST[$label];
    $$label = preg_replace("/\'|\"/", "’", $$label);
    $$label = preg_replace("/\s+/", " ", $$label);
}

if(preg_match("/website|facebook/i", $bktracking) && isset($bkrestaurant) && strlen($bkrestaurant) > 10) {
	$res = new WY_restaurant;
	$res->getRestaurant($bkrestaurant);
	$restaurant_tnc = preg_replace("/\r|\n/", "<br />", $res->restaurant_tnc);;
	}

?>

<html>
<html><head>
<meta http-equiv="content-type" content="text/html; charset=utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
<meta name="HandheldFriendly" content="true" />
<link href='https://fonts.googleapis.com/css?family=Dosis:400,600,700bold|Roboto:400,900,700italic,700,500italic,500,400italic' rel='stylesheet' type='text/css'>
<title>Terms and conditions of services</title>

<style>

body {
font-size:11px;
font-family:Roboto;
margin: 0 0 0 10px;
}


h5 {
color:red;
text-transform:uppercase;
margin: 30px 0 10px 0;
}

</style>
</head>
<body>

<div class="modal-header">
	<button type="button" class="close btn-primary" data-dismiss="modal" aria-hidden="true">&times;</button>
	 <h4 class="modal-title">Terms and Conditions of Service</h4>
</div>	
<div class="modal-body">
<p>
<?php echo $restaurant_tnc; ?>
</p>
</div>

<div class="modal-footer">
	<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
</div>
</body>
</html>
