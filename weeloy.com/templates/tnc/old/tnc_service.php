
<style>
body {
font-family: Roboto; 
font-size: 12px;
}

ol { counter-reset:section; list-style-type:none; }
ol li { list-style-type:none; }
ol li ol { counter-reset:subsection; }
ol li ol li ol { counter-reset:subsubsection; }
ol li:before{
    counter-increment:section;
    content:counter(section) ". ";/*content:"Section " counter(section) ". ";*/
    font-weight:bold;
}
ol li ol li:before {
    counter-increment:subsection;
    content:counter(section) "." counter(subsection) " ";
}

ol li ol li ol li:before {
    counter-increment:subsubsection;
    content:counter(section) "." counter(subsection) "." counter(subsubsection);
}

</style>
    
<div class="container">


<div class=WordSection1>

<p class=MsoNormal align=center style='text-align:center;tab-stops:79.35pt 106.35pt center 226.75pt'><span
class=SpellE><b style='mso-bidi-font-weight:normal'><span lang=FR
style='mso-bidi-font-size:14.0pt;font-family:Arial;color:#595959;mso-themecolor:
text1;mso-themetint:166'>Terms</span></b></span><b style='mso-bidi-font-weight:
normal'><span lang=FR style='mso-bidi-font-size:14.0pt;font-family:Arial;
color:#595959;mso-themecolor:text1;mso-themetint:166'> and Conditions of
Service<o:p></o:p></span></b></p>

<p class=MsoNormal align=center style='text-align:center;tab-stops:79.35pt 106.35pt center 226.75pt'><b
style='mso-bidi-font-weight:normal'><span lang=FR style='mso-bidi-font-size:
14.0pt;font-family:Arial;color:#595959;mso-themecolor:text1;mso-themetint:166'><o:p>&nbsp;</o:p></span></b></p>

<p class=MsoNormal align=center style='text-align:center;tab-stops:79.35pt 106.35pt center 226.75pt'><b
style='mso-bidi-font-weight:normal'><span lang=FR style='mso-bidi-font-size:
14.0pt;font-family:Arial;color:#595959;mso-themecolor:text1;mso-themetint:166'><o:p>&nbsp;</o:p></span></b></p>

<p class=MsoNormal style='margin-bottom:12.0pt;mso-pagination:none;mso-layout-grid-align:
none;text-autospace:none'><span class=GramE><span lang=EN-US style='font-size:
9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;color:#595959;mso-themecolor:
text1;mso-themetint:166;mso-ansi-language:EN-US;mso-fareast-language:EN-US'>Weeloy.com,
Weeloy.Dining.com and its sub-domains (hereinafter collectively referred to as
the &quot;Sites&quot;) are powered by <span class=SpellE>Weeloy</span> Pte</span></span><span
lang=EN-US style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;
color:#595959;mso-themecolor:text1;mso-themetint:166;mso-ansi-language:EN-US;
mso-fareast-language:EN-US'>. <span class=GramE>Ltd. (hereinafter referred to
as &quot;<span class=SpellE><b style='mso-bidi-font-weight:normal'>Weeloy</b></span>&quot;).</span>
<o:p></o:p></span></p>

<p class=MsoNormal style='margin-bottom:12.0pt;mso-pagination:none;mso-layout-grid-align:
none;text-autospace:none'><span class=SpellE><span lang=EN-US style='font-size:
9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;color:#595959;mso-themecolor:
text1;mso-themetint:166;mso-ansi-language:EN-US;mso-fareast-language:EN-US'>Weeloy</span></span><span
lang=EN-US style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;
color:#595959;mso-themecolor:text1;mso-themetint:166;mso-ansi-language:EN-US;
mso-fareast-language:EN-US'> and/or affiliate(s) hereinafter collectively
referred to as “<span class=SpellE><b style='mso-bidi-font-weight:normal'>Weeloy</b></span>”;
if the context requires or permits, “<span class=SpellE>Weeloy</span>” may
refer to any one of <span class=SpellE>Weeloy’s</span> and/or affiliate(s) that
also operate and provide services via, apart from the Sites, other media
platforms or other websites (hereinafter referred to as the
&quot;Platforms&quot;) and applications including mobile applications
(hereinafter referred to as the &quot;Applications&quot;) which are developed
in whole or in part by the <span class=SpellE>Weeloy</span>. <o:p></o:p></span></p>

<p class=MsoNormal style='margin-bottom:12.0pt;mso-pagination:none;mso-layout-grid-align:
none;text-autospace:none'><span lang=EN-US style='font-size:9.0pt;mso-bidi-font-size:
12.0pt;font-family:Arial;color:#595959;mso-themecolor:text1;mso-themetint:166;
mso-ansi-language:EN-US;mso-fareast-language:EN-US'>The Sites, the Platforms
and the Applications may be referred to collectively as the
&quot;Channels&quot; hereinafter. The services provided through the Channels shall
only be available to Users, registered or non-registered, uploading, posting,
viewing, forwarding and/or otherwise using the advertisements, promotional
materials, views, comments and/or other information on the Channels
(hereinafter collectively referred to as the “Users”). <o:p></o:p></span></p>

<p class=MsoNormal style='margin-bottom:12.0pt;mso-pagination:none;mso-layout-grid-align:
none;text-autospace:none'><span lang=EN-US style='font-size:9.0pt;mso-bidi-font-size:
12.0pt;font-family:Arial;color:#595959;mso-themecolor:text1;mso-themetint:166;
mso-ansi-language:EN-US;mso-fareast-language:EN-US'>Access to and use of the
contents and services provided on the Channels shall be subject to the <span
class=SpellE>Weeloy</span> Privacy Policy and the terms and conditions, which
are stated below and hereinafter referred to as the &quot;Terms and
Conditions&quot;. By using the Channels and any other site and/or media
platforms and/or applications accessed through such Channels, Users acknowledge
and agree that the <span class=SpellE>Weeloy</span> Privacy Policy and the
Terms and Conditions set out below are binding upon them. If a User does not
accept either or both of the <span class=SpellE>Weeloy</span> Privacy Policy
and the Terms and Conditions, please do not use the Channels. <o:p></o:p></span></p>

<p class=MsoNormal style='margin-bottom:12.0pt;mso-pagination:none;mso-layout-grid-align:
none;text-autospace:none'><span class=SpellE><span lang=EN-US style='font-size:
9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;color:#595959;mso-themecolor:
text1;mso-themetint:166;mso-ansi-language:EN-US;mso-fareast-language:EN-US'>Weeloy</span></span><span
lang=EN-US style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;
color:#595959;mso-themecolor:text1;mso-themetint:166;mso-ansi-language:EN-US;
mso-fareast-language:EN-US'> reserves the right, at its own discretion, to revise
the <span class=SpellE>Weeloy</span> Privacy Policy and the Terms and
Conditions at any time without prior notice. Once posted on the Channels, the
amended <span class=SpellE>Weeloy</span> Privacy Policy and the Terms and
Conditions shall apply to all Users. Users are advised to visit this page
periodically to review the latest <span class=SpellE>Weeloy</span> Privacy
Policy and the Terms and Conditions. <o:p></o:p></span></p>

<p class=MsoNormal style='margin-bottom:12.0pt;mso-pagination:none;mso-layout-grid-align:
none;text-autospace:none'><span lang=EN-US style='font-size:9.0pt;mso-bidi-font-size:
12.0pt;font-family:Arial;color:#595959;mso-themecolor:text1;mso-themetint:166;
mso-ansi-language:EN-US;mso-fareast-language:EN-US'>Continued use shall
constitute acceptance of all changes and shall remain binding upon the User;
unless User serves his/her notice to <span class=SpellE>Weeloy</span> that such
changes are unacceptable at which time access to the Channels and the Services
(as defined herein) will be terminated. <o:p></o:p></span></p>

<p class=MsoNormal style='margin-bottom:12.0pt;mso-pagination:none;mso-layout-grid-align:
none;text-autospace:none'><span lang=EN-US style='font-size:9.0pt;mso-bidi-font-size:
12.0pt;font-family:Arial;color:#595959;mso-themecolor:text1;mso-themetint:166;
mso-ansi-language:EN-US;mso-fareast-language:EN-US'>By using the Channels,
Users also acknowledge and agree that, if applicable, in addition to being bound
by the Terms and Conditions stated herein, they will at the same time shall be
bound by the terms and conditions of the relevant and applicable Channel(s)
they use and the terms and conditions of the relevant and applicable Channel(s)
operated by the <span class=SpellE>Weeloy</span> which their Materials (as
defined in Section 3.1 below) are being or have been uploaded to or posted on.<o:p></o:p></span></p>

<p class=MsoNormal style='margin-bottom:12.0pt;mso-pagination:none;mso-layout-grid-align:
none;text-autospace:none'><span lang=EN-US style='font-size:9.0pt;mso-bidi-font-size:
12.0pt;font-family:Arial;color:#595959;mso-themecolor:text1;mso-themetint:166;
mso-ansi-language:EN-US;mso-fareast-language:EN-US'>The terms “User&quot; and
“Users” herein refer to all individuals and/or entities accessing and/or using
the Channels at anytime, in any country, for any reason or purpose.<o:p></o:p></span></p>

<p class=MsoListParagraphCxSpFirst style='margin-left:18.0pt;mso-add-space:
auto;text-indent:-18.0pt;mso-pagination:none;mso-list:l10 level1 lfo3;
tab-stops:11.0pt 36.0pt;mso-layout-grid-align:none;text-autospace:none'><![if !supportLists]><span
lang=EN-US style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;
mso-fareast-font-family:Arial;color:#595959;mso-themecolor:text1;mso-themetint:
166;mso-ansi-language:EN-US;mso-fareast-language:EN-US'><span style='mso-list:
Ignore'>1.<span style='font:7.0pt "Times New Roman"'>&nbsp; </span></span></span><![endif]><b><u><span
lang=EN-US style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;
color:#595959;mso-themecolor:text1;mso-themetint:166;mso-ansi-language:EN-US;
mso-fareast-language:EN-US'>General Terms</span></u></b><span lang=EN-US
style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;color:#595959;
mso-themecolor:text1;mso-themetint:166;mso-ansi-language:EN-US;mso-fareast-language:
EN-US'><o:p></o:p></span></p>

<p class=MsoListParagraphCxSpMiddle style='text-indent:-18.0pt;mso-pagination:
none;mso-list:l6 level1 lfo5;tab-stops:11.0pt 36.0pt;mso-layout-grid-align:
none;text-autospace:none'><![if !supportLists]><span lang=EN-US
style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;mso-fareast-font-family:
Arial;color:#595959;mso-themecolor:text1;mso-themetint:166;mso-ansi-language:
EN-US;mso-fareast-language:EN-US'><span style='mso-list:Ignore'>a.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]><span
class=SpellE><span lang=EN-US style='font-size:9.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial;color:#595959;mso-themecolor:text1;mso-themetint:166;
mso-ansi-language:EN-US;mso-fareast-language:EN-US'>Weeloy</span></span><span
lang=EN-US style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;
color:#595959;mso-themecolor:text1;mso-themetint:166;mso-ansi-language:EN-US;
mso-fareast-language:EN-US'> provides online platforms for Users to list the
services or products related to food and beverage which they offer and provides
online marketing (including but not limited to posting advertisements,
promotional campaigns, marketing materials and hosting special events) as well
as providing discussion forums and online platforms for Users to upload, post
and forward their comments and photos related to their dining experiences
(hereinafter referred to as the &quot;Services&quot;).<o:p></o:p></span></p>

<p class=MsoListParagraphCxSpMiddle style='text-indent:-18.0pt;mso-pagination:
none;mso-list:l6 level1 lfo5;tab-stops:47.0pt 72.0pt;mso-layout-grid-align:
none;text-autospace:none'><![if !supportLists]><span lang=EN-US
style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;mso-fareast-font-family:
Arial;color:#595959;mso-themecolor:text1;mso-themetint:166;mso-ansi-language:
EN-US;mso-fareast-language:EN-US'><span style='mso-list:Ignore'>b.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]><span
lang=EN-US style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;
color:#595959;mso-themecolor:text1;mso-themetint:166;mso-ansi-language:EN-US;
mso-fareast-language:EN-US'><span style="mso-spacerun:yes">&nbsp;&nbsp;</span><span
class=SpellE>Weeloy</span> is not a party to nor is it involved in any actual
transaction between Users.<o:p></o:p></span></p>

<p class=MsoListParagraphCxSpLast style='text-indent:-18.0pt;mso-pagination:
none;mso-list:l6 level1 lfo5;tab-stops:47.0pt 72.0pt;mso-layout-grid-align:
none;text-autospace:none'><![if !supportLists]><span lang=EN-US
style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;mso-fareast-font-family:
Arial;color:#595959;mso-themecolor:text1;mso-themetint:166;mso-ansi-language:
EN-US;mso-fareast-language:EN-US'><span style='mso-list:Ignore'>c.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]><span
lang=EN-US style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;
color:#595959;mso-themecolor:text1;mso-themetint:166;mso-ansi-language:EN-US;
mso-fareast-language:EN-US'><span style="mso-spacerun:yes">&nbsp;&nbsp;</span><span
class=SpellE>Weeloy</span> is committed to protect the privacy of the Users. <span
class=SpellE>Weeloy</span> uses the information of the Users according to the
terms as stated in the <span class=SpellE>Weeloy</span> Privacy Policy <o:p></o:p></span></p>

<p class=MsoNormal style='mso-pagination:none;tab-stops:47.0pt 72.0pt;
mso-layout-grid-align:none;text-autospace:none'><span lang=EN-US
style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;color:#595959;
mso-themecolor:text1;mso-themetint:166;mso-ansi-language:EN-US;mso-fareast-language:
EN-US'><o:p>&nbsp;</o:p></span></p>

<p class=MsoListParagraph style='margin-left:18.0pt;mso-add-space:auto;
text-indent:-18.0pt;mso-pagination:none;mso-list:l10 level1 lfo3;tab-stops:
11.0pt 36.0pt;mso-layout-grid-align:none;text-autospace:none'><![if !supportLists]><span
lang=EN-US style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;
mso-fareast-font-family:Arial;color:#595959;mso-themecolor:text1;mso-themetint:
166;mso-ansi-language:EN-US;mso-fareast-language:EN-US'><span style='mso-list:
Ignore'>2.<span style='font:7.0pt "Times New Roman"'>&nbsp; </span></span></span><![endif]><b><u><span
lang=EN-US style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;
color:#595959;mso-themecolor:text1;mso-themetint:166;mso-ansi-language:EN-US;
mso-fareast-language:EN-US'>Prohibited Uses for all Users</span></u></b><span
lang=EN-US style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;
color:#595959;mso-themecolor:text1;mso-themetint:166;mso-ansi-language:EN-US;
mso-fareast-language:EN-US'><o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:11.0pt;mso-pagination:none;tab-stops:
11.0pt 36.0pt;mso-layout-grid-align:none;text-autospace:none'><span lang=EN-US
style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;color:#595959;
mso-themecolor:text1;mso-themetint:166;mso-ansi-language:EN-US;mso-fareast-language:
EN-US'>Users of the Channels, registered or non-registered, agree not to use
any of the Channels for any of the following purposes which are expressly
prohibited: <o:p></o:p></span></p>

<p class=MsoListParagraphCxSpFirst style='margin-left:29.0pt;mso-add-space:
auto;text-indent:-18.0pt;mso-pagination:none;mso-list:l1 level1 lfo8;
tab-stops:83.0pt 108.0pt;mso-layout-grid-align:none;text-autospace:none'><![if !supportLists]><span
lang=EN-US style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;
mso-fareast-font-family:Arial;color:#595959;mso-themecolor:text1;mso-themetint:
166;mso-ansi-language:EN-US;mso-fareast-language:EN-US'><span style='mso-list:
Ignore'>a.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]><span
lang=EN-US style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;
color:#595959;mso-themecolor:text1;mso-themetint:166;mso-ansi-language:EN-US;
mso-fareast-language:EN-US'>All Users are prohibited from violating or
attempting to violate the security of the Channels including, without limitation,
accessing data not intended for them or logging into a server or account which
they are not authorized to access, attempting to probe, scan or test the
vulnerability of a system or network or attempting to breach security or
authentication measures without proper authorization, attempting to interfere
with service to any user, host or network or sending unsolicited e-mail.
Violation of system or network security may result in civil and/or criminal
liabilities.<o:p></o:p></span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:29.0pt;mso-add-space:
auto;text-indent:-18.0pt;mso-pagination:none;mso-list:l1 level1 lfo8;
tab-stops:83.0pt 108.0pt;mso-layout-grid-align:none;text-autospace:none'><![if !supportLists]><span
lang=EN-US style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;
mso-fareast-font-family:Arial;color:#595959;mso-themecolor:text1;mso-themetint:
166;mso-ansi-language:EN-US;mso-fareast-language:EN-US'><span style='mso-list:
Ignore'>b.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]><span
lang=EN-US style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;
color:#595959;mso-themecolor:text1;mso-themetint:166;mso-ansi-language:EN-US;
mso-fareast-language:EN-US'>A User shall not delete or revise any material or
information posted by any other Users.<o:p></o:p></span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:29.0pt;mso-add-space:
auto;text-indent:-18.0pt;mso-pagination:none;mso-list:l1 level1 lfo8;
tab-stops:83.0pt 108.0pt;mso-layout-grid-align:none;text-autospace:none'><![if !supportLists]><span
lang=EN-US style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;
mso-fareast-font-family:Arial;color:#595959;mso-themecolor:text1;mso-themetint:
166;mso-ansi-language:EN-US;mso-fareast-language:EN-US'><span style='mso-list:
Ignore'>c.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]><span
lang=EN-US style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;
color:#595959;mso-themecolor:text1;mso-themetint:166;mso-ansi-language:EN-US;
mso-fareast-language:EN-US'>All Users shall not use the Channels (1) for
uploading, posting, publishing, transmitting, distributing, circulating or
storing material in violation of any applicable laws or regulations; or (2) in
any manner that will infringe the copyright, trademark, trade secrets or other
intellectual property rights of others or violate the privacy or publicity or
other personal rights of others; or (3) in any manner that is harmful,
defamatory, libelous, obscene, discriminatory, harassing, threatening, abusive,
hateful or is otherwise offensive or objectionable. In particular, all Users
shall not print, download, duplicate or otherwise copy or use any personally
identifiable information about other Users (if any). All unsolicited
communications of any type to Users are strictly prohibited.<o:p></o:p></span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:29.0pt;mso-add-space:
auto;text-indent:-18.0pt;mso-pagination:none;mso-list:l1 level1 lfo8;
tab-stops:83.0pt 108.0pt;mso-layout-grid-align:none;text-autospace:none'><![if !supportLists]><span
lang=EN-US style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;
mso-fareast-font-family:Arial;color:#595959;mso-themecolor:text1;mso-themetint:
166;mso-ansi-language:EN-US;mso-fareast-language:EN-US'><span style='mso-list:
Ignore'>d.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]><span
lang=EN-US style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;
color:#595959;mso-themecolor:text1;mso-themetint:166;mso-ansi-language:EN-US;
mso-fareast-language:EN-US'>Users shall not use the Channels if they do not
have legal capacity to form legally binding contracts.<o:p></o:p></span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:29.0pt;mso-add-space:
auto;text-indent:-18.0pt;mso-pagination:none;mso-list:l1 level1 lfo8;
tab-stops:83.0pt 108.0pt;mso-layout-grid-align:none;text-autospace:none'><![if !supportLists]><span
lang=EN-US style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;
mso-fareast-font-family:Arial;color:#595959;mso-themecolor:text1;mso-themetint:
166;mso-ansi-language:EN-US;mso-fareast-language:EN-US'><span style='mso-list:
Ignore'>e.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]><span
lang=EN-US style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;
color:#595959;mso-themecolor:text1;mso-themetint:166;mso-ansi-language:EN-US;
mso-fareast-language:EN-US'>Users shall not upload or post any advertisement or
materials on the Channels which contains any false, inaccurate, misleading or
libelous content or contains any computer viruses, <span class=SpellE>trojan</span>
horses, worms, computed files or other materials that may interrupt, damage or
limit the functionality of any computer software or hardware or telecommunication
equipment. Also, the advertisement shall not be fraudulent or involve sale of
illegal products.<o:p></o:p></span></p>

<p class=MsoListParagraphCxSpLast style='margin-left:29.0pt;mso-add-space:auto;
text-indent:-18.0pt;mso-pagination:none;mso-list:l1 level1 lfo8;tab-stops:83.0pt 108.0pt;
mso-layout-grid-align:none;text-autospace:none'><![if !supportLists]><span
lang=EN-US style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;
mso-fareast-font-family:Arial;color:#595959;mso-themecolor:text1;mso-themetint:
166;mso-ansi-language:EN-US;mso-fareast-language:EN-US'><span style='mso-list:
Ignore'>f.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span><![endif]><span lang=EN-US style='font-size:9.0pt;
mso-bidi-font-size:12.0pt;font-family:Arial;color:#595959;mso-themecolor:text1;
mso-themetint:166;mso-ansi-language:EN-US;mso-fareast-language:EN-US'>Users
shall not engage in spamming, including but not limited to any form of
emailing, posting or messaging that is unsolicited.<o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:108.0pt;mso-pagination:none;tab-stops:
11.0pt 36.0pt;mso-layout-grid-align:none;text-autospace:none'><b><span
lang=EN-US style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;
color:#595959;mso-themecolor:text1;mso-themetint:166;mso-ansi-language:EN-US;
mso-fareast-language:EN-US'><o:p>&nbsp;</o:p></span></b></p>

<p class=MsoListParagraph style='margin-left:18.0pt;mso-add-space:auto;
text-indent:-18.0pt;mso-pagination:none;mso-list:l10 level1 lfo3;tab-stops:
11.0pt 36.0pt;mso-layout-grid-align:none;text-autospace:none'><![if !supportLists]><span
lang=EN-US style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;
mso-fareast-font-family:Arial;color:#595959;mso-themecolor:text1;mso-themetint:
166;mso-ansi-language:EN-US;mso-fareast-language:EN-US'><span style='mso-list:
Ignore'>3.<span style='font:7.0pt "Times New Roman"'>&nbsp; </span></span></span><![endif]><b><u><span
lang=EN-US style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;
color:#595959;mso-themecolor:text1;mso-themetint:166;mso-ansi-language:EN-US;
mso-fareast-language:EN-US'>Acceptable uses of the Channels</span></u></b><span
lang=EN-US style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;
color:#595959;mso-themecolor:text1;mso-themetint:166;mso-ansi-language:EN-US;
mso-fareast-language:EN-US'><o:p></o:p></span></p>

<p class=MsoNormal style='mso-pagination:none;tab-stops:47.0pt 72.0pt;
mso-layout-grid-align:none;text-autospace:none'><span lang=EN-US
style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;color:#595959;
mso-themecolor:text1;mso-themetint:166;mso-ansi-language:EN-US;mso-fareast-language:
EN-US'>Specific uses - User(s) uploading or posting advertisements, photos,
content, views, comments, messages and/or other information on the Channels
(hereinafter collectively referred to as the “Material(s)”). &#8232;Such
User(s) uploading or posting the Materials shall hereinafter be referred to as
the &quot;Posting User(s)&quot;.<o:p></o:p></span></p>

<p class=MsoListParagraphCxSpFirst style='margin-left:29.0pt;mso-add-space:
auto;text-indent:-18.0pt;mso-pagination:none;mso-list:l8 level1 lfo9;
tab-stops:83.0pt 108.0pt;mso-layout-grid-align:none;text-autospace:none'><![if !supportLists]><span
lang=EN-US style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;
mso-fareast-font-family:Arial;color:#595959;mso-themecolor:text1;mso-themetint:
166;mso-ansi-language:EN-US;mso-fareast-language:EN-US'><span style='mso-list:
Ignore'>a.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]><span
lang=EN-US style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;
color:#595959;mso-themecolor:text1;mso-themetint:166;mso-ansi-language:EN-US;
mso-fareast-language:EN-US'>The Posting User agrees that he/she/it shall only
use the Channels for lawful purposes and for enjoying the Services provided
through the Channels. <span class=SpellE>Weeloy</span> reserves the right to
edit, share, reject, disapprove, erase and delete any Materials posted on the
Channels as it sees appropriate.<o:p></o:p></span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:29.0pt;mso-add-space:
auto;text-indent:-18.0pt;mso-pagination:none;mso-list:l8 level1 lfo9;
tab-stops:83.0pt 108.0pt;mso-layout-grid-align:none;text-autospace:none'><![if !supportLists]><span
lang=EN-US style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;
mso-fareast-font-family:Arial;color:#595959;mso-themecolor:text1;mso-themetint:
166;mso-ansi-language:EN-US;mso-fareast-language:EN-US'><span style='mso-list:
Ignore'>b.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]><span
lang=EN-US style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;
color:#595959;mso-themecolor:text1;mso-themetint:166;mso-ansi-language:EN-US;
mso-fareast-language:EN-US'>In the event that the Posting User is an
individual, he/she shall not post his/her identity card and/or passport number
on the Channels.<o:p></o:p></span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:29.0pt;mso-add-space:
auto;text-indent:-18.0pt;mso-pagination:none;mso-list:l8 level1 lfo9;
tab-stops:83.0pt 108.0pt;mso-layout-grid-align:none;text-autospace:none'><![if !supportLists]><span
lang=EN-US style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;
mso-fareast-font-family:Arial;color:#595959;mso-themecolor:text1;mso-themetint:
166;mso-ansi-language:EN-US;mso-fareast-language:EN-US'><span style='mso-list:
Ignore'>c.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]><span
lang=EN-US style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;
color:#595959;mso-themecolor:text1;mso-themetint:166;mso-ansi-language:EN-US;
mso-fareast-language:EN-US'>Although <span class=SpellE>Weeloy</span> shall use
its reasonable endeavors to restrict access to the database of the Posting
Users’ personal data only to the personnel of <span class=SpellE>Weeloy</span>,
<span class=SpellE>Weeloy</span> does not guarantee that other parties will
not, without <span class=SpellE>Weeloy’s</span> consent, gain access to such
database. For the usage and protection of personal data provided by the Posting
Users, please refer to the <span class=SpellE>Weeloy</span> Privacy Policy.<o:p></o:p></span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:29.0pt;mso-add-space:
auto;text-indent:-18.0pt;mso-pagination:none;mso-list:l8 level1 lfo9;
tab-stops:83.0pt 108.0pt;mso-layout-grid-align:none;text-autospace:none'><![if !supportLists]><span
lang=EN-US style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;
mso-fareast-font-family:Arial;color:#595959;mso-themecolor:text1;mso-themetint:
166;mso-ansi-language:EN-US;mso-fareast-language:EN-US'><span style='mso-list:
Ignore'>d.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]><span
lang=EN-US style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;
color:#595959;mso-themecolor:text1;mso-themetint:166;mso-ansi-language:EN-US;
mso-fareast-language:EN-US'>Users who upload or post Materials on the Channels
shall be solely responsible for the Materials uploaded, posted or shared by
them and/or any web pages and/or media platforms and/or applications linked to
the Channels posted by them. <span class=SpellE>Weeloy</span> reserves the
right to edit, share, reject, erase, remove and delete any Materials and links
to web pages and/or media platforms and/or applications as it sees appropriate.
<o:p></o:p></span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:29.0pt;mso-add-space:
auto;text-indent:-18.0pt;mso-pagination:none;mso-list:l8 level1 lfo9;
tab-stops:83.0pt 108.0pt;mso-layout-grid-align:none;text-autospace:none'><![if !supportLists]><span
lang=EN-US style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;
mso-fareast-font-family:Arial;color:#595959;mso-themecolor:text1;mso-themetint:
166;mso-ansi-language:EN-US;mso-fareast-language:EN-US'><span style='mso-list:
Ignore'>e.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]><span
class=SpellE><span lang=EN-US style='font-size:9.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial;color:#595959;mso-themecolor:text1;mso-themetint:166;
mso-ansi-language:EN-US;mso-fareast-language:EN-US'>Weeloy</span></span><span
lang=EN-US style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;
color:#595959;mso-themecolor:text1;mso-themetint:166;mso-ansi-language:EN-US;
mso-fareast-language:EN-US'> shall have the right to terminate any services to
any Posting Users at its sole discretion. If a User uploads or posts Materials
on the Channels and subsequently deletes and/or removes the same or the User
terminates his/her/its accounts, or that <span class=SpellE>Weeloy</span>
deletes any and/or removes such uploaded or posted Materials, such Materials
will no longer be accessible by the User who uploaded or posted the same via
that User’s account; however, such deleted Materials may still persist and appear
on any part of the Channels, and/or be used in any form by <span class=SpellE>Weeloy</span>.<o:p></o:p></span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:29.0pt;mso-add-space:
auto;text-indent:-18.0pt;mso-pagination:none;mso-list:l8 level1 lfo9;
tab-stops:83.0pt 108.0pt;mso-layout-grid-align:none;text-autospace:none'><![if !supportLists]><span
lang=EN-US style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;
mso-fareast-font-family:Arial;color:#595959;mso-themecolor:text1;mso-themetint:
166;mso-ansi-language:EN-US;mso-fareast-language:EN-US'><span style='mso-list:
Ignore'>f.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span><![endif]><span class=SpellE><span lang=EN-US
style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;color:#595959;
mso-themecolor:text1;mso-themetint:166;mso-ansi-language:EN-US;mso-fareast-language:
EN-US'>Weeloy</span></span><span lang=EN-US style='font-size:9.0pt;mso-bidi-font-size:
12.0pt;font-family:Arial;color:#595959;mso-themecolor:text1;mso-themetint:166;
mso-ansi-language:EN-US;mso-fareast-language:EN-US'> reserves the right to
request any User to cease using or to change his/her/its username immediately
upon notice given to the relevant user without giving any reason as and when <span
class=SpellE>Weeloy</span> deems appropriate to do so; if any User disagrees
and refuses to abide by such request made by <span class=SpellE>Weeloy</span>, <span
class=SpellE>Weeloy</span> may at any time at its sole discretion, deactivate
that User’s account without prior notification to that User and without
prejudice to all <span class=SpellE>Weeloy’s</span> other rights and remedies.<o:p></o:p></span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:29.0pt;mso-add-space:
auto;text-indent:-18.0pt;mso-pagination:none;mso-list:l8 level1 lfo9;
tab-stops:83.0pt 108.0pt;mso-layout-grid-align:none;text-autospace:none'><![if !supportLists]><span
lang=EN-US style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;
mso-fareast-font-family:Arial;color:#595959;mso-themecolor:text1;mso-themetint:
166;mso-ansi-language:EN-US;mso-fareast-language:EN-US'><span style='mso-list:
Ignore'>g.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]><span
lang=EN-US style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;
color:#595959;mso-themecolor:text1;mso-themetint:166;mso-ansi-language:EN-US;
mso-fareast-language:EN-US'>This paragraph shall only be applicable to Users
posting advertisements, promotional and marketing materials (“Advertiser(s)”)<span
class=GramE>:-</span> &#8232;<o:p></o:p></span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:65.0pt;mso-add-space:
auto;text-indent:-65.0pt;mso-text-indent-alt:-9.0pt;mso-pagination:none;
mso-list:l8 level3 lfo9;tab-stops:83.0pt 108.0pt;mso-layout-grid-align:none;
text-autospace:none'><![if !supportLists]><span lang=EN-US style='font-size:
9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;mso-fareast-font-family:Arial;
color:#595959;mso-themecolor:text1;mso-themetint:166;mso-ansi-language:EN-US;
mso-fareast-language:EN-US'><span style='mso-list:Ignore'><span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span>i.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]><span
lang=EN-US style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;
color:#595959;mso-themecolor:text1;mso-themetint:166;mso-ansi-language:EN-US;
mso-fareast-language:EN-US'>Upon payment of a service fee to <span
class=SpellE>Weeloy</span> or upon acceptance of any free trial promotion offer,
Users will be entitled to use the Sites and/or Platforms and/or Applications
(as the case may be) to post advertisements and promotional materials (subject
to the Terms and Conditions and any specific terms and conditions of any
service agreement(s) entered into between <span class=SpellE>Weeloy</span> and
the User(s), and in the event of any conflict between the two, the latter shall
prevail).<o:p></o:p></span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:65.0pt;mso-add-space:
auto;text-indent:-65.0pt;mso-text-indent-alt:-9.0pt;mso-pagination:none;
mso-list:l8 level3 lfo9;tab-stops:83.0pt 108.0pt;mso-layout-grid-align:none;
text-autospace:none'><![if !supportLists]><span lang=EN-US style='font-size:
9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;mso-fareast-font-family:Arial;
color:#595959;mso-themecolor:text1;mso-themetint:166;mso-ansi-language:EN-US;
mso-fareast-language:EN-US'><span style='mso-list:Ignore'><span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span>ii.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]><span
class=SpellE><span lang=EN-US style='font-size:9.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial;color:#595959;mso-themecolor:text1;mso-themetint:166;
mso-ansi-language:EN-US;mso-fareast-language:EN-US'>Weeloy</span></span><span
lang=EN-US style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;
color:#595959;mso-themecolor:text1;mso-themetint:166;mso-ansi-language:EN-US;
mso-fareast-language:EN-US'> also reserves the right to change the service fee
or institute new charges or fees to be paid by Advertisers for posting
advertisements and promotional materials on any of the Channels, as it deems
appropriate. <o:p></o:p></span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:65.0pt;mso-add-space:
auto;text-indent:-65.0pt;mso-text-indent-alt:-9.0pt;mso-pagination:none;
mso-list:l8 level3 lfo9;tab-stops:83.0pt 108.0pt;mso-layout-grid-align:none;
text-autospace:none'><![if !supportLists]><span lang=EN-US style='font-size:
9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;mso-fareast-font-family:Arial;
color:#595959;mso-themecolor:text1;mso-themetint:166;mso-ansi-language:EN-US;
mso-fareast-language:EN-US'><span style='mso-list:Ignore'><span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span>iii.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]><span
lang=EN-US style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;
color:#595959;mso-themecolor:text1;mso-themetint:166;mso-ansi-language:EN-US;
mso-fareast-language:EN-US'>In the event that any Advertiser posting
advertisements and promotional materials fails to pay the service fee or any
other fees or charges due to the <span class=SpellE>Weeloy</span>, <span
class=SpellE>Weeloy</span> reserves the right to suspend or terminate that
Advertiser’s user account, advertisements and links to web pages and/or media platforms
and/or applications without prejudice to all its other rights and remedies.<o:p></o:p></span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:29.0pt;mso-add-space:
auto;text-indent:-18.0pt;mso-pagination:none;mso-list:l8 level1 lfo9;
tab-stops:83.0pt 108.0pt;mso-layout-grid-align:none;text-autospace:none'><![if !supportLists]><span
lang=EN-US style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;
mso-fareast-font-family:Arial;color:#595959;mso-themecolor:text1;mso-themetint:
166;mso-ansi-language:EN-US;mso-fareast-language:EN-US'><span style='mso-list:
Ignore'>h.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]><span
lang=EN-US style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;
color:#595959;mso-themecolor:text1;mso-themetint:166;mso-ansi-language:EN-US;
mso-fareast-language:EN-US'>This paragraph shall only be applicable to the use
of the Applications<span class=GramE>:-</span> &#8232;By using the
Applications, Users acknowledge and agree to the following:-<o:p></o:p></span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:65.0pt;mso-add-space:
auto;text-indent:-65.0pt;mso-text-indent-alt:-9.0pt;mso-pagination:none;
mso-list:l8 level3 lfo9;tab-stops:83.0pt 108.0pt;mso-layout-grid-align:none;
text-autospace:none'><![if !supportLists]><span lang=EN-US style='font-size:
9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;mso-fareast-font-family:Arial;
color:#595959;mso-themecolor:text1;mso-themetint:166;mso-ansi-language:EN-US;
mso-fareast-language:EN-US'><span style='mso-list:Ignore'><span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span>i.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]><span
lang=EN-US style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;
color:#595959;mso-themecolor:text1;mso-themetint:166;mso-ansi-language:EN-US;
mso-fareast-language:EN-US'>If Users use the Applications to upload, post and
share materials, including but not limited to instantly uploading, posting and
sharing photos taken, the Users are consenting to the Materials being shared;<o:p></o:p></span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:65.0pt;mso-add-space:
auto;text-indent:-65.0pt;mso-text-indent-alt:-9.0pt;mso-pagination:none;
mso-list:l8 level3 lfo9;tab-stops:83.0pt 108.0pt;mso-layout-grid-align:none;
text-autospace:none'><![if !supportLists]><span lang=EN-US style='font-size:
9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;mso-fareast-font-family:Arial;
color:#595959;mso-themecolor:text1;mso-themetint:166;mso-ansi-language:EN-US;
mso-fareast-language:EN-US'><span style='mso-list:Ignore'><span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span>ii.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]><span
lang=EN-US style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;
color:#595959;mso-themecolor:text1;mso-themetint:166;mso-ansi-language:EN-US;
mso-fareast-language:EN-US'>The Users’ use of the Applications may cause
personally identifying information to be publicly disclosed and/or associated
with the relevant Users, even if <span class=SpellE>Weeloy</span> has not
itself provided such information; and<o:p></o:p></span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:65.0pt;mso-add-space:
auto;text-indent:-65.0pt;mso-text-indent-alt:-9.0pt;mso-pagination:none;
mso-list:l8 level3 lfo9;tab-stops:83.0pt 108.0pt;mso-layout-grid-align:none;
text-autospace:none'><![if !supportLists]><span lang=EN-US style='font-size:
9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;mso-fareast-font-family:Arial;
color:#595959;mso-themecolor:text1;mso-themetint:166;mso-ansi-language:EN-US;
mso-fareast-language:EN-US'><span style='mso-list:Ignore'><span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span>iii.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]><span
lang=EN-US style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;
color:#595959;mso-themecolor:text1;mso-themetint:166;mso-ansi-language:EN-US;
mso-fareast-language:EN-US'>The Users shall use the Applications at their own
option and risk and at their own accord. The Users will hold <span
class=SpellE>Weeloy</span> harmless for activities related to their use of the
Applications.<o:p></o:p></span></p>

<p class=MsoListParagraphCxSpLast style='margin-left:29.0pt;mso-add-space:auto;
text-indent:-18.0pt;mso-pagination:none;mso-list:l8 level1 lfo9;tab-stops:83.0pt 108.0pt;
mso-layout-grid-align:none;text-autospace:none'><![if !supportLists]><span
lang=EN-US style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;
mso-fareast-font-family:Arial;color:#595959;mso-themecolor:text1;mso-themetint:
166;mso-ansi-language:EN-US;mso-fareast-language:EN-US'><span style='mso-list:
Ignore'>i.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span><![endif]><span lang=EN-US style='font-size:9.0pt;
mso-bidi-font-size:12.0pt;font-family:Arial;color:#595959;mso-themecolor:text1;
mso-themetint:166;mso-ansi-language:EN-US;mso-fareast-language:EN-US'>Specific
uses - User(s) viewing the Materials posted on the Channels<o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:29.0pt;mso-pagination:none;tab-stops:
83.0pt 108.0pt;mso-layout-grid-align:none;text-autospace:none'><span
lang=EN-US style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;
color:#595959;mso-themecolor:text1;mso-themetint:166;mso-ansi-language:EN-US;
mso-fareast-language:EN-US'>(<span class=GramE>hereinafter</span> referred to
as the &quot;Viewer(s)&quot;)&#8232;The Viewer agrees that he/she/it shall only
use the Channels for lawful purposes and for enjoying the Services provided
therein. The Viewer agrees that any personal data received from the Channels or
<span class=SpellE>Weeloy</span> shall only be used for the purpose of
identifying and/or locating advertisements or materials or any content therein
or for the purpose of enjoying the Services provided through the Channels. Any
personal data received which are irrelevant to the above purposes shall be
disregarded and shall not be saved, stored, collected, processed, used,
distributed, published, disclosed or transmitted in any way, including but not
limited to for any commercial purpose. The Viewer also agrees that any personal
data collected from the Channels or <span class=SpellE>Weeloy</span> shall be
promptly and properly deleted when the above purposes have lapsed or been
achieved. <span class=SpellE>Weeloy</span> shall not be responsible or held
liable in any way if any Users, in breach of the Terms and Conditions or the
terms and conditions of any relevant and applicable Channel(s), in any country,
use the other Users’ personal data, information or materials (whether obtained
from the Channels or not) for any purpose. <o:p></o:p></span></p>

<p class=MsoListParagraphCxSpFirst style='margin-left:29.0pt;mso-add-space:
auto;text-indent:-18.0pt;mso-pagination:none;mso-list:l8 level1 lfo9;
tab-stops:83.0pt 108.0pt;mso-layout-grid-align:none;text-autospace:none'><![if !supportLists]><span
lang=EN-US style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;
mso-fareast-font-family:Arial;color:#595959;mso-themecolor:text1;mso-themetint:
166;mso-ansi-language:EN-US;mso-fareast-language:EN-US'><span style='mso-list:
Ignore'>j.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span><![endif]><span lang=EN-US style='font-size:9.0pt;
mso-bidi-font-size:12.0pt;font-family:Arial;color:#595959;mso-themecolor:text1;
mso-themetint:166;mso-ansi-language:EN-US;mso-fareast-language:EN-US'>All Users
accept that all personal data, information or materials provided by them
publicly on the Channels are voluntarily provided and are given entirely at
their own risk. The <span class=SpellE>Weeloy</span> shall not bear the
responsibility of protecting the personal data, information or materials so
provided publicly therein.<o:p></o:p></span></p>

<p class=MsoListParagraphCxSpLast style='margin-left:29.0pt;mso-add-space:auto;
text-indent:-18.0pt;mso-pagination:none;mso-list:l8 level1 lfo9;tab-stops:83.0pt 108.0pt;
mso-layout-grid-align:none;text-autospace:none'><![if !supportLists]><span
lang=EN-US style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;
mso-fareast-font-family:Arial;color:#595959;mso-themecolor:text1;mso-themetint:
166;mso-ansi-language:EN-US;mso-fareast-language:EN-US'><span style='mso-list:
Ignore'>k.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]><span
lang=EN-US style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;
color:#595959;mso-themecolor:text1;mso-themetint:166;mso-ansi-language:EN-US;
mso-fareast-language:EN-US'>Specific uses - User(s) using online restaurant
reservation service. For any User using online restaurant reservation service
via </span><span lang=FR><a href="http://www.weeloy.com"><span lang=EN-US
style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;color:#595959;
mso-themecolor:text1;mso-themetint:166;mso-ansi-language:EN-US;mso-fareast-language:
EN-US'>www.weeloy.com</span></a></span><span class=GramE><span
class=MsoHyperlink><span lang=EN-US style='font-size:9.0pt;mso-bidi-font-size:
12.0pt;font-family:Arial;color:#595959;mso-themecolor:text1;mso-themetint:166;
mso-ansi-language:EN-US;mso-fareast-language:EN-US;text-decoration:none;
text-underline:none'> </span></span><span lang=EN-US style='font-size:9.0pt;
mso-bidi-font-size:12.0pt;font-family:Arial;color:#595959;mso-themecolor:text1;
mso-themetint:166;mso-ansi-language:EN-US;mso-fareast-language:EN-US'>which</span></span><span
lang=EN-US style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;
color:#595959;mso-themecolor:text1;mso-themetint:166;mso-ansi-language:EN-US;
mso-fareast-language:EN-US'> is operated by the <span class=SpellE>Weeloy</span>,
the User is deemed to have read, understood and accepted the Terms and
Conditions before using the said service.<o:p></o:p></span></p>

<p class=MsoNormal style='mso-pagination:none;tab-stops:47.0pt 72.0pt;
mso-layout-grid-align:none;text-autospace:none'><span lang=EN-US
style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;color:#595959;
mso-themecolor:text1;mso-themetint:166;mso-ansi-language:EN-US;mso-fareast-language:
EN-US'><o:p>&nbsp;</o:p></span></p>

<p class=MsoListParagraph style='margin-left:18.0pt;mso-add-space:auto;
text-indent:-18.0pt;mso-pagination:none;mso-list:l10 level1 lfo3;tab-stops:
47.0pt 72.0pt;mso-layout-grid-align:none;text-autospace:none'><![if !supportLists]><span
lang=EN-US style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;
mso-fareast-font-family:Arial;color:#595959;mso-themecolor:text1;mso-themetint:
166;mso-ansi-language:EN-US;mso-fareast-language:EN-US'><span style='mso-list:
Ignore'>4.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]><b><u><span
lang=EN-US style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;
color:#595959;mso-themecolor:text1;mso-themetint:166;mso-ansi-language:EN-US;
mso-fareast-language:EN-US'>Content License</span></u></b><span lang=EN-US
style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;color:#595959;
mso-themecolor:text1;mso-themetint:166;mso-ansi-language:EN-US;mso-fareast-language:
EN-US'><o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:18.0pt;mso-pagination:none;tab-stops:
47.0pt 72.0pt;mso-layout-grid-align:none;text-autospace:none'><span lang=EN-US
style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;color:#595959;
mso-themecolor:text1;mso-themetint:166;mso-ansi-language:EN-US;mso-fareast-language:
EN-US'>By uploading or posting Materials on the Channels, the User
unconditionally grants <span class=SpellE>Weeloy</span> a non-exclusive,
worldwide, irrevocable, royalty-free right to exercise the copyright, publicity
and database rights (but no other rights) he/she/it has in the Materials in
order that <span class=SpellE>Weeloy</span> can use, publish, host, display,
promote, copy, download, forward, distribute, reproduce, transfer, edit, sell
and re-use the Materials in any form and anywhere, with or without making any
commercial gains or profits, and carry out the purposes set out in the <span
class=SpellE>Weeloy</span> Privacy Policy and herein.<o:p></o:p></span></p>

<p class=MsoNormal style='text-indent:36.0pt;mso-pagination:none;tab-stops:
11.0pt 36.0pt;mso-layout-grid-align:none;text-autospace:none'><b><span
lang=EN-US style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;
color:#595959;mso-themecolor:text1;mso-themetint:166;mso-ansi-language:EN-US;
mso-fareast-language:EN-US'><o:p>&nbsp;</o:p></span></b></p>

<p class=MsoListParagraph style='margin-left:18.0pt;mso-add-space:auto;
text-indent:-18.0pt;mso-pagination:none;mso-list:l10 level1 lfo3;tab-stops:
11.0pt 36.0pt;mso-layout-grid-align:none;text-autospace:none'><![if !supportLists]><span
lang=EN-US style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;
mso-fareast-font-family:Arial;color:#595959;mso-themecolor:text1;mso-themetint:
166;mso-ansi-language:EN-US;mso-fareast-language:EN-US'><span style='mso-list:
Ignore'>5.<span style='font:7.0pt "Times New Roman"'>&nbsp; </span></span></span><![endif]><b><span
lang=EN-US style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;
color:#595959;mso-themecolor:text1;mso-themetint:166;mso-ansi-language:EN-US;
mso-fareast-language:EN-US'><span style="mso-spacerun:yes">&nbsp;&nbsp;</span><u>Intellectual
Property Rights</u></span></b><span lang=EN-US style='font-size:9.0pt;
mso-bidi-font-size:12.0pt;font-family:Arial;color:#595959;mso-themecolor:text1;
mso-themetint:166;mso-ansi-language:EN-US;mso-fareast-language:EN-US'><o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:18.0pt;mso-pagination:none;tab-stops:
47.0pt 72.0pt;mso-layout-grid-align:none;text-autospace:none'><span lang=EN-US
style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;color:#595959;
mso-themecolor:text1;mso-themetint:166;mso-ansi-language:EN-US;mso-fareast-language:
EN-US'>All contents of the Channels, including without limitation the text,
images, information, comments, layout, database, graphics, photos, pictures,
sounds or audio formats, software, brands and HTML are the intellectual
properties of <span class=SpellE>Weeloy</span> or the Users (as the case may
be) which are protected by applicable copyright and trademark laws and may not
be downloaded or otherwise duplicated without the express written permission of
<span class=SpellE>Weeloy</span> or the Users (as the case may be). Re-use of
any of the foregoing is strictly prohibited and <span class=SpellE>Weeloy</span>
reserves all its rights. Any use of any of such content other than those
permitted under the Terms and Conditions, the terms and conditions of any
specific Channel(s) and the terms and conditions of any service agreement(s)
entered into between <span class=SpellE>Weeloy</span> and the User(s) is
strictly prohibited and <span class=SpellE>Weeloy</span> reserves all its
rights in this respect. For the avoidance of doubt, any purported consent of
any third parties on the use of the contents and materials mentioned under this
Clause shall not exonerate the Users from the restrictions/prohibitions imposed
hereunder in whatsoever manner.<o:p></o:p></span></p>

<p class=MsoNormal style='text-indent:36.0pt;mso-pagination:none;tab-stops:
11.0pt 36.0pt;mso-layout-grid-align:none;text-autospace:none'><b><span
lang=EN-US style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;
color:#595959;mso-themecolor:text1;mso-themetint:166;mso-ansi-language:EN-US;
mso-fareast-language:EN-US'><o:p>&nbsp;</o:p></span></b></p>

<p class=MsoListParagraphCxSpFirst style='margin-left:18.0pt;mso-add-space:
auto;text-indent:-18.0pt;mso-pagination:none;mso-list:l10 level1 lfo3;
tab-stops:11.0pt 36.0pt;mso-layout-grid-align:none;text-autospace:none'><![if !supportLists]><span
lang=EN-US style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;
mso-fareast-font-family:Arial;color:#595959;mso-themecolor:text1;mso-themetint:
166;mso-ansi-language:EN-US;mso-fareast-language:EN-US'><span style='mso-list:
Ignore'>6.<span style='font:7.0pt "Times New Roman"'>&nbsp; </span></span></span><![endif]><b><span
lang=EN-US style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;
color:#595959;mso-themecolor:text1;mso-themetint:166;mso-ansi-language:EN-US;
mso-fareast-language:EN-US'><span style="mso-spacerun:yes">&nbsp;&nbsp;</span><u>Contents</u></span></b><span
lang=EN-US style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;
color:#595959;mso-themecolor:text1;mso-themetint:166;mso-ansi-language:EN-US;
mso-fareast-language:EN-US'><o:p></o:p></span></p>

<p class=MsoListParagraphCxSpMiddle style='text-indent:-18.0pt;mso-pagination:
none;mso-list:l14 level1 lfo12;tab-stops:47.0pt 72.0pt;mso-layout-grid-align:
none;text-autospace:none'><![if !supportLists]><span lang=EN-US
style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;mso-fareast-font-family:
Arial;color:#595959;mso-themecolor:text1;mso-themetint:166;mso-ansi-language:
EN-US;mso-fareast-language:EN-US'><span style='mso-list:Ignore'>a.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]><span
lang=EN-US style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;
color:#595959;mso-themecolor:text1;mso-themetint:166;mso-ansi-language:EN-US;
mso-fareast-language:EN-US'>Users acknowledge that <span class=SpellE>Weeloy</span>
may not pre-screen or pre-approve certain content posted on the Channels or any
content sent through the Channels. In any event, <span class=SpellE>Weeloy</span>
takes no responsibility whatsoever for the content on the Channels or any
content sent through the Channels, or for any content lost and does not make
any representations or warranties regarding the content or accuracy of any
material therein. &#8232;&#8232;<o:p></o:p></span></p>

<p class=MsoListParagraphCxSpMiddle style='text-indent:-18.0pt;mso-pagination:
none;mso-list:l14 level1 lfo12;tab-stops:47.0pt 72.0pt;mso-layout-grid-align:
none;text-autospace:none'><![if !supportLists]><span lang=EN-US
style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;mso-fareast-font-family:
Arial;color:#595959;mso-themecolor:text1;mso-themetint:166;mso-ansi-language:
EN-US;mso-fareast-language:EN-US'><span style='mso-list:Ignore'>b.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]><span
lang=EN-US style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;
color:#595959;mso-themecolor:text1;mso-themetint:166;mso-ansi-language:EN-US;
mso-fareast-language:EN-US'>Any Materials uploaded or posted on the Channels by
the Users may be viewed by users of other web sites and/or media platforms
and/or applications linked to the Channels and <span class=SpellE>Weeloy</span>
is not responsible for any improper and/or illegal use by any user or third
party from linked third party web sites and/or media platforms and/or
applications of any data or materials posted on the Channels. <o:p></o:p></span></p>

<p class=MsoListParagraphCxSpMiddle style='text-indent:-18.0pt;mso-pagination:
none;mso-list:l14 level1 lfo12;tab-stops:47.0pt 72.0pt;mso-layout-grid-align:
none;text-autospace:none'><![if !supportLists]><span lang=EN-US
style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;mso-fareast-font-family:
Arial;color:#595959;mso-themecolor:text1;mso-themetint:166;mso-ansi-language:
EN-US;mso-fareast-language:EN-US'><span style='mso-list:Ignore'>c.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]><span
lang=EN-US style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;
color:#595959;mso-themecolor:text1;mso-themetint:166;mso-ansi-language:EN-US;
mso-fareast-language:EN-US'>Links to third party web sites and/or media
platforms and/or applications provided on the Channels are provided solely as a
convenience to the Users and as <span class=GramE>internet</span> navigation
tools, and not in any way an endorsement by the <span class=SpellE>Weeloy</span>
of the contents on such third party web sites and/or media platforms and/or
applications. <o:p></o:p></span></p>

<p class=MsoListParagraphCxSpMiddle style='text-indent:-18.0pt;mso-pagination:
none;mso-list:l14 level1 lfo12;tab-stops:47.0pt 72.0pt;mso-layout-grid-align:
none;text-autospace:none'><![if !supportLists]><span lang=EN-US
style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;mso-fareast-font-family:
Arial;color:#595959;mso-themecolor:text1;mso-themetint:166;mso-ansi-language:
EN-US;mso-fareast-language:EN-US'><span style='mso-list:Ignore'>d.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]><span
lang=EN-US style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;
color:#595959;mso-themecolor:text1;mso-themetint:166;mso-ansi-language:EN-US;
mso-fareast-language:EN-US'>Unless otherwise stated on the Channels, <span
class=SpellE>Weeloy</span> has no control over or rights in such third party
web sites and/or media platforms and/or applications and is not responsible for
any contents on such third party web sites and/or media platforms and/or
applications or any use of services provided by such third party web sites
and/or media platforms and/or applications by the Users. <o:p></o:p></span></p>

<p class=MsoListParagraphCxSpMiddle style='text-indent:-18.0pt;mso-pagination:
none;mso-list:l14 level1 lfo12;tab-stops:47.0pt 72.0pt;mso-layout-grid-align:
none;text-autospace:none'><![if !supportLists]><span lang=EN-US
style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;mso-fareast-font-family:
Arial;color:#595959;mso-themecolor:text1;mso-themetint:166;mso-ansi-language:
EN-US;mso-fareast-language:EN-US'><span style='mso-list:Ignore'>e.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]><span
lang=EN-US style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;
color:#595959;mso-themecolor:text1;mso-themetint:166;mso-ansi-language:EN-US;
mso-fareast-language:EN-US'>All Users acknowledge and agree that they are solely
responsible for the form, content and accuracy of any Materials, web page or
other information contained therein placed by them. <o:p></o:p></span></p>

<p class=MsoListParagraphCxSpMiddle style='text-indent:-18.0pt;mso-pagination:
none;mso-list:l14 level1 lfo12;tab-stops:47.0pt 72.0pt;mso-layout-grid-align:
none;text-autospace:none'><![if !supportLists]><span lang=EN-US
style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;mso-fareast-font-family:
Arial;color:#595959;mso-themecolor:text1;mso-themetint:166;mso-ansi-language:
EN-US;mso-fareast-language:EN-US'><span style='mso-list:Ignore'>f.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]><span
class=SpellE><span lang=EN-US style='font-size:9.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial;color:#595959;mso-themecolor:text1;mso-themetint:166;
mso-ansi-language:EN-US;mso-fareast-language:EN-US'>Weeloy</span></span><span
lang=EN-US style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;
color:#595959;mso-themecolor:text1;mso-themetint:166;mso-ansi-language:EN-US;
mso-fareast-language:EN-US'> is not responsible for the content of any third
party web sites and/or media platforms and/or applications linked to the
Channels, and does not make any representations or warranties regarding the
contents or accuracy of materials on such third party web sites and/or media
platforms and/or applications. If any User accesses any linked third party web
sites and/or media platforms and/or applications, he/she/it does so entirely at
his/her/its own risk.<o:p></o:p></span></p>

<p class=MsoListParagraphCxSpMiddle style='text-indent:-18.0pt;mso-pagination:
none;mso-list:l14 level1 lfo12;tab-stops:47.0pt 72.0pt;mso-layout-grid-align:
none;text-autospace:none'><![if !supportLists]><span lang=EN-US
style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;mso-fareast-font-family:
Arial;color:#595959;mso-themecolor:text1;mso-themetint:166;mso-ansi-language:
EN-US;mso-fareast-language:EN-US'><span style='mso-list:Ignore'>g.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]><span
class=SpellE><span lang=EN-US style='font-size:9.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial;color:#595959;mso-themecolor:text1;mso-themetint:166;
mso-ansi-language:EN-US;mso-fareast-language:EN-US'>Weeloy</span></span><span
lang=EN-US style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;
color:#595959;mso-themecolor:text1;mso-themetint:166;mso-ansi-language:EN-US;
mso-fareast-language:EN-US'> shall have the right to remove any Materials
uploaded or posted on the Channels at its sole discretion without any
compensation or recourse to the Posting Users if <span class=SpellE>Weeloy</span>
considers at its sole discretion that such Users have breached or is likely to
breach any law or the Terms and Conditions or any terms and conditions of any
specific Channel(s) or service agreement(s) entered into between <span
class=SpellE>Weeloy</span> and the Posting User(s).<o:p></o:p></span></p>

<p class=MsoListParagraphCxSpLast style='text-indent:-18.0pt;mso-pagination:
none;mso-list:l14 level1 lfo12;tab-stops:47.0pt 72.0pt;mso-layout-grid-align:
none;text-autospace:none'><![if !supportLists]><span lang=EN-US
style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;mso-fareast-font-family:
Arial;color:#595959;mso-themecolor:text1;mso-themetint:166;mso-ansi-language:
EN-US;mso-fareast-language:EN-US'><span style='mso-list:Ignore'>h.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]><span
lang=EN-US style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;
color:#595959;mso-themecolor:text1;mso-themetint:166;mso-ansi-language:EN-US;
mso-fareast-language:EN-US'>In the event, <span class=SpellE>Weeloy</span>
decides to remove any paid advertisement for any reasons not relating to any
breach of law or the provisions herein, <span class=SpellE>Weeloy</span> may,
after deducting any fees that may charged for the period that the advertisement
has been posted on the Channels, refund the remaining fees (if any) to the
related Posting User in accordance with the Terms and Conditions or the terms
and conditions of any specific Channel(s) or service agreement(s) entered into
between <span class=SpellE>Weeloy</span> and the related Posting User, without
prejudice to <span class=SpellE>Weeloy’s</span> rights and remedies
hereunder.&#8232;&#8232;Users agree and consent that the <span class=SpellE>Weeloy</span>
may, subject to the terms of the <span class=SpellE>Weeloy</span> Privacy
Policy, use their personal data and/or other information provided to the
Channels for purposes relating to the provision of Services and/or offered by
the <span class=SpellE>Weeloy</span> and marketing services and/or special
events of the <span class=SpellE>Weeloy</span>. <o:p></o:p></span></p>

<p class=MsoNormal style='text-indent:36.0pt;mso-pagination:none;tab-stops:
11.0pt 36.0pt;mso-layout-grid-align:none;text-autospace:none'><b><span
lang=EN-US style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;
color:#595959;mso-themecolor:text1;mso-themetint:166;mso-ansi-language:EN-US;
mso-fareast-language:EN-US'><o:p>&nbsp;</o:p></span></b></p>

<p class=MsoListParagraphCxSpFirst style='margin-left:18.0pt;mso-add-space:
auto;text-indent:-18.0pt;mso-pagination:none;mso-list:l10 level1 lfo3;
tab-stops:11.0pt 36.0pt;mso-layout-grid-align:none;text-autospace:none'><![if !supportLists]><span
lang=EN-US style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;
mso-fareast-font-family:Arial;color:#595959;mso-themecolor:text1;mso-themetint:
166;mso-ansi-language:EN-US;mso-fareast-language:EN-US'><span style='mso-list:
Ignore'>7.<span style='font:7.0pt "Times New Roman"'>&nbsp; </span></span></span><![endif]><b><span
lang=EN-US style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;
color:#595959;mso-themecolor:text1;mso-themetint:166;mso-ansi-language:EN-US;
mso-fareast-language:EN-US'><span style="mso-spacerun:yes">&nbsp;&nbsp;</span><u>Responsibility</u></span></b><span
lang=EN-US style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;
color:#595959;mso-themecolor:text1;mso-themetint:166;mso-ansi-language:EN-US;
mso-fareast-language:EN-US'><o:p></o:p></span></p>

<p class=MsoListParagraphCxSpMiddle style='text-indent:-18.0pt;mso-pagination:
none;mso-list:l9 level1 lfo13;tab-stops:47.0pt 72.0pt;mso-layout-grid-align:
none;text-autospace:none'><![if !supportLists]><span lang=EN-US
style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;mso-fareast-font-family:
Arial;color:#595959;mso-themecolor:text1;mso-themetint:166;mso-ansi-language:
EN-US;mso-fareast-language:EN-US'><span style='mso-list:Ignore'>a.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]><span
class=SpellE><span lang=EN-US style='font-size:9.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial;color:#595959;mso-themecolor:text1;mso-themetint:166;
mso-ansi-language:EN-US;mso-fareast-language:EN-US'>Weeloy</span></span><span
lang=EN-US style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;
color:#595959;mso-themecolor:text1;mso-themetint:166;mso-ansi-language:EN-US;
mso-fareast-language:EN-US'> may not monitor the Channels at all times but reserves
the right to do so. <o:p></o:p></span></p>

<p class=MsoListParagraphCxSpMiddle style='text-indent:-18.0pt;mso-pagination:
none;mso-list:l9 level1 lfo13;tab-stops:47.0pt 72.0pt;mso-layout-grid-align:
none;text-autospace:none'><![if !supportLists]><span lang=EN-US
style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;mso-fareast-font-family:
Arial;color:#595959;mso-themecolor:text1;mso-themetint:166;mso-ansi-language:
EN-US;mso-fareast-language:EN-US'><span style='mso-list:Ignore'>b.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]><span
class=SpellE><span lang=EN-US style='font-size:9.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial;color:#595959;mso-themecolor:text1;mso-themetint:166;
mso-ansi-language:EN-US;mso-fareast-language:EN-US'>Weeloy</span></span><span
lang=EN-US style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;
color:#595959;mso-themecolor:text1;mso-themetint:166;mso-ansi-language:EN-US;
mso-fareast-language:EN-US'> does not warrant that any Materials or web page or
application will be viewed by any specific number of Users or that <span
class=GramE>it will be viewed by any specific User</span>. <o:p></o:p></span></p>

<p class=MsoListParagraphCxSpMiddle style='text-indent:-18.0pt;mso-pagination:
none;mso-list:l9 level1 lfo13;tab-stops:47.0pt 72.0pt;mso-layout-grid-align:
none;text-autospace:none'><![if !supportLists]><span lang=EN-US
style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;mso-fareast-font-family:
Arial;color:#595959;mso-themecolor:text1;mso-themetint:166;mso-ansi-language:
EN-US;mso-fareast-language:EN-US'><span style='mso-list:Ignore'>c.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]><span
class=SpellE><span lang=EN-US style='font-size:9.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial;color:#595959;mso-themecolor:text1;mso-themetint:166;
mso-ansi-language:EN-US;mso-fareast-language:EN-US'>Weeloy</span></span><span
lang=EN-US style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;
color:#595959;mso-themecolor:text1;mso-themetint:166;mso-ansi-language:EN-US;
mso-fareast-language:EN-US'> shall not in any way be considered an agent of any
User with respect to any use of the Channels and shall not be responsible in
any way for any direct or indirect damage or loss that may arise or result from
the use of the Channels, for whatever reason made. <o:p></o:p></span></p>

<p class=MsoListParagraphCxSpMiddle style='text-indent:-18.0pt;mso-pagination:
none;mso-list:l9 level1 lfo13;tab-stops:47.0pt 72.0pt;mso-layout-grid-align:
none;text-autospace:none'><![if !supportLists]><span lang=EN-US
style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;mso-fareast-font-family:
Arial;color:#595959;mso-themecolor:text1;mso-themetint:166;mso-ansi-language:
EN-US;mso-fareast-language:EN-US'><span style='mso-list:Ignore'>d.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]><span
class=SpellE><span lang=EN-US style='font-size:9.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial;color:#595959;mso-themecolor:text1;mso-themetint:166;
mso-ansi-language:EN-US;mso-fareast-language:EN-US'>Weeloy</span></span><span
lang=EN-US style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;
color:#595959;mso-themecolor:text1;mso-themetint:166;mso-ansi-language:EN-US;
mso-fareast-language:EN-US'> endeavors to provide quality service to all Users.
However, <span class=SpellE>Weeloy</span> does not warrant that the Channels
will operate without error at <span class=SpellE>at</span> all time and are
free of viruses or other harmful mechanisms. If use of the Channels or their
contents result in the need for servicing or replacement of equipment or data
by any user, <span class=SpellE>Weeloy</span> shall not be liable for such
costs. The Channels and their contents are provided on an “As Is” basis without
any warranties of any kind. To the fullest extent permitted by law, <span
class=SpellE>Weeloy</span> disclaims all warranties, including, without
prejudice to the foregoing, any in respect of merchantability, non-infringement
of third party rights, fitness for particular purpose, or about the accuracy,
reliability, completeness or timeliness of the contents, services, text,
graphics and links of the Channels.<o:p></o:p></span></p>

<p class=MsoListParagraphCxSpMiddle style='mso-pagination:none;tab-stops:47.0pt 72.0pt;
mso-layout-grid-align:none;text-autospace:none'><span lang=EN-US
style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;color:#595959;
mso-themecolor:text1;mso-themetint:166;mso-ansi-language:EN-US;mso-fareast-language:
EN-US'><o:p>&nbsp;</o:p></span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:18.0pt;mso-add-space:
auto;text-indent:-18.0pt;mso-pagination:none;mso-list:l10 level1 lfo3;
tab-stops:11.0pt 36.0pt;mso-layout-grid-align:none;text-autospace:none'><![if !supportLists]><b><span
lang=EN-US style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;
mso-fareast-font-family:Arial;color:#595959;mso-themecolor:text1;mso-themetint:
166;mso-ansi-language:EN-US;mso-fareast-language:EN-US'><span style='mso-list:
Ignore'>8.<span style='font:7.0pt "Times New Roman"'>&nbsp; </span></span></span></b><![endif]><b><span
lang=EN-US style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;
color:#595959;mso-themecolor:text1;mso-themetint:166;mso-ansi-language:EN-US;
mso-fareast-language:EN-US'><span style="mso-spacerun:yes">&nbsp;&nbsp;</span><u>Own
Risk</u><o:p></o:p></span></b></p>

<p class=MsoListParagraphCxSpMiddle style='text-indent:-18.0pt;mso-pagination:
none;mso-list:l20 level1 lfo15;tab-stops:47.0pt 72.0pt;mso-layout-grid-align:
none;text-autospace:none'><![if !supportLists]><span lang=EN-US
style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;mso-fareast-font-family:
Arial;color:#595959;mso-themecolor:text1;mso-themetint:166;mso-ansi-language:
EN-US;mso-fareast-language:EN-US'><span style='mso-list:Ignore'>a.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]><span
lang=EN-US style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;
color:#595959;mso-themecolor:text1;mso-themetint:166;mso-ansi-language:EN-US;
mso-fareast-language:EN-US'>All Users shall use the Channels and any other web
sites and/or media platforms and/or applications accessed through the Channels
entirely at their own risk.<o:p></o:p></span></p>

<p class=MsoListParagraphCxSpMiddle style='text-indent:-18.0pt;mso-pagination:
none;mso-list:l20 level1 lfo15;tab-stops:47.0pt 72.0pt;mso-layout-grid-align:
none;text-autospace:none'><![if !supportLists]><span lang=EN-US
style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;mso-fareast-font-family:
Arial;color:#595959;mso-themecolor:text1;mso-themetint:166;mso-ansi-language:
EN-US;mso-fareast-language:EN-US'><span style='mso-list:Ignore'>b.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]><span
lang=EN-US style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;
color:#595959;mso-themecolor:text1;mso-themetint:166;mso-ansi-language:EN-US;
mso-fareast-language:EN-US'>ALL Users are responsible for the consequences of
their postings. <span class=SpellE>Weeloy</span> does not represent or
guarantee the truthfulness, accuracy or reliability of any Materials uploaded
or posted by the Posting Users or endorses any opinions expressed by the
Posting Users. <o:p></o:p></span></p>

<p class=MsoListParagraphCxSpLast style='text-indent:-18.0pt;mso-pagination:
none;mso-list:l20 level1 lfo15;tab-stops:47.0pt 72.0pt;mso-layout-grid-align:
none;text-autospace:none'><![if !supportLists]><span lang=EN-US
style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;mso-fareast-font-family:
Arial;color:#595959;mso-themecolor:text1;mso-themetint:166;mso-ansi-language:
EN-US;mso-fareast-language:EN-US'><span style='mso-list:Ignore'>c.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]><span
lang=EN-US style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;
color:#595959;mso-themecolor:text1;mso-themetint:166;mso-ansi-language:EN-US;
mso-fareast-language:EN-US'>Any reliance by any User on advertisements and
materials posted by the other Users will be at their own risk. <span
class=SpellE>Weeloy</span> reserves the right to expel any User and prevent
his/her/its further access to the Channels, at any time for breaching this
agreement or violating the law and also reserves the right to remove any
Materials which is abusive, illegal, disruptive or inappropriate at <span
class=SpellE>Weeloy’s</span> sole discretion.<o:p></o:p></span></p>

<p class=MsoNormal style='mso-pagination:none;tab-stops:11.0pt 36.0pt;
mso-layout-grid-align:none;text-autospace:none'><b><span lang=EN-US
style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;color:#595959;
mso-themecolor:text1;mso-themetint:166;mso-ansi-language:EN-US;mso-fareast-language:
EN-US'><o:p>&nbsp;</o:p></span></b></p>

<p class=MsoListParagraphCxSpFirst style='margin-left:18.0pt;mso-add-space:
auto;text-indent:-18.0pt;mso-pagination:none;mso-list:l10 level1 lfo3;
tab-stops:11.0pt 36.0pt;mso-layout-grid-align:none;text-autospace:none'><![if !supportLists]><span
lang=EN-US style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;
mso-fareast-font-family:Arial;color:#595959;mso-themecolor:text1;mso-themetint:
166;mso-ansi-language:EN-US;mso-fareast-language:EN-US'><span style='mso-list:
Ignore'>9.<span style='font:7.0pt "Times New Roman"'>&nbsp; </span></span></span><![endif]><b><span
lang=EN-US style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;
color:#595959;mso-themecolor:text1;mso-themetint:166;mso-ansi-language:EN-US;
mso-fareast-language:EN-US'><span style="mso-spacerun:yes">&nbsp;&nbsp;</span><u>Indemnity</u></span></b><span
lang=EN-US style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;
color:#595959;mso-themecolor:text1;mso-themetint:166;mso-ansi-language:EN-US;
mso-fareast-language:EN-US'><o:p></o:p></span></p>

<p class=MsoListParagraphCxSpLast style='margin-left:18.0pt;mso-add-space:auto;
mso-pagination:none;tab-stops:47.0pt 72.0pt;mso-layout-grid-align:none;
text-autospace:none'><span lang=EN-US style='font-size:9.0pt;mso-bidi-font-size:
12.0pt;font-family:Arial;color:#595959;mso-themecolor:text1;mso-themetint:166;
mso-ansi-language:EN-US;mso-fareast-language:EN-US'>All Users agree to indemnify,
and hold harmless <span class=SpellE>Weeloy</span>, its officers, directors,
employees, agents, partners, representatives, shareholders, servants,
attorneys, predecessors, successors and assigns from and against any claims,
actions, demands, liabilities, losses, damages, costs and expenses (including
legal fees and litigation expenses on a full indemnity basis) arising from or
resulting from their use of the Channels or their breach of the terms of this
Agreement or any terms and conditions of any specific Channel(s) or service
agreement(s) entered into between <span class=SpellE>Weeloy</span> and the
User(s). <span class=SpellE>Weeloy</span> will provide prompt notice of any
such claim, suit or proceedings to the relevant User.<o:p></o:p></span></p>

<p class=MsoNormal style='text-indent:36.0pt;mso-pagination:none;tab-stops:
11.0pt 36.0pt;mso-layout-grid-align:none;text-autospace:none'><b><span
lang=EN-US style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;
color:#595959;mso-themecolor:text1;mso-themetint:166;mso-ansi-language:EN-US;
mso-fareast-language:EN-US'><o:p>&nbsp;</o:p></span></b></p>

<p class=MsoListParagraphCxSpFirst style='margin-left:18.0pt;mso-add-space:
auto;text-indent:-18.0pt;mso-pagination:none;mso-list:l10 level1 lfo3;
tab-stops:11.0pt 36.0pt;mso-layout-grid-align:none;text-autospace:none'><![if !supportLists]><span
lang=EN-US style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;
mso-fareast-font-family:Arial;color:#595959;mso-themecolor:text1;mso-themetint:
166;mso-ansi-language:EN-US;mso-fareast-language:EN-US'><span style='mso-list:
Ignore'>10.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp; </span></span></span><![endif]><b><span
lang=EN-US style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;
color:#595959;mso-themecolor:text1;mso-themetint:166;mso-ansi-language:EN-US;
mso-fareast-language:EN-US'><span style="mso-spacerun:yes">&nbsp;</span><u>Limitation
of the Service</u></span></b><span lang=EN-US style='font-size:9.0pt;
mso-bidi-font-size:12.0pt;font-family:Arial;color:#595959;mso-themecolor:text1;
mso-themetint:166;mso-ansi-language:EN-US;mso-fareast-language:EN-US'><o:p></o:p></span></p>

<p class=MsoListParagraphCxSpMiddle style='text-indent:-18.0pt;mso-pagination:
none;mso-list:l18 level1 lfo16;tab-stops:47.0pt 72.0pt;mso-layout-grid-align:
none;text-autospace:none'><![if !supportLists]><span lang=EN-US
style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;mso-fareast-font-family:
Arial;color:#595959;mso-themecolor:text1;mso-themetint:166;mso-ansi-language:
EN-US;mso-fareast-language:EN-US'><span style='mso-list:Ignore'>a.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]><span
class=SpellE><span lang=EN-US style='font-size:9.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial;color:#595959;mso-themecolor:text1;mso-themetint:166;
mso-ansi-language:EN-US;mso-fareast-language:EN-US'>Weeloy</span></span><span
lang=EN-US style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;
color:#595959;mso-themecolor:text1;mso-themetint:166;mso-ansi-language:EN-US;
mso-fareast-language:EN-US'> shall have the right to limit the use of the
Services, including the period of time that Materials will be posted on the
Channels, the size, placement and position of the Materials, email messages or
any other contents, which are transmitted by the Services. <o:p></o:p></span></p>

<p class=MsoListParagraphCxSpLast style='text-indent:-18.0pt;mso-pagination:
none;mso-list:l18 level1 lfo16;tab-stops:47.0pt 72.0pt;mso-layout-grid-align:
none;text-autospace:none'><![if !supportLists]><span lang=EN-US
style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;mso-fareast-font-family:
Arial;color:#595959;mso-themecolor:text1;mso-themetint:166;mso-ansi-language:
EN-US;mso-fareast-language:EN-US'><span style='mso-list:Ignore'>b.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]><span
class=SpellE><span lang=EN-US style='font-size:9.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial;color:#595959;mso-themecolor:text1;mso-themetint:166;
mso-ansi-language:EN-US;mso-fareast-language:EN-US'>Weeloy</span></span><span
lang=EN-US style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;
color:#595959;mso-themecolor:text1;mso-themetint:166;mso-ansi-language:EN-US;
mso-fareast-language:EN-US'> reserves the right, at its sole discretion, to
edit, modify, share, erase, delete or remove any Materials posted on the Channels,
for any reason, without giving any prior notice or reason to the Users. The
Users acknowledge that <span class=SpellE>Weeloy</span> shall not be liable to
any party for any modification, suspension or discontinuance of the Services.<o:p></o:p></span></p>

<p class=MsoNormal style='text-indent:36.0pt;mso-pagination:none;tab-stops:
11.0pt 36.0pt;mso-layout-grid-align:none;text-autospace:none'><b><span
lang=EN-US style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;
color:#595959;mso-themecolor:text1;mso-themetint:166;mso-ansi-language:EN-US;
mso-fareast-language:EN-US'><o:p>&nbsp;</o:p></span></b></p>

<p class=MsoListParagraphCxSpFirst style='margin-left:18.0pt;mso-add-space:
auto;text-indent:-18.0pt;mso-pagination:none;mso-list:l10 level1 lfo3;
tab-stops:11.0pt 36.0pt;mso-layout-grid-align:none;text-autospace:none'><![if !supportLists]><span
lang=EN-US style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;
mso-fareast-font-family:Arial;color:#595959;mso-themecolor:text1;mso-themetint:
166;mso-ansi-language:EN-US;mso-fareast-language:EN-US'><span style='mso-list:
Ignore'>11.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp; </span></span></span><![endif]><b><span
lang=EN-US style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;
color:#595959;mso-themecolor:text1;mso-themetint:166;mso-ansi-language:EN-US;
mso-fareast-language:EN-US'><span style="mso-spacerun:yes">&nbsp;</span><u>Termination
of Service</u></span></b><span lang=EN-US style='font-size:9.0pt;mso-bidi-font-size:
12.0pt;font-family:Arial;color:#595959;mso-themecolor:text1;mso-themetint:166;
mso-ansi-language:EN-US;mso-fareast-language:EN-US'><o:p></o:p></span></p>

<p class=MsoListParagraphCxSpMiddle style='text-indent:-18.0pt;mso-pagination:
none;mso-list:l17 level1 lfo17;tab-stops:47.0pt 72.0pt;mso-layout-grid-align:
none;text-autospace:none'><![if !supportLists]><span lang=EN-US
style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;mso-fareast-font-family:
Arial;color:#595959;mso-themecolor:text1;mso-themetint:166;mso-ansi-language:
EN-US;mso-fareast-language:EN-US'><span style='mso-list:Ignore'>a.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]><span
class=SpellE><span lang=EN-US style='font-size:9.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial;color:#595959;mso-themecolor:text1;mso-themetint:166;
mso-ansi-language:EN-US;mso-fareast-language:EN-US'>Weeloy</span></span><span
lang=EN-US style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;
color:#595959;mso-themecolor:text1;mso-themetint:166;mso-ansi-language:EN-US;
mso-fareast-language:EN-US'> shall have the right to delete or deactivate any
account, or block the email or IP address of any User, or terminate the access
of Users to the Services, and remove any Materials within the Services
immediately without notice for any reason, including but not limited to the
reason that the User breached any law or the Terms and Conditions or any terms
and conditions of any specific Channel(s) or any service agreement(s) entered
into between <span class=SpellE>Weeloy</span> and the User(s). <o:p></o:p></span></p>

<p class=MsoListParagraphCxSpMiddle style='text-indent:-18.0pt;mso-pagination:
none;mso-list:l17 level1 lfo17;tab-stops:47.0pt 72.0pt;mso-layout-grid-align:
none;text-autospace:none'><![if !supportLists]><span lang=EN-US
style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;mso-fareast-font-family:
Arial;color:#595959;mso-themecolor:text1;mso-themetint:166;mso-ansi-language:
EN-US;mso-fareast-language:EN-US'><span style='mso-list:Ignore'>b.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]><span
class=SpellE><span lang=EN-US style='font-size:9.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial;color:#595959;mso-themecolor:text1;mso-themetint:166;
mso-ansi-language:EN-US;mso-fareast-language:EN-US'>Weeloy</span></span><span
lang=EN-US style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;
color:#595959;mso-themecolor:text1;mso-themetint:166;mso-ansi-language:EN-US;
mso-fareast-language:EN-US'> reserves the right at any time to take such action
as it considers appropriate, desirable or necessary including but not limited
to taking legal actions against any such User. <o:p></o:p></span></p>

<p class=MsoListParagraphCxSpMiddle style='text-indent:-18.0pt;mso-pagination:
none;mso-list:l17 level1 lfo17;tab-stops:47.0pt 72.0pt;mso-layout-grid-align:
none;text-autospace:none'><![if !supportLists]><span lang=EN-US
style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;mso-fareast-font-family:
Arial;color:#595959;mso-themecolor:text1;mso-themetint:166;mso-ansi-language:
EN-US;mso-fareast-language:EN-US'><span style='mso-list:Ignore'>c.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]><span
class=SpellE><span lang=EN-US style='font-size:9.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial;color:#595959;mso-themecolor:text1;mso-themetint:166;
mso-ansi-language:EN-US;mso-fareast-language:EN-US'>Weeloy</span></span><span
lang=EN-US style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;
color:#595959;mso-themecolor:text1;mso-themetint:166;mso-ansi-language:EN-US;
mso-fareast-language:EN-US'> shall have no obligation to deliver any Materials
posted on the Channels to any User at any time, both before or after cessation
of the <o:p></o:p></span></p>

<p class=MsoListParagraphCxSpMiddle style='mso-pagination:none;tab-stops:47.0pt 72.0pt;
mso-layout-grid-align:none;text-autospace:none'><span class=GramE><span
lang=EN-US style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;
color:#595959;mso-themecolor:text1;mso-themetint:166;mso-ansi-language:EN-US;
mso-fareast-language:EN-US'>Services or upon removal of the related Material(s)
from the Channels.</span></span><span lang=EN-US style='font-size:9.0pt;
mso-bidi-font-size:12.0pt;font-family:Arial;color:#595959;mso-themecolor:text1;
mso-themetint:166;mso-ansi-language:EN-US;mso-fareast-language:EN-US'> <o:p></o:p></span></p>

<p class=MsoListParagraphCxSpLast style='text-indent:-18.0pt;mso-pagination:
none;mso-list:l17 level1 lfo17;tab-stops:47.0pt 72.0pt;mso-layout-grid-align:
none;text-autospace:none'><![if !supportLists]><span lang=EN-US
style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;mso-fareast-font-family:
Arial;color:#595959;mso-themecolor:text1;mso-themetint:166;mso-ansi-language:
EN-US;mso-fareast-language:EN-US'><span style='mso-list:Ignore'>d.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]><span
lang=EN-US style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;
color:#595959;mso-themecolor:text1;mso-themetint:166;mso-ansi-language:EN-US;
mso-fareast-language:EN-US'>The Terms and Conditions set out hereunder shall
become inapplicable to the Users immediately upon the Users discontinuing their
use of the Sites and/or the Platforms and/or Applications as the case may be.<o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:18.0pt;text-indent:36.0pt;mso-pagination:
none;tab-stops:11.0pt 36.0pt;mso-layout-grid-align:none;text-autospace:none'><b><span
lang=EN-US style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;
color:#595959;mso-themecolor:text1;mso-themetint:166;mso-ansi-language:EN-US;
mso-fareast-language:EN-US'><o:p>&nbsp;</o:p></span></b></p>

<p class=MsoListParagraphCxSpFirst style='margin-left:18.0pt;mso-add-space:
auto;text-indent:-18.0pt;mso-pagination:none;mso-list:l10 level1 lfo3;
tab-stops:11.0pt 36.0pt;mso-layout-grid-align:none;text-autospace:none'><![if !supportLists]><span
lang=EN-US style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;
mso-fareast-font-family:Arial;color:#595959;mso-themecolor:text1;mso-themetint:
166;mso-ansi-language:EN-US;mso-fareast-language:EN-US'><span style='mso-list:
Ignore'>12.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp; </span></span></span><![endif]><b><span
lang=EN-US style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;
color:#595959;mso-themecolor:text1;mso-themetint:166;mso-ansi-language:EN-US;
mso-fareast-language:EN-US'><span style="mso-spacerun:yes">&nbsp;</span><u>Disclaimer</u></span></b><span
lang=EN-US style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;
color:#595959;mso-themecolor:text1;mso-themetint:166;mso-ansi-language:EN-US;
mso-fareast-language:EN-US'><o:p></o:p></span></p>

<p class=MsoListParagraphCxSpMiddle style='text-indent:-18.0pt;mso-pagination:
none;mso-list:l2 level1 lfo18;tab-stops:47.0pt 72.0pt;mso-layout-grid-align:
none;text-autospace:none'><![if !supportLists]><span lang=EN-US
style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;mso-fareast-font-family:
Arial;color:#595959;mso-themecolor:text1;mso-themetint:166;mso-ansi-language:
EN-US;mso-fareast-language:EN-US'><span style='mso-list:Ignore'>a.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]><span
class=SpellE><span lang=EN-US style='font-size:9.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial;color:#595959;mso-themecolor:text1;mso-themetint:166;
mso-ansi-language:EN-US;mso-fareast-language:EN-US'>Weeloy</span></span><span
lang=EN-US style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;
color:#595959;mso-themecolor:text1;mso-themetint:166;mso-ansi-language:EN-US;
mso-fareast-language:EN-US'> does not have control over and does not guarantee
the truth or accuracy of listings of any Materials posted on the Channels or
any content on third party web sites and/or media platforms and/or applications
accessed via the Channels.<o:p></o:p></span></p>

<p class=MsoListParagraphCxSpLast style='text-indent:-18.0pt;mso-pagination:
none;mso-list:l2 level1 lfo18;tab-stops:47.0pt 72.0pt;mso-layout-grid-align:
none;text-autospace:none'><![if !supportLists]><span lang=EN-US
style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;mso-fareast-font-family:
Arial;color:#595959;mso-themecolor:text1;mso-themetint:166;mso-ansi-language:
EN-US;mso-fareast-language:EN-US'><span style='mso-list:Ignore'>b.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]><span
lang=EN-US style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;
color:#595959;mso-themecolor:text1;mso-themetint:166;mso-ansi-language:EN-US;
mso-fareast-language:EN-US'>In any event, <span class=SpellE>Weeloy</span>, its
officers, directors, employees, agents, partners, representatives,
shareholders, servants, attorneys, predecessors and successors shall not be
liable for any losses, claims or damages suffered by&#8232;any User whatsoever
and howsoever arising or resulting from his/her/its use or inability to use the
Channels and their contents, including negligence and disputes between any
parties.<o:p></o:p></span></p>

<p class=MsoNormal style='mso-pagination:none;tab-stops:11.0pt 36.0pt;
mso-layout-grid-align:none;text-autospace:none'><b><span lang=EN-US
style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;color:#595959;
mso-themecolor:text1;mso-themetint:166;mso-ansi-language:EN-US;mso-fareast-language:
EN-US'><o:p>&nbsp;</o:p></span></b></p>

<p class=MsoListParagraphCxSpFirst style='margin-left:18.0pt;mso-add-space:
auto;text-indent:-18.0pt;mso-pagination:none;mso-list:l10 level1 lfo3;
tab-stops:11.0pt 36.0pt;mso-layout-grid-align:none;text-autospace:none'><![if !supportLists]><span
lang=EN-US style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;
mso-fareast-font-family:Arial;color:#595959;mso-themecolor:text1;mso-themetint:
166;mso-ansi-language:EN-US;mso-fareast-language:EN-US'><span style='mso-list:
Ignore'>13.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp; </span></span></span><![endif]><b><u><span
lang=EN-US style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;
color:#595959;mso-themecolor:text1;mso-themetint:166;mso-ansi-language:EN-US;
mso-fareast-language:EN-US'>Limitation of Liability</span></u></b><span
lang=EN-US style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;
color:#595959;mso-themecolor:text1;mso-themetint:166;mso-ansi-language:EN-US;
mso-fareast-language:EN-US'><o:p></o:p></span></p>

<p class=MsoListParagraphCxSpLast style='margin-left:18.0pt;mso-add-space:auto;
mso-pagination:none;tab-stops:47.0pt 72.0pt;mso-layout-grid-align:none;
text-autospace:none'><span lang=EN-US style='font-size:9.0pt;mso-bidi-font-size:
12.0pt;font-family:Arial;color:#595959;mso-themecolor:text1;mso-themetint:166;
mso-ansi-language:EN-US;mso-fareast-language:EN-US'>Without prejudice to the
above and subject to the applicable laws, the aggregate liability of <span
class=SpellE>Weeloy</span> to any User for all claims arising from their use of
the Services and the Channels shall be limited to the amount of SGD100.<o:p></o:p></span></p>

<p class=MsoNormal style='text-indent:36.0pt;mso-pagination:none;tab-stops:
11.0pt 36.0pt;mso-layout-grid-align:none;text-autospace:none'><b><span
lang=EN-US style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;
color:#595959;mso-themecolor:text1;mso-themetint:166;mso-ansi-language:EN-US;
mso-fareast-language:EN-US'><o:p>&nbsp;</o:p></span></b></p>

<p class=MsoListParagraphCxSpFirst style='margin-left:18.0pt;mso-add-space:
auto;text-indent:-18.0pt;mso-pagination:none;mso-list:l10 level1 lfo3;
tab-stops:11.0pt 36.0pt;mso-layout-grid-align:none;text-autospace:none'><![if !supportLists]><span
lang=EN-US style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;
mso-fareast-font-family:Arial;color:#595959;mso-themecolor:text1;mso-themetint:
166;mso-ansi-language:EN-US;mso-fareast-language:EN-US'><span style='mso-list:
Ignore'>14.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp; </span></span></span><![endif]><b><u><span
lang=EN-US style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;
color:#595959;mso-themecolor:text1;mso-themetint:166;mso-ansi-language:EN-US;
mso-fareast-language:EN-US'>Security Measures</span></u></b><span lang=EN-US
style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;color:#595959;
mso-themecolor:text1;mso-themetint:166;mso-ansi-language:EN-US;mso-fareast-language:
EN-US'><o:p></o:p></span></p>

<p class=MsoListParagraphCxSpMiddle style='text-indent:-18.0pt;mso-pagination:
none;mso-list:l15 level1 lfo19;tab-stops:47.0pt 72.0pt;mso-layout-grid-align:
none;text-autospace:none'><![if !supportLists]><span lang=EN-US
style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;mso-fareast-font-family:
Arial;color:#595959;mso-themecolor:text1;mso-themetint:166;mso-ansi-language:
EN-US;mso-fareast-language:EN-US'><span style='mso-list:Ignore'>a.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]><span
class=SpellE><span lang=EN-US style='font-size:9.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial;color:#595959;mso-themecolor:text1;mso-themetint:166;
mso-ansi-language:EN-US;mso-fareast-language:EN-US'>Weeloy</span></span><span
lang=EN-US style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;
color:#595959;mso-themecolor:text1;mso-themetint:166;mso-ansi-language:EN-US;
mso-fareast-language:EN-US'> will use its reasonable endeavors to ensure that
its officers, directors, employees, agents and/or contractors will exercise
their prudence and due diligence in handling the personal data submitted by the
Users, and the access to and processing of the personal data by such persons is
on a &quot;need-to-know&quot; and &quot;need-to-use&quot; basis. <o:p></o:p></span></p>

<p class=MsoListParagraphCxSpLast style='text-indent:-18.0pt;mso-pagination:
none;mso-list:l15 level1 lfo19;tab-stops:47.0pt 72.0pt;mso-layout-grid-align:
none;text-autospace:none'><![if !supportLists]><span lang=EN-US
style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;mso-fareast-font-family:
Arial;color:#595959;mso-themecolor:text1;mso-themetint:166;mso-ansi-language:
EN-US;mso-fareast-language:EN-US'><span style='mso-list:Ignore'>b.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]><span
class=SpellE><span lang=EN-US style='font-size:9.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial;color:#595959;mso-themecolor:text1;mso-themetint:166;
mso-ansi-language:EN-US;mso-fareast-language:EN-US'>Weeloy</span></span><span
lang=EN-US style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;
color:#595959;mso-themecolor:text1;mso-themetint:166;mso-ansi-language:EN-US;
mso-fareast-language:EN-US'> will use its reasonable endeavors to protect the
personal data against any unauthorized or accidental access, processing or
erasure of the personal data.<o:p></o:p></span></p>

<p class=MsoNormal style='text-indent:36.0pt;mso-pagination:none;tab-stops:
11.0pt 36.0pt;mso-layout-grid-align:none;text-autospace:none'><b><span
lang=EN-US style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;
color:#595959;mso-themecolor:text1;mso-themetint:166;mso-ansi-language:EN-US;
mso-fareast-language:EN-US'><o:p>&nbsp;</o:p></span></b></p>

<p class=MsoListParagraphCxSpFirst style='margin-left:18.0pt;mso-add-space:
auto;text-indent:-18.0pt;mso-pagination:none;mso-list:l10 level1 lfo3;
tab-stops:11.0pt 36.0pt;mso-layout-grid-align:none;text-autospace:none'><![if !supportLists]><span
lang=EN-US style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;
mso-fareast-font-family:Arial;color:#595959;mso-themecolor:text1;mso-themetint:
166;mso-ansi-language:EN-US;mso-fareast-language:EN-US'><span style='mso-list:
Ignore'>15.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp; </span></span></span><![endif]><b><u><span
lang=EN-US style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;
color:#595959;mso-themecolor:text1;mso-themetint:166;mso-ansi-language:EN-US;
mso-fareast-language:EN-US'>Severability</span></u></b><span lang=EN-US
style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;color:#595959;
mso-themecolor:text1;mso-themetint:166;mso-ansi-language:EN-US;mso-fareast-language:
EN-US'><o:p></o:p></span></p>

<p class=MsoListParagraphCxSpMiddle style='text-indent:-18.0pt;mso-pagination:
none;mso-list:l13 level1 lfo20;tab-stops:47.0pt 72.0pt;mso-layout-grid-align:
none;text-autospace:none'><![if !supportLists]><span lang=EN-US
style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;mso-fareast-font-family:
Arial;color:#595959;mso-themecolor:text1;mso-themetint:166;mso-ansi-language:
EN-US;mso-fareast-language:EN-US'><span style='mso-list:Ignore'>a.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]><span
lang=EN-US style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;
color:#595959;mso-themecolor:text1;mso-themetint:166;mso-ansi-language:EN-US;
mso-fareast-language:EN-US'>The provisions of the Terms and Conditions shall be
enforceable independently of each other and the validity of each provision
shall not be affected if any of the others is invalid. <o:p></o:p></span></p>

<p class=MsoListParagraphCxSpLast style='text-indent:-18.0pt;mso-pagination:
none;mso-list:l13 level1 lfo20;tab-stops:47.0pt 72.0pt;mso-layout-grid-align:
none;text-autospace:none'><![if !supportLists]><span lang=EN-US
style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;mso-fareast-font-family:
Arial;color:#595959;mso-themecolor:text1;mso-themetint:166;mso-ansi-language:
EN-US;mso-fareast-language:EN-US'><span style='mso-list:Ignore'>b.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]><span
lang=EN-US style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;
color:#595959;mso-themecolor:text1;mso-themetint:166;mso-ansi-language:EN-US;
mso-fareast-language:EN-US'>In the event, any provision of the Terms and
Conditions is determined to be illegal, invalid or unenforceable, the validity
and enforceability of the remaining provisions of the Terms and Conditions
shall not be affected and, in lieu of such illegal, invalid, or unenforceable
provision, there shall be added as part of the Terms and Conditions one or more
provisions as similar in terms as may be legal, valid and enforceable under the
applicable law.<o:p></o:p></span></p>

<p class=MsoNormal style='text-indent:36.0pt;mso-pagination:none;tab-stops:
11.0pt 36.0pt;mso-layout-grid-align:none;text-autospace:none'><b><span
lang=EN-US style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;
color:#595959;mso-themecolor:text1;mso-themetint:166;mso-ansi-language:EN-US;
mso-fareast-language:EN-US'><o:p>&nbsp;</o:p></span></b></p>

<p class=MsoListParagraphCxSpFirst style='margin-left:18.0pt;mso-add-space:
auto;text-indent:-18.0pt;mso-pagination:none;mso-list:l10 level1 lfo3;
tab-stops:11.0pt 36.0pt;mso-layout-grid-align:none;text-autospace:none'><![if !supportLists]><span
lang=EN-US style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;
mso-fareast-font-family:Arial;color:#595959;mso-themecolor:text1;mso-themetint:
166;mso-ansi-language:EN-US;mso-fareast-language:EN-US'><span style='mso-list:
Ignore'>16.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp; </span></span></span><![endif]><b><u><span
lang=EN-US style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;
color:#595959;mso-themecolor:text1;mso-themetint:166;mso-ansi-language:EN-US;
mso-fareast-language:EN-US'>Conflict</span></u></b><span lang=EN-US
style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;color:#595959;
mso-themecolor:text1;mso-themetint:166;mso-ansi-language:EN-US;mso-fareast-language:
EN-US'><o:p></o:p></span></p>

<p class=MsoListParagraphCxSpLast style='margin-left:18.0pt;mso-add-space:auto;
mso-pagination:none;tab-stops:47.0pt 72.0pt;mso-layout-grid-align:none;
text-autospace:none'><span lang=EN-US style='font-size:9.0pt;mso-bidi-font-size:
12.0pt;font-family:Arial;color:#595959;mso-themecolor:text1;mso-themetint:166;
mso-ansi-language:EN-US;mso-fareast-language:EN-US'>If there is any conflict
between (1) the Terms and Conditions and/or specific terms of use appearing on
the Platforms and/or the Applications and/or any term and (2) conditions of any
service agreement(s) entered into between <span class=SpellE>Weeloy</span> and
the User(s), and/or any specific terms and conditions of use in respect of any
special events hosted by <span class=SpellE>Weeloy</span>, then the latter
shall prevail.<o:p></o:p></span></p>

<p class=MsoNormal style='text-indent:36.0pt;mso-pagination:none;tab-stops:
11.0pt 36.0pt;mso-layout-grid-align:none;text-autospace:none'><b><span
lang=EN-US style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;
color:#595959;mso-themecolor:text1;mso-themetint:166;mso-ansi-language:EN-US;
mso-fareast-language:EN-US'><o:p>&nbsp;</o:p></span></b></p>

<p class=MsoListParagraphCxSpFirst style='margin-left:18.0pt;mso-add-space:
auto;text-indent:-18.0pt;mso-pagination:none;mso-list:l10 level1 lfo3;
tab-stops:11.0pt 36.0pt;mso-layout-grid-align:none;text-autospace:none'><![if !supportLists]><span
lang=EN-US style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;
mso-fareast-font-family:Arial;color:#595959;mso-themecolor:text1;mso-themetint:
166;mso-ansi-language:EN-US;mso-fareast-language:EN-US'><span style='mso-list:
Ignore'>17.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp; </span></span></span><![endif]><b><u><span
lang=EN-US style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;
color:#595959;mso-themecolor:text1;mso-themetint:166;mso-ansi-language:EN-US;
mso-fareast-language:EN-US'>Governing Law and Dispute Resolutions</span></u></b><span
lang=EN-US style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;
color:#595959;mso-themecolor:text1;mso-themetint:166;mso-ansi-language:EN-US;
mso-fareast-language:EN-US'><o:p></o:p></span></p>

<p class=MsoListParagraphCxSpMiddle style='text-indent:-18.0pt;mso-pagination:
none;mso-list:l16 level1 lfo21;tab-stops:47.0pt 72.0pt;mso-layout-grid-align:
none;text-autospace:none'><![if !supportLists]><span lang=EN-US
style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;mso-fareast-font-family:
Arial;color:#595959;mso-themecolor:text1;mso-themetint:166;mso-ansi-language:
EN-US;mso-fareast-language:EN-US'><span style='mso-list:Ignore'>a.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]><span
lang=EN-US style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;
color:#595959;mso-themecolor:text1;mso-themetint:166;mso-ansi-language:EN-US;
mso-fareast-language:EN-US'>The Terms and Conditions and any dispute or matter
arising from or incidental to the use of the Channels shall be governed by and
construed in accordance with the laws of Singapore unless otherwise specified.
&#8232;&#8232;<o:p></o:p></span></p>

<p class=MsoListParagraphCxSpMiddle style='text-indent:-18.0pt;mso-pagination:
none;mso-list:l16 level1 lfo21;tab-stops:47.0pt 72.0pt;mso-layout-grid-align:
none;text-autospace:none'><![if !supportLists]><span lang=EN-US
style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;mso-fareast-font-family:
Arial;color:#595959;mso-themecolor:text1;mso-themetint:166;mso-ansi-language:
EN-US;mso-fareast-language:EN-US'><span style='mso-list:Ignore'>b.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]><span
lang=EN-US style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;
color:#595959;mso-themecolor:text1;mso-themetint:166;mso-ansi-language:EN-US;
mso-fareast-language:EN-US'>Any dispute, controversy or claim arising out of or
relating to the Terms and Conditions including the validity, invalidity, breach
or termination thereof, shall be settled by arbitration in accordance with the Arbitration
Rules as at present in force and as may be amended by the rest of this Clause:<o:p></o:p></span></p>

<p class=MsoListParagraphCxSpMiddle style='text-indent:-18.0pt;mso-pagination:
none;mso-list:l16 level1 lfo21;tab-stops:83.0pt 108.0pt;mso-layout-grid-align:
none;text-autospace:none'><![if !supportLists]><span lang=EN-US
style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;mso-fareast-font-family:
Arial;color:#595959;mso-themecolor:text1;mso-themetint:166;mso-ansi-language:
EN-US;mso-fareast-language:EN-US'><span style='mso-list:Ignore'>c.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]><span
lang=EN-US style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;
color:#595959;mso-themecolor:text1;mso-themetint:166;mso-ansi-language:EN-US;
mso-fareast-language:EN-US'>The appointing authority shall be Singapore; or
alternatively, <span class=GramE>an appointing authority may be appointed by <span
class=SpellE>Weeloy</span></span> at its sole and absolute discretion in any
country, which <span class=SpellE>Weeloy</span> considers as fit and
appropriate. <o:p></o:p></span></p>

<p class=MsoListParagraphCxSpMiddle style='text-indent:-18.0pt;mso-pagination:
none;mso-list:l16 level1 lfo21;tab-stops:83.0pt 108.0pt;mso-layout-grid-align:
none;text-autospace:none'><![if !supportLists]><span lang=EN-US
style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;mso-fareast-font-family:
Arial;color:#595959;mso-themecolor:text1;mso-themetint:166;mso-ansi-language:
EN-US;mso-fareast-language:EN-US'><span style='mso-list:Ignore'>d.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]><span
lang=EN-US style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;
color:#595959;mso-themecolor:text1;mso-themetint:166;mso-ansi-language:EN-US;
mso-fareast-language:EN-US'>Any User(s) who are in dispute with <span
class=SpellE>Weeloy</span> acknowledge(s) and agree(s) that the choice of the appointing
authority nominated by <span class=SpellE>Weeloy</span> shall be final and
conclusive.<o:p></o:p></span></p>

<p class=MsoListParagraphCxSpMiddle style='text-indent:-18.0pt;mso-pagination:
none;mso-list:l16 level1 lfo21;tab-stops:83.0pt 108.0pt;mso-layout-grid-align:
none;text-autospace:none'><![if !supportLists]><span lang=EN-US
style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;mso-fareast-font-family:
Arial;color:#595959;mso-themecolor:text1;mso-themetint:166;mso-ansi-language:
EN-US;mso-fareast-language:EN-US'><span style='mso-list:Ignore'>e.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]><span
lang=EN-US style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;
color:#595959;mso-themecolor:text1;mso-themetint:166;mso-ansi-language:EN-US;
mso-fareast-language:EN-US'>The place of arbitration shall be in Singapore; or
alternatively, at any such arbitral body in any country as <span class=SpellE>Weeloy</span>
considers fit and appropriate at its sole and absolute discretion. <o:p></o:p></span></p>

<p class=MsoListParagraphCxSpMiddle style='text-indent:-18.0pt;mso-pagination:
none;mso-list:l16 level1 lfo21;tab-stops:83.0pt 108.0pt;mso-layout-grid-align:
none;text-autospace:none'><![if !supportLists]><span lang=EN-US
style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;mso-fareast-font-family:
Arial;color:#595959;mso-themecolor:text1;mso-themetint:166;mso-ansi-language:
EN-US;mso-fareast-language:EN-US'><span style='mso-list:Ignore'>f.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]><span
lang=EN-US style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;
color:#595959;mso-themecolor:text1;mso-themetint:166;mso-ansi-language:EN-US;
mso-fareast-language:EN-US'>There shall be only one arbitrator.<o:p></o:p></span></p>

<p class=MsoListParagraphCxSpMiddle style='text-indent:-18.0pt;mso-pagination:
none;mso-list:l16 level1 lfo21;tab-stops:83.0pt 108.0pt;mso-layout-grid-align:
none;text-autospace:none'><![if !supportLists]><span lang=EN-US
style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;mso-fareast-font-family:
Arial;color:#595959;mso-themecolor:text1;mso-themetint:166;mso-ansi-language:
EN-US;mso-fareast-language:EN-US'><span style='mso-list:Ignore'>g.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]><span
lang=EN-US style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;
color:#595959;mso-themecolor:text1;mso-themetint:166;mso-ansi-language:EN-US;
mso-fareast-language:EN-US'>The language to be used in the arbitral proceedings
shall be English.<o:p></o:p></span></p>

<p class=MsoListParagraphCxSpMiddle style='text-indent:-18.0pt;mso-pagination:
none;mso-list:l16 level1 lfo21;mso-layout-grid-align:none;text-autospace:none'><![if !supportLists]><span
lang=EN-US style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;
mso-fareast-font-family:Arial;color:#595959;mso-themecolor:text1;mso-themetint:
166;mso-ansi-language:EN-US;mso-fareast-language:EN-US'><span style='mso-list:
Ignore'>h.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]><span
lang=EN-US style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;
color:#595959;mso-themecolor:text1;mso-themetint:166;mso-ansi-language:EN-US;
mso-fareast-language:EN-US'>In the event of any breach of the Terms and
Conditions by any one party, the other party shall be entitled to remedies in
law and equity as determined by arbitration.<o:p></o:p></span></p>

<p class=MsoListParagraphCxSpLast style='mso-pagination:none;mso-layout-grid-align:
none;text-autospace:none'><span lang=EN-US style='font-size:9.0pt;mso-bidi-font-size:
12.0pt;font-family:Arial;color:#595959;mso-themecolor:text1;mso-themetint:166;
mso-ansi-language:EN-US;mso-fareast-language:EN-US'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal style='margin-bottom:12.0pt;mso-pagination:none;mso-layout-grid-align:
none;text-autospace:none'><span lang=EN-US style='font-size:9.0pt;mso-bidi-font-size:
12.0pt;font-family:Arial;color:#595959;mso-themecolor:text1;mso-themetint:166;
mso-ansi-language:EN-US;mso-fareast-language:EN-US'>For any query, please email
our Customer Service Representative at </span><span lang=FR><a
href="mailto:info@weeloy.com"><span lang=EN-US style='font-size:9.0pt;
mso-bidi-font-size:10.0pt;font-family:Arial;color:#595959;mso-themecolor:text1;
mso-themetint:166;mso-ansi-language:EN-US;mso-fareast-language:EN-US;
text-decoration:none;text-underline:none'>info@weeloy.com</span></a></span><span
lang=EN-US style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;
color:#595959;mso-themecolor:text1;mso-themetint:166;mso-ansi-language:EN-US;
mso-fareast-language:EN-US'><o:p></o:p></span></p>

<p class=MsoNormal style='mso-pagination:none;mso-layout-grid-align:none;
text-autospace:none'><span lang=EN-US style='font-size:11.0pt;mso-bidi-font-size:
16.0pt;font-family:Arial;color:#595959;mso-themecolor:text1;mso-themetint:166;
mso-ansi-language:EN-US;mso-fareast-language:EN-US'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal><b style='mso-bidi-font-weight:normal'><span lang=EN-GB
style='font-size:7.0pt;mso-bidi-font-size:10.0pt;font-family:Arial;color:#595959;
mso-themecolor:text1;mso-themetint:166;mso-ansi-language:EN-GB'><o:p>&nbsp;</o:p></span></b></p>

<p class=MsoNormal><span lang=FR style='font-size:9.0pt;mso-bidi-font-size:
10.0pt;color:#7F7F7F;mso-themecolor:text1;mso-themetint:128'><o:p>&nbsp;</o:p></span></p>

</div>



</div>