<?php include_once("ressources/analyticstracking.php") ?>


<header id="header" class="navbar-fixed-top navbar-fixed-top-index">
    <div class="container">
        <div class="row"  style="height: 40px;">
            <div class="logo-container--fix">
                <h1 class="logo"><a href="index.php"> <img src="images/logo_w_t_small.png" alt=""></a></h1>
            </div>
            <div class="nav-container--fix  <?php if (isset($_SESSION['user']['email'])) {echo 'iflogged';}?>">
                <nav class="navbar navbar-default custom_navbar" role="navigation" style="margin:0px">
                    <div class="container-fluid" style="width: 100%;margin: auto;margin-top: 6px;"> 
                        <!-- Brand and toggle get grouped for better mobile display -->
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"> 
                                <span class="sr-only">Toggle navigation</span> 
                                <span class="icon-bar"></span> 
                                <span class="icon-bar"></span> 
                                <span class="icon-bar"></span> 
                            </button>
                        </div>
                        <div class="collapse navbar-collapse custom_navigation" id="bs-example-navbar-collapse-1">
                            
                            <ul class="nav navbar-nav pull-right" style="text-shadow: none;">
                                <form class='form-search'  style='margin:0;' action='search_restaurant.php' method='POST' id='idFormsQuery' name='idFormsQuery'  enctype='multipart/form-data'>
                                <li class="search--input">
                                    <input class="form-control search-bar--topnav" id='restitle' name='restitle' placeholder='What / Where do you want to eat?' >

                                </li>

                                <li class="search--button">
                                    <button type="submit" class="btn btn-info btn-sm" style="margin: 0 10px;"><span class="glyphicon glyphicon-search"></span> &nbsp;Search</button>
                                </li>
                                </form>
                                <?php
                                if (!isset($_SESSION['user']['email'])) {
                                    //echo "<li class='simple--menu'><a class='login-register-link'  target='_blank'  href='modules/manage_booking/visitview.php'>Your booking</a></li>";
                                    echo "<li class='simple--menu'><a class='login-register-link' data-toggle='modal' data-target='#loginModal'  href='modules/login/login.php?state=register&platform=member'  onclick='LocalSignUp();'>Register</a></li>";
                                    echo "<li class='simple--menu'><a class='login-register-link'  data-toggle='modal' data-target='#loginModal'  href='modules/login/login.php?platform=member' onclick='LocalSignIn();'>Login</a></li>";
                                }
                                ?>            </ul>



                        </div>
                        <!-- Collect the nav links, forms, and other content for toggling -->

                        <!-- /.navbar-collapse --> 
                    </div>
                    <!-- /.container-fluid --> 
                </nav>
            </div>
            <div class='logged-in--fix'><?php
                                if (isset($_SESSION['user']['email'])) {echo '<div class="dropdown" style="margin-top: 14px; margin-left: 10px; float: right;">
                                                <a class="account" ><img src="images/ico_user-256.png"></a>
                                                <div class="submenu">
                                                <ul class="root">
                                               ';//<li ><a href="mydashboard">Dashboard</a></li>
                                                echo '
                                                <li ><a href="mybookings" >Bookings</a></li>
                                                <li ><a href="myreviews" >Reviews</a></li>
                                                <li ><a href="myaccount">Account</a></li>
                                                <li ><a href="modules/login/logout.php">SignOut</a></li>
                                                </ul>
                                                </div>

                                                </div>';
                                }?></div>
        </div>
    </div>

</header>

<div class="modal fade" id="loginModal" tabindex="-1" role="dialog" aria-labelledby="myLoginModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-body"></div>

        </div>
    </div>
</div>


<script type="text/javascript" >
	function LocalSignUp() {
		if(typeof ShowSignUp !== 'undefined') 
			ShowSignUp();
		}

	function LocalSignIn() {
		if(typeof ShowSignIn !== 'undefined') 
			ShowSignIn();
		}

    $(document).ready(function ()
    {

        $(".account").click(function ()
        {
            var X = $(this).attr('id');
            if (X == 1)
            {
                $(".submenu").hide();
                $(this).attr('id', '0');
            }
            else
            {
                $(".submenu").show();
                $(this).attr('id', '1');
            }

        });

//Mouse click on sub menu
        $(".submenu").mouseup(function ()
        {
            return false
        });

//Mouse click on my account link
        $(".account").mouseup(function ()
        {
            return false
        });


//Document Click
        $(document).mouseup(function ()
        {
            $(".submenu").hide();
            $(".account").attr('id', '');
        });
    });
</script>
