FROM ubuntu:17.04
MAINTAINER Kok How, Teh <khteh@weeloy.com>
# Install dependencies
RUN apt-get update -y
RUN apt-get install -y software-properties-common apt-utils curl wget sudo openssh-server apache2 apache2-utils python-software-properties php7.0 php7.0-fpm php7.0-mysql php-mbstring php-gettext libapache2-mod-php7.0 php7.0-mcrypt php7.0-bz2 php7.0-zip php7.0-xml php7.0-curl php7.0-gmp php7.0-json php7.0-opcache ufw sendmail jpegoptim php7.0-xdebug unzip dnsutils logrotate
# Set Global ServerName to Suppress Syntax Warnings
RUN add-apt-repository "deb http://cz.archive.ubuntu.com/ubuntu zesty web universe" && \
phpenmod mcrypt mbstring
#RUN echo "ServerName dev.weeloy.asia" >> /etc/apache2/apache2.conf
#RUN apt-get update -y


# Configure apache
ENV APACHE_RUN_USER=www-data APACHE_RUN_GROUP=www-data APACHE_LOG_DIR=/var/log/apache2 APACHE_PID_FILE=/var/run/apache2.pid APACHE_RUN_DIR=/var/run/apache2 APACHE_LOCK_DIR=/var/lock/apache2 APACHE_LOG_DIR=/var/log/apache2
RUN mkdir -p $APACHE_RUN_DIR $APACHE_LOCK_DIR $APACHE_LOG_DIR /etc/apache2/ssl /etc/PHP_LIB /root/.aws /var/www/html/tmp/twig_cache/ /var/log/xdebug
ADD feasting_asia.crt weeloy_asia.crt /etc/apache2/ssl/
ADD feasting_asia.key.unsecure /etc/apache2/ssl/feasting_asia.key
ADD weeloy_asia.key.unsecure /etc/apache2/ssl/weeloy_asia.key
RUN a2enconf php7.0-fpm && \
a2enmod proxy_fcgi setenvif rewrite ssl headers proxy_http deflate expires 
#RUN a2ensite dev.weeloy.asia.conf

# Install composer
RUN curl -sS https://getcomposer.org/installer | php && \
mv composer.phar /usr/bin/composer

# Install AWS CLI 
RUN curl -sS https://s3.amazonaws.com/aws-cli/awscli-bundle.zip -o awscli-bundle.zip && \
unzip awscli-bundle.zip && \
python3 awscli-bundle/install -i /usr/local/aws -b /usr/local/bin/aws

# Install Filebeat
RUN curl -sS -L -O https://artifacts.elastic.co/downloads/beats/filebeat/filebeat-5.3.0-amd64.deb && \
dpkg -i filebeat-5.3.0-amd64.deb
ADD filebeat.yml /etc/filebeat/filebeat.yml

ADD cronjob.txt /var/spool/cron/crontabs/root
ADD run.sh /usr/local/bin/run.sh
RUN chmod +x /usr/local/bin/run.sh

# here we have source code from sub image

##RUN service ssh start
##RUN service cron start
##RUN service filebeat start
WORKDIR /var/www

EXPOSE 80 443 8000
#ENTRYPOINT [ "/usr/sbin/apache2" ]
#CMD ["/usr/sbin/apache2", "-D",  "FOREGROUND"]
CMD ["/usr/local/bin/run.sh"]
